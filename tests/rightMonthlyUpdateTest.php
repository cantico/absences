<?php

require_once dirname(__FILE__).'/monthlyUpdate.php';


class absences_rightMonthlyUpdateTest extends absences_monthlyUpdate
{
    
    /**
     * This right exists in database
     * @return absences_Right
     */
    protected function getMockRight()
    {
        return new absences_Right(2); 
    }
    

    
    
    public function testIncrementTheCurrentMonth()
    {
        $this->resetRightQuantity();
        $right = $this->getMockRight();
        $result = $right->monthlyQuantityUpdate();
        
        $this->assertEquals(false, $result);
    }
    
    
    public function testIncrementParametersOnFirstMonth()
    {
        $right = $this->getMockRight();
        
        $this->assertEquals('0.00', $right->quantity);
        $this->assertNotEquals('0000-00-00 00:00:00', $right->quantity_inc_last);
    }
    
    
    public function testIncrementOnNextMonth()
    {
        $this->goBackOneMonth();
        
        $right = $this->getMockRight();
        $result = $right->monthlyQuantityUpdate();
        
        $this->assertEquals(true, $result);
    }
    
    public function testIncrementParametersOnNextMonth()
    {
        $right = $this->getMockRight();
        $this->assertEquals('0.00', $right->quantity, 'The initial quantity is not modified');
        $this->assertEquals(0.25, $right->getIncrementQuantity(), 'The increment quantity is modified');
    }
    
    
    public function testDoNotIncrementTwice()
    {
        $right = $this->getMockRight();
        $result = $right->monthlyQuantityUpdate();
        $this->assertEquals(false, $result);
        $this->assertEquals('0.00', $right->quantity);
        $this->assertEquals(0.25, $right->getIncrementQuantity(), 'The increment quantity is not modified after 2 monthly update the same month');
        
        // after reload from DB
        $right = $this->getMockRight();
        $this->assertEquals('0.00', $right->quantity);
        $this->assertEquals(0.25, $right->getIncrementQuantity(), 'The increment quantity is not modified after 2 monthly update the same month');
        
        $this->assertEquals(1, $right->getIncrementIterator()->count());
    }
    
    
    public function testIncrementTwice()
    {
        $this->goBackOneMonth();
        $right = $this->getMockRight();
        $result = $right->monthlyQuantityUpdate();
        $this->assertEquals(true, $result);
        $this->assertEquals('0.00', $right->quantity);
        $this->assertEquals(0.5, $right->getIncrementQuantity());
    
        // after reload from DB
        $right = $this->getMockRight();
        $this->assertEquals('0.00', $right->quantity);
        $this->assertEquals(0.5, $right->getIncrementQuantity());
        
        $this->assertEquals(2, $right->getIncrementIterator()->count());
    }
    
}
