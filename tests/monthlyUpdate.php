<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/../programs/utilit/right.class.php';
require_once dirname(__FILE__).'/../programs/utilit/agent.class.php';

class absences_monthlyUpdate extends PHPUnit_Framework_TestCase
{
    protected function resetRightQuantity()
    {
        global $babDB;
        $babDB->db_query('UPDATE absences_rights SET quantity = '.$babDB->quote(0).' WHERE id='.$babDB->quote(2));
        $babDB->db_query('UPDATE absences_users_rights SET quantity = '.$babDB->quote('').' WHERE id='.$babDB->quote(2));
        $babDB->db_query('UPDATE absences_users_rights SET quantity = '.$babDB->quote('2.33').' WHERE id='.$babDB->quote(4));
        
        $babDB->db_query('DELETE FROM absences_increment_right WHERE id_right='.$babDB->quote(2));
        $babDB->db_query('DELETE FROM absences_increment_user_right WHERE id_user_right IN('.$babDB->quote(array(2, 4)).')');
    }

    
    
    /**
     * Recule d'un mois la date de derniere modification de l'incrementation automatique
     */
    protected function goBackOneMonth()
    {
        global $babDB;
        $babDB->db_query('UPDATE absences_rights SET quantity_inc_last = DATE_SUB(quantity_inc_last, INTERVAL 1 MONTH) WHERE id='.$babDB->quote(2));
        
        $babDB->db_query('UPDATE absences_increment_right SET createdOn = DATE_SUB(createdOn, INTERVAL 1 MONTH) WHERE id_right='.$babDB->quote(2));
        $babDB->db_query('UPDATE absences_increment_user_right SET createdOn = DATE_SUB(createdOn, INTERVAL 1 MONTH) WHERE id_user_right IN('.$babDB->quote(array(2, 4)).')');
        
        $babDB->db_queryWem("UPDATE absences_increment_right SET monthkey = DATE_FORMAT(createdOn, '%Y%m') WHERE id_right=".$babDB->quote(2));
        $babDB->db_queryWem("UPDATE absences_increment_user_right SET monthkey = DATE_FORMAT(createdOn, '%Y%m') WHERE id_user_right IN(".$babDB->quote(array(2, 4)).')');
    }
}