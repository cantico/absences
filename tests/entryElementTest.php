<?php

require_once dirname(__FILE__).'/../programs/utilit/entry_elem.class.php';
require_once dirname(__FILE__).'/../programs/utilit/entry.class.php';
require_once dirname(__FILE__).'/../programs/utilit/entry_period.class.php';
require_once dirname(__FILE__).'/mock/mockentry.php';

class absences_entryElementTest extends absences_entryMocks
{

    
    protected function workingPeriods_2x4(absences_Entry $entry)
    {
        $entry->setWorkingPeriodIndex(array(
            '20150309am' => $this->mockWorkingPeriod('2015-03-09 08:00:00', '2015-03-09 12:00:00'),
            '20150309pm' => $this->mockWorkingPeriod('2015-03-09 14:00:00', '2015-03-09 18:00:00')
        ));
    }
    
    
    protected function workingPeriods_7h24(absences_Entry $entry)
    {
        $entry->setWorkingPeriodIndex(array(
            '20150309am' => $this->mockWorkingPeriod('2015-03-09 09:03:00', '2015-03-09 12:45:00'),
            '20150309pm' => $this->mockWorkingPeriod('2015-03-09 13:30:00', '2015-03-09 17:12:00')
        ));
    }
    
    protected function workingPeriods_7h24_on3Days(absences_Entry $entry)
    {
        $entry->setWorkingPeriodIndex(array(
            '20150309am' => $this->mockWorkingPeriod('2015-03-09 09:03:00', '2015-03-09 12:45:00'),
            '20150309pm' => $this->mockWorkingPeriod('2015-03-09 13:30:00', '2015-03-09 17:12:00'),
            '20150310am' => $this->mockWorkingPeriod('2015-03-10 09:03:00', '2015-03-10 12:45:00'),
            '20150310pm' => $this->mockWorkingPeriod('2015-03-10 13:30:00', '2015-03-10 17:12:00'),
            '20150311am' => $this->mockWorkingPeriod('2015-03-11 09:03:00', '2015-03-11 12:45:00'),
            '20150311pm' => $this->mockWorkingPeriod('2015-03-11 13:30:00', '2015-03-11 17:12:00')
        ));
    }
    
    
    protected function workingPeriods_7h24_twoHalfDays(absences_Entry $entry)
    {
        $entry->setWorkingPeriodIndex(array(
            '20150309pm' => $this->mockWorkingPeriod('2015-03-09 13:30:00', '2015-03-09 17:12:00'),
            '20150310am' => $this->mockWorkingPeriod('2015-03-10 09:03:00', '2015-03-10 12:45:00')
        ));
    }
    
    
    protected function oneElement(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 1;
        $elem->id_right = 1;
        
        $entry->addElement($elem);
    }
    
    
    protected function twoElements(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 0.25;
        $elem->id_right = 1;
    
        $entry->addElement($elem);
        
        $elem = new absences_EntryElem();
        $elem->quantity = 0.75;
        $elem->id_right = 2;
        
        $entry->addElement($elem); 
    }
    
    
    protected function twoHalfDays(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 0.5;
        $elem->id_right = 1;
    
        $entry->addElement($elem);
    
        $elem = new absences_EntryElem();
        $elem->quantity = 0.5;
        $elem->id_right = 2;
    
        $entry->addElement($elem);
    }
    
    
    protected function twoElementsOn3Days(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 0.5;
        $elem->id_right = 1;
    
        $entry->addElement($elem);
    
        $elem = new absences_EntryElem();
        $elem->quantity = 2;
        $elem->id_right = 2;
    
        $entry->addElement($elem);
    }
    
    
    protected function threeElementsOn2DaysAndHalf(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 0.5;
        $elem->id_right = 1;
    
        $entry->addElement($elem);
    
        $elem = new absences_EntryElem();
        $elem->quantity = 1;
        $elem->id_right = 2;
    
        $entry->addElement($elem);
        
        $elem = new absences_EntryElem();
        $elem->quantity = 1;
        $elem->id_right = 3;
        
        $entry->addElement($elem);
    }
    
    
    
    protected function twoElementsReverse(absences_Entry $entry)
    {
        $elem = new absences_EntryElem();
        $elem->quantity = 0.25;
        $elem->id_right = 2;
    
        $entry->addElement($elem);
    
        $elem = new absences_EntryElem();
        $elem->quantity = 0.75;
        $elem->id_right = 1;
    
        $entry->addElement($elem);
    
    
    }
    
    
    
    /**
     * Entry with element and dates set on element
     * @return absences_Entry
     */
    protected function createEntry($workingPeriodsMethod, $elementsMethod, $date_begin = '2015-03-09 08:00:00', $date_end = '2015-03-09 18:00:00')
    {
        $entry = new absences_Entry();
        $entry->id = 1;
        $entry->id_user = 1;
        $entry->comment = '';

        $entry->date_begin = $date_begin;
        $entry->date_end = $date_end;
        
        $this->$workingPeriodsMethod($entry);

        $entry->loadPlannedPeriods();
        
        $this->$elementsMethod($entry);
        
        $entry->setElementsDates();
        
        return $entry;
    }
    
    
    
    
    public function testgetElement()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'oneElement');
        $elem = $entry->getElement(1);
        $this->assertEquals('2015-03-09 08:00:00', $elem->date_begin);
        $this->assertEquals('2015-03-09 18:00:00', $elem->date_end);
    }
    
    
    public function testgetElements()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'oneElement');
        $elems = $entry->getElements();
        $this->assertEquals(1, count($elems));
    }
    

    public function testOneElementDates()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'oneElement');
        $elems = $entry->getElements();
        
        $this->assertEquals('2015-03-09 08:00:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 18:00:00', $elems[0]->date_end);
    }
    
    
    
    public function testOnTwoRightsQuantity()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'twoElements');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
    
        // elements dates must match the planned periods and current working period index and quanties set on elements
    
        $this->assertEquals('0.25', $elems[0]->quantity);
        $this->assertEquals('0.75', $elems[1]->quantity);
    }
    
    
    public function testOnTwoRightsDates()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'twoElements');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
    
        // elements dates must match the planned periods and current working period index and quanties set on elements

        // 0.25
        $this->assertEquals('2015-03-09 08:00:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 10:00:00', $elems[0]->date_end);
        
        // 0.75
        $this->assertEquals('2015-03-09 10:00:00', $elems[1]->date_begin);
        $this->assertEquals('2015-03-09 18:00:00', $elems[1]->date_end);
    }
    
    
    public function testSortkeyOnRights()
    {
        $entry = $this->createEntry('workingPeriods_2x4', 'twoElementsReverse');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
    
        // elements dates must match the planned periods and current working period index and quanties set on elements

        // 0.75
        $this->assertEquals(1, $elems[0]->getRight()->sortkey);
        $this->assertEquals('2015-03-09 08:00:00', $elems[0]->date_begin, 'begin date of element 1');
        $this->assertEquals('2015-03-09 16:00:00', $elems[0]->date_end, 'end date of element 1');
        
        // 0.25
        $this->assertEquals(2, $elems[1]->getRight()->sortkey);
        $this->assertEquals('2015-03-09 16:00:00', $elems[1]->date_begin, 'begin date of element 2');
        $this->assertEquals('2015-03-09 18:00:00', $elems[1]->date_end, 'end date of element 2');
    }
    
    
    
    
    public function testOnTwoRightsDatesWith7h24()
    {
        $entry = $this->createEntry('workingPeriods_7h24', 'twoElements', '2015-03-09 09:03:00', '2015-03-09 17:12:00');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
    
        // elements dates must match the planned periods and current working period index and quanties set on elements
        
        // 7h24 = 444 min
        // 0,25 days = 111 min = 1h51
        // 0,75 days = 333 min = 5h33
        
        // mid-day break 45min
    
        // 0.25 days
        $this->assertEquals('2015-03-09 09:03:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 10:54:00', $elems[0]->date_end);
    
        // 0.75 days
        // avec pause, 378 min  16:18
        $this->assertEquals('2015-03-09 10:54:00', $elems[1]->date_begin);
        $this->assertEquals('2015-03-09 17:12:00', $elems[1]->date_end);
    }
    
    
    
    public function testOnTwoRightsDatesWith7h24_moreDays()
    {
        $entry = $this->createEntry('workingPeriods_7h24_on3Days', 'twoElementsOn3Days', '2015-03-09 13:30:00', '2015-03-11 17:12:00');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
    
        // elements dates must match the planned periods and current working period index and quanties set on elements
    
        // 0.5 days
        $this->assertEquals('2015-03-09 13:30:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 17:12:00', $elems[0]->date_end);
    
        // 2 days
        $this->assertEquals('2015-03-10 09:03:00', $elems[1]->date_begin);
        $this->assertEquals('2015-03-11 17:12:00', $elems[1]->date_end);
    }
    
    
    
    
    
    
    public function testOnTwoHalfDaysWith7h24()
    {
        // la periode de la matinee va jusqu'a 12:45 mais l'utilisateur a choisis 11:59:59
        // c'est une contrainte imposee par le planning (ne pas depasser 12:00 pour le matin)
        
        $entry = $this->createEntry('workingPeriods_7h24_twoHalfDays', 'twoHalfDays', '2015-03-09 13:30:00', '2015-03-10 11:59:59');
        $elems = $entry->getElements();
        $this->assertEquals(2, count($elems));
        
        /*
         * 
         * Resulat avec l'ancien fonctionnement
         * la decoupe se fait a plus en fonction des horaires de travail
         * le nouveau fonctionnement utilise une proportionalite
    
        // 0.5 days
        $this->assertEquals('2015-03-09 13:30:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 17:12:00', $elems[0]->date_end);
    
        // 0.5 days
        $this->assertEquals('2015-03-10 09:03:00', $elems[1]->date_begin);
        $this->assertEquals('2015-03-10 11:59:59', $elems[1]->date_end);
        
        */
        
        
        // 0.5 days
        $this->assertEquals('2015-03-09 13:30:00', $elems[0]->date_begin);
        $this->assertEquals('2015-03-09 15:58:12', $elems[0]->date_end);
        
        // 0.5 days
        $this->assertEquals('2015-03-09 15:58:12', $elems[1]->date_begin);
        $this->assertEquals('2015-03-10 10:17:24', $elems[1]->date_end); // il reste un blanc jusqu'a 12h !
    }
    
    
    
    
    
    protected function workingPeriods_7h24_overWeekEnd(absences_Entry $entry)
    {
        $entry->setWorkingPeriodIndex(array(
            '20150807am' => $this->mockWorkingPeriod('2015-08-07 09:03:00', '2015-08-07 12:45:00'),
            '20150807pm' => $this->mockWorkingPeriod('2015-08-07 13:30:00', '2015-08-07 17:12:00'),
            
            '20150810am' => $this->mockWorkingPeriod('2015-08-10 09:03:00', '2015-08-10 12:45:00'),
            '20150810pm' => $this->mockWorkingPeriod('2015-08-10 13:30:00', '2015-08-10 17:12:00'),
            
            '20150811am' => $this->mockWorkingPeriod('2015-08-11 09:03:00', '2015-08-11 12:45:00'),
            '20150811pm' => $this->mockWorkingPeriod('2015-08-11 13:30:00', '2015-08-11 17:12:00')
        ));
    }
    

    
    
    public function testOnThreeElementsOn2DaysAndHalfWith7h24()
    {

    
        $entry = $this->createEntry('workingPeriods_7h24_overWeekEnd', 'threeElementsOn2DaysAndHalf', '2015-08-07 09:03:00', '2015-08-11 11:59:59');
        $elems = $entry->getElements();
        $this->assertEquals(3, count($elems));
    
        // 0.5 days
        $this->assertEquals('2015-08-07 09:03:00', $elems[0]->date_begin, 'right 1 start date');
        $this->assertEquals('2015-08-07 12:45:00', $elems[0]->date_end, 'right 1 end date');
    
        // 1 day
        $this->assertEquals('2015-08-07 13:30:00', $elems[1]->date_begin, 'right 2 start date');
        $this->assertEquals('2015-08-10 12:45:00', $elems[1]->date_end, 'right 2 end date');
        
        // 1 day
        $this->assertEquals('2015-08-10 13:30:00', $elems[2]->date_begin, 'right 3 start date');
        $this->assertEquals('2015-08-11 11:59:59', $elems[2]->date_end, 'right 3 end date');
    }
}