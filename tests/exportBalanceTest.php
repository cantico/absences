<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/../programs/utilit/agent.class.php';
require_once dirname(__FILE__).'/../programs/utilit/entry.class.php';
require_once dirname(__FILE__).'/../programs/utilit/cet_deposit_request.class.php';
require_once dirname(__FILE__).'/../programs/utilit/increment_right.class.php';



class absences_exportBalanceTest extends PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        global $babDB;
        $babDB->db_query('TRUNCATE absences_users_rights');
        
        $babDB->db_query("INSERT INTO absences_users_rights (id, id_user, id_right, quantity) VALUES (1, 1, 1, '')");
        
        $babDB->db_query("INSERT INTO absences_users_rights_history (id_user_right, date_end, linkexists, quantity)
	        VALUES (1, '2015-01-01 00:00:00', 0, '')");
    }
    
    
    /**
     * This agent does not exists in database
     */
    protected function getMockAgent()
    {
        $agent = new absences_Agent();
        $agent->setRow(array(
            'id' => 1,
            'id_user' => 1
        ));
    
        return $agent;
    }
    
    
    protected function getMockRight()
    {
        $right = new absences_Right(1);
        
        
        return $right;
    }
    
    
    
    protected function getMockAgentRight()
    {
        return absences_AgentRight::getById(1);
    }
    
    
    
    public function testNoConsuptionAvailableQuantity() {
        
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity());
    }
    
    
    public function testNoConsuptionOnDateAvailableQuantity() {
    
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-01-01')); 
        // createdOn not set, considered at 0000-00-00 00:00:00
    }
    
    
    public function testNoRightOnDateAvailableQuantity() {
    
        $agentRight = $this->getMockAgentRight();
        $right = $agentRight->getRight();
        $row = $right->getRow();
        $row['createdOn'] = '2015-01-02 00:00:00';
        $right->setRow($row);
        $this->assertEquals(0, $agentRight->getAvailableQuantity('2015-01-01'));
    }
    
    
    protected function consume4days()
    {
        $agentRight = $this->getMockAgentRight();

        $entry = new absences_Entry();
        $entry->createdOn = '2015-01-30 14:00:00';
        $entry->date = '2015-01-30 14:00:00';
        $entry->id_user = 1;
        $entry->status = 'Y';
        $entry->date_begin = '2015-02-01 00:00:00';
        $entry->date_end = '2015-02-04 00:00:00';
        $entry->save();
        
        $elem = new absences_EntryElem();
        $elem->date_begin = '2015-02-01 00:00:00';
        $elem->date_end = '2015-02-04 00:00:00';
        $elem->quantity = 4;
        $elem->id_right = 1;
        $elem->id_entry = $entry->id;
        $elem->save();
        
        return $entry;
    }
    
    
    protected function dropConsuption()
    {
        global $babDB;
        
        $babDB->db_query('TRUNCATE absences_entries');
        $babDB->db_query('TRUNCATE absences_entries_elem');
        
        $babDB->db_query('TRUNCATE absences_movement');
    }
    
    
    public function testConsumedNoDate() {
        $this->consume4days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(21, $agentRight->getAvailableQuantity());
        $this->dropConsuption();
    }
    
    
    public function testAfterConsuption() {
        $this->consume4days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(21, $agentRight->getAvailableQuantity('2015-02-15'));
        $this->dropConsuption();
    }
    
    
    public function testBeforeConsuption() {
        $this->consume4days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-01-15'));
        $this->dropConsuption();
    }
    

    

    public function testManagerUpdate()
    {
        $this->consume4days();
        $agentRight = $this->getMockAgentRight();
        $agentRight->quantity = 30;
        $this->assertEquals(26, $agentRight->getAvailableQuantity());
        $this->dropConsuption();
    }
    
    public function testManagerUpdateHistory()
    {
        global $babDB;
        
        // was set to 40 in january (25 was the initial quantity from right)
        $babDB->db_query("INSERT INTO absences_users_rights_history (id_user_right, date_end, linkexists, quantity)
	        VALUES (1, '2015-01-15 00:00:00', 1, '')");
        // and get back to default in march
        $babDB->db_query("INSERT INTO absences_users_rights_history (id_user_right, date_end, linkexists, quantity)
	        VALUES (1, '2015-03-01 00:00:00', 1, 40)");
        
        
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals('', $agentRight->quantity);
        
        $this->consume4days();
        
        $this->assertEquals(40, $agentRight->getAvailableQuantity("2015-01-15")); // after modification and before consuption
        $this->assertEquals(36, $agentRight->getAvailableQuantity("2015-02-01"));
        $this->assertEquals(21, $agentRight->getAvailableQuantity("2015-03-01")); // after removing modification and after consuption
        $this->assertEquals(21, $agentRight->getAvailableQuantity("2015-04-02"));
        $this->assertEquals(21, $agentRight->getAvailableQuantity());
        
        $this->dropConsuption();
        
        $babDB->db_query("TRUNCATE absences_users_rights_history");
    }
    
    
    
    public function testSetWaiting()
    {
        $entry = $this->consume4days();
        
        $this->assertEquals('Y', $entry->getDateStatus('2015-02-15'));
        
        $entry->status = '';
        $entry->save();
        $entry->addMovement('setwaiting', 'change status', '2015-02-01');
        
        $this->assertEquals('', $entry->getDateStatus('2015-02-15'));
        
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-02-15'));
        $this->dropConsuption();
    }
    
    
    
    public function testSetConfirmed()
    {
        $entry = $this->consume4days();

        $entry->status = '';
        $entry->save();
        $entry->addMovement('setwaiting', 'change status', '2015-02-01');
        
        $this->assertEquals('', $entry->getDateStatus('2015-02-03'));

        $entry->status = 'Y';
        $entry->save();
        $entry->addMovement('setwaiting', 'change status', '2015-02-03');

        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(21, $agentRight->getAvailableQuantity('2015-02-03'));
        $this->dropConsuption();
    }
    


    protected function move3days()
    {
        $request = new absences_CetDepositRequest();
        
        $request->createdOn = '2015-04-01 00:00:00';
        $request->status = 'Y';
        $request->id_agent_right_source = 1;
        $request->id_agent_right_cet = 4; // does not exists!
        $request->id_user = 1;
        $request->quantity = 3;
        $request->comment = 'Test';
        $request->comment2 = '';
        $request->id_approver = 0;
        $request->idfai = 0;
        
        $request->save();
        
        return $request;
    }
    
    
    protected function dropMove()
    {
        global $babDB;
        $babDB->db_query('TRUNCATE absences_cet_deposit_request');
        $babDB->db_query('TRUNCATE absences_movement');
    }
    
    
    public function testMoveToCet()
    {
        $this->move3days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(22, $agentRight->getAvailableQuantity());
        $this->dropMove();
    }
    
    
    public function testBeforeMoveToCet()
    {
        $this->move3days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-02-01'));
        $this->dropMove();
    }
    
    public function testAfterMoveToCet()
    {
        $this->move3days();
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(22, $agentRight->getAvailableQuantity('2015-04-02'));
        $this->dropMove();
    }
    
    
    public function testCetSetWaiting()
    {
        $request = $this->move3days();
    
        $this->assertEquals('Y', $request->getDateStatus('2015-04-15'));
    
        $request->status = '';
        $request->save();
        $request->addMovement('setwaiting', 'change status', '2015-05-01');
    
        $this->assertEquals('', $request->getDateStatus('2015-05-02'));
    
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-05-02'));
        $this->dropMove();
    }
    
    
    
    public function testCetSetConfirmed()
    {
        $request = $this->move3days();
    
        $this->assertEquals('Y', $request->getDateStatus('2015-04-15'));
    
        $request->status = '';
        $request->save();
        $request->addMovement('setwaiting', 'change status', '2015-05-01');
    

        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-05-02'));
        
        $request->status = 'Y';
        $request->save();
        
        $request->addMovement('setconfirmed', 'change status', '2015-05-03');
        
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-05-02'));
        $this->assertEquals(22, $agentRight->getAvailableQuantity('2015-05-04'));
        
        $this->dropMove();
    }
    
    


    protected function save5IncMonth($date)
    {

        $increment = new absences_IncrementRight();
        $increment->id_right = 1;
        $increment->quantity = 5;
        $increment->createdOn = date($date);
        $increment->monthkey = date('Ym', bab_mktime($date));
    
        $increment->saveOrUpdate();
    }
    
    
    protected function dropIncMonth()
    {
        global $babDB;
        $babDB->db_query('TRUNCATE absences_increment_right');
    }
    
    
    public function testIncMonth()
    {
        $this->save5IncMonth('2015-01-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(30, $agentRight->getAvailableQuantity());
        $this->dropIncMonth();
    }
    
    public function testBeforeIncMonth()
    {
        $this->save5IncMonth('2015-02-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-01-15'));
        $this->dropIncMonth();
    }
    
    public function testAfterIncMonth()
    {
        $this->save5IncMonth('2015-02-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(30, $agentRight->getAvailableQuantity('2015-02-15'));
        $this->dropIncMonth();
    }
    
    
    protected function save6DynamicRight($date)
    {
        $dynamic_right = new absences_DynamicRight();
        $dynamic_right->id_entry = 1;
        $dynamic_right->createdOn = $date;
        $dynamic_right->id_user_right = 1;
        $dynamic_right->quantity = 6;
        
        $dynamic_right->save();
    }
    
    protected function dropDynamicRight()
    {
        global $babDB;
        $babDB->db_query('TRUNCATE absences_dynamic_rights');
    }
    
    
    public function testDynRight()
    {
        $this->save6DynamicRight('2015-01-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(31, $agentRight->getAvailableQuantity());
        $this->dropDynamicRight();
    }
    
    public function testBeforeDynRight()
    {
        $this->save6DynamicRight('2015-02-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(25, $agentRight->getAvailableQuantity('2015-01-15'));
        $this->dropDynamicRight();
    }
    
    public function testAfterDynRight()
    {
        $this->save6DynamicRight('2015-02-01 00:00:00');
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(31, $agentRight->getAvailableQuantity('2015-02-15'));
        $this->dropDynamicRight();
    }
    
    
    
    
    
}