<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/../programs/utilit/agent.class.php';
require_once dirname(__FILE__).'/../programs/utilit/right.class.php';
require_once dirname(__FILE__).'/../programs/utilit/agent_right.class.php';

class absences_agentRightTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * This agent does not exists in database
     */
    protected function getMockAgent()
    {
        $agent = new absences_Agent();
        $agent->setRow(array(
            'id' => 1,
            'id_user' => 1
        ));
        
        return $agent;
    }
    
    protected function getMockRight()
    {
        $right = new absences_Right(null);
        $right->setRow(array(
            
            'quantity' => '25.0',
            'quantity_unit' => 'D'
        ));
        
        return $right;
    }
    
    protected function getMockEmptyRight()
    {
        $right = new absences_Right(null);
        $right->setRow(array(
    
            'quantity' => '0.0',
            'quantity_unit' => 'D'
        ));
    
        return $right;
    }
    
    
    protected function getMockAgentRight()
    {
        $agentRight = new absences_AgentRight();
        $agentRight->setRow(array(
            'quantity' => ''
        ));
        
        $agentRight->setRight($this->getMockRight());
        $agentRight->setAgent($this->getMockAgent());
        
        return $agentRight;
    }
    
    
    protected function getModifiedMockAgentRightOnEmptyRight()
    {
        $agentRight = new absences_AgentRight();
        $agentRight->setRow(array(
            'quantity' => '3'
        ));
    
        $agentRight->setRight($this->getMockEmptyRight());
        $agentRight->setAgent($this->getMockAgent());
    
        return $agentRight;
    }
    
    
    
    public function testGetQuantityFromInput_SetValue()
    {
        $agentRight = $this->getMockAgentRight();

        $this->assertEquals('3.5', $agentRight->getQuantityFromInput('3,5'));
        $this->assertEquals('3.55', $agentRight->getQuantityFromInput('3,55'));
        $this->assertEquals('3.55', $agentRight->getQuantityFromInput('3,554'));
        $this->assertEquals('3.56', $agentRight->getQuantityFromInput('3,559'));
        $this->assertEquals('-3.56', $agentRight->getQuantityFromInput('-3,559'));
    }
    
    
    public function testGetQuantityFromInput_RemoveQuantity()
    {
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals('0', $agentRight->getQuantityFromInput('0'));
    }
    
    public function testGetQuantityFromInput_SetDefault()
    {
        $agentRight = $this->getMockAgentRight();
        $agentRight->quantity = '20';
        $this->assertEquals('', $agentRight->getQuantityFromInput(''));
        $this->assertEquals('', $agentRight->getQuantityFromInput('25'));
    }
    
    public function testGetQuantityFromInput_StatusNotModified()
    {
        $agentRight = $this->getMockAgentRight();
        $this->assertEquals(null, $agentRight->getQuantityFromInput('25'));
        $this->assertEquals(null, $agentRight->getQuantityFromInput(''));
    }
    
    public function testGetQuantityFromInput_ValueNotModified()
    {
        $agentRight = $this->getMockAgentRight();
        $agentRight->quantity = '20';
        
        $this->assertEquals(null, $agentRight->getQuantityFromInput('20'));
        $this->assertEquals(null, $agentRight->getQuantityFromInput('20,0'));
    }
    
    
    public function testGetQuantityFromInput_ValueNotModified2()
    {
        $agentRight = $this->getModifiedMockAgentRightOnEmptyRight();
        $agentRight->quantity = '3';
    
        $this->assertEquals(null, $agentRight->getQuantityFromInput('3'));
        $this->assertEquals(null, $agentRight->getQuantityFromInput('3,0'));
    }
    
    
    public function testGetQuantityFromInput_BackToDefault()
    {
        $agentRight = $this->getMockAgentRight();
        $agentRight->quantity = '0';
    
        $this->assertEquals('', $agentRight->getQuantityFromInput(''));
        $this->assertEquals('', $agentRight->getQuantityFromInput('25'));
    }
    
    public function testGetQuantityFromInput_BackToDefault2()
    {
        $agentRight = $this->getModifiedMockAgentRightOnEmptyRight();
        $agentRight->quantity = '3';
    
        $this->assertEquals('', $agentRight->getQuantityFromInput(''));
        $this->assertEquals('', $agentRight->getQuantityFromInput('0'));
    }
    
    public function testGetQuantityFromInput_BackToDefault3()
    {
        $agentRight = $this->getModifiedMockAgentRightOnEmptyRight();
        $agentRight->quantity = '0';
    
        $this->assertEquals('', $agentRight->getQuantityFromInput(''));
        $this->assertEquals('', $agentRight->getQuantityFromInput('0'));
    }
    
    
    public function testGetQuantityFromInput_BackToValue()
    {
        $agentRight = $this->getMockAgentRight();
        $agentRight->quantity = '0';

        $this->assertEquals('25.1', $agentRight->getQuantityFromInput('25,1'));
        $this->assertEquals('-2.1', $agentRight->getQuantityFromInput('-2,1'));
    }
   
}
