<?php


require_once dirname(__FILE__).'/mock/mockentry.php';


/**
 * Tests on planned periods
 */
class absences_entryPeriodTest extends absences_entryMocks
{
    
    /*
     * On element on entry
     * 
     * from 2014-02-01 08:00:00
     * to   2014-02-01 18:00:00
     */
    
    
    public function testCreatePlannedPeriods()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
        
        $periods = $entry->getPlannedPeriods();
        
        $this->assertInternalType('array', $periods);
        $this->assertEquals(2, count($periods));
    }
    
    
    
    public function testCreateIfNotInDatabase()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->loadPlannedPeriods();
        $periods = $entry->getPlannedPeriods();
        
        $this->assertInternalType('array', $periods);
        $this->assertEquals(2, count($periods));
    }
    
    
    public function testPlannedDurationDays()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
        
        $days = $entry->getPlannedDurationDays();
        $this->assertEquals(1.0, $days, '', 0.01);
    }
    
    
    public function testPlannedDurationHours()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
    
        $hours = $entry->getPlannedDurationHours();
        $this->assertEquals(8.0, $hours, '', 0.01);
    }
    
    
    
    public function testPlannedDurationDaysAM()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
    
        $days = $entry->getPlannedDurationDays('2014-01-01 08:00:00', '2014-01-01 12:00:00');
        $this->assertEquals(0.5, $days, '', 0.01);
    }
    
    
    public function testPlannedDurationHoursAM()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
    
        $hours = $entry->getPlannedDurationHours('2014-01-01 08:00:00', '2014-01-01 12:00:00');
        $this->assertEquals(4.0, $hours, '', 0.01);
    }
    
    
    
    public function testPlannedDurationDaysOutOfBounds()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
    
        $days = $entry->getPlannedDurationDays('2014-01-02 08:00:00', '2014-01-02 12:00:00');
        $this->assertEquals(0.0, $days, '', 0.01);
    }
    
    
    public function testPlannedDurationHoursOutOfBounds()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        $entry->createPlannedPeriods();
    
        $hours = $entry->getPlannedDurationHours('2014-01-02 08:00:00', '2014-01-02 12:00:00');
        $this->assertEquals(0.0, $hours, '', 0.01);
    }
    
    
    public function testPlannedDurationSimpleEntry()
    {
        $entry = $this->getMockSimpleEntryOnStrangeWorkingPeriodSet(1, 'D');
        $entry->createPlannedPeriods();
    
        $days = $entry->getPlannedDurationDays('2015-06-22 00:00:00', '2015-06-23 00:00:00');
        $this->assertEquals(1, $days, '', 0.01);
    }
    
    
    /**
     *  Two elements on entry 
     * 
     * from 2014-01-01 08:00:00
     * to   2014-01-03 18:00:00
     */
    public function testWithTwoElementsCreatePlannedPeriods()
    {
        $entry = $this->getMockEntryWithTwoElements(1, 'D', 2, 'D');
        $entry->createPlannedPeriods();
    
        $periods = $entry->getPlannedPeriods();
    
        $this->assertInternalType('array', $periods);
        $this->assertEquals(6, count($periods));
        
        // entry periods must match working periods 
        
        $this->assertEquals('2014-01-01 08:00:00', $periods[0]->date_begin);
        $this->assertEquals('2014-01-01 12:00:00', $periods[0]->date_end);
        
        $this->assertEquals('2014-01-01 14:00:00', $periods[1]->date_begin);
        $this->assertEquals('2014-01-01 18:00:00', $periods[1]->date_end);
        
        $this->assertEquals('2014-01-02 08:00:00', $periods[2]->date_begin);
        $this->assertEquals('2014-01-02 12:00:00', $periods[2]->date_end);
        
        $this->assertEquals('2014-01-02 14:00:00', $periods[3]->date_begin);
        $this->assertEquals('2014-01-02 18:00:00', $periods[3]->date_end);
        
        $this->assertEquals('2014-01-03 08:00:00', $periods[4]->date_begin);
        $this->assertEquals('2014-01-03 12:00:00', $periods[4]->date_end);
        
        $this->assertEquals('2014-01-03 14:00:00', $periods[5]->date_begin);
        $this->assertEquals('2014-01-03 18:00:00', $periods[5]->date_end);

        
        $elements = $entry->getElements();
        
        $this->assertEquals(2, count($elements));
        
        $this->assertEquals('2014-01-01 08:00:00', $elements[0]->date_begin);
        $this->assertEquals('2014-01-01 18:00:00', $elements[0]->date_end);
        $this->assertEquals(1, $elements[0]->quantity);
        
        $this->assertEquals('2014-01-02 08:00:00', $elements[1]->date_begin);
        $this->assertEquals('2014-01-03 18:00:00', $elements[1]->date_end);
        $this->assertEquals(2, $elements[1]->quantity);
        
    }
    
    
    protected function assertVacationRequestOnStrangeWorkingPeriods($quantity1, $unit1, $quantity2, $unit2)
    {
        $entry = $this->getMockEntryOnStrangeWorkingPeriodSet($quantity1, $unit1, $quantity2, $unit2);
        $entry->createPlannedPeriods();
        
        $periods = $entry->getPlannedPeriods();
        
        $this->assertInternalType('array', $periods);
        $this->assertEquals(6, count($periods));
        
        // entry periods must match working periods
        
        $this->assertEquals('2014-01-01 09:03:00', $periods[0]->date_begin);
        $this->assertEquals('2014-01-01 12:45:00', $periods[0]->date_end);
        
        $this->assertEquals('2014-01-01 13:30:00', $periods[1]->date_begin);
        $this->assertEquals('2014-01-01 17:12:00', $periods[1]->date_end);
        
        $this->assertEquals('2014-01-02 09:03:00', $periods[2]->date_begin);
        $this->assertEquals('2014-01-02 12:45:00', $periods[2]->date_end);
        
        $this->assertEquals('2014-01-02 13:30:00', $periods[3]->date_begin);
        $this->assertEquals('2014-01-02 17:12:00', $periods[3]->date_end);
        
        $this->assertEquals('2014-01-03 09:03:00', $periods[4]->date_begin);
        $this->assertEquals('2014-01-03 12:45:00', $periods[4]->date_end);
        
        $this->assertEquals('2014-01-03 13:30:00', $periods[5]->date_begin);
        $this->assertEquals('2014-01-03 17:12:00', $periods[5]->date_end);
        
        
        $elements = $entry->getElements();
        
        $this->assertEquals(2, count($elements));
        
        // first and last date are base on request start and finish date
        
        $this->assertEquals('2014-01-01 08:00:00', $elements[0]->date_begin, 'first element start date');
        $this->assertEquals($quantity1, $elements[0]->quantity);

        $this->assertEquals('2014-01-03 18:00:00', $elements[1]->date_end, 'last element end date');
        $this->assertEquals($quantity2, $elements[1]->quantity);
    }
    
    
    public function testOnStrangeWorkingPeriod()
    {
        $this->assertVacationRequestOnStrangeWorkingPeriods(1, 'D', 2, 'D');
    
    }
    
    
    
    public function testOnStrangeWorkingPeriodWithHalfDays()
    {
        $this->assertVacationRequestOnStrangeWorkingPeriods(0.5, 'D', 2.5, 'D');
        $this->assertVacationRequestOnStrangeWorkingPeriods(1.5, 'D', 1.5, 'D');
    }

    
    /**
     * T8809
     */
    public function testWithTwoElementsCreatePlannedPeriodsStartingAt12()
    {
        $entry = $this->getMockEntryStartingAt12(1, 'D', 0.5, 'D');
        $entry->createPlannedPeriods();
    
        $periods = $entry->getPlannedPeriods();

        $this->assertInternalType('array', $periods);
        $this->assertEquals(5, count($periods));
        
        
    
        // entry periods must match working periods
    
        $this->assertEquals('2015-06-05 12:00:00', $periods[0]->date_begin);
        $this->assertEquals('2015-06-05 12:45:00', $periods[0]->date_end);
        $this->assertEquals('2015-06-05 13:30:00', $periods[1]->date_begin);
        $this->assertEquals('2015-06-05 17:12:00', $periods[1]->date_end);
        
        // AM
        $this->assertEquals('2015-06-08 09:03:00', $periods[2]->date_begin);
        $this->assertEquals('2015-06-08 12:00:00', $periods[2]->date_end);
    
        // PM
        $this->assertEquals('2015-06-08 12:00:00', $periods[3]->date_begin);
        $this->assertEquals('2015-06-08 12:45:00', $periods[3]->date_end);
        $this->assertEquals('2015-06-08 13:30:00', $periods[4]->date_begin);
        $this->assertEquals('2015-06-08 17:12:00', $periods[4]->date_end);
        
        
        $elements = $entry->getElements();
        
        $this->assertEquals(2, count($elements));
        
        $this->assertEquals('2015-06-05 12:00:00', $elements[0]->date_begin);
        $this->assertEquals('2015-06-08 12:30:00', $elements[0]->date_end);
        $this->assertEquals(1, $elements[0]->quantity);
        
        $this->assertEquals('2015-06-08 12:30:00', $elements[1]->date_begin);
        $this->assertEquals('2015-06-08 17:12:00', $elements[1]->date_end);
        $this->assertEquals(0.5, $elements[1]->quantity);
        
    }
    
    
    /**
     * mail du 03/10/2016
     */
    public function testOnMonthChange()
    {
        $entry = $this->getMockEntryWithMonthChange();
        $entry->createPlannedPeriods();
        
        $elements = $entry->getElements();

        $this->assertEquals(3, count($elements));
        
        $this->assertEquals('2016-07-27 08:00:00', $elements[0]->date_begin);
        $this->assertEquals('2016-08-03 14:00:00', $elements[0]->date_end);
        $this->assertEquals('2016-08-03 14:00:00', $elements[1]->date_begin);
        $this->assertEquals('2016-08-04 14:00:00', $elements[1]->date_end);
        $this->assertEquals('2016-08-04 14:00:00', $elements[2]->date_begin);
        $this->assertEquals('2016-08-05 18:00:00', $elements[2]->date_end);
        
    }
}