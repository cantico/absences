<?php

require_once dirname(__FILE__).'/mock/mockentry.php';

class absences_entryTest extends absences_entryMocks
{

    
    public function testId()
    {
        $entry = $this->getMockEntryWithOneElement(1, 'D');
        
        $this->assertEquals(1, $entry->id);
    }
    
    
    public function testCountElements()
    {
        $entry = $this->getMockEntryWithTwoElements(2, 'D', 1, 'D');
        
        $this->assertEquals(2, count($entry->getElements()));
    }
    
    
    public function testGetElement()
    {
        $entry = $this->getMockEntryWithTwoElements(2, 'D', 1, 'D');
        $element1 = $entry->getElement(1);
        $element2 = $entry->getElement(2);
    
        $this->assertEquals(2, $element1->quantity);
        $this->assertEquals(1, $element2->quantity);
    }
    
    
    public function testGetTotalHours_3days()
    {
        $entry = $this->getMockEntryWithTwoElements(2, 'D', 1, 'D');
        $this->assertEquals(24, $entry->getTotalHours());
    }
    
    public function testGetTotalHours_2days()
    {
        $entry = $this->getMockEntryWithTwoElements(0.25, 'D', 1.75, 'D');
        $this->assertEquals(16, $entry->getTotalHours());
    }
    
    public function testGetTotalHours_monthChange()
    {
        $entry = $this->getMockEntryWithMonthChange();
        $this->assertEquals(8*8, $entry->getTotalHours());
    }
    
    
    /**
     * Si un seul element dans la demande, la periode de l'element
     * doit toujours corespondre a la periode de la demande
     */
    public function setElementsDatesOnOneElement($duration, $unit)
    {
        $entry = $this->getMockEntryWithOneElement($duration, $unit);
        $entry->setElementsDates();
    
        $this->assertEquals('2014-02-01 08:00:00', $entry->getElement(1)->date_begin);
        $this->assertEquals('2014-02-01 18:00:00', $entry->getElement(1)->date_end);
    }
    
    
    public function testSetElementsDatesOnOneElementMatchedDuration()
    {
        $this->setElementsDatesOnOneElement(1, 'D');
        $this->setElementsDatesOnOneElement(8, 'H');
    }
    
    
    public function testSetElementsDatesOnOneElementUnmatchedDuration()
    {
        $this->setElementsDatesOnOneElement(1.1, 'D');
        $this->setElementsDatesOnOneElement(0.8, 'D');
        $this->setElementsDatesOnOneElement(0.1, 'D');
        $this->setElementsDatesOnOneElement(0.4, 'D');
        
        $this->setElementsDatesOnOneElement(1, 'H');
        $this->setElementsDatesOnOneElement(4, 'H');
        $this->setElementsDatesOnOneElement(9, 'H');
        $this->setElementsDatesOnOneElement(7, 'H');
    }
    
    
    
    private function setElementDatesWithTwoElements(absences_entryElemProvider $elem1, absences_entryElemProvider $elem2)
    {
        $entry = $this->getMockEntryWithTwoElements(
            $elem1->quantity,
            $elem1->quantity_unit,
            $elem2->quantity,
            $elem2->quantity_unit
        );
        $entry->setElementsDates();
        
        $this->assertEquals($elem1->test_date_begin, $entry->getElement(1)->date_begin, 'element 1 begin date (id_right=1)');
        $this->assertEquals($elem1->test_date_end, $entry->getElement(1)->date_end, 'element 1 end date (id_right=1)');
        
        $this->assertEquals($elem2->test_date_begin, $entry->getElement(2)->date_begin, 'element 2 begin date (id_right=2)');
        $this->assertEquals($elem2->test_date_end, $entry->getElement(2)->date_end, 'element 2 end date (id_right=2)');
    }
    
    

    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationFullDays()
    {
        $elem1 = new absences_entryElemProvider(2, 'D');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 18:00:00');
        
        $elem2 = new absences_entryElemProvider(1, 'D');
        $elem2->expectDates('2014-01-03 08:00:00', '2014-01-03 18:00:00');
        
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationFullDaysInHours()
    {
        $elem1 = new absences_entryElemProvider(16, 'H');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 18:00:00');
    
        $elem2 = new absences_entryElemProvider(8, 'H');
        $elem2->expectDates('2014-01-03 08:00:00', '2014-01-03 18:00:00');
    
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }

    
    public function testSetElementsDatesOnTwoElementsMatchedDurationWithHalfDays()
    {
        $elem1 = new absences_entryElemProvider(1.5, 'D');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 12:00:00');
        
        $elem2 = new absences_entryElemProvider(1.5, 'D');
        $elem2->expectDates('2014-01-02 14:00:00', '2014-01-03 18:00:00');
        
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationWithHalfDaysInHours()
    {
        $elem1 = new absences_entryElemProvider(12, 'H');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 12:00:00');
    
        $elem2 = new absences_entryElemProvider(12, 'H');
        $elem2->expectDates('2014-01-02 14:00:00', '2014-01-03 18:00:00');
    
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationNoHalfDays()
    {
        $elem1 = new absences_entryElemProvider(1.25, 'D');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 10:00:00');
        
        $elem2 = new absences_entryElemProvider(1.75, 'D');
        $elem2->expectDates('2014-01-02 10:00:00', '2014-01-03 18:00:00');
        
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }

    
    
    /**
     * 
     * am: 08:00:00 - 12:00:00    4h
     * pm: 14:00:00 - 18:00:00    4h
     * 
     */
    public function testSetElementsDatesOnTwoElementsMatchedDurationNoHalfDaysInHours()
    {
        $elem1 = new absences_entryElemProvider(10, 'H');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 10:00:00');
    
        $elem2 = new absences_entryElemProvider(14, 'H');
        $elem2->expectDates('2014-01-02 10:00:00', '2014-01-03 18:00:00');
    
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationNoHalfDays2()
    {
        $entry = $this->getMockEntryWithTwoElements(0.25, 'D', 2.75, 'D');
        $entry->setElementsDates();
    
        $this->assertEquals('2014-01-01 08:00:00', $entry->getElement(1)->date_begin);
        $this->assertEquals('2014-01-01 10:00:00', $entry->getElement(1)->date_end);
    
        $this->assertEquals('2014-01-01 10:00:00', $entry->getElement(2)->date_begin);
        $this->assertEquals('2014-01-03 18:00:00', $entry->getElement(2)->date_end);
    }
    
    
    public function testSetElementsDatesOnTwoElementsMatchedDurationNoHalfDays3()
    {
        $entry = $this->getMockEntryWithTwoElements(1.2, 'D', 1.8, 'D');
        $entry->setElementsDates();
    
        $this->assertEquals('2014-01-01 08:00:00', $entry->getElement(1)->date_begin);
        $this->assertEquals('2014-01-02 09:36:00', $entry->getElement(1)->date_end);
    
        $this->assertEquals('2014-01-02 09:36:00', $entry->getElement(2)->date_begin);
        $this->assertEquals('2014-01-03 18:00:00', $entry->getElement(2)->date_end);
    }
    
    
    
    public function testSetElementsDatesOnTwoElementsCombinedTypes()
    {
        $elem1 = new absences_entryElemProvider(10, 'H');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 10:00:00');
    
        $elem2 = new absences_entryElemProvider(1.75, 'D');
        $elem2->expectDates('2014-01-02 10:00:00', '2014-01-03 18:00:00');
    
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
    
    public function testSetElementsDatesOnTwoElementsUnmatchedDuration()
    {
        $elem1 = new absences_entryElemProvider(1.25, 'D');
        $elem1->expectDates('2014-01-01 08:00:00', '2014-01-02 10:00:00');
    
        $elem2 = new absences_entryElemProvider(1.5, 'D');
        $elem2->expectDates('2014-01-02 10:00:00', '2014-01-03 16:00:00');
    
        $this->setElementDatesWithTwoElements($elem1, $elem2);
    }
    
}
