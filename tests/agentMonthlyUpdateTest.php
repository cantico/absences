<?php
require_once dirname(__FILE__).'/monthlyUpdate.php';



class absences_agentMonthlyUpdateTest extends absences_monthlyUpdate
{
    protected function assertAgentRight($agentRight)
    {
        
        $row = $agentRight->getRow();
        $this->assertInternalType('array', $row);
    }
    
    
    /**
     * This agent right exists in database
     * @return absences_AgentRight
     */
    protected function getMockAgentRightQteNotSet()
    {
        $agentRight = absences_AgentRight::getById(2);
        $this->assertAgentRight($agentRight);
        return $agentRight;
    }
    
    /**
     * This agent right exists in database with a modified quantity
     * @return absences_AgentRight
     */
    protected function getMockAgentRightQteSet()
    {
        $agentRight = absences_AgentRight::getById(4);
        $this->assertAgentRight($agentRight);
        return $agentRight;
    }
    
    /**
     * Effectuer la mise a jour mensuelle sur le droit, cela provoque la mise a jour sur agentRight
     * attention la valeur de retour est celle du droit
     * on a pas acces a la valeur de retour de agentRight, donc on ne peut pas la tester
     * 
     * @param absences_AgentRight $agentRight
     * @return bool
     */
    protected function monthlyQuantityUpdate($agentRight)
    {
        $right = $agentRight->getRight();
        
        return $right->monthlyQuantityUpdate();
    }
    
    public function testIncrementTheCurrentMonth_QteNotSet()
    {
        $this->resetRightQuantity();
        $agentRight = $this->getMockAgentRightQteNotSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
        $this->assertEquals(false, $resultRight, 'On the right, quantity should not be modified the first month');
    }
    

    
    
    public function testIncrementParametersOnFirstMonth_QteNotSet()
    {
        $agentRight = $this->getMockAgentRightQteNotSet();
        $right = $agentRight->getRight();
        
        $this->assertEquals('', $agentRight->quantity, 'On agentRight quantity is never modified if not set');
        $this->assertNotEquals('0000-00-00 00:00:00', $right->quantity_inc_last);
    }
    
    
    
    public function testIncrementOnNextMonth_QteNotSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteNotSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteNotSet(); // reload from DB
        $this->assertEquals('', $agentRight->quantity, 'On agentRight quantity is never modified');
        $this->assertEquals(0.25, $agentRight->getQuantity(), 'get the dynamic quantity from the right with a modification 0 + 0.25');
    }
    
    
    public function testIncrementOnNextMonth2_QteNotSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteNotSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteNotSet(); // reload from DB
        $this->assertEquals('', $agentRight->quantity, 'On agentRight quantity is never modified');
        $this->assertEquals(0.5, $agentRight->getQuantity(), 'get the dynamic quantity from the right with a modification 0 + 0.25 + 0.25');
    }
    
    
    public function testIncrementOnNextMonth3_QteNotSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteNotSet();
        $right = $agentRight->getRight();
        $right->quantity_inc_max = 0.6;
        $resultRight = $right->monthlyQuantityUpdate();
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteNotSet(); // reload from DB
        $this->assertEquals('', $agentRight->quantity, 'On agentRight quantity is never modified');
        $this->assertEquals(0.6, $agentRight->getQuantity(), 'get the dynamic quantity from the right with a modification 0 + 0.25 + 0.25 + 0.10');
    }
    
    
    public function testIncrementOnNextMonth4_QteNotSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteNotSet();
        $right = $agentRight->getRight();
        $right->quantity_inc_max = 0.6;
        $resultRight = $right->monthlyQuantityUpdate();
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteNotSet(); // reload from DB
        $this->assertEquals('', $agentRight->quantity, 'On agentRight quantity is never modified');
        $this->assertEquals(0.6, $agentRight->getQuantity(), 'get the dynamic quantity from the right with a modification 0 + 0.25 + 0.25 + 0.10');
    }
    
    
    
    // tests if value set
    
    
    public function testIncrementTheCurrentMonth_QteSet()
    {
        $this->resetRightQuantity();
        $agentRight = $this->getMockAgentRightQteSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
        $this->assertEquals(false, $resultRight, 'On the right, quantity should not be modified the first month');
    }
    
    
    public function testIncrementParametersOnFirstMonth_QteSet()
    {
        $agentRight = $this->getMockAgentRightQteSet();
        $right = $agentRight->getRight();
    
        $this->assertEquals('2.33', $agentRight->quantity, 'On agentRight, quantity is never modified');
        $this->assertEquals(2.33, $agentRight->getQuantity(), 'On agentRight, getQuantity() is not modified the first month');
        $this->assertNotEquals('0000-00-00 00:00:00', $right->quantity_inc_last);
    }
    
    
    public function testIncrementOnNextMonth_QteSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteSet(); // reload from DB
        $this->assertEquals('2.33', $agentRight->quantity, 'On agentRight, quantity is never modified');
        $this->assertEquals(2.58, $agentRight->getQuantity(), 'On agentRight, getQuantity() is modified the next month 2.33 + 0.25');
    }
    
    
    public function testIncrementOnNextMonth2_QteSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteSet();
        $resultRight = $this->monthlyQuantityUpdate($agentRight);
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteSet(); // reload from DB
        $this->assertEquals('2.33', $agentRight->quantity, 'On agentRight, quantity is never modified');
        $this->assertEquals(2.83, $agentRight->getQuantity(), 'On agentRight, getQuantity() is modified the next month 2.33 + 0.25 + 0.25');
    }
    
    
    public function testIncrementOnNextMonth3_QteSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteSet();
        $right = $agentRight->getRight();
        $this->assertEquals(0, $right->quantity_inc_max);
        
        $right->quantity_inc_max = 3;
        $resultRight = $right->monthlyQuantityUpdate();
        
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteSet(); // reload from DB
        $this->assertEquals('2.33', $agentRight->quantity, 'On agentRight, quantity is never modified');
        $this->assertEquals(3, $agentRight->getQuantity(), 'On agentRight, getQuantity() is modified the next month 2.33 + 0.25 + 0.25 + 0.17');
    }
    
    public function testIncrementOnNextMonth4_QteSet()
    {
        $this->goBackOneMonth();
    
        $agentRight = $this->getMockAgentRightQteSet();
        $right = $agentRight->getRight();
        $right->quantity_inc_max = 3;
        $resultRight = $right->monthlyQuantityUpdate();
    
        $this->assertEquals(true, $resultRight, 'test sur la mise a jour du droit');
        $agentRight = $this->getMockAgentRightQteSet(); // reload from DB
        $this->assertEquals('2.33', $agentRight->quantity, 'On agentRight, quantity is never modified');
        $this->assertEquals(3, $agentRight->getQuantity(), 'On agentRight, getQuantity() is not modified the next month');
    }


}
