<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/../programs/utilit/agent.class.php';


class absences_agentTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * This agent does not exists in database
     */
    protected function getMockAgent()
    {
        $agent = new absences_Agent();
        $agent->setRow(array(
            'id' => 1,
            'id_user' => 1
        ));
        
        return $agent;
    }
    
    
    
    public function testIdUser()
    {
        $agent = $this->getMockAgent();
        
        $this->assertEquals(1, $agent->getIdUser());
    }
    
    
    public function testInPersonel()
    {
        $agent = $this->getMockAgent();
        $this->assertTrue($agent->isInPersonnel());
    }
    
    
    public function testNotInPersonel()
    {
        $agent = new absences_Agent();
        $agent->setRow(array(
            'id' => 99,
            'id_user' => 99
        ));
        $this->assertFalse($agent->isInPersonnel());
    }
}
