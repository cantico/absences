<?php

require_once dirname(__FILE__).'/mock/functions.php';
require_once dirname(__FILE__).'/mock/database.php';
require_once dirname(__FILE__).'/../programs/utilit/exportentry.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

class absences_exportentryTest extends PHPUnit_Framework_TestCase
{
    public function getExport()
    {
        $dateb = BAB_DateTime::fromIsoDateTime('2014-10-01');
        $datee = BAB_DateTime::fromIsoDateTime('2014-10-02');
        
        return new absences_ExportEntry($dateb, $datee, array(0,1,2), 1, '', ',', 1, array(), '', 0);
    }
    
    
    public function testNbColls()
    {
        $export = $this->getExport();
        $header = $export->getHeader();
        
        // fake entry
        
        $entry = new absences_Entry();
        $entry->id = 1;
        $entry->id_user = 1;
        $entry->createdOn = '2014-10-01 00:00:00';
        $entry->date = '2014-10-01 00:00:00';
        $entry->status = 'Y';
        $entry->date_begin = '2014-10-01 00:00:00';
        $entry->date_end = '2014-10-01 22:00:00';
        
        $element = new absences_EntryElem();
        $element->id = 1;
        $element->quantity = 1;
        $element->id_right = 1;
        $element->date_begin = '2014-10-01 00:00:00';
        $element->date_end = '2014-10-01 22:00:00';
        
        $entry->addElement($element);
        
        $entryRow = $export->getEntryRow($entry);
        
        // fake agent
        
        $agent = new absences_Agent();
        $agent->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'idcoll' => 1
        ));
        
        $agentRow = $export->getAgentRow($agent);
        
        $this->assertEquals(count($header), count($entryRow), 'Number of collumns in entry row does not match number of header collumns');
        $this->assertEquals(count($header), count($agentRow), 'Number of collumns in agent row does not match number of header collumns');
    }
}