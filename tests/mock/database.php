<?php 

require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';

$GLOBALS['babDBHost'] = 'localhost';
$GLOBALS['babDBLogin'] = 'test';
$GLOBALS['babDBPasswd'] = '';
$GLOBALS['babDBName'] = 'test';

$babDB = new babDatabase();

//exec('mysql -u test -Nse "show tables" test | while read table; do mysql -u test -e "drop table $table" test; done');

exec('mysql -u test test < vendor/ovidentia/ovidentia/install/babinstall.sql 2>/dev/null');
//bab_addUser('Test', 'Test', '', 'test@absences.mod', 'absences', 'secret', 'secret', 1, $error);
exec('mysql -u test test < programs/sql/dump.sql');
exec('mysql -u test test < programs/sql/types.sql');
exec('mysql -u test test < programs/sql/rights.sql');
exec('mysql -u test test < programs/sql/agents.sql');