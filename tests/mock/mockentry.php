<?php

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/database.php';
require_once dirname(__FILE__).'/../../programs/utilit/right.class.php';
require_once dirname(__FILE__).'/../../programs/utilit/entry.class.php';
require_once dirname(__FILE__).'/../../programs/utilit/entry_elem.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.calendarperiod.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/cal.periodcollection.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

class absences_entryMocks extends PHPUnit_Framework_TestCase
{
    protected $workingPeriodCollection;
    
    /**
     * Element
     */
    protected function getMockElement($id_right, $quantity, $unit = 'D')
    {
        $right = new absences_Right($id_right);
        $right->setRow(array(
            'id' => $id_right,
            'sortkey' => $id_right,
            'name' => 'test right',
            'id_type' => 1,
            'quantity_unit' => $unit,
            'quantity' => 25
        ));
        
        $element = new absences_EntryElem();
        $element->setRow(array(
        
            'id_entry' => 1,
            'quantity' => $quantity,
            'id_right' => $id_right
        ));
        
        $element->setRight($right);
        
        return $element;
    }
    
    
    
    protected function mockWorkingPeriod($begin, $end)
    {
        if (!isset($this->workingPeriodCollection)) {
            $this->workingPeriodCollection = new bab_WorkingPeriodCollection();
        }
        
        $period = new bab_CalendarPeriod();
        $period->setDates(
            BAB_DateTime::fromIsoDateTime($begin),
            BAB_DateTime::fromIsoDateTime($end)
        );
        
        $period->setProperty('SUMMARY', 'Mock working period');
        
        $period->setCollection($this->workingPeriodCollection);
        
        return array($period);
    }
    
    
    
    
    protected function getMockEntryWithOneElement($elementDuration, $unit)
    {
        $entry = new absences_Entry();
        $entry->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'date_begin' => '2014-02-01 08:00:00',
            'date_end' => '2014-02-01 18:00:00'
        ));
    
        // definition des heures travailles
    
        $entry->setWorkingPeriodIndex(array(
            '20140201am' => $this->mockWorkingPeriod('2014-01-01 08:00:00', '2014-01-01 12:00:00'),
            '20140201pm' => $this->mockWorkingPeriod('2014-01-01 14:00:00', '2014-01-01 18:00:00')
        ));
    
        $entry->addElement($this->getMockElement(1, $elementDuration, $unit)->setEntry($entry));
    
        return $entry;
    }
    
    
    
    
    protected function getMockEntryWithTwoElements($quantity1, $unit1, $quantity2, $unit2)
    {
        $entry = new absences_Entry();
        $entry->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'date_begin' => '2014-01-01 08:00:00',
            'date_end' => '2014-01-03 18:00:00'
        ));
        
        // definition des heures travailles
        
        $entry->setWorkingPeriodIndex(array(
            
            '20140101am' => $this->mockWorkingPeriod('2014-01-01 08:00:00', '2014-01-01 12:00:00'),
            '20140101pm' => $this->mockWorkingPeriod('2014-01-01 14:00:00', '2014-01-01 18:00:00'),
            '20140102am' => $this->mockWorkingPeriod('2014-01-02 08:00:00', '2014-01-02 12:00:00'),
            '20140102pm' => $this->mockWorkingPeriod('2014-01-02 14:00:00', '2014-01-02 18:00:00'),
            '20140103am' => $this->mockWorkingPeriod('2014-01-03 08:00:00', '2014-01-03 12:00:00'),
            '20140103pm' => $this->mockWorkingPeriod('2014-01-03 14:00:00', '2014-01-03 18:00:00')
        ));
        
        // element will be sorted by id_right
        $entry->addElement($this->getMockElement(1, $quantity1, $unit1)->setEntry($entry));
        $entry->addElement($this->getMockElement(2, $quantity2, $unit2)->setEntry($entry));
        
        $entry->setElementsDates();
        
        return $entry;
    }
    
    
    
    
    
    
    protected function getMockEntryStartingAt12($quantity1, $unit1, $quantity2, $unit2)
    {
        $entry = new absences_Entry();
        $entry->setRow(array(
                'id' => 1,
                'id_user' => 1,
                'date_begin' => '2015-06-05 12:00:00',
                'date_end' => '2015-06-08 23:59:00'
        ));
    
        // definition des heures travailles
        
        // AM : 09:03 - 12:45
        // PM : 13:30 - 17:12
    
        $entry->setWorkingPeriodIndex(array(
    
                '20150605pm' => array_merge($this->mockWorkingPeriod('2015-06-05 12:00:00', '2015-06-05 12:45:00'),
                                            $this->mockWorkingPeriod('2015-06-05 13:30:00', '2015-06-05 17:12:00')),
                '20150608am' => $this->mockWorkingPeriod('2015-06-08 09:03:00', '2015-06-08 12:00:00'),
                '20150608pm' => array_merge($this->mockWorkingPeriod('2015-06-08 12:00:00', '2015-06-08 12:45:00'),
                                            $this->mockWorkingPeriod('2015-06-08 13:30:00', '2015-06-08 17:12:00'))
        ));
    
        // element will be sorted by id_right
        $entry->addElement($this->getMockElement(1, $quantity1, $unit1)->setEntry($entry));
        $entry->addElement($this->getMockElement(2, $quantity2, $unit2)->setEntry($entry));
        
        $entry->setElementsDates();
    
        return $entry;
    }
    
    
    
    
    protected function getMockSimpleEntryOnStrangeWorkingPeriodSet($quantity1, $unit1)
    {
        $entry = new absences_Entry();
        $entry->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'date_begin' => '2015-06-22 00:00:00',
            'date_end' => '2015-06-23 00:00:00'
        ));
    
        // definition des heures travailles
    
        $entry->setWorkingPeriodIndex(array(
    
            '20150622am' => $this->mockWorkingPeriod('2015-06-22 09:03:00', '2015-06-22 12:45:00'),
            '20150622pm' => $this->mockWorkingPeriod('2015-06-22 13:30:00', '2015-06-22 17:12:00')
        ));
    
        $entry->addElement($this->getMockElement(1, $quantity1, $unit1)->setEntry($entry)); // 1 day
        $entry->setElementsDates();
    
        return $entry;
    }
    
    
    
    
    protected function getMockEntryOnStrangeWorkingPeriodSet($quantity1, $unit1, $quantity2, $unit2)
    {
        $entry = new absences_Entry();
        $entry->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'date_begin' => '2014-01-01 08:00:00',
            'date_end' => '2014-01-03 18:00:00'
        ));
    
        // definition des heures travailles
    
        $entry->setWorkingPeriodIndex(array(
    
            '20140101am' => $this->mockWorkingPeriod('2014-01-01 09:03:00', '2014-01-01 12:45:00'),
            '20140101pm' => $this->mockWorkingPeriod('2014-01-01 13:30:00', '2014-01-01 17:12:00'),
            '20140102am' => $this->mockWorkingPeriod('2014-01-02 09:03:00', '2014-01-02 12:45:00'),
            '20140102pm' => $this->mockWorkingPeriod('2014-01-02 13:30:00', '2014-01-02 17:12:00'),
            '20140103am' => $this->mockWorkingPeriod('2014-01-03 09:03:00', '2014-01-03 12:45:00'),
            '20140103pm' => $this->mockWorkingPeriod('2014-01-03 13:30:00', '2014-01-03 17:12:00')
        ));
    
        // element will be sorted by id_right
        $entry->addElement($this->getMockElement(1, $quantity1, $unit1)->setEntry($entry)); // 2 days
        $entry->addElement($this->getMockElement(2, $quantity2, $unit2)->setEntry($entry)); // 1 day
    
        
        $entry->setElementsDates();
        
        return $entry;
    }
    
    
    
    protected function getMockEntryWithMonthChange()
    {
        
        $entry = new absences_Entry();
        $entry->setRow(array(
            'id' => 1,
            'id_user' => 1,
            'date_begin' => '2016-07-27 08:00:00',
            'date_end' => '2016-08-05 18:00:00'
        ));
        
        // definition des heures travailles
        
        $entry->setWorkingPeriodIndex(array(
        
            '20160727am' => $this->mockWorkingPeriod('2016-07-27 08:00:00', '2016-07-27 11:00:00'),
            '20160727pm' => $this->mockWorkingPeriod('2016-07-27 13:00:00', '2016-07-27 18:00:00'),
            '20160728am' => $this->mockWorkingPeriod('2016-07-28 08:00:00', '2016-07-28 11:00:00'),
            '20160728pm' => $this->mockWorkingPeriod('2016-07-28 13:00:00', '2016-07-28 18:00:00'),
            '20160729am' => $this->mockWorkingPeriod('2016-07-29 08:00:00', '2016-07-29 11:00:00'),
            '20160729pm' => $this->mockWorkingPeriod('2016-07-29 13:00:00', '2016-07-29 18:00:00'),
            '20160801am' => $this->mockWorkingPeriod('2016-08-01 08:00:00', '2016-08-01 11:00:00'),
            '20160801pm' => $this->mockWorkingPeriod('2016-08-01 13:00:00', '2016-08-01 18:00:00'),
            '20160802am' => $this->mockWorkingPeriod('2016-08-02 08:00:00', '2016-08-02 11:00:00'),
            '20160802pm' => $this->mockWorkingPeriod('2016-08-02 13:00:00', '2016-08-02 18:00:00'),
            '20160803am' => $this->mockWorkingPeriod('2016-08-03 08:00:00', '2016-08-03 11:00:00'),
            '20160803pm' => $this->mockWorkingPeriod('2016-08-03 13:00:00', '2016-08-03 18:00:00'),
            '20160804am' => $this->mockWorkingPeriod('2016-08-04 08:00:00', '2016-08-04 11:00:00'),
            '20160804pm' => $this->mockWorkingPeriod('2016-08-04 13:00:00', '2016-08-04 18:00:00'),
            '20160805am' => $this->mockWorkingPeriod('2016-08-05 08:00:00', '2016-08-05 11:00:00'),
            '20160805pm' => $this->mockWorkingPeriod('2016-08-05 13:00:00', '2016-08-05 18:00:00')
        ));
        
        // element will be sorted by id_right
        $entry->addElement($this->getMockElement(1, 5.5, 'D')->setEntry($entry));
        $entry->addElement($this->getMockElement(2, 1, 'D')->setEntry($entry));
        $entry->addElement($this->getMockElement(3, 1.5, 'D')->setEntry($entry));
        
        $entry->setElementsDates();
        
        return $entry;
    }
    
}