<?php 


$babInstallPath = 'vendor/ovidentia/ovidentia/ovidentia/';
$GLOBALS['babInstallPath'] = $babInstallPath;

require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new bab_SessionMockStorage());

require_once dirname(__FILE__).'/../../programs/utilit/vacincl.php';


function absences_translate($str) {
    return $str;
}



class absences_entryElemProvider {
    
    public $test_date_begin;
    public $test_date_end;
    
    public $quantity;
    public $quantity_unit;
    
    public function __construct($quantity, $unit)
    {
        $this->quantity = $quantity;
        $this->quantity_unit = $unit;
    }
    
    public function expectDates($begin, $end)
    {
        $this->test_date_begin = $begin;
        $this->test_date_end = $end;
    }
}