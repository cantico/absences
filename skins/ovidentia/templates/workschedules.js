
/**
 * Reset values in a TR
 * @param {HtmlElement} tr
 */
function absences_wsReset(tr)
{
	var inputs = tr.getElementsByTagName('INPUT');
	var seloptions = tr.getElementsByTagName('SELECT')[0].options;
	
	for(var i=0; i<seloptions.length; i++) {
		seloptions[i].selected = false;
	}
	
	for(var i=0; i<inputs.length; i++) {
		
		inputs[i].setAttribute('value', '');
		
		if (inputs[i].hasAttribute("id")) {
			inputs[i].removeAttribute('id');
		}
		inputs[i].className = inputs[i].className.replace(' widget-init-done', '');
		inputs[i].className = inputs[i].className.replace(' hasDatepicker', '');
	}
	
	widget_datePickerInit(tr);
}


/**
 * Manage buttons on one row
 * @param {HtmlElement} tr
 * @param {int} i
 */
function absences_wsProcessRow(tr, i)
{
	var buttons = tr.getElementsByTagName('BUTTON');
	var plus = buttons[buttons.length -2];
	var minus = buttons[buttons.length -1];
	
	
	
	plus.onclick = function(evt) {
		var tr = this.parentNode.parentNode;
		var tbody = tr.parentNode;
		evt.preventDefault();
		
		var newtr = tr.cloneNode(true);
		
		
		tbody.appendChild(newtr);
		absences_wsReset(newtr);

		absences_wsRefresh();
	}
	
	if (0 === i) {
		minus.style.visibility = 'hidden';
		return;
	}
	
	minus.style.visibility = 'visible';
	
	minus.onclick = function(evt) {
		var tr = this.parentNode.parentNode;
		var tbody = tr.parentNode;
		evt.preventDefault();
		
		tbody.removeChild(tr);
		absences_wsRefresh();
	}
}



/**
 * Refresh envents on table after page load and after one row is added
 */
function absences_wsRefresh()
{
	var table = document.getElementById('workschedules_profiles');
	if (!table) {
		return;
	}
	
	var trs = table.getElementsByTagName('TBODY')[0].getElementsByTagName('TR');
	

	
	for(var i=0; i<trs.length; i++) {
		absences_wsProcessRow(trs[i], i);
	}
}



bab_initFunctions.push(absences_wsRefresh);