absences_Calendar = new Object();

absences_Calendar.initLength = 0;

//absences_Calendar.previousCell = null;

absences_Calendar.monthList = null;

absences_Calendar.searchPeriod = null;

absences_Calendar.loader = function() {
	return jQuery('#absences_search .loadindicator');
}


absences_Calendar.trIndex = {};

absences_Calendar.editMode = (typeof setPeriod == 'function');


absences_Calendar.getCell = function(id_user, date) {

	var d = date.split('-');
	
	var year = d[0];
	
	var day = parseInt(d[2], 10);

	if (absences_Calendar.trIndex[year+d[1]+id_user])
	{
		var tr = absences_Calendar.trIndex[year+d[1]+id_user];
	}
	else {
		var month = parseInt(d[1], 10);
		var tr = jQuery('table.calendar tr.id_user-'+id_user+'.month-'+month+'.year-'+year+' td');
		absences_Calendar.trIndex[year+d[1]+id_user] = tr;
	}
	
	var cell = jQuery(tr.get(day-1));
	
	return cell;
}


absences_Calendar.creatAmPm = function(cell)
{
	var _date = cell.data('date');
	
	if (null == _date)
	{
		throw 'missing date in cell';
	}
	
	var td = cell.get(0);
	
	var am = document.createElement('DIV');
	am.setAttribute('id', 'd.'+_date+'.0');
	td.appendChild(am);
	
	var pm = document.createElement('DIV');
	pm.setAttribute('id', 'd.'+_date+'.1');
	td.appendChild(pm);

	cell.data('am', am);
	cell.data('pm', pm);
	
	return [am, pm];
}


/**
 * create and return the ampm div
 * @param cell
 * @param entry
 */
absences_Calendar.ampm = function(cell, ampm)
{
	var date = cell.data('date');
	
	if (null == date)
	{
		return jQuery();
	}
	
	switch(ampm)
	{
		case 0: return cell.data('am'); break;
		case 1: return cell.data('pm'); break;
	}
}




absences_Calendar.processCell = function(cell, entry, ampm)
{
	
	var table = cell.closest('table');
	var display_types = table.hasClass('display-types');
	
	switch(entry.period_type)
	{
		case 1: // BAB_PERIOD_WORKING
			
			var halfday = this.ampm(cell, ampm);

			if (!absences_Calendar.inSearchInterval(cell)) {
				halfday.setAttribute('class', 'noday');
				break;
			}
			
			if (this.editMode)
			{
				// vacation request creation
				
				jQuery(halfday).click(function() {
					setPeriod(this);
				});
				
				halfday.setAttribute('class', 'free');
				
			} else {
				
				halfday.setAttribute('class', 'default');
			}
			break;
			
		case 4: // BAB_PERIOD_NWDAY
			
			cell.addClass('nonworking');
			var title = cell.attr('title');
			
			if (title) {
				title += ', ';
			}
			
			if (undefined === title || -1 === title.indexOf(entry.title)) {
				cell.attr('title', title+entry.title);
			}
			cell.css('background-color', '#'+entry.nwd_color);
			break;
			
		case 8: // BAB_PERIOD_CALEVENT
			break;
			
		case 16: // BAB_PERIOD_TSKMGR
			break;
			
		case 32: // BAB_PERIOD_VACATION
			
			var halfday = this.ampm(cell, ampm);
			
			
			if (!absences_Calendar.inSearchInterval(cell)) {
				halfday.setAttribute('class', 'noday');
				break;
			}
			
			if (this.editMode)
			{
				// vacation request creation

				jQuery(halfday).click(function() {
					setPeriod(this);
				});
				
			}
			

			if (entry.id_entry == jQuery('form[name=vacform] input[name=id]').val())
			{
				
				// current selected period
				halfday.setAttribute('class', 'period');
			} else {
				
				switch(entry.status)
				{
				case 'P':
					halfday.setAttribute('class', 'previsional');
					break;
				case '':
					halfday.setAttribute('class', 'wait');
					break;
				default:
				case 'Y':
					halfday.setAttribute('class', 'used');
					break;
				}
				
				if (display_types) {
					halfday.style.backgroundColor = '#'+entry.color;
				}
			}
			
			
			break;
		
		case 2: // BAB_PERIOD_NONWORKING
		default:
			this.ampm(cell, ampm).setAttribute('class', 'weekend');

			break;
	}
}



absences_Calendar.getMeta = function(tr) {
	var classes = tr.attr('class').split(' ');
	
	if (!classes) {
		return;
	}
	
	var meta = new Object();
	jQuery(classes).each(function() { 
		var v = this.split('-');
        if (v.length === 2) {
        	meta[v[0]] = parseInt(v[1], 10);
        }    
    });
	
	return meta;
}




absences_Calendar.setCellDate = function(cell, d, m , y)
{
	m = m.toString();
	if (m.length == 1)
	{
		m = '0'+m;
	}
	
	d = d.toString();
	if (d.length == 1)
	{
		d = '0'+d;
	}

	cell.data('date', y+'-'+m+'-'+d);
	
	var title = cell.attr('title');
	if (!title) {
		var date = new Date(
			parseInt(y, 10),
			parseInt(m, 10),
			parseInt(d, 10)
		);
		var options = { weekday: 'long', day: 'numeric' };
		
		try {
			cell.attr('title', date.toLocaleDateString(undefined, options));
		} catch (e) {
			// if options not suported
		}
	}
}



absences_Calendar.getSearchPeriod = function()
{
	if (null !== absences_Calendar.searchPeriod) {
		return absences_Calendar.searchPeriod;
	}
	
	var r = {
		dateb: null,
		datee: null
	}

	var input = jQuery('#absences_search .absences-search-by-date');
	if (input.length !== 0) {
		var period = window.babAddonWidgets.getMetadata(input.prop('id'));
		r.dateb = period.dateb;
		r.datee = period.datee;
	}
	
	absences_Calendar.searchPeriod = r;
	
	return r;
}



absences_Calendar.inSearchInterval = function(cell) {
	var p = absences_Calendar.getSearchPeriod();
	var d = cell.data('date');
	
	if (undefined === p || '0000-00-00' === p.dateb || null === p.dateb || undefined === p.dateb) {
		return true;
	}
	
	return (p.dateb<=d && d<=p.datee);
}




absences_Calendar.processCells = function(tr)
{

	
	var meta = absences_Calendar.getMeta(tr);
	
	max = new Date(meta.year, meta.month, 0).getDate();
	var i = 1;
	var currententry = null;
	
	tr.find('td').each(function() {
		var cell = jQuery(this);
		
		if (i > max) {
			cell.addClass('noday');
			i++;
			return;
		}
		
		absences_Calendar.setCellDate(cell, i, meta.month, meta.year);
		
		
		arr = absences_Calendar.creatAmPm(cell);
		
		_entry = cell.data('entry');
		
		jQuery.each(arr, function(k, div) {
			
			if (null != _entry && null != _entry[k])
			{
				currententry = _entry[k];
			}
			
			if (null !== currententry)
			{
				absences_Calendar.processCell(cell, currententry, k);
			}
			
		});
		

		i++;
	});
}




absences_Calendar.processNextData = function(data, i, nextAction)
{
	var month_days = data[i];
	
	if (undefined !== month_days) {
		for(var key in month_days)
		{
			var entry = month_days[key];
			
			
		
			var cell = absences_Calendar.getCell(entry['id_user'], entry['cal_date']);
			
			if (cell.length == 1)
			{
				_entry = cell.data('entry');
				if (null == _entry)
				{
					_entry = [];
				}
				
				// ampm is not defined, entry will be set in am
				_entry[entry.ampm] = entry;
				
				cell.data('entry', _entry);
			}
		}
	}
	
	if (i < data.length)
	{
		i++;

		setTimeout(function() {
			
			absences_Calendar.processNextData(data, i, nextAction);
			
		}, 1);
		
	} else {
		
		nextAction();
	}
	
	
}


absences_Calendar.processNextTr = function(tr_list, i, nextAction)
{
	var tr = jQuery(tr_list[i]);
	
	if (0 == tr.length)
	{
		console && console.log('TR not found for '+i);
		nextAction();
		return;
	}
	
	absences_Calendar.processCells(tr);
	
	
	if (i < tr_list.length-1)
	{
		i++;

		setTimeout(function() {
			tr.removeClass('loading');
			absences_Calendar.processNextTr(tr_list, i, nextAction);
			
		}, 1);
		
		return;
	}
	
	
	tr.removeClass('loading');
	nextAction();
}




/**
 * load data for a list of users allready in HTML
 * @param users
 */
absences_Calendar.loadUsers = function(users, success) {

	var qs = '';
	jQuery.each(users, function(k, v) {
		qs += '&users[]='+v;
	});
	
	var form = jQuery('#absences_search');
	if (form.length !== 0) {
		
		var dateb = form.find('[name=dateb]').val();
		var datee = form.find('[name=datee]').val();
		var date = form.find('[name=date]').val();
		
		if (date !== '') {
			dateb = date;
			datee = date;
		}
		
		if ('' !== dateb && '' !== datee) {
			qs += '&dateb='+dateb+'&datee='+datee;
		}
	}
	
	var firstMonthQuery = null;
	
	jQuery.each(absences_Calendar.monthList, function(k, v) {
		
		if (null === firstMonthQuery) {
			firstMonthQuery = v;
		}
		
		jQuery.ajax({
			cache: false,
			url: '?addon=absences.planning&idx=load'+qs+'&year='+v.year+'&month='+v.month,
			dataType: "json",
			success:  function(data) {
				success(data, v, users);
			}
		});
	});
}



absences_Calendar.loadUsersLimit = function(users) {
	
	var limit = 300;
	var i = 0;
	var total = users.length;
	
	/**
	 * contain max 300 queries, differents users, same month
	 */
	var userQueries = [];
	
	var data = [];
	
	function success(chunkData, v, userGroup) {
		
		
		
		data = data.concat(chunkData);
		
		userQueries.splice(userQueries.indexOf(userGroup), 1);

		
		if (userQueries.length === 0) {
			absences_Calendar.processNextData(data, 0, function() {
				
				var tr_list = jQuery('.month-'+v.month+'.year-'+v.year+'.loading');

				absences_Calendar.processNextTr(tr_list, 0, function() {
					absences_Calendar.loader().hide();	
				});				
			});
		}
	}
	
	
	while (i<total) {
		var userGroup = users.splice(0, limit);
		absences_Calendar.loadUsers(userGroup, success);
		i += limit;
		userQueries.push(userGroup);
	}
	
	if (users.length > 0) {
		absences_Calendar.loadUsers(users, success);
		userQueries.push(userGroup);
	}
}



absences_Calendar.addUser = function(id_user, username)
{
	// add user for each month
	
	jQuery('#vac_calendar_body tr:last').each(function() {
		var lasttr = jQuery(this);
		var meta = absences_Calendar.getMeta(lasttr);
		
		var newtr = jQuery('<tr class="inmonth id_user-'+id_user+' month-'+meta.month+' year-'+meta.year+' loading"></tr>');
		if (lasttr.find('th.user').length > 0)
		{
			var th = jQuery('<th class="user"></th>');
			newtr.append(th);
			
			if (lasttr.find('th.user a').length > 0)
			{
				th.append(jQuery('<a href="?tg=addon/absences/vacadm&amp;idx=modp&amp;idp='+id_user+'"></a>').text(username));
			} else {
				th.text(username);
			}
		}
		
		for(var i=0; i<31; i++)
		{
			newtr.append('<td></td>');
		}
		
		newtr.insertAfter(lasttr);
		
		var monthcell = lasttr.parent().find('tr:first th[rowspan]');
		var rowspan = parseInt(monthcell.attr('rowspan'), 10);
		monthcell.attr('rowspan', 1 + rowspan);
	});
}




/**
 * Query server to search for a list of users
 */
absences_Calendar.searchUsers = function(keyword, pos, limit) {
	
	// load from pos
	jQuery.getJSON('?addon=absences.planning&idx=users&keyword='+encodeURIComponent(keyword)+'&pos='+pos+'&limit='+limit, function(data) {
		

		var total = data.count;
		var users = data.users;
		
		var load = [];
		
		for(var i=0; i<users.length; i++)
		{
			id_user = users[i].id;
			
			// add tr
			
			if (-1 !== load.indexOf(id_user)) {
			
				absences_Calendar.addUser(id_user, users[i].name);
				load.push(id_user);
			}
		}
		
		absences_Calendar.loadUsersLimit(load);
		
		// hide button ?
		
		
		loaded = jQuery('#vac_calendar_body tr').length;
		var loadAll = jQuery('#absences_loadall');
		
		if (loaded >= total)
		{
			jQuery('#absences_loadmore').remove();
			if (loadAll.length) {
				jQuery('#absences_loadall span').text(jQuery('#absences_loadall').attr('rel'));
			}
			
		} else if ((total - loaded) < limit) {
			
			var label = jQuery('#absences_loadmore span').text();
			label = label.replace('30', (total - loaded));
			
			jQuery('#absences_loadmore span').text(label);
		}
	});
}




/**
 * load data for the users allready in HTML
 */
absences_Calendar.load = function() {
	
	absences_Calendar.monthList = new Object();
	var users = [];
	
	jQuery('#vac_calendar_body tr').each(function() {
		var tr = jQuery(this);
		var meta = absences_Calendar.getMeta(tr);
		
		absences_Calendar.monthList[meta.year+''+meta.month] = { year: meta.year, month: meta.month };
		
		if (-1 === users.indexOf(meta.id_user)) {
			users.push(meta.id_user);
		}
		
	});

	absences_Calendar.loadUsersLimit(users);
	
}


absences_Calendar.prepareLoadmore = function() {
	
	
	absences_Calendar.initLength = jQuery('#vac_calendar_body tr').length;
	
	var button = jQuery('#absences_loadmore');
	if (button.length > 0)
	{
		button.click(function() {
			
			absences_Calendar.loader().show();
			
			var limit = parseInt(jQuery('#absences_search input[name=limit]').val(), 10);
			var keyword = jQuery('#absences_search input[name=keyword]').val();
			var pos = jQuery('#vac_calendar_body tr').length;
			
			absences_Calendar.searchUsers(keyword, pos, limit);
		});
	}
	
	var loadall = jQuery('#absences_loadall');
	if (loadall.length > 0)
	{
		loadall.firsttext = loadall.find('span').text();
		
		function getLength() {
			var searchTotal = jQuery('#absences_searchTotal');
			if (0 === searchTotal.length) {
				// all loaded, get back to 30
				return 30;
			}
			
			var total = parseInt(searchTotal.text().match(/\d+/)[0], 10);
			var l = jQuery('#vac_calendar_body tr').length;
			if (total <= l) {
				// all loaded, label allready modified in button
				return 0;
			}
			
			return 99999;
		}
		
		
		loadall.click(function() {
			

			absences_Calendar.loader().show();
			
			var form = jQuery('#absences_search');
			
			/*
			var keyword = jQuery('#absences_search input[name=keyword]').val();
			var trs = jQuery('#vac_calendar_body tr');
			var pos = trs.length;
			*/
			
			var length = getLength();
			
			if (length === 0) {
				
				// repost search
				form.submit();
				
				return;
			}
			
			//absences_Calendar.searchUsers(keyword, pos, length);
			
			form.find('[name=limit]').val(length);
			form.submit();
			
		});
	}
}





absences_Calendar.init = function() {
	
	jQuery('#vacationcalendar').not('.absences-init-done').each(function() {
	
		jQuery(this).addClass('absences-init-done');
		
		absences_Calendar.loader().show();
		absences_Calendar.load();
		absences_Calendar.prepareLoadmore();
	
	});
	
	
	if (jQuery('#vacationcalendar').length == 0)
	{
		absences_Calendar.loader().hide();
	}
}

if (null != window.bab && null != window.bab.addInitFunction)
{
	// init with widget, required for portlet
	window.bab.addInitFunction(absences_Calendar.init);
} else {
	// init from jquery if widget not included
	jQuery('#vacationcalendar').ready(absences_Calendar.init);
	
}	




