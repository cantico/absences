function absences_rightEditorInit(domNode)
{
	var vbox = jQuery(domNode).find('#absences_dupli_periods').not('.widget-init-done');
	
	if (vbox.length > 0) {
		vbox.addClass('widget-init-done');
		
		var addbutton = jQuery(domNode).find('#absences_dupli_add');
		var suffix = 0;
		
		addbutton.click(function() {
			var lastindex = parseInt(vbox.find('.widget-layout-vbox-item:last select').attr('name').match(/right\[inperiod\]\[(\d+)\]/)[1], 10);
			var item = vbox.find('.widget-layout-vbox-item:first').clone();
			
			item.removeAttr('id');
			item.find('[id]').removeAttr('id');
			
			item.find('input, select').each(function(k, field) { 
				var newname = jQuery(field).attr('name').replace(/^right\[inperiod\]\[\d+\]/, 'right[inperiod]['+(1 + lastindex)+']');
				jQuery(field).attr('name', newname);
			});
			item.find('button').show().click(function() {
				jQuery(this).closest('.widget-layout-vbox-item').remove();
			});
			item.find('input').val('');
			item.appendTo(vbox);
			
			item.find('.widget-datepicker').removeClass('hasDatepicker');
			window.bab.init(item.get(0));
			
		});
	
		
		vbox.find('.widget-layout-vbox-item:first button').hide();
		vbox.find('.widget-layout-vbox-item').not(':first').find('button').click(function() {
			jQuery(this).closest('.widget-layout-vbox-item').remove();
		});
	}
	
	
	
	
	
	
	
	
	var vbox = jQuery(domNode).find('#absences_dynconf_rows').not('.widget-init-done');
	
	if (vbox.length > 0) {
		vbox.addClass('widget-init-done');
		
		var addbutton = jQuery(domNode).find('#absences_dynconf_add');
		var suffix = 0;
		
		addbutton.click(function() {
			var lastindex = parseInt(vbox.find('.widget-layout-vbox-item:last select').attr('name').match(/right\[dynconf\]\[(\d+)\]/)[1], 10);
			var item = vbox.find('.widget-layout-vbox-item:first').clone();
			
			item.removeAttr('id');
			item.find('[id]').removeAttr('id');
			
			item.find('input, select').each(function(k, field) { 
				var newname = jQuery(field).attr('name').replace(/^right\[dynconf\]\[\d+\]/, 'right[dynconf]['+(1 + lastindex)+']');
				jQuery(field).attr('name', newname);
			});
			item.find('button').show().click(function() {
				jQuery(this).closest('.widget-layout-vbox-item').remove();
			});
			item.find('input').val('');
			item.appendTo(vbox);
			
			window.bab.init(item.get(0));
			
		});
	
		
		vbox.find('.widget-layout-vbox-item:first button').hide();
		vbox.find('.widget-layout-vbox-item').not(':first').find('button').click(function() {
			jQuery(this).closest('.widget-layout-vbox-item').remove();
		});
	}
}



window.bab.addInitFunction(absences_rightEditorInit);