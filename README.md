## Module de gestion des congés ##

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/absences/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/absences/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/absences/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/absences/?branch=master)


Module de gestion des absences d'une organisation (Congés payés, RTT, Pré-attribué, Congés exceptionnels, CDD, Temps partiel, Jours de fractionnement, Maladie, Compte épargne Temps, Ancienneté, Récupération et le Report ...).
Ce module propose une interface de demande d'absence avec affichage des soldes pour les utilisateurs et processus de validation, ainsi qu'une console d'administration permettant au gestionnaire de congés de superviser la fonction.

Documentation
-------------

Sur le wiki Ovidentia :

[Congés](http://wiki.ovidentia.org/index.php/Cong%C3%A9s)