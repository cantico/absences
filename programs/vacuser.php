<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



include_once dirname(__FILE__)."/functions.php";
include_once $GLOBALS['babInstallPath']."utilit/afincl.php";
include_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
include_once dirname(__FILE__)."/utilit/vacincl.php";
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';



/**
 * 
 */
function bab_isEntryEditable($id)
{
	if ($id == 0) {
		// request creation
		return true;
		}

	$agent = absences_Agent::getCurrentUser();
	
	require_once dirname(__FILE__).'/utilit/entry.class.php';
	$entry = absences_Entry::getById($id);
	
	
	if ($agent->isManager()) {
		// le gestionnaire peut tout modifier
		return (int) $entry->id_user;
	}
	
	
	if ($entry->canModify()) {
	    return (int) $entry->id_user;
	}
	
	return false;
}



/**
 * Test if the current logged in user can create a vacation request in the name of $id_user
 * @param unknown_type $id_user
 * 
 * @return bool
 */
function bab_vacRequestCreate($id_user) {
	global $babBody;
	
	
	$target_agent = absences_Agent::getFromIdUser($id_user);


	if (!$target_agent->isInPersonnel()) {
		$babBody->addError(sprintf(absences_translate("The user %s is not registered in the personnel members"), $target_agent->getName()));
		return false;
	}
		
	if (!$target_agent->haveRights()) {
		$babBody->addError(absences_translate("No accessibles vacations rights"));
		return false;
	}

	if ($id_user == bab_getUserId()) {
		return true;
		}
	else
		{
		$current_agent = absences_Agent::getCurrentUser();
		if($current_agent->isManager())
			{
			return true;
			}

		if (absences_getVacationOption('chart_superiors_create_request') && absences_IsUserUnderSuperior($id_user)) {
			return true;
			}
		}

	$babBody->addError(sprintf(absences_translate("Access denied for %s"), $target_agent->getName()));
	return false;
	}

	
function absences_recurringVacation($begin, $end, $id)
{
	global $babBody;
	class temp
	{
		/**
		 * 
		 * @var Func_CalendarBackend
		 */
		private $calendarBackend;
		
		/**
		 * 
		 * @var array
		 */
		private $rrule;
		
		
		public $begin;
		public $end;
		public $id;
		public $duration;
		public $id_user;
		public $rfrom;
		
		public $datebegintxt;
		public $dateendtxt;
		public $t_loaddates;
		public $t_daily;
		public $t_weekly;
		public $t_monthly;
		public $t_all_the;
		public $t_years;
		public $t_months;
		public $t_weeks;
		public $t_days;
		public $repeat_dateendtxt;
		public $t_sun;
		public $t_mon;
		public $t_tue;
		public $t_wen;
		public $t_thu;
		public $t_fri;
		public $t_sat;
		public $calendar;
		public $addvac;
		public $t_vacation_request;
			
		public $calurl;
		public $datebegin;
		public $dateend;
		public $username;
		public $nbdays;
		public $end_day;
		public $end_month;
		public $end_year;
		public $curyear;
		public $ymin;
		public $ymax;
		public $yearmin;
		public $repeat;
		public $repeat_n_1;
		public $repeat_n_2;
		public $repeat_n_3;
		public $repeat_wd_checked;
		
	
		public function __construct($begin, $end, $id)
		{
			global $babBody;
			
			$this->begin = $begin;
			$this->end = $end;
			$this->id = $id;
			$this->duration = bab_mktime($this->end) - bab_mktime($this->begin);
			
			$this->id_user = bab_pp('id_user');
			$this->rfrom = bab_pp('rfrom', 0);
			
			$this->datebegintxt = absences_translate("Begin date");
			$this->dateendtxt = absences_translate("End date");
			$this->t_loaddates = absences_translate("Load dates");
			$this->t_daily = absences_translate("Daily");
			$this->t_weekly = absences_translate("Weekly");
			$this->t_monthly = absences_translate("Monthly");
			$this->t_all_the = absences_translate("Every");
			$this->t_years = absences_translate("years");
			$this->t_months = absences_translate("months");
			$this->t_weeks = absences_translate("weeks");
			$this->t_days = absences_translate("days");
			$this->repeat_dateendtxt = absences_translate("Periodicity end date");
			$this->t_sun = mb_substr(absences_translate("Sunday"),0,3);
			$this->t_mon = mb_substr(absences_translate("Monday"),0,3);
			$this->t_tue = mb_substr(absences_translate("Tuesday"),0,3);
			$this->t_wen = mb_substr(absences_translate("Wednesday"),0,3);
			$this->t_thu = mb_substr(absences_translate("Thursday"),0,3);
			$this->t_fri = mb_substr(absences_translate("Friday"),0,3);
			$this->t_sat = mb_substr(absences_translate("Saturday"),0,3);
			$this->calendar = absences_translate("Planning");
			$this->addvac = absences_translate('Confirm the period selection');
			$this->t_vacation_request = absences_translate('Create a vacation request on the following periods:');
			
			$this->calurl = bab_toHtml(absences_addon()->getUrl()."planning&idx=cal&idu=".$this->id_user."&popup=1");
			
			$this->datebegin = absences_longDate(bab_mktime($begin));
			$end_timestamp = bab_mktime($end);
			$this->dateend = absences_longDate($end_timestamp);
			

			$this->username = bab_toHtml(bab_getUserName($this->id_user));
			
			$this->nbdays = 31;
			$this->end_day = bab_pp('repeat_dayend', date('d', $end_timestamp));
			$this->end_month = bab_pp('repeat_monthend', date('m', $end_timestamp));
			$this->end_year = bab_pp('repeat_yearend', date('Y', $end_timestamp));
			
			
			$this->curyear = date('Y');
			
			$this->ymin = 2;
			$this->ymax = 5;
			$this->yearmin = $this->curyear - $this->ymin;
			
			
			$this->repeat = (int) bab_pp('repeat', 1);
			$this->repeat_n_1 = bab_pp('repeat_n_1');
			$this->repeat_n_2 = bab_pp('repeat_n_2');
			$this->repeat_n_3 = bab_pp('repeat_n_3');
			
			$weekdays = array('SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA');
			$repeat_wd = (array) bab_pp('repeat_wd', array());
			$repeat_wd = array_flip($repeat_wd);
			$this->repeat_wd_checked = array();
			
			foreach($weekdays as $wd)
			{
				$this->repeat_wd_checked[$wd] = isset($repeat_wd[$wd]) ? 'checked="checked"' : '';
			}
			
			if (isset($_POST['loaddates']))
			{
				$backendName = bab_getICalendars($this->id_user)->calendar_backend;
				if (!$backendName)
				{
					$backendName = 'Ovi';
				}
				
				$this->calendarBackend = bab_functionality::get('CalendarBackend/'.$backendName);
				
				try {
					$this->rrule = $this->loaddates();
					$this->dates_loaded = true;
				} catch (ErrorException $e)
				{
					$babBody->addError($e->getMessage());
				}
				
			}
		}
		
		/**
		 * Get an array with posted rules
		 * @return array
		 */
		private function getPostedRecurringParameters()
		{
			$args = array();
			

			$args['until'] = array(
					'year'	=> (int) $_POST['repeat_yearend'],
					'month'	=> (int) $_POST['repeat_monthend'],
					'day'	=> (int) $_POST['repeat_dayend']
			);
		
			$_POST['repeat'] = isset($_POST['repeat']) ? $_POST['repeat'] : '';
			switch ($_POST['repeat']) {
		
				case ABSENCES_RECUR_WEEKLY:
					$args['rrule'] = ABSENCES_RECUR_WEEKLY;
					if( empty($_POST['repeat_n_2']))
					{
						$_POST['repeat_n_2'] = 1;
					}
		
					$args['nweeks'] = (int) $_POST['repeat_n_2'];
		
					if( isset($_POST['repeat_wd']) )
					{
						$args['rdays'] = $_POST['repeat_wd'];
					}
		
					break;
		
				case ABSENCES_RECUR_MONTHLY: 
					$args['rrule'] = ABSENCES_RECUR_MONTHLY;
					if( empty($_POST['repeat_n_3']))
					{
						$_POST['repeat_n_3'] = 1;
					}
		
					$args['nmonths'] = (int) $_POST['repeat_n_3'];
					break;
		
				case ABSENCES_RECUR_YEARLY:
					$args['rrule'] = ABSENCES_RECUR_YEARLY;
					if( empty($_POST['repeat_n_4']))
					{
						$_POST['repeat_n_4'] = 1;
					}
					$args['nyears'] = (int) $_POST['repeat_n_4'];
					break;
		
				case ABSENCES_RECUR_DAILY:
				default:
					$args['rrule'] = ABSENCES_RECUR_DAILY;
					if( empty($_POST['repeat_n_1']))
					{
						$_POST['repeat_n_1'] = 1;
					}
		
					$args['ndays'] = (int) $_POST['repeat_n_1'];
					break;
			}
			
			
			
			return $args;
		}
		
		
		/**
		 * Convert posted data to a standard RRULE icaldendar property
		 * @throws ErrorException
		 */
		private function getRRule()
		{
			$args = $this->getPostedRecurringParameters();
			
			if(!isset($args['rrule']) ) {
				return null;
			}

			$duration = $this->duration;
			$rrule = array();
		
			switch( $args['rrule'] )
			{
				case ABSENCES_RECUR_WEEKLY:
					if( $duration > 24 * 3600 * 7 * $args['nweeks']) {
						throw new ErrorException(absences_translate("The duration of the event must be shorter than how frequently it occurs"));
					}
		
					$rrule[]= 'INTERVAL='.$args['nweeks'];
		
					if( !isset($args['rdays']) ) {
						// no week day specified, reapeat event every week
						$rrule[]= 'FREQ=WEEKLY';
					}
					else {
						$rrule[]= 'FREQ=WEEKLY';
						// BYDAY : add list of weekday    = "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
						$rrule[] = 'BYDAY='.implode(',', $args['rdays']);
					}
		
					break;
		
		
				case ABSENCES_RECUR_MONTHLY:
					if( $duration > 24*3600*28*$args['nmonths']) {
						throw new ErrorException(absences_translate("The duration of the event must be shorter than how frequently it occurs"));
					}
		
					$rrule[]= 'INTERVAL='.$args['nmonths'];
					$rrule[]= 'FREQ=MONTHLY';
					break;
		
				case ABSENCES_RECUR_YEARLY: /* yearly */
		
					if( $duration > 24*3600*365*$args['nyears']) {
						throw new ErrorException(absences_translate("The duration of the event must be shorter than how frequently it occurs"));
					}
					$rrule[]= 'INTERVAL='.$args['nyears'];
					$rrule[]= 'FREQ=YEARLY';
					break;
		
				case ABSENCES_RECUR_DAILY: /* daily */
					if( $duration > 24*3600*$args['ndays'] ) {
						throw new ErrorException(absences_translate("The duration of the event must be shorter than how frequently it occurs"));
					}
					$rrule[]= 'INTERVAL='.$args['ndays'];
					$rrule[]= 'FREQ=DAILY';
					break;
			}
		
		
			
			if (isset($args['until'])) {
			
				$until = self::getDateTime($args['until']);
				$until->add(1, BAB_DATETIME_DAY);
			
				if( $until->getTimeStamp() < bab_mktime($this->end)) {
					throw new ErrorException(absences_translate("Repeat date must be older than end date"));
				}
			
			
				$rrule[] = 'UNTIL='.$until->getICal(true);
			}
			
			return implode(';',$rrule);
		}
		
		
		/**
		 * Get dateTime object from date as array with keys
		 * <ul>
		 *	<li>year</li>
		 *	<li>month<li>
		 *	<li>day</li>
		 *	<li>hours (optional)</li>
		 *	<li>minutes (optional)</li>
		 * <ul>
		 *
		 *
		 * @param	array	$arr
		 *
		 * @param	int		$default_ts default timestamp value to use if values of date are not set
		 *
		 * @return 	BAB_DateTime
		 */
		public static function getDateTime($arr, $default_ts = null) {
		
		
			if (!isset($default_ts) && (!isset($arr['year']) || !isset($arr['month']) || !isset($arr['day']))) {
				return null;
			}
		
			if (!isset($arr['year'])) {
				$arr['year'] = date('Y', $default_ts);
			}
		
			if (!isset($arr['month'])) {
				$arr['month'] = date('m', $default_ts);
			}
		
			if (!isset($arr['day'])) {
				$arr['day'] = date('d', $default_ts);
			}
		
			if (!isset($arr['hours'])) {
				$arr['hours'] = 0;
			}
		
			if (!isset($arr['minutes'])) {
				$arr['minutes'] = 0;
			}
		
			return new BAB_DateTime($arr['year'], $arr['month'], $arr['day'], $arr['hours'],$arr['minutes']);
		}
		
		
		/**
		 * @return RRule
		 */
		private function loaddates()
		{
			if (null === $rrule = $this->getRRule())
			{
				return null;
			}
			
			$arr = array();
			
			
			require_once dirname(__FILE__).'/utilit/RRule.php';
			
			$o = new RRule(new iCalDate(bab_mktime($this->begin)), $rrule);
			
			
			while($date = $o->GetNext())
			{
				$begin = BAB_DateTime::fromIsoDateTime($date->Render());
				$end = clone $begin;
				$end->add($this->duration, BAB_DATETIME_SECOND);
				
				// do not add the unavailable periods
				
				$hd_index = absences_getHalfDaysIndex($this->id_user, $begin, $end);
				
				if (count($hd_index[2]) > 0) // free half day on the requested period
				{
					$arr[$begin->getTimeStamp()] = array($begin, $end);
				}
			}
			
			ksort($arr);
			
			return $arr;
		}
		
		
		public function getnextday()
		{
			static $i = 1;
			if( $i <= $this->nbdays)
			{
				$this->dayid = $i;
				if($this->end_day == $i)
				{
					$this->selected = "selected";
				}
				else
					$this->selected = "";
		
				$i++;
				return true;
			}
			else
			{
				$i = 1;
				return false;
			}
		
		}
		
		public function getnextmonth()
		{
			static $i = 1;
		
			if( $i < 13)
			{
				$this->monthid = $i;
				$this->monthname = bab_toHtml(bab_DateStrings::getMonth($i));
				if($this->end_month == $i)
				{
					$this->selected = "selected";
				}
				else
					$this->selected = "";
		
				$i++;
				return true;
			}
			else
			{
				$i = 1;
				return false;
			}
		
		}
		
		public function getnextyear()
		{
			static $i = 0;
			if( $i < $this->ymin + $this->ymax + 1)
			{
				$this->yearidval = ($this->yearmin + $i);
				$this->yearid = $this->yearidval;
				if($this->end_year == $this->yearidval)
				{
					$this->selected = "selected";
				}
				else
					$this->selected = "";
				$i++;
				return true;
			}
			else
			{
				$i = 0;
				return false;
			}
		
		}
		
	
		/**
		 * list periods computed from posted recurring parameters
		 */
		public function getnextperiod()
		{
			if (isset($this->rrule) && list(,$arr) = each($this->rrule))
			{
				$begin = $arr[0];
				$end = $arr[1];
				
				$this->p_begin = $begin->getIsoDateTime();
				$this->p_end = $end->getIsoDateTime();
				
				$this->period = sprintf(absences_translate('from %s to %s'), $begin->shortFormat(), $end->shortFormat());
				$this->checked = (isset($_POST['loaddates']) || isset($_POST['period'][$this->p_begin]));
				return true;
			}
			return false;
		}
	
	}
	
	$temp = new temp($begin,$end, $id);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacuser.html", "recurring"));
}
	
	



class absences_RequestVacation
{

	public $recurring;
	
	
	/**
	 * @var array
	 */
	public $rights;
	
	/**
	 * @var array
	 */
	public $right;
	
	public $id;
	public $id_user;
	public $ide;
	public $username;
	
	public $datebegintxt;
	public $dateendtxt;
	public $vactype;
	public $addvac;
	public $save_previsional;
	public $remark;
	public $nbdaystxt;
	public $invaliddate;
	public $invaliddate2;
	public $invalidentry;
	public $invalidentry1;
	public $invalidentry2;
	public $invalidentry3;
	public $totaltxt;
	public $balancetxt;
	public $previsionalbalancetxt;
	public $calendar;
	public $total_days;
	public $total_hours;
	public $maxallowed;
	public $t_days;
	public $t_alert_nomatch;
	public $t_confirm_nomatch;
	public $t_or;
	public $t_recurring;
	public $t_recurring_help;
	public $t_previsional;
	public $t_no_approval;
	public $t_force_approval;
	public $allow_mismatch;


	public function __construct($begin, $end, $id)
	{
		global $babBody, $babDB;
		require_once dirname(__FILE__).'/utilit/entry.class.php';

		$begin = $this->formatInputDate($begin);
		$end = $this->formatInputDate($end);


		if ($end < date('Y-m-d H:i:s') && !$id)
		{
			$babBody->addMessage(absences_translate('Warning, the request is in the past, do you really want to create a request at this date?'));
		}

		$this->recurring = (!empty($_POST['period']) && is_array($_POST['period']));
		$nb_request = $this->recurring ? count($_POST['period']) : 1;

		$this->datebegintxt = absences_translate("Begin date");
		$this->dateendtxt = absences_translate("End date");
		$this->vactype = absences_translate("Vacation type");
		if ($id)
		{
			$this->addvac = absences_translate("Edit vacation request");
			$this->save_previsional = absences_translate("Edit previsional request");
		} else {
			$this->addvac = absences_translate("Send vacation request");
			$this->save_previsional = absences_translate("Save previsional request");
		}
		$this->remark = absences_translate("Description:");
		$this->nbdaystxt = absences_translate("Quantity");
		$this->invaliddate = bab_toHtml(absences_translate("ERROR: End date must be older"), BAB_HTML_JS);
		$this->invaliddate2 = bab_toHtml(absences_translate("Total number of days does not fit between dates"), BAB_HTML_JS);
		$this->invalidentry = bab_toHtml(absences_translate("Invalid entry!  Only numbers are accepted or . !"), BAB_HTML_JS);
		$this->invalidentry1 = absences_translate("Invalid entry");
		$this->invalidentry2 = absences_translate("Days must be multiple of 0.5");
		$this->invalidentry3 = absences_translate("The number of days exceed the total allowed");
		$this->totaltxt = absences_translate("Total:");
		$this->balancetxt = absences_translate("Balance");
		$this->previsionalbalancetxt = absences_translate("Previsional balance");
		$this->calendar = absences_translate("Planning");
		$this->total_days = 0;
		$this->total_hours = 0;
		$this->maxallowed = 0;
		$this->id = $id;
		$this->id_user = $_POST['id_user'];
		$this->ide = bab_pp('ide');
		$this->username = bab_toHtml(bab_getUserName($this->id_user));
		$this->t_days = absences_translate("working days");
		$this->t_alert_nomatch = bab_toHtml(absences_translate("Total number of affected days does not match the period."),BAB_HTML_JS);
		$this->t_confirm_nomatch = bab_toHtml(absences_translate("Total number of affected days does not match the period, do you really want to submit your request with this mismatch?"),BAB_HTML_JS);
		$this->t_or = absences_translate('Or');
		$this->t_recurring = absences_translate('All periods:');
		$this->t_recurring_help = absences_translate('The allocation is made on a single period, the other will be reserved with the same configuration');
		$this->t_previsional = absences_translate('Previsional request (will not be sent to approval)', 'Previsional requests (will not be sent to approval)', $nb_request);
		$this->t_no_approval = absences_translate('This right do not require approval');
        $this->t_force_approval = absences_translate('Launch the approval process on this request');

		$this->allow_mismatch = absences_getVacationOption('allow_mismatch');

        

		if (empty($this->id))
		{

			// create a new entry (not saved)
			$entry = new absences_Entry();
			$entry->setRow(array(
					'id_user' 	 => $this->id_user,
					'date_begin' => $begin,
					'date_end'	 => $end
			));

			$this->previsional = true;
			$this->previsional_checked = isset($_POST['previsional']);

			$this->upd_recurring = false;

		} else {
			$entry = absences_Entry::getById($this->id);
			$dates_modified = ($entry->date_begin !== $begin || $entry->date_end !== $end);

			$entry->date_begin = $begin;
			$entry->date_end = $end;
			if ($this->previsional = $entry->isPrevisonal())
			{
				$this->previsional_checked = true;
			}

			if ($entry->folder && !$dates_modified)
			{
				$this->t_upd_recurring = absences_translate('Update description and rights for all periods with same total quantity in the recurring request');
				$this->upd_recurring = true;
			} else {
				$this->upd_recurring = false;
			}
		}


		$days = $entry->getDurationDays();
		$hours = $entry->getDurationHours();



		$this->period_nbdays = $days;
		$this->period_nbhours = $hours;

		/**
		 * nombre de jours non utilises restant, initialisation
		 */
		$this->last_days = $days;

		/**
		 * nombre d'heures non utilises restantes, initialisation
		 */
		$this->last_hours = $hours;


		$this->t_days = absences_translate("Day(s)");



		$this->begin		= $entry->date_begin;
		$this->end			= $entry->date_end;

		$this->rfrom = isset($_POST['rfrom'])? $_POST['rfrom'] : 0;
		$this->rights = array();
		$rights = absences_getRightsOnPeriod($this->begin, $this->end, $this->id_user, $this->rfrom);

		$this->contain_hours_rights = false;

		$this->no_approval_message = false;
		$one_approval = false;


		foreach($rights as $right) {
			$id		= empty($right['id_rgroup']) ? 'r'.$right['id'] : 'g'.$right['id_rgroup'];

			if ('H' === $right['quantity_unit'])
			{
				$this->contain_hours_rights = true;
			}

			if (isset($this->rights[$id])) {
				$this->rights[$id]['rights'][$right['id']] = array(
					'description'			=> $right['description'],
					'quantity_available'	=> $right['quantity_available'] - $right['waiting'],
			        'previsional_available'	=> $right['quantity_available'] - $right['agentRight']->getPrevisionalQuantity() - $right['waiting'],
			        'sortkey'               => $this->getOptionSortKey($right)
				);
				continue;

			} elseif(!empty($right['id_rgroup'])) {
			    
			    // les droits avec regroupement sont ajoutes dans ['rights']
			    
			    
				$right['rights'] = array(
					$right['id'] => array(
						'description' 			=> $right['description'],
						'quantity_available'	=> $right['quantity_available'] - $right['waiting'],
				        'previsional_available'	=> $right['quantity_available'] - $right['agentRight']->getPrevisionalQuantity() - $right['waiting'],
				        'sortkey'               => $this->getOptionSortKey($right)
					)
				);
			}

			$this->rights[$id] = $right;

			$agentRight = $right['agentRight'];
			if (0 === (int) $agentRight->getRight()->require_approval) {
				$this->no_approval_message = true;
			} else {
			    $one_approval = true;
			}
			
			
		}
		
		
		$author = absences_Agent::getCurrentUser();
		$this->manager_propose_approval = ($author->isManager() && $one_approval && $this->rfrom);
		
		
		if (empty($this->rights)) {
			$babBody->addError(absences_translate('No rights accessibles on this period'));
		}


		bab_Sort::asort($this->rights, 'sortkey');


		$s_days = sprintf('<strong>%s</strong>', absences_editQuantity($days, 'D'));
		$s_hours = sprintf('<strong>%s</strong>', absences_editQuantity($hours, 'H'));

		$r_days = sprintf('<strong>%s</strong>', absences_editQuantity($nb_request*$days, 'D'));
		$r_hours = sprintf('<strong>%s</strong>', absences_editQuantity($nb_request*$hours, 'H'));


		if ($this->contain_hours_rights)
		{
			$this->period_infos = sprintf(absences_translate('The period contain %s day(s) or %s hour(s)', 'Each period contain %s day(s) or %s hour(s)', $nb_request), $s_days, $s_hours);
			$this->recurring_total = sprintf(absences_translate('Total in all periods: %s day(s) or %s hour(s)'), $r_days, $r_hours);
		} else {
			$this->period_infos = sprintf(absences_translate('The period contain %s day(s)', 'Each period contain %s day(s)', $nb_request), $s_days);
			$this->recurring_total = sprintf(absences_translate('All periods contains %s day(s)'), $r_days);
		}



		if (!empty($this->id))
		{
			$res = $babDB->db_query("SELECT id_right, quantity FROM ".ABSENCES_ENTRIES_ELEM_TBL." WHERE id_entry='".$babDB->db_escape_string($this->id)."'");
			while ($arr = $babDB->db_fetch_array($res))
			{
				$this->current['r'.$arr['id_right']] = $arr['quantity'];
			}
		}

		$this->recorded = array();
		if (!empty($this->id))
		{
			$res = $babDB->db_query("
					SELECT
					e.id_right,
					r.id_rgroup,
					e.quantity
					FROM
					".ABSENCES_ENTRIES_ELEM_TBL." e,
					".ABSENCES_RIGHTS_TBL." r

					WHERE
					e.id_entry='".$babDB->db_escape_string($this->id)."'
					AND e.id_right = r.id
					");
			while($arr = $babDB->db_fetch_array($res))
			{
				if (empty($arr['id_rgroup'])) {
					$this->recorded['r'.$arr['id_right']] = $arr['quantity'];
				} else {
					$this->recorded['g'.$arr['id_rgroup']] = $arr['quantity'];
				}
			}

			list($this->remarks) = $babDB->db_fetch_array($babDB->db_query("SELECT comment FROM ".ABSENCES_ENTRIES_TBL." WHERE id=".$babDB->quote($this->id)));
		}
		else
		{
			$this->remarks = isset($_POST['remarks']) ? stripslashes($_POST['remarks']) : '';
		}

		$this->datebegin = absences_longDate(bab_mktime($begin));
		$this->dateend = absences_longDate(bab_mktime($end));


		$this->calurl = absences_addon()->getUrl()."planning&idx=cal&idu=".$this->id_user."&popup=1";

	}
	
	
	private function getOptionSortKey(Array $right)
	{
	    // T8618 ordonner en fonction de la date de fin de validite
	     
	    $sortkey = $right['date_end_valid'];
	    if ('0000-00-00' === $sortkey) {
	        $sortkey = $right['date_end'];
	    }
	    
	    $sortkey .= ' '.$right['description'];
	    
	    return $sortkey;
	}
	


	/**
	 * Correct errors in the DATETIME format
	 * @param string $str
	 * @return string
	 */
	private function formatInputDate($str)
	{
	    $m = null;
		preg_match('/(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/', $str, $m);
		return sprintf('%04d-%02d-%02d %02d:%02d:%02d', $m[1], $m[2], $m[3], $m[4], $m[5], $m[6]);
	}



	/**
	 * Convert days to hours
	 * @param float $days
	 * @return float
	 */
	private function daysToHours($days)
	{
		if (0 == $this->period_nbdays)
		{
			return 0;
		}

		$ratio = $this->period_nbhours / $this->period_nbdays;
		return round(($ratio * $days), 2);
	}

	/**
	 * Convert hours to days
	 * @param float $hours
	 * @return float
	 */
	private function hoursToDays($hours)
	{
		if (0 == $this->period_nbhours)
		{
			return 0;
		}

		$ratio = $this->period_nbdays / $this->period_nbhours;
		return round(($ratio * $hours), 2);
	}

	/**
	 *
	 * @param string 	$unit		D | H
	 * @param float		$set
	 *
	 * @return float | null
	 */
	private function last($unit = null, $set = null)
	{
		if (null === $set)
		{
			switch($unit)
			{
				case 'D':
					return $this->last_days;

				case 'H':
					return $this->last_hours;
			}
		}
		else
		{
			switch($unit)
			{
				case 'D':
					$this->last_days = $set;
					$this->last_hours = $this->daysToHours($set);
					break;

				case 'H':
					$this->last_days = $this->hoursToDays($set);
					$this->last_hours = $set;
					break;
			}
		}

	}


	public function getnextright()
	{

		if (list($id,$this->right) = each($this->rights))
		{
		    // ordonner les listes deroulantes
		    
	        if (!empty($this->right['rights'])) {
	            bab_Sort::asort($this->rights[$id]['rights'], 'sortkey', bab_Sort::CASE_INSENSITIVE);
	        }
		    

	        $agentRight = $this->right['agentRight'];
	        /*@var $agentRight absences_AgentRight */

			$this->id_rgroup = $this->right['id_rgroup'];
			$this->rgroup = bab_toHtml($this->right['rgroup']);

			$this->right['description'] = bab_toHtml($this->right['description']);
			$this->right['current_quantity'] = 
			$this->right['quantity_available'] = $this->right['quantity_available'] - $this->right['waiting'];
			$this->right['previsional_available'] = $this->right['quantity_available'] - $agentRight->getPrevisionalQuantity();
			
			$this->current_quantity = '';
			if (isset($this->current[$id])) {
			    $this->current_quantity = bab_toHtml($this->current[$id]);
			}
			$this->quantity_available = bab_toHtml(absences_quantity($this->right['quantity_available'], $this->right['quantity_unit']));
			$this->previsional_quantity_available = bab_toHtml(absences_quantity($this->right['previsional_available'], $this->right['quantity_unit']));

			
			$right = $agentRight->getRight();

			$this->no_approval = !((bool) $right->require_approval);


			switch($this->right['quantity_unit'])
			{
				case 'D':
					$this->unit = absences_translate('day(s)');
					break;

				case 'H':
					$this->unit = absences_translate('hour(s)');
					break;
			}


			$float_quantity = 0.0;


			if (isset($_POST['nbdays'][$id]))
			{
				$float_quantity = (float) str_replace(',', '.', $_POST['quantity'][$id]);
				$this->quantity = $_POST['quantity'][$id];
			}
			elseif( count($this->recorded) > 0) {
				if (isset($this->recorded[$id])) {
					$float_quantity = (float) $this->recorded[$id];
					$this->quantity = absences_editQuantity($this->recorded[$id], $this->right['quantity_unit']);
				}
				else {
					$this->quantity = '0';
					$float_quantity = 0.0;
				}
			}
			elseif (0 == $this->right['no_distribution'] && $this->last($this->right['quantity_unit']) > 0 && $this->right['quantity_available'] > 0)
			{

				$last = $this->last($this->right['quantity_unit']);


				if ($last >= $this->right['quantity_available'])
				{
					$float_quantity = $this->right['quantity_available'];
					$this->quantity = absences_editQuantity($this->right['quantity_available'], $this->right['quantity_unit']);

					$last -= $this->right['quantity_available'];
					$this->last($this->right['quantity_unit'], $last);
				}
				elseif ($this->right['quantity_available'] > 0)
				{
					$float_quantity = $last;
					$this->quantity = absences_editQuantity($last, $this->right['quantity_unit']);
					$this->last($this->right['quantity_unit'], 0);
				}

			}
			else
			{
				$float_quantity = 0.0;
				$this->quantity = '0';

			}




			switch($this->right['quantity_unit'])
			{
				case 'D':
					$this->total_days += $float_quantity;
					$this->total_hours += $this->daysToHours($this->quantity);
					break;
				case 'H':
					$this->total_hours += $float_quantity;
					$this->total_days += $this->hoursToDays($this->quantity);
					break;
			}



			return true;
		}
		else
			return false;

	}


	public function getnextrgroupright() {
		if (list($id, $arr) = each($this->right['rights'])) {
			$this->id_right = bab_toHtml($id);
			$this->description = bab_toHtml($arr['description']);

			$recorded = isset($this->current['r'.$id]) ? $this->current['r'.$id] : 0;
			$this->current_quantity = bab_toHtml($recorded);
			$this->quantity_available = bab_toHtml($arr['quantity_available']);
			$this->previsional_available = bab_toHtml($arr['previsional_available']);
			$this->selected = isset($this->current['r'.$id]);
			return true;
		}
		return false;
	}

	public function getnextrecurring() {
		if (list($begin, $end) = each($_POST['period'])) {
			$this->period = bab_toHtml(sprintf(absences_translate('from %s to %s'), bab_shortDate(bab_mktime($begin)), bab_shortDate(bab_mktime($end))));
			$this->p_begin = bab_toHtml($begin);
			$this->p_end = bab_toHtml($end);
			return true;
		}
		return false;
	}

}





function requestVacation($begin,$end, $id, $rfrom, $ide)
	{
	global $babBody;
	
	if ($id)
	{
		$babBody->setTitle(absences_translate("Edit vacation request"));
	} else
	{
		$babBody->setTitle(absences_translate("Request vacation"));
	}
	
	
	if( !bab_isEntryEditable($id) )
	{
		return false;
	}
	
	if (absences_lockedForMainteance())
	{
		$babBody->addError(absences_getMaintenanceMessage());
		return false;
	}
	
	$agent = absences_Agent::getCurrentUser();
	
	if ($id)
	{
		if ($rfrom)
		{
			if ($ide && $agent->isEntityManagerOf($ide))
			{
				// modification d'une demande par un gestionnaire delegue
				$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
				$babBody->addItemMenu("entity_members", absences_translate("Entity requests"), absences_addon()->getUrl()."vacchart&idx=entity_requests&ide=".$ide);
				$babBody->addItemMenu("vunew", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=vunew&id=".$id."&rfrom=1&ide=".$ide);
				
			} elseif (!$ide && $agent->isManager()) {
				// modification d'une demande par un gestionnaire
				$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
				$babBody->addItemMenu("lper", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
				$babBody->addItemMenu("vunew", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=vunew&id=".$id."&rfrom=1");
			}
		} else {
			// modification d'une demande par un agent
			$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
			$babBody->addItemMenu("vunew", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=vunew&id=".$id);
		}
	} else {
		
		if ($rfrom)
		{
			if ($ide && $agent->isEntityManagerOf($ide))
			{
				// creation d'une demande par un gestionnaire delegue
				$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
				$babBody->addItemMenu("entity_members", absences_translate("Entity requests"), absences_addon()->getUrl()."vacchart&idx=entity_requests&ide=".$ide);
				$babBody->addItemMenu("vunew", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=vunew&rfrom=1&ide=".$ide);
		
			} elseif (!$ide && $agent->isManager()) {
				// creation d'une demande par un gestionnaire
				$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
				$babBody->addItemMenu("lper", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
				$babBody->addItemMenu("vunew", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=vunew&rfrom=1");
			}
		} else {
			// creation d'une demande par un agent
			$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
			$babBody->addItemMenu("vunew", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=vunew");
		}
		
	}
	

	$temp = new absences_RequestVacation($begin,$end, $id);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacuser.html", "newvacation"));
	}



function period($id_user, $id = 0)
	{
	    require_once dirname(__FILE__).'/utilit/period.ui.php';
	    
		$temp = new absences_PeriodFrame($id_user, $id);
		$GLOBALS['babBody']->babecho(absences_addon()->printTemplate($temp, "vacuser.html", "period"));
	}




function viewrights($id_user)
	{
	class temp
		{

		var $altbg = true;

		function temp($id_user) {
			$this->rights = absences_getRightsByGroupOnPeriod($id_user);
			$this->total = 0;
			$this->total_waiting = 0;
			$this->t_avariable_days = absences_translate("Avariable days");
			$this->t_waiting_days = absences_translate("Waiting days");
			$this->t_period_nbdays = absences_translate("Period days");
			$this->t_total = absences_translate("Total");

			$this->t_available = absences_translate("Available");
			$this->t_waiting = absences_translate("Waiting");

			$this->total = array('D' => 0, 'H' => 0);
			$this->total_waiting = array('D' => 0, 'H' => 0);
		}

		function getnextright()
			{
			if ($right = & current($this->rights))
				{
				$this->altbg = !$this->altbg;
				next($this->rights);
				$quantity_available = $right['quantity_available'] - $right['waiting'];
				$this->description = bab_toHtml($right['description']);

				$this->quantity_available = array('D' => 0, 'H' => 0);
				$this->waiting = array('D' => 0, 'H' => 0);

				$this->quantity_available[$right['quantity_unit']] = $quantity_available;
				$this->waiting[$right['quantity_unit']] = $right['waiting'];

				$this->total[$right['quantity_unit']] += $this->quantity_available[$right['quantity_unit']];
				$this->total_waiting[$right['quantity_unit']] += $this->waiting[$right['quantity_unit']];
				return true;
				}
			else
				return false;

			}

		}
		$temp = new temp($id_user);
		
		global $babBody;
		
		$html = absences_addon()->printTemplate($temp, "vacuser.html", "viewrights");
		
		if (1 === (int) bab_rp('popup'))
		{
		    $babBody->babPopup($html);
		} else {
		    $babBody->babEcho($html);
		}
	}



function vedUnload()
	{
	class temp
		{
		var $message;
		var $close;
		var $redirecturl;

		function temp()
			{
			$this->message = absences_translate("Vacation entry has been updated");
			$this->close = absences_translate("Close");
			$this->redirecturl = absences_addon()->getUrl()."vacuser&idx=lval";
			}
		}

	$temp = new temp();
	global $babBody;
	$babBody->babPopup(absences_addon()->printTemplate($temp, "vacuser.html", "vedunload"));
	}



class absences_saveVacation
{


	/**
	 * Test la quantite dispo sur un droit
	 * 
	 *
	 * @param	array	&$nbdays	list of rights to save in request, rows are added on each valid calls
	 * @param	Array	$arr		vacation rights properties
	 * @param	float	$quantity	requested quantity in form
	 * @param	int		$id_request	if this is a request modification
	 * 
	 * @throws Exception
	 * 
	 * @return bool
	 */
	private static function addVacationRight(&$nbdays, Array $arr, $quantity, $id_request, &$used_in_previous_periods)
	{
		$quantity = str_replace(',','.', $quantity);
		$available = ($arr['quantity_available'] - $arr['waiting']);
		
		if (!isset($used_in_previous_periods[$arr['id']]))
		{
			$used_in_previous_periods[$arr['id']] = 0;
		}

		if (!empty($id_request)) {
			
			$entry = absences_Entry::getById($id_request);
			
			if (!$entry->isPrevisonal()) {
    			$element = $entry->getElement($arr['id']);
    			
    			if (isset($element)) {
        			// quantity in current request is unavailable, add it to $available (waiting or confirmed)
        			$available += $element->quantity;
    			}
			}
		}

		
		if ('' === $quantity) {
			$quantity = 0;
		}
		

		if(!is_numeric($quantity) || $quantity < 0 ) {
			throw new Exception(sprintf(absences_translate("You must specify a correct number days on right %s"), $arr['description']));
		}

		if (!empty($quantity) && $arr['cbalance'] != 'Y' && ($available - $quantity - $used_in_previous_periods[$arr['id']]) < 0) {
			throw new Exception(sprintf(absences_translate("You can't take more than %s on the right %s"), absences_quantity($available, $arr['quantity_unit']), $arr['description']));
		}
		
		
		// si le droit est un compte epargne temps, verifier les conditions d'utilisations speciales
		if (!empty($quantity))
		{
			$agentRight = $arr['agentRight'];
			/*@var $agentRight absences_agentRight */
			$right = $agentRight->getRight();
			
			if (absences_Right::CET === $right->getKind() && ($cet = $right->getRightCet()))
			{
				if (0 !== (int) round(10*$cet->min_use)) // 0 = min_use disabled
				{
					// -1 = force using all available
					
					$min_use = (float) $cet->min_use;
					
					if ($min_use < 0 && $quantity < $available)
					{
						throw new Exception(sprintf(absences_translate("The quantity available in %s must be used all in one request"), $arr['description']));
					}
					
					
					if ($min_use > 0 && ((int) round(10*$quantity)) < ((int) round(10*$min_use)))
					{
						throw new Exception(sprintf(absences_translate("The minimal quantity usable in one request for the right %s is %s"), $arr['description'], absences_quantity($min_use, 'D')));
					}
				}
			}
		}
		

		if( $quantity > 0 ) {
			$nbdays['id'][] = (int) $arr['id'];
			$nbdays['val'][] = $quantity;
			$nbdays['total'][$arr['quantity_unit']] += $quantity;
			$used_in_previous_periods[$arr['id']] += $quantity;
		}

		return true;
	}
	
	
	/**
	 * requests in same folder and with same quantity and with same status
	 * @param absences_Entry $entry
	 * @return array
	 */
	private static function selectFolderSimilarRequests($entry)
	{
		require_once dirname(__FILE__).'/utilit/entry.class.php';
		global $babDB;
		
		$res = $babDB->db_query('SELECT
				e.* 
			FROM
				absences_entries e 
			WHERE e.folder='.$babDB->quote($entry->folder).'
				AND e.status='.$babDB->quote($entry->status).'
				AND e.id<>'.$babDB->quote($entry->id).' 
		');
		
		$return = array();
		while($arr = $babDB->db_fetch_assoc($res))
		{
			$e = new absences_Entry();
			$e->setRow($arr);
			
			if ($e->getTotalDays() == $entry->getTotalDays())
			{
				$return[] = $e;
			}
		}
		
		return $return;
	}
	
	
	/**
	 * Save posted data
	 */
	public static function save()
	{
		global $babBody;
		
		if (absences_lockedForMainteance())
		{
			return false;
		}
		
		
		$id_request		= (int) $_POST['id'];
		$id_user		= (int) $_POST['id_user'];
		$rfrom			= $_POST['rfrom'];
		
		if( $rfrom == 1 )
		{
			$agent = absences_Agent::getCurrentUser();
			if( !$agent->isManager())
			{
				$rfrom = 0;
			}
		}
		
		
		
		if (isset($_POST['period']) && is_array($_POST['period']))
		{
			// recurring request
			
			$period_list = $_POST['period'];
			$folder = count($period_list) > 1 ? time() : 0;
			$period_list_id = array();
			
		} else if (isset($_POST['upd_recurring']) && $id_request > 0) 
		{

			// update recurring request

			require_once dirname(__FILE__).'/utilit/entry.class.php';
			$entry = absences_Entry::getById($id_request);
			
			$folder = (int) $entry->folder;
			
			$period_list = array(
					$_POST['begin'] => $_POST['end']
			);
			
			$period_list_id = array(
					$_POST['begin'] => $id_request
			);
			
			$list = self::selectFolderSimilarRequests($entry);
			
			foreach($list as $e)
			{
				$period_list[$e->date_begin] = $e->date_end;
				$period_list_id[$e->date_begin] = $e->id;
			}
			
			
			
	    } else {
		
			$period_list = array(
				$_POST['begin'] => $_POST['end']
			);
			
			$period_list_id = array(
				$_POST['begin'] => $id_request
			);
			
			$folder = 0;
		}
		
		$allow_mismatch = absences_getVacationOption('allow_mismatch');
		
		if (!$allow_mismatch) {
    		// T9957 verification supplementaire pour les incoherence dans le nombre de jours
    		$period_nbdays = str_replace(',', '.', $_POST['period_nbdays']);
    		$req_nbdays = str_replace(',', '.', $_POST['total']['D']);
    		
    		if (!absences_cq($period_nbdays, $req_nbdays)) {
    		    $babBody->addError(absences_translate('Total number of affected days does not match the period.'));
    		    return false;
    		}
		}
		
		/**
		 * Les droits utilises pour les tests sont les droits affiches a l'ecran
		 * dans une demande repetitive, c'est la premiere periode qui est utilise pour les tests d'affichage
		 */
		$rights = absences_getRightsOnPeriod(key($period_list), reset($period_list), $id_user, $rfrom);
		
		// test phase
		
		$used_in_previous_periods = array();
		
		
		$nbdays = null;
		
		
		foreach($period_list as $begin => $end)
		{
			$id_request = isset($period_list_id[$begin]) ? $period_list_id[$begin] : 0;
			
			
			try {
			    // la repartition des jours sur les droits
			    $_nbdays = self::testRightsQuantity($id_request, $rights, $used_in_previous_periods);
			    if (!isset($nbdays)) {
			        $nbdays = $_nbdays;
			    }
			    
			} catch (Exception $e)
			{
			    $babBody->addError($e->getMessage());
			    return false;
			}
			
			
			// verifier que le droit est dispo sur la periode
			// utile pour les demandes repetitives
			if (!test_periodValidity($id_request, $id_user, bab_mktime($begin), bab_mktime($end))) {
				return false;
			}

			try {
				self::testRightsAvailability($rights, $nbdays, $begin, $end);
			} catch (Exception $e)
			{
				$babBody->addError($e->getMessage());
				return false;
			}
		}
		
		
		if (!isset($nbdays)) {
		    throw new Exception('missing quantity');
		}
		
		
		
		// save phase
		$saved = false;
		foreach ($period_list as $begin => $end) {
			$id_request = isset($period_list_id[$begin]) ? $period_list_id[$begin] : 0;
			
			if (self::savePeriod($id_request, $begin, $end, $id_user, $rfrom, $nbdays, $folder)) {
				$saved = true;
			}
		}
		
		
		if ($saved && !isset($_POST['previsional']))
		{
			$defer = (bool) absences_getVacationOption('approb_email_defer');
			if (!$defer) {
			    require_once dirname(__FILE__).'/utilit/request.notify.php';
				absences_notifyRequestApprovers();
			}
		}

		return true;
	}
	
	
	
	
	
	
	
	/**
	 * Test des soldes 
	 * retourne la liste des droit qui devons etre inseres ou false si la demande n'est pas autorise
	 * 
	 * @throws Exception
	 * 
	 * @return array | false
	 */
	private static function testRightsQuantity($id_request, $rights, &$used_in_previous_periods)
	{
		
		
		$rgroups = array();
		$nbdays = array(
			'id' => array(),
			'val' => array(),
		    'total' => array(
		        'D' => 0,
		        'H' => 0
		    )
		);
		
		// regular vacation rights
		foreach($rights as &$arr)
		{
			if( isset($_POST['quantity'][$arr['id']]))
			{
				$quantity = $_POST['quantity'][$arr['id']];
				if (!self::addVacationRight($nbdays, $arr, $quantity, $id_request, $used_in_previous_periods))
				{
					return false;
				}
			}
		
			if ($arr['id_rgroup'] > 0) {
				$rgroups[$arr['id']] = $arr['id_rgroup'];
			}
		}
		
		// right groups
		
		if (isset($_POST['rgroup_right'])) {
			foreach($_POST['rgroup_right'] as $id_rgroup => $id_right) {
				if (isset($rgroups[$id_right]) && $rgroups[$id_right] == $id_rgroup) {
					$quantity = $_POST['rgroup_value'][$id_rgroup];

					if (!self::addVacationRight($nbdays, $rights[$id_right], $quantity, $id_request, $used_in_previous_periods))
					{
						return false;
					}
				}
			}
		}
		
		if (empty($nbdays['val']))
		{
			throw new Exception(absences_translate("Empty requests are not allowed"));
		}
		
		
		$days = 0.0;
		$hours = 0.0;
		
		if (isset($_POST['total']['D'])) {
		    $days = (float) str_replace(',', '.', $_POST['total']['D']);
		}
		
		if (isset($_POST['total']['H'])) {
		    $hours = (float) str_replace(',', '.', $_POST['total']['H']);
		}
		
		

		
		if ($nbdays['total']['D'] > 0 && !absences_cq($nbdays['total']['D'], $days)) {
		    throw new Exception(absences_translate("Quantities must match total days"));
		}
		
		if ($nbdays['total']['H'] > 0 && !absences_cq($nbdays['total']['H'], $hours)) {
		    throw new Exception(absences_translate("Quantities must match total hours"));
		}
		
		return $nbdays;
	}
	
	
	
	/**
	 * Tester pour une periode si les droits avec une quantite saisie est disponibe
	 * 
	 * @param array $rights			Array from absences_getRightsOnPeriod
	 * @param array $nbdays
	 * @param string $begin			ISO datetime
	 * @param string $end			ISO datetime
	 * 
	 * @throws Exception
	 * 
	 * @return bool
	 */
	private static function testRightsAvailability(Array $rights, Array $nbdays, $begin, $end)
	{
		$begin_ts = bab_mktime($begin);
		$end_ts = bab_mktime($end);
		
		
		foreach($nbdays['id'] as $k => $id_right)
		{
			if (isset($nbdays['val'][$k]) && !empty($nbdays['val'][$k]))
			{
				$agentRight = $rights[$id_right]['agentRight'];
				

				
				if (!$agentRight->isAccessibleOnPeriod($begin_ts, $end_ts))
				{
					throw new Exception(sprintf(
							absences_translate('The right "%s" is not accessible %s, in a recurring request, all periods must be inside the used right validity period'), 
							$rights[$id_right]['description'], 
							mb_strtolower(absences_DateTimePeriod($begin, $end))
					));
				}
				
				if (!$agentRight->isAccessibleOnDelay($begin_ts))
				{
					$right = $agentRight->getRight();
					$rightRule = $right->getRightRule();
					
					throw new Exception(sprintf(
							absences_translate('The right "%s" is not accessible in the next %s days'), 
							$right[$id_right]['description'], 
							$rightRule->delay_before
					));
				}
			}
		}
		
		return true;
	}
	
	
	
	private static function getPeriodSpanOnEdit($old_date_begin, $old_date_end, BAB_DateTime $date_begin, BAB_DateTime $date_end)
	{
	    $old_date_begin_obj = BAB_DateTime::fromIsoDateTime($old_date_begin);
	    $old_date_end_obj = BAB_DateTime::fromIsoDateTime($old_date_end);
	    
	    $old_date_begin = $old_date_begin_obj->getTimeStamp();
	    $old_date_end = $old_date_end_obj->getTimeStamp();
	    
	    $new_date_begin = $date_begin->getTimeStamp();
	    $new_date_end = $date_end->getTimeStamp();
	    
	    $period_begin	= $old_date_begin 	< $new_date_begin 	? $old_date_begin 	: $new_date_begin;
	    $period_end 	= $old_date_end 	> $new_date_end 	? $old_date_end 	: $new_date_end;
	    
	    return array($period_begin, $period_end);
	}
	
	
	
	/**
	 * Save in calendar
	 * 
	 * 
	 * @param int $id_request
	 * @param BAB_DateTime | null $old_date_begin_obj
	 * @param BAB_DateTime | null $old_date_end_obj
	 */
	private static function saveInCalendar($id_request, $id, $old_date_begin_obj, $old_date_end_obj)
	{
	    if (empty($id_request)) {
	        // event creation
	        absences_createPeriod($id);
	    
	    } else {
	        // event modification
	        absences_updatePeriod($id, $old_date_begin_obj, $old_date_end_obj);
	    }
	}


	/**
	 * 
	 * @param string   $begin		ISO datetime
	 * @param string   $end		    ISO datetime
	 * @param int      $id_user     Request owner
	 * @param int      $rfrom       rfrom=1 if modification from a manager and from the back-office
	 * @return boolean
	 */
	private static function savePeriod($id_request, $begin, $end, $id_user, $rfrom, $nbdays, $folder = 0)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		require_once dirname(__FILE__).'/utilit/entry.class.php';
		require_once dirname(__FILE__).'/utilit/entry_elem.class.php';
		global $babBody;


		
		$remarks		= $_POST['remarks'];
		
		
		$date_begin = BAB_DateTime::fromIsoDateTime($begin);
		$date_end	= BAB_DateTime::fromIsoDateTime($end);
		$status = isset($_POST['previsional']) ? 'P' : 'Y';
		
		


		if (empty($id_request)) {
			// event creation
			
			$entry = new absences_Entry;
			$entry->id_user = $id_user;
			$entry->createdBy = bab_getUserId();
			$entry->date_begin = $begin;
			$entry->date_end = $end;
			$entry->comment = $remarks;
			$entry->createdOn = date('Y-m-d H:i:s');
			$entry->idfai = 0;
			$entry->status = $status;
			$entry->folder = $folder;
			    
			$entry->save();
			
			$id = $entry->id;

			$period_begin = bab_mktime($begin);
			$period_end = bab_mktime($end);
			
			$old_date_begin_obj = null;
			$old_date_end_obj = null;


		}
		else {
			// event modification

			$entry = absences_Entry::getById($id_request);

			$old_date_begin = $entry->date_begin;
			$old_date_end = $entry->date_end;

			if ($entry->idfai > 0) {
				deleteFlowInstance($entry->idfai);
			}
				
			$entry->date_begin = $date_begin->getIsoDateTime();
			$entry->date_end = $date_end->getIsoDateTime();
			$entry->comment = $remarks;
			$entry->idfai = 0;
			$entry->status = $status;
			
			$entry->save();
			
			$entry->loadElements();

			$id = $id_request;

			
            list($period_begin, $period_end) = self::getPeriodSpanOnEdit($old_date_begin, $old_date_end, $date_begin, $date_end);
            
            $old_date_begin_obj = BAB_DateTime::fromIsoDateTime($old_date_begin);
            $old_date_end_obj = BAB_DateTime::fromIsoDateTime($old_date_end);
		}


		// insert rights
		
	   $saved_rights = array();

		if (isset($nbdays['id'])) {
		  $count = count($nbdays['id']);
		  for( $i = 0; $i < $count; $i++) {
			   
		      $id_right = $nbdays['id'][$i];
		      $saved_rights[$id_right] = $id_right;
		      
    			$elem = $entry->getElement($id_right);
    			if (!isset($elem)) {
    			     $elem = new absences_EntryElem();
    			     $elem->setEntry($entry);
    			     $entry->addElement($elem);
    			}
    			
    			$elem->id_right = $id_right;
    			$elem->date_begin = '0000-00-00 00:00:00';
    			$elem->date_end = '0000-00-00 00:00:00';
    			$elem->quantity = $nbdays['val'][$i];
			}
		}
		
		// remove elements set to zero
		foreach($entry->getElements() as $element) {
		    if (!isset($saved_rights[$element->id_right])) {
		        $element->delete();
		    }
		}
		
		$entry->setElementsDates();
		$entry->saveElements();
		$entry->createPlannedPeriods();
		$entry->savePlannedPeriods();
		

		// set period into calendar backend if necessary
		try {
            self::saveInCalendar($id_request, $id, $old_date_begin_obj, $old_date_end_obj);
		} catch (Exception $e) {
		    // ex: caldav is not accessible
		    
		    /*@var $babBody babBody */
		    $babBody->addNextPageError(sprintf(absences_translate('Failed to save event in calendar: %s'), $e->getMessage()));
		}


		include_once $GLOBALS['babInstallPath']."utilit/eventperiod.php";
		$event = new bab_eventPeriodModified($period_begin, $period_end, $id_user);
		$event->types = BAB_PERIOD_VACATION;
		bab_fireEvent($event);
		
		
		

		if (!isset($_POST['previsional']))
		{
		    $author = absences_Agent::getCurrentUser();
		    $approbCreated = false;
		    $startApproval = true;
		    
		    if ($author->isManager()) {
		        if (isset($_POST['force_approval'])) { // checkbox does not exists if manager create his own request
		            $startApproval = (bool) bab_pp('force_approval');
		        }
		    }
		    
		    
		    if ($startApproval) {
    			// create approbation
    			if (self::createInstance($entry, !empty($id_request))) {
    			    $approbCreated = true;
    			}
		    } else {
		        self::addNoApprovalMovement($entry, !empty($id_request));
		    }
		    
		    if (!$approbCreated)
		    {
		        // ex : retirer des RTT en fonction des arret maladie, l'absences maladie est cree par le gestionnaire sans approbation
		        $entry->applyDynamicRight();
		        
		        // notify owner and other emails about the confirmed vacation request
		        $entry->notifyOwner();
		    }
			
			if (!empty($id_request))
			{
				// notification de modification
				$entry = absences_Entry::getById($id);
				absences_notifyOnRequestChange(array($entry), $entry->id_user);
				absences_notifyManagers::send($id);
			}
			
		} else {
		    self::addPrevisionalMovement($entry, !empty($id_request));
		}
		
		

		return true;
	}

	
	
	public static function submitRequest($id)
	{
	    require_once dirname(__FILE__).'/utilit/entry.class.php';
	    global $babBody;
	    
	    $entry = absences_Entry::getById($id);
	    
	    try {
	        $entry->checkAvailable();
	    } catch(Exception $e) {
	        $babBody->addNextPageError($e->getMessage());
	        return false;
	    }
	    
	    if (!self::createInstance($entry)) {
	        // ex : retirer des RTT en fonction des arret maladie, l'absences maladie est cree par le gestionnaire sans approbation
	        $entry->applyDynamicRight();
	        
	        // notify owner and other emails about the confirmed vacation request
	        $entry->notifyOwner();
	    }
	}
	
	
	
	protected static function addPrevisionalMovement($entry, $modify)
	{
	    if ($modify)
	    {
	        $text = absences_translate('The previsional %s has been modified without approval');
	    } else {
	        $text = absences_translate('The previsional %s has been created without approval');
	    }
	     
	    $entry->addMovement(sprintf($text, $entry->getTitle()));
	}
	
	
	protected static function addNoApprovalMovement($entry, $modify)
	{
	    if ($modify)
	    {
	        $text = absences_translate('The %s has been modified without approval');
	    } else {
	        $text = absences_translate('The %s has been created without approval');
	    }
	    
	    $entry->addMovement(sprintf($text, $entry->getTitle()));
	}
	
	
	/**
	 * Create approbation instance, set status in waiting state
	 * @param 	absences_Entry 	$entry
	 * @param	bool	        $modify
	 * 
	 * @return bool
	 */
	public static function createInstance(absences_Entry $entry, $modify = false)
	{
		require_once dirname(__FILE__).'/utilit/request.notify.php';
		global $babBody;
		
		
		
		if (!$entry->requireApproval())
		{

		    self::addNoApprovalMovement($entry, $modify);
			return false;
		} else {
		    
		    
		    if ($modify)
		    {
		        $text = absences_translate('The %s has been modified');
		    } else {
		        $text = absences_translate('The %s has been created');
		    }
		    
		    $entry->addMovement(sprintf($text, $entry->getTitle()));
		}
		
		
		if (!$entry->createApprobationInstance()) {
		    // impossible de crer une instance, 
		    // l'utilisateur peut etre en auto-approbation ou le schema ne retourne aucun approbateur
    		$entry->status = 'Y';
    		$entry->onConfirm();
    		$entry->save();
    		return false;
		}
		
		
		
		return true;
	}
	
	
	
	public static function gotoList()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		
		$url = bab_url::get_request('tg');
		$url->idx = 'lvreq';
		$url->location();
	}

}








/**
 * Test period validity
 * verifie qu'il n'y a pas de chevauchements de periodes
 * 
 * @param int $id_entry
 * @param int $id_user
 * @param int $begin
 * @param int $end
 * @return boolean
 */
function test_periodValidity($id_entry,$id_user,$begin,$end)
{
	global $babBody, $babDB;
	
	if( $begin >= $end)
		{
		$babBody->msgerror = absences_translate("ERROR: End date must be older")." !";
		return false;
		}
		

	$date_begin = date('Y-m-d H:i:s',$begin);
	$date_end = date('Y-m-d H:i:s',$end);

	$req = "SELECT
				COUNT(*)
		FROM ".ABSENCES_ENTRIES_TBL."
			WHERE
			id_user='".$babDB->db_escape_string($id_user)."'
			AND date_begin < '".$babDB->db_escape_string($date_end)."'  
			AND date_end > '".$babDB->db_escape_string($date_begin)."' 
			AND id <> '".$babDB->db_escape_string($id_entry)."'
			AND status<>'N'";

	$res = $babDB->db_query($req);
	list($n) = $babDB->db_fetch_array($res);

	if ($n > 0) {
		$babBody->msgerror = absences_translate("ERROR: a request is already defined on this period");
		return false;
	}

	return true;
}

/**
 * Test period validaity from posted first step
 * @return boolean
 */
function test_period()
{
global $babBody;
include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

if (!isset($_POST['daybegin']) ||
	!isset($_POST['monthbegin']) ||
	!isset($_POST['yearbegin']) ||
	!isset($_POST['hourbegin']) ||
	!isset($_POST['dayend']) ||
	!isset($_POST['monthend']) ||
	!isset($_POST['yearend']) ||
	!isset($_POST['hourend']) ||
	!isset($_POST['id_user'])
	)
	{
	$babBody->msgerror = absences_translate("Error");
	return false;
	}

	$yearbegin = $_POST['year'] + $_POST['yearbegin'] - 1;
	$yearend = $_POST['year'] + $_POST['yearend'] - 1;

	$begin	= BAB_DateTime::fromIsoDateTime($yearbegin.'-'.$_POST['monthbegin'].'-'.$_POST['daybegin'].' '.$_POST['hourbegin']);
	$end	= BAB_DateTime::fromIsoDateTime($yearend.'-'.$_POST['monthend'].'-'.$_POST['dayend'].' '.$_POST['hourend']);

	$id_entry = isset($_POST['id']) ? $_POST['id'] : 0;

return test_periodValidity($id_entry, $_POST['id_user'], $begin->getTimeStamp(), $end->getTimeStamp());
}




/**
 * Get posted date from the first step edit form or from the hidden field
 *
 */
class absences_PostedDate
{
	/**
	 * @return string ISO datetime
	 */
	public static function begin()
	{
		if (isset($_POST['daybegin']))
		{
			$yearbegin = $_POST['year'] + $_POST['yearbegin'] -1;
			return $yearbegin.'-'.$_POST['monthbegin'].'-'.$_POST['daybegin'].' '.$_POST['hourbegin'];
		}
		
		if (isset($_POST['begin']))
		{
			return $_POST['begin'];
		}
		
		if (isset($_POST['period']))
		{
			return key($_POST['period']);
		}
	}
	
	/**
	 * @return string ISO datetime
	 */
	public static function end()
	{
		if (isset($_POST['dayend']))
		{
			
			$yearend = $_POST['year'] + $_POST['yearend'] -1;
			return $yearend.'-'.$_POST['monthend'].'-'.$_POST['dayend'].' '.$_POST['hourend'];
		}
		
		if (isset($_POST['end']))
		{
			return $_POST['end'];
		}
		
		if (isset($_POST['period']))
		{
			return reset($_POST['period']);
		}
	}
}




function absences_workperiodGotoList()
{
    $agent = absences_Agent::getCurrentUser();
    
    if (!empty($_POST['workperiod']['id_user'])) {
        
        
        if ($agent->isManager()) {
            $url = new bab_url(absences_addon()->getUrl().'vacadmwd');
            $url->location();
            return;
        }
        
        //TODO: liste des demandes de l'entite si gestionnaire delegue
    }
    
    $url = bab_url::get_request('tg');
    $url->idx = 'lvreq';
    $url->location();
}




/**
 * Save form
 * @param array 							$workperiod
 * @param absences_WorkperiodRecoverRequest	$workperiodRecover
 * @throws Exception
 */
function absences_saveWorkperiodRecoverRequest(Array $workperiod, absences_WorkperiodRecoverRequest $workperiodRecover = null)
{
	$babBody = bab_getInstance('babBody');
	/* @var $babBody babBody */
	
	if (absences_lockedForMainteance())
	{
		return false;
	}
	
	
	if (!absences_WorkperiodRecoverRequest::checkForm($workperiod, $workperiodRecover))
	{
		return false;
	}
	
	
	
	
	
	$type = absences_WorkperiodType::getFromId($workperiod['id_type']);
	
	if (!$type->getRow())
	{
		throw new Exception(absences_translate('This type does not exits'));
	}
	
	$agent = absences_Agent::getCurrentUser();
	
	if (!isset($workperiodRecover))
	{
		$workperiodRecover = new absences_WorkperiodRecoverRequest();
		
		if (!empty($workperiod['id_user'])) {
    		$spoofed = absences_Agent::getFromIdUser($workperiod['id_user']);
    		if (!$agent->isManager() && !$agent->isSuperiorOf($spoofed)) {
    		    throw new Exception(absences_translate('This type does not exits'));
    		}
    		
    		$workperiodRecover->id_user = $spoofed->getIdUser();
    		
		} else {
		    $workperiodRecover->id_user = bab_getUserId();
		}
	}
	
	$workperiodRecover->date_begin = absences_dateTimeForm($workperiod['datebegin'], $workperiod['hourbegin']);
	$workperiodRecover->date_end = absences_dateTimeForm($workperiod['dateend'], $workperiod['hourend']);
	$workperiodRecover->id_type = $type->id;
	$workperiodRecover->comment = $workperiod['comment'];
	$workperiodRecover->quantity = $type->quantity;
	$workperiodRecover->quantity_unit = $type->quantity_unit;
	$workperiodRecover->status = 'Y';
	$workperiodRecover->id_right = 0;
	$workperiodRecover->save();
	

	if ($workperiodRecover->createApprobationInstance())
	{
		$babBody->addNextPageMessage(absences_translate('Your workperiod recovery request has been sent for approval'));
	} else {
		$babBody->addNextPageMessage(absences_translate('Your workperiod recovery request has been saved without approval'));
	}
	
	$defer = (bool) absences_getVacationOption('approb_email_defer');
	
	if (!$defer)
	{
		require_once dirname(__FILE__).'/utilit/request.notify.php';
		absences_notifyRequestApprovers();
	}

	
	absences_workperiodGotoList();
}










/**
 * Display form
 */
function absences_createWorkperiodRecoverRequest()
{
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.ui.php';
	
	
	$babBody = bab_getBody();
	
	
	
	$agent = absences_Agent::getCurrentUser();
	
	if ($id_user = (int) bab_rp('id_user')) {
	    $spoofed = absences_Agent::getFromIdUser($id_user);
	    if (!$agent->isManager() && !$agent->isSuperiorOf($spoofed)) {
	        return $babBody->addError('Spoofing failed workperiod recover request creation, you must be superior of the spoofed agent');
	    }
	    
	    $agent = $spoofed;
	}
	
	
	
	$request = null;
	$id = bab_rp('id', null);
	if (isset($id))
	{
		$request = absences_WorkperiodRecoverRequest::getById($id);
		if (!$request->canModify())
		{
			return;
		}
	} else if (!$agent->canCreateWorkperiodRecoverRequest()) {
		return $babBody->addError('Agent not allowed to create workperiod recover requests');
	}
	
	
	if (!isset($request) && $id_user) {
	    $request = new absences_WorkperiodRecoverRequest();
	    $request->id_user = $id_user;
	}
	
	
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	if (isset($_POST['workperiod']))
	{
		if ( isset($_POST['workperiod']['cancel']) )
		{
			absences_workperiodGotoList();
		}
	
	
		if( isset($_POST['workperiod']['save'] ))
		{
			try {
				absences_saveWorkperiodRecoverRequest($_POST['workperiod'], $request);
				
			} catch (Exception $e)
			{
				$page->addError($e->getMessage());
			}
		}
	}
	
	
	$page->setTitle(absences_translate('Create a working days report entitling recovery'));
	
	if (absences_lockedForMainteance())
	{
		$page->addError(absences_getMaintenanceMessage());
		
	} else {
	
		$editor = new absences_WorkperiodRecoverRequestEditor($request, isset($spoofed), false);
		$page->addItem($editor);
		
	}
	
	$page->displayHtml();
}




function absences_cetGotoList()
{
    $agent = absences_Agent::getCurrentUser();

    if (!empty($_POST['cet']['id_user'])) {


        if ($agent->isManager()) {
            $url = new bab_url(absences_addon()->getUrl().'vacadmcet');
            $url->location();
            return;
        }

        //TODO: liste des demandes de l'entite si gestionnaire delegue
    }

    $url = bab_url::get_request('tg');
    $url->idx = 'lvreq';
    $url->location();
}


/**
 * Save a CET request
 * 
 * 
 * @param array 					 $cet					posted form
 * @param absences_CetDepositRequest $cet_deposit_request	Current request to modify or null for new request
 */
function absences_saveCetDepositRequest(Array $cet, absences_CetDepositRequest $cet_deposit_request = null)
{
	require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
	require_once dirname(__FILE__).'/utilit/agent_right.class.php';
	
	$babBody = bab_getInstance('babBody');
	/* @var $babBody babBody */
	
	
	if (absences_lockedForMainteance())
	{
		return false;
	}
	

	$quantity = (float) str_replace(',', '.', $cet['quantity']);
	$agent_right = absences_AgentRight::getById((int) $cet['id_agent_right_source']);
	
	
	$agent = $agent_right->getAgent();
	
	if (!isset($cet['id_user']) && bab_getUserId() !== $agent->getIdUser())
	{
		throw new Exception('Not accessible agentRight');
	}
	
	if (isset($cet['id_user']))
	{
	    $currentUser = absences_Agent::getCurrentUser();
	    if (!$currentUser->isManager() && !$currentUser->isSuperiorOf($agent)) {
	        throw new Exception(absences_translate('Access denied to this account'));
	    }
	}
	
	
	$AgentCet = $agent->Cet();
	if (!$AgentCet->testDepositQuantity($agent_right, $quantity))
	{
		return false;
	}
	
	
	
	if (!isset($cet_deposit_request))
	{
		// create the request
		$cet_deposit_request = new absences_CetDepositRequest();
	}
	
	$CetAgentRight = $AgentCet->getDepositAgentRight(true);
	if (!isset($CetAgentRight)) {
	    throw new Exception(absences_translate('No active time saving account found for this account'));
	}
	
	$cet_deposit_request->id_user 				= $agent->getIdUser();
	$cet_deposit_request->id_agent_right_cet 	= $CetAgentRight->id;
	$cet_deposit_request->id_agent_right_source = $agent_right->id;
	$cet_deposit_request->quantity				= $quantity;
	$cet_deposit_request->idfai					= 0;
	$cet_deposit_request->comment				= $cet['comment'];
	$cet_deposit_request->status				= 'Y';
	
	$cet_deposit_request->save();
	

	if ($cet_deposit_request->createApprobationInstance())
	{
		$babBody->addNextPageMessage(absences_translate('Your time saving account deposit has been sent for approval'));
	} else {
		$babBody->addNextPageMessage(absences_translate('Your time saving account deposit has been saved without approval'));
	}
	
	
	$defer = (bool) absences_getVacationOption('approb_email_defer');
	
	if (!$defer)
	{
		require_once dirname(__FILE__).'/utilit/request.notify.php';
		absences_notifyRequestApprovers();
	}
	
	absences_cetGotoList();
}



function absences_createCetDepositRequest()
{
	require_once dirname(__FILE__).'/utilit/cet_deposit_request.class.php';
	require_once dirname(__FILE__).'/utilit/cet_deposit_request.ui.php';
	
	$agent = absences_Agent::getCurrentUser();
	
	$id = bab_rp('id', null);
	$request = null;
	
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	if (isset($id))
	{
		$request = absences_CetDepositRequest::getById($id);
		if (!$request->canModify())
		{
		    $page->addError(absences_translate('This request is not modifiable'));
		    $page->displayHtml();
			return;
		}
	} elseif (bab_rp('id_user')) {
	    
	    $request = new absences_CetDepositRequest();
	    $request->id_user = bab_rp('id_user');
	    $request->status = '';
	    $request->quantity = 0;
	    
	    $agent = absences_Agent::getFromIdUser($request->id_user);
	    
	    try {
    	    if (!$agent->Cet()->canAdd()) {
    	        $page->addError(sprintf(absences_translate('%s is not allowed to use time saving account'), $agent->getName()));
    	        $page->displayHtml();
    	        return;
    	    }
	    } catch (Exception $e) {
	        $page->addError($e->getMessage());
	        $page->displayHtml();
	        return;
	    }
	}
	
	
	
	
	
	

	
	if (isset($_POST['cet']))
	{
		if ( isset($_POST['cet']['cancel']) )
		{
			absences_cetGotoList();
		}
	
	
		if( isset($_POST['cet']['save'] ))
		{
			try {
				absences_saveCetDepositRequest($_POST['cet'], $request);
			} catch (Exception $e)
			{
				$page->addError($e->getMessage());
			}
		}
	}
	
	if (isset($request) && $request->id)
	{
		$page->setTitle(absences_translate('Edit the time saving account deposit'));
	} else {
		$page->setTitle(absences_translate('Request a deposit in my time saving account'));
	}

	if (absences_lockedForMainteance())
	{
		$page->addError(absences_getMaintenanceMessage());

	} else {
	
		$editor = new absences_CetDepositRequestEditor($request, isset($_REQUEST['id_user']));
		$page->addItem($editor);
	}
	
	$page->displayHtml();
}







/**
 * Display a menu to create vacation request, CET request, workingdays recovery request
 * @return string
 */
function absences_requestMenu()
{
	$toolbar = absences_getToolbar();
	$agent = absences_Agent::getCurrentUser();
	$addon = absences_addon();
	
	$sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';
	
	
	
	$toolbar->addToolbarItem(
			new BAB_ToolbarItem(absences_translate('Request a vacation'), $addon->getUrl().'vacuser&idx=period',
					$sImgPath . 'edit_add.png', '', '', '')
	);
	
	if ($agent->canCreateWorkperiodRecoverRequest()) {
		$toolbar->addToolbarItem(
				new BAB_ToolbarItem(absences_translate("Recovery request"), $addon->getUrl().'vacuser&idx=workperiod',
						$sImgPath . 'edit_add.png', '', '', '')
		);
	}
	
	try {
    	if ($agent->Cet()->canAdd()) {
    		$toolbar->addToolbarItem(
    				new BAB_ToolbarItem(absences_translate("Deposit on my time saving account"), $addon->getUrl().'vacuser&idx=cet',
    						$sImgPath . 'edit_add.png', '', '', '')
    		);
    	}
	} catch (Exception $e) {
	    bab_debug($e->getMessage());
	}
	return $toolbar->printTemplate();
}



function absences_personalMovements()
{
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	require_once dirname(__FILE__).'/utilit/agent.ui.php';
	
	$babBody = bab_getBody();
	$agent = absences_Agent::getCurrentUser();
	
	
	$list = new absences_AgentMovementList($agent);
	
	$babBody->setTitle(absences_translate('My history'));
	
	$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
	$babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacuser&idx=movement");
	
	if (absences_getVacationOption('user_add_email'))
	{
		$babBody->addItemMenu("options", absences_translate("Options"), absences_addon()->getUrl()."vacuser&idx=options");
	}

	$babBody->babEcho($list->getHtml());
}




/**
 * 
 */
function absences_personalOptions()
{
	global $babBody;
	
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	require_once dirname(__FILE__).'/utilit/agent.ui.php';
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
	$agent = absences_Agent::getCurrentUser();
	
	
	
	
	
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	if ($options = bab_pp('options'))
	{
		$emails = $options['emails'];
		$agent->setEmails($emails);
		
		$babBody->addNextPageMessage(absences_translate('The emails were saved'));
		
		$url = bab_url::get_request('tg', 'idx');
		$url->location();
	}
	
	
	
	$page->setTitle(absences_translate('My options'));
	
	$page->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
	
	if (absences_getVacationOption('display_personal_history'))
	{
		$page->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacuser&idx=movement");
	}
	
	$page->addItemMenu("options", absences_translate("Options"), absences_addon()->getUrl()."vacuser&idx=options");
	
	
	$editor = new absences_PersonalOptionsEditor;
	$page->addItem($editor);
	
	
	$page->displayHtml();
}






function absences_personalRights_addRightInTable(Widget_TableView $table, $row, absences_Right $right, absences_AgentRight $agentRight)
{
    $W = bab_Widgets();
    $type = $right->getType();
    
    $icon = $W->Frame();
    $icon->setCanvasOptions($icon->Options()->width(10,'px')->height(10,'px')->backgroundColor('#'.$type->color));
    $icon->setTitle($type->name);

    $col = 0;
    
    $table->addItem($W->HBoxItems($icon,$W->Label($right->description))
        ->setHorizontalSpacing(.8,'em')->setVerticalAlign('middle')									, $row, $col++);
    $table->addItem($W->Label(absences_quantity($agentRight->getQuantity(), $right->quantity_unit))				, $row, $col++);
    $table->addItem($W->Label(absences_quantity($agentRight->getConfirmedQuantity(), $right->quantity_unit))	, $row, $col++);
    $table->addItem($W->Label(absences_quantity($agentRight->getWaitingQuantity(), $right->quantity_unit))		, $row, $col++);
    $balance = $agentRight->getQuantity() - $agentRight->getConfirmedQuantity() - $agentRight->getWaitingQuantity();
    $table->addItem($W->Label(absences_quantity($balance, $right->quantity_unit))								, $row, $col++);
    $previsional = $agentRight->getPrevisionalQuantity();
    $table->addItem($W->Label(absences_quantity($previsional, $right->quantity_unit))		                    , $row, $col++);
    $table->addItem($W->Label(absences_quantity($balance - $previsional, $right->quantity_unit))		        , $row, $col++);
    $table->addItem($W->Label(bab_shortDate(bab_mktime($right->date_begin), false))								, $row, $col++);
    $table->addItem($W->Label(bab_shortDate(bab_mktime($right->date_end), false))								, $row, $col++);
}



function absences_personalRights()
{
	$W = bab_Widgets();
	$agent = absences_Agent::getCurrentUser();
	
	if (!$agent->exists())
	{
		return;
	}
	
	$page = $W->BabPage()->setEmbedded(false);
	$page->setTitle(absences_translate('My vacations rights'));
	
	$table = $W->BabTableView();
	
	$row = 0;
	$col = 0;
	$table->addHeadRow(0);
	
	$table->addItem($W->Label(absences_translate('Description'))	, $row, $col++);
	$table->addItem($W->Label(absences_translate('Rights'))			, $row, $col++);
	$table->addItem($W->Label(absences_translate('Consumed'))		, $row, $col++);
	$table->addItem($W->Label(absences_translate('Waiting'))		, $row, $col++);
	$table->addItem($W->Label(absences_translate('Balance'))		, $row, $col++);
	$table->addItem($W->Label(absences_translate('Previsional'))	, $row, $col++);
	$table->addItem($W->Label(absences_translate('Previ. Bal.'))	, $row, $col++);
	$table->addItem($W->Label(absences_translate('Begin date'))		, $row, $col++);
	$table->addItem($W->Label(absences_translate('End date'))		, $row, $col++);
	$row++;	
	
	$I = $agent->getAgentRightUserIterator();
	
	$rows = array();
	
	foreach($I as $agentRight)
	{
		
	    $right = $agentRight->getRight();
		$rgroup = $right->getRgroupLabel();


		if ($rgroup)
		{
			if (!isset($rows['rgroup'.$right->id_rgroup])) {
    			$rows['rgroup'.$right->id_rgroup] = array(
    			    'sortkey' => $right->getRgroupSortkey(),
    			    'rgroup' => array()
    			);
			}
			
			$rows['rgroup'.$right->id_rgroup]['rgroup'][] = $agentRight;
			continue;
		}
		
		$rows['right'.$right->id] = array(
		    'sortkey' => $right->sortkey,
		    'agentRight' => $agentRight
		);		
	}
	
	bab_Sort::asort($rows, 'sortkey');
	
	foreach($rows as $arr_row) {
	    
	    if (isset($arr_row['rgroup'])) {
	        $rgroup = reset($arr_row['rgroup'])->getRight()->getRgroupLabel();
	        $table->addItem($W->Label($rgroup)->addClass('widget-strong'), $row, 0, -1, 9);
	        $row++;
	        
	        foreach($arr_row['rgroup'] as $agentRight) {
	            $right = $agentRight->getRight();
	            $right->description = bab_nbsp().bab_nbsp().bab_nbsp().bab_nbsp().$right->description;
	            absences_personalRights_addRightInTable($table, $row, $right, $agentRight);
	            $row++;
	        }
	        
	        continue;
	    }
	    
	    
	    $agentRight = $arr_row['agentRight'];
	    $right = $agentRight->getRight();
	    
	    absences_personalRights_addRightInTable($table, $row, $right, $agentRight);
	    $row++;
	}
	
	
	
	
	$page->addItem($table);
	$print = $W->Link($W->Icon(absences_translate('Print'), Func_Icons::ACTIONS_DOCUMENT_PRINT), 'javascript:window.print()');
	$page->addItem($W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addClass('widget-align-center')->addItem($print));
	
	$page->displayHtml();
}









/**
 * @param string $date
 * @return array
 */
function absences_getDayWorkingHours($date)
{
    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
    
    $id_user = bab_gp('id_user');
    $agent = absences_Agent::getFromIdUser(bab_getUserId());
    
    if ($id_user != $agent->getIdUser() && !absences_IsUserUnderSuperior($id_user) && !$agent->isManager()) {
        throw new Exception('Access denied');
    }
    
    $wh = bab_functionality::get('WorkingHours');
    /* @var $wh Func_WorkingHours */
    
    $begin = BAB_DateTime::fromIsoDateTime(bab_gp('date').' 00:00:00');
    $end = clone $begin;
    $end->add(1, BAB_DATETIME_DAY);
    
    return $wh->selectPeriods($id_user, $begin, $end);
}








/**
 * Create or edit a request period 
 * first step of a vacation request
 */
function absences_requestPeriod()
{
	include_once dirname(__FILE__)."/utilit/planningincl.php";
	global $babBody;
	$agent = absences_Agent::getCurrentUser();
	
	
	
	
	
	
	if (!empty($_REQUEST['id']))
	{
		// request modification
		
		
		if (bab_rp('rfrom'))
		{
			if ($agent->isManager() && !bab_rp('ide'))
			{
				// request modification from a manager
			
				$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
				$babBody->addItemMenu("lper", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
				$babBody->addItemMenu("period", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=period");
			}
			
			if ($agent->isEntityManager() && bab_rp('ide'))
			{
				// request modification from a delegated manager
			
				$babBody->addItemMenu("entities", absences_translate("Delegated management"), absences_addon()->getUrl()."vacchart&idx=entities");
				$babBody->addItemMenu("entity_members", absences_translate("Entity requests"), absences_addon()->getUrl()."vacchart&idx=entity_requests&ide=".bab_rp('ide'));
				$babBody->addItemMenu("period", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=period&ide=".bab_rp('ide'));
			}
			
			
		} else {
			
			// request modification from agent
		
			$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
			$babBody->addItemMenu("period", absences_translate("Edit"), absences_addon()->getUrl()."vacuser&idx=period&id=".bab_rp('id'));
			
			if( $agent->isManager())
			{
				$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
			}
			if ($agent->isEntityManager())
			{
				$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
			}
		
		
		}
		
		
		$babBody->setTitle(absences_translate("Edit vacation request"));
		
		if (absences_lockedForMainteance())
		{
			$babBody->addError(absences_getMaintenanceMessage());
			return false;
		}
		
		
		$id_user = bab_isEntryEditable($_REQUEST['id']);
		if (!$id_user)
		{
			$babBody->addError(absences_translate("Access denied, this request is not modifiable"));
			return false;
		}
		else
		{
			absences_viewVacationCalendar(array($id_user), true, true, bab_rp('nbmonth', 12), false);
			period($id_user, $_REQUEST['id']);
			return true;
		}
	}
	else
	{
		
		// request creation
		
		if (isset($_GET['idu']) && is_numeric($_GET['idu'])) {
			$id_user = $_GET['idu']; // deprecated?
		}
		else {
			$id_user = bab_rp('id_user', bab_getUserId());
			
			if (bab_rp('rfrom'))
			{
			
				if ($agent->isManager() && !bab_rp('ide'))
				{
					// request creation from a manager
					
					$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
					$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
					$babBody->addItemMenu("period", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=period");
				}
				
				if ($agent->isEntityManager() && bab_rp('ide'))
				{
					// request creation from a delegated manager
					
					$babBody->addItemMenu("entities", absences_translate("Delegated management"), absences_addon()->getUrl()."vacchart&idx=entities");
					$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".bab_rp('ide'));
					$babBody->addItemMenu("period", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=period");
				}
			
			} else {
				
				// request creation from agent
				
				$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
				$babBody->addItemMenu("period", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=period");
				
				
				if( $agent->isManager())
				{
					$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
				}
				if ($agent->isEntityManager())
				{
					$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
				}
				
			}
		}
		
		
		if ($id_user == bab_getUserId())
		{
			$babBody->setTitle(absences_translate("Request vacation"));
		} else {
			$babBody->setTitle(absences_translate("Request vacation for another user")); // rfrom=1
		}
		
		if (absences_lockedForMainteance())
		{
			$babBody->addError(absences_getMaintenanceMessage());
			return false;
		}
		
	
		if (bab_vacRequestCreate($id_user)) {
			absences_viewVacationCalendar(array($id_user), true, true, bab_rp('nbmonth', 12), false);
			period($id_user);
			return true;
		}
	}
	
	$babBody->addError(absences_translate("Access denied, no access to create a request"));
	return false;
}





/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
$idx = bab_rp('idx', "lvreq");


if(!$agent->isInPersonnel() && !$agent->isEntityManager() && !$agent->isManager())
	{
	$babBody->msgerror = absences_translate("Access denied");
	return;
	}



if (isset($_POST['action']))
{
switch ($_POST['action'])
	{
	case 'period':
		if (!test_period()) {
			$idx = 'period';
		}
		break;

	case 'vacation_request':
		
		$id = bab_pp('id');
		
		if (bab_isEntryEditable($id))
		{
		if(!absences_saveVacation::save()) {
			$idx = "vunew";
			}
		elseif ($_POST['id_user'] == bab_getUserId())
			{
			// demande pour moi meme, retour a la liste de mes demandes
			header("Location: ". absences_addon()->getUrl()."vacuser&idx=lvreq");
			exit;
			}
		elseif ($id)
			{
			// modification d'une demande, retour liste des demandes	
			if (bab_pp('ide')) {
				header("Location: ". absences_addon()->getUrl().'vacchart&idx=entity_requests&ide='.bab_pp('ide'));
				exit;
			}

			header("Location: ". absences_addon()->getUrl()."vacadmb&idx=lreq");
			exit;
			}
		else 
			{
			// creation d'une demande, retour a la liste des agents
			if (bab_pp('ide')) {
				header("Location: ". absences_addon()->getUrl().'vacchart&idx=entity_members&ide='.bab_pp('ide'));
				exit;
			}
			
			header("Location: ". absences_addon()->getUrl()."vacadm&idx=lper");
			exit;
			}
		}
		break;

	case 'delete_request':
		
		if ($id_entry = bab_pp('id_entry'))
		{
			$id_user = bab_isEntryEditable($id_entry);
			if ($id_user || $agent->isManager())
			{
				if (absences_delete_request(bab_pp('id_entry'), bab_pp('folder', 0), (int) bab_pp('rfrom', 0)))
				{
					header("Location: ". bab_pp('url'));
					exit;
				} else {
					$babBody->addError(absences_translate('Failed to delete the vacation request'));
				}
			}
		}
		
		
		if ($id_deposit = bab_pp('id_deposit'))
		{
			require_once dirname(__FILE__).'/utilit/cet_deposit_request.class.php';
			$deposit = absences_CetDepositRequest::getById($id_deposit);
			if ($deposit->canDelete())
			{
				$deposit->delete();
				$url = new bab_url(bab_pp('url'));
				$url->location();
			}
		}
		
		
		if ($id_recovery = bab_pp('id_recovery'))
		{
			require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
			$recovery = absences_WorkperiodRecoverRequest::getById($id_recovery);
			if ($recovery->canDelete())
			{
				$recovery->delete();
				$url = new bab_url(bab_pp('url'));
				$url->location();
			}
		}
		
		break;
	}
}





switch($idx)
	{

	case "unload":
		vedUnload();
		exit;
		break;

	case "morve":
		require_once dirname(__FILE__).'/utilit/request.ui.php';
		$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		$babBody->addItemMenu("morve", absences_translate("View request"), absences_addon()->getUrl()."vacuser&idx=lvreq&id=".bab_rp('id'));
		absences_viewVacationRequestDetail(bab_rp('id'));
		break;
		
	case 'view_cet_deposit':
		require_once dirname(__FILE__).'/utilit/request.ui.php';
		$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		$babBody->addItemMenu("view_cet_deposit", absences_translate("View request"), absences_addon()->getUrl()."vacuser&idx=view_cet_deposit&id=".bab_rp('id'));
		absences_viewCetDepositDetail(bab_rp('id'));
		break;

	case 'view_wp_recovery':
		require_once dirname(__FILE__).'/utilit/request.ui.php';
		$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		$babBody->addItemMenu("view_wp_recovery", absences_translate("View request"), absences_addon()->getUrl()."vacuser&idx=view_wp_recovery&id=".bab_rp('id'));
		absences_viewWpRecoveryDetail(bab_rp('id'));
		break;
		
	case "period":
		// demande, premiere etape
		absences_requestPeriod();
		break;
		
		
	case 'recurring':
		if( bab_isEntryEditable($_POST['id']) )
		{
			absences_recurringVacation(absences_PostedDate::begin(), absences_PostedDate::end(), $_POST['id']);
			
			$babBody->addItemMenu("recurring", absences_translate("Request"), absences_addon()->getUrl()."vacuser&idx=vunew");
			$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		}
		if($agent->isManager())
			$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
		if ($agent->isEntityManager())
			$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
		break;

	case "vunew":
		// demande, deuxieme etape
		requestVacation(absences_PostedDate::begin(), absences_PostedDate::end(), $_POST['id'], bab_pp('rfrom'), bab_pp('ide'));
		break;

	case 'delete':
		
		$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		
		if ($id_entry = bab_rp('id_entry'))
		{
			$babBody->addItemMenu("delete", absences_translate("Delete request"), absences_addon()->getUrl()."vacuser&idx=delete&id_entry=".$id_entry);
			absences_deleteVacationRequest($id_entry);
		}
		
		if ($id_cetdeposit = bab_rp('id_cetdeposit'))
		{
			$babBody->addItemMenu("delete", absences_translate("Delete request"), absences_addon()->getUrl()."vacuser&idx=delete&id_cetdeposit=".$id_cetdeposit);
			absences_deleteCetDepositRequest($id_cetdeposit);
		}
		
		if ($id_recovery = bab_rp('id_recovery'))
		{
			$babBody->addItemMenu("delete", absences_translate("Delete request"), absences_addon()->getUrl()."vacuser&idx=delete&id_recovery=".$id_recovery);
			absences_deleteWpRecoveryRequest($id_recovery);
		}
		
		break;

	case 'viewrights':
		if (absences_IsUserUnderSuperior($_GET['id_user']) || $agent->isManager()) {
			$babBody->setTitle(absences_translate("Balance").' : '.bab_getUserName($_GET['id_user']));

			if (isset($_GET['ide'])) {
				$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_GET['ide']);
			}

			$babBody->addItemMenu("viewrights", absences_translate("Balance"), absences_addon()->getUrl()."vacuser&idx=lvreq");
			viewrights($_GET['id_user']);
		} else {
			$babBody->msgerror = absences_translate("Access denied");
		}
		break;

		
	case 'subprev':
		$id_entry = (int) bab_rp('id_entry');
		if ($id_entry && bab_isEntryEditable($id_entry))
		{
			absences_saveVacation::submitRequest($id_entry);
			absences_saveVacation::gotoList();
		} else {
			$babBody->msgerror = absences_translate("Access denied to request modification");
		}
			
		break;
		
	case 'workperiod':
		absences_createWorkperiodRecoverRequest();
		
		
		if (!bab_rp('id_user')) {
		  $babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		}
		
		$babBody->addItemMenu("workperiod", absences_translate("Working day"), absences_addon()->getUrl()."vacuser&idx=workperiod");
		
		if( $agent->isManager())
			$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
		
		if ($agent->isEntityManager())
			$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
		break;
		
	case 'cet':
		absences_createCetDepositRequest();
		
		if (!bab_rp('id_user')) {
		    $babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
		}
		
		$babBody->addItemMenu("cet", absences_translate("Time saving account"), absences_addon()->getUrl()."vacuser&idx=cet");
		
		if( $agent->isManager())
			$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
		
		if ($agent->isEntityManager())
			$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
		break;
		
	case 'movement':
		
		absences_personalMovements();
		break;
		
	case 'options':
		absences_personalOptions();
		break;
		
	case 'myrights':
		absences_personalRights();
		die();
		break;
		
		
	case 'workinghours':
	    require_once $GLOBALS['babInstallPath'].'utilit/json.php';
	    echo bab_json_encode(absences_getDayWorkingHours(bab_gp('date')));
	    die();

	case 'clear':
		$babDB->db_query("TRUNCATE ".ABSENCES_CALENDAR_TBL);

	case "lvreq":
	default:
		$babBody->title = absences_translate("My already filed requests");
		if( $agent->isInPersonnel() )
		{
			$babBody->babEcho(absences_requestMenu());
			absences_listVacationRequests(bab_getUserId(), false);
			$babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
			if (absences_getVacationOption('display_personal_history'))
			{
				$babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacuser&idx=movement");
			}
			
			if (absences_getVacationOption('user_add_email'))
			{
				$babBody->addItemMenu("options", absences_translate("Options"), absences_addon()->getUrl()."vacuser&idx=options");
			}
			
		} else {
			
			if ($agent->isManager() && 'lvreq' === $idx)
			{
				// manager only
				$url = new bab_url;
				$url->tg = 'addon/absences/vacadm';
				$url->location();
			}
			
			if ($agent->isEntityManager() && 'lvreq' === $idx)
			{
				// delegated manager only
				$url = new bab_url;
				$url->tg = 'addon/absences/vacchart';
				$url->location();
			}
		}
			
		if( $agent->isManager())
		{
			$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
		}

		if ($agent->isEntityManager())
		{
			$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
		}
		break;
	}

if ( absences_isPlanningAccessValid())
{
	$babBody->addItemMenu("planning", absences_translate("Plannings"), absences_addon()->getUrl()."planning&idx=userlist");
}




$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');

