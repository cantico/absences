<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';
require_once dirname(__FILE__).'/utilit/entry.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
bab_Functionality::includeoriginal('PortletBackend');
bab_Functionality::includeoriginal('Icons');

/**
 * 
 */
class Func_PortletBackend_Absences extends Func_PortletBackend
{
	public function getDescription()
	{
		return absences_translate("Absences");
	}
	
	public function select($category = null)
	{
		if (isset($category) && $category !== 'absences')
		{
			return array();
		}
		
		$portlets = array(
		    'absences_forthcoming' => $this->Portlet_forthcoming()
		);
		
		if (bab_isAccessValid('absences_public_planning_groups', 1))
		{
		    $portlets['absences_publicPlanning'] = $this->Portlet_publicPlanning();
		}
		
		return $portlets;
	}
	
	
	
	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
		$name = str_replace('absences_', '', $portletId);
		
		switch($name)
		{
			case 'forthcoming':
				return $this->Portlet_forthcoming();
				
			case 'publicPlanning':
				return $this->Portlet_publicPlanning();
		}
		
		return null;
	}


	public function Portlet_forthcoming()
	{
		return new PortletDefinition_forthcoming();
	}
	
	public function Portlet_publicPlanning()
	{
		return new absences_PublicPlanningDefinition();
	}
	
	
	/**
	 * get a list of categories supported by the backend
	 * @return Array 
	 */
	public function getCategories()
	{

		return array(
			'absences' => absences_translate('Absences')
		);
	}
}


/////////////////////////////////////////




class absences_PublicPlanningDefinition implements portlet_PortletDefinitionInterface
{

	private $id;
	private $name;
	private $description;

	public function __construct()
	{
		$this->id = 'absences_publicPlanning';
		$this->name = absences_translate('Absences planning');
		$this->description = absences_translate('Public planning for absences');
		$this->addon = bab_getAddonInfosInstance('absences');
	}

	public function getId()
	{
		return $this->id;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;
	}


	public function getPortlet()
	{
		return new Portlet_publicPlanning;
	}

	/**
	 * Returns the widget rich icon URL.
	 *
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getImagesPath() . 'icon.png';
	}


	/**
	 * Returns the widget icon URL.
	 *
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getImagesPath() . 'icon.png';
	}

	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return null;
	}

	public function getConfigurationActions()
	{
		return array();
	}

	public function getPreferenceFields()
	{
		return array(
			array(
					'label' => absences_translate('Start date shifted in months'),
					'type' => 'int',
					'name' => 'shiftmonth',
					'default' => 0
			),
				
			array(
					'label' => absences_translate('Display the legend'),
					'type' => 'boolean',
					'name' => 'legend',
					'default' => true
			)
		);
	}
}







class Portlet_publicPlanning extends Widget_Item implements portlet_PortletInterface
{

	private $options = array();



	/**
	 * @param Widget_Canvas	$canvas
	 */
	public function display(Widget_Canvas $canvas)
	{
		global $babDB;
		$W = bab_Widgets();
		require_once dirname(__FILE__).'/utilit/planningincl.php';
		
		$nbmonth = (int) bab_rp('nbmonth', 1);
		
		if (!bab_isAccessValid('absences_public_planning_groups', 1))
		{
		    return '';
		}
		
		// preload the 30 first users
		$initusers = absences_getSearchLimit($nbmonth);
		
		$users = array();
		$res = absences_publicCalendarUsers(bab_rp('keyword', null), bab_rp('departments', null), bab_rp('searchtype'), bab_rp('dateb'), bab_rp('datee'), bab_rp('date'));
		
		$i = 0;
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$users[] = $arr['id'];
			if ($i > $initusers)
			{
				break;
			}
		
			$i++;
		}
		
		if (empty($users) && '' === bab_rp('keyword') && '' === bab_rp('dateb') && '' === bab_rp('datee') && '' === bab_rp('date'))
		{
			return $canvas->span(null, array(), array(absences_translate('No employees found, the public planning is empty')));
		}
		
		$shiftmonth = isset($this->options['shiftmonth']) ? $this->options['shiftmonth'] : 0;
		$legend = isset($this->options['legend']) ? (bool) $this->options['legend'] : true;
		
		
		$search = bab_rp('search'); // button clicked
		$month = (int) bab_rp('month');
		$year = (int) bab_rp('year');
		$searchDate = bab_rp('dateb');
		if (empty($searchDate)) {
		    $searchDate = bab_rp('date');
		}
		
		if (!empty($searchDate) && $search) {
		    // try to replace month/year by elements from searchDate
		    list($searchYear, $searchMmonth) = explode('-', $W->DatePicker()->getISODate($searchDate));
		    $searchYear = (int) $searchYear;
		    $searchMmonth = (int) $searchMmonth;
		    
		    if ($searchYear !== $year) {
		        $_REQUEST['year'] = $searchYear;
		    }
		    
		    if ($searchMmonth !== $month) {
		        $_REQUEST['month'] = $searchMmonth;
		    }
		}
		
		
		$cls = new absences_viewVacationCalendarCls($users, false, true, $nbmonth, true, $shiftmonth, $legend);
		$cls->publiccalendar($babDB->db_num_rows($res));

		$widget = $W->Html($cls->getHtml());
		

		return $widget->display($canvas)
			.$canvas->loadScript($widget->getId(), absences_addon()->getTemplatePath().'calendar.jquery.js')
			.$canvas->loadStyleSheet(absences_addon()->getStylePath().'vacation.css');
	}


	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new absences_PublicPlanningDefinition();
	}

	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}

	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}

	public function setPortletId($id)
	{

	}
}








class PortletDefinition_forthcoming implements portlet_PortletDefinitionInterface
{
	
	private $id;
	private $name;
	private $description;
	
	public function __construct()
	{
		$this->id = 'absences_forthcoming';
		$this->name = absences_translate('Forthcomming absences');
		$this->description = absences_translate('Forthcomming absences for users in the same department or for all personel members');
		$this->addon = bab_getAddonInfosInstance('absences');
	}

	public function getId()
	{
		return $this->id; 
	}
	
	
	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;	
	}


	public function getPortlet()
	{		
		return new Portlet_forthcoming;
	}

	/**
	 * Returns the widget rich icon URL.
	 * 
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getImagesPath() . 'icon.png';
	}


	/**
	 * Returns the widget icon URL.
	 * 
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getImagesPath() . 'icon.png';
	}
	
	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return null;
	}
	
	public function getConfigurationActions()
	{
		return array();
	}
	
	public function getPreferenceFields()
	{
		
		
		$pop_options = array(
			array('value' => 'me', 'label' 	=> absences_translate('Me only')),
			array('value' =>'all', 'label' 	=> absences_translate('All users'))
		);
		
		
		try {
			$agent = absences_Agent::getCurrentUser();
			if (null !== $entity = $agent->getMainEntity())
			{
				$pop_options[] = array('value' =>'department', 'label' 	=> $entity['name']);
			}
		
		} catch (Exception $e)
		{
			// user not logged in
		}
		
		
		
		return array(
			array(
				'label' => absences_translate('Number of days in future'), 
				'type' => 'int', 
				'name' => 'days'
			),
			array(
				'label' => absences_translate('Display absences for'), 
				'type' => 'list', 
				'name' => 'population', 
				'options' => $pop_options
			),
			array(
				'label' => absences_translate('Long dates format'),
				'type' => 'boolean',
				'name' => 'longdates'		
			),
			array(
				'label' => absences_translate('Display request status'),
				'type' => 'boolean',
				'name' => 'viewstatus'		
			)
		);
	}
}




class Portlet_forthcoming extends Widget_Item implements portlet_PortletInterface
{

	private $options = array();
	

	
	/**
	 * @return array
	 */
	private function getDepartmentMembers()
	{
		global $babDB;
		
		try {
			$agent = absences_Agent::getCurrentUser();
			$entity = $agent->getMainEntity();
		
		} catch(Exception $e)
		{
			return null;
		}
		
		if (null === $entity)
		{
			return null;
		}
		
		$members = array();
		$res = bab_OCSelectEntityCollaborators($entity['id'], true);
		
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			if (isset($arr['id_user']))
			{
				$members[] = (int) $arr['id_user'];
			} else {
				trigger_error('Missing id_user key, need ovidentia 8.0.97 version at least');
				return null;
			}
		}
		
		return $members;
	}
		

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = bab_Widgets();
		
		
		
		
		$population = empty($this->options['population']) ? 'me' : $this->options['population'];
		$days = empty($this->options['days']) ? 7 : $this->options['days'];
		
		$to = BAB_DateTime::now();
		$to->add($days, BAB_DATETIME_DAY);
		
		
		$users = null;
		
		if ($population === 'department')
		{
			$users = $this->getDepartmentMembers();
			if (null === $users)
			{
				$population = 'me';
			}
		}
		
		
		if ($population === 'me')
		{
			$users = array(bab_getUserId());
		}
		
		
		$I = new absences_EntryIterator();
		$I->users = $users;
		$I->from = date('Y-m-d H:i:s');
		$I->to = $to->getIsoDateTime();
		$I->status = array('', 'Y');
		
		$I->orderby = 'date_begin ASC';
		
		$list = $W->VBoxLayout()->addClass(Func_Icons::ICON_LEFT_32);

		foreach($I as $entry)
		{
			$list->addItem($this->getEntryWidget($entry));
		}
		
		return '
		<style type="text/css" scoped>
			.icon-left-32 .icon {
			    min-height: 15px;
			    min-width: 0;
			    padding: 1em 8px 1em 36px;
			}
			
			.widget-small {
				color:rgba(0,0,0,.5);
			}
			
			.absences-portlet-forthcoming-entry {
				border-bottom:rgba(0,0,0,.19) 2px dotted;
				padding-bottom:.8em;
				margin-bottom:.8em;
			}
		</style>
		'.
		$list->display($canvas);
	}
	
	/**
	 * @return string|false
	 */
	private function getPhoto($id_user)
	{
		$T = bab_functionality::get('Thumbnailer');
		/*@var $T Func_Thumbnailer */
		
		if (false === $T)
		{
			return false;
		}
		
		
		$direntry = bab_getDirEntry($id_user, BAB_DIR_ENTRY_ID_USER);
		if (!$direntry || !isset($direntry['jpegphoto']['photo']))
		{
			return false;
		}
		
		$photo = $direntry['jpegphoto']['photo'];
		$data = $photo->getData();
		
		if (null !== $data && '' !== $data)
		{
			$T->setSourceBinary($data, $photo->lastUpdate());
			$T->setResizeMode(Func_Thumbnailer::CROP_CENTER);
			return $T->getThumbnail(32, 32);
		}
		
		return false;
	}
	
	
	/**
	 * @return Widget_DisplayableInterface
	 */
	private function getEntryWidget(absences_Entry $entry)
	{
		$W = bab_Widgets();

		$icon = $W->Icon(null, Func_Icons::OBJECTS_USER);
		if ($photo = $this->getPhoto($entry->id_user))
		{
			$icon->setImageUrl($photo);
		}
			
		
		
		$url = bab_getUserDirEntryLink($entry->id_user, BAB_DIR_ENTRY_ID_USER);
		if (false === $url)
		{
			$user = $W->Label(bab_getUserName($entry->id_user));
		} else {
			$user = $W->Link(bab_getUserName($entry->id_user), $url)->setOpenMode(Widget_Link::OPEN_POPUP);
		}
		
		$datefunc = !empty($this->options['longdates']) ? 'bab_longDate' : 'bab_shortDate';
		$period = $W->Label(absences_DateTimePeriod($entry->date_begin, $entry->date_end, $datefunc));
		
		$status = null;
		if (!empty($this->options['viewstatus']))
		{
			$status = $W->Label($entry->getStatusStr())->addClass('widget-small');
		}
		
		$layout = $W->HBoxItems($icon, $W->VBoxItems($user, $period, $status))->setVerticalAlign('top');
		
		return $W->Frame(null, $layout)->addClass('absences-portlet-forthcoming-entry');
	}
	

	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new PortletDefinition_forthcoming();
	}
	
	/**
	 * Get the title defined in preferences or a default title for the widget frame
	 * @return string
	 */
	public function getPreferenceTitle()
	{
		if (!empty($this->options['_blockTitle']))
		{
			return $this->options['_blockTitle'];
		}
		
		$days = empty($this->options['days']) ? 7 : $this->options['days'];
		
		return sprintf(absences_translate('Absences in the next %d days'), $days);
	}
	
	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}
	
	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}
	
	public function setPortletId($id)
	{
		
	}
}

