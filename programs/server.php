<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/utilit/sync_items.class.php';

$oAuthObject = bab_functionality::get('PortalAuthentication/AuthBasic');
/*@var $oAuthObject Func_PortalAuthentication_AuthBasic */

if (false === $oAuthObject)
{
	die('Failed to load AuthBasic functionality');
}

if (!$oAuthObject->isLogged())
{
	$oAuthObject->login();
}


$sync_items = new absences_sync_items;

$server_date = bab_mktime($sync_items->getLastModifiedDate());
$lastmodified = gmdate("D, d M Y H:i:s", $server_date) . " GMT";
header("Last-Modified: $lastmodified");



if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
{
	$client_date = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
	if ($server_date <= $client_date)
	{
		header("HTTP/1.1 304 Not Modified");
		exit;
	}
}


// send data

$data = $sync_items->getData();


$output = serialize($data);
$length = strlen($output);

header("Content-Type: text/plain; charset=".bab_Charset::getIso());
header("Content-Length: " .$length );
header('Content-Disposition: attachment; filename="server"');
// header("Content-Transfer-Encoding: binary\n");

echo $output;