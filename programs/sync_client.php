<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/right.class.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';
require_once dirname(__FILE__).'/utilit/sync.ui.php';
require_once dirname(__FILE__).'/utilit/client.class.php';

/**
 * 
 * @param array $values
 */
function absences_syncClientSave($values)
{
	global $babDB;
	
	
	$babBody = bab_getInstance('babBody');
	/*@var $babBody babBody */
	
	$client = new absences_client;
	$checked_uuid = array();
	
	
	
	foreach($values['right'] as $uuid => $checked)
	{
		if ($checked)
		{
			
			$checked_uuid[] = $uuid;
			
			$right = absences_Right::getByUuid($uuid);
			
			if (!$right->getRow())
			{
				// the right does not exists in client database
				$new_right = $client->addRight($uuid);
				$babBody->addNextPageMessage(sprintf(absences_translate('The right %s has been created'), $new_right->description));
			
			} else {
				// the right exists, update sync status
				$client->updateRight($uuid);
				$babBody->addNextPageMessage(sprintf(absences_translate('The right %s has been updated'), $right->description));
			}
		}
	}
	
	$babDB->db_query('UPDATE absences_rights SET sync_status='.$babDB->quote(0).' WHERE uuid NOT IN('.$babDB->quote($checked_uuid).')');
	
	
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
	$url = bab_url::get_request('tg');
	$url->location();
	
}





function absences_syncClientEdit()
{
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	if (isset($_POST['sync_client']))
	{
		
		if( isset($_POST['sync_client']['save'] ))
		{
			
			// modification uniquement
	
			$values = $_POST['sync_client'];
			absences_syncClientSave($values);
		}
	}
	
	$page->setTitle(absences_translate('Add vacation rights'));
	try {
		$editor = new absences_SyncClientEditor();
		$page->addItem($editor);
	} catch(Exception $e)
	{
		$page->addError($e->getMessage());
	}
	
	$page->displayHtml();
}





/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");

$idx = bab_rp('idx', "edit");


switch($idx)
{
	case 'edit':
		absences_syncClientEdit();
		$babBody->addItemMenu("edit", absences_translate("Edit"), absences_addon()->getUrl()."sync_client&idx=edit");
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');