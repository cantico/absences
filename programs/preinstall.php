<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

$prerequisits = array();
$LibCaldav = bab_getAddonInfosInstance('LibCaldav');

if ($LibCaldav && $LibCaldav->isInstalled())
{
	$prerequisits[] = array(
	
			'description' 		=> 'module LibCaldav',
			'required' 			=> '0.4.6',
			'recommended' 		=> false,
			'current'			=> $LibCaldav->getIniVersion(),
			'result'			=> $LibCaldav->isInstalled() && version_compare($LibCaldav->getIniVersion(), '0.4.6', '>=' )
	
	);
}


$LibZendFramework = bab_getAddonInfosInstance('LibZendFramework');

if ($LibZendFramework && $LibZendFramework->isInstalled())
{
	$prerequisits[] = array(

			'description' 		=> 'module LibZendFramework',
			'required' 			=> '1.12.0rc3.01',
			'recommended' 		=> false,
			'current'			=> $LibZendFramework->getIniVersion(),
			'result'			=> version_compare($LibZendFramework->getIniVersion(), '1.12.0rc3.01', '>=' )

	);
}


$workschedules = bab_getAddonInfosInstance('workschedules');

if ($workschedules && $workschedules->isInstalled())
{
    $prerequisits[] = array(

        'description' 		=> 'module Workschedules',
        'required' 			=> '1.5',
        'recommended' 		=> false,
        'current'			=> $workschedules->getIniVersion(),
        'result'			=> version_compare($workschedules->getIniVersion(), '1.5', '>=' )

    );
}


	
return $prerequisits;