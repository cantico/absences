<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once $babInstallPath."utilit/afincl.php";
include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/entry.class.php';


function absences_listVacationRequestsb()
{
	global $babBody;

	class temp extends absences_Paginate 
		{
		var $nametxt;
		var $urlname;
		var $url;
		var $editurl;
		var $begindatetxt;
		var $enddatetxt;
		var $quantitytxt;
		var $statustxt;
		var $begindate;
		var $enddate;
		var $quantity;
		var $status;
				
		var $arr = array();
		var $count;
		var $res;

		var $statarr;
		var $total;
		var $checkall;
		var $uncheckall;

		var $usersbrowurl;
		var $datetxt;
		var $filteron;
		var $usertxt;
		var $begintxt;
		var $endtxt;
		var $userval;
		var $userid;
		var $dateb;
		var $datee;
		var $dateburl;
		var $dateeurl;
		var $topurl;
		var $bottomurl;
		var $nexturl;
		var $prevurl;
		var $topname;
		var $bottomname;
		var $nextname;
		var $prevname;
		var $pos;

		var $resettxt;

		var $entryid;
		var $alttxt;
		var $altbg = true;

		function temp()
			{
			
			
			$idstatus = $this->param('idstatus');
			$userid = (int) $this->param('userid');
			$organization = (int) $this->param('organization');
			$dateb = $this->param('dateb');
			$datee = $this->param('datee');
			$vpos = (int) $this->param('vpos', 0);
			$pos = (int) $this->param('pos', 0);
			$archived = (int) bab_rp('archived', 0);
			
			include_once $GLOBALS['babInstallPath']."utilit/urlincl.php";
			
			global $babDB, $babBody;
			
			if ($archived)
			{
				$babBody->setTitle(absences_translate("Archived vacations requests list"));
			} else {
				$babBody->setTitle(absences_translate("Vacations requests list"));
			}
			
			
			
			$this->uncheckall = absences_translate("Uncheck all");
			$this->checkall = absences_translate("Check all");
			$this->nametxt = absences_translate("Fullname");
			$this->begindatetxt = absences_translate("Begin date");
			$this->enddatetxt = absences_translate("End date");
			$this->quantitytxt = absences_translate("Quantity");
			$this->statustxt = absences_translate("Status");
			$this->datetxt = absences_translate("Date")." ( ".absences_translate("dd-mm-yyyy")." )";
			$this->filteron = absences_translate("Filter on");
			$this->usertxt = absences_translate("User");
			$this->begintxt = absences_translate("Begin");
			$this->endtxt = absences_translate("End");
			$this->resettxt = absences_translate("Reset");
			$this->alttxt = absences_translate("Modify");
			$this->t_edit = absences_translate("Modification");
			$this->t_delete = absences_translate("Delete");

			$this->t_first_page = absences_translate("First page");
			$this->t_previous_page = absences_translate("Previous page");
			$this->t_next_page = absences_translate("Next page");
			$this->t_last_page = absences_translate("Last page");

			$this->topurl = "";
			$this->bottomurl = "";
			$this->nexturl = "";
			$this->prevurl = "";

			$this->t_position = '';

			
			$this->statarr = array(absences_translate("Waiting"), absences_translate("Accepted"), absences_translate("Refused"), absences_translate('Previsional'));
			$this->dateb = $dateb;
			$this->datee = $datee;
			$this->idstatus = $idstatus;
			$this->userid = $userid;
			$this->pos = $pos;
			$this->userval = $userid != "" ? bab_toHtml(bab_getUserName($userid)) : "";
			$aaareq = array();
			
			$req = 'absences_entries e, 
			     bab_users u LEFT JOIN absences_personnel p ON p.id_user=u.id WHERE ';
			
			$aaareq[] = 'u.id=e.id_user';
			$aaareq[] = 'e.archived='.$babDB->quote($archived);
			
			if( $idstatus != "" || $userid > 0 || $organization > 0 || $dateb != "" || $datee != "")
				{

				if( $idstatus != "")
					{
					switch($idstatus)
						{
						case 0:
							$aaareq[] = "e.status=''"; break;
						case 1:
							$aaareq[] = "e.status='Y'"; break;
						case 2:
							$aaareq[] = "e.status='N'"; break;
						case 3:
							$aaareq[] = "e.status='P'"; break;
						}
					}

				if( $userid > 0)
					{
					$aaareq[] = "e.id_user='".$babDB->db_escape_string($userid)."'";
					}
					
				if ($organization > 0) {
				    
				    $aaareq[] = "p.id_organization='".$babDB->db_escape_string($organization)."'";
				}

				if( $dateb != "" )
					{
					$ar = explode("-", $dateb);
					$dateb = $ar[2]."-".$ar[1]."-".$ar[0];
					}

				if( $datee != "" )
					{
					$ar = explode("-", $datee);
					$datee = $ar[2]."-".$ar[1]."-".$ar[0];
					}

				if( $datee != "" )
					{
					$aaareq[] = "e.date_begin <= DATE_ADD('".$babDB->db_escape_string($datee)."', INTERVAL 1 DAY)";
					}
				if( $dateb != "" )
					{
					$aaareq[] = "e.date_end >= '".$babDB->db_escape_string($dateb)."'";
					}
				}

			if( sizeof($aaareq) > 0 )
				{
				if( sizeof($aaareq) > 1 )
					$req .= implode(' AND ', $aaareq);
				else
					$req .= $aaareq[0];
				}
				
				
			$orderby = bab_rp('orderby', 'begin');
				
			$url = bab_url::request('tg', 'idx', 'idstatus', 'userid', 'dateb', 'datee', 'vpos', 'orderby', 'archived');
			
			$this->orderby = bab_toHtml($orderby);

			
			switch($orderby) {
			
				case 'begin.asc':
					$this->orderbyname = bab_url::mod($url, 'orderby', 'name');
					$this->orderbybegin	= bab_url::mod($url, 'orderby', 'begin.desc');
					$req .= " ORDER BY e.date_begin ASC, u.lastname, u.firstname";
					break;
					
				case 'begin.desc':
				case 'begin':
					$this->orderbyname = bab_url::mod($url, 'orderby', 'name');
					$this->orderbybegin	= bab_url::mod($url, 'orderby', 'begin.asc');
					$req .= " ORDER BY e.date_begin desc, u.lastname, u.firstname";
					break;
					
				case 'name.desc':
					$this->orderbyname = bab_url::mod($url, 'orderby', 'name.asc');
					$this->orderbybegin	= bab_url::mod($url, 'orderby', 'begin');
					$req .= " ORDER BY u.lastname DESC, u.firstname DESC, e.date desc";
					break;
					
				case 'name.asc':
				case 'name':
					$this->orderbyname = bab_url::mod($url, 'orderby', 'name.desc');
					$this->orderbybegin	= bab_url::mod($url, 'orderby', 'begin');
					$req .= " ORDER BY u.lastname ASC, u.firstname ASC, e.date desc";
					break;
			}
			

			list($total) = $babDB->db_fetch_row($babDB->db_query("select count(*) as total from ".$req));
			$this->paginate($total, ABSENCES_MAX_REQUESTS_LIST);


			if( $total > ABSENCES_MAX_REQUESTS_LIST)
				{
				$req .= " limit ".$pos.",".ABSENCES_MAX_REQUESTS_LIST;
				}
			
				
			bab_debug("select e.*, u.lastname, u.firstname from ".$req);

			$this->res = $babDB->db_query("select e.*, u.lastname, u.firstname from ".$req);
			$this->count = $babDB->db_num_rows($this->res);
			
			
			$this->searchform = $this->getSearchForm();
			}
			
		/**
		 * @return string
		 */
		private function getSearchForm()
		{
			$f = new absences_getRequestSearchForm();
			return $f->getHtmlForm($this->statarr);
		}
			
			
		private function param($name, $default = '')
		{
			if (isset($_REQUEST[$name]))
			{
				$_SESSION['babVacation'][$name] = $_REQUEST[$name];
				return $_REQUEST[$name];
			}
			
			if (isset($_SESSION['babVacation'][$name]))
			{
				return $_SESSION['babVacation'][$name];
			}
			
			return $default;
		}

		public function getnext()
			{
			global $babDB;
			static $i = 0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$arr = $babDB->db_fetch_array($this->res);
				
				$entry = new absences_Entry();
				$entry->setRow($arr);
				
				$this->urlname		= bab_toHtml($arr['lastname'].' '.$arr['firstname']);
				$this->url 			= bab_toHtml(absences_addon()->getUrl()."vacadmb&idx=morvw&id=".$arr['id']);
				$this->editconfirmed = false;
				if ($entry->firstconfirm && '' === $entry->status) {
				    $this->editconfirmed = absences_translate('Modification of an allready confirmed request');
				}
				
				$this->todelete = false;
				if ($entry->todelete) {
				    $this->todelete = absences_translate('Deletion request');
				}
				
				$this->editurl 		= bab_toHtml($entry->getEditUrl(1));
				$url = absences_addon()->getUrl()."vacadmb&idx=lreq";
				$this->urldelete 	= bab_toHtml(absences_addon()->getUrl()."vacadmb&idx=delete&id_entry=".$arr['id']."&from=".urlencode($url));
				$this->quantity		= bab_toHtml(absences_vacEntryQuantity($arr['id']));
				
				$this->begindate	= bab_toHtml(absences_shortDate(bab_mktime($arr['date_begin'])));
				$this->enddate		= bab_toHtml(absences_shortDate(bab_mktime($arr['date_end'])));

				$this->status = $entry->getStatusStr();
				
				$i++;
				return true;
				}
			else
				return false;

			}

		function getnextstatus()
			{
			static $i = 0;
			if( $i < count($this->statarr))
				{
				$this->statusid = $i;
				$this->statusname = bab_toHtml($this->statarr[$i]);
				if( $this->idstatus != "" && $i == $this->idstatus )
					$this->selected = "selected";
				else
					$this->selected = "";
				$i++;
				return true;
				}
			else
				return false;
			}
		}

	$temp = new temp();
	
	/*@var $babBody babBody */

	
	absences_addSpoofButton('entry', absences_translate('Add a vacation request'));
	
	$babBody->addStyleSheet(absences_addon()->getStylePath().'vacation.css');
	$babBody->addJavascriptFile($GLOBALS['babInstallPath'].'scripts/bab_dialog.js');
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadmb.html", "vrequestslist"));
	return $temp->count;
}

function editVacationRequest($vrid)
{
	global $babBody;
	class temp
		{
		var $datebegin;
		var $dateend;
		var $vactype;
		var $addvac;

		var $daybeginid;
		var $monthbeginid;
		var $nbdaystxt;

		var $remark;

		var $res;
		var $count;
		
		var $daybegin;
		var $monthbegin;
		var $yearbegin;
		var $dayend;
		var $monthend;
		var $yearend;
		var $halfdaybegin;
		var $halfdayend;
		var $nbdays;
		var $remarks;

		var $daysel;
		var $monthsel;
		var $yearsel;
		var $halfdaysel;
		var $totaltxt;
		var $totalval;

		var $invaliddate;
		var $invaliddate2;
		var $invalidentry;
		var $invalidentry1;
		var $invalidentry2;
		var $iduser;
		var $deletetxt;

		function temp($id)
			{
			global $babBody, $babDB;
			$this->vrid = $id;
			$this->datebegintxt = absences_translate("Begin date");
			$this->dateendtxt = absences_translate("End date");
			$this->vactype = absences_translate("Vacation type");
			$this->addvac = absences_translate("Update");
			$this->remark = absences_translate("Remarks");
			$this->nbdaystxt = absences_translate("Quantity");
			$this->invaliddate = absences_translate("ERROR: End date must be older");
			$this->invaliddate = str_replace("'", "\'", $this->invaliddate);
			$this->invaliddate = str_replace('"', "'+String.fromCharCode(34)+'",$this->invaliddate);
			$this->invaliddate2 = absences_translate("Total days does'nt fit between dates");
			$this->invaliddate2 = str_replace("'", "\'", $this->invaliddate2);
			$this->invaliddate2 = str_replace('"', "'+String.fromCharCode(34)+'",$this->invaliddate2);
			$this->invalidentry = absences_translate("Invalid entry!  Only numbers are accepted or . !");
			$this->invalidentry = str_replace("'", "\'", $this->invalidentry);
			$this->invalidentry = str_replace('"', "'+String.fromCharCode(34)+'",$this->invalidentry);
			$this->totaltxt = absences_translate("Total");
			$this->invalidentry1 = absences_translate("Invalid entry");
			$this->invalidentry2 = absences_translate("Days must be multiple of 0.5");
			$this->balancetxt = absences_translate("Balance");

			$arr = $babDB->db_fetch_array($babDB->db_query("select * from ".ABSENCES_ENTRIES_TBL." where id='".$babDB->db_escape_string($id)."'"));
			$this->iduser = $arr['id_user'];

			
			include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
			

			$date_begin = BAB_DateTime::fromIsoDateTime($arr['date_begin']);
			$date_end	= BAB_DateTime::fromIsoDateTime($arr['date_end']);
			
			
			$this->daybegin		= $date_begin->getDayOfMonth();
			$this->daysel		= $this->daybegin;

			

			$this->monthbegin	= $date_begin->getMonth();
			$this->monthsel		= $this->monthbegin;
			
			$this->yearbegin 	= $date_begin->getYear();
			$this->yearsel 		= $this->yearbegin;
			$this->timestampbegin	= $date_begin->getTimeStamp();
			$this->timestampsel	= $this->timestampbegin;
			
			
			$this->dayend		= $date_end->getDayOfMonth();

			$this->monthend		= $date_end->getMonth();
			
			$this->yearend 		= $date_end->getYear();
			$this->yearendsel 	= $this->yearend;
			$this->timestampend	= $date_end->getTimeStamp();


			$this->hourbegin	= date('H:i:s', $date_begin->getTimeStamp());
			$this->hoursel 		= $this->hourbegin;
			$this->hourend		= date('H:i:s', $date_end->getTimeStamp());
			

			$this->remarks		= $arr['comment'];
			
			$this->startyear = $this->yearbegin - 5;

			$this->res = $babDB->db_query("
					select 
						e.id_right,
						e.quantity, 
						e.id,
						r.description,
						r.quantity right_quantity,
						r.quantity_unit 
					 
					FROM ".ABSENCES_ENTRIES_ELEM_TBL." e,
						absences_rights r
					where 
						e.id_entry=".$babDB->quote($id)." 
						AND r.id = e.id_right 
				");
			$this->count = $babDB->db_num_rows($this->res);
			$this->totalval = 0;

			$this->dayType = array(absences_translate("Morning"), absences_translate("Afternoon"));
			
			$babBody->addJavascriptFile($GLOBALS['babInstallPath'].'scripts/bab_dialog.js');
			
			$this->hours = absences_hoursList($GLOBALS['BAB_SESS_USERID']);
		}


		function getnexttype()
			{
			static $i = 0;
			global $babDB;
			if( $i < $this->count)
				{
				$arr = $babDB->db_fetch_array($this->res);

				$this->typename = bab_toHtml($arr['description']);
				$this->id_entry_elem = bab_toHtml($arr['id']);
				$this->nbdays = $arr['quantity'];
				$this->totalval += $this->nbdays;

				$row2 = $babDB->db_fetch_array($babDB->db_query("select sum(quantity) as total from ".ABSENCES_ENTRIES_ELEM_TBL." ee
				join ".ABSENCES_ENTRIES_TBL." e 
				where e.id_user='".$babDB->db_escape_string($this->iduser)."' 
					and e.status!='N' 
					and ee.id_right='".$babDB->db_escape_string($arr['id_right'])."' 
					and ee.id_entry=e.id"));

				$qdp = isset($row2['total'])? $row2['total'] : 0;

				list($quant) = $babDB->db_fetch_row($babDB->db_query("select quantity from ".ABSENCES_USERS_RIGHTS_TBL." where id_right='".$babDB->db_escape_string($arr['id_right'])."' and id_user='".$babDB->db_escape_string($this->iduser)."'"));
				if( $quant == '' )
					$quant = $arr['right_quantity'];
				
				switch($arr['quantity_unit'])
				{
					case 'D':
						$this->unit = absences_translate('day(s)');
						break;
					case 'H':
						$this->unit = absences_translate('hour(s)');
						break;
				}

				$this->quantity_available = $quant - $qdp;
				$i++;
				return true;
				}
			else
				{
				$this->daysel = $this->dayend;
				$i = 1;
				return false;
				}

			}


		function getnextday()
			{
			static $i = 1;

			if( $i <= date('t', $this->timestampsel))
				{
				$this->dayid = $i;
				if( $this->daysel == $i)
					{
					$this->selected = "selected";
					}
				else
					$this->selected = "";
				
				$i++;
				return true;
				}
			else
				{
				$this->daysel = $this->dayend;
				$this->timestampsel = $this->timestampend;
				$i = 1;
				return false;
				}

			}

		function getnextmonth()
			{
			static $i = 1;

			if( $i < 13)
				{
				$this->monthid = $i;
				$this->monthname = bab_DateStrings::getMonth($i);
				if( $this->monthsel == $i)
					{
					$this->selected = "selected";
					}
				else
					$this->selected = "";

				$i++;
				return true;
				}
			else
				{
				$this->monthsel = $this->monthend;
				$i = 1;
				return false;
				}

			}
		function getnextyear()
			{
			static $i = 0;
			if( $i < 20)
				{
				$this->yearidval = $this->startyear + $i;
				if( $this->yearsel == $this->yearidval )
					{
					$this->selected = "selected";
					}
				else
					$this->selected = "";
				$i++;
				return true;
				}
			else
				{
				$this->yearsel = $this->yearendsel;
				$i = 0;
				return false;
				}

			}
		function getnexthour()
			{
			if (list($key, $value) = each($this->hours))
			{
				$this->value = bab_toHtml($key);
				$this->option = bab_toHtml($value);
				if ($this->hoursel === $this->value)
				{
					$this->selected = "selected";
				} else {
					$this->selected = "";
				}
				return true;
			}
			
			$this->hoursel = $this->hourend;
			reset($this->hours);
			return false;
			}

		}

	$temp = new temp($vrid);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadmb.html", "editvacrequest"));
}



function deleteVacationRequests($dateb, $userid)
	{
	global $babBody, $babDB;
	class tempa
		{
		var $datetxt;
		var $dateformattxt;
		var $delete;
		var $usertext;
		var $usersbrowurl;
		var $dateburl;

		function tempa($dateb, $userid)
			{
			global $babDB;
			$this->datetxt = absences_translate("End date");
			$this->dateformattxt = "( ".absences_translate("dd-mm-yyyy")." )";
			$this->delete = absences_translate("Delete");
			$this->usertext = absences_translate("User");
			$this->usersbrowurl = absences_addon()->getUrl()."vacadm&idx=browu&cb=";
			$this->dateburl = $GLOBALS['babUrlScript']."?tg=month&callback=dateBegin&ymin=10&ymax=0";
			if( $dateb != "" )
				$this->datebval = $dateb;
			else
				$this->datebval = "";
			if( $userid != "" )
				{
				$this->userval = bab_getUserName($userid);
				$this->userid =$userid;
				}
			else
				{
				$this->userval ="";
				$this->userid ="";
				}
			}
		}

	$temp = new tempa($dateb, $userid);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadmb.html", "reqdelete"));
	}

function deleteInfoVacationRequests($dateb, $userid)
	{
	global $babBody;
	
	class temp
		{
		var $warning;
		var $message;
		var $title;
		var $urlyes;
		var $urlno;
		var $yes;
		var $no;

		function temp($dateb, $userid)
			{
			$this->message = absences_translate("Are you sure you want to remove the requests which finish before the following date ").$dateb;
			if( $userid == "" )
				$this->title = bab_getUserName("All users");
			else
				$this->title = bab_getUserName($userid);
			$this->warning = absences_translate("WARNING: This operation will delete vacations requests"). "!";
			$this->urlyes = absences_addon()->getUrl()."vacadmb&idx=lreq&date=".$dateb."&userid=".$userid."&action=Yes";
			$this->yes = absences_translate("Yes");
			$this->urlno = absences_addon()->getUrl()."vacadmb&idx=lreq";
			$this->no = absences_translate("No");
			}
		}

	$ret = true;
	if( $dateb == "" )
		{
		$ret = false;
		}

	$ar = explode("-", $dateb);
	if( count($ar) != 3 || !is_numeric($ar[0]) || !is_numeric($ar[1]) || !is_numeric($ar[2]))
		{
		$ret = false;
		}

	if( $ar[0] <= 0 || $ar[1] <= 0 || $ar[2] <= 0)
		{
		$ret = false;
		}

	if( !$ret )
		{
		$babBody->msgerror = absences_translate("You must provide a correct date");
		return false;
		}

	$temp = new temp($dateb, $userid);
	$babBody->babecho(absences_addon()->printTemplate($temp, "warning.html", "warningyesno"));
	return true;
	}

	
	
function updateVacationRequest($daybegin, $monthbegin, $yearbegin,$dayend, $monthend, $yearend, $hourbegin, $hourend, $remarks, $vrid, $quantity)
{
	global $babBody, $babDB;
	$nbdays = array();

	$res = $babDB->db_query("select * from ".ABSENCES_ENTRIES_ELEM_TBL." where id_entry='".$babDB->db_escape_string($vrid)."'");

	$ntotal = 0;
	while( $arr = $babDB->db_fetch_array($res))
	{
		if( isset($quantity[$arr['id']]))
		{
			$nbd = $quantity[$arr['id']];
			if( !is_numeric($nbd) || $nbd < 0 )
				{
				$babBody->msgerror = absences_translate("You must specify a correct number days") ." !";
				return false;
				}
			
			if( $nbd >= 0 )
			{
				$nbdays['id'][] = $arr['id'];
				$nbdays['val'][] = $nbd;
				$ntotal += $nbd;
			}
		}
	}

	$begin = bab_mktime("$yearbegin-$monthbegin-$daybegin $hourbegin");
	$end = bab_mktime("$yearend-$monthend-$dayend $hourend");

	if( $begin >= $end) {
		$babBody->msgerror = absences_translate("ERROR: End date must be older")." !";
		return false;
	}
	
	$res = $babDB->db_query("
		SELECT 
			date_begin,
			date_end,
			id_user 
		FROM ".ABSENCES_ENTRIES_TBL." 
		WHERE 
			id='".$babDB->db_escape_string($vrid)."'
		");
		
	$old = $babDB->db_fetch_assoc($res);
	
	$old_begin = bab_mktime($old['date_begin']);
	$old_end = bab_mktime($old['date_end']);


	$b = date('Y-m-d H:i:s', $begin);
	$e = date('Y-m-d H:i:s', $end);
	
	

	
	

	$babDB->db_query("
		update ".ABSENCES_ENTRIES_TBL." SET 
			date_begin	= '".$babDB->db_escape_string($b)."', 
			date_end	= '".$babDB->db_escape_string($e)."',  
			comment		= '".$babDB->db_escape_string($remarks)."' 
		where 
			id='".$babDB->db_escape_string($vrid)."'
		");

	for( $i = 0; $i < count($nbdays['id']); $i++)
		{
		if( $nbdays['val'][$i] > 0 ) {
			$babDB->db_query("update ".ABSENCES_ENTRIES_ELEM_TBL." set quantity='".$babDB->db_escape_string($nbdays['val'][$i])."' where id='".$babDB->db_escape_string($nbdays['id'][$i])."'");
		}
		else {
			$babDB->db_query("delete from ".ABSENCES_ENTRIES_ELEM_TBL." where id='".$babDB->db_escape_string($nbdays['id'][$i])."'");
		}
	}
	
	
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	absences_updatePeriod($vrid, BAB_DateTime::fromIsoDateTime($b), BAB_DateTime::fromIsoDateTime($e));
	
	
	$period_begin	= $old_begin 	< $begin 	? $old_begin 	: $begin;
	$period_end 	= $old_end 		> $end 		? $old_end 		: $end;
	

	include_once $GLOBALS['babInstallPath']."utilit/eventperiod.php";
	$event = new bab_eventPeriodModified($period_begin, $period_end, $old['id_user']);
	$event->types = BAB_PERIOD_VACATION;
	bab_fireEvent($event);
	
	return true;
}


function doDeleteVacationRequests($date, $userid)
{
	global $babDB;

	$ar = explode("-", $date);
	$dateb = sprintf("%04d-%02d-%02d", $ar[2], $ar[1], $ar[0]);

	$req = "SELECT id FROM ".ABSENCES_ENTRIES_TBL." WHERE date_end <= ".$babDB->quote($dateb);
	if( $userid != "" )
		$req .= " and id_user=".$babDB->quote($userid);

	$res = 	$babDB->db_query($req);
	while( $arr = $babDB->db_fetch_array($res))
	{
		absences_delete_request($arr['id']);
	}
}



/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
	{
	$babBody->msgerror = absences_translate("Access denied");
	return;
	}


$idx = bab_rp('idx', "lreq");
$Submit = bab_pp('Submit', null);

if( bab_rp('add') == "modvr")
{
	if( isset($Submit))
	{
	if(!updateVacationRequest(bab_pp('daybegin'), bab_pp('monthbegin'), bab_pp('yearbegin'),bab_pp('dayend'), bab_pp('monthend'), bab_pp('yearend'), bab_pp('hourbegin'), bab_pp('hourend'), bab_pp('remarks'), bab_pp('vrid'), bab_pp('quantity')))
		$idx = "vunew";
	}
	else if( isset($bdelete))
	{
		$idx = "delur";
	}
}
else if( bab_rp('action') == "Yes")
	{
	doDeleteVacationRequests(bab_rp('date'), bab_rp('userid'));
	}

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}

$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");





/**
 * Manually update the calendar for one absence entry
 * @param int $id
 */
function absences_updateEntryCalendar($id) {
    $entry = absences_Entry::getById($id);
    $entry->updateCalendar();
}



		
$pos = bab_rp('pos');
$idcol = bab_rp('idcol');
$idsa = bab_rp('idsa');		

switch($idx)
	{
	case "morvw":
		require_once dirname(__FILE__).'/utilit/request.ui.php';
		$babBody->addItemMenu("lreq", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
		$babBody->addItemMenu("morvw", absences_translate("View"), absences_addon()->getUrl()."vacadmb&idx=morvw&id=".bab_rp('id'));
		absences_viewVacationRequestDetail(bab_rp('id'), true);
		break;
		
	case 'updcal':
	    if (bab_isUserAdministrator()) {
    	    absences_updateEntryCalendar(bab_rp('id'));
	    }
	    break;
	    
	    
    case 'crecal':
        if (bab_isUserAdministrator()) {
            // warning, this can create duplicates
            absences_createPeriod(bab_rp('id'));
        }
        break;
		

	case "edvr":
		$babBody->title = absences_translate("Edit request vacation");
		editVacationRequest(bab_rp('id'));
		$babBody->addItemMenu("lreq", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
		$babBody->addItemMenu("edvr", absences_translate("Modify"), absences_addon()->getUrl()."vacadmb&idx=edvr");
		break;


		
	case "ddreq":
		$babBody->title = absences_translate("Delete vacations requests");
		if(!empty($_POST)) {
			deleteInfoVacationRequests(bab_rp('dateb'), bab_rp('userid'));
		} else {
			deleteVacationRequests(bab_rp('dateb'), bab_rp('userid'));
		}
		
		$babBody->addItemMenu("lreq", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
		$babBody->addItemMenu("ddreq", absences_translate("Delete"), absences_addon()->getUrl()."vacadmb&idx=ddreq");
		break;
		
	case 'delete':
		$babBody->title = absences_translate("Delete vacation request");
		absences_deleteVacationRequest(bab_rp('id_entry'), true);
		$babBody->addItemMenu("lreq", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
		$babBody->addItemMenu("delete", absences_translate("Delete"), absences_addon()->getUrl()."vacadmb&idx=delete");
		break;
		

	case 'archives';
	case "lreq":
	default:
		
		absences_listVacationRequestsb();
		$babBody->addItemMenu("lreq", absences_translate("Requests"), absences_addon()->getUrl()."vacadmb&idx=lreq");
		$babBody->addItemMenu("archives", absences_translate("Archives"), absences_addon()->getUrl()."vacadmb&idx=archives&archived=1");
		$babBody->addItemMenu("ddreq", absences_translate("Delete"), absences_addon()->getUrl()."vacadmb&idx=ddreq");
		break;
	}
$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');

