<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/workperiod_type.class.php';
require_once dirname(__FILE__).'/utilit/workperiod_type.ui.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';

class absences_WorkperiodList
{
	public $altbg = true;
	public $t_name;
	public $t_quantity;
	public $name;
	public $quantity;
	
	private $res;
	
	public function __construct()
	{
		global $babDB;
		
		$this->t_name = absences_translate('Name');
		$this->t_quantity = absences_translate('Quantity');
		
		$this->res = $babDB->db_query('SELECT * FROM absences_workperiod_type ORDER BY name');
	}
	
	public function getnext()
	{
		global $babDB;
		if ($arr = $babDB->db_fetch_assoc($this->res))
		{
			$this->altbg = !$this->altbg;
			$this->name = bab_toHtml($arr['name']);
			$this->quantity = bab_toHtml(absences_quantity($arr['quantity'], $arr['quantity_unit']));
			$this->editurl = bab_toHtml(absences_addon()->getUrl().'workperiod_type&idx=edit&id_type='.$arr['id']);
			return true;
		}
		
		return false;
	}
	
	public static function display()
	{
		$list = new absences_WorkperiodList;
		$html = absences_addon()->printTemplate($list, 'workperiod_type.html', 'list');
		
		$babBody = bab_getInstance('babBody');
		
		$babBody->setTitle(absences_translate('Types of working days entitling recovery'));
		
		require_once $GLOBALS['babInstallPath'] . 'utilit/toolbar.class.php';
		$toolbar = new BAB_Toolbar();
		$sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';
		$toolbar->addToolbarItem(
				new BAB_ToolbarItem(absences_translate('Add a type'), absences_addon()->getUrl().'workperiod_type&idx=edit',
						$sImgPath . 'edit_add.png', '', '', '')
		);
		
		$babBody->addStyleSheet('toolbar.css');
		$babBody->babEcho($toolbar->printTemplate());
		
		
		$babBody->babEcho($html);
	}
}



/**
 * @throws Exception
 */
function absences_saveWorkperiodType(Array $posted, absences_WorkperiodType $type = null)
{
	global $babDB;
	
	if (isset($posted['cancel']))
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->location();
	}
	
	
	if (isset($posted['save']))
	{
		$name = $posted['name'];
		
		if (empty($name))
		{
			throw new Exception(absences_translate('The name is mandatory'));
		}
	
		
		$quantity = str_replace(',', '.', $posted['quantity']);
		$quantity_unit = $posted['quantity_unit'];
	
		if (null !== $type)
		{
	
			$babDB->db_query('UPDATE absences_workperiod_type SET
					name='.$babDB->quote($name).',
					quantity='.$babDB->quote($quantity).',
					quantity_unit='.$babDB->quote($quantity_unit).'
	
					WHERE id='.$babDB->quote($type->id)
			);
	
		} else {
	
			$babDB->db_query('INSERT INTO absences_workperiod_type (name, quantity, quantity_unit)
					VALUES ('.$babDB->quote($name).', '.$babDB->quote($quantity).', '.$babDB->quote($quantity_unit).')');
	
		}
	
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->location();
	}
	
	
	if (isset($posted['delete']) && null !== $type)
	{
		
		$type->delete();
	
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->location();
	}
	
	
	
	
}



function absences_editWorkperiodType()
{
	global $babDB;
	$id_type = (int) bab_rp('id_type');
	if (isset($_POST['type']['id']))
	{
		$id_type = (int) $_POST['type']['id'];
	}
	
	
	if ($id_type)
	{
		$type = absences_WorkperiodType::getFromId($id_type);
	} else {
		$type = null;
	}
	
	
	if (isset($_POST['type']))
	{
		absences_saveWorkperiodType($_POST['type'], $type);
	}
	
	
	
	$W = bab_Widgets();
	
	$page = $W->BabPage();
	$page->setTitle(absences_translate('Create a working day type'));
	
	
	$editor = new absences_WorkperiodTypeEditor($type);
	$page->addItem($editor);
	
	
	$page->displayHtml();
}




// main
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

$idx = bab_rp('idx', 'list');

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
$babBody->addItemMenu("list", absences_translate("Work day types"), absences_addon()->getUrl()."workperiod_type");


switch($idx)
{
	case 'edit':
		absences_editWorkperiodType();
		$babBody->addItemMenu("edit", absences_translate("Edit"), absences_addon()->getUrl()."workperiod_type&idx=edit");
		
		break;
	
	case 'list':
		absences_WorkperiodList::display();
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');