;<?php /*

[general]
name							="absences"
version							="2.119.6"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="Leave management addon"
description.fr					="Module de gestion des absences"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.2.0"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
mysql_character_set_database	="latin1,utf8"
icon							="icon.png"
configuration_page				="admvacs"
preinstall_script				="preinstall.php"
tags							="extension,recommend,intranet"

[recommendations]
mod_curl						="Available"


[addons]

jquery						=">=1.7.1.5"
widgets						=">=1.0.72"
LibTranslate				=">=1.12.0rc3.01"

;*/
