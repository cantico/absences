<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/../utilit/entry.class.php';

/**
 * add dates to entry_elem
 */
class absences_upgradeEntryElemDates
{
    private $totalCount = 0;
    private $currentUpdate = 1;
    
    private $progress;
    
    public function select()
    {
        global $babDB;
        
        $res = $babDB->db_query("SELECT ee.id_entry, ee.id, ee.quantity, e.date_begin, e.date_end, e.id_user, r.quantity_unit FROM
        	    absences_entries e,
        	    absences_entries_elem ee,
    	        absences_rights r,
    	        absences_types t
    	    WHERE
    	       ee.id_entry = e.id
    	       AND ee.date_begin = '0000-00-00 00:00:00' 
    	       AND r.id = ee.id_right
    	       AND t.id = r.id_type
    	    ORDER BY e.id, r.sortkey, t.name
    	");
        
        $this->totalCount = $babDB->db_num_rows($res);
        
        if ($this->totalCount > 0) {
            $this->progress = new bab_installProgressBar;
            $this->progress->setTitle(sprintf(absences_translate('Update existing vacation requests (%d)'), $this->totalCount));
        }
        
        $entry_stack = array();
    	while ($entry_elem = $babDB->db_fetch_assoc($res)) {
    	    
    	    $id_entry = (int) $entry_elem['id_entry'];
    	    
    	    if ('0000-00-00 00:00:00' === $entry_elem['date_begin'] || '0000-00-00 00:00:00' === $entry_elem['date_end']) {
    	        absences_Entry::getById($entry_elem['id_entry'])->delete();
    	        continue;
    	    }
    	    
    	    if (!isset($entry_stack[$id_entry])) {
    	        
    	        if (!empty($entry_stack)) {
    	            // process previous stack
    	           $this->updateEntry(reset($entry_stack));
    	           $entry_stack = array();
    	        }
    	        
    	        $entry_stack[$id_entry] = array();
    	    }
    	    
    	    $entry_stack[$id_entry][] = $entry_elem;
    	}
    	
    	// process last stack entry if exists
    	if ($entry_stack) {
    	   $this->updateEntry(reset($entry_stack));
    	}
    	
    	if (isset($this->progress)) {
    	    $this->progress->setProgression(100);
    	}
    }
    
    /**
     * @param array $arrentry      Elements of one entry
     */
    protected function updateEntry(Array $arrentry)
    {
        require_once dirname(__FILE__).'/../utilit/vacincl.php';
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        
        if (1 === count($arrentry)) {
            // one elem per entry
            
            return $this->updateOneElem($arrentry);
        }

        $row = reset($arrentry);
        $entry = absences_Entry::getById($row['id_entry']);
        
        $entry->loadElements();
        
        try {
            $entry->setElementsDates();
            $entry->saveElements();
        } catch (absences_EntryException $e) {
            bab_installWindow::message(bab_toHtml($e->getMessage()));
        }
        
        $percent = ($this->currentUpdate * 100) / $this->totalCount;
        $this->progress->setProgression(round($percent));
        
        $this->currentUpdate++;
    }
    
    /**
     * Copy entry dates to elem date
     */
    protected function updateOneElem(Array $arrentry)
    {
        global $babDB;
        
        $elem = reset($arrentry);
        
        $babDB->db_query('
            UPDATE absences_entries_elem 
            SET 
                date_begin='.$babDB->quote($elem['date_begin']).', 
                date_end='.$babDB->quote($elem['date_end']).' 
            WHERE id='.$babDB->quote($elem['id'])
        );
    }
    
    
    
    public static function onUpgrade()
    {
        $obj = new absences_upgradeEntryElemDates();
        $obj->select();   
    }
}

