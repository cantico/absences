<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
require_once dirname(__FILE__).'/utilit/workperiod_recover_request.ui.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';

class absences_WorkingDayList extends absences_Paginate 
{
	const MAX = 30;
	
	private $res;
	
	public $altbg = true;
	
	public function __construct()
	{
		$this->t_user = absences_translate('User');
		$this->t_begin = absences_translate('Begin');
		$this->t_end = absences_translate('End');
		$this->t_type = absences_translate('Type');
		$this->t_status = absences_translate('Status');
		$this-> t_delete = absences_translate('Delete');
		$this->t_confirmdelete = absences_translate('Do you really want to delete this working day? the generated recovery right will not be deleted');
		
		$f = new absences_getRequestSearchForm();
		
		$this->res = new absences_WorkperiodRecoverRequestIterator;
		$this->res->archived = (int) bab_rp('archived', 0);
		
		if ($status = $f->param('idstatus'))
		{
			if ('W' === $status)
			{
				$status = '';
			}
			$this->res->status = $status;
		}
		
		if ($userid = $f->param('userid'))
		{
			$this->res->users = array($userid);
		}
		
		if ($organization = $f->param('organization'))
		{
		    $this->res->organization = array($organization);
		}
		
		$W = bab_Widgets();
		$datePicker = $W->DatePicker();
		
		if ($begin = $datePicker->getISODate($f->param('dateb', null)))
		{
			$this->res->startFrom = $begin;
		}
		
		if ($end = $datePicker->getISODate($f->param('datee', null)))
		{
			$this->res->startTo = $end;
		}
		
		
		$this->res->rewind();
		
		$this->paginate($this->res->count(), self::MAX);
		$this->res->seek($this->pos);
		
		
		$this->searchform = $f->getHtmlForm(array('W' => absences_translate("Waiting"), 'Y' => absences_translate("Accepted"), 'N' => absences_translate("Refused")));
	}


	public function getnext()
	{
		if (($this->res->key() - $this->pos) >= self::MAX)
		{
			return false;
		}
		
		if ($this->res->valid())
		{
			$wd = $this->res->current();
			/*@var $wd absences_WorkperiodRecoverRequest */
			
			$this->altbg = !$this->altbg;
			
			$this->editurl = bab_toHtml(absences_addon()->getUrl().'vacadmwd&idx=edit&id='.$wd->id);
			$this->deleteurl = bab_toHtml(absences_addon()->getUrl().'vacadmwd&idx=delete&id='.$wd->id);
			$this->username = bab_toHtml(bab_getUserName($wd->id_user));
			$this->begin = bab_toHtml(bab_shortDate(bab_mktime($wd->date_begin)));
			$this->end = bab_toHtml(bab_shortDate(bab_mktime($wd->date_end)));
			
			$type = $wd->getType();
			$this->type = '';
			if ($type->getRow()) {
			     $this->type = bab_toHtml($type->name);
			}
			$this->status = bab_toHtml($wd->getStatusStr());
			$this->res->next();
			return true;
		}
		
		return false;
	}
	
	
	public function getnextstatus()
	{
		static $i = 0;
		if( $i < count($this->statarr))
		{
			$this->statusid = $i;
			$this->statusname = bab_toHtml($this->statarr[$i]);
			if( $this->idstatus != "" && $i == $this->idstatus )
				$this->selected = "selected";
			else
				$this->selected = "";
			$i++;
			return true;
		}
		else
			return false;
	}
}



function absences_WorkingDayList()
{
	$babBody = bab_getInstance('babBody');
	$list = new absences_WorkingDayList;
	
	if (bab_rp('archived'))
	{
		$babBody->setTitle(absences_translate('Archived working days entitling recovery'));
	} else {
		$babBody->setTitle(absences_translate('Working days entitling recovery'));
	}
	
	absences_addSpoofButton('workperiod_recover_request', absences_translate('Add a workperiod'));
	
	$babBody->addStyleSheet(absences_Addon()->getStylePath().'vacation.css');
	$babBody->addJavascriptFile($GLOBALS['babInstallPath'].'scripts/bab_dialog.js');
	$babBody->babEcho(absences_addon()->printTemplate($list, "vacadmwd.html", "list"));
}




/**
 * Save form
 * @param array $workperiod
 * @throws Exception
 */
function absences_saveWorkperiodRecoverRequestAdm(absences_WorkperiodRecoverRequest $workperiodRecover, Array $workperiod)
{

	if (!absences_WorkperiodRecoverRequest::checkForm($workperiod, $workperiodRecover))
	{
		return false;
	}

	$workperiodRecover->date_begin = absences_dateTimeForm($workperiod['datebegin'], $workperiod['hourbegin']);
	$workperiodRecover->date_end = absences_dateTimeForm($workperiod['dateend'], $workperiod['hourend']);
	$workperiodRecover->id_type = $workperiod['id_type'];
	$workperiodRecover->comment = $workperiod['comment'];
	
	if ('' === $workperiodRecover->status)
	{
		$workperiodRecover->quantity = $workperiod['quantity'];
		$workperiodRecover->quantity_unit = $workperiod['quantity_unit'];
	}
	
	$workperiodRecover->save();

	return true;
}



function absences_WDgotoList()
{
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

	$url = bab_url::get_request('tg');
	$url->idx = 'list';
	$url->location();
}


function absences_WorkingDayEdit()
{
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	if (isset($_POST['workperiod']))
	{
		if ( isset($_POST['workperiod']['cancel']) )
		{
			absences_WDgotoList();
		}
		
		
		if( isset($_POST['workperiod']['save'] ))
		{
		
			// modification uniquement
			
			$values = $_POST['workperiod'];
			$wd = absences_WorkperiodRecoverRequest::getById($values['id']);
			
			if (!$wd->getRow())
			{
				throw new Exception('Work period not found');
			}
			
			try {
				absences_saveWorkperiodRecoverRequestAdm($wd, $values);
				absences_WDgotoList();
			} catch(Exception $e)
			{
				$page->addError($e->getMessage());
			}
		}
	}
	
	$id = bab_gp('id', null);
	
	if ($id)
	{
		$wd = absences_WorkperiodRecoverRequest::getById($id);
	} else {
		$wd = null;
	}
	
	$editor = new absences_WorkperiodRecoverRequestEditor($wd);
	
	$page->setTitle(absences_translate('Worked day entitling recovery'));
	$page->addItem($editor);
	$page->displayHtml();
}



function absences_WorkingDayDelete()
{
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	$id = bab_gp('id', null);
	
	if ($id)
	{
		$wd = absences_WorkperiodRecoverRequest::getById($id);
		$wd->delete();
	}
	
	bab_url::get_request('tg')->location();
}


/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
$babBody->addItemMenu("list", absences_translate("Working days"), absences_addon()->getUrl()."vacadmwd&idx=list");



$idx = bab_rp('idx', "list");

switch($idx)
{
	case 'edit':
		absences_WorkingDayEdit();
		$babBody->addItemMenu("edit", absences_translate("Edit"), absences_addon()->getUrl()."vacadmwd&idx=edit&id=".bab_rp('id'));
		break;
	
	case 'archives':
	case 'list':
		absences_WorkingDayList();
		$babBody->addItemMenu("archives", absences_translate("Archives"), absences_addon()->getUrl()."vacadmwd&idx=archives&archived=1");
		break;
		
	case 'delete':
		absences_WorkingDayDelete();
		break;
}

$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');