<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/entry.class.php';


function absences_TestEntries()
{
    $limit = (int) bab_rp('limit', 12);
    
    $page = bab_Widgets()->BabPage();
    $page->setTitle(sprintf(absences_translate('Entries with more than %d hours offset beetween the end date and the last right end date'), $limit));
    
    $I = new absences_EntryIterator();
    
    foreach($I as $entry) {
        /*@var $entry absences_Entry */
        
        $entry->loadElements();
        
        $elements = $entry->getElements();
        $lastElement = end($elements);
        
        if (!isset($lastElement) || !$lastElement) {
            continue;
        }
        
        /*@var $lastElement absences_EntryElem */
        
        $last = bab_mktime($lastElement->date_end)/3600;
        $entry_end = bab_mktime($entry->date_end)/3600;
        $diff = abs($entry_end - $last);
        
        if ($diff > $limit) {
            $page->addItem($entry->getCardFrame(true, 0, 0));
        }
    }
    
    $page->displayHtml();
}


if (!bab_isUserAdministrator()) {
    throw new Exception('Access denied');
}


absences_TestEntries();