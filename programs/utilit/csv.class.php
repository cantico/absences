<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


class absences_Csv
{
    
    /**
     * @var string
     */
    protected $separator;
    
    /**
     * @var string
     */
    protected $sepdec;


    protected function csvEncode($str)
    {
        $str = str_replace("\n"," ",$str);
        return '"'.str_replace('"','""',$str).'"';
    }
    
    
    /**
     * output an array on raw values
     * @param array $arr
     *
     */
    protected function outputArr(Array $arr)
    {
        $arr = $this->encodeFloats($arr);
    
        foreach ($arr as &$v) {
            $v = $this->csvEncode($v);
        }
        echo implode($this->separator, $arr)."\n";
    }
    
    
    /**
     *
     * @return boolean
     */
    protected function isRowEmpty(Array $currentRow)
    {
        foreach($currentRow as &$val)
        {
            if (is_float($val))
            {
                if ($val > 0) {
                    return false;
                }
            }
        }
    
        return true;
    }
    
    
    protected function encodeFloats(Array $currentRow)
    {
        // encode float
        foreach($currentRow as &$val) {
            if (is_float($val)) {
                $val = number_format($val, 1 , $this->sepdec , '' );
            }
        }
    
        return $currentRow;
    }
    
    
    /**
     * @return string
     */
    protected function getAgentDirValue(absences_Agent $agent, $fieldname)
    {
        $direntry = $agent->getDirEntry();
    
        if (isset($direntry[$fieldname]))
        {
            return $direntry[$fieldname]['value'];
        }
    
        return '';
    }
    
    

    /**
     *
     * @param string $quantity_unit
     * @return string
     */
    protected function getUnit($quantity_unit)
    {
        switch($quantity_unit)
        {
            case 'D': return absences_translate('days');
            case 'H': return absences_translate('hours');
        }
         
        return '';
    }
    
    
    protected function setHeaders($filename)
    {
        header("Content-Disposition: attachment; filename=\"".$filename.".csv\""."\n");
        header("Content-Type: text/csv"."\n");
        header("Content-transfert-encoding: binary"."\n");
    }
    
    
    
    protected function date($strDate) {
        return bab_shortDate(bab_mktime($strDate), false);
    }
}