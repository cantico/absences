<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';



/**
 * 
 * @property string $name
 * @property string $description
 * @property string $id_cat
 *
 */
class absences_Collection extends absences_Record
{
	
	
	
	public static function getById($id)
	{
		$collection = new absences_Collection;
		$collection->id = $id;
		
		return $collection;
	}
	
	
	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_collections WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	/**
	 * @return absences_AgentIterator
	 */
	public function getAgentIterator()
	{
		require_once dirname(__FILE__).'/agent.class.php';
		$I = new absences_AgentIterator;
		$I->setCollection($this);
		
		return $I;
	}
	

	/**
	 * Add a vacation right to the collection
	 * @param	absences_Right	$right
	 * @return absences_Collection 
	 */
	public function addRight(absences_Right $right)
	{
		global $babDB;
		$babDB->db_queryWem('INSERT INTO absences_coll_rights (id_coll, id_right) VALUES ('.$babDB->quote($this->id).', '.$babDB->quote($right->id).')');
		
		$right->addMovement(sprintf(absences_translate('The collection %s has been linked to the right %s'), $this->name, $this->description));
		
		return $this;
	}
	
	/**
	 * Link all agents with the regime to the right given in parameter
	 * Must be used il a progress bar context
	 * 
	 * @param	absences_Right			$right
	 * @param   Widget_ProgressBar		$progress
	 * 
	 * @return absences_Collection
	 */
	public function linkAgentsToRight(absences_Right $right, Widget_ProgressBar $progress)
	{
		foreach($this->getAgentIterator() as $agent)
		{	
		    bab_setTimeLimit(5); // 5s per agent
		    
			/*@var $agent absences_Agent */
		    
			if ($agent->addRight($right))
			{
				try {
					$agent->addFixedEntry($right);
				} catch (absences_EntryException $e)
				{
					$agent = $e->entry->getAgent();
					echo bab_toHtml(sprintf(absences_translate('Failed to update the period for %s, %s (%s)'), 
							$agent->getName(), 
							$e->getMessage(),
							absences_DateTimePeriod($e->entry->date_begin, $e->entry->date_end))
					, BAB_HTML_ALL);
				}
			}

		}
		
		bab_setTimeLimit(60);
		
		return $this;
	}
	
	/**
	 * Mettre a jour la demande des agents de ce regime
	 * les agents sont deja lies au droit, mais il faut creer la demande si ce n'est pas deja fait pour les droits a date fixe
	 * 
	 * @param	absences_Right			$right
	 * @param   Widget_ProgressBar		$progress
	 */
	public function updateAgents(absences_Right $right, Widget_ProgressBar $progress)
	{
	    foreach ($this->getAgentIterator() as $agent) {
	        /*@var $agent absences_Agent */
	        
	        // per user time limit
	        bab_setTimeLimit(5);
	        ini_set('memory_limit', '256M'); // mises a jour caldav massives
	        
	        try {
	            $agent->addFixedEntryIfNotExists($right);
	        } catch (absences_EntryException $e)
	        {
	            
	            echo bab_toHtml(sprintf(absences_translate('Failed to update the period for %s, %s (%s)'),
	                    $agent->getName(),
	                    $e->getMessage(),
	                    absences_DateTimePeriod($e->entry->date_begin, $e->entry->date_end))
	                    , BAB_HTML_ALL);
	        } catch (Exception $e)
	        {
	            echo bab_toHtml($e->getMessage(), BAB_HTML_ALL);
	        }
	    }
	    
	    bab_setTimeLimit(30);
	}
	
	
	/**
	 * Remove vacation right from collection
	 * @param	absences_Right	$right
	 * @return absences_Collection
	 */
	public function removeRight(absences_Right $right)
	{
		global $babDB;
		$babDB->db_query('DELETE FROM absences_coll_rights WHERE id_coll='.$babDB->quote($this->id).' AND id_right='.$babDB->quote($right->id));
		
		$right->addMovement(sprintf(absences_translate('The collection %s has been unlinked from the right %s'), $this->name, $this->description));
		
		return $this;
	}
	
	/**
	 * Unlink all agents with the collection from the right given in parameter
	 * @param	absences_Right	$right
	 * @return absences_Collection
	 */
	public function unlinkAgentsFromRight(absences_Right $right)
	{
	    
		foreach($this->getAgentIterator() as $agent)
		{
		    bab_setTimeLimit(5);

			if ($agent->removeRight($right))
			{
				$agent->removeFixedEntry($right);
			}
		}
		
		bab_setTimeLimit(10);
		
		return $this;
	}
	
	
	/**
	 * Test if agent is linked to collection
	 * @param absences_Agent $agent
	 * @return bool
	 */
	public function isLinkedToAgent(absences_Agent $agent)
	{
		require_once dirname(__FILE__).'/agent.class.php';
		
		return (((int) $agent->id_coll) === ((int) $this->id));
	}
	
	/**
	 * Test if right is linked to collection
	 * @param absences_Right $right
	 * @return boolean
	 */
	public function isLinkedToRight(absences_Right $right)
	{
		require_once dirname(__FILE__).'/collection_right.class.php';
		
		$link = new absences_CollectionRight;
		$link->setCollection($this);
		$link->setRight($right);
		$row = $link->getRow();
		
		if ($row)
		{
			return true;
		}
		return false;
	}
	
	
}










class absences_CollectionIterator extends absences_Iterator
{
	/**
	 * @var absences_Right
	 */
	protected $right;



	public function setRight(absences_Right $right)
	{
		$this->right = $right;
	}


	public function getObject($data)
	{
		$collection = new absences_Collection;
		$collection->setRow($data);

		return $collection;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			
			$query = 'SELECT c.* FROM absences_coll_rights cr, absences_collections c WHERE  c.id=cr.id_coll';

			if (isset($this->right))
			{
			    $query .= ' AND cr.id_right='.$babDB->quote($this->right->id);
			}
			
			$query .= ' ORDER BY c.name';

			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}

}