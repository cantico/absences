<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/agent_right.class.php';
require_once dirname(__FILE__).'/csv.class.php';

class exportAvailableBalancesCls extends absences_Csv
{
	private $resYears;

	/**
	 *
	 * @var array
	 */
	private $rights;


	/**
	 * @var array
	 */
	private $recoveries;
	
	
	/**
	 * @var string YYYY-MM-DD
	 */
	private $date = null;
	
	/**
	 * @var array
	 */
	private $dirfields;


	public function getHtml()
	{
		global $babDB;

		$this->separatortxt = absences_translate("Separator");
		$this->other = absences_translate("Other");
		$this->comma = absences_translate("Comma");
		$this->tab = absences_translate("Tab");
		$this->semicolon = absences_translate("Semicolon");
		$this->export = absences_translate("Export");
		$this->sepdectxt = absences_translate("Decimal separator");
		$this->t_yes = absences_translate("Yes");
		$this->t_no = absences_translate("No");
		$this->t_date = absences_translate('Export day date');
		$this->t_year = absences_translate('Year filter');
		$this->t_organization = absences_translate('Organization');
		$this->additional_fields = absences_translate("Additional fields to export:");
		
		$W = bab_Widgets();
		$this->datePicker = $W->DatePicker()->setName('date')->display($W->HtmlCanvas()); 

		$this->resYears = $babDB->db_query("SELECT YEAR(date_begin) year FROM absences_rights WHERE YEAR(date_begin)<>'0' GROUP BY year");

		$this->resOrganization = $babDB->db_query("SELECT * FROM `absences_organization` ORDER BY name ASC");


		$this->dirfields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

		unset($this->dirfields['sn']);
		unset($this->dirfields['givenname']);
		unset($this->dirfields['jpegphoto']);

		return absences_addon()->printTemplate($this, "vacadm.html", "abexport");
	}

	/**
	 * template method to list available years
	 */
	public function getnextyear()
	{
		global $babDB;

		if ($arr = $babDB->db_fetch_assoc($this->resYears))
		{
			$this->year = bab_toHtml($arr['year']);
			return true;
		}

		return false;
	}


	public function getnextfield()
	{
		if (list($name,$arr) = each($this->dirfields))
		{
			$this->fieldname = bab_toHtml($name);
			$this->fieldlabel = bab_toHtml($arr['name']);
			return true;
		}
		return false;
	}

	/**
	 * template method to list available organization
	 */
	public function getnextorganization()
	{
		global $babDB;

		if ($arr = $babDB->db_fetch_assoc($this->resOrganization))
		{
			$this->organization = bab_toHtml($arr['name']);
			$this->id_organization = bab_toHtml($arr['id']);
			return true;
		}

		return false;
	}



	private function query($year, $groupby = '', $organization = '', $user = '')
	{
		global $babDB;

		if (!empty($year)) {
			$year = 'AND YEAR(r.date_begin)='.$babDB->quote($year);
		}

		if ($organization) {
			$organization = 'AND p.id_organization='.$babDB->quote($organization);
		}
		
		if ($user) {
		    $user = ' AND p.id_user='.$babDB->quote($user);
		}


		$query = "
			SELECT
				ur.* 
			FROM
				absences_users_rights ur, 
				absences_rights r,
				absences_personnel p,
				bab_users u
			WHERE
				ur.id_user=p.id_user
				AND p.id_user=u.id
				AND r.id = ur.id_right 
				$year
				$organization
				$user
			$groupby
			ORDER BY
				u.lastname,
				u.firstname,
				r.description
		";

		return $babDB->db_query($query);
	}

	

	
	
	
	/**
	 * New row for the user
	 * 
	 * @param absences_AgentRight $agentRight
	 * @param array $remain
	 * 
	 * @return Array
	 */
	private function getNewCurrentRow(absences_AgentRight $agentRight, Array $remain)
	{
	    $arr = bab_getUserName($agentRight->id_user, false);
	    
	    $currentRow = array(
	            $arr['lastname'],
	            $arr['firstname'],
	            (float) $remain['D'], // total days
	            (float) $remain['H']  // total hours
	    );
	    
	    foreach($this->rights as $initcol) {
	        $currentRow[$initcol] = 0.0;
	    }
	     
	    foreach($this->recoveries as $quantity_unit => $initcol) {
	        if (isset($initcol)) {
	            $currentRow[$initcol] = 0.0;
	        }
	    }
	    
	    return $currentRow;
	}


	/**
	 * Process one row
	 * if user row exists, add remain to the correct right column
	 * if the user row does not exists, create it
	 *
	 * return the row only if it is the first user row
	 * else return null
	 *
	 * @return array | null
	 */
	private function processRow($arr)
	{
		static $currentUser = null;
		static $currentRow = null;

		$return = null;

		if (null === $arr) {
		    
		    if (!isset($currentRow)) {
		        return array();
		    }

			return $currentRow;
		}
		
		$agentRight = new absences_AgentRight();
		$agentRight->setRow($arr);
		
		$right = $agentRight->getRight();

		$remain = array('D' => 0.0, 'H' => 0.0);

		$remain_line = $remain[$right->quantity_unit] = $agentRight->getAvailableQuantity($this->date);


		if ($currentUser !== $agentRight->id_user) {
			$currentUser = $agentRight->id_user;

			if (null !== $currentRow && !$this->isRowEmpty($currentRow)) {
				$return = $currentRow;
			}

			$currentRow = $this->getNewCurrentRow($agentRight, $remain);
			
			$this->addDirValues($arr['id_user'], $currentRow);

		} else {

			$currentRow[2] += (float) $remain['D']; // total days
			$currentRow[3] += (float) $remain['H']; // total hours
		}


        $this->setRightColumn($right, $currentRow, $remain, $remain_line);
		

		return $return;
	}
	
	
	/**
	 * Complete current row with right value
	 * @param absences_Right $right
	 * @param array &$currentRow
	 */
	private function setRightColumn(absences_Right $right, Array &$currentRow, Array $remain, $remain_line)
	{
	    
	    if (absences_Right::RECOVERY === $right->getKind()) {
	    
	        // recovery right
	        $col = $this->recoveries[$right->quantity_unit];
	        if ($col > 3) {
	            $currentRow[$col] += $remain[$right->quantity_unit];
	        }
	    
	    
	    } else {
	    
	    
	        //regular right, add to specific column
	        $col = $this->rights[$right->id];
	    
	        if ($col > 3) {
	            $currentRow[$col] = $remain_line;
	        }
	    }
	}
	
	/**
	 * Complete current row with directory entry values
	 * @param int $id_user
	 * @param array &$currentRow
	 */
	private function addDirValues($id_user, Array &$currentRow)
	{
	    $agent = absences_Agent::getFromIdUser($id_user);
	    
	    foreach ($this->dirfields as $name => $dummy) {
	        $currentRow[] = $this->getAgentDirValue($agent, $name);
	    }
	}
	
	

	
	/**
	 * @return string
	 */
	private function getSeparator()
	{
	    switch((int) bab_pp('wsepar'))
	    {
	        case 0: return bab_pp('separ');
	        case 1: return ',';
	        case 2: return "\t";
	        case 3: return ';';
	    }
	}
	
	
	
	protected function getDate()
	{
	    $W = bab_Widgets();
	    return $W->DatePicker()->getISODate(bab_rp('date'));
	}
	
	
	/**
	 * @return array
	 */
	private function getHeader($year = '', $organization = '')
	{
	    global $babDB;
	    
	    $columns = $this->query($year, 'GROUP BY r.id', $organization);
	    $this->rights = array();
	    
	    
	    if ($this->dirfields && !empty($this->dirfields))
	    {
	        $ov_fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
	    }
	    
	    $header = array(
	        absences_translate('Lastname'),
	        absences_translate('Firstname'),
	        absences_translate('Total days'),
	        absences_translate('Total hours')
	    );
	    
	    
	    $this->recoveries = array(
	        'D' => null,
	        'H' => null
	    );
	    
	    $recovery = array(
	        'D' => false,
	        'H' => false
	    );
	    
	    while ($arr = $babDB->db_fetch_assoc($columns))
	    {
	        $agentRight = new absences_AgentRight();
	        $agentRight->setRow($arr);
	    
	        $right = $agentRight->getRight();
	    
	        if (absences_Right::RECOVERY === $right->getKind()) {
	            $recovery[$right->quantity_unit] = true;
	            continue;
	        }
	    
	        $header[] = $right->description.' ('.$right->getUnitLabel().')';
	        $this->rights[$arr['id_right']] = (count($header) -1);
	    }
	    
	    if ($recovery['D']) {
	        $header[] = absences_translate('Recoveries (days)');
	        $this->recoveries['D'] = (count($header) -1);
	    }
	    
	    if ($recovery['H']) {
	        $header[] = absences_translate('Recoveries (hours)');
	        $this->recoveries['H'] = (count($header) -1);
	    }
	    
	    foreach($this->dirfields as $name => $dummy)
	    {
	        $header[] = $ov_fields[$name]['name'];
	    }
	    
	    return $header;
	}
	


	public function csv()
	{

		$W = bab_Widgets();
		 
		if ($date = $W->DatePicker()->getISODate(bab_rp('date'))) {
		    if ('0000-00-00' !== $date) {
		        $this->date = $date;
		    }
		}
		 
		$this->dirfields = bab_rp('dirfields', array());

		
		
        $this->separator = $this->getSeparator();
        $this->sepdec = bab_rp('sepdec');
        $header = $this->getHeader(bab_rp('year'), bab_rp('organization'));
        

        $this->echoCsv($header);
		
	}
	
	
	
	
	
	private function echoCsv($header)
	{
	    global $babDB;
	    
	    $this->setHeaders(absences_translate("Vacation"));

	    $this->outputArr($header);

	    $rows = $this->query(bab_rp('year'), 'GROUP BY u.id, r.id', bab_rp('organization'));
	    
	    while ($arr = $babDB->db_fetch_assoc($rows))
	    {
	        if (null !== $line = $this->processRow($arr))
	        {
	            $this->outputArr($line);
	        }
	    }
	    
	    if (null !== $line = $this->processRow(null)) {
	        $this->outputArr($line);
	    }
	    
	    exit;
	}
	
	
	/**
	 * @return array
	 */
	private function getUserRow($id_user)
	{
	    global $babDB;
	    
	    $rows = $this->query('', 'GROUP BY u.id, r.id', '', $id_user);
	    
	    while ($arr = $babDB->db_fetch_assoc($rows))
	    {
	        if (null !== $line = $this->processRow($arr))
	        {
	            return $line;
	        }
	    }
	     
	    return $this->processRow(null);
	}
	
	
	
	/**
	 * @return Widget_Displayable_Interface
	 */
	public function compareDateForOneUser($id_user)
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	    
	    $this->dirfields = array();
	    
	    $header = $this->getHeader();
	    
	    
	    $yesterday = BAB_DateTime::now();
	    $yesterday->less(1, BAB_DATETIME_DAY);
	    
	    $this->date = $yesterday->getIsoDate();
	    $arr0 = $this->getUserRow($id_user);
	    
	    $this->date = date('Y-m-d');
	    $arr1 = $this->getUserRow($id_user);
	    
	    $this->date = null;
	    $arr2 = $this->getUserRow($id_user);
	    
	    
	    $W = bab_Widgets();
	    $table = $W->TableView();
	    
	    $row = 0;
	    
	    $table->addItem($W->Label(''), $row, 0 );
	    $table->addItem($W->Label('Yesterday'), $row, 1);
	    $table->addItem($W->Label('Today'), $row, 2);
	    $table->addItem($W->Label('No date'), $row, 3);
	    $row++;
	    
	    foreach ($header as $k => $label) {
	        
	        if ('"0,0"' === $arr1[$k] && '"0,0"' === $arr2[$k]) {
	            continue;
	        }
	        
	        $table->addItem($W->Label($label), $row, 0 );
	        $table->addItem($l0 = $W->Label($arr0[$k]), $row, 1);
	        $table->addItem($l1 = $W->Label($arr1[$k]), $row, 2);
	        $table->addItem($l2 = $W->Label($arr2[$k]), $row, 3);
	        
	        $values = array_unique(array($arr0[$k], $arr1[$k], $arr2[$k]));
	        
	        if (1 !== count($values)) {
	            $l0->addClass('widget-strong');
	            $l1->addClass('widget-strong');
	            $l2->addClass('widget-strong');
	        }
	        
	        $row++;
	    }
	    
	    return $table;
	}
}