<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/base.ui.php';


bab_Widgets()->includePhpClass('Widget_Form');


class absences_RightBaseEditor extends Widget_Form
{
	

	
	


	
	
	
	/**
	 * Disponibilite en fonction de la date de saisie de la demande de conges
	 */
	protected function by_request_input_date()
	{
		$W = bab_Widgets();
		$period = $W->PeriodPicker()->setNames('date_begin_valid', 'date_end_valid');
	
		return $W->VBoxItems(
				$W->Label(absences_translate('The right is available if the request is in the period')),
				$period,
				$W->Label(absences_translate('if empty, the right will be available with others conditions'))
		)->setVerticalSpacing(1,'em');
	}
	
	
	
	/**
	 * CET saving period
	 */
	protected function saving()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Saving period'),
				$W->PeriodPicker()->setNames('saving_begin', 'saving_end')
		);
	}
}


class absences_RightEditor extends absences_RightBaseEditor 
{
	/**
	 * 
	 * @var absences_Right
	 */
	protected $right;
	
	/**
	 * 
	 * @var Widget_VBoxLayout
	 */
	protected $fixed;
	
	/**
	 * 
	 * @var Widget_VBoxLayout
	 */
	protected $advanced_options;
	
	
	/**
     * @var Widget_VBoxLayout
	 */
	protected $no_distribution;
	
	/**
	 *
	 * @var Widget_VBoxLayout
	 */
	protected $availability_rules;
	
	
	/**
	 *
	 * @var Widget_VBoxLayout
	 */
	protected $test_other_types;
	
	
	/**
	 *
	 * @var Widget_VBoxLayout
	 */
	protected $cet;
	
	
	public function __construct(absences_Right $right = null)
	{
		$W = bab_Widgets();
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$this->right = $right;
		
		$this->setName('right');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
		
		$this->setCanvasOptions($this->Options()->width(70,'em'));
		
		
		// init frames dynamically displayed
		
		$this->quantity = $this->quantity();
		$this->advanced_options = $W->FlowItems($this->cbalance(), $this->require_approval(), $this->use_in_cet())->setSpacing(1,'em', 4,'em');
		$this->no_distribution = $this->no_distribution();
		$this->availability_rules = $this->availability_rules();
		$this->fixed = $this->fixed();
		$this->cet = $this->cet();
		$this->inc_month = $this->inc_month();
		$this->report = $this->report();
		
		$this->test_other_types = $W->VBoxItems($this->alert(), $this->dynamic_configuration())->setVerticalSpacing(1,'em');
		
		$this->addFields();
		$this->loadFormValues();
		
		$this->addButtons();
		$this->setRightHiddenFields();

	}
	
	
	protected function setRightHiddenFields()
	{
	    $arr = array();
	    $this->getSelfPageHiddenFields($arr);
	    $this->setRightHiddenValues($arr);
	}
	
	
	protected function setRightHiddenValues($arr)
	{
	    foreach ($arr as $name => $value) {
	        $this->setHiddenValue($name, $value);
	    }
	}
	
	
	
	/**
	 * Updates the content of hiddenFields array with values from _GET and _POST matching the fields of the form $id.
	 *
	 * @param array $hiddenFields
	 */
	protected function getSelfPageHiddenFields(&$hiddenFields)
	{
	    $W = bab_Widgets();
	    $canvas = $W->HtmlCanvas();
	    $context = array_keys($_POST + $_GET);
	    $form = $this;
	     
	    $fnames = array();
	    
	    
	    foreach ($form->getFields() as $f) {
	        $htmlname = $canvas->getHtmlName($f->getFullName());
	        if (!empty($htmlname)) {
	            $fnames[] = $htmlname;
	        }
	    }
	     
	    $context = array_diff($context, $fnames);
	     
	    foreach ($context as $fieldname) {
	        $value = bab_rp($fieldname);
	        if (!is_array($value) && !isset($hiddenFields[$fieldname])) {
	            $hiddenFields[$fieldname] = $value;
	        }
	    }
	}
	
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		$this->addItem($W->FlowItems($this->kind(), $this->active())->setHorizontalSpacing(3,'em')->setVerticalAlign('bottom'))
			->addItem($this->description())
			->addItem($this->theoretical_period())
			->addItem($W->FlowItems($this->id_type(), $this->id_rgroup())->setHorizontalSpacing(3,'em'))
			->addItem($this->quantity)
			->addItem($this->inc_month)
			->addItem($this->advanced_options)
			->addItem($this->no_distribution)
			
			->addItem($this->fixed)
			->addItem($this->cet)
			
			->addItem($this->availability_rules)
			->addItem($this->report)
			->addItem($this->test_other_types)
			
		;
	}
	
	
	protected function hide_empty()
	{
	    $W = bab_Widgets();
	    
	    return $W->LabelledWidget(
	        absences_translate('Hide the right when the quantity is 0 or negative'),
	        $W->CheckBox(),
	        __FUNCTION__
	    );
	}
	
	
	protected function setRightId()
	{
	    if (isset($this->right) && !empty($this->right->id))
	    {
	        $this->setHiddenValue('right[id]', $this->right->id);
	    }
	}
	
	
	
	protected function loadFormValues()
	{
		$this->setRightId();
		
		if (isset($_POST['right']))
		{
			$values = $_POST['right'];
		} else if (isset($this->right))
		{
			$values = $this->right->getRow();
			$rules = $this->right->getRightRule();
			if ($rules_values = $rules->getRow())
			{
				$values = array_merge($values, $rules_values);
			}
			
			$cet = $this->right->getRightCet();
			if ($cet_values = $cet->getRow())
			{
				$values = array_merge($values, $cet_values);
			}
			
			list($values['datebeginfx'], $values['hourbeginfx']) = explode(' ',$values['date_begin_fixed']);
			list($values['dateendfx'], $values['hourendfx']) = explode(' ',$values['date_end_fixed']);
			
			if (isset($values['min_use']))
			{
				$test_min_use = (int) round($values['min_use'] * 10);
				
				if (0 === $test_min_use)
				{
					$values['min_use_opt'] = 0;
				} else if ($test_min_use < 0) {
					$values['min_use_opt'] = -1;
				} else if ($test_min_use > 0) {
					$values['min_use_opt'] = 1;
				}
			}
			
			if ('0000-00-00' !== $values['date_end_report'])
			{
				$values['report'] = 1;
			}
			
			$values['quantity_alert_types'] = explode(',',$values['quantity_alert_types']);
			$values['dynconf_types'] = explode(',',$values['dynconf_types']);
			
		} else {
			$values = array(
				'active' => 'Y',	
				'id_type' => bab_rp('idtype'),
				'delay_before' => 0
			);
		}

		
		$this->setValues(array('right' => $values));
	}
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
		
		$button = $W->FlowItems(
			$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
			$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
		
		if (isset($this->right))
		{
			$button->addItem(
				$W->SubmitButton()
					->setConfirmationMessage(absences_translate('Do you really want to delete the vacation right?'))
					->setName('delete')->setLabel(absences_translate('Delete'))
			);
		}
		
		$this->addItem($button);
	}
	
	
	
	protected function description()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
				absences_translate('Right name'),
				$W->LineEdit()->setSize(60)->setMaxSize(255)->setMandatory(true, absences_translate('The name is mandatory')),
				__FUNCTION__
		);
	}
	
	
	protected function kind()
	{
		$W = bab_Widgets();
		
		$select = $W->Select()->setOptions(absences_kinds());
		
		$select->setAssociatedDisplayable($this->quantity, array(absences_Right::REGULAR, absences_Right::INCREMENT, absences_Right::FIXED, absences_Right::RECOVERY));
		$select->setAssociatedDisplayable($this->advanced_options, array(absences_Right::REGULAR, absences_Right::INCREMENT));
		$select->setAssociatedDisplayable($this->no_distribution, array(absences_Right::REGULAR, absences_Right::CET, absences_Right::INCREMENT));
		$select->setAssociatedDisplayable($this->availability_rules, array(absences_Right::REGULAR, absences_Right::CET, absences_Right::INCREMENT, absences_Right::RECOVERY));
		$select->setAssociatedDisplayable($this->fixed, array(absences_Right::FIXED));
		$select->setAssociatedDisplayable($this->cet, array(absences_Right::CET));
		$select->setAssociatedDisplayable($this->inc_month, array(absences_Right::INCREMENT));
		$select->setAssociatedDisplayable($this->report, array(absences_Right::REGULAR, absences_Right::INCREMENT));
		$select->setAssociatedDisplayable($this->test_other_types, array(absences_Right::REGULAR, absences_Right::INCREMENT, absences_Right::RECOVERY));
		return $W->LabelledWidget(
				absences_translate('Right kind'),
				$select,
				__FUNCTION__
		);
	}
	
	
	/**
	 * quantity & quantity_unit
	 * @return Widget_FlowLayout
	 */
	protected function quantity()
	{
		$W = bab_Widgets();
	
		$lineedit = $W->LineEdit()->setSize(5)->setMaxSize(5)->setMandatory(true, absences_translate('The quantity is mandatory'))->setName('quantity');
		$select = $W->Select()->setName('quantity_unit')
    		->addOption('D', absences_translate('Day(s)'))
    		->addOption('H', absences_translate('Hour(s)'));
	
		$vbox = $W->VBoxItems(
				$W->Label(absences_translate('Quantity'))->setAssociatedWidget($lineedit),
				$W->FlowItems($lineedit, $select)
		)->setVerticalSpacing(.2,'em');
		
		if (isset($this->right) && absences_Right::INCREMENT === $this->right->getKind()) {
    		$monthlyUpdates = $W->FlowItems(
    		    $W->Label(absences_translate('Additional quantity by monthly updates'))->colon(),
    		    $W->Label('+'.absences_quantity($this->right->getIncrementQuantity(), $this->right->quantity_unit))
    		)->setSpacing(.5, 'em');
    		
    		$vbox->addItem($monthlyUpdates);
		}
		
		return $vbox;
	}
	
	
	private function type_select($empty = false)
	{
		$W = bab_Widgets();
		global $babDB;
		
		$select = $W->Select();
		
		if ($empty)
		{
			$select->addOption('0', '');
		}
		
		$res = $babDB->db_query('SELECT id, name FROM absences_types ORDER BY name');
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$select->addOption($arr['id'], $arr['name']);
		}
		
		return $select;
	}
	
	
	private function multiType()
	{
		$W = bab_Widgets();
		global $babDB;
	
		$select = $W->Multiselect();

	
		$res = $babDB->db_query('SELECT id, name FROM absences_types ORDER BY name');
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$select->addOption($arr['id'], $arr['name']);
		}
	
		return $select;
	}
	
	
	
	
	/**
	 * 
	 * @return Widget_LabelledWidget
	 */
	protected function id_type()
	{
		$W = bab_Widgets();

		return $W->LabelledWidget(
			absences_translate('Right type'),
			$this->type_select(),
			__FUNCTION__
		);
	}
	
	
	
	/**
	 *
	 * @return Widget_LabelledWidget
	 */
	protected function id_report_type()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
				absences_translate('Type for report rights'),
				$this->type_select(true),
				__FUNCTION__
		);
	}
	
	
	/**
	 *
	 * @return Widget_LabelledWidget
	 */
	protected function date_end_report()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
				absences_translate('Report availability end'),
				$W->DatePicker(),
				__FUNCTION__
		);
	}
	
	
	protected function description_report()
	{
		$W = bab_Widgets();
	
		return $W->LabelledWidget(
				absences_translate('Report right name'),
				$W->LineEdit()->setSize(60)->setMaxSize(255),
				__FUNCTION__
		);
	}
	
	
	
	protected function report()
	{
		$W = bab_Widgets();
		
		$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		$morefields = $W->FlowItems(
			$this->description_report(),
			$this->id_report_type(), 
			$this->date_end_report(),
			$W->Icon(absences_translate('The report will be created if the end date of right visibility is in the past and if there is unused quantity left'), Func_Icons::STATUS_DIALOG_QUESTION)
		)->setSpacing(1,'em')->addClass(Func_Icons::ICON_LEFT_24);
		
		$report = $W->LabelledWidget(
				absences_translate('Create a report for lasting right after the expiry'),
				$W->CheckBox()->setAssociatedDisplayable($morefields, array('1')),
				__FUNCTION__
		);
		
		$layout->addItem($report);
		$layout->addItem($morefields);
		
		return $layout;
	}
	
	
	
	
	protected function alert()
	{
		$W = bab_Widgets();
		
		$quantity_alert_days = $W->LineEdit()->setSize(6)->setMaxSize(10)->setName('quantity_alert_days');
		$quantity_alert_types = $this->multiType()->setName('quantity_alert_types');
		$period = $W->PeriodPicker()->setNames('quantity_alert_begin', 'quantity_alert_end');
		
		$flow = $W->FlowItems(
			$W->Label(absences_translate('Alert if the user has consumed more than')), 
			$quantity_alert_days, 
			$W->Label(absences_translate('days of')), 
			$quantity_alert_types
		)->setSpacing(.2,'em');
		
		$morefields = $W->VBoxItems(
			$flow,
			$W->VBoxItems($W->Label(absences_translate('Among the absences within the period'))->setAssociatedWidget($period), $period)->setVerticalSpacing(.5,'em')
		)->setVerticalSpacing(1,'em');
		
		return $W->Section(absences_translate('Alerting depending on the consumed quantity'), $morefields, 6)->setFoldable(true, true);
	}
	
	
	
	private function createDuplicableDynconf($index, absences_DynamicConfiguration $dynconf = null)
	{
		$W = bab_Widgets();
		$duplicable = $W->Frame(null, $W->FlowLayout()->setSpacing(.5,'em'))->setName(array('dynconf', (string) $index));
	
		$select = $W->Select()
			->addOption('-', absences_translate('Remove'))
			->addOption('+', absences_translate('Add'))
			->setName('sign');
	
		$test_quantity = $W->LineEdit()->setSize(6)->setMaxSize(10)->setName('test_quantity');
		$quantity = $W->LineEdit()->setSize(6)->setMaxSize(10)->setName('quantity');
		
		if (isset($dynconf))
		{
			// the edit quantity unit is not known here
			$quantity->setValue(absences_editQuantity(abs((float) $dynconf->quantity)));
			if ($dynconf->quantity > 0)
			{
				$select->setValue('+');
			} else {
				$select->setValue('-');
			}
			
			$test_quantity->setValue(absences_editQuantity($dynconf->test_quantity));
		}
		
		$duplicable->addItem($W->Label(absences_translate('If quantity taken is beyond')));
		$duplicable->addItem($test_quantity);
		$duplicable->addItem($W->Label(absences_translate('day(s), ')));
		$duplicable->addItem($select)->addItem($quantity);
		$duplicable->addItem($W->Button()->addItem($W->Icon('', Func_Icons::ACTIONS_LIST_REMOVE)));
	
		return $duplicable;
	}
	
	
	/**
	 *
	 * @param Widget_VBoxLayout $list
	 * @return bool
	 */
	private function addDuplicableDynconfToList($list)
	{
		if (!isset($this->right))
		{
			return false;
		}
	
		global $babDB;
	
		$res = $this->right->getDynamicConfigurationIterator();
	
		if (!$res->count())
		{
			return false;
		}
	
		$index = 0;
		foreach ($res as $dynconf)
		{
			$dupli = $this->createDuplicableDynconf($index++, $dynconf);
			$list->addItem($dupli);
		}
	
	
		return true;
	}
	
	
	
	protected function dynamic_configuration()
	{
		$W = bab_Widgets();
		
		
		
		$quantity_alert_types = $this->multiType()->setName('dynconf_types');
		$period = $W->PeriodPicker()->setNames('dynconf_begin', 'dynconf_end');
		
		$comoncriteria = $W->FlowItems(
				$W->Label(absences_translate('Test days of type')),
				$quantity_alert_types,
				$W->VBoxItems($W->Label(absences_translate('Among the absences within the period'))->setAssociatedWidget($period), $period)->setVerticalSpacing(.5,'em')
		)->setSpacing(1,'em')->setVerticalAlign('middle');
	
		
		
		$list = $W->VBoxLayout('absences_dynconf_rows');
		
		if (!$this->addDuplicableDynconfToList($list))
		{
			$list->addItem($this->createDuplicableDynconf(0));
		}

		$morefields = $W->VBoxItems(
			$comoncriteria,
			$W->VBoxItems(
				$list,
				$W->Button('absences_dynconf_add')->addItem($W->Icon(absences_translate('Add a test'), Func_Icons::ACTIONS_LIST_ADD))
			)->addClass('widget-bordered'),
			$W->Icon(absences_translate('The added or removed quantity is in the same unit as the vacation right'), Func_Icons::STATUS_DIALOG_INFORMATION)
		)->setVerticalSpacing(.5,'em');
		
		
		
		return $W->Section(absences_translate('Change available quantity depending on the consumed quantity'), $morefields, 6)->setFoldable(true, true)->addClass(Func_Icons::ICON_LEFT_16);
		
		
	}
	
	
	
	
	/**
	 * Right group
	 * @return Widget_LabelledWidget
	 */
	protected function id_rgroup()
	{
		$W = bab_Widgets();
		global $babDB;
	
		$select = $W->Select();
		$select->addOption(0, absences_translate('None'));
	
		$res = $babDB->db_query('SELECT id, name FROM absences_rgroup ORDER BY name');
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$select->addOption($arr['id'], $arr['name']);
		}
	
		return $W->LabelledWidget(
				absences_translate('Right group'),
				$select,
				__FUNCTION__
		);
	}
	
	
	
	/**
	 * Periode theorique du droit
	 */
	protected function theoretical_period()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			absences_translate('Right theoretical period'),
			$W->PeriodPicker()->setNames('date_begin', 'date_end')
		);
	}

	
	
	/**
	 * 
	 */
	protected function active()
	{
		$W = bab_Widgets();
		
		/*
		return $W->LabelledWidget(
			absences_translate('Active'),
			$W->Checkbox()->setCheckedValue('Y')->setUncheckedValue('N'),
			__FUNCTION__
		);
		*/
		return $W->LabelledWidget(
				absences_translate('Active'),
				$W->Select()->setOptions(array(
						'Y' => absences_translate('Yes, usable by appliquant'),
						'N' => absences_translate('No, only for managers or delegated managers')
				)),
				__FUNCTION__
		);
	}
	
	
	
	/**
	 * 
	 */
	protected function cbalance()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Accept negative balance'),
				$W->Select()->setOptions(array(
					'Y' => absences_translate('Yes'),
					'N' => absences_translate('No')
				)),
				__FUNCTION__
		);
	}
	
	/**
	 *
	 */
	protected function no_distribution()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Distribution on request'),
				$W->Select()->setOptions(array(
					'0' => absences_translate('Yes'),
					'1' => absences_translate('No')
				)),
				__FUNCTION__
		);
	}
	
	
	/**
	 *
	 */
	protected function use_in_cet()
	{
		$W = bab_Widgets();
		
		$cet_quantity = $W->Frame();
		$cet_quantity->addItem($this->cet_quantity());
		
		$use_in_cet = $W->LabelledWidget(
				absences_translate('Allow moving to the time savings account'),
				$W->Select()->setOptions(array(
						'1' => absences_translate('Yes'),
						'0' => absences_translate('No')
				))->setAssociatedDisplayable($cet_quantity, array('1')),
				__FUNCTION__
		);
		
		
		return $W->HBoxItems($use_in_cet, $cet_quantity)->setHorizontalSpacing(3,'em');
		
	}
	
	
	/**
	 *
	 */
	protected function cet_quantity()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Saving quantity'),
				$W->LineEdit()->setSize(6),
				__FUNCTION__,
				absences_translate('in the same unit as right quantity')
		);
	}
	
	
	protected function require_approval()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Require approval'),
				$W->Select()->setOptions(array(
						'1' => absences_translate('Yes'),
						'0' => absences_translate('No')
				)),
				__FUNCTION__
		);
	}
	
	
	/**
	 * Interface for FIXED right
	 * @return Widget_VBoxLayout
	 */
	protected function fixed()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$W->Label(absences_translate('This vacation period will be set for all right beneficiaries')),
			$W->FlowItems($this->beginfx(), $this->endfx())	
		)->setVerticalSpacing(1,'em');
	}
	
	
	
	
	protected function beginfx()
	{
		$W = bab_Widgets();
		$date = $W->DatePicker()->setMandatory(true)->setName('datebeginfx');
		$hours = $W->Select()->setName('hourbeginfx')->setOptions(absences_hoursList());
		
		return $W->FlowItems(
			$W->Label(absences_translate('from date'))->setAssociatedWidget($date),
			$date,
			$hours
		)->setSpacing(.5,'em');
	}
	
	
	protected function endfx()
	{
		$W = bab_Widgets();
		$date = $W->DatePicker()->setMandatory(true)->setName('dateendfx');
		$hours = $W->Select()->setName('hourendfx')->setOptions(absences_hoursList());
		
		return $W->FlowItems(
			$W->Label(absences_translate('to date'))->setAssociatedWidget($date),
			$date,
			$hours
		)->setSpacing(.5,'em');
	}
	
	
	protected function inc_month()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
				$this->quantity_inc_month(),
				$this->quantity_inc_max()
		)->setVerticalSpacing(1,'em');
	}
	
	
	protected function quantity_inc_month()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Quantity to add each month'),
				$W->LineEdit()->setSize(6)->setMaxSize(10)->setMandatory(true, absences_translate('The quantity per month is mandatory')),
				__FUNCTION__,
				absences_translate('The quantity will grow each month before the end date from "Availability depends on the requested period dates"')
		);
	}
	
	
	protected function quantity_inc_max()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Maximum quantity'),
				$W->LineEdit()->setSize(6)->setMaxSize(10),
				__FUNCTION__,
				absences_translate('The quantity will grow each month up to this limit')
		);
	}
	
	
	/**
	 * parametres specifiques pour les CET
	 */
	protected function cet()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
			$this->saving(),
			$this->per_year(),
		//	$this->per_cet(), TODO comment ce critere peut-il fonctionner ? cela devrait etre un plafont par utilisateur en plus pour ne pas faire doublon avec le plafont du CET configure au niveau du droit
			$this->ceiling(),
			$this->min_use()
		)->setVerticalSpacing(1,'em');
	}
	
	
	

	
	
	/**
	 * CET max days saving per years
	 */
	protected function per_year()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Maximum savings per year'),
				$W->LineEdit()->setSize(6)->setMaxSize(10),
				__FUNCTION__,
				null,
				absences_translate('day(s)')
		);
	}
	
	
	/**
	 * CET max days saving for CET period
	 */
	protected function per_cet()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Maximum savings for the account saving time period'),
				$W->LineEdit()->setSize(6)->setMaxSize(10),
				__FUNCTION__,
				null,
				absences_translate('day(s)')
		);
	}
	
	
	/**
	 * CET ceiling
	 */
	protected function ceiling()
	{
		$W = bab_Widgets();
		return $W->LabelledWidget(
				absences_translate('Maximum savings in the account saving time'),
				$W->LineEdit()->setSize(6)->setMaxSize(10),
				__FUNCTION__,
				null,
				absences_translate('day(s)')
		);
	}
	
	
	
	/**
	 * CET minimal use
	 */
	protected function min_use()
	{
		$W = bab_Widgets();
		
		$select = $W->Select()
			->addOption('0', absences_translate('No constraint'))
			->addOption('-1', absences_translate('Force the use of the total in one take'))
			->addOption('1', absences_translate('Set the minimum to use in one take'));
		
		
		$options = $W->LabelledWidget(
				absences_translate('Constraint when using the account'),
				$select,
				'min_use_opt'
		);
		
		$min_use = $W->LabelledWidget(
				absences_translate('Minimum allowed'),
				$W->LineEdit()->setSize(6)->setMaxSize(10),
				__FUNCTION__,
				null,
				absences_translate('day(s)')
		);
		
		
		$displayable = $W->Frame()->addItem($min_use);
		
		
		$select->setAssociatedDisplayable($displayable, array('1'));
		
		
		return $W->HBoxItems($options, $displayable)->setHorizontalSpacing(3,'em');
	}
	
	
	
	
	/**
	 * Criteres de disponibilite d'un droit
	 */
	protected function availability_rules()
	{
		$W = bab_Widgets();
		
		
	
		return $W->Accordions()
			->addPanel(absences_translate('Availability depends on the date of the vacation request input'), $this->by_request_input_date())
			->addPanel(absences_translate('Availability depends on the requested period dates'), $this->by_requested_period())
			->addPanel(absences_translate('Right assignement in function of requested days'), $this->by_confirmed_quantity())
			->addPanel(absences_translate('Availability depends on an earlier date'), $this->by_earlier_date())
			->addPanel(absences_translate('Availability depends on an later date'), $this->by_later_date())
			->addPanel(absences_translate('Availability depends on the interval between the requested period dates and the current date'), $this->by_request_date())
		;
	}
	
	
	
	
	
	private function createDuplicable($index, $values = null)
	{
		$W = bab_Widgets();
		$duplicable = $W->Frame(null, $W->FlowLayout()->setSpacing(.5,'em'))->setName(array('inperiod', (string) $index));
		
		$select = $W->Select()
		->addOption(1, absences_translate('In rule period'))
		->addOption(2, absences_translate('Out of rule period'))
		->setName('right_inperiod');
		
		
		
		$periodPicker = $W->PeriodPicker()->setNames('period_start', 'period_end');
		
		if (isset($values))
		{
			$select->setValue($values['right_inperiod']);
			$periodPicker->setValues($values['period_start'], $values['period_end']);
		}
		
		$duplicable->addItem($select);
		$duplicable->addItem($W->Label(absences_translate('in this period'))->colon());
		$duplicable->addItem($periodPicker);
		$duplicable->addItem($W->Button()->addItem($W->Icon('', Func_Icons::ACTIONS_LIST_REMOVE)));
		
		return $duplicable;
	}
	
	
	/**
	 * 
	 * @param Widget_VBoxLayout $list
	 * @return bool
	 */
	private function addDuplicableToList($list)
	{
		if (!isset($this->right))
		{
			return false;
		}
		
		global $babDB;
		
		$res = $this->right->getRightRule()->getInPeriodRes();
		
		if (!$res || 0 === $babDB->db_num_rows($res))
		{
			return false;
		}
		
		$index = 0;
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$dupli = $this->createDuplicable($index++, $arr);
			$list->addItem($dupli);
		}
		
		
		return true;
	}
	
	
	
	/**
	 * Disponibilite en fonction de la periode de conges demandee
	 * @return Widget_VBoxLayout
	 */
	protected function by_requested_period()
	{
		$W = bab_Widgets();
		bab_functionality::includefile('Icons');
		
		
		
		
		$validoverlap = $W->LabelledWidget(
				absences_translate('Allow overlap between the request period and the test periods'),
				$W->CheckBox(),
				'validoverlap'
		);
		
		$list = $W->VBoxLayout('absences_dupli_periods');
		
		if (!$this->addDuplicableToList($list))
		{
			$list->addItem($this->createDuplicable(0));
		}
		
		return $W->VBoxItems(
				$W->Label(absences_translate('The right is available if the vacation request is')),
				$list,
				$W->Button('absences_dupli_add')->addItem($W->Icon(absences_translate('Add a test period'), Func_Icons::ACTIONS_LIST_ADD)),
				$validoverlap
		)->setVerticalSpacing(1,'em')->addClass(Func_Icons::ICON_LEFT_16);
	}
	
	
	/**
	 * Attribution du droit en fonction des jours demandes et valides
	 */
	protected function by_confirmed_quantity()
	{
		global $babDB;
		$W = bab_Widgets();
		
		$types = $W->Select()->setName('trigger_type');
		$res = $babDB->db_query('SELECT id, name FROM absences_types ORDER BY name');
		$types->addOption(0, absences_translate('All'));
		while($arr = $babDB->db_fetch_assoc($res))
		{
			$types->addOption($arr['id'], $arr['name']);
		}
		
		
		return $W->VBoxItems(
				$W->Label(absences_translate('The right is displayed if the user has requested')),
				$W->FlowItems(
					$W->Label(absences_translate('at least'))->colon(), 
					$W->LineEdit()->setSize(5)->setMaxSize(10)->setName('trigger_nbdays_min'),
					$W->Label(absences_translate('day(s)')),
					$W->Label(absences_translate('but less than'))->colon(),
					$W->LineEdit()->setSize(5)->setMaxSize(10)->setName('trigger_nbdays_max'),
					$W->Label(absences_translate('day(s)'))
				)->setSpacing(.5,'em'),
				$W->FlowItems($W->Label(absences_translate('vacation of type'))->colon(), $types),
				$W->LabelledWidget(absences_translate('First test period'), $W->PeriodPicker()->setNames('trigger_p1_begin', 'trigger_p1_end')),
				$W->LabelledWidget(absences_translate('Second test period'), $W->PeriodPicker()->setNames('trigger_p2_begin', 'trigger_p2_end')),
				$W->LabelledWidget(absences_translate('Use vacation requests witch overlap the test period'), $W->CheckBox(), 'trigger_overlap'),
		        $this->hide_empty()
				
		)->setVerticalSpacing(1,'em');
	}
	
	
	/**
	 * A select with directory fields
	 * @return Widget_Select
	 */
	private function dirFieldsSelect()
	{
		$W = bab_Widgets();
		$select = $W->Select();
		
		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
		$select->addOption('', '');
		
		foreach($fields as $fieldname => $arr)
		{
			$select->addOption($fieldname, $arr['name']);
		}
		
		return $select;
	}
	
	protected function by_earlier_date()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
				$W->VBoxItems(
					$W->FlowItems(
						$W->Label(absences_translate('The right is granted if the field')),
						$this->dirFieldsSelect()->setName('earlier'),
						$W->Label(absences_translate('is set with a date in the user directory entry'))
					)->setSpacing(.5,'em'),
					$W->Label(absences_translate('and the day of the request is:')),
					
					$W->HBoxItems(
						$W->Label(absences_translate('at least')),
						$W->LineEdit()->setSize(3)->setMaxSize(10)->setName('earlier_begin_valid'),
						$W->Label(absences_translate('year(s) after the date set in the directory entry'))
					)->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle'),
					$W->HBoxItems(
						$W->Label(absences_translate('and at more')),
						$W->LineEdit()->setSize(3)->setMaxSize(10)->setName('earlier_end_valid'),
						$W->Label(absences_translate('year(s) after the date set in the directory entry'))
					)->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle')
				)->setSpacing(1,'em'),
				
				$W->Icon(absences_translate('Example: use a field with the user birthday, the criteria will be an age range'), Func_Icons::STATUS_DIALOG_QUESTION)
		
		)->setVerticalSpacing(1,'em')->addClass(Func_Icons::ICON_LEFT_24);
	}
	
	protected function by_later_date()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
				$W->VBoxItems(
						$W->FlowItems(
								$W->Label(absences_translate('The right is granted if the field')),
								$this->dirFieldsSelect()->setName('later'),
								$W->Label(absences_translate('is set with a date in the user directory entry'))
						)->setSpacing(.5,'em'),
						$W->Label(absences_translate('and the day of the request is:')),
		
						$W->HBoxItems(
								$W->Label(absences_translate('at least')),
								$W->LineEdit()->setSize(3)->setMaxSize(10)->setName('later_begin_valid'),
								$W->Label(absences_translate('year(s) before the date set in the directory entry'))
						)->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle'),
						$W->HBoxItems(
								$W->Label(absences_translate('and at more')),
								$W->LineEdit()->setSize(3)->setMaxSize(10)->setName('later_end_valid'),
								$W->Label(absences_translate('year(s) before the date set in the directory entry'))
						)->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle')
				)->setSpacing(1,'em'),
				
				$W->Icon(absences_translate('Example: use a field with the user retirment projected date, it will be possible to assign the right in the last years of the employee'), Func_Icons::STATUS_DIALOG_QUESTION)
		
		)->setVerticalSpacing(1,'em')->addClass(Func_Icons::ICON_LEFT_24);
	}
	
	
	
	/**
	 * Disponibilite en fonction de la date de la demande de conges et de la date de saisie
	 */
	protected function by_request_date()
	{
		$W = bab_Widgets();
	
		return $W->FlowItems(
				$W->Label(absences_translate('The right is available if the request start date is at least')),
				$W->LineEdit()->setSize(3)->setMaxSize(10)->setName('delay_before'),
				$W->Label(absences_translate('days after the current date. (0: test disabled)'))
		)->setVerticalAlign('middle')->setHorizontalSpacing(.25, 'em');
	}
	
	
	
	public function display(Widget_Canvas $canvas)
	{
		$html = parent::display($canvas);
		$html.= $canvas->loadScript($this->getId(), bab_getAddonInfosInstance('absences')->getTemplatePath().'righteditor.jquery.js');
		
		return $html;
	}
}



class absences_CreateRightEditor extends absences_RightEditor
{
    protected function setRightHiddenFields()
    {
        $arr = array();
        $this->getSelfPageHiddenFields($arr);
        unset($arr['from']);
        $this->setRightHiddenValues($arr);
    }
    
    protected function setRightId()
    {
        
    }
}



/**
 * Add personnel tu right by user group
 *
 */
class absences_RightAddPersonnelEditor extends Widget_Form 
{
	/**
	 * 
	 * @var absences_Right
	 */
	protected $right;
	

	
	public function __construct(absences_Right $right = null)
	{
		$W = bab_Widgets();
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$this->right = $right;
		
		$this->setName('right');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
		
		$this->setCanvasOptions($this->Options()->width(70,'em'));
		
		
		
		$this->addFields();
		
		$this->addButtons();
		$this->setSelfPageHiddenFields();
		
		$this->setHiddenValue('right[id]', $right->id);
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		$this->addItem($W->LabelledWidget(absences_translate('Grant the right by using the group'), $W->GroupPicker(), 'group'));
		
		$this->addItem($W->LabelledWidget(absences_translate('Use the group and his childen'), $W->Checkbox(), 'tree'));
	}
	

	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}

}





class absences_RightCardFrame extends absences_CardFrame
{
	protected $right;
	
	public function __construct(absences_Right $right, $layout = null)
	{
		$W = bab_Widgets();
	
		if (null === $layout)
		{
			$layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
		}
		
	
		parent::__construct(null, $layout);
	
		$this->right = $right;
	
		$this->loadPage();
	}
	
	
	protected function initClasses()
	{
		$this->addClass('absences-right-cardframe');
		$this->addClass(Func_Icons::ICON_LEFT_24);
		
	}
	
	
	protected function loadPage()
	{
		$this->initClasses();
		$this->addColHead();

		
		if ($conditions = $this->conditions())
		{
			$this->addItem($conditions);
		}
	}
	
	
	protected function addColHead()
	{
		$W = bab_Widgets();
		
		$icon = $W->Frame();
		
		$type = $this->right->getType();
		if ($type->getRow())
		{
			$icon->setCanvasOptions($icon->Options()->width(20,'px')->height(20,'px')->backgroundColor('#'.$type->color));
		} else {
			$icon = null;
		}
		
		$this->addItem($W->HBoxItems($icon, $W->Title($this->right->description, 2))->setHorizontalSpacing(.5,'em')->setVerticalAlign('middle'));
		
		
		$this->addItem($W->HBoxItems(
				$col1 = $W->VBoxLayout()->setVerticalSpacing(.2,'em'),
				$col2 = $W->VBoxLayout()->setVerticalSpacing(.2,'em')
		)->setHorizontalSpacing(3,'em'));
		
		
		$col1->addItem(
				$this->titledLabel(
						absences_translate('Right kind'),
						$this->right->getKindLabel()
				)
		);
		
		if ($type->getRow())
		{
			$col1->addItem(
					$this->titledLabel(
							absences_translate('Type'),
							$this->right->getType()->name
					)
			);
		}
		
		if (absences_Right::CET !== $this->right->getKind())
		{
			$col1->addItem(
					$this->titledLabel(
							absences_translate('Quantity'),
							absences_quantity($this->right->quantity, $this->right->quantity_unit)
					)
			);
		}
		
		
		if (absences_Right::INCREMENT === $this->right->getKind())
		{
		    $col1->addItem(
		        $this->titledLabel(
		            absences_translate('Quantity to add each month'),
		            absences_quantity($this->right->quantity_inc_month, $this->right->quantity_unit)
		        )
		    );
		    
		    
		    $col1->addItem(
		        $this->titledLabel(
		            absences_translate('Do not increment more than'),
		            absences_quantity($this->right->quantity_inc_max, $this->right->quantity_unit)
		        )
		    );
		    
		    
		    $col1->addItem(
		        $this->titledLabel(
		            absences_translate('Additional quantity by monthly updates'),
		            '+'.absences_quantity($this->right->getIncrementQuantity(), $this->right->quantity_unit)
		        )
		    );
		    
		    
		}
		
		
		$col1->addItem(
				$this->titledLabel(
						absences_translate('Status'),
						$this->right->getStatus()
				)
		);
		
		
		if ($this->right->renewal_parent) {
		    
		    $parent = new absences_Right($this->right->renewal_parent);
		    
		    if ($parent->getRow()) {
		    
    		    $addon = bab_getAddonInfosInstance('absences');
    		    
        		$col1->addItem(
        		    $W->FlowItems(
        		        $W->Label(absences_translate('Previous right in the renewal history'))->colon()->addClass('widget-strong'),
        		        $W->Link($parent->description, $addon->getUrl().'vacadma&idx=viewvr&idvr='.$parent->id)
        		    )->addClass('widget-small')->setSpacing(.2,'em', .3,'em')
        		);
		    }
		}
		
		
		
		$col2->addItem(
				$this->titledLabel(
						absences_translate('Author'),
						bab_getUserName($this->right->id_creditor)
				)
		);
		
		
		if ('0000-00-00 00:00:00' !== $this->right->createdOn) {
    		$col2->addItem(
    		    $this->titledLabel(
    		        absences_translate('Creation date'),
    		        bab_shortDate(bab_mktime($this->right->createdOn), false)
    		    )
    		);
		}
		
		
		$col2->addItem(
			$this->titledLabel(
				absences_translate('Last modification date'),
				bab_shortDate(bab_mktime($this->right->date_entry), false)
			)
		);
		
		
		if ($theoretical_period = $this->getTheoreticalPeriod())
		{
			$col2->addItem($theoretical_period);
		}
		
		
		if ($fixed_period = $this->getFixedPeriod())
		{
			$col2->addItem($fixed_period);
		}
		
		
		// CET
		$rightCet = $this->right->getRightCet();
		if ($rightCet->getRow())
		{
			if ($p = $this->getSavingPeriod($rightCet))
			{
				$col1->addItem($p);
			}
			
			if (0 !== (int) round(10*$rightCet->per_year))
			{
				$col2->addItem(
					$this->titledLabel(
						absences_translate('Maximum savings per year'),
						sprintf(absences_translate('%s days'), $rightCet->per_year)
					)
				);
			}
			
			
			if (0 !== (int) round(10*$rightCet->ceiling))
			{
				$col2->addItem(
					$this->titledLabel(
						absences_translate('Maximum savings in the account saving time'),
						sprintf(absences_translate('%s days'), $rightCet->ceiling)
					)
				);
			}
			
			// TODO others CET fields


		}
		
		
		
		
		
		
	}
	
	
	
	
	protected function getPeriod($begin, $end, $hours = true)
	{
		return sprintf(
			absences_translate('from %s to %s'), 
			bab_shortDate(bab_mktime($begin), $hours), 
			bab_shortDate(bab_mktime($end), $hours)
		);
	}
	
	
	protected function getTheoreticalPeriod()
	{
		if ('0000-00-00' === $this->right->date_begin || '0000-00-00' === $this->right->date_end)
		{
			return null;
		}
		
		
		return $this->titledLabel(
			absences_translate('Theoretical period'),
			$this->getPeriod($this->right->date_begin, $this->right->date_end, false)
		);
	}
	
	
	protected function getFixedPeriod()
	{
		if (absences_Right::FIXED !== $this->right->getKind())
		{
			return null;
		}
		
		return $this->titledLabel(
			absences_translate('Fixed right period'),
			$this->getPeriod($this->right->date_begin_fixed, $this->right->date_end_fixed)
		);
	}
	
	
	
	protected function sync()
	{
		$W = bab_Widgets();

		if (0 === $this->right->getSyncStatus())
		{
			return null;
		}
		
		
		
		if (absences_Right::SYNC_SERVER !== $this->right->getSyncStatus())
		{
			$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.5,'em'))->addClass('widget-bordered');
			$frame->addItem($W->Title(sprintf(absences_translate('Shared right from %s'), absences_getVacationOption('sync_url')), 5));
			$frame->addItem($W->Label($this->right->getSyncStatusLabel()));
			$frame->addItem($this->titledLabel(absences_translate('Last update'), bab_shortDate(bab_mktime($this->right->sync_update))));
			
			return $frame;
			
		} else {
			
			return $W->Label($this->right->getSyncStatusLabel());
		}
	}
	
	
	protected function conditions()
	{
		$W = bab_Widgets();
		$cond = $this->right->getAccessConditions();
		
		if (null === $cond)
		{
			return null;
		}
		
		return $W->RichText($cond);
	}
	
	
	
	
	
	/**
	 * 
	 * @param absences_CollectionIterator $collections
	 */
	protected function getCollectionsTable($collections)
	{
		$W = bab_Widgets();
		$table = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
		
		foreach($collections as $collection)
		{
			$table->addItem($W->Label($collection->name));
		}
		
		return $table;
	}
	
	
	protected function getSavingPeriod($rightCet)
	{
		if ('0000-00-00' === $rightCet->saving_begin || '0000-00-00' === $rightCet->saving_end)
		{
			return null;
		}
	
	
		return $this->titledLabel(
				absences_translate('Saving period'),
				$this->getPeriod($rightCet->saving_begin, $rightCet->saving_end, false)
		);
	}

}






class absences_RightFullFrame extends absences_RightCardFrame
{
	
	protected function initClasses()
	{
		$this->addClass('absences-right-fullframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		
		$this->setCanvasOptions($this->Options()->width(70,'em'));
	
	}
	
	
	protected function loadPage()
	{
		$this->initClasses();
		$W = bab_Widgets();
	
		$this->addColHead();
		
		if ($sync = $this->sync())
		{
			$this->addItem($sync);
		}
	
	
		if ($conditions = $this->conditions())
		{
			$this->addItem($conditions);
		}
	
		if ($beneficiaries = $this->beneficiaries())
		{
			$this->addItem($beneficiaries);
		}
		
		if ($alert = $this->alert())
		{
			$this->addItem($alert);
		}
		
		if ($dynconf = $this->dynamic_configuration())
		{
			$this->addItem($dynconf);
		}
		
		$addon = bab_getAddonInfosInstance('absences');
		$this->addItem($W->Link(
		    $W->Icon(absences_translate('Create a new right from this one'), Func_Icons::ACTIONS_LIST_ADD), 
		    $addon->getUrl().'vacadma&idx=addvr&from='.$this->right->id
		));
	}
	
	
	
	protected function beneficiaries()
	{
		$W = bab_Widgets();
		$vbox = $W->VBoxLayout();
	
		$coll_section = $W->Section(absences_translate('Collections'), $coll_layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em'))->setFoldable(true, false);
		$vbox->addItem($coll_section);
	
		$collections = $this->right->getCollectionIterator();
	
		if ($collections->count() == 0)
		{
			$coll_layout->addItem($W->Label(absences_translate('No collections associated to this right')));
		}
		else if ($collections->count() < 20)
		{
			$coll_layout->addItem($this->getCollectionsTable($collections));
		} else {
			$coll_layout->addItem($W->Label(sprintf(absences_translate('%d collections'), $collections->count())));
		}
	
		$button = $W->Button()->addItem($W->Label(absences_translate('Edit beneficiaries collections'))->addClass('widget-small'));
		$coll_layout->addItem($W->Link($button, absences_addon()->getUrl().'vacadma&idx=lvrc&idvr='.$this->right->id)->setSizePolicy(Widget_SizePolicy::MINIMUM));
		$coll_layout->setCanvasOptions($coll_layout->Options()->width(100, '%'));
	
	
		$a_section = $W->Section(absences_translate('Agents'), $a_layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em'))->setFoldable(true, false);
		$vbox->addItem($a_section);
	
		$agents = $this->right->getAgentIterator();
	
		if ($agents->count() == 0)
		{
			$a_layout->addItem($W->Label(absences_translate('No users associated to this right')));
		}
		else {
			$a_layout->addItem($stat_layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
	
	
			$stat_layout->addItem($W->Label(sprintf(absences_translate('%d associated users'), $agents->count())));
	
			$stats = $this->right->getAgentUsage();
	
			$stat_layout->addItem(
					$this->titledLabel(
							absences_translate('Total unused quantity'),
							$stats['available']
					)
			);
	
			$stat_layout->addItem(
					$this->titledLabel(
							absences_translate('Total waiting quantity (approbation ongoing)'),
							$stats['waiting']
					)
			);
	
			$stat_layout->addItem(
					$this->titledLabel(
							absences_translate('Agents with remaining rights'),
							$stats['agents']
					)
			);
	
			/*
			 $stat_layout->addItem(
			 		$this->titledLabel(
			 				absences_translate('agents with remaining rights (without unconfirmed requests)'),
			 				$stats['agents_iu']
			 		)
			 );
			*/
		}
	
		$button = $W->Button()->addItem($W->Label(absences_translate('Edit beneficiaries agents'))->addClass('widget-small'));
		$a_layout->addItem($W->Link($button, absences_addon()->getUrl().'vacadma&idx=lvrp&idvr='.$this->right->id)->setSizePolicy(Widget_SizePolicy::MINIMUM));
	
		$a_layout->setCanvasOptions($a_layout->Options()->width(100, '%'));
	
		return $vbox;
	}
	
	
	/**
	 * Liste des agents concernes par lalerte configuree
	 * @return Widget_Displayable_Interface
	 */
	public function alert()
	{
		$W = bab_Widgets();
		
		$types = $this->right->getQuantityAlertTypes();
		
		if (empty($types))
		{
			return null;
		}
		
		$section = $W->Section(absences_translate('Alert depending on the consumed quantity'), $hbox = $W->HBoxLayout())->setFoldable(true);
		
		$hbox->addItem($vbox = $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		
		$vbox->addItem($this->titledLabel(
			absences_translate('Consumed quantity on types'),
			$types
		));
		
		
		$vbox->addItem($this->titledLabel(
			absences_translate('Consumed quantity on period'),
			$this->getPeriod($this->right->quantity_alert_begin, $this->right->quantity_alert_end, false)
		));
		
		$vbox->addItem($this->titledLabel(
				absences_translate('Consumed quantity limit'),
				absences_quantity($this->right->quantity_alert_days, 'D')
		));
		
		$I = $this->right->getAgentRightIterator();
		
		$table = $W->BabTableView();
		
		$row = 0;
		$table->addHeadRow($row);
		
		$table->addItem($W->Label(absences_translate('Beneficiary with alert')), $row, 0);
		$table->addItem($W->Label(absences_translate('Quantity')), $row, 1);
		$row++;
		
		$quantity_alert_days = (float) $this->right->quantity_alert_days;
		
		foreach($I as $agentRight)
		{
			/*@var $agentRight absences_AgentRight */
			$qte = $agentRight->getQuantityAlertConsumed();
			
			if ($qte > $quantity_alert_days)
			{
				$table->addItem($W->Link($agentRight->getAgent()->getName(), absences_addon()->getUrl().'vacadm&idx=agentright&ar='.$agentRight->id), $row, 0);
				$table->addItem($W->Label(absences_quantity(round($qte, 2), 'D')), $row, 1);
				$row++;
			}
			
			
		}
		
		$vbox->setSizePolicy('widget-50pc');
		$table->setSizePolicy('widget-50pc');
		
		$hbox->addItem($table);
		
		return $section;
	}
	
	
	
	
	/**
	 * afficher la configuration des soldes dynamiques
	 */
	public function dynamic_configuration()
	{
		$I = $this->right->getDynamicConfigurationIterator();
		
		if (0 === $I->count())
		{
			return null;
		}
		
		
		$W = bab_Widgets();
		$section = $W->Section(absences_translate('Quantity modification depending on the consumed number of days'), $hbox = $W->HBoxLayout())->setFoldable(true);
		
		$hbox->addItem($vbox = $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		
		$vbox->addItem($this->titledLabel(
			absences_translate('Consumed quantity on period'),
			$this->getPeriod($this->right->dynconf_begin, $this->right->dynconf_end, false)
		));
		
		
		
		$vbox->addItem($this->titledLabel(
				absences_translate('Consumed quantity on types'),
				implode(', ',$this->right->getDynamicTypes())
		));
		

		$table = $W->BabTableView();
		
		$row = 0;
		$table->addHeadRow($row);
		
		$table->addItem($W->Label(absences_translate('Quantity limit')), $row, 0);
		$table->addItem($W->Label(absences_translate('Quantity modification')), $row, 1);
		$row++;
		
		foreach($I as $dynamic_configuration)
		{
			$table->addItem($W->Label(absences_quantity($dynamic_configuration->test_quantity, 'D')), $row, 0);
			$table->addItem($W->Label(absences_quantity($dynamic_configuration->quantity, $this->right->quantity_unit)), $row, 1);
			$row++;
		}
		
		$hbox->addItem($table);
		
		$vbox->setSizePolicy('widget-50pc');
		$table->setSizePolicy('widget-50pc');
		
		
		return $section;
	}
}














class absences_RightMovementList extends absences_MovementList
{

    public function __construct(absences_Right $right)
    {
        parent::__construct();
        
        $this->res = $right->getMovementIterator();
        $this->res->rewind();

        $this->paginate($this->res->count(), self::MAX);
        $this->res->seek($this->pos);
    }

}

