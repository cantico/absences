<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/collection.class.php';
require_once dirname(__FILE__).'/vacincl.php';

/**
 * Organization
 * 
 * @property	string	$name
 * @property	string	$description
 */
class absences_Organization extends absences_Record 
{
	

    /**
     * @return absences_Organization
     */
    public static function getById($id)
    {
        $organization = new absences_Organization;
        $organization->id = $id;
    
        return $organization;
    }
    
    /**
     * get by name or return null
     * @return absences_Organization
     */
    public static function getByName($name)
    {
        global $babDB;
        
        $query = "SELECT * FROM absences_organization WHERE name LIKE '".$babDB->db_escape_like($name)."'";
        
        $res = $babDB->db_query($query);
        $row = $babDB->db_fetch_assoc($res);
			
		if (!$row)
		{
			return null;
		}
        
		$organization = new absences_Organization();
		$organization->setRow($row);
		
        return $organization;
    }
	
	
	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			
			$query = 'SELECT * FROM absences_organization WHERE id='.$babDB->quote($this->id);

			$res = $babDB->db_query($query);
			$row = $babDB->db_fetch_assoc($res);
			
			if (!$row)
			{
				throw new Exception('This organization does not exists id='.$this->id);
			}
			
			$this->setRow($row);
			
			return $this->row;
		}
		
		return $this->row;
	}
	
	
	
	
	
	/**
	 * Save organization
	 * @return bool
	 */
	public function save()
	{
	    global $babDB;
	
	    if (empty($this->id))
	    {
	        $babDB->db_query('
				INSERT INTO absences_organization (name
				) VALUES (
					'.$babDB->quote($this->name).'
				)
			');
	    } else {
	        $babDB->db_query('UPDATE absences_organization
				SET
					name='.$babDB->quote($this->name).' 
				WHERE
					id='.$babDB->quote($this->id));
	    }
	    
	    return true;
	}
	
	
	/**
	 * Remove references and delete
	 * @return bool
	 */
	public function delete()
	{
	    global $babDB;
	    
	    if (!$this->id) {
	        throw new Exception('Missing ID');
	    }
	    
	    $babDB->db_query("UPDATE absences_personnel SET id_organization='0' 
	        WHERE id_organization=".$babDB->quote($this->id));
	    
	    $babDB->db_query("DELETE FROM absences_organization 
	        WHERE id=".$babDB->quote($this->id));
	    
	    return (1 == $babDB->db_affected_rows());
	}
	
	
	
	/**
	 * Get agents
	 * @return absences_AgentIterator | false
	 */
	public function getAgentInterator()
	{
	    if (!$this->id) {
	        return false;
	    }
	    
	    require_once dirname(__FILE__).'/agent.class.php';
	    $agents = new absences_AgentIterator();
	    $agents->setOrganization($this);
	    
	    return $agents;
	}
	
	
	
	
	
	
	/**
	 * Create organizations from the organization directory field "organisationname"
	 * this method must not modify the organizations ID
	 * 
	 */
	public static function createFromDirectory()
	{
	    global $babDB;
	    
	    $res = $babDB->db_query("
	        SELECT 
	           e.organisationname name, 
	           o.id 
	        
	        FROM bab_dbdir_entries e
	               LEFT JOIN absences_organization o ON o.name LIKE e.organisationname, 
	           absences_personnel p 
	        WHERE e.id_user=p.id_user 
	           AND e.organisationname<>'' 
	        
	        GROUP BY organisationname
	    ");
	    
	    while ($arr = $babDB->db_fetch_assoc($res)) {
	        $org = new absences_Organization();
	        $org->setRow($arr);
	        $org->save();
	    }
	}
	
}





class absences_OrganizationIterator extends absences_Iterator
{
	public function getObject($data)
	{
		$org = new absences_Organization;
		$org->setRow($data);


		return $org;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
	
			$query = '
				SELECT
					o.*
				FROM 
					absences_organization o 
			';			
			
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}

}