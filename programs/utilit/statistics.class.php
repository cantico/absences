<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/xls.class.php';


/**
 * days by year and by right types
 *
 */
class absences_StatisticsXls extends absences_Xls
{
	/**
	 *
	 * @var array
	 */
	private $type_rows = null;

	/**
	 *
	 * @var Worksheet
	 */
	private $type_worksheet = null;


	/**
	 *
	 * @var array
	 */
	private $type_years = array();


	/**
	 *
	 * @var array
	 */
	private $filter = array();



	public function __construct(Array $filter)
	{
		parent::__construct(absences_translate('Statistics'));

		$this->filter = $filter;

		$this->year_format = $this->workbook->addFormat();
		$this->year_format->setBold();
		$this->year_format->setPattern(1);
		$this->year_format->setFgColor(15);
		$this->year_format->setAlign('center');

		$this->addTypesWorksheet();


		$this->workbook->close();
	}


	/**
	 *
	 */
	private function getRows()
	{

		if (!isset($this->type_rows))
		{

			global $babDB;


			$res = $babDB->db_query('
				SELECT
					id,
					name
				FROM
					absences_types
				ORDER BY name
					');

			$row = 2;
			while ($arr = $babDB->db_fetch_assoc($res))
			{
				$this->type_rows[$arr['id']] = array(
					'name' 	=> $arr['name'],
					'row' 	=> $row++
				);
			}

		}


		return $this->type_rows;
	}


	/**
	 * Get row number by id type
	 */
	private function getRow($id_type)
	{
		$rows = $this->getRows();
		return $rows[$id_type]['row'];
	}




	private function getTypeWorksheet()
	{
		if (!isset($this->type_worksheet))
		{
			$this->type_worksheet = $this->workbook->addWorksheet(absences_translate('Vacation rights'));

			// create first column
			$this->type_worksheet->setColumn(0, 0, 30);
			$this->type_worksheet->writeString(1, 0, absences_translate('Vacation type')		, $this->header);

			foreach($this->getRows() as $arr)
			{
				$this->type_worksheet->writeString($arr['row'], 0, $arr['name']);
			}
		}

		return $this->type_worksheet;
	}



	private function prepareTypeData()
	{
		global $babDB;

		$years = array();
		$types = array();

		$organization = '';
		if(isset($this->filter['organization']) && $this->filter['organization']){
			$organization = 'AND p.id_organization='.$babDB->quote($this->filter['organization']);
		}

		$res = $babDB->db_query('
			SELECT
				t.id,
				YEAR(e.date_begin) year,
				SUM(ee.quantity) total,
				quantity_unit
			FROM
				absences_types t,
				absences_rights r,
				absences_entries_elem ee,
				absences_entries e,
				absences_personnel p
			WHERE r.id_type=t.id
			AND ee.id_right=r.id
			AND ee.id_entry=e.id 
		    AND e.status=\'Y\' 
 			AND p.id_user=e.id_user
			'.$organization.'
			GROUP BY year, t.id, r.quantity_unit
			ORDER BY year
				');

		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$years[$arr['year']] = $arr['year'];
			$types[$arr['id']][$arr['year']][$arr['quantity_unit']] = $arr['total'];
		}


		return array($years, $types);
	}


	protected function addTypesWorksheet()
	{
		$worksheet = $this->getTypeWorksheet();
		list($years, $types) = $this->prepareTypeData();

		$col_index = array();

		// first an second row
		$col = 1;
		foreach($years as $y)
		{
			$worksheet->mergeCells(0, $col, 0, 1+$col);

			$worksheet->setColumn(0, 0, 30);
			$worksheet->writeString(0, $col, $y		, $this->year_format);
			$col_index[$y]['D'] = $col;
			$worksheet->writeString(1, $col++, absences_translate('days')		, $this->header);
			$col_index[$y]['H'] = $col;
			$worksheet->writeString(1, $col++, absences_translate('hours')		, $this->header);
		}


		foreach($types as $id_type => $yearly)
		{
			$row = $this->getRow($id_type);
			foreach($yearly as $year => $quantities)
			{
				foreach($quantities as $quantity_unit => $total)
				{
					$col = $col_index[$year][$quantity_unit];
					$worksheet->writeNumber($row, $col, $total, $this->quantity);
				}
			}
		}
	}
}