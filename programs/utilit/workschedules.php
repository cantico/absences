<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * save profiles list into workschedules addon
 * 
 * @throws bab_SaveErrorException
 * 
 * @param int $id_user
 * @param array $profiles
 * 
 * @return boolean
 */
function absences_setProfiles($id_user, Array $profiles)
{
    $babBody = bab_getBody();
    
    $workschedules = bab_functionality::get('WorkingHours/Workschedules');
    /*@var $workschedules Func_WorkingHours_Workschedules */
    
    $set = $workschedules->userProfileSet();
    $res = $workschedules->getUserProfiles($id_user);
    $existingProfiles = array();
    
    foreach($res as $profile) {
        $existingProfiles[$profile->id] = $profile;
    } 
    
    $W = bab_Widgets();
    
    foreach($profiles['userprofile'] as $position => $id) {
        if (isset($existingProfiles[$id])) {
            $record = $set->get($id); // the existing profile contain a joined record
            unset($existingProfiles[$id]);
        } else {
            $record = $set->newRecord();
        }
        
        $record->profile = (int) $profiles['profile'][$position];
        
        $record->from = $W->DatePicker()->getISODate($profiles['from'][$position]);
        $record->to = $W->DatePicker()->getISODate($profiles['to'][$position]);
        
        $record->user = $id_user;
        $record->save();
    }
    
    
    // delete unprocessed existing profiles
    
    if (count($existingProfiles) > 0) {
        $set->delete($set->id->in(array_keys($existingProfiles)));
    }
    
    return true;
}