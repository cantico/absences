<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';


/**
 *
 * @property $name
 * @property $quantity
 * @property $quantity_unit
 *
 */
class absences_WorkperiodType extends absences_Record
{
	public static function getFromId($id)
	{
		$type = new absences_WorkperiodType;
		$type->id = $id;
		return $type;
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			$query = 'SELECT * FROM absences_workperiod_type WHERE id='.$babDB->quote($this->id);
	
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	
	/**
	 * @throws Exception
	 */
	public function delete()
	{
		// ne pas permettre la suppression si il existe des demandes en attente
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM absences_workperiod_recover_request WHERE status='.$babDB->quote('').' AND id_type='.$babDB->quote($this->id));

		$n = $babDB->db_num_rows($res);
		if (0 != $n)
		{
			throw new Exception(sprintf(absences_translate('The workperiod type cannot be deleted because it is linked to %d waiting requests'), $n));
		}
		
		$babDB->db_query("UPDATE absences_workperiod_recover_request SET id_type='0' WHERE id_type=".$babDB->quote($this->id));
		$babDB->db_query('DELETE FROM absences_workperiod_type WHERE id='.$babDB->quote($this->id));
	}
	
}





class absences_WorkperiodTypeIterator extends absences_Iterator
{

	
	public function getObject($data)
	{
		$type = new absences_WorkperiodType;
		$type->setRow($data);

		return $type;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			$query = 'SELECT * FROM absences_workperiod_type ORDER BY name';
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}

}