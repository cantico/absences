<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



class absences_RightCopyStep1
{

    public $altbg;
    
	public function __construct() {
		$this->t_help = absences_translate("The right theoretical period is used to determine the year");
		$this->t_year_from = absences_translate("Year");
		$this->t_year_to = absences_translate("Create rights for year");
		$this->t_record = absences_translate("Record");
		$this->t_report = absences_translate("Create report rights with type");
		
		$this->t_right_name = absences_translate("Right name");
		$this->t_right_kind = absences_translate("Right kind");
		$this-> t_quantity = absences_translate("Quantity");
		
		$this->t_help = absences_translate("Users will be associated to rights as in the previous period but the specific quantities of users will not be retained. Only the rights configured with a theoretical period can be renewed using this form.");

		global $babDB;

		$this->resyear = $babDB->db_query("SELECT 
		    YEAR(date_begin) year  
		FROM 
		    absences_rights 
		WHERE 
		    date_begin<>'0000-00-00' 
		    AND kind IN(".$babDB->quote($this->getAllowedKinds()).") 
	     GROUP BY year ORDER BY year DESC");

		$this->year_to = isset($_POST['year_to']) ? $_POST['year_to'] : '';
		
	}
	
	
	protected function getAllowedKinds()
	{
	    return array(
	        absences_Right::REGULAR, 
	        absences_Right::CET,
	        absences_Right::INCREMENT 
	    );
	}
	

	public function getnextyear() {
		global $babDB;
		if ($arr = $babDB->db_fetch_assoc($this->resyear)) {
			$this->year = bab_toHtml($arr['year']);
			if (empty($this->year_to)) {
				$this->year_to = $this->year;
			}
			$this->selected = isset($_POST['year_from']) && $_POST['year_from'] == $arr['year'];
			return true;
		}
		return false;
	}

	public function year_to() {
		$selected_year = isset($_POST['year_from']) ? $_POST['year_from'] : $this->year_to;
		if (!isset($_POST['year_to'])) {
			$this->year_to++;
		}
		global $babDB;

		$this->resrights = $babDB->db_query("
			SELECT
				r.* 
			FROM
				".ABSENCES_RIGHTS_TBL." r
			WHERE
				 YEAR(r.date_begin) = ".$babDB->quote($selected_year)." 
				AND r.kind IN(".$babDB->quote($this->getAllowedKinds()).")
		    
			GROUP BY r.id
		    ORDER BY r.sortkey, r.description  
			");

		return false;
	}

	public function getnextright() {
		global $babDB;
		if ($arr = $babDB->db_fetch_assoc($this->resrights)) {
		    
		    $this->altbg = !$this->altbg;
		    
		    $right = new absences_Right($arr['id']);
		    $right->setRow($arr);
		    
			$this->right_description	= bab_toHtml($right->description);
			$this->right_kind           = bab_toHtml($right->getKindLabel());
			$this->id_right				= bab_toHtml($right->id);
			$this->quantity             = bab_toHtml(absences_editQuantity($right->quantity, $right->quantity_unit));
			$this->right_unit           = bab_toHtml($right->getUnitLabel());
			$this->checked				= (isset($_POST['rights']) && isset($_POST['rights'][$arr['id']])) || empty($_POST);
			return true;
		}
		return false;
	}
	

}



class absences_RightCopyStep2 {

	var $messages = array();

	public function __construct() {

		include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

		global $babDB;
		$this->increment = ((int) $_POST['year_to']) - ((int) $_POST['year_from']);

		$this->nb_right_insert = 0;
		
		
		$renewal_uid = date('YmdHis').'-'.$_POST['year_from'].'-'.$_POST['year_to'];


		// create rights

		if (isset($_POST['rights'])) {
		    
			foreach($_POST['rights'] as $arr) {
			    
			    if (!isset($arr['checked'])) {
			        continue;
			    }
			    
				$row = $this->get_row($arr['id']);
				$row['quantity'] = str_replace(',', '.', $arr['quantity']);
				$row['quantity'] = str_replace(' ', '', $row['quantity']);
				if (true === $this->transform_row($row)) {
				    
				    $row['renewal_uid'] = $renewal_uid;
					$this->insert_row($row);
				}
			}
		}


		$this->addMessage( sprintf( absences_translate("%d rights has been inserted"), $this->nb_right_insert) );
	}


	private function addMessage($str) {
		$this->messages[] = $str;
	}

	public function getnextmessage() {
		return list(,$this->message) = each($this->messages);
	}


	private function get_row($id_right) {
		global $babDB;
		$res = $babDB->db_query("SELECT * FROM ".ABSENCES_RIGHTS_TBL." WHERE id=".$babDB->quote($id_right));
		return $babDB->db_fetch_assoc($res);
	}


	private function increment_ISO($date) {
		if ('0000-00-00' === $date) {
			return $date;
		}
		
		if ('0000-00-00 00:00:00' === $date) {
			return $date;
		}
		
		$obj = BAB_DateTime::fromIsoDateTime($date);
		$obj->add($this->increment,BAB_DATETIME_YEAR);
		
		if (10 == mb_strlen($date)) {
			return $obj->getIsoDate();
		} else {
			return $obj->getIsoDateTime();
		}
	}


	public function transform_row(&$row) {

		global $babDB;

		$row['id_creditor']			= $GLOBALS['BAB_SESS_USERID'];

		$row['createdOn']			= date('Y-m-d H:i:s');
		$row['date_entry']			= date('Y-m-d H:i:s');
		$row['date_begin']			= $this->increment_ISO($row['date_begin']);
		$row['date_end']			= $this->increment_ISO($row['date_end']);
		$row['date_begin_valid']	= $this->increment_ISO($row['date_begin_valid']);
		$row['date_end_valid']		= $this->increment_ISO($row['date_end_valid']);
		$row['date_end_fixed']		= $this->increment_ISO($row['date_end_fixed']);
		$row['date_begin_fixed']	= $this->increment_ISO($row['date_begin_fixed']);
		
		$row['date_end_report']		= $this->increment_ISO($row['date_end_report']);
		$row['quantity_alert_begin']= $this->increment_ISO($row['quantity_alert_begin']);
		$row['quantity_alert_end']	= $this->increment_ISO($row['quantity_alert_end']);
		
		$row['dynconf_begin']       = $this->increment_ISO($row['dynconf_begin']);
		$row['dynconf_end']         = $this->increment_ISO($row['dynconf_end']);

		$row['description'] = preg_replace_callback("/\d{4}/",
			create_function(
			   '$matches',
			   'return $matches[0] + '.$this->increment.';'
			 ),
			$row['description']
		);
		
		$row['renewal_parent']    = $row['id'];
		
		

		$res = $babDB->db_query("
		SELECT COUNT(*) FROM ".ABSENCES_RIGHTS_TBL."
		WHERE
			description=".$babDB->quote($row['description'])."
			AND date_begin=".$babDB->quote($row['date_begin'])."
			AND date_end=".$babDB->quote($row['date_end'])."
		");

		list($n) = $babDB->db_fetch_array($res);

		if ($n > 0) {
			$this->addMessage(sprintf(absences_translate("The right %s already exists"), $row['description']));
			return false;
		}

		return true;
	}



	private function insert_row($row) {

		global $babDB;

		$old_id_right = $row['id'];

		unset($row['id']);

		foreach($row as $key => $value) {
			$row[$key] = $babDB->db_escape_string($value);
		}

		$babDB->db_query("INSERT INTO ".ABSENCES_RIGHTS_TBL." (".implode(',',array_keys($row)).") VALUES (".$babDB->quote($row).")");

		$this->nb_right_insert++;

		$new_id_right = $babDB->db_insert_id();


		$res = $babDB->db_query("SELECT * FROM ".ABSENCES_RIGHTS_RULES_TBL." WHERE id_right=".$babDB->quote($old_id_right));
		if ($rule = $babDB->db_fetch_assoc($res)) {
			unset($rule['id']);
			$rule['id_right'] = $new_id_right;

			$rule['trigger_p1_begin']	= $this->increment_ISO($rule['trigger_p1_begin']);
			$rule['trigger_p1_end']		= $this->increment_ISO($rule['trigger_p1_end']);

			$rule['trigger_p2_begin']	= $this->increment_ISO($rule['trigger_p2_begin']);
			$rule['trigger_p2_end']		= $this->increment_ISO($rule['trigger_p2_end']);

			$babDB->db_query("INSERT INTO ".ABSENCES_RIGHTS_RULES_TBL." (".implode(',',array_keys($rule)).") VALUES (".$babDB->quote($rule).")");
		}


		$res = $babDB->db_query("SELECT * FROM ".ABSENCES_RIGHTS_INPERIOD_TBL." WHERE id_right=".$babDB->quote($old_id_right));
		if ($inperiod = $babDB->db_fetch_assoc($res)) {
			unset($inperiod['id']);
			$inperiod['id_right'] = $new_id_right;

			$inperiod['period_start']	= $this->increment_ISO($inperiod['period_start']);
			$inperiod['period_end']		= $this->increment_ISO($inperiod['period_end']);

			$babDB->db_query("INSERT INTO ".ABSENCES_RIGHTS_INPERIOD_TBL." (".implode(',',array_keys($inperiod)).") VALUES (".$babDB->quote($inperiod).")");
		}
		
		
		
		
		$res = $babDB->db_query("SELECT * FROM absences_dynamic_configuration WHERE id_right=".$babDB->quote($old_id_right));
		while ($arr = $babDB->db_fetch_assoc($res))
		{
		    $babDB->db_query('INSERT INTO absences_dynamic_configuration (id_right, test_quantity, quantity) 
		        VALUES ('.$babDB->quote($new_id_right).', '.$babDB->quote($arr['test_quantity']).', '.$babDB->quote($arr['quantity']).')');
		}
		
		
		// recopier les liens droit-regime
		
		$res = $babDB->db_query('SELECT * FROM absences_coll_rights WHERE id_right='.$babDB->quote($old_id_right));
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$babDB->db_query('INSERT INTO absences_coll_rights (id_coll, id_right) VALUES ('.$babDB->quote($arr['id_coll']).', '.$babDB->quote($new_id_right).')');
		}
		
		
		
		// recopier les liens droit-utilisateur
		
		$res = $babDB->db_query("SELECT
			t2.* 
		FROM
				absences_users_rights t2
				WHERE t2.id_right=".$babDB->quote($old_id_right)." AND renewal='1'");

		while ($arr = $babDB->db_fetch_assoc($res)) {
			$babDB->db_query("INSERT INTO absences_users_rights (
				id_user, 
				id_right,
				date_begin_valid,
				date_end_valid,
				inperiod_start,
				inperiod_end,
				validoverlap,
				saving_begin,
				saving_end 
			) VALUES (
				".$babDB->quote($arr['id_user']).",
				".$babDB->quote($new_id_right).",
				".$babDB->quote($this->increment_ISO($arr['date_begin_valid'])).",
				".$babDB->quote($this->increment_ISO($arr['date_begin_valid'])).",
				".$babDB->quote($this->increment_ISO($arr['inperiod_start'])).",
				".$babDB->quote($this->increment_ISO($arr['inperiod_end'])).",
				".$babDB->quote($arr['validoverlap']).",
				".$babDB->quote($this->increment_ISO($arr['saving_begin'])).",
				".$babDB->quote($this->increment_ISO($arr['saving_end']))."
			)");
		}

	}
}
