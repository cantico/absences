<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

bab_Widgets()->includePhpClass('Widget_Form');


class absences_WorkperiodRecoverRequestEditor extends Widget_Form
{
	/**
	 * @var absences_WorkperiodRecoverRequest
	 */
	protected $workperiod;
	
	/**
	 *
	 * @var bool
	 */
	protected $withCardFrame;
	
	/**
	 * Display the quantity field
	 * @var bool
	 */
	protected $quantityField;
	
	/**
	 * 
	 * @param absences_WorkperiodRecoverRequest $wd
	 * @param bool $withCardFrame			Display the appliquant card frame (approval, manager)
	 * @param bool $quantityField			Display the quantity field (approval, manager)
	 */
	public function __construct(absences_WorkperiodRecoverRequest $wd = null, $withCardFrame = null, $quantityField = null)
	{
		$W = bab_Widgets();
		
		$this->workperiod = $wd;
		
		$this->withCardFrame = isset($withCardFrame) ? $withCardFrame : isset($wd);
		$this->quantityField = isset($quantityField) ? $quantityField : isset($wd);
	
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));
		
		bab_functionality::includefile('Icons');
		
	
		$this->setName('workperiod');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->addClass(Func_Icons::ICON_LEFT_24);
		$this->colon();
	
		$this->setCanvasOptions($this->Options()->width(60,'em'));
	
	
		
		
		$this->addFields();
		$this->addButtons();
		$this->setSelfPageHiddenFields();
		$this->loadValues();
		
	}
	
	
	protected function loadValues()
	{
		if (isset($_POST['workperiod']))
		{
			$this->setValues(array('workperiod' => $_POST['workperiod']));
			
			if (isset($_POST['workperiod']['id']))
			{
				$this->setHiddenValue('workperiod[id]', $_POST['workperiod']['id']);
			}
			return;
		}
		
		if (isset($this->workperiod))
		{
			$values = $this->workperiod->getRow();
			if (isset($values['date_begin'])) {
			     $values['datebegin'] = mb_substr($values['date_begin'], 0, 10);
			     $values['hourbegin'] = mb_substr($values['date_begin'], 11);
			}
			
			if (isset($values['date_end'])) {
			$values['dateend'] = mb_substr($values['date_end'], 0, 10);
			$values['hourend'] = mb_substr($values['date_end'], 11);
			}
			
			$this->setValues(array('workperiod' => $values));
			
			if (isset($values['id_user'])) {
			    $this->setHiddenValue('workperiod[id_user]', $values['id_user']);
			}
			
			if (isset($values['id'])) {
			     $this->setHiddenValue('workperiod[id]', $values['id']);
			}
			
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		if (isset($this->workperiod) && $this->withCardFrame)
		{
			require_once dirname(__FILE__).'/agent.class.php';
			require_once dirname(__FILE__).'/agent.ui.php';
			
			$agent = absences_Agent::getFromIdUser($this->workperiod->id_user);
			$cardFrame = new absences_AgentCardFrame($agent);
			$this->addItem($cardFrame);
		}
	
		$this->addItem($this->period());
		$this->addItem($this->id_type());
		
		if ($this->quantityField)
		{
			if ($this->workperiod->status === '')
			{
				// waiting request
				$this->addItem($this->quantity());
			} else {
				
				// confirmed or rejected
				$this->addItem(
					$W->Label(
						sprintf(
							absences_translate('Quantity used in the recovery creation : %s'), 
							absences_quantity($this->workperiod->quantity, $this->workperiod->quantity_unit)
						)
					)
				);
			}
		}
		
		$this->addItem($this->comment());
	}
	
	
	protected function id_type()
	{
		$W = bab_Widgets();
		$select = $W->Select();
		
		require_once dirname(__FILE__).'/workperiod_type.class.php';
		
		$I = new absences_WorkperiodTypeIterator;
		foreach($I as $workperiodType)
		{
			$select->addOption($workperiodType->id, $workperiodType->name);
		}
		
		return $W->LabelledWidget(absences_translate('Working day type'), $select, __FUNCTION__);
	}
	
	
	/**
	 * quantity & quantity_unit
	 * @return Widget_FlowLayout
	 */
	protected function quantity()
	{
		$W = bab_Widgets();
	
		$lineedit = $W->LineEdit()->setSize(2)->setMaxSize(5)->setMandatory(true, absences_translate('The quantity is mandatory'))->setName('quantity');
		$select = $W->Select()->setName('quantity_unit')
		->addOption('D', absences_translate('Day(s)'))
		->addOption('H', absences_translate('Hour(s)'));
	
		return $W->VBoxItems(
				$W->Label(absences_translate('Quantity'))->setAssociatedWidget($lineedit)->addClass('widget-strong'),
				$W->FlowItems($lineedit, $select)
		)->setVerticalSpacing(.2,'em');
	}
	
	
	
	protected function comment()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			absences_translate('Reason for the request'), 
			$W->TextEdit()->setColumns(80)->setLines(5)->setMandatory(true, absences_translate('The reason is mandatory')),
			 __FUNCTION__
		);
	}
	
	
	/**
	 * 
	 * @return Widget_VBoxLayout
	 */
	protected function period()
	{
		$W = bab_Widgets();
	
		return $W->VBoxItems(
				$W->Label(absences_translate('Worked period')),
				$W->FlowItems($this->date_begin(), $this->date_end())
		)->setVerticalSpacing(1,'em');
	}
	
	
	protected function date_begin()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$delay = BAB_DateTime::now();
		$delay->add(absences_getVacationOption('delay_recovery'), BAB_DATETIME_DAY);
		
		$W = bab_Widgets();
		$date = $W->DatePicker()->setMaxDate($delay)->setMandatory(true)->setName('datebegin');
		$hours = $W->Select()->setName('hourbegin')->setOptions(absences_hoursList());
		
		$this->beginDate = $date;
		
		return $W->FlowItems(
			$W->Label(absences_translate('from date'))->setAssociatedWidget($date),
			$date,
			$hours
		)->setSpacing(.5,'em');
	}
	
	
	protected function date_end()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	    
	    $delay = BAB_DateTime::now();
	    $delay->add(absences_getVacationOption('delay_recovery'), BAB_DATETIME_DAY);
	    
		$W = bab_Widgets();
		$date = $W->DatePicker()->setMinDate($this->beginDate)->setMaxDate($delay)->setMandatory(true)->setName('dateend');
		$hours = $W->Select()->setName('hourend')->setOptions(absences_hoursList());
		
		return $W->FlowItems(
			$W->Label(absences_translate('to date'))->setAssociatedWidget($date),
			$date,
			$hours
		)->setSpacing(.5,'em');
	}

	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))->validate()
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}
	
}






class absences_WorkperiodRecoverApprobEditor extends absences_WorkperiodRecoverRequestEditor {
	
	
	public function __construct(absences_WorkperiodRecoverRequest $workperiod)
	{
		
		parent::__construct($workperiod);
		
		
	}
	
	/**
	 * Load values into editor
	 * 
	 */
	protected function loadValues()
	{
		if (isset($_POST['workperiod']))
		{
			$this->setValues(array('workperiod' => $_POST['workperiod']));
		} else {
			if (isset($this->workperiod))
			{
				$this->setValues(array('workperiod' => $this->workperiod->getRow()));
			}
		}
		
		if (isset($this->workperiod))
		{
			$this->setHiddenValue('workperiod[id]', $this->workperiod->id);
			$this->setHiddenValue('id_workperiod', $this->workperiod->id);
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		require_once dirname(__FILE__).'/agent.ui.php';
		
		$agent = $this->workperiod->getAgent();
		$card = new absences_AgentCardFrame($agent);
		
		$this->addItem($card);
	
		$this->addItem($this->period());
		
		if ($type = $this->type())
		{
			$this->addItem($type);
		}
		
		$this->addItem($this->computed_quantity());
		$this->addItem($this->quantity());
		$this->addItem($this->validity_end());
		
		if ($comment = $this->comment())
		{
			$this->addItem($comment);
		}
		
		$this->addItem($this->comment2());
		
		if (!$this->workperiod->todelete) {
		    $this->addItem($W->Icon(
		      absences_translate('The acceptation will open a recovery vacation right for the applicant with the specified quantity'), Func_Icons::STATUS_DIALOG_INFORMATION)
		    );
		}
	}
	
	
	
	protected function validity_end()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
				absences_translate('Recovery right validity end date'),
				$W->DatePicker(),
				__FUNCTION__
		);
	}
	
	
	protected function computed_quantity()
	{
		$W = bab_Widgets();
	
		$type = $this->workperiod->getType();
		
		return $W->VBoxItems(
				$W->Label(absences_translate('Computed quantity on period'))->colon()->addClass('widget-strong'),
				$W->Label(absences_quantity($this->workperiod->getComputedDuration(), $type->quantity_unit))
		);
	}
	
	
	/**
	 *
	 * 
	 */
	protected function period()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
				$W->Label(absences_translate('Working period'))->colon()->addClass('widget-strong'),
				$W->Label(absences_DateTimePeriod($this->workperiod->date_begin, $this->workperiod->date_end, 'bab_longDate'))
		);
	}
	
	/**
	 * 
	 */
	protected function type()
	{
		$W = bab_Widgets();
		
		$type = $this->workperiod->getType();
		
		if (!isset($type))
		{
			return null;
		}
		
		return $W->VBoxItems(
				$W->Label(absences_translate('Working day type'))->colon()->addClass('widget-strong'),
				$W->Label($type->name)
		);
		
	}
	
	/**
	 * 
	 */
	protected function comment()
	{
		$W = bab_Widgets();
		
		if (empty($this->workperiod->comment))
		{
			return null;
		}
	
		return $W->VBoxItems(
			$W->Label(absences_translate('Reason for the request'))->colon()->addClass('widget-strong'),
			$W->RichText($this->workperiod->comment)->setRenderingOptions(BAB_HTML_ALL ^ BAB_HTML_P)
		);
	}
	
	
	
	protected function duration()
	{
		$W = bab_Widgets();
		
		return $W->VBoxItems(
				$W->Label(absences_translate('Computed duration'))->colon(),
				$W->Label(absences_quantity($this->workperiod->getComputedDuration(), $this->workperiod->getType()->quantity_unit))
		);
	}
	
	
	protected function comment2()
	{
		$W = bab_Widgets();
	
		return $W->LabelledWidget(
				absences_translate('Approver comment'),
				$W->TextEdit()->setColumns(80)->setLines(5),
				__FUNCTION__
		);
	}
	
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
		
		if ($this->workperiod->todelete) {
		    $label = absences_translate('Confirm deletion');
		} else {
		    $label = absences_translate('Confirm');
		}
	
		$button = $W->FlowItems(
				
				$W->SubmitButton()->setName('confirm')->setLabel($label),
				$W->SubmitButton()->setName('refuse')->setLabel(absences_translate('Refuse'))
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}
	
}