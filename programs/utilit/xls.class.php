<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



/**
 * 
 */
class absences_Xls
{
	/**
	 * @var Workbook
	 */
	protected $workbook;
	
	
	public function __construct($name)
	{

		$instance = bab_functionality::get('ExcelExport');
		/*@var $instance Func_ExcelExport */
		$instance->setDownloadFilename(sprintf('%s.xls', bab_removeDiacritics(str_replace(' ', '-',$name))));
		
		$this->workbook = $instance->getWorkbook('0.9.2');
		
		
		$this->workbook->setCustomColor(15, 192, 192, 192);
		$this->header = $this->workbook->addFormat();
		$this->header->setBold();
		$this->header->setPattern(1);
		$this->header->setFgColor(15);
		
		
		$this->date = $this->workbook->addFormat();
		$this->date->setNumFormat('D MMM YYYY');
		
		$this->datetime = $this->workbook->addFormat();
		$this->datetime->setNumFormat('D/M/YYYY h:mm');
		
		$this->quantity = $this->workbook->addFormat();
		$this->quantity->setNumFormat('0.00');
		
		bab_setTimeLimit(3600);
	}
	
	
	
}


