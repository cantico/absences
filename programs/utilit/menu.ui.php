<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

bab_functionality::includeOriginal('Icons');


class absences_ManagerMenu
{
	protected function category($name)
	{
		$W = bab_Widgets();

		return $W->Section(
				$name,
				$W->VBoxItems()
				->setHorizontalSpacing(6, 'px')->setVerticalSpacing(1, 'em')
		)
		->setFoldable(false);
	}

	protected function category_settings()
	{
		$W = bab_Widgets();
		$cat = $this->category(absences_translate('Settings'));

		$cat->addItem(
			$W->Link(
				$W->Icon(absences_translate("Vacations types"), Func_Icons::ACTIONS_VIEW_LIST_TEXT),
				absences_addon()->getUrl()."vacadm&idx=lvt"
			)
		);


		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Collections"), Func_Icons::ACTIONS_VIEW_LIST_TEXT),
						absences_addon()->getUrl()."vacadm&idx=lcol"
				)
		);


		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Personnel members"), Func_Icons::ACTIONS_USER_GROUP_PROPERTIES),
						absences_addon()->getUrl()."vacadm&idx=lper"
				)
		);


		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Workdays types entitling recovery"), Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT),
						absences_addon()->getUrl()."workperiod_type"
				)
		);


		if (absences_getVacationOption('sync_server'))
		{
			$cat->addItem(
					$W->Link(
							$W->Icon(absences_translate("Configure the shared vacation rights"), Func_Icons::APPS_PREFERENCES_WEBSERVICES),
							absences_addon()->getUrl()."sync_server"
					)
			);
		}

		if (absences_getVacationOption('sync_url'))
		{
			$cat->addItem(
					$W->Link(
							$W->Icon(absences_translate("Configure the synchronized rights"), Func_Icons::APPS_PREFERENCES_WEBSERVICES),
							absences_addon()->getUrl()."sync_client"
					)
			);
		}


		$WorkingHours = bab_functionality::get('WorkingHours');
		if ($WorkingHours instanceof Func_WorkingHours_Workschedules)
		{
			$cat->addItem(
					$W->Link(
							$W->Icon(absences_translate("Configure the works schedules"), Func_Icons::APPS_PREFERENCES_CALENDAR),
							$WorkingHours->getProfileListUrl()
					)
			);
		}


		
		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Plannings"), Func_Icons::APPS_CALENDAR),
						absences_addon()->getUrl()."planning&idx=list"
				)
		);
		



	    $cat->addItem(
	        $W->Link(
	            $W->Icon(absences_translate("Organizations"), Func_Icons::APPS_GROUPS),
	            absences_addon()->getUrl()."organizations"
	        )
	    );


		return $cat;
	}


	protected function category_rights()
	{
		$W = bab_Widgets();
		$cat = $this->category(absences_translate('Vacation rights'));

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Vacations rights"), Func_Icons::APPS_VACATIONS),
						absences_addon()->getUrl()."vacadma&idx=lrig"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Rights groups"), Func_Icons::ACTIONS_LIST_ADD),
						absences_addon()->getUrl()."vacadma&idx=rgroup"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Rights renewal by years"), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE),
						absences_addon()->getUrl()."vacadma&idx=copy"
				)
		);


		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Archive rights"), Func_Icons::ACTIONS_ARCHIVE_CREATE),
						absences_addon()->getUrl()."archive&idx=right"
				)
		);


		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Sort rights"), Func_Icons::ACTIONS_ARROW_DOWN_DOUBLE),
						absences_addon()->getUrl()."sort&idx=right"
				)
		);

		return $cat;
	}

	protected function category_requests()
	{
		$W = bab_Widgets();
		$cat = $this->category(absences_translate('Users requests'));

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Vacations requests"), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
						absences_addon()->getUrl()."vacadmb&idx=lreq"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Waiting requests"), Func_Icons::APPS_APPROBATIONS),
						absences_addon()->getUrl()."waiting"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Working days entitling recovery"), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK),
						absences_addon()->getUrl()."vacadmwd"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Time saving accounts deposits"), Func_Icons::ACTIONS_VIEW_HISTORY),
						absences_addon()->getUrl()."vacadmcet"
				)
		);

		$cat->addItem(
				$W->Link(
						$W->Icon(absences_translate("Archive requests"), Func_Icons::ACTIONS_ARCHIVE_CREATE),
						absences_addon()->getUrl()."archive&idx=request"
				)
		);



		return $cat;
	}
	
	
	
	
	
	protected function category_export()
	{
	    $W = bab_Widgets();
	    $cat = $this->category(absences_translate('Exports'));
	    
	    
	    $cat->addItem(
            $W->Link(
                $W->Icon(absences_translate("Rights export"), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET),
                absences_addon()->getUrl()."vacadm&idx=rightsexport"
            )
        );
	    
	    $cat->addItem(
	            $W->Link(
	                    $W->Icon(absences_translate("Vacation requests exports"), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET),
	                    absences_addon()->getUrl()."exportvac&idx=reqx"
	            )
	    );
	    
	    
	    $cat->addItem(
	            $W->Link(
	                    $W->Icon(absences_translate("Vacation requests Sage exports"), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET),
	                    absences_addon()->getUrl()."exportvac&idx=sage"
	            )
	    );
	    
	    
	    $cat->addItem(
	            $W->Link(
	                    $W->Icon(absences_translate("Available balances export"), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET),
	                    absences_addon()->getUrl()."vacadm&idx=abexport"
	            )
	    );
	    
	    
	    $cat->addItem(
	            $W->Link(
	                    $W->Icon(absences_translate("Download statistics"), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET),
	                    absences_addon()->getUrl()."statistics&idx=filter"
	            )
	    );
	    
	    
	    return $cat;
	}


	public function getFrame()
	{
		$W = bab_Widgets();

		$frame = $W->Frame()
			->setLayout($W->FlowLayout()->setVerticalAlign('top')->setSpacing(5, 'em'))
			->addClass(Func_Icons::ICON_LEFT_24)
			->addClass('BabLoginMenuBackground')
			->addClass('widget-bordered');

		$frame->addItem($this->category_settings());
		$frame->addItem($this->category_rights());
		$frame->addItem($this->category_requests());
		$frame->addItem($this->category_export());

		return $frame;
	}
}