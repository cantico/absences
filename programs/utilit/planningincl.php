<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/vacincl.php';





/**
 * Get period index from database
 *
 * @param	int		$id_user
 * @param	int		$month
 * @param	int		$year
 * @param   string  $dateb      ISO date
 * @param   string  $datee      ISO date
 *
 * @return array
 */
function absences_getPeriodIndex($id_user, $month, $year, $dateb, $datee)
{
	require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
	global $babDB;

	$req = "
	SELECT 
		c.id_user,
		c.cal_date,
		c.ampm,
		c.period_type,
		c.id_entry,
		c.color,
		c.title,
		e.status 
	FROM
		".ABSENCES_CALENDAR_TBL." c
		LEFT JOIN ".ABSENCES_ENTRIES_TBL." e ON e.id = c.id_entry 
	WHERE
		monthkey=".$babDB->quote($month.$year)."
		AND c.id_user=".$babDB->quote($id_user)."
	ORDER BY c.cal_date, c.ampm 
	";
	$res = $babDB->db_query($req);


	if (0 === $babDB->db_num_rows($res))
	{
		// cache not found

		absences_updateCalendar($id_user, $year, $month);
		$res = $babDB->db_query($req);
	}


	$periodIndex = array();
	
	while ($arr = $babDB->db_fetch_assoc($res)) {
	    /*
	    if ('0000-00-00' !== $dateb && $dateb > $arr['cal_date']) {
	        continue;
	    }
	    
	    if ('0000-00-00' !== $datee && $datee < $arr['cal_date']) {
	        continue;
	    }
		*/
		$arr['period_type'] = (int) $arr['period_type'];
		$arr['ampm'] = (int) $arr['ampm'];
		$arr['id_user'] = (int) $arr['id_user'];
		
		$key = 'd.'.$arr['cal_date'];
		$key .= $arr['ampm'] ? '.1' : '.0';
		$periodIndex[$key] = $arr;
	}


	return $periodIndex;
}





/**
 * 
 */
function absences_getEntites()
{
    $id_chart = absences_getVacationOption('id_chart');
    $org = new bab_OrgChartUtil($id_chart);
    if (!$org->isAccessValid()) {
        return array();
    }
    return bab_OCGetEntities($id_chart);
}

/**
 * 
 * @param int $ide
 */
function absences_getChildsEntities($ide)
{
    $id_chart = absences_getVacationOption('id_chart');
    $org = new bab_OrgChartUtil($id_chart);
    if (!$org->isAccessValid()) {
        return array();
    }
    return bab_OCGetChildsEntities($ide, $id_chart);
}




class absences_viewVacationCalendarCls
{
	var $entries = array();
	var $fullname;
	var $vacwaitingtxt;
	var $vacapprovedtxt;
	var $print;
	var $close;
	var $emptylines = true;

	public $display_types;
	
	
	public $public = false;
	public $total = null;
	
	public $loadmore = null;
	public $loadall = false;
	
	public $nbmonth;
	
	public $limit;


	/**
	 * 
	 * @param array 	$users			Users displayed by default
	 * @param bool 		$period			Allow period selection (vacation request creation first step)
	 * @param bool 		$display_types	Display types color and legend on planning
	 * @param int		$nbmonth		Number of month to load
	 * @param bool		$dispusers		Display user names column
	 */
	public function __construct($users, $period, $display_types, $nbmonth, $dispusers, $shiftmonth = 0, $legend = true)
	{
		global $babBody;

		include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
		include_once $GLOBALS['babInstallPath']."utilit/urlincl.php";

		$month = isset($_REQUEST['month']) ? (int) $_REQUEST['month'] : (date("n") + $shiftmonth);
		$year = isset($_REQUEST['year']) ? (int) $_REQUEST['year'] : date("Y");

		global $babDB;
		$this->month = $month;
		$this->year = $year;

		$this->dispusers = $dispusers;
		$this->display_types = $display_types;
		$this->display_legend = $legend;

		$this->userNameArr = array();
		
		foreach ($users as $uid)
		{
			$uid = (int) $uid;
			$this->userNameArr[$uid] = bab_getUserName($uid);
		}

		bab_sort::natcasesort($this->userNameArr);


		$this->idusers 		= array_keys($this->userNameArr);
		$this->nbusers 		= count($this->idusers);
		$this->firstuser 	= bab_toHtml(current($this->userNameArr));
		$this->display_firstuser = 1 === $this->nbusers;

		$this->period = $period;
		$this->vacwaitingtxt = absences_translate("Waiting vacation request");
		$this->vacapprovedtxt = absences_translate("Approved vacation request");
		$this->t_selected = absences_translate("Selected period");
		$this->print = absences_translate("Print");
		$this->close = absences_translate("Close");
		$this->t_noresult = absences_translate("No results found for this search query");

		$this->t_previousmonth = absences_translate("Previous month");
		$this->t_previousyear = absences_translate("Previous year");
		$this->t_nextmonth = absences_translate("Next month");
		$this->t_nextyear = absences_translate("Next year");

		$this->t_nonworking = absences_translate("Non-working day");
		$this->t_weekend = absences_translate("Week-end");
		$this->t_rotate = absences_translate("Print in landscape");
		$this->t_non_used = $this->display_types ? absences_translate("Non-used days") : absences_translate("Absences");
		$this->t_waiting = absences_translate("Waiting vacation request");
		$this->t_previsional = absences_translate("Previsional vacation request");
		$this->t_waiting_vac = absences_translate("Waiting vacation request");
		$this->t_legend = absences_translate("Legend");

		$this->id_request = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

		$this->nbmonth = $nbmonth;
		
		$this->limit = absences_getSearchLimit($nbmonth);

		$urltmp = bab_url::get_request_gp();
		$urltmp->search = null;      // the search button, disabled for navigation
		$this->nwd_color = 'yellow'; // default color for non working days if no categories

		if( $GLOBALS['babBody']->babsite['id_calendar_cat'] != 0)
		{
			include_once $GLOBALS['babInstallPath']."utilit/calapi.php";
			$idcat = bab_calGetCategories($GLOBALS['babBody']->babsite['id_calendar_cat']);
			if( isset($idcat[0]['color']))
			{
				$this->nwd_color = $idcat[0]['color'];
			}
		}

		if (!empty($_REQUEST['popup']))
		{
			$this->popup = true;
		}

		if (!isset($_REQUEST['ide']))
		{
			$urltmp->idu = implode(',',$this->idusers);
		}

		$switchurl = clone $urltmp;

		if (1 == $this->nbmonth) {

			$switchurl->nbmonth = 12;
			$this->switchurl = $switchurl->toString();
			$this->switchlabel = absences_translate("Year view");
			
			
			
			$this->prevmonthclass = 'prev1';
			$this->prevyearclass = 'prev2';
			
			$this->nextmonthclass = 'next1';
			$this->nextyearclass = 'next2';
			
		} else {
			$switchurl->nbmonth = 1;
			$this->switchurl = $switchurl->toString();
			$this->switchlabel = absences_translate("Month view");
			
			$this->prevmonthclass = 'prev2';
			$this->prevyearclass = 'prev1';
			
			$this->nextmonthclass = 'next2';
			$this->nextyearclass = 'next1';
		}

		$urltmp->nbmonth = $this->nbmonth;


		$previousmonth = clone $urltmp;
		$previousmonth->month = date("n", mktime( 0,0,0, $month-1, 1, $year));
		$previousmonth->year = date("Y", mktime( 0,0,0, $month-1, 1, $year));
		$this->previousmonth	= $previousmonth->toString();

		$nextmonth = clone $urltmp;
		$nextmonth->month = date("n", mktime( 0,0,0, $month+1, 1, $year));
		$nextmonth->year = date("Y", mktime( 0,0,0, $month+1, 1, $year));
		$this->nextmonth = $nextmonth->toString();

		$previousyear = clone $urltmp;
		$previousyear->month = date("n", mktime( 0,0,0, $month, 1, $year-1));
		$previousyear->year = date("Y", mktime( 0,0,0, $month, 1, $year-1));
		$this->previousyear = $previousyear->toString();

		$nextyear = clone $urltmp;
		$nextyear->month = date("n", mktime( 0,0,0, $month, 1, $year+1));
		$nextyear->year = date("Y", mktime( 0,0,0, $month, 1, $year+1));
		$this->nextyear = $nextyear->toString();

		$dateb = new BAB_DateTime($year, $month, 1);
		$datee = $dateb->cloneDate();
		$datee->add($this->nbmonth, BAB_DATETIME_MONTH);
		
		

		$this->yearname = $this->getTitle($dateb, $datee);
		
		





		if ($this->display_types)
		{
			$this->restypes = $babDB->db_query("

				SELECT
					t.*
					FROM
					".ABSENCES_TYPES_TBL." t,
					absences_rights r,
					absences_users_rights ur,
					".ABSENCES_PERSONNEL_TBL." p
				WHERE
					p.id_user IN(".$babDB->quote($this->idusers).")
					AND p.id_user=ur.id_user
					AND ur.id_right=r.id
					AND r.id_type=t.id
				GROUP BY
					t.id
					");

		}
		
		
		// filtre par service
		
		$W = bab_Widgets();
		$departments = $W->Multiselect();
		$departments->setName('departments')->setValue(bab_rp('departments'));
		
		$entities = absences_getEntites();
		foreach($entities as $e) {
		    $departments->addOption($e['id'], $e['name']);
		}
		
		$this->departments = '';

		if (count($entities) > 0) {
		    $this->departments = $departments->display($W->HtmlCanvas());
		}
		
		
		// filtre recherche par date
		
		$this->datefilter = $this->getDateFilter()->display($W->HtmlCanvas());
	}
	
	
	
	private function getDateFilter()
	{
	    $W = bab_Widgets();
	    
	    switch (bab_rp('searchtype')) {
	        case '2':
    	       $dateb = bab_rp('dateb');
    	       $datee = bab_rp('datee');
    	       $_date = null;
    	       break;
    	      
	        default:
	        case '1':
	           $dateb = null;
	           $datee = null;
	           $_date = bab_rp('date');
	           break;
	    }
	    
	    $periodpicker = $W->PeriodPicker()->setNames('dateb', 'datee')->setValues($dateb, $datee);
	    
	    $period = $W->LabelledWidget(
	        absences_translate('Date'),
	        $periodpicker
	    );
	    
	    $picker = $W->DatePicker()
    	    ->setValue($_date)
    	    ->addClass('absences-search-by-date');
	    
    	if (method_exists($picker, 'setDefaultDate')) {
    	    $picker->setDefaultDate($this->year.'-'.$this->month.'-01');
    	    $periodpicker->setDefaultDate($this->year.'-'.$this->month.'-01');
    	}
	    
	    $date = $W->LabelledWidget(
	            absences_translate('Date'),
	            $picker,
	            'date'
	    );
	    
	    $begin = $picker->getISODate($dateb);
	    $end = $picker->getISODate($datee);
	    $d = $picker->getISODate($_date);
	    
	    if ('0000-00-00' !== $d) {
	        $begin = $d;
	        $end = $d;
	    }
	    
	    if ('0000-00-00' !== $begin && '0000-00-00' !== $end) {
	        $picker->setMetadata('dateb', $begin);
	        $picker->setMetadata('datee', $end);
	    }
	    
	    
	    $frame = $W->Frame(null , $W->HBoxItems());
	    
	    
	    $frame->addItem(
	            $W->LabelledWidget(
	                    absences_translate('Search by date'),
	                    $W->Select()
	                    ->setAssociatedDisplayable($date, array('1'))
	                    ->setAssociatedDisplayable($period, array('2'))
	                    ->addOption('1', absences_translate("Absences on a date"))
	                    ->addOption('2', absences_translate("Absences in a period"))
	                    ->setValue(bab_rp('searchtype', 1)),
	                    'searchtype'
	            )
	    );
	    
	    
	    $frame->addItem($period);
	    $frame->addItem($date);
	    
	    return $frame;
	}
	
	
	/**
	 * @param BAB_DateTime $dateb
	 * @param BAB_DateTime $datee
	 * 
	 * @return string
	 */
	private function getTitle(BAB_DateTime $dateb, BAB_DateTime $datee)
	{
	    $months = bab_DateStrings::getMonths();
	    $duration = (int) round(($datee->getTimeStamp() - $dateb->getTimeStamp()) / 86400);

	    if ($duration > 33) {
	        
	        $end = $datee->cloneDate();
	        $end->less(1, BAB_DATETIME_DAY);
	        
	        return $months[$dateb->getMonth()].' '.$dateb->getYear()."-".$months[$end->getMonth()].' '.$end->getYear();
	    }
	    
	    return $months[$dateb->getMonth()].' '.$dateb->getYear();
	}
	
	
	
	
	public function getnexthidden()
	{
		if (list($name, $value) = each($_GET))
		{
			if (is_array($value))
			{
				return true;
			}
			
			$this->name = bab_toHtml($name);
			$this->value = bab_toHtml($value);
			return true;
		}
		
		reset($_GET);
		return false;
	}
	
	

	public function getdayname()
	{
		static $i = 1;
		if( $i <= 31)
		{
			$this->dayname = sprintf('%02d',$i);
			$i++;
			return true;
		}
		else
			return false;
	}
	
	
	/**
	 *
	 */
	public function getmonth()
	{
		static $i = 0;
		if( $i < $this->nbmonth)
		{
	
			$dateb = new BAB_DateTime($this->year, $this->month + $i, 1);
	
			$this->curyear = $dateb->getYear();
			$this->curmonth = $dateb->getMonth();
			$months = bab_DateStrings::getMonths();
			$this->monthname = bab_toHtml($months[$this->curmonth]);
			$this->totaldays = date("t", $dateb->getTimeStamp());
			$this->previous_period = NULL;
			$i++;
			return true;
		}
		else
			return false;
	}
	
	
	
	

	public function getnextuser()
	{
		static $i = 0;

		$n = $this->nbusers;

		if ( $n == 0 )
			$n = 1;

		$this->rowspan = $this->emptylines ? $this->nbusers : $n;

		if ($i < $n)
		{
			$this->first = $i == 0 ;
			$this->id_user = $this->idusers[$i];
			$this->username = bab_toHtml($this->userNameArr[$this->id_user]);
			try {
				$agent = absences_Agent::getCurrentUser();
				if ($agent->isManager())
				{
					$this->userurl = bab_toHtml('?tg=addon/absences/vacadm&idx=modp&idp='.$this->idusers[$i]);
				} else {
					$this->userurl = false;
				}
			} catch(Exception $e)
			{
				$this->userurl = false;
			}
			

			$i++;
			return true;
		}
		else
		{
			$i = 0;
			return false;
		}
	}


	/**
	 * 
	 * @return boolean
	 */
	public function getday()
	{
		static $d = 1;
		static $total = 0;
		if( $d <= 31)
		{
			$time = mktime(0, 0, 0, $this->curmonth, $d, $this->curyear);
            $this->day = bab_formatDate('%D', $time).' '.date('j', $time);
			$d++;
			return true;
		}
		else
		{
			$d = 1;
			return false;
		}
	}

	public function getnexttype()
	{

		if (!$this->display_types) {
			return false;
		}

		global $babDB;
		if ($this->arr = $babDB->db_fetch_array($this->restypes)) {
			$this->arr['name'] 			= bab_toHtml($this->arr['name']);
			$this->arr['description'] 	= bab_toHtml($this->arr['description']);
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public function entity($ide, $all = false)
	{
	    // permettre de charger les sous-entites fusionnes dans le meme planning
	    
	    $url = bab_url::get_request_gp();
	    $url->all = $all ? '0' : '1';
	    
	    $this->mergedurl = false;
	    
	    if (bab_OCGetChildCount($ide)) {
	        $this->mergedurl = bab_toHtml($url->toString());
	    }
	    
	    if ($all) {
	        $this->t_merged = absences_translate('View the entity only');
	    } else {
	        $this->t_merged = absences_translate('View users from all entities');
	    }
	}
	
	
	public function publiccalendar($total)
	{
		$this->display_firstuser = false;
		$this->public = true;
		$this->t_search_name = absences_translate('Search by name');
		$this->t_search_entity = absences_translate('By entity');
		$this->keyword = bab_rp('keyword');
		$this->t_ok = absences_translate('Ok');
		
		$this->total = $total;
		$this->t_total = sprintf(absences_translate('%d result found', '%d results found', $total), $total);
		
		
		// prepare button for first load
		
		$this->t_use_month_view = false;
		
		$this->t_loadall = absences_translate('View all results');
		$this->t_viewBegin = absences_translate('View the first 30 only');
		
		if ($total > $this->nbusers)
		{
			$last = $total - $this->nbusers;
			
			if (1 < $this->nbmonth)
			{
				$this->t_use_month_view = sprintf(absences_translate('The planning display only %d results from %d total results, use the month view to fetch next results'), $this->nbusers, $total);
				$this->loadmore = false;
				$this->loadall = false;
			} else {

			    $this->loadall = false;
				$this->loadmore = $this->limit < $last ? $this->limit : $last;
				$this->t_loadmore = sprintf(absences_translate('Load the next %d results'), $this->loadmore);
			}
		} else {
			$this->loadmore = false;
			$this->loadall = false;
		}
	}
	
	
	public function getHtml()
	{
	    // dans les vielles versions d'ovidentia
	    // l'access a l'api des organigramme renvoi un message d'erreur si on a pas les droits de lecture
	    // dans le planning le bouton qui permet d'acceder au sous-entites a besoin de cette api
	    // on supprime le message d'erreur pour eviter qui apparaisse sur le planning perso
	    // dans le vielles versions d'ovidentia < 8.4.0 le bouton pour afficher les sous-entites dans le meme planning sera probablement inoperant
	    // tant que l'organigramme n'est pas accessible en lecture
	    
	    $babBody = bab_getBody();
	    
	    if ($babBody->msgerror === bab_translate('Error: Right insufficient')) {
	       bab_debug($babBody->msgerror);
	       $babBody->msgerror = null;
	    }
	    
	    return absences_addon()->printTemplate($this, "vacuser.html", "calendarbyuser");
	}
	
	

	public function printhtml()
	{
		global $babBody;
		
		/*@var $babBody babBody */
		
		$html = $this->getHtml();
		
		/*@var $jquery Func_Jquery */
		$jquery = bab_functionality::get('jquery');
		$jquery->includeCore();

		$babBody->addStyleSheet(absences_addon()->getStylePath().'vacation.css');
		$babBody->addJavascriptFile(absences_addon()->getTemplatePath().'calendar.jquery.js', true);

		if (isset($_REQUEST['popup']) && $_REQUEST['popup'] == 1) {
			$babBody->babpopup($html);
		}
		else {
			$babBody->babecho($html);
		}
	}
}







/**
 * Display a vacation calendar
 * @param	array		$users			array of id_user to display
 * @param	boolean		$period			allow period selection, first step of vacation request
 * @param	boolean		$display_types	Display types color and legend on planning
 * @param	int			$nbmonth		Number of month to display
 * @param	bool		$dispusers		display a column with user names
 * @param 	int			$total			Total number of results in search
 */
function absences_viewVacationCalendar($users, $period, $display_types, $nbmonth = 12, $dispusers = true, $total = null )
{
	global $babBody;

	$temp = new absences_viewVacationCalendarCls($users, $period, $display_types, $nbmonth, $dispusers);

	if (count($users) == 0)
	{
		$babBody->addError(absences_translate("ERROR: No members"));
	}
	
	if (bab_rp('idx') == 'entity_cal')
	{
	    $temp->entity(bab_rp('ide'), (bool) bab_rp('all', false));
	}
	
	if (bab_rp('idx') == 'public')
	{
		$temp->publiccalendar($total);
	}
	
	$temp->printhtml();
}



/**
 * Serach in active users to display the public calendar
 * @param string $keyword
 * @param array $departments
 * @param string $dateb
 * @param string $datee
 * @param string $date
 */
function absences_publicCalendarUsers($keyword, $departments, $searchtype, $dateb, $datee, $date)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
	
	
	$W = bab_Widgets();
	$datePicker = $W->DatePicker();
	
	switch($searchtype) {
	    default:
	    case 1:
	        $dateb = '0000-00-00';
	        $datee = '0000-00-00';
	        $date = $datePicker->getISODate($date);
	        break;
	        
	    case 2:
	        $dateb = $datePicker->getISODate($dateb);
	        $datee = $datePicker->getISODate($datee);
	        $date = '0000-00-00';
	        break;
	}
	
	
	if ('0000-00-00' !== $date && false !== $date) {
	    $dateb = $date;
	    $datee = $date;
	}
	
	
	$query = 'SELECT
			u.id,
			u.lastname,
			u.firstname
		FROM
			bab_users u
	           LEFT JOIN bab_oc_roles_users ru ON ru.id_user=u.id 
	           LEFT JOIN bab_oc_roles r ON r.id=ru.id_role,
			absences_personnel p
	
		WHERE
			p.id_user=u.id AND '.bab_userInfos::queryAllowedUsers('u');

	
	if (isset($keyword))
	{
		$query .= ' AND (u.lastname LIKE \'%'.$babDB->db_escape_like($keyword).'%\' OR u.firstname LIKE \'%'.$babDB->db_escape_like($keyword).'%\')';
	}
	
	if (isset($departments))
	{
	    $query .= ' AND r.id_entity IN('.$babDB->quote($departments).')';
	}
	
	if ('0000-00-00' !== $dateb && '0000-00-00' !== $datee && false !== $dateb && false !== $datee) {
	    $subquery = 'SELECT id_user FROM absences_entries WHERE date_begin<'.$babDB->quote($datee.' 23:59:59').' AND date_end>'.$babDB->quote($dateb.' 00:00:00').' GROUP BY id_user';
	    $query .= ' AND u.id IN('.$subquery.')';
	}
	
	
	$query .= 'GROUP BY u.id ORDER BY u.lastname, u.firstname';
	
	$res = $babDB->db_query($query);
	
	return $res;
}


function absences_getSearchLimit($nbmonth)
{
    $initusers = (int) bab_rp('limit');
    if (empty($initusers)) {
        // preload the 30 first users if limit not set
        $initusers = 1 === $nbmonth ? 30 : 10;
    }
    
    return $initusers;
}
