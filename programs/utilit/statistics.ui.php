<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


bab_Widgets()->includePhpClass('Widget_Form');


class absences_StatisticsEditor extends Widget_Form
{

	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));


		$this->setName('filter');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->setHiddenValue('tg', 'addon/absences/statistics');
		$this->setHiddenValue('idx', 'types');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(70,'em'));

		$this->addFields();

		$this->addButtons();
	}



	protected function addFields()
	{
		global $babDB;
		$W = bab_Widgets();
		
		require_once dirname(__FILE__).'/organization.class.php';
		$resOrganization = new absences_OrganizationIterator();

		$orgaSelect = $W->Select();
		$orgaSelect->addOption('','');

		foreach ($resOrganization as $organization) {
			$orgaSelect->addOption($organization->id, $organization->name);
		}

		$this->addItem(
			$W->LabelledWidget(
				absences_translate('Organization'),
				$orgaSelect,
				'organization'
			)
		);


	}

	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
			$W->SubmitButton()->setName('export')->setLabel(absences_translate('Export'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}