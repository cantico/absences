<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/agentlist.ui.php';

class absences_BeneficiariesList extends absences_AgentList  
{
    public $fullname;
    public $urlname;
    public $url;

    public $fullnameval;

    public $arr = array();
    public $count;
    public $res;
    public $idvr;

    public $pos;
    public $selected;
    public $allselected;
    public $allurl;
    public $allname;
    public $checkall;
    public $uncheckall;
    public $deletealt;
    public $modify;
    public $quantitytxt;
    public $quantity;
    public $altbg = true;


    public function __construct($idvr, Array $filter)
    {
        parent::__construct($filter);
        
        require_once dirname(__FILE__).'/agent.class.php';
        require_once dirname(__FILE__).'/agent_right.class.php';
         
        $babBody = bab_getInstance('babBody');

        $this->allname = absences_translate("All");
        $this->uncheckall = absences_translate("Uncheck all");
        $this->checkall = absences_translate("Check all");
        $this->quantitytxt = absences_translate("Quantity");
        $this->t_used = absences_translate("Used");
        $this->t_collection = absences_translate("Collection");
        $this->t_waiting = absences_translate("Waiting");
        $this->modify = absences_translate("Modify");
        $this->t_add_group = absences_translate("Add by group");
        $this->t_selected_collection = absences_translate("This user is linked to the right by the collection");

        global $babDB;
        $this->idvr = $idvr;
         
        require_once dirname(__FILE__).'/right.class.php';
        $this->right = new absences_Right($idvr);
         
        $babBody->setTitle(sprintf(absences_translate("Users associated to the right %s"), $this->right->description));
        $this->add_group_url = bab_toHtml(absences_addon()->getUrl()."vacadma&idx=lvrp_add&idvr=".$this->idvr);
        	
    }

    public function getnext()
    {

        if(parent::getnext())
        {
            $arr = $this->arr;
            $this->used = 0;
            $this->selected = "";
            $this->nuserid = "";

            // create the agent and the agent_right objects

            $agent = new absences_Agent();
            $agent->setRow(
                array(
                    'id' 		=> $arr['agent_id'],
                    'id_user'	=> $arr['id_user'],
                    'id_coll'	=> $arr['id_coll']
                )
            );

            $collection = new absences_Collection();
            $collection->setRow(
                array(
                    'id' 		=> $arr['id_coll'],
                    'name'		=> $arr['collname']
                )
            );

            $this->collection = $collection->name;
            $this->collectionRight = $collection->isLinkedToRight($this->right);


            $agentRight = new absences_AgentRight();
            $agentRight->setAgent($agent);
            $agentRight->setRight($this->right);

            if( $agentRight->getRow() )
            {
                $this->nuserid = $arr['id_user'];
                $this->selected = "checked";
                $this->quantity = absences_quantity($agentRight->getQuantity(), $this->right->quantity_unit);
                $this->used = absences_quantity($agentRight->getConfirmedQuantity(), $this->right->quantity_unit);
                $this->waiting = absences_quantity($agentRight->getWaitingQuantity(), $this->right->quantity_unit);
                 
            } else {
                $this->nuserid = '';
                $this->selected = "";
                $this->quantity = '';
                $this->used = '';
                $this->waiting = '';
            }

            
            return true;
        }
        
        return false;
    }

}




