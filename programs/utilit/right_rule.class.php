<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';
require_once dirname(__FILE__).'/type.class.php';

/**
 * Abscence Right
 *
 * @property	int		$id_right
 * @property	int		$validoverlap
 * @property	float	$trigger_nbdays_min
 * @property 	float	$trigger_nbdays_max
 * @property	int		$trigger_type
 * @property	string	$trigger_p1_begin
 * @property	string	$trigger_p1_end
 * @property	string	$trigger_p2_begin
 * @property 	string	$trigger_p2_end
 * 
 */
class absences_RightRule extends absences_Record 
{
	private $right;
	
	private $type;
	
	private $_id_right;
	
	public static function getFromId($id)
	{
		$rightrule = new absences_RightRule;
		$rightrule->id = $id;
		return $rightrule;
	}
	
	public static function getFromRight($id_right)
	{
		$rightrule = new absences_RightRule;
		$rightrule->_id_right = $id_right;
		return $rightrule;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id) && !isset($this->_id_right))
			{
				$this->row = false;
				return false;
			}
			
			
			global $babDB;
			
			$query = 'SELECT * FROM absences_rights_rules WHERE ';
			
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}
			
			if (isset($this->_id_right))
			{
				$query .= 'id_right='.$babDB->quote($this->_id_right);
			}
			
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
		
		return $this->row;
	}
	
	/**
	 * 
	 * @param absences_Right $right
	 * @return absences_RightRule
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
		
		return $this->right;
	}
	
	
	
	/**
	 * @return absences_Type
	 */
	public function getType()
	{
		if (!isset($this->type))
		{
			$row = $this->getRow();
			
			if (empty($row['trigger_type']))
			{
				return null;
			}
			
			$this->type = new absences_Type($row['trigger_type']);
		}
	
		return $this->type;
	}
	
	
	
	public function setType(absences_Type $type)
	{
		$this->type = $type;
		return $this;
	}
	
	
	
	/**
	 * @return ressource
	 */
	public function getInPeriodRes()
	{
		if (isset($this->_id_right))
		{
			$id_right = $this->_id_right;
		} else if(isset($this->id_right))
		{
			$id_right = $this->id_right;
		} else if(isset($this->right))
		{
			$id_right = $this->right->id;
		} else {
			throw new Exception('Failed');
		}
		
		global $babDB;
		return $babDB->db_query('SELECT * FROM '.ABSENCES_RIGHTS_INPERIOD_TBL.' WHERE id_right='.$babDB->quote($id_right));
	}
	
	
	
	
	/**
	 * Disponibilite en fonction de la periode de conges demandee
	 * 
	 * @param	int		&$access_include	Cumul des tests sur les periodes d'inclusion
	 * @param	int		&$access_exclude	Cumul des tests sur les periodes d'exclusion
	 * @param 	int 	$beginp				Timestamp, debut de la periode demandee
	 * @param 	int 	$endp				Timestamp, fin de la periode demandee
	 * @param	int		$inperiod			1: Dans l'intervalle, 2: en dehors de l'intervalle
	 * @param	int		$validoverlap		1: Permettre le chevauchement de la periode de conges avec les periodes de test
	 * @param	string	$start				ISO datetime, Debut de la periode a tester
	 * @param	string	$end				ISO datetime, Fin de la periode a tester
	 * 
	 * 			
	 */
	public function addPeriodTest(&$access_include, &$access_exclude, $beginp, $endp, $inperiod, $validoverlap, $start, $end)
	{
		// periode de test
		$period_start 	= bab_mktime($start);
		$period_end 	= 86400 + bab_mktime($end);
		
		
		$period_access = ((int) $inperiod) + (((int) $validoverlap)*10);
		
		switch ($period_access)
		{
			case 0: // Toujours
			case 10:
		
				break;
		
			case 1: // Dans la periode de la regle
				if ($period_start <= $beginp && $period_end >= $endp) {
					$access_include |= 1;
					$debug_result = 'TRUE';
				} else {
					$access_include |= 0;
					$debug_result = 'FALSE';
				}
		
				bab_debug(
						"Disponibilite en fonction de la periode de conges demandee\n".
						"Dans l'intervale\n".
						'id = '.$this->id_right."\n".
						'description = '.$this->getRight()->description."\n".
						bab_shortDate($period_start).' <= '.bab_shortDate($beginp).
						' && '.bab_shortDate($period_end).' >= '.bab_shortDate($endp)."\n".
						' => '.$debug_result
				);
		
		
				break;
		
			case 2: // En dehors de la periode de la regle
				if ($period_end <= $beginp || $period_start >= $endp) {
					$access_exclude &= 1;
					$debug_result = 'TRUE';
				} else {
					$access_exclude &= 0;
					$debug_result = 'FALSE';
				}
		
		
				bab_debug(
						"Disponibilite en fonction de la periode de conges demandee\n".
						"En dehors de l'intervale\n".
						'id = '.$this->id_right."\n".
						'description = '.$this->getRight()->description."\n".
						bab_shortDate($period_end).' <= '.bab_shortDate($beginp).
						' && '.bab_shortDate($period_start).' >= '.bab_shortDate($endp)."\n".
						' => '.$debug_result
				);
				break;
		
			case 11: // Dans la periode de la regle mais peut depasser a l'exterieur
		
				if ($period_start < $endp && $period_end > $beginp ) {
					$access_include |= 1;
					$debug_result = 'TRUE';
				} else {
					$access_include |= 0;
					$debug_result = 'FALSE';
				}
		
		
				bab_debug(
						"Disponibilite en fonction de la periode de conges demandee\n".
						"Dans l'intervale mais peut depasser a l'exterieur\n".
						'id = '.$this->id_right."\n".
						'description = '.$this->getRight()->description."\n".
						bab_shortDate($period_start).' < '.bab_shortDate($endp).
						' && '.bab_shortDate($period_end).' > '.bab_shortDate($beginp)."\n".
						' => '.$debug_result
				);
				break;
		
			case 12: // En dehors de la periode de la regle mais peut depasser a l'interieur
				if ($period_start > $beginp || $period_end < $endp) {
					$access_exclude &= 1;
					$debug_result = 'TRUE';
				} else {
					$access_exclude &= 0;
					$debug_result = 'FALSE';
				}
		
				bab_debug(
						"acces sur la periode, en fonction de la periode de la demande\n".
						"En dehors de l'intervale mais peut depasser a l'interieur\n".
						'id = '.$this->id_right."\n".
						'description = '.$this->getRight()->description."\n".
						bab_shortDate($period_start).' < '.bab_shortDate($endp).
						' && '.bab_shortDate($period_end).' > '.bab_shortDate($beginp)."\n".
						' => '.$debug_result
				);
				break;
		}
	}
	
	
	
	
	/**
	 * Disponibilite en fonction de la periode de conges demandee
	 * le doit est accessible si on ne test pas de demande (premiere page de la demande, ne pas appeller cette methode dans ce cas)
	 * 
	 * @param int $beginp	Timestamp, debut de la periode demandee
	 * @param int $endp		Timestamp, fin de la periode demandee
	 * 
	 * @return bool
	 */
	public function isAccessibleOnPeriod($beginp, $endp)
	{
		global $babDB;
		
		$res = $this->getInPeriodRes();
		
		if( !$res || $babDB->db_num_rows($res) == 0 )
		{
			return true;
		}
		
		
		$access_include = 0;
		$access_exclude = 1;
		
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
		
			$this->addPeriodTest(
				$access_include, // reference
				$access_exclude, // reference
				$beginp, 
				$endp, 
				$arr['right_inperiod'], 
				$this->validoverlap, 
				$arr['period_start'], 
				$arr['period_end']
			);
			
		}
		
		$debug_include = $access_include ? 'TRUE' : 'FALSE';
		$debug_exclude = $access_exclude ? 'TRUE' : 'FALSE';
		
		bab_debug(sprintf("id = %d \ntests de periodes d'inclusion %s \ntests de periodes d'exclusion %s\n",$this->id_right, $debug_include, $debug_exclude));
		
		return ($access_include && $access_exclude);
	
	}
	

	
	/**
	 * Attribution du droit en fonction des jours demandes et valides
	 * @return bool
	 */
	public function isAccessibleAccordingToRequests($id_user)
	{
		global $babDB;
		
		$p1 = '';
		$p2 = '';
		$req = '';
		
		if ('0000-00-00' != $this->trigger_p1_begin && '0000-00-00' != $this->trigger_p1_end) {
			$p1 = "(e.date_begin < ".$babDB->quote($this->trigger_p1_end)." AND e.date_end > ".$babDB->quote($this->trigger_p1_begin).')';
		}
		
		if ('0000-00-00' != $this->trigger_p2_begin && '0000-00-00' != $this->trigger_p2_end) {
			$p2 = "(e.date_begin < ".$babDB->quote($this->trigger_p2_end)." AND e.date_end > ".$babDB->quote($this->trigger_p2_begin).')';
		}
		
		if ($p1 && $p2) {
			$req = 'AND ('.$p1.' OR '.$p2.')';
		} else if ($p1 || $p2) {
			$req = 'AND '.$p1.$p2;
		}
		
		
		if (!$req) {
			
			return true;	
		}
		
	
	
		if (!empty($this->trigger_type))
		{
			$table = ", ".ABSENCES_RIGHTS_TBL." r ";
			$where = " AND el.id_right=r.id AND r.id_type=".$babDB->quote($this->trigger_type)." ";
		}
		else
		{
			$table = '';
			$where = '';
		}
	
		$req = "SELECT
		e.date_begin,
		e.date_end,
		sum(el.quantity) total
		FROM
		".ABSENCES_ENTRIES_ELEM_TBL." el,
		".ABSENCES_ENTRIES_TBL." e
		".$table."
		WHERE
		e.id_user=".$babDB->quote($id_user)."
		and e.status='Y'
		and el.id_entry=e.id
		".$req.$where."
		GROUP BY e.id";
	
		$nbdays = 0;
		$res_entry = $babDB->db_query($req);
		while ($entry = $babDB->db_fetch_assoc($res_entry)) {
	
			list($entry_date_begin) = explode(' ',$entry['date_begin']);
			list($entry_date_end) = explode(' ',$entry['date_end']);
	
			$intersect_p1 = BAB_DateTime::periodIntersect(
					$entry_date_begin,
					$entry_date_end,
					$this->trigger_p1_begin,
					$this->trigger_p1_end
			);
	
			if (false !== $intersect_p1) {
				$period_length = 1 + BAB_DateTime::dateDiffIso($intersect_p1['begin'], $intersect_p1['end']);
				// + 1 for end day
				if ($period_length < $entry['total']) {
					$nbdays += $period_length;
				} else {
					$nbdays += $entry['total'];
				}
			}
	
			$intersect_p2 = BAB_DateTime::periodIntersect(
					$entry['date_begin'],
					$entry['date_end'],
					$this->trigger_p2_begin,
					$this->trigger_p2_end
			);
	
			if (false !== $intersect_p2) {
				$period_length = 1 + BAB_DateTime::dateDiffIso($intersect_p2['begin'], $intersect_p2['end']);
				// + 1 for end day
				if ($period_length < $entry['total']) {
					$nbdays += $period_length;
				} else {
					$nbdays += $entry['total'];
				}
			}
		}
	
		if ( '' !== $this->trigger_nbdays_min && '' !== $this->trigger_nbdays_max && $this->trigger_nbdays_min <= $nbdays && $nbdays < $this->trigger_nbdays_max ) {
	
			$right = $this->getRight();
			
			bab_debug(
					"Attribution du droit en fonction des jours demandes et valides\n".
					"Le droit est accorde si l'utilisateur a pris entre ".$this->trigger_nbdays_min." et ".$this->trigger_nbdays_max." jours\n".
					$right->description."\n".
					"nb de jours pris : ".$nbdays
			);
			return true;
		}
		
		
		return false;
	
	}
	
	
	
	
	
	
	
	
	public function insert()
	{
		global $babDB;
		
		$babDB->db_query('INSERT INTO absences_rights_rules 
			(
				id_right,
				validoverlap,
				trigger_nbdays_min,
  				trigger_nbdays_max,
  				trigger_type,
  				trigger_p1_begin,
  				trigger_p1_end,
  				trigger_p2_begin,
  				trigger_p2_end,
  				trigger_overlap
			) 
				VALUES 
			(
				'.$babDB->quote($this->id_right).',
				'.$babDB->quote($this->validoverlap).',
				'.$babDB->quote($this->trigger_nbdays_min).',
				'.$babDB->quote($this->trigger_nbdays_max).',
				'.$babDB->quote($this->trigger_type).',
				'.$babDB->quote($this->trigger_p1_begin).',
				'.$babDB->quote($this->trigger_p1_end).',
				'.$babDB->quote($this->trigger_p2_begin).',
				'.$babDB->quote($this->trigger_p2_end).',
				'.$babDB->quote($this->trigger_overlap).'
			)
		');
		
		
		$this->id = $babDB->db_insert_id();
	}
	
	
	
	public function update()
	{
		global $babDB;
		
		$babDB->db_query('UPDATE absences_rights_rules SET 
				
			validoverlap 		= '.$babDB->quote($this->validoverlap).',
			trigger_nbdays_min 	= '.$babDB->quote($this->trigger_nbdays_min).',
  			trigger_nbdays_max 	= '.$babDB->quote($this->trigger_nbdays_max).',
  			trigger_p1_begin 	= '.$babDB->quote($this->trigger_p1_begin).',
  			trigger_p1_end 		= '.$babDB->quote($this->trigger_p1_end).',
  			trigger_p2_begin 	= '.$babDB->quote($this->trigger_p2_begin).',
  			trigger_p2_end 		= '.$babDB->quote($this->trigger_p2_end).',
  			trigger_overlap 	= '.$babDB->quote($this->trigger_overlap).'	
				
		WHERE id_right='.$babDB->quote($this->id_right));
		
	}
}






