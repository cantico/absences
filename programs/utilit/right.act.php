<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/right.class.php';
require_once dirname(__FILE__).'/type.class.php';
require_once dirname(__FILE__).'/movement.class.php';


class absences_RightAct
{
	private static $post;

	
	
	/**
	 * Get date from field name
	 * @param string $name
	 * @return string
	 */
	private static function getDateValue($name)
	{
		if (!isset(self::$post[$name]))
		{
			return '0000-00-00';
		}
	
		$W = bab_Widgets();
		$date = $W->DatePicker()->getISODate(self::$post[$name]);
	
		if (false === $date)
		{
			return '0000-00-00';
		}
	
		return $date;
	}
	
	
	/**
	 *
	 * @param string $datename		name of datepicker
	 * @param string $timename		Name of select contains times values
	 * @return string
	 */
	private static function getDateTimeValue($datename, $timename)
	{
		if (!isset(self::$post[$datename]))
		{
			return '0000-00-00 00:00:00';
		}
	
		$W = bab_Widgets();
		$date = $W->DatePicker()->getISODate(self::$post[$datename]);
	
		if (false === $date || '0000-00-00' === $date)
		{
			return '0000-00-00 00:00:00';
		}
	
		$time = '00:00:00';
	
		if (isset(self::$post[$timename]))
		{
			if (8 !== mb_strlen(self::$post[$timename]))
			{
				return '0000-00-00 00:00:00';
			}
	
			$time = self::$post[$timename];
		}
	
		return $date.' '.$time;
	}
	
	
	/**
	 * 
	 * @param string $begin		Field name
	 * @param string $end		Field name
	 * @param string $title		title for error message
	 * 
	 * @return boolean
	 */
	private static function checkPeriod($begin, $end, $title)
	{
		global $babBody;
		
		$date_begin = self::getDateValue($begin);
		$date_end = self::getDateValue($end);
		
		if ('0000-00-00' === $date_begin)
		{
			$babBody->addError(sprintf(absences_translate("Invalid start date for the %s"), mb_strtolower($title)));
			return false;
		}
		
		if ('0000-00-00' === $date_end)
		{
			$babBody->addError(sprintf(absences_translate("Invalid end date for the %s"), mb_strtolower($title)));
			return false;
		}
		
		if($date_begin > $date_end )
		{
			$babBody->addError(sprintf(absences_translate("Begin date must be less than end date in %s"), $title));
			return false;
		}
		
		return true;
	}
	
	/**
	 * Validate optional period
	 * 
 	 * @param string $begin		Field name
	 * @param string $end		Field name
	 * @param string $title		title for error message
	 * 
	 * @return boolean
	 */
	private static function checkOptionalPeriod($begin, $end, $title)
	{
		if ('' === self::$post[$begin] && '' === self::$post[$end])
		{
			// period not set
			return true;
		}
		
		return self::checkPeriod($begin, $end, $title);
	}
	
	
	
	/**
	 * Test posted values before saving
	 * @return bool
	 */
	protected static function checkPost()
	{
		global $babBody, $babDB;
		
		
		if( empty(self::$post['description']))
		{
			$babBody->msgerror = absences_translate("You must specify a vacation description");
			return false;
		}
		
		$query = "SELECT id FROM absences_rights WHERE description LIKE '".$babDB->db_escape_like(self::$post['description'])."'";
		if (!empty(self::$post['id']))
		{
			$query .= ' AND id<>'.$babDB->quote(self::$post['id']);
		}
		
		$res = $babDB->db_query($query);
		if (0 !== $babDB->db_num_rows($res))
		{
			$babBody->addError(absences_translate("A vacation right with the same name already exists"));
			return false;
		}
		
		
		if(empty(self::$post['id']))
		{
			if (absences_Right::REPORT === (int) self::$post['kind'])
			{
				$babBody->addError(absences_translate("A report right cannot be created by a manager, they are created automaticaly after expiration of a right"));
				return false;
			}
			
			if (absences_Right::RECOVERY === (int) self::$post['kind'])
			{
				$babBody->addError(absences_translate("A recovery right cannot be created by a manager, they are created after approval of a workingday entitling recovery"));
				return false;
			}
		}
		
		
		if ((empty(self::$post['date_begin']) || empty(self::$post['date_end'])) && !empty(self::$post['quantity_alert_days']))
		{
			$babBody->addError(absences_translate("The configuration of an alert depending on the consumed number of days require a valid theoretical period"));
			return false;
		}
		
		
		if( absences_Right::CET !== (int) self::$post['kind'])
		{
			// tests de la quantite
			
			if( !is_numeric(self::$post['quantity']))
			{
				$babBody->addError(absences_translate("You must specify a correct quantity"));
				return false;
			}
			
			if( 0 > (int) self::$post['quantity'])
			{
				$babBody->addError(absences_translate("You must specify a correct quantity"));
				return false;
			}
		}
		
		if (0 !== (int) self::$post['id_rgroup'])
		{
			$res = $babDB->db_query("SELECT quantity_unit FROM absences_rgroup WHERE id=".$babDB->quote(self::$post['id_rgroup']));
			$rgroup = $babDB->db_fetch_assoc($res);

			if (!$rgroup || ($rgroup['quantity_unit'] != self::$post['quantity_unit']))
			{
				$babBody->msgerror = absences_translate("The right quantity unit and the right group quantity unit must be the same");
				return false;
			}
			
		}
		
		if (absences_Right::REGULAR === (int) self::$post['kind'] || absences_Right::INCREMENT === (int) self::$post['kind'])
		{
			$type = new absences_Type(self::$post['id_type']);
			if (!isset($type->cbalance) || ($type->cbalance == 'N' && self::$post['cbalance'] != 'N'))
			{
				$babBody->msgerror = absences_translate("Negative balance are not allowed with this vacation type");
				return false;
			}
		}
		
		
		if (!self::checkOptionalPeriod('date_begin', 'date_end', absences_translate('theoretical period')))
		{
			return false;
		}
		
		if (!self::checkOptionalPeriod('date_begin_valid', 'date_end_valid', absences_translate('validity period')))
		{
			return false;
		}
		
		if (!self::checkOptionalPeriod('trigger_p1_begin', 'trigger_p1_end', absences_translate('first test period')))
		{
			return false;
		}
		
		if (!self::checkOptionalPeriod('trigger_p2_begin', 'trigger_p2_end', absences_translate('second test period')))
		{
			return false;
		}
		
		
		
		
		
		if(absences_Right::FIXED === (int) self::$post['kind'])
		{
			// mandatory if fixed vacation
			
			$date_begin_fixed = self::getDateTimeValue('datebeginfx', 'hourbeginfx');
			$date_end_fixed = self::getDateTimeValue('dateendfx', 'hourendfx');
			
			if ('0000-00-00 00:00:00' === $date_begin_fixed)
			{
				$babBody->msgerror = absences_translate("Invalid start date for the fixed vacation period");
				return false;
			}
			
			if ('0000-00-00 00:00:00' === $date_end_fixed)
			{
				$babBody->msgerror = absences_translate("Invalid end date for the fixed vacation period");
				return false;
			}
			
			if($date_begin_fixed >= $date_end_fixed )
			{
				$babBody->msgerror = absences_translate("Begin date must be less than end date");
				return false;
			}
			
		}
		
		
		return true;
	}
	
	
	/**
	 * Si le droit est fixe+modifie ou si le droit deviens fixe
	 * @param absences_Right $right
	 * @return boolean
	 */
	protected function isFixedModified(absences_Right $right = null)
	{
		if (absences_Right::FIXED !== (int) self::$post['kind'] || null === $right)
		{
			return false;
		}
		
		if (absences_Right::FIXED !== $right->getKind())
		{
			return true;
		}
		
		
		if($right->date_begin_fixed != self::getDateTimeValue('datebeginfx', 'hourbeginfx')
				|| $right->date_end_fixed != self::getDateTimeValue('dateendfx', 'hourendfx'))
		{
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Update rights beneficiaries
	 * @param absences_Right 		$right
	 * @param Widget_ProgressBar 	$progress
	 * @param array					$checked_collections
	 */
	public static function updateCollectionsBeneficiaries(absences_Right $right, Widget_ProgressBar $progress, Array $checked_collections)
	{
		$already_exists = array();
		$I = $right->getCollectionIterator();
		
		$total = $I->count() + count($checked_collections);
		$pos = 1;
		$progress->updateProgress(0);
		
		foreach($I as $collection)
		{
			/*@var $collection absences_Collection */
		    
		    
			if (!in_array($collection->id, $checked_collections))
			{
				
				try
				{
					$collection->removeRight($right);
					$collection->unlinkAgentsFromRight($right);
				} catch (absences_EntryException $e) {
					$agent = $e->entry->getAgent();
					echo bab_toHtml(sprintf(absences_translate('Failed to update the period for %s, %s (%s)'), 
							$agent->getName(), 
							$e->getMessage(),
							absences_DateTimePeriod($e->entry->date_begin, $e->entry->date_end))
					, BAB_HTML_ALL);
				}
			} else {
			    
			    
				$already_exists[$collection->id] = true;
				
				// la collection etait deja cochee, il faut creer les demandes pour les agents qui n'en ont pas
				$collection->updateAgents($right, $progress);
			}
			
			$p = ($pos * 100) / $total;
			$progress->updateProgress($p);
			$pos++;
		}
		
		
		
		
		foreach($checked_collections as $id_collection)
		{
			$p = ($pos * 100) / $total;
			$progress->updateProgress($p);
			$pos++;
			
			
			if (isset($already_exists[$id_collection]))
			{
				continue;
			}
		
			$collection = absences_Collection::getById($id_collection);
			$collection->addRight($right);
			
			try {
			    $collection->linkAgentsToRight($right, $progress);
			}  catch (Exception $e) {
			    // ex: l'url vers caldav est vide mais on utilise caldav
			    // exception remontee par LibCaldav
			    echo bab_toHtml($e->getMessage(), BAB_HTML_ALL);
			}
		}
		
		
		$progress->updateProgress(100, absences_translate('Finished'));
	}

	
	/**
	 * Update fixed vacation periods on an existing right
	 * 
	 */
	public static function updateFixed(absences_Right $right, Widget_ProgressBar $progress)
	{
		global $babDB;
		
		
		$rightusers = array();
		foreach($right->getAgentIterator() as $agent)
		{
			$rightusers[$agent->id_user] = $agent;
		}

		$entriesusers = array();
		$res = $babDB->db_query("select 
			vet.*, 
			veet.id elem,
			veet.quantity 
				
				from absences_entries_elem veet 
					left join absences_entries vet on veet.id_entry=vet.id 
			where 
				veet.id_right=".$babDB->quote($right->id)." 
		");
		
		$remove_total = $babDB->db_num_rows($res);
		
		if ($remove_total > 0)
		{
			// dans ce cas on effectue une premiere progression de la gauge pour le nettoyage
			$pos = 0;
			while( $arr = $babDB->db_fetch_array($res))
			{
				if (!isset($arr['id']))
				{
					throw new Exception('missing entry for elem id='.$arr['elem']);
				}
				
				
				$entriesusers[$arr['id_user']] = $arr['id'];
				if( !isset($rightusers[$arr['id_user']]) )
				{
					$progress->updateProgress(100*$pos/$remove_total, sprintf(absences_translate('Remove vacation for user %s'), bab_getUserName($arr['id_user'])));
					absences_removeFixedVacation($arr['id']);
					$arrnotif = array($arr['id_user']);
					// remove vacation and notify
					absences_notifyOnVacationChange($arrnotif, $arr['quantity'], $arr['date_begin'], $arr['date_end'], ABSENCES_FIX_DELETE);
				}
				
				$pos++;
			}
		}
	
		// reset gauge
		$progress->updateProgress(0, absences_translate('Add and update vacations'));
		$update_total = count($rightusers);
		$pos = 0;
		
		$uupd = array();
		$uadd = array();
		foreach($rightusers as $ukey => $agent )
		{

			
			if( isset($entriesusers[$ukey]))
			{
				// update
				$progress->updateProgress(90*$pos/$update_total, sprintf(absences_translate('Update vacation for user %s'), $agent->getName()));
				try 
				{
					absences_updateFixedVacation($ukey, $right);
					$uupd[] = $ukey;
				} catch(absences_EntryException $e)
				{
					echo bab_toHtml(sprintf(absences_translate('Failed to update the period for %s, %s'), $agent->getName(), $e->getMessage()), BAB_HTML_ALL);
				}
				
				
			}
			else
			{
				// add
				$progress->updateProgress(90*$pos/$update_total, sprintf(absences_translate('Add new vacation for user %s'), bab_getUserName($ukey)));
				try {
					absences_addFixedVacation($ukey, $right);
					$uadd[] = $ukey;
				} catch(absences_EntryException $e)
				{
					echo bab_toHtml(sprintf(absences_translate('Failed to add the period for %s, %s'), $agent->getName(), $e->getMessage()), BAB_HTML_ALL);
				}
			}
			
			$pos++;
		}
	
		if( count($uupd)> 0 )
		{
			$progress->updateProgress(92, absences_translate('Notify users about the modified vacation'));
			absences_notifyOnVacationChange($uupd, $right->quantity, $right->date_begin_fixed,  $right->date_end_fixed, ABSENCES_FIX_UPDATE);
		}
		
		if( count($uadd)> 0 )
		{
			$progress->updateProgress(97, absences_translate('Notify users about the new vacation'));
			absences_notifyOnVacationChange($uadd, $right->quantity, $right->date_begin_fixed,  $right->date_end_fixed,  ABSENCES_FIX_ADD);
		}
		
		$progress->updateProgress(100, absences_translate('Finished'));
	}
	


	/**
	 * Save posted values
	 * @return boolean
	 */
	public static function save()
	{
		global $babBody, $babDB;
	
	
		self::$post = $_POST['right'];
	
		
		if (!self::checkPost())
		{
			return false;
		}
		
		
		if (absences_Right::CET === ((int) self::$post['kind']) || absences_Right::FIXED === ((int) self::$post['kind']))
		{
			$use_in_cet = 0;
			$cet_quantity = 0.0;
		} else {
			$use_in_cet = self::$post['use_in_cet'];
			$cet_quantity = str_replace(',', '.', self::$post['cet_quantity']);
		}
		
		
		
		
		if (isset(self::$post['report']) && self::$post['report'])
		{
			$id_report_type = self::$post['id_report_type'];
			$date_end_report = self::getDateValue('date_end_report');
			$description_report = self::$post['description_report'];
		} else {
			$id_report_type = 0;
			$date_end_report = '0000-00-00';
			$description_report = '';
		}
		
		
	
		if (!empty(self::$post['id']))
		{
				
			$right = new absences_Right(self::$post['id']);
			
			if (!$right->getRow())
			{
				// on ne doit pas passer ici, normallement
				$babBody->addError(absences_translate("This right does not exists"));
				return false;
			}
	
			if($right->date_begin_fixed != '0000-00-00 00:00:00' && absences_Right::FIXED !== (int) self::$post['kind'])
				{
				$babBody->addError(absences_translate("A fixed vacation right cannot be changed to another right because vacation entries can be associated with it, please delete the right to remove all vacation entries created by this right"));
				return false;
			}
	
			
			$fixedModified = self::isFixedModified($right);


			$babDB->db_query("
	
				UPDATE ".ABSENCES_RIGHTS_TBL."
				SET 
					kind				=".$babDB->quote(self::$post['kind']).",
					description			=".$babDB->quote(self::$post['description']).",
					id_creditor			=".$babDB->quote($GLOBALS['BAB_SESS_USERID']).",
					id_type				=".$babDB->quote(self::$post['id_type']).",
					quantity			=".$babDB->quote(self::$post['quantity']).",
					quantity_unit		=".$babDB->quote(self::$post['quantity_unit']).",
					date_entry			=curdate(),
					date_begin			=".$babDB->quote(self::getDateValue('date_begin')).",
					date_end			=".$babDB->quote(self::getDateValue('date_end')).",
					active				=".$babDB->quote(self::$post['active']).",
					cbalance			=".$babDB->quote(self::$post['cbalance']).",
					date_begin_valid	=".$babDB->quote(self::getDateValue('date_begin_valid')).",
					date_end_valid		=".$babDB->quote(self::getDateValue('date_end_valid')).",
					date_begin_fixed	=".$babDB->quote(self::getDateTimeValue('datebeginfx', 'hourbeginfx')).",
					date_end_fixed		=".$babDB->quote(self::getDateTimeValue('dateendfx', 'hourendfx')).",
			        hide_empty		    =".$babDB->quote(self::$post['hide_empty']).",
					no_distribution		=".$babDB->quote(self::$post['no_distribution']).",
					use_in_cet			=".$babDB->quote($use_in_cet).",
					cet_quantity		=".$babDB->quote($cet_quantity).",
					id_rgroup			=".$babDB->quote(self::$post['id_rgroup']).",
					earlier				=".$babDB->quote(self::$post['earlier']).",
				 	earlier_begin_valid =".$babDB->quote(self::$post['earlier_begin_valid']).",
				 	earlier_end_valid	=".$babDB->quote(self::$post['earlier_end_valid']).",
					later				=".$babDB->quote(self::$post['later']).",
					later_begin_valid	=".$babDB->quote(self::$post['later_begin_valid']).",
					later_end_valid		=".$babDB->quote(self::$post['later_end_valid']).",
					delay_before		=".$babDB->quote(self::$post['delay_before']).",
					id_report_type		=".$babDB->quote($id_report_type).", 
					date_end_report		=".$babDB->quote($date_end_report).", 
					description_report	=".$babDB->quote($description_report).", 
					quantity_alert_days	=".$babDB->quote(self::$post['quantity_alert_days']).", 
					quantity_alert_types =".$babDB->quote(isset(self::$post['quantity_alert_types']) ? implode(',',self::$post['quantity_alert_types']) : '').", 
					quantity_alert_begin =".$babDB->quote(self::getDateValue('quantity_alert_begin')).", 
					quantity_alert_end	=".$babDB->quote(self::getDateValue('quantity_alert_end')).", 
					quantity_inc_month	=".$babDB->quote(str_replace(',', '.', self::$post['quantity_inc_month'])).",
					quantity_inc_max	=".$babDB->quote(str_replace(',', '.', self::$post['quantity_inc_max'])).",
					
					dynconf_types 		=".$babDB->quote(isset(self::$post['dynconf_types']) ? implode(',',self::$post['dynconf_types']) : '').", 
					dynconf_begin 		=".$babDB->quote(self::getDateValue('dynconf_begin')).", 
					dynconf_end			=".$babDB->quote(self::getDateValue('dynconf_end')).", 
					
					require_approval	=".$babDB->quote(self::$post['require_approval'])."  
				WHERE 
					id=".$babDB->quote(self::$post['id'])."
	
				");
	
			$id = self::$post['id'];
			
			$right->addMovement(sprintf(absences_translate('Vacation right updated : %s'), self::$post['description']));
			 
			// si la quantite du droit a ete changee, enregistrer un mouvement pour tout les utilisateurs concernes
			if (!absences_cq($right->quantity, self::$post['quantity']) || $right->quantity_unit !== self::$post['quantity_unit'])
			{
				foreach($right->getAgentRightIterator() as $agentRight)
				{
					/*@var $agentRight absences_AgentRight */
					if ($agentRight->quantity == '')
					{
						$agentRight->addMovement(sprintf(absences_translate('The quantity on right %s has been modified from %s to %s for the user %s'), 
								self::$post['description'], 
								absences_quantity($right->quantity, $right->quantity_unit),
								absences_quantity(self::$post['quantity'], self::$post['quantity_unit']),
								$agentRight->getAgent()->getName()
						));
					}
				}
			}
		}
		else
		{
	
			require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
			
			$fixedModified = self::isFixedModified(null);
			
			if (!isset(self::$post['active'])) {
				self::$post['active'] = 'N';
			}
	
	
			$babDB->db_query("
	
				INSERT into ".ABSENCES_RIGHTS_TBL."
					(
					kind,
					description,
					id_creditor,
					id_type,
					quantity,
					quantity_unit,
			        createdOn,
					date_entry,
					date_begin,
					date_end,
					active,
					cbalance,
					date_begin_valid,
					date_end_valid,
					date_begin_fixed,
					date_end_fixed,
					no_distribution,
					use_in_cet,
					cet_quantity,
					id_rgroup,
					
					earlier,
				 	earlier_begin_valid,
				 	earlier_end_valid,
					later,
					later_begin_valid,
					later_end_valid,
					delay_before,
					
					id_report_type,
					date_end_report,
					description_report,
					quantity_alert_days,
					quantity_alert_types,
					quantity_alert_begin,
					quantity_alert_end,
					quantity_inc_month,
					quantity_inc_max,
					
					dynconf_types, 
					dynconf_begin,
					dynconf_end,
					
					require_approval,
					uuid
					)
				VALUES 
					(
					".$babDB->quote(self::$post['kind']).",
					".$babDB->quote(self::$post['description']).",
					".$babDB->quote($GLOBALS['BAB_SESS_USERID']).",
					".$babDB->quote(self::$post['id_type']).",
					".$babDB->quote(self::$post['quantity']).",
					".$babDB->quote(self::$post['quantity_unit']).",
			        curdate(),
					curdate(),
					".$babDB->quote(self::getDateValue('date_begin')).",
					".$babDB->quote(self::getDateValue('date_end')).",
					".$babDB->quote(self::$post['active']).",
					".$babDB->quote(self::$post['cbalance']).",
					".$babDB->quote(self::getDateValue('date_begin_valid')).",
					".$babDB->quote(self::getDateValue('date_end_valid')).",
					".$babDB->quote(self::getDateTimeValue('datebeginfx', 'hourbeginfx')).",
					".$babDB->quote(self::getDateTimeValue('dateendfx', 'hourendfx')).",
					".$babDB->quote(self::$post['no_distribution']).",
					".$babDB->quote($use_in_cet).",
					".$babDB->quote($cet_quantity).",
					".$babDB->quote(self::$post['id_rgroup']).",
					
					".$babDB->quote(self::$post['earlier']).",
					".$babDB->quote(self::$post['earlier_begin_valid']).",
					".$babDB->quote(self::$post['earlier_end_valid']).",
					".$babDB->quote(self::$post['later']).",
					".$babDB->quote(self::$post['later_begin_valid']).",
					".$babDB->quote(self::$post['later_end_valid']).",
					".$babDB->quote(self::$post['delay_before']).",
					
					".$babDB->quote($id_report_type).",
					".$babDB->quote($date_end_report).",
					".$babDB->quote($description_report).",
					".$babDB->quote(self::$post['quantity_alert_days']).",
					".$babDB->quote(isset(self::$post['quantity_alert_types']) ? implode(',',self::$post['quantity_alert_types']) : '').",
					".$babDB->quote(self::getDateValue('quantity_alert_begin')).",
					".$babDB->quote(self::getDateValue('quantity_alert_end')).",
					".$babDB->quote(str_replace(',', '.', self::$post['quantity_inc_month'])).",
					".$babDB->quote(str_replace(',', '.', self::$post['quantity_inc_max'])).",
					
					".$babDB->quote(isset(self::$post['dynconf_types']) ? implode(',',self::$post['dynconf_types']) : '').", 
					".$babDB->quote(self::getDateValue('dynconf_begin')).", 
					".$babDB->quote(self::getDateValue('dynconf_end')).", 
					
					".$babDB->quote(self::$post['require_approval']).",
					".$babDB->quote(bab_uuid())."
					)
				");
	
			$id = $babDB->db_insert_id();
			
			
			$movement = new absences_Movement;
			$movement->id_right = $id;
			$movement->message = sprintf(absences_translate('Vacation right created : %s'), self::$post['description']);
			$movement->save();
		}
			
		self::saveRules($id);
		self::saveCet($id);
		self::applyDynamicRights($id);
		

		// si aucun beneficiaire, proposer d'en ajouter
		$res = $babDB->db_query('SELECT id FROM absences_users_rights WHERE id_right='.$babDB->quote($id));
		if ($babDB->db_num_rows($res) === 0)
		{
			absences_RightAct::goToNoBeneficiaries($id);
		}
		
		
		// if fixed, update fixed requests
		if ($fixedModified)
		{
		    absences_RightAct::goToUpdateFixed($id);
		}
	
		
		// to list
		absences_RightAct::redirect();
	}
	
	
	
	/**
	 * Apply dynamic rights for current beneficiaries
	 * @param int $id
	 */
	protected static function applyDynamicRights($id)
	{
		$right = new absences_Right($id);
		$res = $right->getAgentRightIterator();
		
		foreach($res as $agentRight)
		{
			$agentRight->applyDynamicRight();
		}
	}
	
	
	
	protected static function saveCet($id)
	{
		global $babDB;

		if (absences_Right::CET !== (int) self::$post['kind'])
		{
			// save
			$babDB->db_query("DELETE FROM absences_rights_cet WHERE id_right=".$babDB->quote($id));
			
			
		} else {
			
			$per_year = str_replace(',', '.', self::$post['per_year']);
			// $per_cet = str_replace(',', '.', self::$post['per_cet']);
			$per_cet = 0.0;
			$ceiling = str_replace(',', '.', self::$post['ceiling']);
			
			switch((int) self::$post['min_use_opt'])
			{
				case 0:
					$min_use = 0.0;
					break;
				case -1:
					$min_use = -1.0;
					break;
				case 1:
					$min_use = str_replace(',', '.', self::$post['min_use']);
					break;
			}
			
			
			
			$res = $babDB->db_query("SELECT id FROM absences_rights_cet WHERE id_right=".$babDB->quote($id));
			if ($babDB->db_num_rows($res) > 0)
			{
				list($id_cet) = $babDB->db_fetch_array($res);
				$babDB->db_query("
					UPDATE absences_rights_cet 
					SET
						saving_begin	=".$babDB->quote(self::getDateValue('saving_begin')).",
						saving_end		=".$babDB->quote(self::getDateValue('saving_end')).",
						per_year		=".$babDB->quote($per_year).",
						per_cet			=".$babDB->quote($per_cet).",
						ceiling			=".$babDB->quote($ceiling).",
						min_use			=".$babDB->quote($min_use)."
						
					WHERE
						id=".$babDB->quote($id_cet)."
				");
			}
			else
			{
				$babDB->db_query("
					INSERT INTO absences_rights_cet 
					(
						id_right,
						saving_begin,
						saving_end,
						per_year,
						per_cet,
						ceiling,
						min_use
					)
					VALUES 
					(
						".$babDB->quote($id).",
						".$babDB->quote(self::getDateValue('saving_begin')).",
						".$babDB->quote(self::getDateValue('saving_end')).",
						".$babDB->quote($per_year).",
						".$babDB->quote($per_cet).",
						".$babDB->quote($ceiling).",
						".$babDB->quote($min_use)."
					)
				");
			}
		}
	}
	
	
	
	/**
	 * Save rules associated to right
	 * @param	int		$id			
	 */
	protected static function saveRules($id)
	{
		global $babDB;
		require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
		
		if (absences_Right::FIXED === (int) self::$post['kind'])
		{
			$babDB->db_query("DELETE FROM ".ABSENCES_RIGHTS_RULES_TBL." WHERE id_right=".$babDB->quote($id));
			$babDB->db_query("DELETE FROM ".ABSENCES_RIGHTS_INPERIOD_TBL." WHERE id_right=".$babDB->quote($id));
		}
		else // rules
		{
			$validoverlap = isset(self::$post['validoverlap']) ? self::$post['validoverlap'] : 0;
			$trigger_type = isset(self::$post['trigger_type']) ? self::$post['trigger_type'] : 0;
			$trigger_overlap = isset(self::$post['trigger_overlap']) ? self::$post['trigger_overlap'] : 0;
		
		
			$res = $babDB->db_query("SELECT id FROM ".ABSENCES_RIGHTS_RULES_TBL." WHERE id_right=".$babDB->quote($id));
			if ($babDB->db_num_rows($res) > 0)
			{
				list($id_rule) = $babDB->db_fetch_array($res);
				$babDB->db_query("
						UPDATE ".ABSENCES_RIGHTS_RULES_TBL."
						SET
							validoverlap		=".$babDB->quote($validoverlap).",
							trigger_nbdays_min	=".$babDB->quote((int) self::$post['trigger_nbdays_min']).",
							trigger_nbdays_max	=".$babDB->quote((int) self::$post['trigger_nbdays_max']).",
							trigger_type		=".$babDB->quote($trigger_type).",
							trigger_p1_begin	=".$babDB->quote(self::getDateValue('trigger_p1_begin')).",
							trigger_p1_end		=".$babDB->quote(self::getDateValue('trigger_p1_end')).",
							trigger_p2_begin	=".$babDB->quote(self::getDateValue('trigger_p2_begin')).",
							trigger_p2_end		=".$babDB->quote(self::getDateValue('trigger_p2_end')).",
							trigger_overlap		=".$babDB->quote($trigger_overlap)."
						WHERE
							id=".$babDB->quote($id_rule)."
					");
			}
			else
			{
				$babDB->db_query("
						INSERT INTO ".ABSENCES_RIGHTS_RULES_TBL."
						(
							id_right,
							validoverlap,
							trigger_nbdays_min,
							trigger_nbdays_max,
							trigger_type,
							trigger_p1_begin,
							trigger_p1_end,
							trigger_p2_begin,
							trigger_p2_end,
							trigger_overlap
						)
						VALUES
						(
							".$babDB->quote($id).",
							".$babDB->quote($validoverlap).",
							".$babDB->quote((int) self::$post['trigger_nbdays_min']).",
							".$babDB->quote((int) self::$post['trigger_nbdays_max']).",
							".$babDB->quote($trigger_type).",
							".$babDB->quote(self::getDateValue('trigger_p1_begin')).",
							".$babDB->quote(self::getDateValue('trigger_p1_end')).",
							".$babDB->quote(self::getDateValue('trigger_p2_begin')).",
							".$babDB->quote(self::getDateValue('trigger_p2_end')).",
							".$babDB->quote($trigger_overlap)."
				)
						");
			}
		
		
			$babDB->db_query('DELETE FROM '.ABSENCES_RIGHTS_INPERIOD_TBL.' WHERE id_right='.$babDB->quote($id));
		
		
			$W = bab_Widgets();
			$datePicker = $W->DatePicker();
			
			foreach(self::$post['inperiod'] as $arr) {
				
				
				$period_start = $datePicker->getISODate($arr['period_start']);
				$period_end = $datePicker->getISODate($arr['period_end']);
		
				if($period_start && $period_end && '0000-00-00' !== $period_start && '0000-00-00' !== $period_end)
				{
					$babDB->db_query('
						INSERT INTO '.ABSENCES_RIGHTS_INPERIOD_TBL.'
						(
							id_right,
							period_start,
							period_end,
							right_inperiod,
							uuid
						)
						VALUES
						(
							'.$babDB->quote($id).',
							'.$babDB->quote($period_start).',
							'.$babDB->quote($period_end).',
							'.$babDB->quote($arr['right_inperiod']).',
							'.$babDB->quote(bab_uuid()).'
						)
					');
				}
			}
			
			
			$babDB->db_query('DELETE FROM absences_dynamic_configuration WHERE id_right='.$babDB->quote($id));
			
			foreach(self::$post['dynconf'] as $arr) {
			
			
				$test_quantity = (float) str_replace(',', '.', $arr['test_quantity']);
				$quantity = (float) str_replace(',', '.', $arr['quantity']);
				
				if ('-' === $arr['sign'])
				{
					$quantity *= -1;
				}
			
				if(0 !== (int) round(100 * $test_quantity) && 0 !== (int) round(100 * $quantity))
				{
					$babDB->db_query('
						INSERT INTO absences_dynamic_configuration 
						(
							id_right,
							test_quantity,
							quantity
						)
						VALUES
						(
							'.$babDB->quote($id).',
							'.$babDB->quote($test_quantity).',
							'.$babDB->quote($quantity).'
						)
					');
				}
			}
		
		}
	}
	
	
	public static function goToNoBeneficiaries($id)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->idx = 'nobenef';
		$url->idvr = $id;
		$url->location();
		return true;
	}
	
	
	public static function goToUpdateFixed($id)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->idx = 'fixedud';
		$url->idvr = $id;
		$url->location();
		return true;
	}
	
	
	public static function redirect()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->idx = 'lrig';
		$url->location();
		return true;
	}
	
}	
	