<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/





require_once dirname(__FILE__).'/record.class.php';


class absences_DynamicConfiguration extends absences_Record
{
	private $right;
	
	/**
	 * @return absences_DynamicConfiguration
	 */
	public static function getById($id)
	{
		$dc = new absences_DynamicConfiguration;
		$dc->id = $id;
	
		return $dc;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load dynamic configuration, missing id');
			}
	
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_dynamic_configuration WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	
	/**
	 *
	 * @param absences_Right $right
	 * @return absences_DynamicConfiguration
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
	
		return $this->right;
	}
}







class absences_DynamicConfigurationIterator extends absences_Iterator
{
	protected $right;
	
	/**
	 * Min quantity in days
	 * @var float
	 */
	public $min_test_quantity = null;
	
	/**
	 * Max quantity in days
	 * @var float
	 */
	public $max_test_quantity = null;
	

	public function setRight(absences_Right $right)
	{
		$this->right = $right;
	}


	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = "SELECT
				dc.* 
			FROM
				absences_dynamic_configuration dc
			WHERE dc.id_right=".$babDB->quote($this->right->id)."
			";
			
			if (isset($this->min_test_quantity))
			{
				$req .= " AND dc.test_quantity>".$babDB->quote($this->min_test_quantity);
			}
			
			if (isset($this->max_test_quantity))
			{
				$req .= " AND dc.test_quantity<=".$babDB->quote($this->max_test_quantity);
			}
			
			$req .= " ORDER BY dc.test_quantity, dc.quantity";


			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}


	public function getObject($data)
	{
		$dynRight = absences_DynamicConfiguration::getById($data['id']);
		$dynRight->setRow($data);
		$dynRight->setRight($this->right);

		return $dynRight;
	}


}