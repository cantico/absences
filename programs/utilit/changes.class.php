<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/






/**
 * Convert values changes to a text for agent history
 *
 */
class absences_Changes
{
	private $list = array();
	
	
	/**
	 * 
	 * @param mixed $old_value
	 * @param mixed $new_value
	 * 
	 * @return bool
	 */
	public function setQuantity($old_value, $new_value)
	{
		$old_value = (string) $old_value;
		$new_value = (string) $new_value;
		
		if ($old_value === $new_value)
		{
			return false;
		}
		
		if ($old_value === '' || $old_value === '0')
		{
			$this->list[] = sprintf(absences_translate('set quantity to %s'), $new_value);
		} else if ($new_value === '' || $new_value === '0') {
			
			$this->list[] = absences_translate('remove quantity');
		} else {
			
			$this->list[] = sprintf(absences_translate('set quantity from %s to %s'), $old_value, $new_value);
		}
		
		return true;
	}
	
	
	
	public function fromRightQuantityToAgentQuantity()
	{
	    $this->list[] = absences_translate('A specific quantity for the user is used instead of the default right quantity');
	}
	
	
	public function fromAgentQuantityToRightQuantity()
	{
	    $this->list[] = absences_translate('The default right quantity is used instead of the specific quantity of the user');
	}
	
	
	
	/**
	 * Get period as internationalized string or null if period not set
	 * @param string $begin
	 * @param string $end
	 * @return string
	 */
	private function period($begin, $end)
	{
		if (null === $begin && null === $end)
		{
			return null;
		}
		
		if ('0000-00-00' === $begin && '0000-00-00' === $end)
		{
			return null;
		}
		
		$b = bab_shortDate(bab_mktime($begin), false);
		$e = bab_shortDate(bab_mktime($end), false);
		
		return $b.' '.$e;
	}
	
	
	
	/**
	 * 
	 *
	 *
	 * @param	string	$old_begin		Mysql date format
	 * @param 	string	$old_end		Mysql date format
	 * @param	string	$new_begin		Mysql date format
	 * @param 	string	$new_end		Mysql date format
	 * 
	 * @param	string	$label			Period name
	 * 
	 * 
	 * @return bool
	 */
	private function setPeriodText($old_begin, $old_end, $new_begin, $new_end, $label)
	{
		$old_value = $this->period($old_begin, $old_end);
		$new_value = $this->period($new_begin, $new_end);
		
		if (null === $old_value && null === $new_value)
		{
			return false;
		}
		
		if ($old_value === $new_value)
		{
			return false;
		}
		
		if (null === $old_value)
		{
			$this->list[] = sprintf(absences_translate('set %s to %s'), $label, $new_value);
		} else if ($new_value === null) {
		
			$this->list[] = sprintf(absences_translate('remove %s'), $label);
		} else {
		
			$this->list[] = sprintf(absences_translate('set %s from %s to %s'), $label, $old_value, $new_value);
		}
		
		return true;
	}
	
	
	
	/**
	 * Disponibilite en fonction de la date de saisie de la demande de conges
	 * 
	 * 
	 * @param	string	$old_begin		Mysql date format
	 * @param 	string	$old_end		Mysql date format
	 * @param	string	$new_begin		Mysql date format
	 * @param 	string	$new_end		Mysql date format
	 * 
	 */
	public function setValidPeriod($old_begin, $old_end, $new_begin, $new_end)
	{
		return $this->setPeriodText($old_begin, $old_end, $new_begin, $new_end, absences_translate('validity period'));
	}
	
	
	/**
	 * Disponibilite en fonction de la periode de conges demandee
	 *
	 *
	 * @param	string	$old_begin		Mysql date format
	 * @param 	string	$old_end		Mysql date format
	 * @param	string	$new_begin		Mysql date format
	 * @param 	string	$new_end		Mysql date format
	 *
	 */
	public function setInPeriod($old_begin, $old_end, $new_begin, $new_end)
	{
		return $this->setPeriodText($old_begin, $old_end, $new_begin, $new_end, absences_translate('usage period'));
	}
	
	/**
	 * 
	 * @param unknown_type $old_value
	 * @param unknown_type $new_value
	 */
	public function setValidOverlap($old_value, $new_value)
	{
		$old_value = (int) $old_value;
		$new_value = (int) $new_value;
		
		if ($old_value === $new_value)
		{
			return false;
		}
		
		if ($new_value === 1)
		{
			 $this->list[] = absences_translate('overlapping enabled');
		} else {
			$this->list[] = absences_translate('overlapping disabled');
		}
		
		
		return true;
	}
	
	/**
	 * Periode d'epargne du CET
	 *
	 *
	 * @param	string	$old_begin		Mysql date format
	 * @param 	string	$old_end		Mysql date format
	 * @param	string	$new_begin		Mysql date format
	 * @param 	string	$new_end		Mysql date format
	 *
	 */
	public function setSavingPeriod($old_begin, $old_end, $new_begin, $new_end)
	{
		return $this->setPeriodText($old_begin, $old_end, $new_begin, $new_end, absences_translate('saving period'));
	}
	
	
	
	/**
	 * @return string
	 */
	public function __toString()
	{
		return implode(', ', $this->list);
	}
}