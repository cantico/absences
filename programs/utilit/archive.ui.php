<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



bab_Widgets()->includePhpClass('Widget_Form');






abstract class absences_ArchiveYearEditor extends Widget_Form
{




	public function __construct()
	{
		$W = bab_Widgets();
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));

		$this->setName('archive');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(50,'em'));

		$this->addFields();
		$this->addButtons();
		$this->setSelfPageHiddenFields();

	}


	abstract protected function addFields();
	
	
	abstract protected function getOptions();
	

	
	protected function year()
	{
		$W = bab_Widgets();
		
		
		$options = $this->getOptions();
		ksort($options);
		
		return $W->LabelledWidget(
				absences_translate('Year to archive'),
				$W->RadioSet()->setOptions($options),
				__FUNCTION__
		)->setValue(key($options));
	}




	protected function comment()
	{
		$W = bab_Widgets();

		return $W->LabelledWidget(
				absences_translate('Comment'),
				$W->TextEdit()->setColumns(60)->setLines(4),
				__FUNCTION__
		);
	}





	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
				$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}









class absences_RightArchiveYearEditor extends absences_ArchiveYearEditor
{
	
	/**
	 * (non-PHPdoc)
	 * @see absences_ArchiveYearEditor::getOptions()
	 * 
	 * @return array
	 */
	protected function getOptions()
	{
		$years = array();
		
		foreach(absences_getArchivableRights() as $right)
		{
			
			$y = $right->getYear();
			if (isset($years[$y]))
			{
				$years[$y]++;
			} else {
				$years[$y] = 1;
			}
		}
		
		$options = array();
		foreach($years as $year => $quantity)
		{
			if ($year >= date('Y'))
			{
				continue;
			}
			
			$options[$year] = sprintf(absences_translate('%d : %d right', '%d : %d rights', $quantity), $year, $quantity);
		}
		
		return $options;
	}
	
	
	protected function addFields()
	{
	    $this->addItem($this->year());
	}
	
}


class absences_RequestArchiveYearEditor extends absences_ArchiveYearEditor
{
	protected function getOptions()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		require_once dirname(__FILE__).'/request.class.php';
		$I = new absences_RequestIterator();
		$I->archived = 0;
		
		$day = absences_getVacationOption('archivage_day');
		$month = absences_getVacationOption('archivage_month');
		
		$years = array();
		foreach($I as $request)
		{
			/*@var $request absences_Request */
			$y = $request->getArchiveYear();
			
			if (isset($years[$y]))
			{
				$years[$y]++;
			} else {
				$years[$y] = 1;
			}
		}
		
		$options = array();
		foreach($years as $year => $quantity)
		{
			if ($year >= date('Y'))
			{
			//	continue;
			}
			
			$startDate = new BAB_DateTime($year, $month, $day);
			$endDate = new BAB_DateTime($year+1, $month, $day-1);
			
			$options[$year] = sprintf(
				absences_translate(
					'%d (from %s to %s) : %d request',
					'%d (from %s to %s) : %d requests',
					$quantity
				),
				$year, $startDate->getFrenchDate(), $endDate->getFrenchDate(), $quantity
			);
		}
		
		return $options;
	}
	
	
	protected function addFields()
	{
	    $this->addItem($this->organization());
	    $this->addItem($this->year());
	}
	
	
	
	

	protected function organization()
	{
	    $W = bab_Widgets();
	    $select = $W->Select()->addClass('absences-select');
	     
	    $select->addOption('', absences_translate('On all organizations'));
	     
	    require_once dirname(__FILE__).'/organization.class.php';
	     
	    $I = new absences_OrganizationIterator();
	    foreach($I as $org) {
	        $select->addOption($org->id, $org->name);
	    }
	
	     
	    return $W->LabelledWidget(
	        absences_translate('Organization'),
	        $select,
	        __FUNCTION__
	    );
	}
	
}