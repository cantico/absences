<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
************************************************************************
* Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
*                                                                      *
* This file is part of Ovidentia.                                      *
*                                                                      *
* Ovidentia is free software; you can redistribute it and/or modify    *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation; either version 2, or (at your option)  *
* any later version.													*
*																		*
* This program is distributed in the hope that it will be useful, but  *
* WITHOUT ANY WARRANTY; without even the implied warranty of			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
* See the  GNU General Public License for more details.				*
*																		*
* You should have received a copy of the GNU General Public License	*
* along with this program; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
* USA.																	*
************************************************************************/


class absences_PeriodFrame
{
    public $isfixed = false;

    public $altbg = true;
    
    public $datebegin;
    public $datebegintxt;
    
    public $dateend;
    public $dateendtxt;
    
    public $vactype;
    public $addvac;
    public $save_previsional;
    public $remark;
    public $nbdaystxt;
    public $invaliddate;
    public $invaliddate2;
    public $invalidentry;
    public $invalidentry1;
    public $invalidentry3;
    public $totaltxt;
    public $balancetxt;
    public $calendar;
    public $t_total;
    public $t_available;
    public $t_waiting;
    public $t_period_nbdays;
    public $t_view_rights;
    public $t_recurring;
    
    public $totalval;
    public $maxallowed;
    public $id;
    public $id_user;
    public $ide;
    
    public $year;
    public $month;
    public $rfrom;
    
    

    public function __construct($id_user, $id)
    {
        require_once dirname(__FILE__).'/entry.class.php';
        global $babBody, $babDB;
        $this->datebegin = $GLOBALS['babUrlScript']."?tg=month&callback=dateBegin&ymin=0&ymax=2";
        $this->datebegintxt = absences_translate("Begin date");
        $this->dateend = $GLOBALS['babUrlScript']."?tg=month&callback=dateEnd&ymin=0&ymax=2";
        $this->dateendtxt = absences_translate("End date");
        $this->vactype = absences_translate("Vacation type");
        if ($id)
        {
            $this->addvac = absences_translate("Edit vacation request");
            $this->save_previsional = absences_translate("Save previsional request");
        } else {
            $this->addvac = absences_translate("Request vacation");
            $this->save_previsional = absences_translate("Create previsional request");
        }
        $this->remark = absences_translate("Remarks");
        $this->nbdaystxt = absences_translate("Quantity");
        $this->invaliddate = absences_translate("ERROR: End date must be older");
        $this->invaliddate = str_replace("'", "\'", $this->invaliddate);
        $this->invaliddate = str_replace('"', "'+String.fromCharCode(34)+'",$this->invaliddate);
        $this->invaliddate2 = absences_translate("Total days does'nt fit between dates");
        $this->invaliddate2 = str_replace("'", "\'", $this->invaliddate2);
        $this->invaliddate2 = str_replace('"', "'+String.fromCharCode(34)+'",$this->invaliddate2);
        $this->invalidentry = absences_translate("Invalid entry!  Only numbers are accepted or . !");
        $this->invalidentry = str_replace("'", "\'", $this->invalidentry);
        $this->invalidentry = str_replace('"', "'+String.fromCharCode(34)+'",$this->invalidentry);
        $this->invalidentry1 = absences_translate("Invalid entry");
        $this->invalidentry3 = absences_translate("The number of days exceed the total allowed");
        $this->totaltxt = absences_translate("Total");
        $this->balancetxt = absences_translate("Balance");
        $this->calendar = absences_translate("Planning");
        $this->t_total = absences_translate("Total");
        $this->t_available = absences_translate("Available");
        $this->t_waiting = absences_translate("Waiting");
        $this->t_period_nbdays = absences_translate("Period days");
        $this->t_view_rights = absences_translate("View rights details");
        $this->t_recurring = absences_translate("Do a recuring request");
        $this->t_previsional = absences_translate("Previsional");
        $this->t_previsional_available = absences_translate("Prev. bal.");

        $this->totalval = 0;
        $this->maxallowed = 0;
        $this->id = $id;
        $this->id_user = $id_user;
        $this->ide = bab_rp('ide');

        $this->year = isset($_REQUEST['year']) ? $_REQUEST['year'] : date('Y');
        $this->month = isset($_REQUEST['month']) ? $_REQUEST['month'] : date('n');
        $this->rfrom = isset($_REQUEST['rfrom']) ? $_REQUEST['rfrom'] : 0;

        if (!empty($this->id) && !isset($_POST['daybegin']))
        {
            include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

            $entry = absences_Entry::getById($this->id);
            $arr = $entry->getRow();

            $date_begin			= BAB_DateTime::fromIsoDateTime($arr['date_begin']);
            $date_end			= BAB_DateTime::fromIsoDateTime($arr['date_end']);

            $this->yearbegin	= $date_begin->getYear();
            $this->monthbegin	= $date_begin->getMonth();
            $this->daybegin		= $date_begin->getDayOfMonth();

            $this->yearend		= $date_end->getYear();
            $this->monthend		= $date_end->getMonth();
            $this->dayend		= $date_end->getDayOfMonth();

            $this->hourbegin	= date('H:i:s', $date_begin->getTimeStamp());
            $this->halfdaybegin = 'am' === date('a', $date_begin->getTimeStamp())	? 0 : 1;
            $this->hourend		= date('H:i:s', $date_end->getTimeStamp());
            $this->halfdayend	= 'am' === date('a', $date_end->getTimeStamp())		? 0 : 1;
        }
        elseif (isset($_POST['daybegin']))
        {
            $this->daybegin		= $_POST['daybegin'];
            $this->dayend		= $_POST['dayend'];
            $this->monthbegin	= $_POST['monthbegin'];
            $this->monthend		= $_POST['monthend'];
            $this->yearbegin	= $this->year + $_POST['yearbegin']	- 1;
            $this->yearend		= $this->year + $_POST['yearend']	- 1;
            $this->halfdaybegin = 0;
            $this->halfdayend	= 1;
            $this->hourbegin	= $_POST['hourbegin'];
            $this->hourend		= $_POST['hourend'];
        }
        else
        {
            $this->daybegin		= date("j");
            $this->dayend		= date("j");
            $this->monthbegin	= date("n");
            $this->monthend		= date("n");
            $this->yearbegin	= $this->year;
            $this->yearend		= $this->year;
            $this->halfdaybegin = 0;
            $this->halfdayend	= 1;
            $this->hourbegin	= '00:00:00';
            $this->hourend		= '23:59:59';
        }

        $this->halfdaysel = $this->halfdaybegin;

        $this->calurl = absences_addon()->getUrl()."planning&idx=cal&idu=".$id_user;

        $this->rights = absences_getRightsByGroupOnPeriod($id_user, $this->rfrom);
        $this->total_available = array('D' => 0, 'H' => 0);
        $this->total_waiting = array('D' => 0, 'H' => 0);
        $this->total_previsional = array('D' => 0, 'H' => 0);
        $this->total_prevavail = array('D' => 0, 'H' => 0);

        $this->dayType = array(
                absences_translate("Morning"),
                absences_translate("Afternoon")
        );

        $this->hours = absences_hoursList($GLOBALS['BAB_SESS_USERID']);


        require_once $GLOBALS['babInstallPath'].'utilit/workinghoursincl.php';
        $jSON = array();
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 0), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 1), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 2), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 3), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 4), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 5), $jSON);
        $this->addWorkingPeriod(bab_getWHours($GLOBALS['BAB_SESS_USERID'], 6), $jSON);
        

        $this->json_working_hours = '['.implode(',', $jSON).']';

    }

    /**
     * reduce working periods for planning
     */
    private function addWorkingPeriod($whours, &$jSON)
    {
        if (empty($whours))
        {
            return;
        }

        foreach($whours as $k => $period)
        {
            if ($period['startHour'] < '11:59:59' && $period['endHour'] > '12:00:00')
            {
                $whours[$k]['endHour'] = '11:59:59';
                $whours[] = array(
                        'startHour' => '12:00:00',
                        'endHour' => $period['endHour'],
                        'weekDay' => $period['weekDay']
                );
            }
        }


        foreach($whours as $period)
        {
            $jSON[] = '{
            weekDay:'.$period['weekDay'].',
            startHour:\''.$period['startHour'].'\',
            endHour:\''.$period['endHour'].'\'
        }';
        }

    }


    function getnextday()
    {
        static $i = 1;

        if( $i <= date("t"))
        {
            $this->dayid = $i;
            $i++;
            return true;
        }
        else
        {
            $i = 1;
            return false;
        }

    }

    function getnextmonth()
    {
        static $i = 1;

        if( $i < 13)
        {
            $this->monthid = $i;
            $this->monthname = bab_DateStrings::getMonth($i);
            $i++;
            return true;
        }
        else
        {
            $i = 1;
            return false;
        }

    }
    function getnextyear()
    {
        static $i = 0;
        if( $i < 3)
        {
            $this->yearid = $i+1;
            $this->yearidval = $this->year + $i;
            $i++;
            return true;
        }
        else
        {
            $i = 0;
            return false;
        }

    }
    function getnexthour()
    {
        if (list($key, $value) = each($this->hours))
        {
            $this->value = bab_toHtml($key);
            $this->option = bab_toHtml($value);
            return true;
        }
        reset($this->hours);
        return false;
    }

    function getnextright()
    {
        if ($this->right = & current($this->rights))
        {
            next($this->rights);
            $this->altbg = !$this->altbg;
            $this->right['quantity_available'] = $this->right['quantity_available'] - $this->right['waiting'];
            $this->right['previsional_available'] = $this->right['quantity_available'] - $this->right['previsional'];

            $this->total_available[$this->right['quantity_unit']] += $this->right['quantity_available'];
            $this->total_waiting[$this->right['quantity_unit']] += $this->right['waiting'];
            $this->total_previsional[$this->right['quantity_unit']] += $this->right['previsional'];
            $this->total_prevavail[$this->right['quantity_unit']] += $this->right['previsional_available'];

            $this->right['quantity_available'] = bab_toHtml(absences_quantity($this->right['quantity_available'], $this->right['quantity_unit']));
            $this->right['previsional_available'] = bab_toHtml(absences_quantity($this->right['previsional_available'], $this->right['quantity_unit']));
            $this->right['waiting'] = bab_toHtml(absences_quantity($this->right['waiting'], $this->right['quantity_unit']));
            $this->right['previsional'] = bab_toHtml(absences_quantity($this->right['previsional'], $this->right['quantity_unit']));

            if (isset($this->right['type']))
            {
                $this->color = bab_toHtml($this->right['type']->color);
                $this->type = bab_toHtml($this->right['type']->name);
            } else {
                $this->color = null;
                $this->type = null;
            }

            return true;
        }
        else
            return false;

    }

    private function getDisplay($arr)
    {
        $list = array();
        foreach($arr as $quantity_unit => $quantity) {
            if ($quantity) {
                $list[] = absences_quantity($quantity, $quantity_unit);
            }
        }

        return implode(', ', $list);
    }

    public function gettotal()
    {
        static $call = null;

        if (isset($call)) {
            return false;
        }

        $call = true;

        $this->total_available = bab_toHtml($this->getDisplay($this->total_available));
        $this->total_waiting = bab_toHtml($this->getDisplay($this->total_waiting));
        $this->total_previsional = bab_toHtml($this->getDisplay($this->total_previsional));
        $this->total_prevavail = bab_toHtml($this->getDisplay($this->total_prevavail));

        return true;
    }

}