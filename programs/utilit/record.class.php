<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once $GLOBALS['babInstallPath'].'utilit/iterator/iterator.php';


abstract class absences_Record
{
	/**
	 *
	 * @var int
	 */
	public $id;
	
	/**
	 *
	 * @var array
	 */
	protected $row;
	
	
	/**
	 * Set table row as an array
	 * @param array $row
	 * @return absences_Right
	 */
	public function setRow($row)
	{
		if (isset($row['id']))
		{
			$this->id = $row['id'];
		}
		$this->row = $row;
		return $this;
	}
	
	abstract public function getRow();
	
	public function __isset($property)
	{
		$row = $this->getRow();
		return isset($row[$property]);
	}
	
	public function __get($property)
	{
		$row = $this->getRow();
		
		if (!isset($row[$property]))
		{
			require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
			bab_debug_print_backtrace();
			bab_debug($row);
			throw new Exception(sprintf('Failed to load property %s on %s', $property, get_class($this)));
		}
		
		return $row[$property];
	}
	
	public function __set($property, $value)
	{
		if (!isset($this->row))
		{
			if (isset($this->id))
			{
				$this->row = $this->getRow();
			} else {
				$this->row = array();
			}
		}
		
		$this->row[$property] = $value;
		
	}
	
	
	public function getId()
	{
	    return $this->id;
	}
}









/**
 *
 *
 */
abstract class absences_Iterator extends BAB_MySqlResultIterator
{

	
	protected function getRowByPrefix($data, $prefix)
	{
		$row = array();
		foreach($data as $key => $value)
		{
			if ($prefix === mb_substr($key, 0, mb_strlen($prefix)))
			{
				$row[mb_substr($key, mb_strlen($prefix) + 2)] = $value;
			}
		}

		return $row;
	}

	
}



interface absences_RightSort
{
	/**
	 * Method used with bab_Sort
	 */
	public function getSortKey();
	
	public function setSortKey($i);
	
	public function getSortLabel();
	
	public function getIconClassName();
	
	public function getId();
}