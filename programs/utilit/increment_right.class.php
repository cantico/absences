<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/





require_once dirname(__FILE__).'/increment.class.php';

/**
 * @property int	$id_right
 *
 */
class absences_IncrementRight extends absences_IncrementRecord
{
	private $right;
	
	/**
	 * @return absences_DynamicRight
	 */
	public static function getById($id)
	{
		$dright = new absences_IncrementRight;
		$dright->id = $id;
	
		return $dright;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load increment right, missing id');
			}
	
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_increment_right WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}

	
	
	/**
	 *
	 * @param absences_Right $right
	 * @return absences_IncrementRight
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			require_once dirname(__FILE__).'/right.class.php';
	
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_user_right']);
		}
	
		return $this->right;
	}
	
	
	
	
	/**
	 * Save element (insert or update or delete)
	 */
	public function save()
	{
		global $babDB;
	
		if (isset($this->id))
		{
			$quantity = (int) round(100 * $this->quantity);
	
			if (0 === $quantity)
			{
				// if quantity has been set to 0, the element must be deleted
	
				$babDB->db_query("DELETE FROM absences_increment_right WHERE id=".$babDB->quote($this->id));
	
			} else {
	
	
				$babDB->db_query("
					UPDATE absences_increment_right 
					SET
						quantity=".$babDB->quote($this->quantity)." 
					WHERE
						id=".$babDB->quote($this->id)
				);
	
			}
	
	
		} else {
	
	
			$babDB->db_query("
				INSERT INTO absences_increment_right 
					(id_right, quantity, monthkey, createdOn)
				VALUES
					(
					" .$babDB->quote($this->id_right). ",
					" .$babDB->quote($this->quantity). ",
			        " .$babDB->quote($this->monthkey). ",
					" .$babDB->quote($this->createdOn). "
			)
					");
	
			$this->id = $babDB->db_insert_id();
		}
		
		
	}
	
	
	public function saveOrUpdate()
	{
	    global $babDB;
	
	    $res = $babDB->db_query('SELECT id FROM absences_increment_right
	            WHERE monthkey='.$babDB->quote($this->monthkey).'
	            AND id_right='.$babDB->quote($this->id_right));
	
	    if ($arr = $babDB->db_fetch_assoc($res)) {
	        $this->id = (int) $arr['id'];
	    }
	
	    $this->save();
	}
}




class absences_IncrementRightIterator extends absences_Iterator
{
    protected $right;
    
    /**
     * @var string YYYY-MM-DD
     */
    public $upto;


    public function setRight(absences_Right $right)
    {
        $this->right = $right;
    }


    public function executeQuery()
    {
        if(is_null($this->_oResult))
        {
            global $babDB;
            $req = "SELECT
            ir.*
            FROM
            absences_increment_right ir
            WHERE  
                ir.id_right=".$babDB->quote($this->right->id);
            
            if (isset($this->upto) && '0000-00-00' !== $this->upto) {
                $req .= ' AND ir.createdOn<='.$babDB->quote($this->upto.' 23:59:59');
            }
            
            $req .= " ORDER BY ir.createdOn, ir.id";

            $this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
        }
    }


    public function getObject($data)
    {
        $dynRight = absences_IncrementRight::getById($data['id']);
        $dynRight->setRow($data);
        $dynRight->setRight($this->right);

        return $dynRight;
    }


}