<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



require_once dirname(__FILE__).'/../functions.php';
require_once dirname(__FILE__).'/agent.class.php';

class Func_AbsencesAgent extends bab_functionality 
{
	public function getDescription()
	{
		return absences_translate('Agent API from the absences addon');
	}
	
	/**
	 * Get agent by id_user
	 * @throws Exception if agent does not exists
	 * @return absences_Agent
	 */
	public function getAgent($id_user)
	{
		$agent = absences_Agent::getFromIdUser($id_user);
		$agent->getRow();
		
		return $agent;
	}
	
	
	/**
	 * Set parameters for agent
	 * associate rights to agent according to collection
	 * if the agent does not exists, il will be created
	 * 
	 * @param	int		$id_user
	 * @param	int		$id_collection
	 * @param	int		$id_sa				Approval scheme
	 * @param	int		$id_sa_cet			Optionnal approval scheme for time saving account 
	 * @param	int		$id_sa_recover		Optionnal approval scheme for work period recovery
	 * 
	 * @throws Exception
	 * 
	 */
	public function setAgent($id_user, $id_collection, $id_sa, $id_sa_cet = 0, $id_sa_recover = 0)
	{
		require_once dirname(__FILE__).'/vacincl.php';
		
		absences_setAgentInfos($id_user,  $id_collection, $id_sa, $id_sa_cet, $id_sa_recover);
		absences_setCollectionRights($id_user, $id_collection);
	}
	
	

	
	
	
	/**
	 * Remove agent and requests associted with it
	 */
	public function deleteAgent($id_user)
	{
		$agent = absences_Agent::getFromIdUser($id_user);
		$agent->delete();
	}
	
	
	/**
	 * get the list of available collections as array
	 * @return array[absences_Collection]
	 */
	public function getCollections()
	{
		global $babDB;
		$return = array();
		
		$res = $babDB->db_query("select * from absences_collections order by name asc");
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$id = (int) $arr['id'];
			$collection = new absences_Collection();
			$collection->setRow($arr);
			
			$return[$id] = $collection;
		}
		
		return $return;
	}
	
	/**
	 * Get the list of rights
	 * @return absences_Right[]
	 */
	public function getRightIterator()
	{
		require_once dirname(__FILE__).'/right.class.php';
		return new absences_RightIterator();
	}
	
}