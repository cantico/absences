<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/entry.class.php';
require_once dirname(__FILE__).'/agent.class.php';
require_once dirname(__FILE__).'/organization.class.php';

class absences_ExportEntry
{
    /**
     * @var BAB_DateTime
     */
    protected $dateb;

    /**
     * @var BAB_DateTime
     */
    protected $datee;

    
    /**
     * @var bool
     */
    private $splittype;
    
    /**
     * @var absences_EntryIterator
     */
    private $iterator;

    /**
     * @var string
     */
    private $separ;

    /**
     * @var string
     */
    private $sepdec;

    /**
     * @var array
     */
    private $users_with_requests = array();

    /**
     * @var bool
     */
    private $users_without_requests;

    /**
     * @var array
     */
    private $dirfields;


    private $types = array();

    /**
     * @var int
     */
    private $organization = '';

    public function __construct(BAB_DateTime $dateb, BAB_DateTime $datee, $idstatus, $wsepar, $separ, $sepdec, $users_without_requests, $dirfields, $organization, $splittype)
    {
        $this->dateb = $dateb;
        $this->datee = $datee;
        $this->organization = $organization;

        $status = array();
        if (in_array(0,$idstatus)) {
            $status[] = '';
        }
        
        if (in_array(1,$idstatus)) {
            $status[] = 'Y';
        }
        
        if (in_array(2,$idstatus)) {
            $status[] = 'N';
        }
        
        $this->splittype = (bool) $splittype;

        if ($this->splittype) {
            $this->iterator = $this->getRequestTypeIterator($status);
        } else {
            $this->iterator = $this->getRequestIterator($status);
        }

        switch($wsepar)
        {
            case "1":
                $this->separ = ",";
                break;
            case "2":
                $this->separ = "\t";
                break;
            case "3":
                $this->separ = ";";
                break;
            default:
                $this->separ = $separ;
                if( empty($separ)) {
                    $this->separ = ",";
                }
                break;
        }

        $this->sepdec = $sepdec;

        $this->users_without_requests = (bool) $users_without_requests;

        $this->dirfields = $dirfields;
    }
    
    
    private function getRequestIterator($status)
    {
        $I = new absences_EntryIterator();
        $I->from = $this->dateb->getIsoDateTime();
        $I->to = $this->datee->getIsoDateTime();
        
        if($this->organization){
            $I->organization = (int) $this->organization ;
        }
        
        
        $I->status = $status;
        
        return $I;
    }
    
    
    
    private function getRequestTypeIterator($status)
    {
        $I = new absences_ElementEntryIterator();
        $I->from = $this->dateb->getIsoDateTime();
        $I->to = $this->datee->getIsoDateTime();
        
        if($this->organization ){
            $I->organization = (int) $this->organization ;
        }
        
        $I->status = $status;
        
        return $I;
    }


    private function arr_csv(&$value)
    {
        $value = str_replace("\n"," ",$value);
        $value = str_replace('"',"'",$value);
        $value = '"'.$value.'"';
    }


    private function numberFormat($quantity)
    {
        return number_format($quantity, 1, $this->sepdec, '');
    }
    
    
    
    


    protected function getEntriesHeader()
    {
        global $babDB;
        
        $line = array();

        if ($this->dirfields) {
            $ov_fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
        }

        $line[] = absences_translate("lastname");
        $line[] = absences_translate("firstname");
        $line[] = absences_translate("Created on");
        $line[] = absences_translate("Modified on");
        
        $dirfields = array_keys($this->dirfields);
        foreach ($dirfields as $name) {
            $line[] = $ov_fields[$name]['name'];
        }
        $line[] = absences_translate("Begin date");
        $line[] = absences_translate("Begin hour");
        $line[] = absences_translate("End date");
        $line[] = absences_translate("End hour");
        $line[] = absences_translate("Status");
        $line[] = absences_translate("Total days");
        $line[] = absences_translate("Total hours");

        $res = $babDB->db_query("SELECT 
                t.id,
                t.name, 
                COUNT(rd.id) with_day,
                COUNT(rh.id) with_hour 
            FROM absences_types t 
                LEFT JOIN absences_rights rd ON rd.id_type=t.id AND rd.quantity_unit='D' 
                LEFT JOIN absences_rights rh ON rh.id_type=t.id AND rh.quantity_unit='H' 
            GROUP BY t.id 
            ORDER BY t.name 
        ");
        while ($arr = $babDB->db_fetch_assoc($res))
        {
            if ((bool) $arr['with_day']) {
                $line[] = $arr['name'].' '.absences_translate('days');
            }
            
            if ((bool) $arr['with_hour']) {
                $line[] = $arr['name'].' '.absences_translate('hours');
            }
            
            $this->types[] = array(
                'id'        => $arr['id'],
                'with_day'  => (bool) $arr['with_day'],
                'with_hour' => (bool) $arr['with_hour']
            );
        }
        array_walk($line, array($this, 'arr_csv'));

        return $line;
    }
    
    
    protected function getElementsHeader()
    {
        $line = array();
        
        if ($this->dirfields) {
            $ov_fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
        }
        
        $line[] = absences_translate("lastname");
        $line[] = absences_translate("firstname");
        $line[] = absences_translate("Created on");
        $line[] = absences_translate("Modified on");
        
        $dirfields = array_keys($this->dirfields);
        foreach ($dirfields as $name) {
            $line[] = $ov_fields[$name]['name'];
        }
        $line[] = absences_translate("Type");
        $line[] = absences_translate("Right");
        $line[] = absences_translate("Begin date");
        $line[] = absences_translate("Begin hour");
        $line[] = absences_translate("End date");
        $line[] = absences_translate("End hour");
        $line[] = absences_translate("Status");
        $line[] = absences_translate("Days");
        $line[] = absences_translate("Hours");
        
        array_walk($line, array($this, 'arr_csv'));
        
        return $line;
    }
    
    
    public function getHeader()
    {
        if ($this->splittype) {
            return $this->getElementsHeader();
        }
        
        return $this->getEntriesHeader();
    }


    private function getAgentDirValue(absences_Agent $agent, $fieldname)
    {
        $direntry = $agent->getDirEntry();

        if (isset($direntry[$fieldname]))
        {
            return $direntry[$fieldname]['value'];
        }

        return '';
    }


    /**
     * Quantity for one type
     * @return float
     */
    private function getTypeDays(absences_Entry $entry, $id_type)
    {
        return $entry->getPlannedDaysBetween($this->iterator->from, $this->iterator->to, $id_type);
    }


    /**
     * Quantity for one type
     * @return float
     */
    private function getTypeHours(absences_Entry $entry, $id_type)
    {
        return $entry->getPlannedHoursBetween($this->iterator->from, $this->iterator->to, $id_type);
    }

    /**
     * Entry begin timestamp
     *
     * if overlap with the end date :
     * First try with planned periods (in database for absences >= 2.46)
     * second with the current working periods (if not removed by configuration)
     * last with the iterator boundaries
     *
     * @return int
     */
    private function getBeginTs(absences_Entry $entry)
    {
        if ($this->iterator->from > $entry->date_begin) {

            $day = substr($this->iterator->from, 0, 10);
            $planned = $entry->getDayPlannedPeriods($day);

            if (0 === count($planned)) {
                $workingPeriods = $entry->getDayWorkingPeriods($day);

                if (0 === count($workingPeriods)) {
                    return bab_mktime($this->iterator->from);
                }

                $firstPeriod = reset($workingPeriods);
                return $firstPeriod->ts_begin;
            }

            $firstperiod = reset($planned);

            return bab_mktime($firstperiod->date_begin);
        }

        return bab_mktime($entry->date_begin);
    }

    /**
     * Entry end timestamp
     *
     * if overlap with the end date :
     * First try with planned periods (in database for absences >= 2.46)
     * second with the current working periods (if not removed by configuration)
     * last with the iterator boundaries
     *
     * @return int
     */
    private function getEndTs(absences_Entry $entry)
    {
        if ($this->iterator->to < $entry->date_end) {

            $end = $this->datee->cloneDate();
            $end->less(1, BAB_DATETIME_DAY); // un jour a ete ajoute pour inclure le dernier jour dans l'iterateur
                                             // on enleve 1 jour pour retrouver la date saisie
            $day = $end->getIsoDate();
            $planned = $entry->getDayPlannedPeriods($day);

            if (0 === count($planned)) {
                $workingPeriods = $entry->getDayWorkingPeriods($day);

                if (0 === count($workingPeriods)) {
                    return bab_mktime($this->iterator->to);
                }

                $lastPeriod = end($workingPeriods);
                return $lastPeriod->ts_end;
            }

            $lastperiod = end($planned);

            return bab_mktime($lastperiod->date_end);
        }

        return bab_mktime($entry->date_end);
    }
    
    
    
    protected function getElementRow(absences_EntryElem $elem)
    {
        $entry = $elem->getEntry();
        $right = $elem->getRight();
        $type = $right->getType();
        $agent = $entry->getAgent();
        
        $line = array();
        

        $line[] = $this->getAgentDirValue($agent, 'sn');
        $line[] = $this->getAgentDirValue($agent, 'givenname');
        $line[] = bab_shortDate(bab_mktime($entry->createdOn), false);
        $line[] = bab_shortDate(bab_mktime($entry->modifiedOn()), false);
        foreach($this->dirfields as $name => $dummy)
        {
            $line[] = $this->getAgentDirValue($agent, $name);
        }
        $line[] = $type->name;
        $line[] = $right->description;
        
        $begin = bab_mktime($elem->date_begin);
        $end = bab_mktime($elem->date_end);
        
        $line[] = bab_shortDate($begin, false);
        $line[] = date('H:i', $begin);
        $line[] = bab_shortDate($end, false);
        $line[] = date('H:i', $end);
        $line[] = $entry->getShortStatusStr();
        
        $line[] = $this->numberFormat($elem->getDays());
        $line[] = $this->numberFormat($elem->getHours());
        
        array_walk($line, array($this, 'arr_csv'));

        return $line;
    }


    /**
     *
     * @return array
     */
    public function getEntryRow(absences_Entry $entry)
    {
        $agent = $entry->getAgent();

        if (isset($agent)) {
            $this->users_with_requests[] = $agent->getIdUser();
        }

        $line = array();
        $line[] = $this->getAgentDirValue($agent, 'sn');
        $line[] = $this->getAgentDirValue($agent, 'givenname');
        
        
        if (empty($line[0]) && empty($line[1])) {
            // the user has been deleted?
            return null;
        }
        
        
        $line[] = bab_shortDate(bab_mktime($entry->createdOn), false);
        $line[] = bab_shortDate(bab_mktime($entry->modifiedOn()), false);

        foreach($this->dirfields as $name => $dummy)
        {
            $line[] = $this->getAgentDirValue($agent, $name);
        }

        $begin = $this->getBeginTs($entry);
        $end = $this->getEndTs($entry);

        $line[] = bab_shortDate($begin, false);
		$line[] = date('H:i', $begin);
		$line[] = bab_shortDate($end, false);
		$line[] = date('H:i', $end);
        $line[] = $entry->getShortStatusStr();

        $pos = count($line);
        $tdays = 0.0;
        $thours = 0.0;
        foreach ($this->types as $_arr) {
            
            $id_type = $_arr['id'];
            
            if ($_arr['with_day']) {
                $days = $this->getTypeDays($entry, $id_type);
                $line[] = $this->numberFormat($days);
                $tdays += $days;
            }
            
            if ($_arr['with_hour']) {
                $hours = $this->getTypeHours($entry, $id_type);
                $line[] = $this->numberFormat($hours);
                $thours += $hours;
            }
            
        }
        
        
        if (0 === (int) round(100 * $tdays) && 0 === (int) round(100 * $thours)) {
            return null;
        }
        

        array_splice($line, $pos++, 0, $this->numberFormat($tdays));
        array_splice($line, $pos++, 0, $this->numberFormat($thours));

        array_walk($line, array($this, 'arr_csv'));

        return $line;
    }

    
    
    public function getRow($object)
    {
        if ($this->splittype) {
            return $this->getElementRow($object);
        }
        
        return $this->getEntryRow($object);
    }
    
    

    /**
     * @return absences_Agent[]
     */
    private function getUsersWithoutRequests()
    {
        $I = new absences_AgentIterator();
        if (count($this->users_with_requests) > 0)
        {
            $I->exclude_users = $this->users_with_requests;
        }
        if($this->organization){
        	$Orga = absences_Organization::getById($this->organization);
        	$I->setOrganization($Orga);
        }
        return $I;
    }


    /**
     * @return array
     */
    public function getAgentRow(absences_Agent $agent)
    {

        $line = array();
        $line[] = $this->getAgentDirValue($agent, 'sn');
        $line[] = $this->getAgentDirValue($agent, 'givenname');
        $line[] = '';
        $line[] = '';
        foreach($this->dirfields as $name => $dummy)
        {
            $line[] = $this->getAgentDirValue($agent, $name);
        }

        $line[] = '';
        $line[] = '';
        $line[] = '';
        $line[] = '';
        $line[] = '';
        $line[] = 0;
        $line[] = 0;
        foreach ($this->types as $_arr) {
            if ($_arr['with_day']) {
                $line[] = 0;
            }
            
            if ($_arr['with_hour']) {
                $line[] = 0;
            }
        }

        array_walk($line, array($this, 'arr_csv'));

        return $line;
    }



    public function output()
    {

        header("Content-Disposition: attachment; filename=\"".absences_translate("Vacation").".csv\""."\n");
        header("Content-Type: text/csv"."\n");

        $header = $this->getHeader();
        echo implode($this->separ, $header)."\n";

        foreach ($this->iterator as $object) {
            $line = $this->getRow($object);
            
            
            if (!isset($line)) {
                // the entry contain no total quantity or no directory entry
                continue;
            }

            

            echo implode($this->separ, $line)."\n";
        }

        if ($this->users_without_requests) {
            foreach ($this->getUsersWithoutRequests() as $agent) {
                $line = $this->getAgentRow($agent);
                echo implode($this->separ, $line)."\n";
            }
        }

        exit;
    }
}

