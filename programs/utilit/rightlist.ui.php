<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/




class absences_RightList extends absences_Paginate
{
    public $typetxt;
    public $desctxt;
    public $quantitytxt;
    public $creditortxt;
    public $datetxt;
    public $date2txt;
    public $vrurl;
    public $vrviewurl;
    public $description;

    public $typename;
    public $quantity;
    public $creditor;
    public $date;
    public $addtxt;
    public $addurl;
    public $filteron;
    public $statustxt;
    public $activeyes;
    public $activeno;
    public $yselected;
    public $nselected;

    public $urllistp;
    public $altlistp;
    public $selected;

    public $begintxt;
    public $endtxt;

    public $arr = array();
    public $count;
    public $res;
    public $topurl;
    public $bottomurl;
    public $nexturl;
    public $prevurl;
    public $topname;
    public $bottomname;
    public $nextname;
    public $prevname;
    public $pos;
    public $bclose;
    public $closedtxt;
    public $openedtxt;
    public $statusval;
    public $alttxt;
    public $altbg = true;

    public function __construct($idtype, $idcoll, $rgroup, $dateb, $datee, $active, $pos, $archived, $recovery, $name)
    {

        $this->desctxt = absences_translate("Description");
        $this->typetxt = absences_translate("Type");
        $this->nametxt = absences_translate("Name");
        $this->kindtxt = absences_translate("Right kind");
        $this->quantitytxt = absences_translate("Quantity");
        $this->creditortxt = absences_translate("Author");
        $this->datetxt = absences_translate("Entry date");
        $this->date2txt = absences_translate("Entry date ( dd-mm-yyyy )");
        $this->addtxt = absences_translate("Allocate vacation rights");
        $this->filteron = absences_translate("Filter on");
        $this->begintxt = absences_translate("Begin");
        $this->endtxt = absences_translate("End");
        $this->altlistp = absences_translate("Beneficiaries");
        $this->statustxt = absences_translate("Active");
        $this->activeyes = absences_translate("Opened rights");
        $this->activeno = absences_translate("Closed rights");
        $this->closedtxt = absences_translate("Vac. closed");
        $this->openedtxt = absences_translate("Vac. opened");
        $this->alttxt = absences_translate("Modify");
        $this->t_edit = absences_translate("Modification");
        $this->t_first_page = absences_translate("First page");
        $this->t_previous_page = absences_translate("Previous page");
        $this->t_next_page = absences_translate("Next page");
        $this->t_last_page = absences_translate("Last page");
        $this->t_available = absences_translate("Availability");
        $this->topurl = "";
        $this->bottomurl = "";
        $this->nexturl = "";
        $this->prevurl = "";
        $this->yselected = "";
        $this->nselected = "";
        $this->t_position = '';
        global $babDB;

        $this->pos = $pos;

        $aaareq = array();
        $req = "".ABSENCES_RIGHTS_TBL." r LEFT JOIN ".ABSENCES_TYPES_TBL." t ON t.id=r.id_type where ";

        if( $name != "") {
            $aaareq[] = "r.description LIKE '%".$babDB->db_escape_like($name)."%'";
        }

        if( $active != "")
            $aaareq[] = "r.active='".$babDB->db_escape_string($active)."'";

        if( $idcoll != "")
        {
            $aaareq[] = "r.id IN(SELECT id_right FROM absences_coll_rights WHERE id_coll=".$babDB->quote($idcoll).")";
        }
        
        if( $idtype != "")
        {
            $aaareq[] = "r.id_type=".$babDB->quote($idtype);
        }

        if( $rgroup != "")
        {
            $aaareq[] = "r.id_rgroup=".$babDB->quote($rgroup);
        }

        if( $dateb != "" )
        {
            $ar = explode("-", $dateb);
            $dateb = $ar[2]."-".$ar[1]."-".$ar[0];
        }

        if( $datee != "" )
        {
            $ar = explode("-", $datee);
            $datee = $ar[2]."-".$ar[1]."-".$ar[0];
        }

        if( $dateb != "" && $datee != "")
        {
            $aaareq[] = "( r.date_entry between '".$babDB->db_escape_string($dateb)."' and '".$babDB->db_escape_string($datee)."')";
        }
        else if( $dateb == "" && $datee != "" )
        {
            $aaareq[] = "r.date_entry <= '".$babDB->db_escape_string($datee)."'";
        }
        else if ($dateb != "" )
        {
            $aaareq[] = "r.date_entry >= '".$babDB->db_escape_string($dateb)."'";
        }

        if ($archived)
        {
            $aaareq[] = "r.archived = ".$babDB->quote(1);
        } else {
            $aaareq[] = "r.archived = ".$babDB->quote(0);
        }

        if ($recovery)
        {
            $aaareq[] = "r.kind = ".$babDB->quote(absences_Right::RECOVERY);
        } else {
            $aaareq[] = "r.kind <> ".$babDB->quote(absences_Right::RECOVERY);
        }


        if( isset($aaareq) && sizeof($aaareq) > 0 )
        {
            if( sizeof($aaareq) > 1 )
                $req .= implode(' and ', $aaareq);
            else
                $req .= $aaareq[0];
        }
        $req .= " order by r.date_entry desc, r.id";

        list($total) = $babDB->db_fetch_row($babDB->db_query("select count(*) as total from ".$req));

        $this->paginate($total, ABSENCES_MAX_RIGHTS_LIST);

        if( $total > ABSENCES_MAX_RIGHTS_LIST)
        {
            $req .= " limit ".$pos.",".ABSENCES_MAX_RIGHTS_LIST;
        }
        bab_debug("select r.*, t.name type, t.color typecolor from ".$req);
        $this->res = $babDB->db_query("select r.*, t.name type, t.color typecolor from ".$req);
        $this->count = $babDB->db_num_rows($this->res);
        $this->addurl = absences_addon()->getUrl()."vacadma&idx=addvr";


        $this->dateburl = $GLOBALS['babUrlScript']."?tg=month&callback=dateBegin&ymin=0&ymax=3";
        $this->dateeurl = $GLOBALS['babUrlScript']."?tg=month&callback=dateEnd&ymin=0&ymax=3";

        $this->searchform = $this->getSearchForm();
    }


    private function getSearchForm()
    {
        global $babDB;
        bab_functionality::includeOriginal('Icons');
        $W = bab_Widgets();
        $form = $W->Form(null, $W->FlowLayout()->setSpacing(1,'em', 3,'em'));
        $form->setSelfPageHiddenFields()->setReadOnly();

        $form->addClass('widget-bordered');
        $form->addClass('BabLoginMenuBackground');
        $form->addClass('widget-centered');
        $form->addClass(Func_Icons::ICON_LEFT_16);
        $form->colon();

        $form->setCanvasOptions($form->Options()->width(97,'%'));
        
        
        
        $form->addItem(
            $W->LabelledWidget(
                absences_translate('Name'),
                $W->LineEdit(),
                'name'
            )
        );
        
        

        $types = $W->Select()->addOption('', '');
        $restype = $babDB->db_query("select * from ".ABSENCES_TYPES_TBL." order by name asc");
        while ($arr = $babDB->db_fetch_assoc($restype))
        {
            $types->addOption($arr['id'], $arr['name']);
        }

        $form->addItem(
                $W->LabelledWidget(
                        absences_translate('Type'),
                        $types,
                        'idtype'
                )
        );


        $collections = $W->Select()->addOption('', '');
        $rescoll = $babDB->db_query("select * from ".ABSENCES_COLLECTIONS_TBL." order by name asc");
        while ($arr = $babDB->db_fetch_assoc($rescoll))
        {
            $collections->addOption($arr['id'], $arr['name']);
        }

        $form->addItem(
                $W->LabelledWidget(
                        absences_translate('Collection'),
                        $collections,
                        'idcoll'
                )
        );


        $rgroups = $W->Select()->addOption('', '');
        $resrg = $babDB->db_query("select * from absences_rgroup order by name asc");
        while ($arr = $babDB->db_fetch_assoc($resrg))
        {
            $rgroups->addOption($arr['id'], $arr['name']);
        }


        $form->addItem(
                $W->LabelledWidget(
                        absences_translate('Right group'),
                        $rgroups,
                        'rgroup'
                )
        );


        $form->addItem(
                $W->LabelledWidget(
                        absences_translate('Status'),
                        $W->Select()
                        ->addOption('', '')
                        ->addOption('Y', absences_translate('Active'))
                        ->addOption('N', absences_translate('Inactive')),
                        'active'
                )
        );


        $form->addItem(
                $W->LabelledWidget(
                        absences_translate('Write date'),
                        $W->PeriodPicker()->setNames('dateb', 'datee')
                )
        );


        $form->setValues($_REQUEST);



        $form->addItem($W->SubmitButton()->setLabel(absences_translate('Search')));

        return $form->display($W->HtmlCanvas());
    }

    public function getnext()
    {
        static $i = 0;
        if( $i < $this->count)
        {
            global $babDB;
            $this->altbg = !$this->altbg;
            $arr = $babDB->db_fetch_array($this->res);


            $this->typecolor	= bab_toHtml($arr['typecolor']);
            $this->typename		= bab_toHtml($arr['type']);

            unset($arr['typecolor']);
            unset($arr['type']);


            $right = new absences_Right($arr['id']);
            $right->setRow($arr);


            $this->vrurl		= bab_toHtml(absences_addon()->getUrl()."vacadma&idx=modvr&idvr=".$arr['id']);
            $this->vrviewurl	= bab_toHtml(absences_addon()->getUrl()."vacadma&idx=viewvr&idvr=".$arr['id']);

            $this->kind 		= bab_toHtml($right->getKindLabel());
            $this->description	= bab_toHtml($right->description);
            if (0 === (int) round(100*$arr['quantity']))
            {
                $this->quantity	= '';
            } else {
                $this->quantity	= bab_toHtml(absences_quantity($arr['quantity'], $arr['quantity_unit']));
            }
            $this->creditor		= bab_toHtml(bab_getUserName($arr['id_creditor']));
            $this->date			= bab_toHtml(bab_shortDate(bab_mktime($arr['date_entry']), false));
            $this->bclose		= $arr['active'] == "N";


            $this->available	= $right->isAvailable() ? absences_translate('Available') : '';

            if( $this->bclose )
                $this->statusval = $this->closedtxt;
            else
                $this->statusval = $this->openedtxt;

            if (absences_Right::RECOVERY === $right->getKind())
            {
                $this->urllistp = false;
            } else {
                $this->urllistp = absences_addon()->getUrl()."vacadma&idx=lvrc&idvr=".$arr['id'];
            }

            $i++;
            return true;
        }
        else
            return false;

    }

}





/**
 * Display a menu to add items to the agent list
 * @return string
 */
function absences_rightListMenu()
{
    $toolbar = absences_getToolbar();

    $sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';

    $url = bab_url::get_request('tg', 'filter');
    $url->idx = 'addvr';

    $toolbar->addToolbarItem(
        new BAB_ToolbarItem(absences_translate('Add'), $url->toString(),
            $sImgPath . 'edit_add.png', '', '', '')
    );
    

    return $toolbar->printTemplate();
}
