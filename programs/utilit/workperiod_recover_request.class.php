<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/request.class.php';



/**
 * Work period recovery request
 * 
 * 
 * @property	int			$id_user
 * @property	string		$date_begin
 * @property	string		$date_end
 * @property	int			$id_type
 * @property	int			$idfai
 * @property	string		$comment
 * @property	string		$createdOn
 * @property	string		$modifiedOn
 * @property	string		$status
 * @property	int			$id_approver
 * @property	int			$id_right
 * @property	float		$quantity
 * @property	string		$quantity_unit
 * @property	string		validity_end
 * @property	int			$archived
 */
class absences_WorkperiodRecoverRequest extends absences_Request
{
	/**
	 * @var absences_WorkperiodType
	 */
	private $type;
	
	
	/**
	 * @param int $id
	 * @return absences_WorkperiodRecoverRequest
	 */
	public static function getById($id)
	{
		$request = new absences_WorkperiodRecoverRequest();
		$request->id = $id;
	
		return $request;
	}
	
	
	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			$query = 'SELECT * FROM absences_workperiod_recover_request WHERE id='.$babDB->quote($this->id);
	
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	
	/**
	 *
	 * @param absences_WorkperiodType $type
	 * @return absences_WorkperiodRecoverRequest
	 */
	public function setType(absences_WorkperiodType $type)
	{
		$this->type = $type;
		return $this;
	}
	
	
	/**
	 * 
	 * @return absences_WorkperiodType
	 */
	public function getType()
	{
		if (!isset($this->type))
		{
			require_once dirname(__FILE__).'/workperiod_type.class.php';
			
			$row = $this->getRow();
			$this->type = absences_WorkperiodType::getFromId($row['id_type']);
		}
	
		return $this->type;
	}
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getRequestType()
	 *
	 * @return string
	 */
	public function getRequestType()
	{
		return absences_translate('Worked day entitling recovery');
	}
	
	
	
	/**
	 * 
	 */
	public function save()
	{
		global $babDB;
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		if (!isset($this->validity_end) || '0000-00-00' === $this->validity_end)
		{
			$end = BAB_DateTime::fromIsoDateTime($this->date_end);
			$days = absences_getVacationOption('end_recup');
			
			if (!$days) {
			    $days = 365;
			}
			
			// les annees ne foncitonnent pas avec cette API ?
			
			if (($days / 365) >= 1) {
			    $years = (int)($days / 365);
			    $days = $days % 365;
			    
			    $end->add($years, BAB_DATETIME_YEAR);
			}
			
			$end->add($days, BAB_DATETIME_DAY);
			
			$this->validity_end = $end->getIsoDate();
		}
		
		
		
		if (isset($this->id))
		{
		    $query = 'UPDATE absences_workperiod_recover_request SET 
				id_user='.$babDB->quote($this->id_user).', 
				date_begin='.$babDB->quote($this->date_begin).',	
				date_end='.$babDB->quote($this->date_end).', 
				id_type='.$babDB->quote($this->id_type).',	
				idfai='.$babDB->quote($this->idfai).',	
				comment='.$babDB->quote($this->comment).',	
				modifiedOn=NOW(),
				status='.$babDB->quote($this->status).',
				comment2='.$babDB->quote($this->comment2).',
				id_approver='.$babDB->quote($this->id_approver).',
				id_right='.$babDB->quote($this->id_right).',
				quantity='.$babDB->quote($this->quantity).',
				quantity_unit='.$babDB->quote($this->quantity_unit).', 
				validity_end='.$babDB->quote($this->validity_end).'
		    ';
		    
		    if (isset($this->todelete)) {
		        $query .= ', todelete='.$babDB->quote($this->todelete);
		    }
		    
		    $query .= 'WHERE id='.$babDB->quote($this->id);
		    

			$babDB->db_query($query);
			
			
		} else {
		
			$babDB->db_query('INSERT INTO absences_workperiod_recover_request (
					id_user,
					date_begin,
					date_end,
					id_type,
					comment,
					createdOn,
					modifiedOn,
					status,
					quantity,
					quantity_unit,
					validity_end
				) VALUES (
					
					'.$babDB->quote($this->id_user).',
					'.$babDB->quote($this->date_begin).',
					'.$babDB->quote($this->date_end).',
					'.$babDB->quote($this->id_type).',
					'.$babDB->quote($this->comment).',
					NOW(),
					NOW(),
					'.$babDB->quote($this->status).',
					'.$babDB->quote($this->quantity).',
					'.$babDB->quote($this->quantity_unit).',
					'.$babDB->quote($this->validity_end).'
				)
			');
			
			$this->id = $babDB->db_insert_id();
		}
	}
	
	
	
	
	/**
	 * Trouver la duree en calculee dans la meme unite que le type associe
	 * 
	 * duree en jour precis a la demie-journee ou duree en heuress precis a l'heure
	 * 
	 * 
	 * @return float | int
	 */
	public function getComputedDuration()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$unit = $this->getType()->quantity_unit;
		
		if ('H' === $unit)
		{
			return (int) floor((bab_mktime($this->date_end) - bab_mktime($this->date_begin)) / 3600);
		}
		
		// trouver les jours complets
		
		list($day1, $hour1) = explode(' ', $this->date_begin);
		$full1 = BAB_DateTime::fromIsoDateTime($day1.' 00:00:00');
		$full1->add(1, BAB_DATETIME_DAY);
		
		list($day2, $hour2) = explode(' ', $this->date_end);
		$full2 = BAB_DateTime::fromIsoDateTime($day2.' 00:00:00');
		
		$date_begin = BAB_DateTime::fromIsoDateTime($this->date_begin);
		$date_end = BAB_DateTime::fromIsoDateTime($this->date_end);
		
		if ($full1->getIsoDate() > $full2->getIsoDate())
		{
			$full_days = 0;
			
			$diff_hour = round(($date_end->getTimeStamp() - $date_begin->getTimeStamp()) / 3600);
			$diff_days = $diff_hour > 4 ? 1 : .5;
			
			return $diff_days;
			
		} else {
			$full_days = $full2->dateDiffIso($full1->getIsoDate(), $full2->getIsoDate());
			
			bab_debug('Full days : '.$full_days);
			
			$diff_hour1 = round(($full1->getTimeStamp() - $date_begin->getTimeStamp()) / 3600);
			$diff_hour2 = round(($date_end->getTimeStamp() - $full2->getTimeStamp()) / 3600);
			
			bab_debug('before first complete day : '.$diff_hour1.' h');
			bab_debug('after last complete day : '.$diff_hour2.' h');
			
			
			$diff_days1 = $diff_hour1 > 12 ? 1 : .5;
			$diff_days2 = $diff_hour2 > 12 ? 1 : .5;
			
			return ($diff_days1 + $full_days + $diff_days2);
		}

	}
	
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getApprobationId()
	 *
	 * @return int
	 */
	public function getApprobationId()
	{
		if ($agent = $this->getAgent())
		{
			return $agent->getRecoverApprobationId();
		}
	
		return null;
	}
	
	/**
	 * Process specific code when the request is rejected
	 *
	 */
	protected function onReject()
	{
		
	}
	
	
    /**
     * programme de reprise des declaration de jours travaille approuvees par auto-approbation sans droit cree
     */
	public function restoreMissingRight()
	{
	    if ($this->id_right) {
	        return false;
	    }
	    $this->onConfirm();
	}
	
	
	
	public function getRight()
	{
	    if (!$this->id_right) {
	        return null;
	    }
	    
	    require_once dirname(__FILE__).'/right.class.php';
	    
	    $right = new absences_Right($this->id_right);
	    
	    if (!$right->getRow()) {
	        return null;
	    }
	    
	    
	    return $right;
	}

	
	
	/**
	 * Process specific code when the request is confirmed
	 * 
	 */
	protected function onConfirm()
	{
	    if (null !== $this->getRight()) {
	        return;
	    }
	    
		// create right access for recovery only if not allready created
		
		global $babDB;
		require_once dirname(__FILE__).'/right.class.php';
		
		$agent = $this->getAgent();
		
		$begin = mb_substr($this->date_end, 0, 10);
		$end = $this->validity_end;
		
		$date = bab_shortDate(bab_mktime($this->date_begin), false);
		
		$description = $agent->getName().' '.$date;
		
		// si un droit avec cette description existe deja on ne le cree pas
		// mais on fait la liaison avec la periode travaillee
		
		$res = $babDB->db_query('SELECT id FROM absences_rights WHERE description='.$babDB->quote($description));
		$existingRight = $babDB->db_fetch_assoc($res);
		
		if ($existingRight) {
		    $id_right = $existingRight['id'];
		} else {
		    $id_right = $agent->createRecoveryRight($begin, $end, $description, $this->quantity, $this->quantity_unit);
		}
		
		$babDB->db_query('UPDATE absences_workperiod_recover_request 
		    SET id_right='.$babDB->quote($id_right).' 
		    WHERE id='.$babDB->quote($this->id)
		);
	}
	
	public function getTitle()
	{
	    if (isset($this->todelete) && $this->todelete) {
	        return absences_translate('worked period entitling recovery to delete');
	    }
	    
		return absences_translate('worked period entitling recovery');
	}
	
	
	public function getNotifyFields()
	{
		return array(
			absences_translate('From') 	=> bab_shortDate(bab_mktime($this->date_begin)),
			absences_translate('Until')	=> bab_shortDate(bab_mktime($this->date_end)),
			absences_translate('Quantity') 	=> absences_quantity($this->quantity, $this->quantity_unit)
		);
	}
	
	
	
	public function getYear()
	{
		$year = (int) substr($this->date_begin, 0, 4);
	
		if (0 === $year)
		{
			return null;
		}
	
		return $year;
	}
	
	
	public function getArchiveYear()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$year = (int) substr($this->date_begin, 0, 4);
		
		if (0 === $year)
		{
			return null;
		}
		
		$day = absences_getVacationOption('archivage_day');
		$month = absences_getVacationOption('archivage_month');
		
		$currentYear = new BAB_DateTime($year, $month, $day);
		if($this->date_begin < $currentYear->getIsoDate()){
			$year = $year-1;
		}
		
		return $year;
	}
	
	
	public function archive()
	{
		global $babDB;
	
		$babDB->db_query("
			UPDATE absences_workperiod_recover_request 
			SET
				archived=".$babDB->quote(1)."
			WHERE
				id=".$babDB->quote($this->id)."
		");
	}
	
	
	
	public function setNotified()
	{
		global $babDB;
	
		$babDB->db_query("
			UPDATE absences_workperiod_recover_request
			SET
				appr_notified=".$babDB->quote(1)."
			WHERE
				id=".$babDB->quote($this->id)."
				");
	}
	
	
	public function getManagerEditUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		return $addon->getUrl().'vacadmwd&idx=edit&id='.$this->id;
	}
	
	public function getManagerDeleteUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		return $addon->getUrl().'vacadmwd&idx=delete&id='.$this->id;
	}
	
	
	
	/**
	 * @return string
	 */
	public function getEditUrl($rfrom, $ide = null)
	{
		$url = absences_addon()->getUrl()."vacuser&idx=workperiod&id=".$this->id;
	
		if (isset($rfrom))
		{
			$url .= '&rfrom='.$rfrom;
		}
	
		if (isset($ide))
		{
			$url .= '&ide='.$ide;
		}
	
		return $url;
	}
	
	
	
	
	public function getManagerFrame()
	{
		$W = bab_Widgets();
	
		return $W->Link($W->Icon(absences_translate('Worked day entitling recovery'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK), absences_addon()->getUrl()."vacadmwd&idx=edit&id=".$this->id );
	}
	
	
	/**
	 * Get request card frame to display in requests list
	 * @param bool 			$display_username	Affiche le nom du demandeur dans la card frame ou non, utile pour les listes contenant plusieurs demandeurs possibles
	 * @param int			$rfrom				si les demandes de la liste sont modifiee par un gestionnaire ou gestionnaire delegue
 	 * @param int			$ide				id entite de l'organigramme >0 si gestionnaire delegue seulement
	 * @return Widget_Frame
	 */
	public function getCardFrame($display_username, $rfrom, $ide)
	{
		bab_functionality::includeOriginal('Icons');
		$W = bab_Widgets();
		$layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em')->addClass('widget-full-width');
		$frame = $W->Frame(null, $layout);
		$frame->addClass(Func_Icons::ICON_LEFT_16);

		$layout->addItem($col1 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$layout->addItem($col2 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$layout->addItem($col3 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		
		
		$col1->addItem($W->Link($W->Icon(absences_translate('Worked day entitling recovery'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK), absences_addon()->getUrl()."vacuser&idx=view_wp_recovery&id=".$this->id));
		$col1->setCanvasOptions($col1->Options()->width(25,'em'));
		$col2->setCanvasOptions($col1->Options()->width(25,'em'));
		$layout->addItem($col3 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$col2->setSizePolicy(Widget_SizePolicy::MAXIMUM);
		
		$col1->addItem($this->getStatusIcon());
	
		$col2->addItem($W->Title(absences_DateTimePeriod($this->date_begin, $this->date_end), 5));
		
		if ($display_username)
		{
			$col2->addItem($this->labelledValue(absences_translate('Owner'), $this->getUserName()));
		}
	
		$type = $this->getType();
		if ($type && $type->getRow())
		{
			$col2->addItem($this->labelledValue(absences_translate('Type'), $type->name));
		}
		
		
		if ($this->canModify())
		{
			$col3->addItem($W->Link($W->Icon(absences_translate('Modify'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->getEditUrl($rfrom, $ide)));
		}
		
		if ($this->canDelete())
		{
			$urldelete = absences_addon()->getUrl()."vacuser&idx=delete&id_recovery=".$this->id;
			$col3->addItem($W->Link($W->Icon(absences_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $urldelete));
		}
	
		$frame->setTitle(sprintf(absences_translate('Created the %s'), bab_shortDate(bab_mktime($this->createdOn()))));
	
		return $frame;
	}
	
	
	
	/**
	 * Delete request
	 */
	public function delete()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	    
	    $date_begin = BAB_DateTime::fromIsoDateTime($this->date_begin);
	    $date_end = BAB_DateTime::fromIsoDateTime($this->date_end);
	    
		parent::delete();
	
		global $babDB;
		$babDB->db_query("delete from absences_workperiod_recover_request where id=".$babDB->quote($this->id));
		
		$this->addMovement(
		    sprintf(
		        absences_translate('The workperiod recover request from the %s to the %s has been deleted'),
		        $date_begin->shortFormat(true),
		        $date_end->shortFormat(true)
		    )
		);
	}
	
	
	/**
	 * Test form validity
	 * 
	 * @throws Exception
	 * 
	 * @return bool
	 */
	public static function checkForm(Array $workperiod, absences_WorkperiodRecoverRequest $wd = null)
	{
		// il ne doit pas exister de periode de conges sur la meme periode
		// TODO la periode ne doit pas etre travaillee ??
		// il ne doit pas deja exister de periode travaillee donnant droit a recuperation
		
		require_once dirname(__FILE__).'/workperiod_type.class.php';
		require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		
		if (isset($wd))
		{
			$id_user = $wd->id_user;
		} else {
			$id_user = bab_getUserId();
		}
		
		
		global $babDB;
		$W = bab_Widgets();
		$datePicker = $W->DatePicker();
		
		$datebegin = $datePicker->getISODate($workperiod['datebegin']);
		$dateend = $datePicker->getISODate($workperiod['dateend']);
		
		if ('0000-00-00' === $datebegin || '0000-00-00' === $dateend)
		{
			throw new Exception(absences_translate('The begin and end dates are mandatory'));
		}
		
		if (!$datebegin || !$dateend)
		{
			throw new Exception(absences_translate('Begin and end date are not correct'));
		}
		
		$begin = $datebegin.' '.$workperiod['hourbegin'];
		$end = $dateend.' '.$workperiod['hourend'];
		
		if ($begin >= $end)
		{
			throw new Exception(absences_translate('Start date must be prior to the end date'));
		}
		
		
		$delay_recovery = absences_getVacationOption('delay_recovery');
		$delay = BAB_DateTime::now();
		if ($delay_recovery) {
		    // 1 is added for the last 24H in the same day
		  $delay->add(1 + $delay_recovery, BAB_DATETIME_DAY);
		}
		if ($end > $delay->getIsoDateTime())
		{
		    if ($delay_recovery) {
		        throw new Exception(sprintf(absences_translate('The request dates must be in the next %d days'), $delay_recovery));
		    } else {
			    throw new Exception(absences_translate('The request dates must be over'));
		    }
		}
		
		
		
		$res = $babDB->db_query('SELECT * FROM absences_entries WHERE
				id_user='.$babDB->quote($id_user).'
				AND !(date_begin > '.$babDB->quote($end).' OR date_end < '.$babDB->quote($begin).')'
		);
		
		if (0 != $babDB->db_num_rows($res))
		{
			throw new Exception(absences_translate('A vacation request already exist on this period'));
		}
		
		
		$query = 'SELECT * FROM absences_workperiod_recover_request WHERE
				id_user='.$babDB->quote($id_user).'
				AND !(date_begin > '.$babDB->quote($end).' OR date_end < '.$babDB->quote($begin).')
				AND status<>'.$babDB->quote('N');
		
		if (isset($wd))
		{
			$query .= ' AND id<>'.$babDB->quote($wd->id); 
		}
		
		$res = $babDB->db_query($query);
		
		if (0 != $babDB->db_num_rows($res))
		{
			throw new Exception(absences_translate('A workperiod recover request already exist on this period'));
		}
		
		if (!isset($workperiod['id_type']))
		{
			throw new Exception(absences_translate('The type is mandatory'));
		}
		
		$type = absences_WorkperiodType::getFromId($workperiod['id_type']);
		
		if (!$type->getRow())
		{
			throw new Exception(absences_translate('The type is mandatory'));
		}
		
		
		return true;
	}
}







/**
 * Vacation requests
 *
 */
class absences_WorkperiodRecoverRequestIterator extends absences_Iterator
{

	/**
	 *
	 * @var array
	 */
	public $users;
	
	
	/**
	 * Organization filter
	 * @var int
	 */
	public $organization;
	
	
	/**
	 * Filtrer les demandes necessitant ou pas un email aux approbateurs
	 * @var int
	 */
	public $appr_notified;
	
	
	/**
	 * Filtrer les demandes avec unes instance d'approbation
	 * @var bool
	 */
	public $idfai_set;
	
	/**
	 * 
	 * @var string
	 */
	public $status;
	
	/**
	 * Search all request created before this date time
	 * @var string
	 */
	public $createdOn;
	
	
	/**
	 * Search all request modified before this date
	 * @var string
	 */
	public $modifiedOn;
	
	
	/**
	 * Filtrer les demandes par date de debut superieur
	 * @var int
	 */
	public $startFrom;
	
	/**
	 * Filtrer les demandes par date de debut inferieur
	 * @var int
	 */
	public $startTo;
	
	/**
	 * Filtrer les demandes par annee
	 * @var int
	 */
	public $year;
	
	
	/**
	 * Filtrer les demandes par statut d'archivage
	 * @var int
	 */
	public $archived = 0;


	public function getObject($data)
	{

		$entry = new absences_WorkperiodRecoverRequest;
		$entry->setRow($data);
		return $entry;

	}



	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = 'SELECT r.*
			FROM
				absences_workperiod_recover_request r 
			         LEFT JOIN absences_personnel p ON p.id_user=r.id_user 
			';


			$where = array();

			if (isset($this->users))
			{
				$where[] = 'r.id_user IN('.$babDB->quote($this->users).')';
			}
			
			if (isset($this->organization))
			{
			    $where[] = 'p.id_organization='.$babDB->quote($this->organization);
			}
			
			if (isset($this->appr_notified))
			{
				$where[] = 'r.appr_notified='.$babDB->quote($this->appr_notified);
			}
			
			if (isset($this->idfai_set) && $this->idfai_set)
			{
				$where[] = 'r.idfai>'.$babDB->quote(0);
			}
			
			if (isset($this->status))
			{
				$where[] = 'r.status='.$babDB->quote($this->status);
			}
			
			if (isset($this->createdOn))
			{
				$where[] = 'r.createdOn<='.$babDB->quote($this->createdOn);
			}
			
			if (isset($this->modifiedOn))
			{
				$where[] = 'r.modifiedOn<='.$babDB->quote($this->modifiedOn);
			}
			
			if (isset($this->year))
			{
				$where[] = 'YEAR(r.date_begin)='.$babDB->quote($this->year);
			}
			
			if (isset($this->startTo) && $this->startTo != '0000-00-00')
			{
				$where[] = 'r.date_begin <='.$babDB->quote($this->startTo);
			}
			
			if (isset($this->startFrom) && $this->startFrom != '0000-00-00')
			{
				$where[] = 'r.date_begin >='.$babDB->quote($this->startFrom);
			}
			
			if (isset($this->archived))
			{
				$where[] = 'r.archived='.$babDB->quote($this->archived);
			}

			if ($where)
			{
				$req .= ' WHERE '.implode(' AND ', $where);
			}
			
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
}
