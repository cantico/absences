<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

bab_Widgets()->includePhpClass('Widget_Form');


class absences_CetDepositRequestEditor extends Widget_Form
{
	
	protected $deposit;
	
	/**
	 * 
	 * @var absences_AgentCet
	 */
	protected $cet;
	
	/**
	 * 
	 * @var bool
	 */
	protected $withCardFrame;
	
	
	public function __construct(absences_CetDepositRequest $deposit = null, $withCardFrame = null)
	{
		$W = bab_Widgets();
		
		$this->deposit = $deposit;
		$this->withCardFrame = isset($withCardFrame) ? $withCardFrame : isset($deposit);
		
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));
	
		$this->setName('cet');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
	
		$this->setCanvasOptions($this->Options()->width(50,'em'));
	
		require_once dirname(__FILE__).'/agent.class.php';
		
		if (isset($this->deposit))
		{
			$agent = $this->deposit->getAgent();
		} else {
			$agent = absences_Agent::getCurrentUser();
		}
		
		$this->cet = $agent->Cet();
	
		$this->addFields();
		$this->addButtons();
		$this->setSelfPageHiddenFields();
		
		$this->loadValues();
	}
	
	
	protected function loadValues()
	{
		if (isset($_POST['cet']))
		{
			$this->setValues(array('cet' => $_POST['cet']));
			return;
		}
	
		if (isset($this->deposit))
		{
			$values = $this->deposit->getRow();
			$this->setValues(array('cet' => $values));
	
			if (isset($values['id'])) {
			     $this->setHiddenValue('cet[id]', $values['id']);
			}
			
			if (isset($values['id_user'])) {
			    $this->setHiddenValue('cet[id_user]', $values['id_user']);
			}
	
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		if (isset($this->deposit))
		{
			require_once dirname(__FILE__).'/agent.class.php';
			require_once dirname(__FILE__).'/agent.ui.php';
		
			if ($this->withCardFrame)
			{
				$agent = absences_Agent::getFromIdUser($this->deposit->id_user);
				$cardFrame = new absences_AgentCardFrame($agent);
				$this->addItem($cardFrame);
			}
			
			if ('' === $this->deposit->status)
			{
				// modification of a waiting request
				$this->addItem($this->quantity());
				$this->addItem($this->id_agent_right_source());
			} else {
				
				// modification of a confirmed/rejected request
				
				$this->addItem($W->Label(sprintf(absences_translate('Deposit on the time saving account : %s'), absences_quantity($this->deposit->quantity, 'D'))));
				
				$right = $this->deposit->getAgentRightSource()->getRight();
				if (isset($right))
				{
					$this->addItem($W->Label(sprintf(absences_translate('Taken from right : %s'), $right->description)));
				}
			}
		} else {
	
			// a request
			$this->addItem($this->quantity());
			$this->addItem($this->id_agent_right_source());
		}
		
		$this->addItem($this->comment());
	}
	
	
	protected function id_agent_right_source()
	{
		$W = bab_Widgets();
		$select = $W->Select();
		
		foreach($this->cet->getRightsIterator() as $agentRight)
		{
			/*@var $agentRight absences_AgentRight */
			$right = $agentRight->getRight();
			$select->addOption($agentRight->id, $right->description);
		}
		
		return $W->LabelledWidget(
				absences_translate('Taken from right'),
				$select,
				__FUNCTION__
		);
	}
	
	
	
	protected function comment()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			absences_translate('Comment'),
			$W->TextEdit()->setColumns(60)->setLines(4),
			__FUNCTION__
		);
	}
	
	
	protected function quantity()
	{
		$W = bab_Widgets();
		
		$quantity_unit = $this->cet->getQuantityUnit();
		switch($quantity_unit)
		{
			case 'D': 
				$unit = absences_translate('Day(s)');
				break;
				
			case 'H':
				$unit = absences_translate('Hour(s)');
				break;
				
			default:
				$unit = $quantity_unit;
		}
		
		return $W->LabelledWidget(
				absences_translate('Move into the time saving account'), 
				$W->LineEdit()->setSize(6)->setMaxSize(10)->setMandatory(true, absences_translate('The quantity is mandatory')), 
				__FUNCTION__,
				null,
				$unit
		);
	}
	
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save')),
				$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel'))
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}
}




class absences_CetDepositRequestApprobEditor extends absences_CetDepositRequestEditor
{
	public function __construct(absences_CetDepositRequest $deposit)
	{
		
		parent::__construct($deposit);
		
		
	}
	
	/**
	 * Load values into editor
	 * 
	 */
	protected function loadValues()
	{
		if (isset($_POST['cet']))
		{
			$this->setValues(array('cet' => $_POST['cet']));
		} else {
			if (isset($this->deposit))
			{
				$this->setValues(array('cet' => $this->deposit->getRow()));
			}
		}
		
		if (isset($this->deposit))
		{
			$this->setHiddenValue('cet[id]', $this->deposit->id);
			$this->setHiddenValue('id_deposit', $this->deposit->id);
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		require_once dirname(__FILE__).'/agent.ui.php';
		
		$agent = $this->deposit->getAgent();
		$card = new absences_AgentCardFrame($agent);
		
		$this->addItem($card);

		$this->addItem($this->agent_right_source());
		$this->addItem($this->quantity());
		
		if ($comment = $this->comment())
		{
			$this->addItem($comment);
		}
		
		$this->addItem($this->comment2());
		
		$this->addItem($W->Icon(absences_translate('The acceptation will add the specified quantity in the applicant time saving account'), Func_Icons::STATUS_DIALOG_INFORMATION));
	}
	
	/**
	 *
	 */
	protected function agent_right_source()
	{
		$W = bab_Widgets();
		
		$agentRight = $this->deposit->getAgentRightSource();
		if (!$agentRight->getRow())
		{
			$description = absences_translate('Unknown');
		} else {
			$right = $agentRight->getRight();
			$description = $right->description;
		}
		
	
		return $W->VBoxItems(
			$W->Label(absences_translate('From right'))->colon(),
			$W->Label($description)
		);
	
	}
	
	
	/**
	 * 
	 */
	protected function comment()
	{
		$W = bab_Widgets();
		
		if (empty($this->deposit->comment))
		{
			return null;
		}
	
		return $W->VBoxItems(
			$W->Label(absences_translate('Reason for the request'))->colon(),
			$W->RichText($this->deposit->comment)
		);
	}
	
	
	
	protected function comment2()
	{
		$W = bab_Widgets();
	
		return $W->LabelledWidget(
				absences_translate('Approver comment'),
				$W->TextEdit()->setColumns(60)->setLines(5),
				__FUNCTION__
		);
	}
	
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				
				$W->SubmitButton()->setName('confirm')->setLabel(absences_translate('Confirm')),
				$W->SubmitButton()->setName('refuse')->setLabel(absences_translate('Refuse'))
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}
}