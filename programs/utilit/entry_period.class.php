<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/entry.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

/**
 * Entry planned period
 * this is a working period saved when the entry has been created
 * the planned period remain after a workschedule modification
 * 
 * @property int	$id_entry
 * @property string $date_begin
 * @property string $date_end
 * 
 */
class absences_EntryPeriod extends absences_Record 
{

	/**
	 * 
	 * @var absences_Entry
	 */
	private $entry;
	
	
	
	/**
	 * @return absences_EntryPeriod
	 */
	public static function getById($id_elem)
	{
		$entryPeriod = new absences_EntryPeriod;
		$entryPeriod->id = $id_elem;
	
		return $entryPeriod;
	}
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load entry period, missing entry period id');
			}
	
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_entries_periods WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	

	
	
	/**
	 *
	 * @param absences_Entry $entry
	 * @return absences_EntryPeriod
	 */
	public function setEntry(absences_Entry $entry)
	{
		$this->entry = $entry;
		return $this;
	}
	
	
	/**
	 * @return absences_Entry
	 */
	public function getEntry()
	{
		if (!isset($this->entry))
		{
			$row = $this->getRow();
			$this->entry = absences_Entry::getById($row['id_entry']);
		}
	
		return $this->entry;
	}
	
	
	/**
	 * Check validity before saving an element
	 * @return bool
	 */
	public function checkValidity()
	{

		if ($this->date_end <= $this->date_begin)
		{
			throw new UnexpectedValueException('date_begin must be lower than date_end');
		}
		
		return true;
	}
	
	

	
	/**
	 * Save element (insert or update or delete)
	 */
	public function save()
	{
		global $babDB;
		
		if (isset($this->id))
		{

			$babDB->db_query("
				UPDATE absences_entries_periods 
				SET 
			        date_begin=".$babDB->quote($this->date_begin).",
			        date_end=".$babDB->quote($this->date_end)." 
				WHERE 
					id=".$babDB->quote($this->id)
			);

			
		} else {
			
			if (isset($this->id_entry))
			{
				$id_entry = $this->id_entry;
			} else {
				$entry = $this->getEntry();
				$id_entry = $entry->id;
			}
			
			$babDB->db_query("
				INSERT INTO absences_entries_periods 
					(id_entry, date_begin, date_end)
				VALUES 
				(
					" .$babDB->quote($id_entry). ",
			        " .$babDB->quote($this->date_begin). ",
			        " .$babDB->quote($this->date_end). "
				)
			");
			
			$this->id = $babDB->db_insert_id();
		}
	}
	
	
	/**
	 * get a period on boundaries
	 * 
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 * 
	 * @return array
	 */
	private function getPeriod($begin = null, $end = null)
	{
	    if (!isset($begin)) {
	        $begin = $this->date_begin;
	    } elseif ($begin < $this->date_begin) {
	        $begin = $this->date_begin;
	    }
	     
	    if (!isset($end)) {
	        $end = $this->date_end;
	    } elseif ($end > $this->date_end) {
	        $end = $this->date_end;
	    }
	     
	    return array($begin, $end);
	}
	
	
	/**
	 * Test if the restrictive boundaries exclude the totality of period
	 * 
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 * 
	 * @return bool
	 */
	private function isOutOfBounds($begin = null, $end = null)
	{
	    if (!isset($begin) && !isset($end)) {
	        return false;
	    }
	    
	    if (isset($begin) && $begin >= $this->date_end) {
	        return true;
	    }
	    
	    if (isset($end) && $end <= $this->date_begin) {
	        return true;
	    }
	    
	    return false;
	}
	
	
	/**
	 * Get duration in days for the working period
	 * 
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 * 
	 * @return float
	 */
	public function getDurationDays($begin = null, $end = null)
	{
	    if ($this->isOutOfBounds($begin, $end)) {
	        return 0;
	    }
	    
	    list($begin, $end) = $this->getPeriod($begin, $end);
	    $dtbegin = BAB_DateTime::fromIsoDateTime($begin);
	    $dtend = BAB_DateTime::fromIsoDateTime($end);
	    
	    $hb = sprintf('%02d:%02d', $dtbegin->getHour(), $dtbegin->getMinute());
	    $he = sprintf('%02d:%02d', $dtend->getHour(), $dtend->getMinute());
	    
	    if ($hb === $he) {
	        return 0;
	    }

	    if ($he <= '12:00') {
	        return $this->getMorningDays(); // 0.5 or 0
	    }
	    
	    if ($hb >= '12:00') {
	        return $this->getAfternoonDays(); // 0.5 or 0
	    }
	    
	    if ($hb <= '12:00' && $he >= '12:00') {
	        $duration = $this->getMorningDays();
	        $duration += $this->getAfternoonDays();
	        return $duration;
	    }
	}
	
	/**
	 * Get or set the cache for half day
	 * @param string $period  am|pm
	 * @return string the entry_period dates
	 */
	private function halfDayCache($period)
	{
	    $date = substr($this->date_begin, 0, 10);
	    $entry = $this->getEntry();
	    

	    if (!isset($entry->_getDurationDays_halfDays[$date])) {
	        $entry->_getDurationDays_halfDays[$date] = array(
	            'am' => null,
	            'pm' => null
	        );
	    }
	    
	    if (isset($entry->_getDurationDays_halfDays[$date][$period])) {
	        return $entry->_getDurationDays_halfDays[$date][$period];
	    }
	    
	    $entry->_getDurationDays_halfDays[$date][$period] = $this->date_begin.'/'.$this->date_end;
	     
	    return $entry->_getDurationDays_halfDays[$date][$period];
	}
	
	
	/**
	 * Get number of days for the morning of the day
	 * if more than one period exists in the morning, the half day duration will be positive only for the first period
	 * @return float
	 */
	public function getMorningDays()
	{
	    $period = $this->halfDayCache('am');
	    
	    if ($this->date_begin.'/'.$this->date_end == $period) {
	        // this period is the first for the morning, it is accounted
	        return 0.5;
	    }
	    
	    return 0;
	}
	
	
	/**
	 * Get number of days for the afternoon of the day
	 * if more than one period exists in the morning, the half day duration will be positive only for the first period
	 * @return float
	 */
	public function getAfternoonDays()
	{
	    $period = $this->halfDayCache('pm');
	     
	    if ($this->date_begin.'/'.$this->date_end == $period) {
	        // this period is the first for the afternoon, it is accounted
	        return 0.5;
	    }
	     
	    return 0;
	}
	
	
	
	
	/**
	 * Get duration in hours for the working period
	 * 
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 * 
	 * @return float
	 */
	public function getDurationHours($begin = null, $end = null)
	{
	    if ($this->isOutOfBounds($begin, $end)) {
	        return 0;
	    }
	    
	    list($begin, $end) = $this->getPeriod($begin, $end);
	    $dtbegin = BAB_DateTime::fromIsoDateTime($begin);
	    $dtend = BAB_DateTime::fromIsoDateTime($end);
	    
	    return (($dtend->getTimeStamp() - $dtbegin->getTimeStamp()) / 3600);
	}
}