<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/request.class.php';



/**
 * CET deposit request
 * 
 * @property	int 	$id_user
 * @property 	int		$id_agent_right_cet
 * @property 	int		$id_agent_right_source
 * @property 	float	$quantity
 * @property 	int		$idfai
 * @property 	string	$comment
 * @property 	string	$createdOn
 * @property 	string	$modifiedOn
 * @property 	string	$status
 * @property 	int		$id_approver
 * @property	int		$archived
 * 
 */
class absences_CetDepositRequest extends absences_Request
{
	/**
	 * 
	 * @var absences_AgentRight
	 */
	private $agentRightSource;
	
	/**
	 *
	 * @var absences_AgentRight
	 */
	private $agentRightCet;
	
	
	/**
	 * @param int $id
	 * @return absences_CetDepositRequest
	 */
	public static function getById($id)
	{
		$request = new absences_CetDepositRequest();
		$request->id = $id;
	
		return $request;
	}

	
	
	/**
	 *
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			$query = 'SELECT * FROM absences_cet_deposit_request WHERE ';
	
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}
	
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	
	
	
	
	/**
	 * @return absences_AgentRight
	 */
	public function getAgentRightSource()
	{
		if (!isset($this->agentRightSource))
		{
			require_once dirname(__FILE__).'/agent_right.class.php';
			$this->agentRightSource = absences_AgentRight::getById($this->id_agent_right_source);
		}
		
		return $this->agentRightSource;
	}
	
	
	
	/**
	 * @return absences_AgentRight
	 */
	public function getAgentRightCet()
	{
		if (!isset($this->agentRightCet))
		{
			require_once dirname(__FILE__).'/agent_right.class.php';
			$this->agentRightCet = absences_AgentRight::getById($this->id_agent_right_cet);
		}
	
		return $this->agentRightCet;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getRequestType()
	 * 
	 * @return string
	 */
	public function getRequestType()
	{
		return absences_translate('Time saving account deposit');
	}
	
	
	public function save()
	{
		global $babDB;
		
		if (!isset($this->createdOn)) {
		    $this->createdOn = date('Y-m-d H:i:s');
		}
		
		if (!isset($this->modifiedOn)) {
		    $this->modifiedOn = date('Y-m-d H:i:s');
		}
		
		
		if (isset($this->id) && !empty($this->id))
		{
		    $query = '
				UPDATE absences_cet_deposit_request	 
					SET 
						id_agent_right_source='.$babDB->quote($this->id_agent_right_source).',
						quantity='.$babDB->quote($this->quantity).',
						modifiedOn='.$babDB->quote($this->modifiedOn).',
						idfai='.$babDB->quote($this->idfai).',
						status='.$babDB->quote($this->status).',
						quantity='.$babDB->quote($this->quantity).',
						comment='.$babDB->quote($this->comment).',
						comment2='.$babDB->quote($this->comment2).',
						id_approver='.$babDB->quote($this->id_approver).' 
			';
		    
		    if (isset($this->todelete)) {
		        $query .= ', todelete='.$babDB->quote($this->todelete);
		    }
		    
		    $query .= ' WHERE 
					id='.$babDB->quote($this->id).' ';
		    
			$babDB->db_query($query);
		} else {
		
		
			$babDB->db_query('INSERT INTO 
				absences_cet_deposit_request 
					(
						id_user, 
						id_agent_right_cet, 
						id_agent_right_source, 
						quantity,
						comment,
						createdOn,
						modifiedOn,
						idfai,
						status
					) 
				VALUES 
					(
						'.$babDB->quote($this->id_user).',
						'.$babDB->quote($this->id_agent_right_cet).',
						'.$babDB->quote($this->id_agent_right_source).',
						'.$babDB->quote($this->quantity).',
						'.$babDB->quote($this->comment).',
						'.$babDB->quote($this->createdOn).',
						'.$babDB->quote($this->modifiedOn).',
						'.$babDB->quote($this->idfai).',
						'.$babDB->quote($this->status).'
					)
			');
			
			$this->id = $babDB->db_insert_id();
		
		}
	}
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getApprobationId()
	 * 
	 * @return int
	 */
	public function getApprobationId()
	{
		if ($agent = $this->getAgent())
		{
			return $agent->getCetApprobationId();
		}
	
		return null;
	}
	
	/**
	 * Process specific code when the request is rejected
	 *
	 */
	protected function onReject()
	{
	
	}
	
	/**
	 * Process specific code when the request is confirmed
	 *
	 */
	public function onConfirm()
	{
		// the quantity in source right is removed dynamically
		
		$source = $this->getAgentRightSource()->getRight();
		$cet = $this->getAgentRightCet()->getRight();
		
		
		
		// add quantity to the CET
		
		$agentCet = $this->getAgentRightCet();
		
		if (!$agentCet->getRow())
		{
			bab_debug('No agentRight found, agent deleted?');
			return;
		}
		
		
		$agentCet->quantity += $this->quantity;
		$agentCet->save();
		
		$qte_text = absences_quantity($this->quantity, $source->quantity_unit);
		
		// ajout d'un message dans l'historique de chaque droit et dans l'historique de l'utilisateur
		
		$source->addMovement(sprintf(absences_translate('%s has saved %s to his time saving account'), $this->getAgent()->getName(), $qte_text));
		$cet->addMovement(sprintf(absences_translate('The time saving account of %s has received %s'), $this->getAgent()->getName(), $qte_text));
		$this->addMovement(sprintf(absences_translate('%s of right "%s" saved to "%s"'), $qte_text, $source->description, $cet->description));
	}
	
	
	public function getTitle()
	{
	    if (isset($this->todelete) && $this->todelete) {
	        return absences_translate('time saving account deposit request to delete');
	    }
	    
		return absences_translate('time saving account deposit request');
	}
	
	
	public function getNotifyFields()
	{
		$source = $this->getAgentRightSource()->getRight();
		
		if (!$source->getRow())
		{
			return array();
		}
		
		return array(
				absences_translate('From right')	=> $source->description,
				absences_translate('Quantity') 		=> absences_quantity($this->quantity, $source->quantity_unit)
		);
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getYear()
	 * 
	 * @return int
	 */
	public function getYear()
	{
		$year = (int) substr($this->createdOn, 0, 4);
	
		if (0 === $year)
		{
			return null;
		}
	
		return $year;
	}
	
	
	public function getArchiveYear()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$year = (int) substr($this->createdOn, 0, 4);
		
		if (0 === $year)
		{
			return null;
		}
		
		$day = absences_getVacationOption('archivage_day');
		$month = absences_getVacationOption('archivage_month');
		
		$currentYear = new BAB_DateTime($year, $month, $day);
		if($this->createdOn < $currentYear->getIsoDate()){
			$year = $year-1;
		}
		
		return $year;
	}
	
	
	
	public function archive()
	{
		global $babDB;
	
		$babDB->db_query("
			UPDATE absences_cet_deposit_request 
			SET 
				archived=".$babDB->quote(1)."
			WHERE 
				id=".$babDB->quote($this->id)."
		");
	}
	
	
	
	public function setNotified()
	{
		global $babDB;
	
		$babDB->db_query("
			UPDATE absences_cet_deposit_request
			SET 
				appr_notified=".$babDB->quote(1)."
			WHERE 
				id=".$babDB->quote($this->id)."
		");
	}
	
	
	public function getManagerEditUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		return $addon->getUrl().'vacadmcet&idx=edit&id='.$this->id;
	}
	
	public function getManagerDeleteUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		return $addon->getUrl().'vacadmcet&idx=delete&id='.$this->id;
	}
	
	
	
	/**
	 * @return string
	 */
	public function getEditUrl($rfrom, $ide = null)
	{
		$url = absences_addon()->getUrl()."vacuser&idx=cet&id=".$this->id;
	
		if (isset($rfrom))
		{
			$url .= '&rfrom='.$rfrom;
		}
	
		if (isset($ide))
		{
			$url .= '&ide='.$ide;
		}
	
		return $url;
	}
	
	
	public function getManagerFrame()
	{
		$W = bab_Widgets();

		return $W->Link($W->Icon(absences_translate('Deposit on the time saving account'), Func_Icons::MIMETYPES_UNKNOWN), absences_addon()->getUrl()."vacadmcet&idx=edit&id=".$this->id );
	}
	
	
	
	/**
	 * Get request card frame to display in requests list
	 * @param bool 			$display_username	Affiche le nom du demandeur dans la card frame ou non, utile pour les listes contenant plusieurs demandeurs possibles
	 * @param int			$rfrom				si les demandes de la liste sont modifiee par un gestionnaire ou gestionnaire delegue
 	 * @param int			$ide				id entite de l'organigramme >0 si gestionnaire delegue seulement
	 * @return Widget_Frame
	 */
	public function getCardFrame($display_username, $rfrom, $ide)
	{
		bab_functionality::includeOriginal('Icons');
		$W = bab_Widgets();
		
		
		$layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em')->addClass('widget-full-width');
		$frame = $W->Frame(null, $layout);
		$frame->addClass(Func_Icons::ICON_LEFT_16);
		
		if (!$this->getRow())
		{
			return $frame;
		}
	
		$layout->addItem($col1 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$layout->addItem($col2 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$layout->addItem($col3 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		
		$col1->addItem($W->Link($W->Icon(absences_translate('Deposit on the time saving account'), Func_Icons::MIMETYPES_UNKNOWN), absences_addon()->getUrl()."vacuser&idx=view_cet_deposit&id=".$this->id ));
		$col1->setCanvasOptions($col1->Options()->width(25,'em'));
		$col2->setCanvasOptions($col1->Options()->width(25,'em'));
		$layout->addItem($col3 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$col2->setSizePolicy(Widget_SizePolicy::MAXIMUM);
		
		$col1->addItem($this->getStatusIcon());
		
		
		
		if ($display_username)
		{
			$col2->addItem($this->labelledValue(absences_translate('Owner'), $this->getUserName()));
		}
		
		$source = $this->getAgentRightSource()->getRight();
		
		if ($source && $source->getRow())
		{
			$col2->addItem($this->labelledValue(absences_translate('From right'), $source->description));
			$col2->addItem($this->labelledValue(absences_translate('Quantity'), absences_quantity($this->quantity, $source->quantity_unit)));
		} else {
			$col2->addItem($this->labelledValue(absences_translate('Quantity'), $this->quantity));
		}
		
		
		if ($this->canModify())
		{
			$col3->addItem($W->Link($W->Icon(absences_translate('Modify'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->getEditUrl($rfrom, $ide)));
		}
		
		if ($this->canDelete())
		{
			$urldelete = absences_addon()->getUrl()."vacuser&idx=delete&id_cetdeposit=".$this->id;
			$col3->addItem($W->Link($W->Icon(absences_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $urldelete));
		}

		$frame->setTitle(sprintf(absences_translate('Created the %s'), bab_shortDate(bab_mktime($this->createdOn()))));
	
		return $frame;
	}
	
	
	
	/**
	 * Delete request
	 */
	public function delete()
	{

		parent::delete();
		
		global $babDB;
		$babDB->db_query("delete from absences_cet_deposit_request where id=".$babDB->quote($this->id));
		
		$this->addMovement(
		    absences_translate('The time saving account deposit request has been deleted')
		);
	}
	
	
	/**
	 * Notify request owner about the approval or reject of the request
	 */
	public function notifyOwner()
	{
		require_once dirname(__FILE__).'/request.notify.php';
	
		switch($this->status)
		{
			case 'N':
				$subject = absences_translate("Your time saving account deposit request has been refused");
				break;
	
			case 'Y':
				$subject = absences_translate("Your time saving account deposit request has been accepted");
				break;
				
			default:
				return;
		}
		
		absences_notifyRequestAuthor(array($this), $subject, $subject, $this->id_user);
	}
}







/**
 * Vacation requests
 *
 */
class absences_CetDepositRequestIterator extends absences_Iterator
{

	/**
	 *
	 * @var array
	 */
	public $users;
	
	
	/**
	 * Filter by organization
	 * @var int
	 */
	public $organization;
	
	
	/**
	 * Filtrer les demandes necessitant ou pas un email aux approbateurs
	 * @var int
	 */
	public $appr_notified;
	
	
	/**
	 * Filtrer les demandes avec unes instance d'approbation
	 * @var bool
	 */
	public $idfai_set;
	
	/**
	 * 
	 * @var string
	 */
	public $status;
	
	/**
	 * Search all request created before this datetime
	 * @var string
	 */
	public $createdOn;
	
	
	/**
	 * Search all request modified before this date
	 * @var string
	 */
	public $modifiedOn;
	
	
	/**
	 * Filtrer les demandes par annee
	 * @var int
	 */
	public $year;
	
	
	/**
	 * Filtrer les demandes par statut d'archivage
	 * @var int
	 */
	public $archived = 0;
	
	
	/**
	 * @var int
	 */
	public $id_agent_right_source;


	public function getObject($data)
	{

		$entry = new absences_CetDepositRequest;
		$entry->setRow($data);
		return $entry;

	}



	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = 'SELECT r.*
			FROM 
				absences_cet_deposit_request r 
			         LEFT JOIN absences_personnel p ON p.id_user=r.id_user 
			';


			$where = array();

			if (isset($this->users))
			{
				$where[] = 'r.id_user IN('.$babDB->quote($this->users).')';
			}
			
			if (isset($this->organization))
			{
			    $where[] = 'p.id_organization='.$babDB->quote($this->organization);
			}
			
			if (isset($this->appr_notified))
			{
				$where[] = 'r.appr_notified='.$babDB->quote($this->appr_notified);
			}
			
			if (isset($this->idfai_set) && $this->idfai_set)
			{
				$where[] = 'r.idfai>'.$babDB->quote(0);
			}

			if (isset($this->status))
			{
				$where[] = 'r.status='.$babDB->quote($this->status);
			}
			
			if (isset($this->createdOn))
			{
				$where[] = 'r.createdOn<='.$babDB->quote($this->createdOn);
			}
			
			if (isset($this->modifiedOn))
			{
				$where[] = 'r.modifiedOn<='.$babDB->quote($this->modifiedOn);
			}
			
			if (isset($this->year))
			{
				$where[] = 'YEAR(r.createdOn)='.$babDB->quote($this->year);
			}
			
			if (isset($this->startTo) && $this->startTo != '0000-00-00')
			{
				$where[] = 'r.createdOn <='.$babDB->quote($this->startTo);
			}
			
			if (isset($this->startFrom) && $this->startFrom != '0000-00-00')
			{
				$where[] = 'r.createdOn >='.$babDB->quote($this->startFrom);
			}
			
			if (isset($this->archived))
			{
				$where[] = 'r.archived='.$babDB->quote($this->archived);
			}
			
			if (isset($this->id_agent_right_source)) {
			    $where[] = 'r.id_agent_right_source='.$babDB->quote($this->id_agent_right_source);
			}
			
			if ($where)
			{
				$req .= ' WHERE '.implode(' AND ', $where);
			}
			
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
}
