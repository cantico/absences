<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/type.class.php';
require_once dirname(__FILE__).'/rgroup.class.php';

/**
 * Abscence Right
 *
 * 
 * 
 * @property int 		$id_creditor
 * @property int 		$kind
 * @property string 	$date_entry
 * @property string 	$createdOn
 * @property string 	$date_begin
 * @property string 	$date_end
 * @property float 		$quantity
 * @property string 	$quantity_unit
 * @property float 		$quantity_inc_month
 * @property float 		$quantity_inc_max
 * @property string		$quantity_inc_last
 * @property int 		$id_type
 * @property string 	$description
 * @property string 	$active
 * @property string 	$cbalance
 * @property string 	$date_begin_valid
 * @property string 	$date_end_valid
 * @property string 	$date_end_fixed
 * @property string 	$date_begin_fixed
 * @property int        $hide_empty
 * @property int 		$no_distribution
 * @property int 		$id_rgroup
 * @property string 	$earlier
 * @property int 		$earlier_begin_valid
 * @property int 		$earlier_end_valid
 * @property string 	$later
 * @property int 		$later_begin_valid
 * @property int 		$later_end_valid
 * @property int		$delay_before
 * @property int		$use_in_cet
 * @property float		$cet_quantity
 * @property int		$id_report_type
 * @property string 	$date_end_report
 * @property string		$description_report
 * @property int		$id_reported_from
 * @property int		$sync_status
 * @property string		$sync_update
 * @property string		$uuid
 * @property int		$archived
 * @property int		$sortkey
 * @property int		$require_approval
 * 
 * @property int		$quantity_alert_days
 * @property int		$quantity_alert_types
 * @property string		$quantity_alert_begin
 * @property string		$quantity_alert_end
 * 
 * @property int		$dynconf_types
 * @property string		$dynconf_begin
 * @property string		$dynconf_end
 * 
 * @property string     $renewal_uid
 * @property int        $renewal_parent     Right ID of the right in the previous period
 * 
 */
class absences_Right extends absences_Record implements absences_RightSort 
{
	/**
	 * Genre du droit default
	 * @var int
	 */
	const REGULAR 	= 1;
	
	/**
	 * Genre du droit a date fixe
	 * @var int
	 */
	const FIXED		= 2;
	
	/**
	 * Genre du droit compte epargne temps
	 * @var int
	 */
	const CET		= 4;
	
	/**
	 * Ajout periodique de solde sur le droit
	 * @var int
	 */
	const INCREMENT = 8;

	
	/**
	 * Droit a recuperation (cree apres une approbation de declaration de jours travailles donnant droit a recuperation)
	 * @var int
	 */
	const RECOVERY	= 16;
	
	/**
	 * Droit de report (cree automatiquement)
	 * @var int
	 */
	const REPORT	= 32;
	
	
	
	
	
	const SYNC_CLIENT 		= 1;
	const SYNC_CLIENT_END 	= 2;
	const SYNC_CLIENT_ERROR	= 3;
	
	const SYNC_SERVER 		= 8;
	
	
	
	
	/**
	 * 
	 * @var absences_Type
	 */
	private $type;
	
	
	/**
	 * @var absences_Rgroup
	 */
	private $rgroup;
	
	
	/**
	 * 
	 * @var absences_RightRule
	 */
	private $right_rule;
	
	/**
	 * 
	 * @var absences_RightCet
	 */
	private $right_cet;
	
	
	/**
	 * List of unsaved inperiod rules
	 * @var array
	 */
	private $inperiod = array();
	
	/**
	 *
	 * @var absences_Right
	 */
	private $reported_from;
	
	/**
	 *
	 * @var absences_Right
	 */
	private $report;
	
	
	/**
	 * 
	 * @var string
	 */
	private $_uuid;
	
	
	public function __construct($id)
	{
		$this->id = $id;
	}
	
	
	/**
	 * 
	 * @param string $uuid
	 * @return absences_Right
	 */
	public static function getByUuid($uuid)
	{
		$right = new absences_Right(null);
		$right->_uuid = $uuid;
		
		return $right;
	}
	
	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			
			$query = '
				SELECT 
					r.*
				FROM absences_rights r	
				WHERE ';
			
			if (isset($this->id))
			{
				$query .= 'r.id='.$babDB->quote($this->id);
			} elseif (isset($this->_uuid))
			{
				$query .= 'r.uuid='.$babDB->quote($this->_uuid);
			} else {
				return false;
			}
			
			$res = $babDB->db_query($query);
			
			$this->setRow($babDB->db_fetch_assoc($res));
		}
		
		return $this->row;
	}

	
	/**
	 * La nature du droit
	 * @return int
	 */
	public function getKind()
	{
		return (int) $this->kind;
	}
	
	/**
	 * @return string
	 */
	public function getKindLabel()
	{
		$arr = absences_kinds();
		$kind = $this->getKind();
		
		if (!isset($arr[$kind])) {
		    return '';
		}
		
		return $arr[$kind];
	}
	
	/**
	 * @return int
	 */
	public function getSyncStatus()
	{
		return (int) $this->sync_status;
	}
	
	/**
	 * @return string
	 */
	public function getSyncStatusLabel()
	{
		$arr = absences_syncStatus();
		return $arr[$this->getSyncStatus()];
	}
	
	
	/**
	 *
	 * @param absences_Type $type
	 * @return absences_Right
	 */
	public function setType(absences_Type $type)
	{
		$this->type = $type;
		return $this;
	}
	
	
	/**
	 * @return absences_Type
	 */
	public function getType()
	{
		if (!isset($this->type))
		{
			$row = $this->getRow();
			$this->type = new absences_Type($row['id_type']);
		}
	
		return $this->type;
	}
	
	
	
	/**
	 *
	 * @param absences_Rgroup $rgroup
	 * @return absences_Right
	 */
	public function setRgroup(absences_Rgroup $rgroup)
	{
		$this->rgroup = $rgroup;
		return $this;
	}
	
	
	/**
	 * @return absences_Rgroup
	 */
	public function getRgroup()
	{
		if (!isset($this->rgroup))
		{
			$row = $this->getRow();
			if (empty($row['id_rgroup']))
			{
				return null;
			}
			$this->rgroup = new absences_Rgroup($row['id_rgroup']);
		}
	
		return $this->rgroup;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getRgroupLabel()
	{
		$rgroup = $this->getRgroup();
		
		if (null === $rgroup || !$rgroup->getRow())
		{
			return null;
		}
		
		return $rgroup->name;
	}
	
	
	
	/**
	 *
	 * @return int
	 */
	public function getRgroupSortkey()
	{
		$rgroup = $this->getRgroup();
	
		if (null === $rgroup || !$rgroup->getRow())
		{
			return null;
		}
	
		return $rgroup->sortkey;
	}
	
	
	/**
	 * @return string
	 */
	public function getUnitLabel()
	{
		switch($this->quantity_unit)
		{
			case 'D':
				return absences_translate('day(s)');
	
			case 'H':
				return absences_translate('hour(s)');
		}
	
		return '';
	}
	
	/**
	 * Start date year
	 * @return int
	 */
	public function getYear()
	{
		$year = (int) substr($this->date_begin, 0, 4);
		
		if (0 === $year)
		{
			return null;
		}
		
		return $year;
	}
	
	
	/**
	 * 
	 * @param absences_RightRule $RightRule
	 */
	public function setRightRule(absences_RightRule $RightRule)
	{
		$this->rightrule = $RightRule;
		return $this;
	}
	
	/**
	 * @return absences_RightRule
	 */
	public function getRightRule()
	{
		if (!isset($this->right_rule))
		{
			require_once dirname(__FILE__).'/right_rule.class.php';
			$this->right_rule = absences_RightRule::getFromRight($this->id);	
		}
		return $this->right_rule;
	}
	
	
	
	
	/**
	 *
	 * @param absences_RightCet $RightCet
	 */
	public function setRightCet(absences_RightCet $RightCet)
	{
		$this->right_cet = $RightCet;
		return $this;
	}
	
	/**
	 * @return absences_RightCet
	 */
	public function getRightCet()
	{
		if (!isset($this->right_cet))
		{
			require_once dirname(__FILE__).'/right_cet.class.php';
			$this->right_cet = absences_RightCet::getFromRight($this->id);
		}
		return $this->right_cet;
	}
	
	
	
	/**
	 * 
	 * @param absences_RightInPeriod $inperiod
	 */
	public function addInPeriod(absences_RightInPeriod $inperiod)
	{
		$this->inperiod[] = $inperiod;
		$inperiod->setRight($this);
		return $this;
	}
	
	
	/**
	 * @return multitype:absences_RightInPeriod
	 */
	public function getInperiodRules()
	{
		return $this->inperiod;
	}
	
	
	/**
	 * Get the source right of report or false if the right is not a report
	 * @return absences_Right | false
	 */
	public function getReportedFrom()
	{
		if (!isset($this->reported_from))
		{
			if (self::REPORT !== $this->getKind() || empty($this->id_reported_from))
			{
				$this->reported_from = false;
			} else {
			
				$this->reported_from = new absences_Right($this->id_reported_from);
			}
		}
		
		return $this->reported_from;
	}
	
	
	/**
	 * Get the report right if exists
	 * @return absences_Right | false
	 */
	public function getReport()
	{
		if (!isset($this->report))
		{
			global $babDB;
			
			$res = $babDB->db_query('SELECT * FROM absences_rights WHERE id_reported_from='.$babDB->quote($this->id));
			if (0 === $babDB->db_num_rows($res))
			{
				$this->report = false;
			} else {
				
				$row = $babDB->db_fetch_assoc($res);
				$right = new absences_Right($row['id']);
				$right->setRow($row);
				
				$this->report = $right;
			}
		}
		
		return $this->report;
	}
	
	
	/**
	 * Get the report right or create it if not exists
	 * @return absences_Right | null
	 */
	public function getOrCreateReport()
	{
		$right = $this->getReport();
		
		if (false !== $right)
		{
			return $right;
		}
		
		
		if (empty($this->date_end_report) || '0000-00-00'===$this->date_end_report)
		{
			return null;
		}
		
		
		$description = empty($this->description_report) ? $this->description : $this->description_report;
		$id_type = empty($this->id_report_type) ? $this->id_type : $this->id_report_type;
		
		global $babDB;
		
		$babDB->db_query('INSERT INTO absences_rights (
				kind, 
				description, 
		        createdOn,
				date_entry, 
				date_begin, 
				date_end,
				date_begin_valid,
				date_end_valid,
				quantity_unit,
				id_type,
				cbalance,
				use_in_cet,
				id_reported_from
			) 
				VALUES 
			(
				'.$babDB->quote(self::REPORT).',
				'.$babDB->quote($description).',
		        NOW(),
				NOW(),
				'.$babDB->quote($this->date_begin).',
				'.$babDB->quote($this->date_end_report).',
				'.$babDB->quote($this->date_begin_valid).',
				'.$babDB->quote($this->date_end_report).',
				'.$babDB->quote($this->quantity_unit).',
				'.$babDB->quote($id_type).',
				'.$babDB->quote('N').',
				'.$babDB->quote('0').',
				'.$babDB->quote($this->id).'
			)');
		
		
		$id_report = $babDB->db_insert_id();
		
		$this->report = new absences_Right($id_report);
		
		return $this->report;
	}
	
	
	
	/**
	 * Tester si le droit est disponible dans le mois en cours (en fonction de la periode de conges demandee)
	 * si cette methode renvoi true, il n'y a pas de fin de validite dans le mois en cours sauf si validoperlap est actif
	 *
	 * si on ce base sur cette methode pour determiner si un droit doit etre incremente dans le mois
	 *  - le droit sera incremente si pas de fin de validite dans le mois
	 *  - le droit sera incremente si le chevauchement est active et que la fin de validite est dans le mois
	 *
	 *
	 * @return bool
	 */
	public function isAccessibleOnMonth()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$begin = new BAB_DateTime(date('Y'), date('n'), 1);
		$end = new BAB_DateTime(date('Y'), date('n'), date('t'), 23, 59, 59);
		
		$rightRule = $this->getRightRule();
	
		return $rightRule->isAccessibleOnPeriod($begin->getTimeStamp(), $end->getTimeStamp());
	}
	
	
	
	protected function saveQuantityIncMonth($quantity)
	{
	    require_once dirname(__FILE__).'/increment_right.class.php';
	
	    $increment = new absences_IncrementRight();
	    $increment->id_right = $this->id;
	    $increment->quantity = $quantity;
	    $increment->createdOn = date('Y-m-d H:i:s');
	    $increment->monthkey = date('Ym');
	
	    $increment->saveOrUpdate();
	}
	
	
	
	
	/**
	 * Increment quantity for the month if not allready done
	 * return true if quantity has been modified
	 *
	 * @param LibTimer_eventHourly $event optional event if the action is done via a background task
	 *
	 * @return bool
	 */
	public function monthlyQuantityUpdate(LibTimer_eventHourly $event = null)
	{
	    if ($this->getKind() !== absences_Right::INCREMENT || $this->quantity_inc_month<='0.00')
	    {
	        return false;
	    }

	    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	    $limit = new BAB_DateTime(date('Y'), date('n'), 1);

	    if ($this->quantity_inc_last >= $limit->getIsoDateTime())
	    {
	        return false;
	    }

	    $quantity = (float) $this->quantity;
	    $quantity += $this->getIncrementQuantity();
	    
	    $initial_quantity = $quantity;
	    
	    if ($this->quantity_inc_last !== '0000-00-00 00:00:00' && $this->isAccessibleOnMonth())
	    {
	        // Do not increment the first month
	        // Do not increment if the full month is not totally available
	        
	        $quantity_inc_month = $this->getQuantityIncMonth($quantity);
	        
	        if (0 !== (int) round(100 * $quantity_inc_month)) {
	        
    	        $quantity += $quantity_inc_month;
    	        
    	        $this->saveQuantityIncMonth($quantity_inc_month);
    
    	        $message = sprintf(
    	            absences_translate('The quantity of right "%s" has been modified from %s to %s by the monthly update'), 
                    $this->description,
                    $initial_quantity,
                    $quantity
    	        );
    	        
    	        $this->addMovement($message, '', 0);
    	        
    	        if (isset($event)) {
    	            $event->log('absences', $message);
    	        }
	        }
	    }
	    	
	    global $babDB;
	    	
	    $babDB->db_query('UPDATE absences_rights SET 
				quantity_inc_last='.$babDB->quote($limit->getIsoDateTime()).'
			WHERE id='.$babDB->quote($this->id));
	    
	    if ($this->quantity_inc_last !== '0000-00-00 00:00:00') {
    	    // update the modified quantites but not the first month
    	    // agentRight have their own isAccessibleOnMonth method because the period can be different
    	    
    	    $modified = $this->getAgentRightIterator();
    	    $modified->modified_quantity = true;
    	    
    	    foreach ($modified as $agentRight)
    	    {
    	        /*@var $agentRight absences_AgentRight */
    	        $agentRight->monthlyQuantityUpdate($event);
    	    }
    	    
    	    
    	    // UPDATE the current instance
    	    $this->quantity_inc_last = $limit->getIsoDateTime();
    	    
    	    return true;
	    }
	    
	    // UPDATE the current instance
	    $this->quantity_inc_last = $limit->getIsoDateTime();
	    return false;
	}
	
	
	
	
	/**
	 * Tester si le droit est accessible en fonction de la periode de validite du droit
	 * @see absences_AgentRight::isAccessibleByValidityPeriod()
	 * @return bool
	 */
	public function isAccessibleByValidityPeriod()
	{
		$access= true;
		
		if( $this->date_begin_valid != '0000-00-00' && (bab_mktime($this->date_begin_valid." 00:00:00") > mktime())){
			$access= false;
		}
		
		if( $this->date_end_valid != '0000-00-00' && (bab_mktime($this->date_end_valid." 23:59:59") < mktime())){
			$access= false;
		}
		
		return $access;
	}
	
	/**
	 * Test if the vacation right is visible to the end user by testing properties of fixed vacation right
	 * if the right is not a fixed vacation right, this method return true
	 * 
	 * @return bool
	 */
	public function isAccessibleIfFixed()
	{
		if ($this->kind != self::FIXED)
		{
			return true;
		}
		
		// dont't display vacations with fixed dates that are gone
		if( $this->date_end_fixed != '0000-00-00 00:00:00' && (bab_mktime($this->date_end_fixed) < mktime())){
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * If the right is displayed as available in the manager list
	 * Test if the right is accessible for the user to display information in exported file
	 * pour l'export de l'agent et pour l'export des droits
	 * 
	 * @return bool
	 */
	public function isAvailable()
	{
		return ('Y' == $this->active && $this->isAccessibleByValidityPeriod() && $this->isAccessibleIfFixed());
	}
	
	
	public function getStatus()
	{
		if ($this->archived)
		{
			return absences_translate('Archived');
		}
		
		
		if ('Y' == $this->active)
		{
			return absences_translate('Active');
		}
		
		if ('N' == $this->active)
		{
			return absences_translate('Disabled for appliquant, usable by managers and delegated managers');
		}
	}
	
	
	/**
	 * Get a period description in text
	 * @param string $begin
	 * @param string $end
	 */
	private function periodText($begin, $end)
	{
		if ('0000-00-00'=== $begin && '0000-00-00' === $end) {
			return null;
		}
		
		$d1 = bab_shortDate(bab_mktime($begin), false);
		$d2 = bab_shortDate(bab_mktime($end), false);
		
		if (!$d1) {
		    $d1 = absences_translate('??/??/????');
		}
		
		if (!$d2) {
		    $d2 = absences_translate('??/??/????');
		}
		
		
		return sprintf(absences_translate('the %s and the %s'), $d1, $d2);
	}
	
	
	/**
	 * 
	 * @param string $name
	 * @return string
	 */
	private function getDirFieldLabel($name)
	{
		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
		return $fields[$name]['name'];
	}
	
	
	
	
	/**
	 * Get access conditions as a string
	 * of null if there are no access conditions
	 * @return string
	 */
	public function getAccessConditions()
	{
		global $babDB;
		$conditions = array();
		
		
		// Disponibilite en fonction de la date de saisie de la demande de conges
		if ('0000-00-00' !== $this->date_begin_valid)
		{
			$period = $this->periodText($this->date_begin_valid, $this->date_end_valid);
			$conditions[] = sprintf(absences_translate('To create a vacation request at a date beetween %s.'), $period);
		}
		
		
		$rightRules = $this->getRightRule();
		
		// Disponibilite en fonction de la periode de conges demandee
		$res = $rightRules->getInPeriodRes();
		if ($babDB->db_num_rows($res) > 0)
		{
			$s = array();
			
			while ($arr = $babDB->db_fetch_assoc($res))
			{
				$d1 = bab_shortDate(bab_mktime($arr['period_start']), false);
				$d2 = bab_shortDate(bab_mktime($arr['period_end']), false);
				
				
				switch($arr['right_inperiod'])
				{
					case 1: $subcond = sprintf(absences_translate('beetween the %s and the %s'), $d1, $d2);	break;
					case 2: $subcond = sprintf(absences_translate('before the %s or after the %s'), $d1, $d2);	break;
				}
				
				$s[] = $subcond;
			}
			
			$conditions[] = absences_translate('To apply for a vacation period').' '.implode(', ', $s).'.';
		}
		
		// Attribution du droit en fonction des jours demandes et valides

		if ($rightRules->getRow() && $rightRules->trigger_nbdays_min && $rightRules->trigger_nbdays_max)
		{
			$text = sprintf(absences_translate('To have requested at least %d days but less than %d days'), $rightRules->trigger_nbdays_min, $rightRules->trigger_nbdays_max);
			if ($rightRules->trigger_type)
			{
				$text .= ' '. sprintf(absences_translate('of type %s'), $rightRules->getType()->name);
			}
			
			$periods = array();
			$p1 = $this->periodText($rightRules->trigger_p1_begin, $rightRules->trigger_p1_end);
			$p2 = $this->periodText($rightRules->trigger_p2_begin, $rightRules->trigger_p2_end);
			
			if ($p1) {
				$periods[] = sprintf(absences_translate('beetween %s'), $p1);
			}
			
			if ($p2) {
				$periods[] = sprintf(absences_translate('beetween %s'), $p2);
			}
			$text .= ' '.implode(' '.absences_translate('or').' ', $periods).'.';
			
			$conditions[] = $text;
		}
		
		// Disponibilite en fonction d'une date anterieure
		
		if ($this->earlier)
		{
			$f = $this->getDirFieldLabel($this->earlier);
			$cond = array();
			
			if ($this->earlier_begin_valid)
			{
				$cond[] = sprintf(absences_translate('%d years later "%s"'), $this->earlier_begin_valid, $f);
			} else {
				$cond[] = sprintf(absences_translate('after "%s"'), $f);
			}
			
			if ($this->earlier_end_valid)
			{
				$cond[] = sprintf(absences_translate('in the %d years next to "%s"'), $this->earlier_end_valid, $f);
			} 
			
			$conditions[] = absences_translate('To request a period').' '.implode(' '.absences_translate('and').' ', $cond).'.';
		}
		
		
		// Disponibilite en fonction d'une date posterieure
		
		if ($this->later)
		{
			$f = $this->getDirFieldLabel($this->later);
			$cond = array();
			
			if ($this->later_begin_valid)
			{
				$cond[] = sprintf(absences_translate('in the %d years preceding "%s"'), $this->later_begin_valid, $f);
			} else {
				$cond[] = sprintf(absences_translate('after "%s"'), $f);
			}
			
			if ($this->later_end_valid)
			{
				$cond[] = sprintf(absences_translate('at least %d years before the "%s"'), $this->later_end_valid, $f);
			}
			
			$conditions[] = absences_translate('To request a period').' '.implode(' '.absences_translate('and').' ', $cond).'.';
		}
		
		if($this->delay_before)
		{
			$conditions[] = sprintf(absences_translate('To request a period at least %s days before.'), $this->delay_before);
		}
		
		
		if (0 ===count($conditions))
		{
			return null;
		}
		
		
		$text = absences_translate('The condition to access this vacation right is', 'The conditions to access this vacation right are:', count($conditions));
		
		foreach($conditions as $c)
		{
			$text .= " \n".$c;
		}
		
		
		return $text;
		
	}
	
	
	
	
	/**
	 * Agents associated to vacation right
	 * @return absences_AgentIterator
	 */
	public function getAgentIterator()
	{
		require_once dirname(__FILE__).'/agent.class.php';
		$I = new absences_AgentIterator;
		$I->setRight($this);
		
		return $I;
	}
	
	/**
	 * Collections associated to vacation right
	 * @return absences_CollectionIterator
	 */
	public function getCollectionIterator()
	{
		require_once dirname(__FILE__).'/collection.class.php';
		$I = new absences_CollectionIterator;
		$I->setRight($this);
		
		return $I;
	}
	
	
	/**
	 * @return absences_AgentRightStatIterator
	 */
	public function getAgentRightIterator()
	{
		require_once dirname(__FILE__).'/agent_right.class.php';
		$I = new absences_AgentRightStatIterator;
		$I->setRight($this);
		
		return $I;
	}
	
	
	/**
	 * determiner le nombre de d'agent avec ce droit non solde
	 * @return int
	 */
	public function getAgentUsage()
	{
		$last_available = 0;
		$last_waiting = 0;
		$last_agents = 0;
		$last_agents_iu = 0;
		
		
		foreach($this->getAgentRightIterator() as $agent_right)
		{
			/*@var $agent_right absences_AgentRight */
			
			$waiting = $agent_right->getWaitingQuantity();
			if (0 !== (int) round(100 * $waiting))
			{
				$last_agents_iu++;
			}
			
			$available = $agent_right->getAvailableQuantity();
			if (0 !== (int) round(100 * $available))
			{
				$last_agents++;
				$last_agents_iu++;
			}
			
			
			
			
			$last_waiting += $waiting;
			$last_available += $available; 
				
		}
		
		
		
		return array(
			'available' => absences_quantity($last_available, $this->quantity_unit),
			'waiting' => absences_quantity($last_waiting, $this->quantity_unit),
			'agents' => $last_agents,
			'agents_iu' => $last_agents_iu
		);
	}
	
	
	
	
	
	/**
	 *
	 * @param string $message	Generated message
	 * @param string $comment	Author comment
	 */
	public function addMovement($message, $comment = '', $id_author = null)
	{
		require_once dirname(__FILE__).'/movement.class.php';
	
		$movement = new absences_Movement();
		$movement->message = $message;
		$movement->comment = $comment;
		
		if (isset($id_author))
		{
			$movement->id_author = $id_author;
		}
		
		$movement->setRight($this);
		$movement->save();
	}
	
	
	
	/**
	 * Tester si tout les beneficiaires du droit on consome tout le solde
	 * @return bool
	 */
	public function isResulted()
	{
		if ($this->cbalance === 'N')
		{
			// soldes negatifs autorises
			// considere comme solde si le droit n'est pas dispo
			return !$this->isAvailable();
		}
		
		
		foreach($this->getAgentRightIterator() as $agent_right)
		{
			if ($agent_right->getAvailableQuantity() > 0)
			{
				// tout n'a pas ete consome
				return false;
			}
		}
		
		// tout a ete consome et le droit n'est pas dispo
		return !$this->isAvailable();
	}
	
	
	
	
	/**
	 * Archiver le droit
	 */
	public function archive()
	{
		global $babDB;
		
		$babDB->db_query('UPDATE absences_rights SET archived='.$babDB->quote(1).' WHERE id='.$babDB->quote($this->id));
	}
	
	
	/**
	 * Method used with bab_Sort
	 */
	public function getSortKey()
	{
		return $this->sortkey;
	}
	
	public function setSortKey($i)
	{
		global $babDB;
		$babDB->db_query('UPDATE absences_rights SET sortkey='.$babDB->quote($i).' WHERE id='.$babDB->quote($this->id));
	}
	
	public function getSortLabel()
	{
		return $this->description;
	}
	
	
	public function getIconClassName()
	{
		bab_functionality::includeOriginal('Icons');
		return Func_Icons::ACTIONS_ARROW_RIGHT;
	}
	
	
	
	
	/**
	 * Insert new right
	 */
	public function insert()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
		global $babDB;
		
		$babDB->db_query("INSERT INTO absences_rights 
			(
				id_creditor,
				kind,
		        createdOn,
				date_entry,
				date_begin,
				date_end,
				quantity,
				quantity_unit,
				quantity_inc_month,
				quantity_inc_max,
				id_type,
				description,
				active,
				cbalance,
				date_begin_valid,
				date_end_valid,
				date_end_fixed,
				date_begin_fixed,
		        hide_empty,
				no_distribution,
				use_in_cet,
				cet_quantity,
				id_rgroup,  
				earlier,
				earlier_begin_valid,
				earlier_end_valid,  
				later,
				later_begin_valid,
				later_end_valid,
				delay_before,
				sync_status,
				sync_update,
				uuid,
				archived
			) 
		VALUES 
			(
				".$babDB->quote(bab_getUserId()).",
				".$babDB->quote($this->getKind()).",
		        NOW(),
				NOW(),
				".$babDB->quote($this->date_begin).",
				".$babDB->quote($this->date_end).",
				".$babDB->quote($this->quantity).",
				".$babDB->quote($this->quantity_unit).",
				".$babDB->quote($this->quantity_inc_month).",
				".$babDB->quote($this->quantity_inc_max).",
				".$babDB->quote($this->id_type).",
				".$babDB->quote($this->description).",
				".$babDB->quote($this->active).",
				".$babDB->quote($this->cbalance).",
				".$babDB->quote($this->date_begin_valid).",
				".$babDB->quote($this->date_end_valid).",
				".$babDB->quote($this->date_end_fixed).",
				".$babDB->quote($this->date_begin_fixed).",
		        ".$babDB->quote($this->hide_empty).",
				".$babDB->quote($this->no_distribution).",
				".$babDB->quote($this->use_in_cet).",
				".$babDB->quote($this->cet_quantity).",
				".$babDB->quote($this->id_rgroup).",
				".$babDB->quote($this->earlier).",
				".$babDB->quote($this->earlier_begin_valid).",
				".$babDB->quote($this->earlier_end_valid).",
				".$babDB->quote($this->later).",
				".$babDB->quote($this->later_begin_valid).",
				".$babDB->quote($this->later_end_valid).",
				".$babDB->quote($this->delay_before).",
				".$babDB->quote($this->sync_status).",
				".$babDB->quote($this->sync_update).",
				".$babDB->quote($this->uuid).",
				".$babDB->quote($this->archived)."
			)
		");
		
		
		$this->id = $babDB->db_insert_id();
	}
	
	
	/**
	 * Update by UUID
	 */
	public function update()
	{
		global $babDB;
		
		$query = 'UPDATE absences_rights SET 
				kind='.$babDB->quote($this->kind).',
				date_entry=NOW(),
				date_begin='.$babDB->quote($this->date_begin).',
				date_end='.$babDB->quote($this->date_end).',
				quantity='.$babDB->quote($this->quantity).',
				quantity_unit='.$babDB->quote($this->quantity_unit).',
				quantity_inc_month='.$babDB->quote($this->quantity_inc_month).',
				quantity_inc_max='.$babDB->quote($this->quantity_inc_max).',
				description='.$babDB->quote($this->description).',
				active='.$babDB->quote($this->active).',
				cbalance='.$babDB->quote($this->cbalance).',
				date_begin_valid='.$babDB->quote($this->date_begin_valid).',
				date_end_valid='.$babDB->quote($this->date_end_valid).',
				date_end_fixed='.$babDB->quote($this->date_end_fixed).',
				date_begin_fixed='.$babDB->quote($this->date_begin_fixed).',
				hide_empty='.$babDB->quote($this->hide_empty).',
				no_distribution='.$babDB->quote($this->no_distribution).',
				use_in_cet='.$babDB->quote($this->use_in_cet).',
				cet_quantity='.$babDB->quote($this->cet_quantity).',
				earlier='.$babDB->quote($this->earlier).',
				earlier_begin_valid='.$babDB->quote($this->earlier_begin_valid).',
				earlier_end_valid='.$babDB->quote($this->earlier_end_valid).',
				later='.$babDB->quote($this->later).',
				later_begin_valid='.$babDB->quote($this->later_begin_valid).',
				later_end_valid='.$babDB->quote($this->later_end_valid).',
				delay_before='.$babDB->quote($this->delay_before).',
				sync_status='.$babDB->quote($this->sync_status).',
				sync_update='.$babDB->quote($this->sync_update).',
				archived='.$babDB->quote($this->archived).' 
			WHERE uuid='.$babDB->quote($this->uuid).' 
		';
		
		bab_debug($query);
		
		$babDB->db_query($query);
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getQuantityAlertTypes()
	{
		$types = explode(',',$this->quantity_alert_types);
		
		if (empty($types))
		{
			return '';
		}
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT name FROM absences_types WHERE id IN('.$babDB->quote($types).')');
		
		$r = array();
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$r[] = $arr['name'];
		}
		
		return implode(', ', $r);
	}

	
	/**
	 * @return absences_IncrementRight[]
	 */
	public function getIncrementIterator()
	{
	    require_once dirname(__FILE__).'/increment_right.class.php';
	    $I = new absences_IncrementRightIterator;
	    $I->setRight($this);
	    return $I;
	}
	
	
	/**
	 * Quantitee attribuee sur le droit par la mise a jour mensuelle
	 * a condition qu'il n'y ai pas de quantite specifique pour l'agent
	 * @param string $date     YYYY-MM-DD
	 * @return float
	 */
	public function getIncrementQuantity($date = null)
	{
	    $n = 0.0;
	    $I = $this->getIncrementIterator();
	    $I->upto = $date;
	    
	    foreach($I as $d)
	    {
	        $n += (float) $d->quantity;
	    }
	    
	    return $n;
	}
	
	
	
	public function getDynamicConfigurationIterator()
	{
		require_once dirname(__FILE__).'/dynamic_configuration.class.php';
		
		$I = new absences_DynamicConfigurationIterator;
		$I->setRight($this);
		
		return $I;
	}
	
	/**
	 * @return array
	 */
	public function getDynamicTypes()
	{
		global $babDB;
		
		$arr = explode(',', $this->dynconf_types);
		$r = array();
		
		$res = $babDB->db_query('SELECT name FROM absences_types WHERE id IN('.$babDB->quote($arr).')');
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$r[] = $arr['name'];
		}
		
		return $r;
	}
	
	
	/**
	 * Quantite a ajouter pour le mois
	 * tiens compte de la quantite max
	 * 
	 * @param float $current_quantity		Can be the right quantity or the user right quantity
	 * 
	 * @return float
	 */
	public function getQuantityIncMonth($current_quantity)
	{
		$add_quantity = (float) $this->quantity_inc_month;
		$max = (float) $this->quantity_inc_max;

		$textmax = (int) round($max * 100);
		
		$new = ($add_quantity + $current_quantity);
		
		if ($textmax > 0 && $new > $max)
		{
		    
			if ($current_quantity >= $max)
			{
				return 0.0;
			}
			
			return ($max - $current_quantity);
		}
		
		return $add_quantity;
	}
	
	
	/**
	 * Movements related to the right
	 * @return absences_MovementIterator
	 */
	public function getMovementIterator()
	{
	    require_once dirname(__FILE__).'/movement.class.php';
	
	    $I = new absences_MovementIterator();
	    $I->setRight($this);
	
	    return $I;
	}
	
	
}











class absences_RightIterator extends absences_Iterator
{
	
	/**
	 * 
	 * @var int | array
	 */
	public $kind;
	
	/**
	 * les droit pas incrmentees depuis le premier jour du mois
	 * @var ???
	 */
	public $increment;
	
	/**
	 * 
	 * @var bool
	 */
	public $active;
	
	
	/**
	 * 
	 * @var int | array
	 */
	public $sync_status;
	
	
	/**
	 * 
	 * @var int
	 */
	public $archived = 0;


	public function getObject($data)
	{
		$right = new absences_Right($data['id']);
		$right->setRow($data);


		return $right;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;

			$query = '
			SELECT
				r.*
			FROM
				absences_rights r
			';
			
			$where = array();

			if (isset($this->kind))
			{
				$where[] = 'r.kind IN('.$babDB->quote($this->kind).')';
			}

			if (isset($this->increment))
			{
				require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
				$limit = new BAB_DateTime(date('Y'), date('n'), 1);
				
				$where[] = 'r.quantity_inc_last<'.$babDB->quote($limit->getIsoDateTime());
				$where[] = "r.quantity_inc_month>'0.00'";
			}
			
			if (isset($this->active))
			{
				$where[] = 'r.active='.$babDB->quote($this->active ? 'Y' : 'N');
			}
			
			if (isset($this->sync_status))
			{
				$where[] = 'r.sync_status IN('.$babDB->quote($this->sync_status).')';
			}
			
			if (isset($this->archived))
			{
				$where[] = 'r.archived='.$babDB->quote($this->archived);
			}
			
			if ($where)
			{
				$query .= ' WHERE '.implode(' AND ', $where);
			}
			
			bab_debug($query);

			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}
}
