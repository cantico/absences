<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



require_once dirname(__FILE__).'/../define.php';
require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';
require_once dirname(__FILE__).'/agent.class.php';

require_once $GLOBALS['babInstallPath'].'utilit/iterator/iterator.php';

/**
 * A vacation right linked to agent
 * 
 * 
 * 
 * @property int	$id_user
 * @property int	$id_right
 * @property string	$quantity
 * @property string	$date_begin_valid
 * @property string $date_end_valid
 * @property string $inperiod_start
 * @property string $inperiod_end
 * @property int	$validoverlap
 * @property string	$saving_begin
 * @property string	$saving_end 
 */
class absences_AgentRight extends absences_Record 
{

	
	/**
	 * Contain default values for vacation right
	 * @var absences_Right
	 */
	private $right;
	
	/**
	 * Contain the user vacation parameters
	 * @var absences_Agent
	 */
	private $agent;
	
	
	/**
	 * Cache for consumed confirmed quantity computed value
	 * @var float
	 */
	private $confirmed_quantity;
	
	/**
	 * Cache for consumed waiting quantity computed value
	 * @var float
	 */
	private $waiting_quantity;
	
	
	/**
	 * Cache for consumed previsional quantity computed value
	 * @var float
	 */
	private $previsional_quantity;
	
	

	/**
	 * Create absences_AgentRight object using the absences_users_rights table id
	 * @param int $id
	 * @return absences_AgentRight
	 */
	public static function getById($id)
	{
		$agentRight = new absences_AgentRight;
		$agentRight->id = $id;
		
		return $agentRight;
	}
	

	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			
			if (isset($this->id))
			{
				$res = $babDB->db_query('SELECT * FROM absences_users_rights WHERE id='.$babDB->quote($this->id));
				$this->setRow($babDB->db_fetch_assoc($res));
				
			} else if (isset($this->agent) && isset($this->right))
			{
				$res = $babDB->db_query('SELECT * FROM absences_users_rights WHERE id_user='.$babDB->quote($this->agent->getIdUser()).' AND id_right='.$babDB->quote($this->right->id));
				$this->setRow($babDB->db_fetch_assoc($res));
			}
		}
		
		return $this->row;
	}
	
	/**
	 * 
	 * @param absences_Agent $agent
	 * @return absences_AgentRight
	 */
	public function setAgent(absences_Agent $agent)
	{
		$this->agent = $agent;
		return $this;
	}
	
	/**
	 * 
	 * @param absences_Right $right
	 * @return absences_AgentRight
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			
			if (!$row['id_right'])
			{
				return null;
			}
			
			require_once dirname(__FILE__).'/right.class.php';
			$this->right = new absences_Right($row['id_right']);
		}
		
		return $this->right;
	}
	
	/**
	 * @return absences_Agent
	 */
	public function getAgent()
	{
		if (!isset($this->agent))
		{
			$row = $this->getRow();
			$this->agent = absences_Agent::getFromIdUser($row['id_user']);
		}
		
		return $this->agent;
	}
	
	
	/**
	 * Delete agentRight
	 * 
	 */
	public function delete()
	{
		$res = $this->getDynamicRightIterator();
		foreach($res as $dynright)
		{
			/*@var $dynright absences_DynamicRight */
			$dynright->quantity = 0;
			$dynright->save();
		}	
			
		global $babDB;
		$babDB->db_query('DELETE FROM absences_users_rights WHERE id='.$babDB->quote($this->id).'');
		
		return true;
	}
	
	public function save()
	{
		global $babDB;
		
		
		if (!isset($this->date_begin_valid) || empty($this->date_begin_valid))
		{
			$this->date_begin_valid = '0000-00-00';
		}
		
		if (!isset($this->date_end_valid) || empty($this->date_end_valid))
		{
			$this->date_end_valid = '0000-00-00';
		}
		
		
		if (!isset($this->inperiod_start) || empty($this->inperiod_start))
		{
			$this->inperiod_start = '0000-00-00';
		}
		
		if (!isset($this->inperiod_end) || empty($this->inperiod_end))
		{
			$this->inperiod_end = '0000-00-00';
		}
		
		if (!isset($this->saving_begin) || empty($this->saving_begin))
		{
			$this->saving_begin = '0000-00-00';
		}
		
		if (!isset($this->saving_end) || empty($this->saving_begin))
		{
			$this->saving_end = '0000-00-00';
		}
		
		if (!isset($this->validoverlap))
		{
			$this->validoverlap = 0;
		}
		
		
		
		$exists = 0;
		$quantity = '';
		
		if (empty($this->id))
		{

		    
			$babDB->db_query('
				INSERT INTO absences_users_rights (
					id_user, 
					id_right, 
					quantity, 
					date_begin_valid, 
					date_end_valid, 
					inperiod_start, 
					inperiod_end, 
					validoverlap, 
					saving_begin, 
					saving_end
				) VALUES (
					'.$babDB->quote($this->getIdUser()).',
					'.$babDB->quote($this->getIdRight()).',
					'.$babDB->quote($this->quantity).',
					'.$babDB->quote($this->date_begin_valid).',
					'.$babDB->quote($this->date_end_valid).',
					'.$babDB->quote($this->inperiod_start).',
					'.$babDB->quote($this->inperiod_end).',
					'.$babDB->quote($this->validoverlap).',
					'.$babDB->quote($this->saving_begin).',
					'.$babDB->quote($this->saving_end).' 
				)	
			');
			
			
			$this->id = $babDB->db_insert_id();
			
		} else {
		    
		    $res = $babDB->db_query('SELECT quantity FROM absences_users_rights WHERE id='.$babDB->quote($this->id));
		    $arr = $babDB->db_fetch_assoc($res);
		     
		    if ($arr) {
		        $exists = 1;
		        $quantity = $arr['quantity'];
		    }

			$babDB->db_query('UPDATE absences_users_rights
				SET 
					quantity='.$babDB->quote($this->quantity).',
					date_begin_valid='.$babDB->quote($this->date_begin_valid).',
					date_end_valid='.$babDB->quote($this->date_end_valid).',
					inperiod_start='.$babDB->quote($this->inperiod_start).',
					inperiod_end='.$babDB->quote($this->inperiod_end).',
					validoverlap='.$babDB->quote($this->validoverlap).',
					saving_begin='.$babDB->quote($this->saving_begin).',
					saving_end='.$babDB->quote($this->saving_end).' 
				WHERE
					id='.$babDB->quote($this->id));
		}
		
		
		$this->saveHistory($exists, $quantity);
	}
	
	
	
	
	
	public function saveHistory($exists, $quantity)
	{
	    global $babDB;
	    
	    $babDB->db_query('INSERT INTO absences_users_rights_history (id_user_right, linkexists, quantity, date_end) 
	        VALUES (
	           '.$babDB->quote($this->id).', 
	           '.$babDB->quote($exists).', 
	           '.$babDB->quote($quantity).', 
	           '.$babDB->quote(date('Y-m-d H:i:s')).'
	        )
	    ');
	}
	
	
	
	
	
	
	/**
	 * @deprecated replace by save()
	 */
	public function saveRightModifications()
	{
		$this->save();
	}
	
	
	/**
	 * Get quantity value using the history
	 * 
	 * @param string $date         YYYY-MM-DD
	 * 
	 * return values:
	 *   null          - unknow value at that date, use the main value
	 *   empty string  - used right value at that date
	 *   string        - used this value at that date
	 *   false         - the user was not linked to right at that date
	 * 
	 * @return mixed
	 */
	protected function getQuantityValueOnDate($date)
	{
	    if (!isset($date) || '0000-00-00' === $date) {
	        // no history because no date
	        return null;
	    }
	    
	    if (!$this->id) {
	        // no history because not saved
	        return null;
	    }
	    
	    global $babDB;
	    
	    $res = $babDB->db_query('SELECT linkexists, quantity 
	        FROM absences_users_rights_history WHERE id_user_right='.$babDB->quote($this->id).' 
	        AND date_end >= '.$babDB->quote($date.' 23:59:59').'
	        ORDER BY date_end DESC');
	    
	    $arr = $babDB->db_fetch_assoc($res);
	    
	    if (!$arr) {
	        // no history
	        return null;
	    }
	    
	    if ('0' === $arr['linkexists']) {
	        return false;
	    }
	    
	    return $arr['quantity'];
	}
	
	
	
	/**
	 * Initial quantity
	 * @param string $date     YYYY-MM-DD
	 * @return float
	 */
	public function getInitialQuantity($date = null)
	{
	    $ur_quantity = $this->getQuantityValueOnDate($date);
	    
	    if (null === $ur_quantity) {
	        // this approximation is used for installations older than 
	        // the creation of the absences_users_rights_history table
	        $ur_quantity = $this->quantity;
	    }
	    
	    if (false === $ur_quantity) {
	        $ur_quantity = '0';
	    }

	    
	    if ('' === $ur_quantity) // char(5)
	    {
	        $right = $this->getRight();
	        	
	        if (!$right || !$right->getRow())
	        {
	            bab_debug('agent right linked to invalid right '.$this->id_right);
	            return null;
	        }
	        
	        // ignore quantity if right not created
	        if (isset($date) && $date.' 23:59:59' < $right->createdOn) {
	            return null;
	        }
	        
	        $quantity = (float) $this->getRight()->quantity; // decimal(4,2)
	    } else {
	    
	        $quantity = (float) $ur_quantity;
	    }
	    
	    return $quantity;
	}
	
	

	
	
	/**
	 * Get total quantity for an agent
	 * Il s'agit de la quantite visible pour l'utilisateur final et qui n'inclu par la quantitee consommee
	 * @param string $date     YYYY-MM-DD
	 * @return float	in days or in hours
	 */
	public function getQuantity($date = null)
	{
		
	    $quantity = $this->getInitialQuantity($date);
	    
	    if (null === $quantity) {
	        // invalid or does not exists on date
	        return 0;
	    }
	    
	    $quantity += $this->getIncrementQuantity($date);
		$quantity += $this->getDynamicQuantity($date);
		
		
		return $quantity;
	}
	
	

    /**
     * Get quantity for the agent if no specific initial quantity
     * Il s'agit de la quantite visible pour l'utilisateur final et qui n'inclu par la quantitee consommee.
     * Sans tenir compte d'une eventuelle quantite initiale specifique pour l'utilisateur
     * @param string $date     YYYY-MM-DD
     * @return float	in days or in hours
     */
	public function getRightQuantity($date = null)
	{
	    $right = $this->getRight();
	    $quantity = (float) $right->quantity;
	    $quantity += $right->getIncrementQuantity($date);
	    $quantity += $this->getDynamicQuantity($date);
	    
	    return $quantity;
	}
	
	
	
	/**
	 * Get dynamic quantity
	 * quantite ajoutee ou retiree dynamiquement (ex diminution des RTT en fonction des arrets maladie)
	 * avant la date specifiee en parametre
	 * 
	 * @param string $date     YYYY-MM-DD
	 * @return float
	 */
	public function getDynamicQuantity($date = null)
	{
		$quantity = 0.0;
		
		$I = $this->getDynamicRightIterator();
		if (isset($date)) {
		    $I->createdOn = $date.' 23:59:59';
		}
		
		foreach($I as $dr)
		{
			/*@var $dr absences_DynamicRight */
			$quantity += $dr->getQuantity();
		}
		
		return $quantity;
	}
	
	
	/**
	 * Reset the user agent to default right quantity
	 */
	public function setDefaultQuantity()
	{
		$agent = $this->getAgent();
		$right = $this->getRight();
		$u = $right->quantity_unit;
		
		$old_quantity = absences_quantity($this->quantity, $u);
		$new_quantity = absences_quantity($right->quantity, $u);
		
		$this->quantity = '';
		$this->save();
		
		
		
		$this->addMovement(sprintf(absences_translate('The quantity for user %s on right %s has been changed from %s to %s'), 
				$agent->getName(), $right->description, $old_quantity, $new_quantity));
	}
	
	
	
	
	/**
	 * Get the agentRight quantity to save in table
	 * the agentRight quantity will be set to default empty string
	 * if the input value is an empty string or the same value as the right quantity
	 * 
	 * return null if the value do not need any modification
	 *
	 * @param string $inputStr    The user input string from a form
     *
	 * @return string
	 */
	public function getQuantityFromInput($inputStr)
	{
	    $right = $this->getRight();
	    $posted = (float) str_replace(',', '.', $inputStr);
	    $posted = round($posted, 2);
	     
	    // les chiffres sont compares a 2 chiffre apres la virgule pour eviter les modifiaction dues a des arrondis
	    // si le gestionnaire clique plusieures fois sur enregistrer

	    $right_quantity = round(((float) $right->quantity) + $this->getDynamicQuantity() + $right->getIncrementQuantity(), 2);
	    $agentright_quantity = round(((float) $this->quantity) + $this->getDynamicQuantity() + $this->getAgentIncrementQuantity(), 2);

	    
	    if (absences_cq($posted, $right_quantity) || '' === $inputStr)
	    {
	        if ('' === $this->quantity) {
	            // already use the default
	            return null;
	        }

	        return '';
	    }
	    
	    
	    if (absences_cq($posted, 0) && '' === $this->quantity) {
	         
	        // agentRight does use the default, but modification was set to 0
	        // ->quantity must be set to the posted value
	        
	        return (string) (-1 * ($this->getDynamicQuantity() + $this->getAgentIncrementQuantity()));
	    }
	    
	    
	    if (absences_cq($posted, $agentright_quantity)) {
	        // agentRight quantity not modified
	        return null;
	    }
	    
	    
	    return (string) ($posted - ($this->getDynamicQuantity() + $this->getAgentIncrementQuantity()));
	}
	
	
	
	
	/**
	 * Disponibilite en fonction de la date de saisie de la demande de conges
	 * @return string
	 */
	public function getDateBeginValid()
	{
		if ('0000-00-00' === $this->date_begin_valid)
		{
			return $this->getRight()->date_begin_valid;
		}
		
		return $this->date_begin_valid;
	}
	
	/**
	 * Disponibilite en fonction de la date de saisie de la demande de conges
	 * @return string
	 */
	public function getDateEndValid()
	{
		if ('0000-00-00' === $this->date_end_valid)
		{
			return $this->getRight()->date_end_valid;
		}
	
		return $this->date_end_valid;
	}
	
	
	
	
	
	/**
	 * Get id user from row or from agent
	 * @return int
	 */
	private function getIdUser()
	{
		if (isset($this->id_user))
		{
			return $this->id_user;
		}
		
		if (isset($this->agent))
		{
			return $this->agent->getIdUser();
		}
		
		return null;
	}
	
	/**
	 * @return int
	 */
	private function getIdRight()
	{
		if (isset($this->id_right))
		{
			return $this->id_right;
		}
		
		if (isset($this->right))
		{
			return $this->right->id;
		}
		
		return null;
	}
	
	
	
	/**
	 * Get sum of entry element filtered by status
	 * @param string $status
	 * @param string $date     YYYY-MM-DD  Up to date
	 * @return float
	 */
	private function getEntryElemSum($status, $date = null)
	{
	    
	    require_once dirname(__FILE__).'/entry.class.php';
		$I = new absences_EntryIterator();
		$I->archived = null;
		
		if (!isset($date) || '0000-00-00' === $date) {
		    $I->status = $status;
		} else {
		    $I->createdOn = $date.' 23:59:59';
		}
		
		$I->users = array($this->id_user);
		$I->id_right = $this->id_right;
		
		$total = 0.0;
		

		foreach ($I as $entry) {
		    
		    /* @var $entry absences_Entry */

	        $dateStatus = $entry->getDateStatus($date);
	        
	        
	        
	        if ($dateStatus !== $status) {
	            continue;
	        }

		    
		    /*@var $entry absences_Entry */
		    $element = $entry->getElement($this->id_right);
		    
		    if (!isset($element)) {
		        continue;
		    }

		    
		    $total += (float) $element->quantity;
		}

		
		return $total;
	}
	
	/**
	 * Get sum of CET deposits filtered by status
	 * @param string $status
	 * @param string $date     YYYY-MM-DD  Up to date
	 * @return number
	 */
	private function getCetDepositSum($status, $date = null)
	{
	    require_once dirname(__FILE__).'/cet_deposit_request.class.php';
	    $I = new absences_CetDepositRequestIterator();
	    
	    if (!isset($date) || '0000-00-00' === $date) {
	        $I->status = $status;
	    } else {
	        $I->createdOn = $date.' 23:59:59';
	    }
	    
	    $I->archived = null;
	    $I->users = array($this->id_user);
	    $I->id_agent_right_source = $this->id;
	    
	    
	    $total = 0.0;
	    
	    foreach ($I as $cetDeposit) {
	        
	        

	        /* @var $cetDeposit absences_CetDepositRequest */
	    
            $dateStatus = $cetDeposit->getDateStatus($date);
            
            if ($dateStatus !== $status) {
                continue;
            }
	    
	        $total += (float) $cetDeposit->quantity;
	    }
	    
	    return $total;
	    
	}
	
	
	
	
	/**
	 * Get consumed confirmed quantity
	 * should work if user not associated to right
	 * @param string $date
	 * @return float	in days or in hours
	 */
	public function getConfirmedQuantity($date = null)
	{
	    if (isset($date) && '0000-00-00' !== $date) {
	        // do not cache
	        return $this->getEntryElemSum('Y', $date) + $this->getCetDepositSum('Y', $date);
	    }
	    
		if (!isset($this->confirmed_quantity))
		{
			$this->confirmed_quantity = $this->getEntryElemSum('Y');
			$this->confirmed_quantity += $this->getCetDepositSum('Y');
		}

		return $this->confirmed_quantity;
	}
	
	
	
	
	/**
	 * Get consumed waiting quantity
	 *
	 * @return float	in days or in hours
	 */
	public function getWaitingQuantity()
	{
		if (!isset($this->waiting_quantity))
		{
			$this->waiting_quantity = $this->getEntryElemSum('');
			$this->waiting_quantity += $this->getCetDepositSum('');
		}
		return $this->waiting_quantity;
	}
	
	
	/**
	 * Get previsional quantity
	 *
	 * @return float	in days or in hours
	 */
	public function getPrevisionalQuantity()
	{
	    if (!isset($this->previsional_quantity)) {
	        $this->previsional_quantity = $this->getEntryElemSum('P');
	    }
	    
	    return $this->previsional_quantity;
	}
	
	
	
	
	/**
	 * Get available quantity 
	 * les demandes en attente de validation ne sont pas comptabilises comme des jours consommes
	 * @param string $date
	 * @return float	in days or in hours
	 */
	public function getAvailableQuantity($date = null)
	{
		$available = ($this->getQuantity($date) - $this->getConfirmedQuantity($date));

		return round($available, 2);
	}
	
	
	/**
	 * quantite disponible (colone solde de l'export)
	 * @return float
	 */
	public function getBalance()
	{
	    $available = ($this->getAvailableQuantity()- $this->getWaitingQuantity());
	    return $available;
	}
	
	/**
	 * Quantite disponible epargnable
	 * @return float
	 */
	public function getCetAvailableQuantity()
	{
		return $this->getBalance();
	}
	
	
	public function getCetMaxQuantity()
	{
		$cet_quantity = (float) $this->getRight()->cet_quantity;
		
		return $cet_quantity;
	}
	
	
	
	/**
	 * Tester si le droit est dans le regime de l'agent
	 * @return bool
	 */
	public function isRightInAgentCollection()
	{
		global $babDB;
		
		$agent = $this->getAgent();
		
		$res = $babDB->db_query('SELECT id FROM absences_coll_rights WHERE id_coll='.$babDB->quote($agent->id_coll).' AND id_right='.$babDB->quote($this->id_right));
		
		return ($babDB->db_num_rows($res) > 0);
	}
	
	
	/**
	 * Liste des messages d'erreur produit par ce lien droit/utilisateur
	 * cette methode droit renvoyer des messages si le droit ne fonctionnera pas ou mal
	 * @return array
	 */
	public function getErrors()
	{
		$errors = array();
		$right = $this->getRight();
		$agent = $this->getAgent();
		
		if ($right->earlier && null === $agent->getDirEntryDate($right->earlier))
		{
			$errors[] = absences_translate('The right contain a criterion based on an earlier date in the directory entry but the date is invalid');
		}
		
		if ($right->later && null === $agent->getDirEntryDate($right->later))
		{
			$errors[] = absences_translate('The right contain a criterion based on a later date in the directory entry but the date is invalid');
		}
		
		
		return $errors;
	}
	
	
	
	/**
	 * Create report on right for the agent if possible
	 * @return bool
	 */
	public function createReport()
	{
		// faire le report des droit si le droit n'est plus valide et si il reste un solde non consome
		
		$report_quantity = ($this->getAvailableQuantity() - $this->getWaitingQuantity());
		$available = (int) round(100 * $report_quantity);
		if ($available === 0)
		{
			return false;
		}
		
		if ($this->getDateEndValid() == '0000-00-00')
		{
			// le droit est toujours valide dans le temps, on ne cree pas de report
			return false;
		}
		
		if (bab_mktime($this->getDateEndValid()." 23:59:59") > mktime())
		{
			// le droit est encore valide
			return false;
		}
		
		$report = $this->getRight()->getOrCreateReport();
		
		if (!($report instanceof absences_Right))
		{
			// le droit de report ne peut pas etre cree
			return false;
		}
		
		$agent = $this->getAgent();
		
		
		
		$agent_report = new absences_AgentRight();
		$agent_report->setAgent($agent);
		$agent_report->setRight($report);
		
		if (!$agent_report->getRow()) {
		    
			// le report n'existe pas pour cet utilisateur
			$agent_report->quantity = $report_quantity;	
			$agent_report->save();
			return true;
		} 
			
		// le report existe deja
	    
		$current_value = (int) round(100 *$agent_report->getQuantity());
		
		if ($current_value == (int) round(100 *$report_quantity)) {
		    
			return false;
		} 

		// exiting report need to be updated?
		// NO: T9487
		// $agent_report->quantity = $report_quantity;
		// $agent_report->save();
		return true;
		
	}
	
	
	/**
	 * @return float in days
	 */
	public function getQuantityAlertConsumed()
	{
		// trouver la consomation de l'agent pour les droits de type $quantity_alert_types sur la periode
		
		global $babDB;
		$right = $this->getRight();
		
		$query = '
		
		SELECT
		
		SUM(ee.quantity) quantity,
		r.quantity_unit
		FROM
		absences_entries e,
		absences_entries_elem ee,
		absences_rights r
		
		WHERE
		e.id_user='.$babDB->quote($this->getAgent()->getIdUser()).'
		AND r.id_type IN('.$babDB->quote(explode(',',$right->quantity_alert_types)).')
		AND r.id=ee.id_right
		AND e.id=ee.id_entry
		AND e.date_begin>='.$babDB->quote($right->quantity_alert_begin).'
		AND e.date_end<='.$babDB->quote($right->quantity_alert_end).'
		
		GROUP BY r.quantity_unit
		';
		
		
		$res = $babDB->db_query($query);
		
		
		$qte = 0.0;
		
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$quantity = (float) $arr['quantity'];
		
			if ($arr['quantity_unit'] == 'H')
			{
				$qte += ($quantity / 8);
			} else {
				$qte += $quantity;
			}
		}
		
		return $qte;
	}
	
	
	
	/**
	 * Retourne le message d'alerte sur la quantite consomme
	 * ou null si l'alerte n'est pas ateinte
	 * Le test est effectue sur la periode configuree
	 * @return string | null
	 */
	public function getQuantityAlert()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$right = $this->getRight();
		$quantity_alert_days = (float) $right->quantity_alert_days;
		if (0 === (int) round(100 * $quantity_alert_days))
		{
			return null;
		}

		
		if ('0000-00-00' === $right->quantity_alert_begin || '0000-00-00' === $right->quantity_alert_end)
		{
			bab_debug('Alert failed because of missing test period');
			return null;
		}
		
		
		$qte = $this->getQuantityAlertConsumed();

		if ($qte > $quantity_alert_days)
		{
			return sprintf(
				absences_translate('%s has consumed %s of %s beetween the %s and the %s'), 
				$this->getAgent()->getName(), 
				absences_quantity(round($qte,2), 'D'), 
				$right->getQuantityAlertTypes(), 
				bab_shortDate(bab_mktime($right->quantity_alert_begin), false),
				bab_shortDate(bab_mktime($right->quantity_alert_end), false)
			);
		}

		return null;
	}
	
	
	
	
	
	/**
	 * Attribution du droit en fonction des jours demandes et valides
	 * @return bool
	 */
	public function isAccessibleAccordingToRequests()
	{
		return $this->getRight()
				->getRightRule()
				->isAccessibleAccordingToRequests($this->getAgent()->getIdUser());
	}
	
	
	/**
	 * Tester si le droit est accessible en fonction de la periode de validite du droit
	 * @return bool
	 */
	public function isAccessibleByValidityPeriod()
	{
		$access= true;
		
		if( $this->getDateBeginValid() != '0000-00-00' && (bab_mktime($this->getDateBeginValid()." 00:00:00") > mktime())){
			$access= false;
		}
		
		if( $this->getDateEndValid() != '0000-00-00' && (bab_mktime($this->getDateEndValid()." 23:59:59") < mktime())){
			$access= false;
		}
		
		return $access;
	}
	
	
	
	
	/**
	 * Tester si le droit est accessible en fonction de la fiche d'annuaire de l'agent
	 * @return bool
	 */
	public function isAcessibleByDirectoryEntry()
	{
		$right = $this->getRight();
		
		if (!$right->earlier && !$right->later)
		{
			return true;
		}
		
		$agent = $this->getAgent();
		
		if ($right->earlier)
		{
			$date = $agent->getDirEntryDate($right->earlier);
			if (!isset($date))
			{
				return false;
			}
			
			$year = (int) date('Y', bab_mktime($date));
			$age = (((int)date('Y')) - $year);
			
			bab_debug(sprintf("Filtre par date anterieure pour le droit %s | age=%d (intervale %d - %d)", $right->description,$age, $right->earlier_begin_valid, $right->earlier_end_valid));

			if ($right->earlier_begin_valid > $age || $right->earlier_end_valid < $age)
			{
				return false;
			}
		}
		
		
		if ($right->later)
		{
			$date = $agent->getDirEntryDate($right->later);
			if (!isset($date))
			{
				return false;
			}
			
			$year = (int) date('Y', bab_mktime($date));
			$last = ($year - (int) date('Y'));
			
			bab_debug(sprintf("Filtre par date posterieure pour le droit %s | reste=%d (intervale %d - %d)", $right->description, $last, $right->earlier_begin_valid, $right->earlier_end_valid));
		
			if ($right->later_begin_valid < $last || $right->later_end_valid > $last)
			{
				return false;
			}
		}
		
		
		
		return true;
	}
	
	
	
	
	/**
	 * Tester si le droit est disponible dans le mois en cours (en fonction de la periode de conges demandee)
	 * si cette methode renvoi true, il n'y a pas de fin de validite dans le mois en cours sauf si validoperlap est actif
	 * 
	 * si on ce base sur cette methode pour determiner si un droit doit etre incremente dans le mois
	 *  - le droit sera incremente si pas de fin de validite dans le mois
	 *  - le droit sera incremente si le chevauchement est active et que la fin de validite est dans le mois
	 * 
	 * 
	 * @return bool
	 */
	public function isAccessibleOnMonth()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$begin = new BAB_DateTime(date('Y'), date('n'), 1);
		$end = new BAB_DateTime(date('Y'), date('n'), date('t'), 23, 59, 59);
		
		return $this->isAccessibleOnPeriod($begin->getTimeStamp(), $end->getTimeStamp());
	}
	
	
	
	protected function saveQuantityIncMonth($quantity)
	{
	    require_once dirname(__FILE__).'/increment_agent_right.class.php';
	    
	    $increment = new absences_IncrementAgentRight();
	    $increment->id_user_right = $this->id;
	    $increment->quantity = $quantity;
	    $increment->createdOn = date('Y-m-d H:i:s');
	    $increment->monthkey = date('Ym');
	    
	    $increment->saveOrUpdate();
	}
	
	
	/**
	 * Increment quantity for the month if not allready done
	 * return true if quantity has been modified
	 * Do not call directly because the quantity_inc_last field exists only on the right
	 * 
	 * @param LibTimer_eventHourly $event optional event if the action is done via a background task
	 * 
	 * @return bool
	 */
	public function monthlyQuantityUpdate(LibTimer_eventHourly $event = null)
	{
	    if ('' === $this->quantity) {
	        return false;
	    }
	    
	    if (!$this->isAccessibleOnMonth())
	    {
	        return false;   
	    }
	    
        $right = $this->getRight();	
        
        $ur_quantity = $this->getQuantity();
        $old_ur_quantity = $ur_quantity;
        $quantity_inc_month = $right->getQuantityIncMonth($ur_quantity);
        $ur_quantity += $quantity_inc_month;
        
        if (0 === (int) round(100 * $quantity_inc_month)) {
            return false;
        }
        
        $this->saveQuantityIncMonth($quantity_inc_month);
        

        $message = sprintf(
            absences_translate('The quantity of the right "%s" and for user "%s" has been modified from %s to %s by the monthly update'),
            $right->description,
            $this->getAgent()->getName(),
            $old_ur_quantity,
            $ur_quantity
        );
        
        $this->addMovement($message, '', 0);
        
        if (isset($event)) {
            $event->log('absences', $message);
        }
        
        return true;
	}
	
	
	
	
	
	/**
	 * Disponibilite en fonction de la periode de conges demandee
	 * le droit est accessible si on ne test pas de demande (premiere page de la demande, ne pas appeller cette methode dans ce cas)
	 *
	 * @param int $beginp	Timestamp, debut de la periode demandee
	 * @param int $endp		Timestamp, fin de la periode demandee
	 * 
	 * @return bool
	 */
	public function isAccessibleOnPeriod($beginp, $endp)
	{
		$rightRule = $this->getRight()->getRightRule();
		
		if (!$rightRule->getRow()) {
		    return true;
		}
		
		if ('0000-00-00' === $this->inperiod_start || '0000-00-00' === $this->inperiod_end)
		{
			// Use default
			return $rightRule->isAccessibleOnPeriod($beginp, $endp);
		}
		
		$access_include = 0;
		$access_exclude = 1;
	
		$rightRule->addPeriodTest(
				$access_include, // reference
				$access_exclude, // reference
				$beginp,
				$endp,
				1, // toujours dans l'intervalle car il y en a qu'un configure
				$this->validoverlap,
				$this->inperiod_start,
				$this->inperiod_end
		);
	
		
	
		$debug_include = $access_include ? 'TRUE' : 'FALSE';
		$debug_exclude = $access_exclude ? 'TRUE' : 'FALSE';
	
		bab_debug(sprintf("id = %d \ntests de periodes d'inclusion %s \ntests de periodes d'exclusion %s\n",$this->id_right, $debug_include, $debug_exclude));
	
		return ($access_include && $access_exclude);
	
	}
	
	
	
	/**
	 * Disponibilite en fonction de l'intervale entre date de debut de la periode de conges demandee et de la date courrante
	 * le droit est accessible si on ne test pas de demande (premiere page de la demande, ne pas appeller cette methode dans ce cas)
	 *
	 * @param int $beginp	Timestamp, debut de la periode demandee
	 * 
	 * @return bool
	 */
	public function isAccessibleOnDelay($beginp)
	{
		$right = $this->getRight();
		$delay_before = (int) $right->delay_before;
		
		if ($delay_before > 0)
		{
		
			$now = BAB_DateTime::now();
			$now->add($delay_before, BAB_DATETIME_DAY);
			$nowTS = $now->getTimeStamp();
		
			bab_debug(sprintf("%s / Debut de periode : %s, Date de delais minimum : %s", $right->description, bab_shortDate($beginp), bab_shortDate($nowTS)));
			
			if($beginp <= $nowTS) {
				return false;
			}
		}

		return true;
	}

	
	
	/**
	 *
	 * @param string $message	generated message
	 * @param string $comment	Author comment
	 */
	public function addMovement($message, $comment = '', $id_author = null)
	{
		require_once dirname(__FILE__).'/movement.class.php';
	
		$movement = new absences_Movement();
		$movement->message = $message;
		$movement->comment = $comment;
		
		if (isset($id_author))
		{
			$movement->id_author = $id_author;
		}
		
		$movement->setAgent($this->getAgent());
		$movement->setRight($this->getRight());
		$movement->save();
	}
	
	
	/**
	 * Enregistre un mouvement suite a la modification d'un droit pour un agent
	 * a partir de la liste des droit ou dans la modification d'un seul droit pour l'agent
	 * 
	 * @param absences_Agent   $agent
	 * @param absences_Changes $changes
	 * @param string           $comment
	 */
	public function addAgentMovement(absences_Agent $agent, absences_Changes $changes, $comment)
	{
	    $changesStr = $changes->__toString();
	    
	    if (empty($changesStr)) {
	        return;
	    }
	    
	    $right = $this->getRight();
	    
	    $this->addMovement(
	        sprintf(
	            absences_translate('Update right "%s" parameters for user "%s", details : %s'),
	            $right->description,
	            $agent->getName(),
	            $changesStr
	        ),
	        $comment
	    );
	}
	
	
	/**
	 * @return absences_IncrementAgentRightIterator
	 */
	protected function getIncrementAgentRightIterator()
	{
	    require_once dirname(__FILE__).'/increment_agent_right.class.php';
	    $I = new absences_IncrementAgentRightIterator;
	    $I->setAgentRight($this);
	    return $I;
	}
	
	
	/**
	 * @return absences_IncrementRecord[]
	 */
	public function getIncrementIterator()
	{
	    if ('' === $this->quantity) {
	        return $this->getRight()->getIncrementIterator();
	    }
	    
	    return $this->getIncrementAgentRightIterator();
	}
	
	
	/**
	 * Somme des quantitees produites par l'incrementation mensuelle du solde, valeur specifique de l'agent
	 *
	 * @return float
	 */
	public function getAgentIncrementQuantity($date = null)
	{
	    $n = 0.0;
	    $I = $this->getIncrementAgentRightIterator();
	    $I->upto = $date;
	    
	    foreach($I as $d)
	    {
	        $n += (float) $d->quantity;
	    }
	    
	    return $n;
	}
	
	
	
	/**
	 * Somme des quantitees produites par l'incrementation mensuelle du solde
	 * @param string $date YYYY-MM-DD
	 * @return float
	 */
	public function getIncrementQuantity($date = null)
	{
	    
	    $n = 0.0;
	    $I = $this->getIncrementIterator();
	    
	    if (isset($date) && '0000-00-00' !== $date) {
	         $I->upto = $date;
	    }
	    
	    foreach($I as $d)
	    {
	        $n += (float) $d->quantity;
	    }
	
	    return $n;
	}
	
	
	
	/**
	 * @return absences_DynamicRightIterator
	 */
	public function getDynamicRightIterator()
	{
		require_once dirname(__FILE__).'/dynamic_right.class.php';
		$I = new absences_DynamicRightIterator;
		$I->setAgentRight($this);
		return $I;
	}
	
	
	/**
	 * Somme des quantitees deja prises dans l'intervale de date specifie dans la configuration dynamique et sur les types specifies
	 * peut contenir 2 lignes par demande, une en jours et une en heures
	 * @return float
	 */
	protected function getDynamicConfirmedRes()
	{
		global $babDB;
		
		$right = $this->getRight();
		
		if ($right->dynconf_types != '' && $right->dynconf_end > $right->dynconf_begin)
		{
			$res = $babDB->db_query("
				SELECT 
					e.id id_entry, 
					r.quantity_unit, 
					SUM(ee.quantity) as quantity 
				FROM
					absences_entries_elem ee,
					absences_entries e,
					absences_rights r 
				WHERE
					e.id_user = ".$babDB->quote($this->getIdUser())."
					AND e.status=".$babDB->quote('Y')." 
					AND ee.id_entry = e.id 
					AND e.date_begin<=".$babDB->quote($right->dynconf_end.' 23:59:59')." 
					AND e.date_end>".$babDB->quote($right->dynconf_begin.' 00:00:00')." 
					AND ee.id_right=r.id 
					AND r.id_type IN(".$right->dynconf_types.") 
					
				GROUP BY r.quantity_unit, e.id 
				ORDER BY e.createdOn
			");
			
			
			return $res;
		}
	
		return null;
	}
	
	
	/**
	 * Somme des quantitees deja prises dans l'intervale de date specifie dans la configuration dynamique et sur les types specifies
	 * Une seule ligne par demande, en jours
	 * 
	 * @return array		associative array with id entry as key, number of days as value
	 */
	public function getDynamicConfirmedDays()
	{
		$res = $this->getDynamicConfirmedRes();
		
		if (!isset($res))
		{
			return array();
		}
		
		global $babDB;
		
		$r = array();
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			
			$id_entry = (int) $arr['id_entry'];
			$quantity = (float) $arr['quantity'];
			
			if (!isset($r[$id_entry]))
			{
				$r[$id_entry] = 0.0;
			}
			
			switch($arr['quantity_unit'])
			{
				case 'H':
					$r[$id_entry] += $quantity/8;
					break;
				case 'D':
					$r[$id_entry] += $quantity;
					break;
			}
		}
		
		return $r;
	}
	
	
	/**
	 * Somme des quantitees deja prises dans l'intervale de date specifie dans la configuration dynamique et sur les types specifies
	 *
	 * @return float
	 */
	public function getDynamicTotalConfirmedDays()
	{
		$n = 0.0;
		$arr = $this->getDynamicConfirmedDays();
		foreach($arr as $d)
		{
			$n += $d;
		}
		
		return $n;
	}
	
	
	
	
	/**
	 * Liste dynamique des modification du solde
	 * Calculee a partir de la configuration de absences_dynamic_configuration
	 * indexe par id_user_right/id_entry
	 * @return array
	 */
	public function getDynamicMovements()
	{
		// parcourir les demandes effectuees sur ce droit, dans l'ordre de creation
		// ajouter les quantitees dynamiques a chaque fois quand necessaire
		
		require_once dirname(__FILE__).'/dynamic_configuration.class.php';
		require_once dirname(__FILE__).'/dynamic_right.class.php';
		
		
		$min = 0.0;
		$total = 0.0;
		$movements = array();
		
		foreach ($this->getDynamicConfirmedDays() as  $id_entry => $quantity)
		{
		    // $id_entry : demande qui provoque une augmentation ou une diminution du solde
		    // $quantity : total des jours confirmes qui provoquent une augmentation ou une diminution du solde
		    
		    
			$I = new absences_DynamicConfigurationIterator();
			$I->setRight($this->getRight());
			$I->min_test_quantity = $min;
			
			$total += $quantity;
			$I->max_test_quantity = $total;
			
			
			if ($I->count() > 0)
			{
				$min = $I->max_test_quantity;

				foreach($I as $configuration)
				{
					// lignes de configuration corespondantes au supplements de quantites induite par la demande
					
					$dynamic_right = new absences_DynamicRight();
					$dynamic_right->id_entry = $id_entry;
					$dynamic_right->createdOn = date('Y-m-d H:i:s');
					$dynamic_right->id_user_right = $this->id;
					$dynamic_right->quantity = $configuration->quantity; // negatif ou positif, dans la meme unite que le droit associe associe a $this
					
					if (isset($movements[$this->id.'/'.$id_entry])) {
					    $movements[$this->id.'/'.$id_entry]->quantity += $configuration->quantity;
					} else {
					   $movements[$this->id.'/'.$id_entry] = $dynamic_right;
					}
					
					if ($this->id_user == 190) {
					    bab_debug($this->id.'/'.$id_entry.' '.$configuration->quantity.' min:'.$I->min_test_quantity.' max:'.$I->max_test_quantity);
					}
				}
			
			}
		}
		
		
		
		return $movements;
	}
	
	/**
	 * Liste des movements de solde dynamiques enregistres en base de donnes, indexes par id_user_right/id_entry
	 */
	public function getDynamicRights()
	{
		$movements = array();
		$res = $this->getDynamicRightIterator();
		foreach($res as $dynamic_right)
		{
			$movements[$dynamic_right->id_user_right.'/'.$dynamic_right->id_entry] = $dynamic_right;
		}
		
		return $movements;
	}
	
	
	/**
	 * Update the dynamic rights
	 * creer les lignes corespondantes a la configuration dans la table absences_dynamic_rights
	 * retirer les lignes qui ne sont plus valides
	 */
	public function applyDynamicRight()
	{
		$saved_list = $this->getDynamicRights();
		$update_list = $this->getDynamicMovements();
		
		foreach($saved_list as $key => $dynamic_right)
		{
			/*@var $dynamic_right absences_DynamicRight */
			
			if (!isset($update_list[$key]))
			{
				$dynamic_right->quantity = 0; // delete
				$dynamic_right->save();
			} else {
				
				$update = (int) round(100 * $update_list[$key]->quantity);
				$existing = (int) round(100 * $dynamic_right->quantity);
				
				if ($update !== $existing)
				{
					$dynamic_right->quantity = $update_list[$key]->quantity;
					$dynamic_right->save();
				}
			}
		}
		
		
		foreach($update_list as $key => $dynamic_right)
		{
			/*@var $dynamic_right absences_DynamicRight */
			
			if (!isset($saved_list[$key]))
			{
				$dynamic_right->save();
			}
		}
	}
	
	
	/**
	 * Movements related to the agentRight
	 * @return absences_MovementIterator
	 */
	public function getMovementIterator()
	{
	    require_once dirname(__FILE__).'/movement.class.php';
	
	    $I = new absences_MovementIterator();
	    $I->setRight($this->getRight());
	    $I->setAgent($this->getAgent());
	
	    return $I;
	}
}




class absences_AgentRightIterator extends absences_Iterator
{
	protected $agent;

	
	
	public function setAgent(absences_Agent $agent)
	{
		$this->agent = $agent;
	}
	
	
	public function getObject($data)
	{
		$agentRight = absences_AgentRight::getById($data['agentright__id']);
		$agentRight->setRow($this->getRowByPrefix($data, 'agentright'));
		
		if ($right_row = $this->getRowByPrefix($data, 'right'))
		{
			$right = new absences_Right($right_row['id']);
			$right->setRow($right_row);
			$agentRight->setRight($right);
			
			if ($type_row = $this->getRowByPrefix($data, 'type'))
			{
				$type = new absences_Type($type_row['id']);
				$type->setRow($type_row);
				$right->setType($type);
			}
			
			if ($right_rule = $this->getRowByPrefix($data, 'rule'))
			{
				require_once dirname(__FILE__).'/right_rule.class.php';
				$rightrule = absences_RightRule::getFromId($right_rule['id']);
				$rightrule->setRow($right_rule);
				$right->setRightRule($rightrule);
				$rightrule->setRight($right);
			}
			
			$rgroup_row = $this->getRowByPrefix($data, 'rgroup');
			if (isset($rgroup_row['id']))
			{
				$rgroup = new absences_Rgroup($rgroup_row['id']);
				$rgroup->setRow($rgroup_row);
				$right->setRgroup($rgroup);
			}
		}
	

	
		if (isset($this->agent))
		{
			$agentRight->setAgent($this->agent);
		}
	
		return $agentRight;
	}
	
	
	
}


/**
 * Iterateur des utilisations d'un droit
 *
 */
class absences_AgentRightStatIterator extends absences_Iterator
{
	/**
	 * @var absences_Right
	 */
	protected $right;

	/**
	 * 
	 * @var bool
	 */
	public $modified_quantity;


	public function setRight(absences_Right $right)
	{
		$this->right = $right;
	}
	
	
	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = "SELECT

				ur.* 
				
			FROM
				".ABSENCES_USERS_RIGHTS_TBL." ur 
			WHERE ur.id_right=".$babDB->quote($this->right->id)."
			";
			
			if (isset($this->modified_quantity))
			{
				$req .= " AND ur.quantity<>''";
			}
	
	
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
	
	
	public function getObject($data)
	{
		$agentRight = absences_AgentRight::getById($data['id']);
		$agentRight->setRow($data);
		$agentRight->setRight($this->right);
		
		return $agentRight;
	}
	
}




/**
 * iterateur des droits d'un agent, vue utilisateur
 * droit ouverts, ordonnes alphabetiquement
 */
class absences_AgentRightUserIterator extends absences_AgentRightIterator
{
	/**
	 * Show inactive rights or not
	 * @var bool
	 */
	private $show_inactive = false;
	
	/**
	 * Timestamp
	 * @var int
	 */
	private $begin = null;
	
	/**
	 * Timestamp
	 * @var int
	 */
	private $end = null;
	
	/**
	 * 
	 * @var int
	 */
	private $kind = null;
	
	/**
	 * 
	 * @var bool
	 */
	private $use_in_cet = null;
	
	/**
	 * 
	 * @var string
	 */
	private $quantity_unit = null;
	
	
	/**
	 * si un gestionnaire depose une demande a la place d'un agent, il peut utiliser des droits inactifs
	 * 
	 * @param bool $status
	 * @return absences_AgentRightUserIterator
	 */
	public function showInactive($status = true)
	{
		$this->show_inactive = $status;
		return $this;
	}
	
	/**
	 * Definir la periode de la demande
	 * si la periode de la demande n'est pas renseignee, les droits qui s'affichent en fonction de la periode de la demande serons toujours retournes par l'iterateur
	 * 
	 * @param int $begin
	 * @param int $end
	 * 
	 * @return absences_AgentRightUserIterator
	 */
	public function setRequestPeriod($begin, $end)
	{
		$this->begin = $begin;
		$this->end = $end;
		return $this;
	}
	
	/**
	 * Filter results by kind
	 * @param	int	$kind
	 * 
	 * @return absences_AgentRightUserIterator
	 */
	public function setKind($kind)
	{
		$this->kind = $kind;
		return $this;
	}
	
	
	
	/**
	 * Filter results by use in CET
	 * @param	bool	$pickup
	 * @return absences_AgentRightUserIterator
	 */
	public function setUseInCet($pickup = true)
	{
		$this->use_in_cet = $pickup;
		return $this;
	}
	
	
	/**
	 * Filter by quantity_unit
	 * @param	string	$unit		D | H
	 * @return absences_AgentRightUserIterator
	 */
	public function setQuantityUnit($unit)
	{
		$this->quantity_unit = $unit;
		return $this;
	}
	
	
	
	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = "SELECT
			
				r.id				right__id,
				r.kind				right__kind,
				r.id_creditor		right__id_creditor,
			    r.createdOn		    right__createdOn,
				r.date_entry		right__date_entry,
				r.date_begin 		right__date_begin,
				r.date_end 			right__date_end,
				r.quantity			right__quantity,
				r.quantity_unit 	right__quantity_unit,
				r.id_type			right__id_type,
				r.description		right__description,
				r.active			right__active,
				r.cbalance			right__cbalance,
				r.date_begin_valid	right__date_begin_valid,
				r.date_end_valid	right__date_end_valid,
				r.date_end_fixed	right__date_end_fixed,
				r.date_begin_fixed	right__date_begin_fixed,
			    r.hide_empty        right__hide_empty,
				r.no_distribution	right__no_distribution,
				r.id_rgroup			right__id_rgroup,
				r.earlier					right__earlier,
	 			r.earlier_begin_valid		right__earlier_begin_valid,
	  			r.earlier_end_valid			right__earlier_end_valid,
	  			r.later						right__later,
	  			r.later_begin_valid			right__later_begin_valid,
	  			r.later_end_valid			right__later_end_valid,
	  			r.sortkey					right__sortkey, 
	  			r.require_approval			right__require_approval,
				r.delay_before				right__delay_before, 
				
				
				rules.id					rule__id,
				rules.id_right				rule__id_right,
				rules.validoverlap			rule__validoverlap,
				rules.trigger_nbdays_min 	rule__trigger_nbdays_min,
				rules.trigger_nbdays_max	rule__trigger_nbdays_max,
				rules.trigger_type			rule__trigger_type,
				rules.trigger_p1_begin		rule__trigger_p1_begin,
				rules.trigger_p1_end		rule__trigger_p1_end,
				rules.trigger_p2_begin		rule__trigger_p2_begin,
				rules.trigger_p2_end		rule__trigger_p2_end,
				rules.trigger_overlap		rule__trigger_overlap,
				
				rg.id 				rgroup__id,
				rg.name 			rgroup__name,
				rg.quantity_unit	rgroup__quantity_unit,
				rg.recover			rgroup__recover,
				rg.sortkey			rgroup__sortkey,
				
				ur.id				agentright__id,
				ur.id_user			agentright__id_user,
				ur.id_right			agentright__id_right,
				ur.quantity 		agentright__quantity,
				ur.date_begin_valid	agentright__date_begin_valid,
				ur.date_end_valid	agentright__date_end_valid, 
				
				ur.inperiod_start	agentright__inperiod_start,
				ur.inperiod_end		agentright__inperiod_end,
				ur.validoverlap		agentright__validoverlap,
				ur.saving_begin		agentright__saving_begin,
				ur.saving_end		agentright__saving_end   
				
				
			FROM
				".ABSENCES_TYPES_TBL." t,
				".ABSENCES_USERS_RIGHTS_TBL." ur,
				".ABSENCES_RIGHTS_TBL." r
				LEFT JOIN
					".ABSENCES_RIGHTS_RULES_TBL." rules ON rules.id_right = r.id
				LEFT JOIN
					".ABSENCES_RGROUPS_TBL." rg ON rg.id = r.id_rgroup
			WHERE 
				ur.id_user=".$babDB->quote($this->agent->getIdUser())." AND ur.id_right=r.id AND r.id_type=t.id 	
			";
			
			/*
			$query .= "
				AND t.id = c.id_type
				AND c.id_coll=p.id_coll
				AND p.id_user=".$babDB->quote($this->agent->getIdUser())."
			";
			*/
			
			if( !$this->show_inactive )
			{
				$req .= " AND r.active='Y'";
			}
			
			if (isset($this->kind))
			{
				$req .= " AND r.kind=".$babDB->quote($this->kind);
			}
			
			if (isset($this->use_in_cet))
			{
				$req .= " AND r.use_in_cet=".$babDB->quote((int) $this->use_in_cet);
			}
			
			if (isset($this->quantity_unit))
			{
				$req .= " AND r.quantity_unit=".$babDB->quote($this->quantity_unit);
			}
			
			$req .= " 
			
				GROUP BY r.id 
				ORDER BY r.sortkey, r.description";
			
			
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_AgentRightIterator::getObject()
	 */
	public function getObject($data)
	{
		$agentRight = parent::getObject($data);
		
		
		$right = $agentRight->getRight();

		if (!$agentRight->isAccessibleByValidityPeriod() || !$right->isAccessibleIfFixed())
		{
			return null;
		}
		
		if (!$agentRight->isAcessibleByDirectoryEntry())
		{
			return null;
		}
		
		if (isset($this->begin) && isset($this->end)) { // le doit est accessible si on ne test pas de periode de la demande (premiere page de la demande)
			if (!$agentRight->isAccessibleOnPeriod($this->begin, $this->end))
			{
				return null;
			}
			
			if (!$agentRight->isAccessibleOnDelay($this->begin))
			{
				return null;
			}
		}
		
		$rightRule = $agentRight->getRight()->getRightRule();
	
		// Attribution du droit en fonction des jours demandes et valides
		if ($rightRule->getRow() && !$rightRule->isAccessibleAccordingToRequests($this->agent->getIdUser()) ) {
			return null;
		}
		

		if ($right->hide_empty && $agentRight->getAvailableQuantity() <= 0) {
		    return null;
		}
		
		
		return $agentRight;
		
	}
	
	
	
	/**
	 * 
	 *
	 * @return mixed
	 */
	public function next()
	{
		$this->executeQuery();
		
		do {
			$agentRight = false;
			if ($aDatas = $this->getDataBaseAdapter()->db_fetch_assoc($this->_oResult))
			{
				$agentRight = $this->getObject($aDatas);
			}
		} while (!isset($agentRight));
	
		if(false !== $agentRight)
		{
			$this->_iKey++;
			$this->_oObject = $agentRight;
		}
		else
		{
			$this->_oObject = null;
			$this->_iKey = self::EOF;
		}
	
		return $this->_oObject;
	}
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see BAB_MySqlResultIterator::count()
	 */
	public function count()
	{
		$i = 0;
		foreach($this as $agentRight)
		{
			$i++;
		}
		
		$this->rewind();
		return $i;
	}
}





/**
 * iterateur des droits d'un agent, vue gestionnaire
 * tout les droits associes a l'agent, ordonnes par annees, puis alphabetiquement
 */
class absences_AgentRightManagerIterator extends absences_AgentRightIterator
{
	
	/**
	 * Filter rights by year
	 * @var int
	 */
	public $year = null;
	

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
	
			$query = '
			SELECT
			u.id				agentright__id,
			u.id_user			agentright__id_user,
			u.id_right			agentright__id_right,
			u.quantity			agentright__quantity,
			u.date_begin_valid	agentright__date_begin_valid,
			u.date_end_valid	agentright__date_end_valid, 
			u.inperiod_start    agentright__inperiod_start,
			u.inperiod_end      agentright__inperiod_end,
			u.validoverlap      agentright__validoverlap,
			u.saving_begin      agentright__saving_begin,
			u.saving_end        agentright__saving_end,
	
			r.id				right__id,
			r.kind				right__kind,
			r.id_creditor		right__id_creditor,
			r.createdOn  		right__createdOn,
			r.date_entry		right__date_entry,
			r.date_begin 		right__date_begin,
			r.date_end 			right__date_end,
			r.quantity			right__quantity,
			r.quantity_unit 	right__quantity_unit,
			r.id_type			right__id_type,
			r.description		right__description,
			r.active			right__active,
			r.cbalance			right__cbalance,
			r.date_begin_valid	right__date_begin_valid,
			r.date_end_valid	right__date_end_valid,
			r.date_end_fixed	right__date_end_fixed,
			r.date_begin_fixed	right__date_begin_fixed,
			r.hide_empty    	right__hide_empty,
			r.no_distribution	right__no_distribution,
			r.id_rgroup			right__id_rgroup,
			r.earlier			right__earlier,
 			r.earlier_begin_valid	right__earlier_begin_valid,
  			r.earlier_end_valid		right__earlier_end_valid,
  			r.later					right__later,
  			r.later_begin_valid		right__later_begin_valid,
  			r.later_end_valid		right__later_end_valid,
  			r.id_report_type		right__id_report_type,
  			r.date_end_report		right__date_end_report,
  			r.description_report	right__description_report,
  			r.id_reported_from		right__id_reported_from,
  			r.quantity_alert_days	right__quantity_alert_days,
  			r.quantity_alert_types	right__quantity_alert_types,
  			r.quantity_alert_begin	right__quantity_alert_begin,
  			r.quantity_alert_end	right__quantity_alert_end,
  			r.dynconf_types			right__dynconf_types,
  			r.dynconf_begin			right__dynconf_begin,
  			r.dynconf_end			right__dynconf_end,
  			r.require_approval		right__require_approval,
			r.delay_before			right__delay_before, 
			r.quantity_inc_month    right__quantity_inc_month,
			r.quantity_inc_last     right__quantity_inc_last,
			r.quantity_inc_max      right__quantity_inc_max, 
  			
	
			t.id				type__id,
			t.name 				type__name,
			t.description		type__description,
			t.quantity			type__quantity,
			t.maxdays			type__maxdays,
			t.mindays			type__mindays,
			t.defaultdays		type__defaultdays,
			t.color				type__color,
			t.cbalance			type__cbalance,
	
			rg.id				rgroup__id,
			rg.name 			rgroup__name,
	
			IFNULL(rg.name, r.description) label,
	
	
			YEAR(r.date_begin) year
			FROM
				absences_users_rights u,
				absences_rights r
					LEFT JOIN absences_rgroup rg ON rg.id = r.id_rgroup,
			absences_types t
	
			WHERE 
				id_user='.$babDB->quote($this->agent->getIdUser()).'
				AND r.id = u.id_right
				AND t.id = r.id_type ';
			
			if (isset($this->year)) {
			    $query .= 'AND YEAR(r.date_begin)='.$babDB->quote($this->year).' ';
			}
			
			$query .= '
			ORDER BY year DESC, label ASC';
	
	
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}
	
}
