<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



require_once dirname(__FILE__).'/right.class.php';

class absences_AgentCet
{
	/**
	 * 
	 * @var absences_Agent
	 */
	private $agent;
	
	/**
	 * 
	 * @var absences_Right
	 */
	private $depositRight;
	
	/**
	 *
	 * @var absences_AgentRight
	 */
	private $depositAgentRight;
	
	
	private $rights_withDeposits = null;
	
	/**
	 * 
	 * @var array
	 */
	private $rights_withAvailable = null;
	
	
	public function __construct(absences_Agent $agent)
	{
		$this->agent = $agent;
	}
	
	/**
	 * Liste des droits contenant de l'epargne
	 * @return multitype:absences_AgentRight
	 */
	private function updateRights()
	{
		
		$this->rights_withDeposits = array();
		$this->rights_withAvailable = array();
		
		$I = $this->agent->getAgentRightUserIterator();
		$I->setKind(absences_Right::CET);

		foreach($I as $agentRight)
		{
			/*@var $agentRight absences_AgentRight */
			
			
			if (0 !== (int) round(100 * $agentRight->getQuantity()))
			{
				$this->rights_withDeposits[] = $agentRight;
			}
			
			

			if (0 !== (int) round(100 * $agentRight->getAvailableQuantity()))
			{
				$begin = $agentRight->getDateBeginValid();
				if (isset($this->rights_withAvailable[$begin]))
				{
					$this->rights_withAvailable[] = $agentRight;
				} else {
					$this->rights_withAvailable[$begin] = $agentRight;
				}
			}
		}
		
		ksort($this->rights_withAvailable);
	
	}

	
	/**
	 * Get user agent rights with flag use_in_cet
	 * liste des droits accessibles par l'utilisateurs, qui peuvent etres utilises par le CET
	 * 
	 * @return absences_AgentRightUserIterator
	 */
	public function getRightsIterator()
	{
		$deposit = $this->getDepositRight(false);

		require_once dirname(__FILE__).'/agent_right.class.php';
		
		$I = new absences_AgentRightUserIterator;
		$I->setAgent($this->agent);
		$I->setUseInCet(true);
		if (isset($deposit))
		{
		    // si on a access a un droit CET pour le depot
		    // le droits remontes comme source compatibles 
		    // sont obligatoirement dans la meme unite
			$I->setQuantityUnit($deposit->quantity_unit);
		}
		return $I;
	}
	
	
	
	
	/**
	 * Premier droit a utiliser pour les retraits, peut ne pas etre disponible pour les depots
	 * utiliser toujours le droit dont la date de debut de disponiblite est la plus ancienne pour l'agent, ignorer les droits soldes
	 * 
	 * @throws absences_CetException
	 * @return absences_AgentRight
	 */
	public function getUseRight()
	{
		if (!isset($this->rights_withAvailable))
		{
			$this->updateRights();
		}
		
		if (empty($this->rights_withAvailable))
		{
			return null;
		}
		
		return reset($this->rights_withAvailable);
	}
	
	
	
	
	
	
	
	
	/**
	 * somme des jours epargnes sur les CET ouverts
	 * @return float
	 */
	public function getQuantity()
	{
		if (!isset($this->rights_withDeposits))
		{
			$this->updateRights();
		}
		
		$total = 0.0;
		foreach($this->rights_withDeposits as $agentRight)
		{
			$total += $agentRight->getQuantity();
		}
	
		return $total;
	}
	
	
	
	/**
	 * somme des jours utilises confirmes sur les CET ouverts
	 * @return float
	 */
	public function getConfirmedQuantity()
	{
		if (!isset($this->rights_withDeposits))
		{
			$this->updateRights();
		}
		
		$total = 0.0;
		foreach($this->rights_withDeposits as $agentRight)
		{
			$total += $agentRight->getConfirmedQuantity();
		}
		
		return $total;
	}
	
	
	/**
	 * Get number of days in CET
	 * somme des jours epargnes - les jours utilises sur les CET ouverts
	 * @return float
	 */
	public function getAvailableQuantity()
	{
		if (!isset($this->rights_withAvailable))
		{
			$this->updateRights();
		}
		
		$total = 0.0;
		foreach($this->rights_withAvailable as $agentRight)
		{
			$total += $agentRight->getAvailableQuantity();
		}
		
		return $total;
	}
	
	
	
	
	/**
	 * le droit a utiliser pour les depots, peut ne pas etre accessible a l'utilsation
	 * Deposer sur le droit avec la date de fin de depot la plus recente
	 * 
	 * @param bool $onlyActive
	 * 
	 * @return absences_AgentRight
	 */
	public function getDepositAgentRight($onlyActive)
	{

		require_once dirname(__FILE__).'/agent_right.class.php';
		global $babDB;
		
		$query = "SELECT u.* FROM 
			absences_users_rights u,
			absences_rights r,
			absences_rights_cet c
		WHERE 
			u.id_right = r.id 
			AND u.id_user=".$babDB->quote($this->agent->getIdUser())."
	    ";
		
		$currentAgent = absences_Agent::getCurrentUser();
		
		
		if ($onlyActive && !$currentAgent->isManager()) {
		    $query .= " AND r.active='Y' ";
		}
		
		$query .= "
			AND r.kind=".$babDB->quote(absences_Right::CET)." 
			AND c.id_right=r.id 
			AND (c.saving_begin='0000-00-00' OR c.saving_begin<=CURDATE()) 
			AND (c.saving_end='0000-00-00' OR c.saving_end>=CURDATE()) 
			AND (u.saving_begin='0000-00-00' OR u.saving_begin<=CURDATE()) 
			AND (u.saving_end='0000-00-00' OR u.saving_end>=CURDATE()) 
		 ORDER BY c.saving_end DESC ";
		

		$res = $babDB->db_query($query);
		
		if (0 === $babDB->db_num_rows($res)) {
			return null;
		}
		
		if (1 !== $babDB->db_num_rows($res)) {
		    throw new Exception(absences_translate('There is more than one time saving account associated to user'));
		}
		
		$row = $babDB->db_fetch_assoc($res);
		$depositAgentRight = new absences_AgentRight;
		$depositAgentRight->setRow($row);
		
		return $depositAgentRight;
	}
	
	
	/**
	 * Droit utilise pour les depot dans le CET
	 * @return absences_Right
	 */
	public function getDepositRight($onlyActive = true)
	{
		if (!isset($this->depositRight))
		{
			$agent_right = $this->getDepositAgentRight($onlyActive);
			
			if (!isset($agent_right))
			{
				return null;
			}
			
			$this->depositRight = $agent_right->getRight();
		}
		
		return $this->depositRight;
	}
	
	
	
	/**
	 * 
	 * @return string
	 */
	public function getQuantityUnit()
	{
		$right = $this->getDepositRight(false);
		
		if (!$right)
		{
			return null;
		}
		
		return $right->quantity_unit;
	}
	
	
	/**
	 * Total des depot sur le CET
	 * @return float
	 */
	public function getTotalDeposits()
	{
		global $babDB;
	
		$agentRight = $this->getDepositAgentRight(false);
		$id_user = $this->agent->getIdUser();
	
		$res = $babDB->db_query('
				SELECT SUM(quantity) total
				FROM absences_cet_deposit_request
				WHERE
				id_user='.$babDB->quote($id_user).'
				AND id_agent_right_cet='.$babDB->quote($agentRight->id));
	
		$arr = $babDB->db_fetch_assoc($res);
	
		return (float) $arr['total'];
	}
	
	
	
	/**
	 * Total des depot dans l'annee en cours
	 * @return float
	 */
	public function getYearlyDeposits()
	{
		global $babDB;
		
		$agentRight = $this->getDepositAgentRight(false);
		$id_user = $this->agent->getIdUser();
		
		$res = $babDB->db_query('
			SELECT SUM(quantity) total  
				FROM absences_cet_deposit_request 
				WHERE 
					id_user='.$babDB->quote($id_user).' 
					AND id_agent_right_cet='.$babDB->quote($agentRight->id).' 
					AND YEAR(createdOn)='.$babDB->quote(date('Y')));
		
		$arr = $babDB->db_fetch_assoc($res);
		
		return (float) $arr['total'];
	}
	
	/**
	 * Restant a deposer avant d'atteindre le max pour l'annee
	 * @return number
	 */
	public function getYearlyMaxDeposit()
	{
		$right = $this->getDepositRight(false);
		
		if (!isset($right))
		{
			throw new Exception(absences_translate('The applicant time saving account is not accessible'));
		}
		
		$rightcet = $right->getRightCet();

		$per_year = (float) $rightcet->per_year;
		
		if (0 === (int) round($per_year * 10))
		{
			// no limit set
			return null;
		}
		
		
		return ($per_year - $this->getYearlyDeposits()); 
	}
	
	
	/**
	 * Restant a deposer avant d'atteindre le plafont total
	 * @return number
	 */
	public function getMaxDepositTotal()
	{
		$right = $this->getDepositRight(false);
		
		if (!isset($right))
		{
			throw new Exception(absences_translate('The applicant time saving account is not accessible'));
		}
		
		$rightcet = $right->getRightCet();
		
		$celling = (float) $rightcet->ceiling;
		
		if (0 === (int) round($celling * 10))
		{
			// no limit set
			return null;
		}
		
		return ($celling - $this->getTotalDeposits());
		
		
		
		// TODO available period deposits
	}
	
	
	/**
	 * Tester si il existe au moins un droit CET actif avec une periode de depot accessible
	 * is l'utilsateur est gestionnaire permettre l'acces aux droits desactives
	 * 
	 * @throws Exception
	 * 
	 * @return bool
	 */
	public function canAdd()
	{
	    $onlyActive = !absences_agent::getCurrentUser()->isManager();
	    
	    return (null !== $this->getDepositRight($onlyActive));
	    
	}
	
	/**
	 * Tester si il existe des droits utilisables
	 * @return bool
	 */
	public function canUse()
	{
		$I = $this->agent->getAgentRightUserIterator();
		$I->setKind(absences_Right::CET);
		
		return ($I->count() > 0);
	}
	
	
	
	/**
	 * Tester s'il est possible d'epargner
	 * @throws Exception
	 * 
	 * @param	absences_AgentRight				$agent_right		Right source
	 * @param	float							$quantity			Quantity to save
	 * @param 	absences_CetDepositRequest		$deposit			The deposit if allready created
	 * 
	 * @return bool
	 */
	public function testDepositQuantity(absences_AgentRight $agent_right, $quantity, absences_CetDepositRequest $deposit = null)
	{
		if (0 === (int) round($quantity*100))
		{
			throw new Exception(absences_translate('This quantity in not valid'));
		}
		
		
		$right = $agent_right->getRight();

		$onlyActive = !absences_agent::getCurrentUser()->isManager();
		$rightCet = $this->getDepositRight($onlyActive);
		
		if (!isset($rightCet))
		{
			throw new Exception(absences_translate('The applicant time saving account is not accessible'));
		}
		
		if ($rightCet->quantity_unit !== $right->quantity_unit)
		{
			throw new Exception(absences_translate('The quantity unit of source right must be the same as the time saving account quantity unit'));
		}
		
		
		$available = $agent_right->getCetAvailableQuantity(); // Do not allow using waiting quantity
		
		if (isset($deposit) && 'Y' !== $deposit->status)
		{
			// la quantite de la demande en cours est considere comme disponible si la demande est bien en attente
			$available += (float) $deposit->quantity;
		}
		
		$right = $agent_right->getRight();
		
		
		// verifier que la quantite est disponible dans le droit
		
		if ($available < $quantity)
		{
			throw new Exception(sprintf(
					absences_translate('The requested quantity is not available in the specified right, the right %s contain only %s available'),
					$right->description,
					absences_quantity($available, $right->quantity_unit)
				)
			);
		}
		
		$cet_quantity = $agent_right->getCetMaxQuantity();
		if ($cet_quantity < $quantity)
		{
			
			throw new Exception(sprintf(
					//TRANSLATORS: Vous ne pouvez deposer plus de %s de "%s" sur le compte epargne temps
					absences_translate('You cant deposit more than %s of "%s" one the time saving account'),
					$right->description,
					absences_quantity($cet_quantity, $right->quantity_unit)
				)
			);
		}
		
		
		
		
		// verifier que la quantite peut etre recue par le CET
		
		$max = $this->getMaxDepositTotal();
		
		if (isset($deposit) && 'Y' !== $deposit->status)
		{
			// la quantite de la demande en cours est considere comme disponible si la demande est bien en attente
			$max += (float) $deposit->quantity;
		}
		
		if ($max > 0 && $quantity > $max)
		{
			throw new Exception(sprintf(
					absences_translate('The time saving account does not allow more than %s to reach the ceilling'),
					absences_quantity($max, $right->quantity_unit)
				)
			);
		}
		
		$max = $this->getYearlyMaxDeposit();
		
		
		if (isset($deposit) && 'Y' !== $deposit->status)
		{
			// la quantite de la demande en cours est considere comme disponible si la demande est bien en attente
			$max += (float) $deposit->quantity;
		}
		
		if ($max > 0 && $quantity > $max)
		{
			throw new Exception(sprintf(
					absences_translate('The time saving account does not allow more than %s to reach the yearly maximum deposit'),
					absences_quantity($max, $right->quantity_unit)
				)
			);
		}
		
		
		return true;
	}
}




class absences_CetException extends Exception {
	
}