<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';
require_once dirname(__FILE__).'/agent.class.php';
require_once dirname(__FILE__).'/entry.class.php';
require_once dirname(__FILE__).'/cet_deposit_request.class.php';
require_once dirname(__FILE__).'/workperiod_recover_request.class.php';

/**
 * 
 * @property int	$id_user
 * @property int	$id_right
 * @property int	$id_request
 * @property string	$request_class
 * @property int	$id_author
 * @property string	$comment
 * @property string $createdOn
 * @property string $message
 * @property string $status
 */
class absences_Movement extends absences_Record
{

	
	
	
	/**
	 * 
	 * @var absences_Right
	 */
	private $right;
	
	/**
	 * 
	 * @var absences_Agent
	 */
	private $agent;
	
	
	/**
	 *
	 * @var absences_Request
	 */
	private $request;
	
	
	public static function getById($id)
	{
		$movement = new absences_Movement;
		$movement->id = $id;
		
		return $movement;
	}
	
	
	public function __get($property)
	{
	    $row = $this->getRow();
	
	    if (!isset($row[$property]) && 'status' !== $property)
	    {
	        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	        bab_debug_print_backtrace();
	        bab_debug($row);
	        throw new Exception(sprintf('Failed to load property %s on %s', $property, get_class($this)));
	    }
	
	    return $row[$property];
	}
	
	
	
	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
	
			$query = 'SELECT * FROM absences_movement WHERE ';
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}
	
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
	
			if (!$this->row)
			{
				throw new Exception('This movement does not exists id='.$this->id.'');
			}
		}
	
		return $this->row;
	}
	
	
	
	
	/**
	 *
	 * @param absences_Agent $agent
	 */
	public function setAgent(absences_Agent $agent)
	{
		$this->agent = $agent;
		return $this;
	}
	
	/**
	 *
	 * @param absences_Right $right
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
	
		return $this->right;
	}
	
	/**
	 * @return absences_Agent
	 */
	public function getAgent()
	{
		if (!isset($this->agent))
		{
			$row = $this->getRow();
			$this->agent = absences_Agent::getFromIdUser($row['id_user']);
		}
	
		return $this->agent;
	}
	
	
	
	/**
	 *
	 * @param absences_Request $request
	 */
	public function setRequest(absences_Request $request)
	{
		$this->request = $request;
		return $this;
	}
	
	
	/**
	 * @return absences_Request
	 */
	public function getRequest()
	{
		if (!isset($this->request))
		{
			$row = $this->getRow();
			$class = $row['request_class'];
			
			if (empty($class)||empty($row['id_request']))
			{
				return null;
			}
			
			$request = call_user_func(array($class, 'getById'), $row['id_request']);
			/*@var $request absences_Request */
			
			if (!$request->getRow())
			{
				return null;
			}
			
			$this->request = $request;
		}
	
		return $this->request;
	}
	
	
	/**
	 * Save entry to database
	 * without validity checking
	 *
	 * @return bool
	 */
	public function save()
	{
		// save entry
	
		global $babDB;
	
	
		if (!isset($this->comment))
		{
			$this->comment = '';
		}
	
		if (!isset($this->message))
		{
			throw new Exception('Message is mandatory');
		}
	
		if (!isset($this->id_user))
		{
			if (isset($this->agent))
			{
				$this->id_user = $this->agent->getIdUser();
			} else {
				$this->id_user = 0;
			}
			
		}
		
		if (!isset($this->id_right))
		{
			if (isset($this->right))
			{
				$this->id_right = $this->right->id;
			} else {
				$this->id_right = 0;
			}
			
		}
		
		
		
		if (!isset($this->id_request))
		{
			if (isset($this->request))
			{
				$this->id_request = $this->request->id;
			} else {
				$this->id_request = 0;
			}
		
		}
		
		
		if (!isset($this->request_class))
		{
			if (isset($this->request))
			{
				$this->request_class = get_class($this->request);
			} else {
				$this->request_class = '';
			}
		
		}
		
		if (!isset($this->status))
		{
		    $this->status = null;
		    $request = $this->getRequest();
		    if (isset($request))
			{
				$this->status = $request->status;
			}
		}
		
		
		if (!isset($this->id_author))
		{
			$this->id_author = bab_getUserId();
		}
		
		if (!isset($this->createdOn))
		{
		    $this->createdOn = date('Y-m-d H:i:s');
		}
	
	
		if (isset($this->id))
		{
	
			$babDB->db_query("
				UPDATE absences_movement
				SET
					comment	=".$babDB->quote($this->comment)." 
				WHERE
					id=".$babDB->quote($this->id)."
			");
	
	
		} else {
	
			$babDB->db_query("
				INSERT INTO absences_movement
				(
					id_user,
					id_right,
					id_request,
					request_class,
					id_author,
					comment,
					createdOn,
					message,
			        status
				)
				VALUES
					(
					".$babDB->quote($this->id_user).",
					".$babDB->quote($this->id_right).",
					".$babDB->quote($this->id_request).",
					".$babDB->quote($this->request_class).",
					".$babDB->quote($this->id_author).",
					".$babDB->quote($this->comment).",
					".$babDB->quote($this->createdOn).",
					".$babDB->quote($this->message).",
			        ".$babDB->quoteOrNull($this->status)."
				)
			");
	
			$this->id = $babDB->db_insert_id();
		}
	}
}








class absences_MovementIterator extends absences_Iterator
{
	protected $agent;

	protected $right;
	
	protected $request;
	
	/**
	 * get movements before date
	 * @var string
	 */
	public $createdOn;


	public function setRight(absences_Right $right)
	{
		$this->right = $right;
	}

	public function setAgent(absences_Agent $agent)
	{
		$this->agent = $agent;
	}
	
	public function setRequest(absences_Request $request)
	{
		$this->request = $request;
	}


	public function getObject($data)
	{
		$movement = new absences_Movement;
		$movement->setRow($this->getRowByPrefix($data, 'movement'));

		if ($right_row = $this->getRowByPrefix($data, 'right'))
		{
			$right = new absences_Right($right_row['id']);
			$right->setRow($right_row);
			$movement->setRight($right);
		}
		
		if ($agent_row = $this->getRowByPrefix($data, 'agent'))
		{
			$agent = new absences_Agent();
			$agent->setRow($agent_row);
			$movement->setAgent($agent);
		}
		
		return $movement;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;

			$query = '
			SELECT
				m.id 				movement__id,
				m.id_user			movement__id_user,
				m.id_right			movement__id_right,
				m.id_request		movement__id_request,
				m.request_class		movement__request_class,
				m.id_author			movement__id_author,
				m.comment			movement__comment,
				m.createdOn			movement__createdOn,
				m.message			movement__message, 
                m.status            movement__status,
				
				r.id				right__id,
				r.kind				right__kind,
				r.id_creditor		right__id_creditor,
			    r.createdOn  		right__createdOn,
				r.date_entry		right__date_entry,
				r.date_begin 		right__date_begin,
				r.date_end 			right__date_end,
				r.quantity			right__quantity,
				r.quantity_unit 	right__quantity_unit,
				r.id_type			right__id_type,
				r.description		right__description,
				r.active			right__active,
				r.cbalance			right__cbalance,
				r.date_begin_valid	right__date_begin_valid,
				r.date_end_valid	right__date_end_valid,
				r.date_end_fixed	right__date_end_fixed,
				r.date_begin_fixed	right__date_begin_fixed,
			    r.hide_empty	    right__hide_empty,
				r.no_distribution	right__no_distribution,
				r.id_rgroup			right__id_rgroup,
				r.earlier					right__earlier,
	 			r.earlier_begin_valid		right__earlier_begin_valid,
	  			r.earlier_end_valid			right__earlier_end_valid,
	  			r.later						right__later,
	  			r.later_begin_valid			right__later_begin_valid,
	  			r.later_end_valid			right__later_end_valid,
	  			r.require_approval			right__require_approval,
				r.delay_before				right__delay_before,  
	  			
	  			
	  			a.id_user			agent__id_user,
	  			a.id_coll			agent__id_coll,
	  			a.id_sa				agent__id_sa,
	  			a.id_sa_cet			agent__id_sa_cet,
	  			a.id_sa_recover		agent__id_sa_recover 
			
			FROM 
				absences_movement m 
					LEFT JOIN absences_personnel a ON a.id_user=m.id_user 
					LEFT JOIN absences_rights r ON r.id=m.id_right 
			    
			';
			
			$where = array();

			if (isset($this->agent))
			{
				$where[] = 'm.id_user='.$babDB->quote($this->agent->getIdUser());
			}

			if (isset($this->right))
			{
				$where[] = 'm.id_right='.$babDB->quote($this->right->id);
			}
			
			if (isset($this->request))
			{
				$where[] = 'm.id_request='.$babDB->quote($this->request->id);
				$where[] = 'm.request_class='.$babDB->quote(get_class($this->request));
			}
			
			if (isset($this->createdOn)) {
			    $where[] = 'm.createdOn<='.$babDB->quote($this->createdOn);
			}
			
			if($where)
			{
				$query .= ' WHERE '.implode(' AND ', $where);
			}
			
			$query .= ' ORDER BY m.createdOn DESC, m.id DESC';
			

			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}

}
