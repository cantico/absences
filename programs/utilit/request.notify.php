<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/










class absences_notifyRequestApproversCls
{
	public $message;
	public $commenttxt;
	public $comment;
	public $title_message;

	/**
	 * 
	 * @var array
	 */
	 private $requests;
	
	/**
	 * 
	 * @var array
	 */
	private $fields;

	public function __construct(Array $requests)
	{
		$this->requests = $requests;

		$this->fromuser = absences_translate("User");
		$this->commenttxt = absences_translate("Comment");
		$this->t_approbation_page = absences_translate("Approbations page");
		$this->approburl = $GLOBALS['babUrlScript'].'?tg=approb';
	}
	
	
	public function getRequest()
	{
		return $this->requests;
	}
	
	
	public function getnextrequest()
	{		
		if (list(,$request) = each($this->requests))
		{
			/* @var $request absences_Request */
			
			if ($request->modifiedOn() !== $request->createdOn())
			{
				$this->title_message = bab_toHtml(sprintf(absences_translate("The %s has been modified"), $request->getTitle()));
			}
			else
			{
				$this->title_message = bab_toHtml(sprintf(absences_translate("A %s is waiting to be validated"), $request->getTitle()));
			}
			
			$this->username = bab_toHtml(bab_getUserName($request->id_user));
			$this->comment = bab_toHtml($request->comment, BAB_HTML_ENTITIES | BAB_HTML_BR);
			$this->fields = $request->getNotifyFields();
			$this->alert = $request->approbAlert();
			
			return true;
		}
		
		return false;
	}
	
	
	public function getnextfield()
	{
		if ($arr = each($this->fields))
		{
			$this->label = bab_toHtml($arr[0]);
			$this->value = bab_toHtml($arr[1]);
			return true;
		}
		
		return false;
	}
}



/**
 * Notifier les approbateurs
 * 
 */
function absences_notifyRequestApprovers()
{
	$mail = bab_mail();
	if( $mail == false )
	{
		return;
	}
	
	
	$list = absences_getRequestsApprovers();
	
	foreach($list as $email)
	{
	    $mail->clearTo();
	    
		foreach($email['approvers'] as $id_user)
		{
			$mail->mailTo(bab_getUserEmail($id_user), bab_getUserName($id_user));
		}
		
		$mail->mailFrom($GLOBALS['babAdminEmail'], $GLOBALS['babAdminName']);
		$mail->mailSubject(absences_translate("Vacation request is waiting to be validated", "Vacations requests are waiting to be validated", count($email['requests'])));
		
		$content = new absences_notifyRequestApproversCls($email['requests']);
		
		$message = $mail->mailTemplate(absences_addon()->printTemplate($content, "mailinfo.html", "newrequests"));
		$mail->mailBody($message, "html");
		
		$message = absences_addon()->printTemplate($content, "mailinfo.html", "newrequeststxt");
		$mail->mailAltBody($message);
		
		if ($mail->send())
		{
			$requests = $content->getRequest();
			foreach($requests as $request)
			{
				$request->setNotified();
			}
		}

	}
}




















class absences_notifyRequestAuthorCls
{
	public $message;
	public $from;
	public $site;
	public $bview;
	public $by;
	public $reason;
	public $reasontxt;

	private $res;

	function __construct(absences_Request $request, $message)
	{
		$this->message = $message;
		
		$this->reasontxt = absences_translate("Additional information");
		$this->reason = bab_toHtml($request->comment2, BAB_HTML_BR | BAB_HTML_ENTITIES);
		if( $request->status == 'N')
		{
			$this->by = absences_translate("By");
			$this->username = bab_getUserName($request->id_approver);
			$this->bview = true;
		}
		else
		{
			$this->bview = false;
		}
		
		$this->res = $request->getNotifyFields();
	}
	
	
	public function getnext()
	{
		if ($arr = each($this->res))
		{
			$this->label = bab_toHtml($arr[0]);
			$this->value = bab_toHtml($arr[1]);
			return true;
		}
		
		return false;
	}
}



/**
 * Notify author of a request about approval
 * @param	array | Iterator 	$requests 	<absences_Request>
 * @param	string				$subject	Email subject
 * @param   string				$message	message content introduction for each request
 * @param	int					$id_user	Recipient (author)
 */
function absences_notifyRequestAuthor($requests, $subject, $message, $id_user)
{
	global $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail;

	$mail = bab_mail();
	if( $mail == false )
		return;

	$mail->mailTo(bab_getUserEmail($id_user), bab_getUserName($id_user));

	$mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);
	$mail->mailSubject($subject);
	
	$message = '';
	$messagetxt = '';

	foreach($requests as $request)
	{
		$tempa = new absences_notifyRequestAuthorCls($request, $message);
		$message .= absences_addon()->printTemplate($tempa, "mailinfo.html", "infovacation");
		$messagetxt .= absences_addon()->printTemplate($tempa, "mailinfo.html", "infovacationtxt");
	}
	$mail->mailBody($mail->mailTemplate($message), "html");
	$mail->mailAltBody($messagetxt);

	$mail->send();
}








class absences_notifyEntryOwnerEmailsCls
{
	public $message;
	public $from;
	public $site;
	public $bview;
	public $by;
	public $reason;
	public $reasontxt;

	private $res;

	function __construct(absences_Entry $entry, $subject)
	{
		$this->message = $subject;

		$this->res = $entry->getNotifyFields();
	}


	public function getnext()
	{
		if ($arr = each($this->res))
		{
			$this->label = bab_toHtml($arr[0]);
			$this->value = bab_toHtml($arr[1]);
			return true;
		}

		return false;
	}
}





/**
 * 
 * @param Array | absences_EntryIterator $entries <absences_Entry> 
 * @param Array $emails <string>
 */
function absences_notifyEntryOwnerEmails($entries, Array $emails)
{
	global $babBody, $babDB, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail;
	
	$mail = bab_mail();
	if( $mail == false )
		return;
	
	$i = 0;
	foreach($emails as $email)
	{
		$email = trim($email);
		if (!empty($email))
		{
			$mail->mailTo($email);
			$i++;
		}
	}
	
	if ($i === 0)
	{
		return; // no recipient
	}
	
	
	$mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);
	if (1 === count($entries))
	{
		$entry = reset($entries);
		$subject = sprintf(absences_translate("An absence has been confirmed for %s"), $entry->getUserName());
	} else {
		$subject = absences_translate("absences request set has been confirmed");
	}
	$mail->mailSubject($subject);
	
	$message = '';
	$messagetxt = '';
	foreach($entries as $entry)
	{
		$subject = sprintf(absences_translate("An absence has been confirmed for %s"), $entry->getUserName());
		$tempa = new absences_notifyEntryOwnerEmailsCls($entry, $subject);
		$message .= absences_addon()->printTemplate($tempa, "mailinfo.html", "vacationemails");
		$messagetxt .= absences_addon()->printTemplate($tempa, "mailinfo.html", "vacationemailstxt");
	}
	
	$mail->mailBody($mail->mailTemplate($message), "html");
	$mail->mailAltBody($messagetxt);
	
	$mail->send();
}
