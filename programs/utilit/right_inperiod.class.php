<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';

/**
 * Absence InPeriod
 * 
 * @property 	int		$id_right
 * @property	string	$period_start
 * @property	string	$period_end
 * @property	int		$right_inperiod
 * @property	string	$uuid
 * 
 */
class absences_RightInPeriod extends absences_Record 
{
	private $right;
	
	private $id_right;
	
	public static function getFromId($id)
	{
		$rightinperiod = new absences_RightInPeriod;
		$rightinperiod->id = $id;
		return $rightinperiod;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			
			$query = 'SELECT * FROM absences_rights_inperiod WHERE ';
			
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}
			
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
		
		return $this->row;
	}
	
	/**
	 * 
	 * @param absences_Right $right
	 * @return absences_RightInPeriod
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
		
		return $this->right;
	}
	
	
	
	public function save()
	{
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM absences_rights_inperiod WHERE uuid='.$babDB->quote($this->uuid));
		if (0 === $babDB->db_num_rows($res))
		{
			$this->insert();
		} else {
			
			$this->update();
		}
	}
	
	
	public function insert()
	{
		global $babDB;
		require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
		
		$right = $this->getRight();
		$this->id_right = $right->id;
		
		$babDB->db_query('
			INSERT INTO absences_rights_inperiod 
				(
					id_right,
					period_start,
					period_end,
					right_inperiod,
					uuid
				)
			VALUES 
				(
					'.$babDB->quote($this->id_right).',
					'.$babDB->quote($this->period_start).',
					'.$babDB->quote($this->period_end).',
					'.$babDB->quote($this->right_inperiod).',
					'.$babDB->quote($this->uuid).'
				)
		');
	}
	
	
	public function update()
	{
		global $babDB;
		
		if (empty($this->uuid))
		{
			throw new Exception('missing absences_rights_inperiod.uuid');
		}
		
		$babDB->db_query('UPDATE absences_rights_inperiod SET 
			period_start	='.$babDB->quote($this->period_start).',
			period_end		='.$babDB->quote($this->period_end).',
			right_inperiod	='.$babDB->quote($this->right_inperiod).' 
		WHERE uuid='.$babDB->quote($this->uuid));
	}
}
