<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/request.ui.php';


class absences_deleteVacationRequestCls extends absences_vacationRequestDetail
{

    public function __construct($id, $manager_view)
    {
        parent::__construct($id);

        $this->id_entry = (int) $id;

        $this->t_delete_folder = absences_translate("Delete all periods from the recurring request");
        $this->t_deleteconfirm = absences_translate("Do you really want to delete the vacation request ?");

        $this->initDelete($manager_view);
    }

}


class absences_deleteCetDepositRequestCls extends absences_CetDepositRequestDetail
{

    public function __construct($id, $manager_view)
    {
        parent::__construct($id);

        $this->id_deposit = (int) $id;

        $this->t_deleteconfirm = absences_translate("Do you really want to delete the time saving account deposit ?");

        $this->initDelete($manager_view);
    }

}


class absences_deleteWpRecoveryRequestCls extends absences_WpRecoveryDetail
{

    public function __construct($id, $manager_view)
    {
        parent::__construct($id);

        $this->id_recovery = (int) $id;

        $this->t_deleteconfirm = absences_translate("Do you really want to delete the work period recover request ?");

        $this->initDelete($manager_view);
    }

}