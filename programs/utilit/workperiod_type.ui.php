<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

bab_Widgets()->includePhpClass('Widget_Form');

class absences_WorkperiodTypeEditor extends Widget_Form
{
	/**
	 * @var absences_WorkperiodType
	 */
	private $type;
	
	public function __construct(absences_WorkperiodType $type = null)
	{
		$W = bab_Widgets();
		$this->type = $type;
	
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
	
		$this->setName('type');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
	
		$this->setCanvasOptions($this->Options()->width(70,'em'));
	
	
		$this->addFields();
		$this->addButtons();
		$this->setSelfPageHiddenFields();
		
		if (isset($type))
		{
			$this->setHiddenValue('type[id]', $type->id);
			$this->setValues(array('type' => $type->getRow()));
		}
	}
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
	
		$this->addItem(
			$W->LabelledWidget(
				absences_translate('Name'), 
				$W->LineEdit()->setMandatory(true, absences_translate('The name is mandatory'))->setSize(60)->setMaxSize(100),
				'name'
			)
		);
		
		$this->addItem($this->quantity());
	}
	
	
	/**
	 * quantity & quantity_unit
	 * @return Widget_FlowLayout
	 */
	protected function quantity()
	{
		$W = bab_Widgets();
	
		$lineedit = $W->LineEdit()->setSize(2)->setMaxSize(5)->setMandatory(true, absences_translate('The quantity is mandatory'))->setName('quantity');
		$select = $W->Select()->setName('quantity_unit')
		->addOption('D', absences_translate('Day(s)'))
		->addOption('H', absences_translate('Hour(s)'));
	
		return $W->VBoxItems(
				$W->Label(absences_translate('Quantity'))->setAssociatedWidget($lineedit),
				$W->FlowItems($lineedit, $select)
		)->setVerticalSpacing(.2,'em');
	}
	
	
	
	
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
		
		if (isset($this->type))
		{
			$button->addItem(
					$W->SubmitButton()
					->setConfirmationMessage(absences_translate('Do you really want to delete the type?'))
					->setName('delete')->setLabel(absences_translate('Delete'))
			);
		}
	
		$this->addItem($button);
	}
}