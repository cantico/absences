<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/xls.class.php';






class absences_AgentXls extends absences_Xls
{
	/**
	 * 
	 * @var absences_Agent
	 */
	protected $agent;
	
	public function __construct(absences_Agent $agent)
	{
		parent::__construct($agent->getName());
		$this->agent = $agent;
		
		$this->addRightsWorksheet();
		$this->addRequestsWorksheet();
		
		$this->workbook->close();
	}
	
	
	/**
	 * 
	 */
	protected function addRightsWorksheet()
	{
		$ee = bab_functionality::get('ExcelExport');
		/* @var $ee Func_ExcelExport */
		
		$worksheet = $this->workbook->addWorksheet(absences_translate('Vacation rights'));
		
		$col = 0;
		$worksheet->setColumn($col, $col++, 30);	// description
		$worksheet->setColumn($col, $col++, 25);	// type
		$worksheet->setColumn($col, $col++, 15);	// quantity
		$worksheet->setColumn($col, $col++, 15);	// consumed
		$worksheet->setColumn($col, $col++, 15);	// waiting
		$worksheet->setColumn($col, $col++, 15);	// balance
		$worksheet->setColumn($col, $col++, 15);	// begin
		$worksheet->setColumn($col, $col++, 15);	// end
		$worksheet->setColumn($col, $col++, 10);	// Accessible
		
		$header = $this->header;
		
		
		$row =0;
		$col =0;
		$worksheet->write($row, $col++, $this->agent->getName());
		$row++;
		
		$col =0;
		$worksheet->write($row, $col++, absences_translate('Description')		, $header);
		$worksheet->write($row, $col++, absences_translate('Type')				, $header);	
		$worksheet->write($row, $col++, absences_translate('Initial quantity')	, $header);
		$worksheet->write($row, $col++, absences_translate('Consumed')			, $header);
		$worksheet->write($row, $col++, absences_translate('Waiting approval')	, $header);
		$worksheet->write($row, $col++, absences_translate('Balance')			, $header);
		$worksheet->write($row, $col++, absences_translate('Begin date')		, $header);
		$worksheet->write($row, $col++, absences_translate('End date')			, $header);
		$worksheet->write($row, $col++, absences_translate('Accessible')        , $header);
		
		$row++;
		foreach($this->agent->getAgentRightManagerIterator() as $agentRight) {
			$col =0;
			
			/*@var $agentRight absences_AgentRight */
		
			$right = $agentRight->getRight();
			
			$accessible = $agentRight->isAccessibleByValidityPeriod()
			&& $agentRight->isAccessibleOnPeriod(time(), time())
			&& $agentRight->isAcessibleByDirectoryEntry()
			&& $right->isAccessibleIfFixed();

			$worksheet->writeString($row, $col++, $right->description);
			$worksheet->writeString($row, $col++, $right->getType()->name);
			$worksheet->writeNumber($row, $col++, $agentRight->getQuantity(), $this->quantity);
			$worksheet->writeNumber($row, $col++, $agentRight->getConfirmedQuantity(), $this->quantity);
			$worksheet->writeNumber($row, $col++, $agentRight->getWaitingQuantity(), $this->quantity);
			$worksheet->writeNumber($row, $col++, $agentRight->getBalance(), $this->quantity);
			$worksheet->write($row, $col++, $ee->getExcelDate(bab_mktime($right->date_begin)), $this->date);
			$worksheet->write($row, $col++, $ee->getExcelDate(bab_mktime($right->date_end)), $this->date);
            $worksheet->write($row, $col++, $accessible ? '1':'0');

			$row++;
		}
		
		
	}
	
	protected function addRequestsWorksheet()
	{
		$ee = bab_functionality::get('ExcelExport');
		/*@var $ee Func_ExcelExport */
		
		$worksheet = $this->workbook->addWorksheet(absences_translate('User requests'));
		
		$row =0;
		$col =0;
		$worksheet->write($row, $col++, $this->agent->getName());
		$row++;
		
		$col = 0;
		$worksheet->setColumn($col, $col++, 20);	// createdOn
		$worksheet->setColumn($col, $col++, 40);	// type
		$worksheet->setColumn($col, $col++, 20);	// begin
		$worksheet->setColumn($col, $col++, 20);	// end
		$worksheet->setColumn($col, $col++, 50);	// status
		
		
		$header = $this->header;
		
		
		$col =0;
		$worksheet->write($row, $col++, absences_translate('Created on')		, $header);
		$worksheet->write($row, $col++, absences_translate('Type')				, $header);
		$worksheet->write($row, $col++, absences_translate('Start date')		, $header);
		$worksheet->write($row, $col++, absences_translate('End date')			, $header);
		$worksheet->write($row, $col++, absences_translate('Status')			, $header);
		
		$row++;
		foreach($this->agent->getRequestIterator() as $request) {
			$col =0;
		
			/*@var $request absences_Request */
		
			$worksheet->write($row, $col++, $ee->getExcelDate(bab_mktime($request->createdOn())), $this->datetime);
			$worksheet->writeString($row, $col++, $request->getRequestType());
			
			if ($request instanceof absences_CetDepositRequest)
			{
				$worksheet->writeString($row, $col++, '');
				$worksheet->writeString($row, $col++, '');
				
			} else {
				$worksheet->write($row, $col++, $ee->getExcelDate(bab_mktime($request->date_begin)), $this->datetime);
				$worksheet->write($row, $col++, $ee->getExcelDate(bab_mktime($request->date_end)), $this->datetime);
			}
			
			
			$worksheet->writeString($row, $col++, $request->getStatusStr());

			$row++;
		}
		
	}
}
