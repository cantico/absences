<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/entry.class.php';

/**
 * @property int	$id_entry
 * @property int	$id_right
 * @property float 	$quantity
 * @property string $date_begin
 * @property string $date_end
 * 
 */
class absences_EntryElem extends absences_Record 
{
	/**
	 * @var absences_Right
	 */
	private $right;
	
	
	/**
	 * 
	 * @var absences_Entry
	 */
	private $entry;
	
	
	
	/**
	 * @return absences_EntryElem
	 */
	public static function getById($id_elem)
	{
		$entry = new absences_EntryElem;
		$entry->id = $id_elem;
	
		return $entry;
	}
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load entry elem, missing entry elem id');
			}
	
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_entries_elem WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	
	
	/**
	 *
	 * @param absences_Right $right
	 * @return absences_EntryElem
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			require_once dirname(__FILE__).'/right.class.php';
			
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
	
		return $this->right;
	}
	
	
	/**
	 *
	 * @param absences_Entry $entry
	 * @return absences_EntryElem
	 */
	public function setEntry(absences_Entry $entry)
	{
		$this->entry = $entry;
		return $this;
	}
	
	
	/**
	 * @return absences_Entry
	 */
	public function getEntry()
	{
		if (!isset($this->entry))
		{
			$row = $this->getRow();
			$this->entry = absences_Entry::getById($row['id_entry']);
		}
	
		return $this->entry;
	}
	
	
	/**
	 * @return absences_AgentRight
	 */
	public function getAgentRight()
	{
	    $entry = $this->getEntry();
	    $agent = $entry->getAgent();
	    
	    $I = $agent->getAgentRightManagerIterator();
	    
	    foreach($I as $agentRight) {
	        if ($agentRight->id_right === $this->id_right) {
	            return $agentRight;
	        }
	    }
	    
	    return null;
	}
	
	
	
	/**
	 * Check validity before saving an element
	 * @throws UnexpectedValueException
	 * 
	 * @return bool
	 */
	public function checkValidity()
	{
		if (!isset($this->id_right) || $this->id_right <= 0)
		{
			throw new UnexpectedValueException('Missing id_right');
		}
		
		$quantity = (int) round(100 * $this->quantity);
		
		if (!$quantity && !isset($this->id))
		{
		    
		    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		    bab_debug_print_backtrace();
		    
		    $message = sprintf(
		        absences_translate('An absence cannot be created because of a missing quantity in right "%s"'), 
		        $this->getRight()->description
		    );
			throw new UnexpectedValueException($message);
		}
		
		return true;
	}
	
	
	/**
	 * For previsional requests, check if requested quantity is available
	 * @return bool
	 */
	public function isQuantityAvailable()
	{
	    $agentRight = $this->getAgentRight();
	    $bookableQuantity = $agentRight->getAvailableQuantity() - $agentRight->getWaitingQuantity();
	    
	    if ($bookableQuantity >= (float) $this->quantity) {
	        return true;
	    }
	    
	    return false;
	}
	
	
	
	/**
	 *
	 * @param string $message	Generated message
	 * @param string $comment	Author comment
	 */
	public function addMovement($message, $comment = '')
	{
		require_once dirname(__FILE__).'/movement.class.php';
	
		$movement = new absences_Movement();
		$movement->message = $message;
		$movement->comment = $comment;
		$movement->setRequest($this->getEntry());
		$movement->setAgent($this->getEntry()->getAgent());
		$movement->setRight($this->getRight());
		$movement->save();
	}
	
	
	
	/**
	 * Save element (insert or update or delete)
	 */
	public function save()
	{
		global $babDB;
		
		if (isset($this->id))
		{
			$quantity = (int) round(100 * $this->quantity);
			
			if (0 === $quantity)
			{
				// if quantity has been set to 0, the element must be deleted

				$babDB->db_query("DELETE FROM absences_entries_elem id=".$babDB->quote($this->id));
				
			} else {
			
			
				$babDB->db_query("
					UPDATE absences_entries_elem 
					SET 
						quantity=".$babDB->quote($this->quantity).",
				        date_begin=".$babDB->quote($this->date_begin).",
				        date_end=".$babDB->quote($this->date_end)." 
					WHERE 
						id=".$babDB->quote($this->id)
				);
			
			}
			
			
		} else {
			
			if (isset($this->id_entry))
			{
				$id_entry = $this->id_entry;
			} else {
				$entry = $this->getEntry();
				$id_entry = $entry->id;
			}
			
			$babDB->db_query("
				INSERT INTO absences_entries_elem 
					(id_entry, id_right, quantity, date_begin, date_end)
				VALUES 
				(
					" .$babDB->quote($id_entry). ",
					" .$babDB->quote($this->id_right). ",
					" .$babDB->quote($this->quantity). ",
			        " .$babDB->quote($this->date_begin). ",
			        " .$babDB->quote($this->date_end). "
				)
			");
			
			$this->id = $babDB->db_insert_id();
		}
	}
	
	
	
	public function delete()
	{
	    global $babDB;
	    
	    if (!$this->id) {
	        return false;
	    }
	    
	    $babDB->db_query("DELETE FROM absences_entries_elem WHERE id=".$babDB->quote($this->id));
	}
	
	
	
	
	/**
	 * Trouver la quantitee totale de la demande disponible sur la periode
	 * la quantitee est dans l'unite du droit
	 * si les dates de l'element sont comprises dans l'interval, la quantite 
	 * de l'element est utilises
	 * si les dates de l'element sont a cheval sur la periode demandee
	 * on utlise les heures travailles de l'utilisateur
	 *
	 * @param string $begin		Datetime
	 * @param string $end		Datetime
	 *
	 * @return float (days or hours)
	 */
	public function getQuantityBetween($begin, $end)
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	    
	    if ($begin <= $this->date_begin && $this->date_end <= $end) {
	        return (float) $this->quantity;
	    }
	    
	    $quantity_unit = $this->getRight()->quantity_unit;
	    
	    switch($quantity_unit) {
	        case 'D': $seconds = 86400; break;
	        case 'H': $seconds = 3600; break;
	    }
	
	    $test_begin = ($begin > $this->date_begin) ? $begin : $this->date_begin;
	    $test_end 	= ($end   < $this->date_end  ) ? $end   : $this->date_end;
	    
	    $entry = $this->getEntry();
	
	    list(, $index) = absences_getHalfDaysIndex($entry->id_user, BAB_DateTime::fromIsoDateTime($test_begin), BAB_DateTime::fromIsoDateTime($test_end), true);
	
	    $total = 0.0;
	    foreach($index as $period)
	    {
	        /*@var $period bab_CalendarPeriod */
	        $total += ($period->getDuration() / $seconds);
	    }
	   
	    return $total;
	}
	
	
	/**
	 * Trouver la quantitee totale de la demande disponible sur la periode
	 * la quantitee est dans l'unite du droit
	 * si les dates de l'element sont comprises dans l'interval, la quantite
	 * de l'element est utilises
	 * si les dates de l'element sont a cheval sur la periode demandee
	 * on utlise les heures travailles qui etait en vigeur au moment de la creation 
	 * de la demande
	 *
	 * @param string $begin		Datetime
	 * @param string $end		Datetime
	 *
	 * @return float (days or hours)
	 */
	public function getPlannedQuantityBetween($begin, $end)
	{
	    if ($begin <= $this->date_begin && $this->date_end <= $end) {
	        return (float) $this->quantity;
	    }
	    
	    $entry = $this->getEntry();
	   
	    // ne pas sortir des bornes de l'element pour ne pas 
	    // capter une quantite sur un autre element de la meme demande
	    $begin = $begin > $this->date_begin ? $begin : $this->date_begin;
	    $end = $end < $this->date_end ? $end : $this->date_end;

	    switch($this->getRight()->quantity_unit) {
	        case 'D': return $entry->getPlannedDurationDays($begin, $end);
	        case 'H': return $entry->getPlannedDurationHours($begin, $end);
	    }
	    
	    return 0;
	}
	
	
	/**
	 * @return float
	 */
	public function getDays()
	{
	    $right = $this->getRight();
	    if ('D' !== $right->quantity_unit) {
	        return 0;
	    }
	     
	    return (float) $this->quantity;
	}
	
	/**
	 * @return float
	 */
	public function getHours()
	{
	    $right = $this->getRight();
	    if ('H' !== $right->quantity_unit) {
	        return 0;
	    }
	    
	    return (float) $this->quantity;
	}
}