<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/right.class.php';


class absences_sync_items
{
	
	/**
	 * Derniere modification
	 * @return string
	 */
	public function getLastModifiedDate()
	{
		global $babDB;
		
		$res = $babDB->db_query('SELECT date_entry FROM absences_rights ORDER BY date_entry DESC LIMIT 0,1');
		
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			return $arr['date_entry'];
		}
		
		return date('Y-m-d H:i:s');
	}
	
	/**
	 * get data to export for rights
	 * @return array
	 */
	public function getData()
	{
		global $babDB;
		$return = array();
		
		$I = new absences_RightIterator();
		$I->sync_status = absences_Right::SYNC_SERVER;
		
		foreach($I as $right)
		{
			/*@var $right absences_Right */
			
			$inperiod = array();
			$res = $babDB->db_query('SELECT * FROM absences_rights_inperiod WHERE id_right='.$babDB->quote($right->id));
			while ($row = $babDB->db_fetch_assoc($res))
			{
				$inperiod[] = $row;
			}
			
			$rgroup_row = null;
			$rgroup = $right->getRgroup();
			if (isset($rgroup))
			{
				$rgroup_row = $rgroup->getRow();
			}
			
			if (isset($rgroup))
			{
				$rgroup->getRow();
			}
			
			
			$rules_type_row = null;
			$rules_type = $right->getRightRule()->getType();
			
			if (isset($rules_type))
			{
				$rules_type_row = $rules_type->getRow();
			}
			
			
			
			$return[] = array(
				'absences_rights' 			=> $right->getRow(),
				'absences_rights_rules' 	=> $right->getRightRule()->getRow(),
				'rules_type'			 	=> $rules_type_row,
				'absences_rights_cet' 		=> $right->getRightCet()->getRow(),
				'absences_rights_inperiod' 	=> $inperiod,
				'absences_types'			=> $right->getType()->getRow(),
				'absences_rgroup'			=> $rgroup_row 
			);
		}
		
		return $return;
	}
}