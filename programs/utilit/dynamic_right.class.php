<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/





require_once dirname(__FILE__).'/record.class.php';

/**
 * @property int	$id_user_right
 * @property float 	$quantity
 * @property int	$id_entry
 * @property string $createdOn
 *
 */
class absences_DynamicRight extends absences_Record
{
	
	
	/**
	 *
	 * @var absences_Entry
	 */
	private $entry;
	
	
	/**
	 * @return absences_DynamicRight
	 */
	public static function getById($id)
	{
		$dright = new absences_DynamicRight;
		$dright->id = $id;
	
		return $dright;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load dynamic right, missing id');
			}
	
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_dynamic_rights WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
	
		return $this->row;
	}
	
	/**
	 * Get quantity
	 * @return number
	 */
	public function getQuantity()
	{
		return (float) $this->quantity;
	}
	
	
	/**
	 *
	 * @param absences_AgentRight $agentright
	 * @return absences_DynamicRight
	 */
	public function setAgentRight(absences_AgentRight $agentright)
	{
		$this->agentright = $agentright;
		return $this;
	}
	
	
	/**
	 * @return absences_AgentRight
	 */
	public function getAgentRight()
	{
		if (!isset($this->agentright))
		{
			require_once dirname(__FILE__).'/agent_right.class.php';
	
			$row = $this->getRow();
			$this->agentright = absences_AgentRight::getById($row['id_user_right']);
		}
	
		return $this->agentright;
	}
	
	
	/**
	 *
	 * @param absences_Entry $entry
	 * @return absences_EntryElem
	 */
	public function setEntry(absences_Entry $entry)
	{
		$this->entry = $entry;
		return $this;
	}
	
	
	/**
	 * @return absences_Entry
	 */
	public function getEntry()
	{
		if (!isset($this->entry))
		{
			$row = $this->getRow();
			$this->entry = absences_Entry::getById($row['id_entry']);
		}
	
		return $this->entry;
	}
	
	
	
	
	/**
	 * Save element (insert or update or delete)
	 */
	public function save()
	{
		global $babDB;
	
		if (isset($this->id))
		{
			$quantity = (int) round(100 * $this->quantity);
	
			if (0 === $quantity)
			{
				// if quantity has been set to 0, the element must be deleted
	
				$babDB->db_query("DELETE FROM absences_dynamic_rights WHERE id=".$babDB->quote($this->id));
	
			} else {
	
	
				$babDB->db_query("
					UPDATE absences_dynamic_rights 
					SET
						id_entry=".$babDB->quote($this->id_entry).", 
						quantity=".$babDB->quote($this->quantity)." 
					WHERE
						id=".$babDB->quote($this->id)
				);
	
			}
	
	
		} else {
	
			if (isset($this->id_entry))
			{
				$id_entry = $this->id_entry;
			} else {
				$entry = $this->getEntry();
				$id_entry = $entry->id;
			}
	
			$babDB->db_query("
				INSERT INTO absences_dynamic_rights 
					(id_entry, id_user_right, quantity, createdOn)
				VALUES
					(
					" .$babDB->quote($id_entry). ",
					" .$babDB->quote($this->id_user_right). ",
					" .$babDB->quote($this->quantity). ",
					" .$babDB->quote($this->createdOn). "
			)
					");
	
			$this->id = $babDB->db_insert_id();
		}
		
		
		$agentRight = $this->getAgentRight();
		$agent = $agentRight->getAgent();
		$right = $agentRight->getRight();
		
		$agentRight->addMovement(sprintf(absences_translate('The quantity for user %s on right %s has been changed because of the rule on consumed quantity : %s'), 
				$agent->getName(), $right->description, absences_quantity($this->quantity, $right->quantity_unit)));
	}
}



class absences_DynamicRightIterator extends absences_Iterator
{
	protected $agentRight;
	
	/**
	 * date time
	 * @var string
	 */
	public $createdOn;
	
	
	public function setAgentRight(absences_AgentRight $agentRight)
	{
		$this->agentRight = $agentRight;
	}
	
	
	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = "SELECT 
				dr.* 
			FROM
				absences_dynamic_rights dr 
			WHERE dr.id_user_right=".$babDB->quote($this->agentRight->id)." 
			";
			
			if (isset($this->createdOn)) {
			    $req .= ' AND dr.createdOn<='.$babDB->quote($this->createdOn);
			}
			
			$req .= ' ORDER BY dr.createdOn, dr.id ';
			
			// On right modification, the dynamic right rows are generated with the same createdOn date
	
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
	
	
	public function getObject($data)
	{
		$dynRight = absences_DynamicRight::getById($data['id']);
		$dynRight->setRow($data);
		$dynRight->setAgentRight($this->agentRight);
	
		return $dynRight;
	}
	
	
}