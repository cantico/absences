<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';

/**
 * 
 * @property string		$name
 * @property string		$quantity_unit
 * @property int		$recover
 * @property int		$sortkey
 */
class absences_Rgroup extends absences_Record implements absences_RightSort 
{
	
	
	public function __construct($id)
	{
		$this->id = $id;
	}
	
	
	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_rgroup WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}
		
		return $this->row;
	}
	
	
	/**
	 * Method used with bab_Sort
	 */
	public function getSortKey()
	{
		return $this->sortkey;
	}
	
	public function setSortKey($i)
	{
		global $babDB;
		$babDB->db_query('UPDATE absences_rgroup SET sortkey='.$babDB->quote($i).' WHERE id='.$babDB->quote($this->id));
	}	
	
	public function getSortLabel()
	{
		return $this->name;
	}
	
	
	public function getIconClassName()
	{
		bab_functionality::includeOriginal('Icons');
		return Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE;
	}
}