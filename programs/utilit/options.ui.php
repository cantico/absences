<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
bab_Widgets()->includePhpClass('Widget_Form');


class absences_OptionsEditor extends Widget_Form 
{
	private $I;
	
	public function __construct()
	{
		$W = bab_Widgets();
	
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
	
	
		$this->setName('options');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
	
		$this->setCanvasOptions($this->Options()->width(70,'em'));
	
		$this->addFields();
		$this->loadFormValues();
	
		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}
	
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		$this->addItem($W->Acl()->setTitle(absences_translate('Vacations mangers'))->setName('managers'));
		
		if (null !== bab_OCgetPrimaryOcId())
		{
			$this->addItem($this->id_chart());
		}
	
		$this->addItem($this->boolfield('chart_superiors_create_request', absences_translate("Allow delegated managers to create vacation requests for users in chart")));
		$this->addItem($this->boolfield('chart_superiors_set_rights', absences_translate("Allow delegated managers to update vacations rights for users in chart")));
		$this->addItem($this->boolfield('chart_superiors_user_edit', absences_translate("Allow delegated managers to update users settings for users in chart")));
		$this->addItem($this->boolfield('allow_mismatch', absences_translate("Allow users to create vacation requests with mismatch in total number of days")));
		$this->addItem($this->boolfield('workperiod_recover_request', absences_translate("Allow users to create workperiods request recovery")));
		$this->addItem($this->boolfield('display_personal_history', absences_translate("Users can access to the personal history")));
		$this->addItem($this->boolfield('modify_confirmed', absences_translate("Users can edit or delete confirmed requests")));
		$this->addItem($this->boolfield('modify_waiting', absences_translate("Users can edit or delete waiting requests")));
		$this->addItem($this->boolfield('email_manager_ondelete', absences_translate('Notify managers when a vacation request is deleted')));
		$this->addItem($this->boolfield('approb_email_defer', absences_translate('Use defered approbation email (daily notification)'),
		        absences_translate('The notification is sent every day between 12:00 and 13:00 to all approvers with waiting items')));
		$this->addItem($this->boolfield('user_add_email', absences_translate('Allow users to notify emails about the accepted vacations requests')));

		
		$this->addItem($W->Acl()->setTitle(absences_translate('Who can view the public calendar?'))->setName('public_planning'));		
		
		$this->addItem($this->boolfield('appliquant_email', absences_translate('Send an email to appliquant when a request is confirmed')));
		
		$this->addItem($W->LabelledWidget(
				absences_translate('Alert for waiting request not modified in more than'), 
				$W->LineEdit()->setSize(3)->setMaxSize(3),
				'approb_alert',
				absences_translate('Requests exceeding this period will be displayed in red in the email sent to approvers and displayed as bold in the manager waiting requests list, no additional mail are sent with this parameter'),
				absences_translate('days')
		));
		
		$this->addItem($this->boolfield('auto_approval', absences_translate('Auto approve if current user is responsible of the first approval step')));
		
		$this->addItem($W->LabelledWidget(
				absences_translate('Auto confirm waiting request not modified in more than'),
				$W->LineEdit()->setSize(3)->setMaxSize(3),
				'auto_confirm',
				null,
				absences_translate('days')
		));
		
		$this->addItem($W->LabelledWidget(
				absences_translate('End of avaibility for the recuperation in days after the working periode'),
				$W->LineEdit()->setSize(3)->setMaxSize(3),
				'end_recup',
				null,
				absences_translate('days')
		));
		
		$this->addItem($W->LabelledWidget(
				absences_translate("Anticipation delay for the recovery period deposit"),
				$W->LineEdit()->setSize(3)->setMaxSize(3),
				'delay_recovery',
				null,
				absences_translate('days')
		));
		
		$months = array(
			1 => absences_translate('January'),
			2 => absences_translate('February'),
			3 => absences_translate('March'),
			4 => absences_translate('April'),
			5 => absences_translate('May'),
			6 => absences_translate('June'),
			7 => absences_translate('July'),
			8 => absences_translate('August'),
			9 => absences_translate('September'),
			10 => absences_translate('October'),
			11 => absences_translate('November'),
			12 => absences_translate('December')
		);

		$this->addItem(
			$W->VBoxItems(
				$W->Label(absences_translate('The day when the periode is changing') . ' :')->addClass('widget-description'),
				$W->FlowItems(
					$W->LineEdit()->setSize(2)->setMaxSize(2)->setName('archivage_day'),
					$W->Select()->setOptions($months)->setName('archivage_month')
				)
			)
		);
		
		$this->addItem($this->boolfield('entity_planning', absences_translate("Allow chart members to access the entity planning of their main entity")));
		$this->addItem($this->boolfield('entity_planning_display_types', absences_translate("Display types and legend for all users in the entity planning, instead of entity responsible and managers only")));
		$this->addItem($this->boolfield('maintenance', absences_translate("Use mainteance mode, only managers can create or modify requests")));
		$this->addItem($this->organization_sync());
		$this->addItem($this->boolfield('sync_server', absences_translate("This server act as a synchronization server")));
		
		$client = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$client->addItem($W->LabelledWidget(
				absences_translate('Shared rights server url'),
				$W->LineEdit()->setSize(40)->setMaxSize(255),
				'sync_url'
		));
		
		$client->addItem($W->LabelledWidget(
				absences_translate('Nickname'),
				$W->LineEdit()->setSize(20)->setMaxSize(255),
				'sync_nickname'
		));
		
		$client->addItem($W->LabelledWidget(
				absences_translate('Password'),
				$W->LineEdit()->setSize(20)->setMaxSize(255),
				'sync_password'
		));
		
		$this->addItem($W->Section(absences_translate('This server act as a synchronization client'), $client)->setFoldable(true, true));
	}
	
	
	
	protected function organization_sync()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	    $W = bab_Widgets();
	    
	    $initUrl = bab_url::get_request('tg');
	    $initUrl->idx = 'init';
	    
	    return $W->FlowItems(
	        $this->boolfield('organization_sync', absences_translate("Synchronize organizations with directory entry.")),
	        $W->Link(absences_translate('Initialize all personnel members'), $initUrl->toString())->setOpenMode(Widget_Link::OPEN_POPUP)
	    )->setVerticalAlign('middle')->setHorizontalSpacing(.5, 'em');
	}
	
	
	
	protected function id_chart()
	{
		
		$W = bab_Widgets();
		$select = $W->Select();

		$arr = bab_OCGetGroupDirOrgCharts();
		$select->setOptions($arr);
		
		// ne pas permettre une valeur vide car les anciennes version n'avais pas ce parametre
		// mais utilisais malgre tout l'organigramme
		$select->setValue(bab_OCgetPrimaryOcId());
		
		return $W->LabelledWidget(absences_translate('Organization chart used for delegated management'), $select, __FUNCTION__);
	}
	
	
	protected function boolfield($name, $title, $description = null)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($title, $W->CheckBox(), $name, $description);
	}

	
	protected function loadFormValues()
	{
		global $babDB;
		
		$req = "SELECT * FROM absences_options";
		$values = $babDB->db_fetch_assoc($babDB->db_query($req));
		
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		$values['managers'] = aclGetRightsString('absences_managers_groups', 1);
		$values['public_planning'] = aclGetRightsString('absences_public_planning_groups', 1);
		
		$this->setValues($values, array('options'));
	}
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
	
		$button = $W->FlowItems(
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
	
		$this->addItem($button);
	}
}
