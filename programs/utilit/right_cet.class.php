<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';

/**
 * Abscence Right
 * 
 * @property 	int		$id_right
 * @property	string	$saving_begin
 * @property	string	$saving_end
 * @property	float	$per_year
 * @property	float	$per_cet
 * @property	float	$ceiling
 * @property	float	$min_use
 * 
 */
class absences_RightCet extends absences_Record 
{
	private $right;
	
	private $id_right;
	
	public static function getFromId($id)
	{
		$rightcet = new absences_RightCet;
		$rightcet->id = $id;
		return $rightcet;
	}
	
	public static function getFromRight($id_right)
	{
		$rightcet = new absences_RightCet;
		$rightcet->id_right = $id_right;
		return $rightcet;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id) && !isset($this->id_right))
			{
				$this->row = false;
				return false;
			}
			
			global $babDB;
			
			$query = 'SELECT * FROM absences_rights_cet WHERE ';
			
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}
			
			if (isset($this->id_right))
			{
				$query .= 'id_right='.$babDB->quote($this->id_right);
			}
			
			$res = $babDB->db_query($query);
			$this->setRow($babDB->db_fetch_assoc($res));
		}
		
		return $this->row;
	}
	
	/**
	 * 
	 * @param absences_Right $right
	 * @return absences_RightCet
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
		
		return $this->right;
	}
	
	
	
	
	public function insert()
	{
		global $babDB;
	
		$babDB->db_query('INSERT INTO absences_rights_cet
			(
				id_right,
				saving_begin,
				saving_end,
				per_year,
				per_cet,
				ceiling,
				min_use
			)
				VALUES
			(
				'.$babDB->quote($this->id_right).',
				'.$babDB->quote($this->saving_begin).',
				'.$babDB->quote($this->saving_end).',
				'.$babDB->quote($this->per_year).',
				'.$babDB->quote($this->per_cet).',
				'.$babDB->quote($this->ceiling).',
				'.$babDB->quote($this->min_use).'
			)
		');
	
	
		$this->id = $babDB->db_insert_id();
	}
	
	
	
	public function update()
	{
		global $babDB;
	
		$babDB->db_query('UPDATE absences_rights_cet SET
	
				saving_begin 	= '.$babDB->quote($this->saving_begin).',
				saving_end 		= '.$babDB->quote($this->saving_end).',
				per_year 		= '.$babDB->quote($this->per_year).',
				per_cet 		= '.$babDB->quote($this->per_cet).',
				ceiling 		= '.$babDB->quote($this->ceiling).',
				min_use 		= '.$babDB->quote($this->min_use).' 
	
				WHERE id_right 	= '.$babDB->quote($this->id_right)
		);
	
	}
	
}
