<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

bab_Widgets()->includePhpClass('Widget_Form');

require_once dirname(__FILE__).'/right.class.php';


class absences_SyncServerEditor extends Widget_Form 
{
	private $I;
	
	public function __construct()
	{
		$W = bab_Widgets();
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		
		$this->setName('sync_server');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
		
		$this->setCanvasOptions($this->Options()->width(70,'em'));
		
		$this->I = new absences_RightIterator;
		$this->I->kind = array(absences_Right::REGULAR, absences_Right::CET);
		
		$this->addFields();
		$this->loadFormValues();
		
		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}
	
	
	
	protected function addFields()
	{
		$W = bab_Widgets();
		
		
		$this->addItem($W->Label(absences_translate('Shared rights')));
		
		$frame = $W->Frame()->setName('right');
		$this->addItem($frame);
		
		foreach($this->I as $right)
		{
			$frame->addItem($this->shareRight($right));
		}
		
	}
	
	
	protected function shareRight(absences_Right $right)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($right->description, $W->CheckBox())->setName($right->id);
	}
	
	
	protected function loadFormValues()
	{
		$values = array('right' => array());
		
		foreach($this->I as $right)
		{
			/*@var $right absences_Right */
			if (absences_Right::SYNC_SERVER === $right->getSyncStatus())
			{
				$values['right'][$right->id] = 1;
			}
		}
		
		$this->setValues($values, array('sync_server'));
	}
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
		
		$button = $W->FlowItems(
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
		
		$this->addItem($button);
	}
}


















class absences_SyncClientEditor extends Widget_Form
{
	private $I;

	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));


		$this->setName('sync_client');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(70,'em'));

		$this->addFields();
		$this->loadFormValues();

		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}


	/**
	 * @throws Exception
	 */
	protected function addFields()
	{
		$W = bab_Widgets();


		$this->addItem($W->Label(sprintf(absences_translate('Shared rights from %s'), absences_getVacationOption('sync_url'))));

		$frame = $W->Frame()->setName('right');
		$this->addItem($frame);
		
		require_once dirname(__FILE__).'/client.class.php';
		
		$client = new absences_client;
		
		$rights = $client->getCachedData();
		
		if (!$rights)
		{
			$frame->addItem($W->Label(absences_translate('No rights found')));
			return;
		}
		
		foreach($rights as $arr)
		{
			
			$right = new absences_Right('');
			$right->setRow($arr['absences_rights']);
			$frame->addItem($this->useRight($right));
		}

		
		$this->addItem($W->Label(absences_translate('The checked rights will be autmatically updated every hours')));
	}


	protected function useRight(absences_Right $right)
	{
		$W = bab_Widgets();
		
		if (!isset($right->description))
		{
			return null;
		}
		
		return $W->LabelledWidget($right->description, $W->CheckBox())->setName($right->uuid);
	}


	protected function loadFormValues()
	{
		$values = array('right' => array());
		
		$I = new absences_RightIterator();
		$I->archived = null;
		$I->sync_status = array(absences_Right::SYNC_CLIENT, absences_Right::SYNC_CLIENT_END, absences_Right::SYNC_CLIENT_ERROR);
	

		foreach($I as $right)
		{
			$values['right'][$right->uuid] = 1;
		}

		$this->setValues($values, array('sync_client'));
	}


	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}