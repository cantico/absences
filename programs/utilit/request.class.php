<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/record.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';





/**
 * A user request (entry, cet deposit, workperiod recover)
 * 
 * @property	int 	$id
 * @property	int 	$id_user
 * @property 	int		$idfai
 * @property 	string	$status
 * @property 	int		$id_approver
 * @property 	int		$appr_notified	
 * @property	string	$comment2
 * @property    int     $todelete
 * @property    int     $firstconfirm
 * 
 */
abstract class absences_Request extends absences_Record
{
	
	/**
	 *
	 * @var absences_Agent
	 */
	protected $agent;
	
	
	/**
	 * 
	 * @var absences_Movement
	 */
	private $lastMovement;
	
	
	
	/**
	 * @param int $id
	 * @return absences_Request
	 */
	abstract static public function getById($id);
	
	
	/**
	 * @return absences_Agent
	 */
	public function getAgent()
	{
		require_once dirname(__FILE__).'/agent.class.php';
	
		if (!isset($this->agent))
		{
			$row = $this->getRow();
			$this->agent = absences_Agent::getFromIdUser($row['id_user']);
		}
	
		return $this->agent;
	}
	
	
	
	
	/**
	 * Method to sort requests by creation date
	 */
	public function createdOn()
	{
		return $this->createdOn;
	}

	/**
	 * Method to sort requests by modification date
	 */
	public function modifiedOn()
	{
		return $this->modifiedOn;
	}

	/**
	 *
	 * @return string
	 */
	public function getUserName()
	{
		return bab_getUserName($this->id_user);
	}


	protected function labelledValue($title, $value)
	{
		$W = bab_Widgets();
		return $W->FlowItems($W->Label($title)->addClass('widget-strong')->colon(), $W->Label($value))->setHorizontalSpacing(.4,'em');
	}
	
	/**
	 * 
	 * @return string
	 */
	abstract public function getRequestType();
	
	/**
	 * Get status on date
	 * @param string $date
	 * @return string
	 */
	public function getDateStatus($date)
	{
        if (!isset($date) || '0000-00-00' === $date) {
            return $this->status;
        }
	    
	    
	    require_once dirname(__FILE__).'/movement.class.php';
	    $I = new absences_MovementIterator();
	    $I->setRequest($this);
	    $I->createdOn = $date;
	    
	    foreach($I as $movement) {

	        // mouvement le plus recent avant la date
	        
	        if (!isset($movement->status)) {
	            // movement created before status field in database
	            return $this->status;
	        }
	        
	        return $movement->status;
	        
	    }
	    
	    return $this->status;
	}
	
	/**
	 * @return string
	 */
	public function getShortStatusStr()
	{
	    switch($this->status)
	    {
	        case 'Y':
	            return absences_translate("Accepted");
	        case 'N':
	            return absences_translate("Refused");
	        case 'P':
	            return absences_translate("Previsional request");
	        default:
	            return absences_translate("Waiting approval");
	    }
	}
	
	
	
	private function endApprovalOnMissingApprovers()
	{
	    $arr = bab_WFGetWaitingApproversInstance($this->idfai);
	    
	    if (0 === count($arr)) {
	        try {
	            $this->acceptApprovalWorkflow();
	            $this->save();
	        } catch (Exception $e) {
	            // Ex: fail to update the event in caldav
	            bab_debug($this->getTitle().' : '.$e->getMessage(), DBG_FATAL);
	        }
	    }
	}
	


	/**
	 * Get status as a string
	 * @return string
	 */
	public function getStatusStr()
	{
	    if ('' === $this->status) {
	        $this->endApprovalOnMissingApprovers();
	    }
	    
		switch($this->status)
		{
			case 'Y':
				return absences_translate("Accepted");
			case 'N':
				return absences_translate("Refused");
			case 'P':
				return absences_translate("Previsional request");
			default:
			    if ($this->todelete) {
			        return sprintf(absences_translate("Will be deleted after approval by %s"), $this->getNextApprovers());
			    }
				return sprintf(absences_translate("Waiting approval by %s"), $this->getNextApprovers());
		}
	}
	
	
	
	/**
	 * @return Widget_Icon
	 */
	public function getStatusIcon()
	{
		$W = bab_Widgets();
		
		switch($this->status)
		{
			case 'Y':
				$icon = Func_Icons::ACTIONS_DIALOG_OK;
				break;
			case 'N':
				$icon = Func_Icons::ACTIONS_DIALOG_CANCEL;
				break;
			case 'P':
				$icon = Func_Icons::STATUS_DIALOG_QUESTION;
				break;
			default:
				$icon = Func_Icons::ACTIONS_VIEW_HISTORY;
				break;
		}
		
		return $W->Icon($this->getStatusStr(), $icon);
	}
	
	
	/**
	 * Test if the has been fixed by a manager (not modifiable by the user)
	 * @return bool
	 */
	public function isFixed()
	{
	    return false;
	
	}


	private function getNames($arr)
	{
		$return = array();

		foreach($arr as $k => $id_user)
		{
			$return[$k] = bab_getUserName($id_user);
		}

		return $return;
	}


	/**
	 * @return string
	 */
	public function getNextApprovers()
	{
		$arr = bab_WFGetWaitingApproversInstance($this->idfai);

		if (count($arr) <= 3)
		{
			return implode(', ', $this->getNames($arr));
		}

		$first = array_shift($arr);
		$second = array_shift($arr);

		return sprintf(absences_translate('%s, %s and %d other approvers'), bab_getUserName($first), bab_getUserName($second), count($arr));
	}
	
	/**
	 * Get the apporbation sheme ID for the request
	 * @return int
	 */
	abstract public function getApprobationId();
	
	
	
	/**
	 * @return bool
	 */
	protected function autoApproval()
	{
	    if (isset($this->todelete) && $this->todelete) {
	        $this->addMovement(sprintf(absences_translate('The %s has been deleted with auto-approval'), $this->getTitle()));
	        $this->delete();
	        return false;
	    }
	    
	    
	    $this->onConfirm();
	    $this->addMovement(sprintf(absences_translate('The %s has been accepted with auto-approval'), $this->getTitle()));
	    return false;
	}
	
	
	
	private function getAutoApprovalUserId()
	{
	    if (!absences_getVacationOption('auto_approval')) {
	        return 0;
	    }
	    
	    return bab_getUserId();
	}
	
	
	
	/**
	 * Create approbation instance and set it "notified" but do not send email notification
	 * retourne TRUE si la demande est bien devenue en attente d'approbation
	 * @return bool
	 */
	public function createApprobationInstance()
	{
		
		if (isset($this->idfai) && $this->idfai > 0)
		{
			bab_WFDeleteInstance($this->idfai);
		}
		
		// si le 3eme parametre est passe avec l'id user en cours, cela active l'auto-approbation
		// l'auto-approbation a ete active a partir de la version 2.21 du module
		// le dernier parametre necessite la version 8.0.100 d'ovidentia pour fonctionner (correction du bug de detection du supperieur hierarchique quand le gestionnaire depose l'absence)
		
		$idfai = bab_WFMakeInstance($this->getApprobationId(), get_class($this).':'.$this->id, $this->getAutoApprovalUserId(), $this->id_user);
		if (is_int($idfai))
		{
			$users = bab_WFGetWaitingApproversInstance($idfai, true);
			if (count($users) == 0)
			{
				$a = bab_WFGetApprobationInfos($this->getApprobationId());
				
				// l'instance n'a pas fonctionne
				bab_WFDeleteInstance($idfai);
				//TRANSLATORS: \n%1: request type (vacation request), \n%2: schema type (Staff schema), \n%3: schema name
				$this->addMovement(sprintf(absences_translate('The approval initialisation failed on the %s because no approvers were found on %s %s'), $this->getTitle(), mb_strtolower($a['type']), $a['name']));
				return $this->autoApproval();
			}
			
			if (isset($this->todelete) && $this->todelete) {
			    $this->addMovement(sprintf(absences_translate('The %s has been sent to deletion approval'), $this->getTitle()));
			} else {
			    $this->addMovement(sprintf(absences_translate('The %s has been sent for approval'), $this->getTitle()));
			}
			
		} else { // $idfai == TRUE : auto-approbation end schema
			return $this->autoApproval();
		}
		
		$this->idfai = $idfai;
		$this->status = '';
		$this->id_approver = 0;
		$this->appr_notified = 0;
		$this->comment2 = '';
		$this->save();

		return true;
	}
	
	
	protected function rejectApprovalWorkflow()
	{
	    bab_WFDeleteInstance($this->idfai);
	    $this->idfai = 0;
	    if ($this->todelete) {
	    
	        // demande de suppression refusee
	        $this->status = 'Y';
	    } else {
	        $this->status = 'N';
	    }
	    $this->onReject();
	}
	
	
	protected function acceptApprovalWorkflow()
	{
	    bab_WFDeleteInstance($this->idfai);
	    $this->idfai = 0;
	    $this->status = 'Y';
	    
	    if (!$this->firstconfirm) {
	        $this->firstconfirm = 1;
	    }
	    
	    if ($this->todelete) {
	        return $this->delete();
	    }
	    $this->onConfirm();
	}
	
	
	/**
	 * Approbator confirm
	 * move to next approbator or close instance and set the request accepted
	 * 
	 * 
	 * @param	bool	$status				Accept or reject approbation step
	 * @param	string	$approver_comment	
	 * 
	 */
	public function approbationNext($status, $approver_comment)
	{
		
		$res = bab_WFUpdateInstance($this->idfai, bab_getUserId(), $status);
		
		switch ($res) {
			case 0:
				$this->rejectApprovalWorkflow();
				break;
				
			case 1:
			    $this->acceptApprovalWorkflow();
				break;

				
			default: // -1
				$nfusers = bab_WFGetWaitingApproversInstance($this->idfai, true);
				if (count($nfusers) > 0) {
					$this->appr_notified = 0;
				} else {
					// no approvers found or approvers allready notified (all user must confirm without order)
					// do nothing
				}
				
			break;
		}

		$this->id_approver = bab_getUserId();
		$this->comment2 = $approver_comment;
		$this->save();
		
		
		$str_status = $status ? absences_translate('accepted') :absences_translate('rejected');
		
		// movement must be saved after status modification to save the new status in movement
		$this->addMovement(
		    sprintf(absences_translate('The %s has been %s by %s'), $this->getTitle(), $str_status, bab_getUserName(bab_getUserId())),
		    $approver_comment
		);
		
	}
	
	
	
	/**
	 * Auto confirm the request
	 */
	public function autoConfirm()
	{
		$this->addMovement(
			sprintf(absences_translate('The %s has been confirmed automatically because of the validation timout'), $this->getTitle())
		);
		
		
		if ($this->idfai > 0)
		{
			bab_WFDeleteInstance($this->idfai);
		}
		
		$this->idfai = 0;
		$this->status = 'Y';
		$this->onConfirm();
		
		$this->id_approver = 0;
		$this->save();
		$this->notifyOwner();
	}
	
	
	
	/**
	 * Test if the logged in user can modify the entry
	 * @return bool
	 */
	public function canModify()
	{
	    $modify_waiting = (bool) absences_getVacationOption('modify_waiting');
	    $modify_confirmed = (bool) absences_getVacationOption('modify_confirmed');
	    
		require_once dirname(__FILE__).'/agent.class.php';
		$agent = absences_Agent::getCurrentUser();
	
		$personal = ($agent->isInPersonnel() && $agent->id_user === $this->id_user);
	
		switch($this->status)
		{
			case 'Y': // confirmed
			    if ($this->isFixed()) {
			        return false;
			    }

			    if ($modify_confirmed) {
			        return true;
			    }
			    
			    return !$personal; // ce n'est pas ma propre demande, je suis gestionnaire delegue

				
	
			case 'N': // rejected
				return false;
	
			case 'P': // previsional
				return $personal;
	
			default: // waiting
				return ($personal && $modify_waiting);
		}
	}
	
	
	/**
	 * Test if the user can delete the entry
	 * @return bool
	 */
	public function canDelete()
	{
		return $this->canModify();
	}
	
	
	
	
	
	/**
	 * Process specific code when the request is rejected
	 *
	 */
	abstract protected function onReject();
	
	/**
	 * Process specific code when the request is confirmed
	 */
	abstract protected function onConfirm();

	/**
	 * request title
	 * @return string
	 */
	abstract public function getTitle();


	
	/**
	 * Get a list of field to display in the notifications for this request
	 * @return array
	 */
	abstract public function getNotifyFields();
	
	
	/**
	 * annee de la demande
	 * @return int
	 */
	abstract public function getYear();
	
	
	/**
	 * annee de la demande, pour l'archivage par annee, tiens compte du parametrage du mois de debut de periode.
	 * Utilise dans l'archivage par annee
	 * @return int
	 */
	abstract public function getArchiveYear();
	
	
	abstract public function archive();
	
	
	/**
	 * approver has been notified
	 */
	abstract public function setNotified();
	
	
	/**
	 * Url to edit request as a manager
	 */
	abstract public function getManagerEditUrl();
	
	/**
	 * Url to delete request as a manager
	 */
	abstract public function getManagerDeleteUrl();
	
	
	/**
	 * Notify request next approvers
	 */
	public function notifyApprovers()
	{
		$defer = (bool) absences_getVacationOption('approb_email_defer');
		if (!$defer)
		{
			require_once dirname(__FILE__).'/request.notify.php';
			absences_notifyRequestApprovers();
		}
	}
	
	
	/**
	 * Notify request owner about the approval or reject of the request
	 */
	public function notifyOwner()
	{
		$appliquant_email = (bool) absences_getVacationOption('appliquant_email');
		
		if (!$appliquant_email)
		{
			return;
		}
		
		require_once dirname(__FILE__).'/request.notify.php';
		
		
		
		switch($this->status)
		{
			case 'N':
				$subject = sprintf(absences_translate("Your %s has been refused"), $this->getTitle());
				break;
				
			case 'Y':
				$subject = sprintf(absences_translate("Your %s has been accepted"), $this->getTitle());
				break;
				
			default: // next approver
				return;
		}
		
		absences_notifyRequestAuthor(array($this), $subject, $subject, $this->id_user);
	}


	/**
	 * Get request card frame to display in requests list
	 * @param bool 			$display_username	Affiche le nom du demandeur dans la card frame ou non, utile pour les listes contenant plusieurs demandeurs possibles
	 * @param int			$rfrom				si les demandes de la liste sont modifiee par un gestionnaire ou gestionnaire delegue
 	 * @param int			$ide				id entite de l'organigramme >0 si gestionnaire delegue seulement
	 * @return Widget_Frame
	 */
	abstract public function getCardFrame($display_username, $rfrom, $ide);

	
	/**
	 * Get request frame to display in manager lists
	 * @return Widget_Frame
	 */
	abstract public function getManagerFrame();
	

	
	/**
	 * Delete request
	 */
	public function delete()
	{
		if ($this->idfai > 0)
		{
			bab_WFDeleteInstance($this->idfai);
		}
		
		global $babDB;
		$babDB->db_query("UPDATE absences_movement SET id_request='0' WHERE request_class=".$babDB->quote(get_class($this))." AND id_request=".$babDB->quote($this->id));
	}
	
	
	
	/**
	 * @return absences_MovementIterator
	 */
	public function getMovementIterator()
	{
		require_once dirname(__FILE__).'/movement.class.php';
	
		$I = new absences_MovementIterator;
		$I->setRequest($this);
	
		return $I;
	}
	
	
	
	/**
	 *
	 * @param string $message	Generated message
	 * @param string $comment	Author comment
	 */
	public function addMovement($message, $comment = '', $createdOn = null)
	{
		require_once dirname(__FILE__).'/movement.class.php';
	
		$movement = new absences_Movement();
		$movement->message = $message;
		$movement->comment = $comment;
		$movement->setRequest($this);
		$movement->setAgent($this->getAgent());
		$movement->createdOn = $createdOn;
		$movement->save();
	}
	
	
	
	/**
	 * @return absences_Movement
	 */
	public function getLastMovement()
	{
		if (!isset($this->lastMovement))
		{
			$this->lastMovement = false;
			$I = $this->getMovementIterator();
			foreach($I as $movement)
			{
				$this->lastMovement = $movement;
				break;
			}
		}
		
		return $this->lastMovement;
	}
	
	
	/**
	 * Faut-il allerter l'approbateur que le delai d'approbation est depasse
	 * @return bool
	 */
	public function approbAlert()
	{
		$last = $this->getLastMovement();
		
		if (!$last)
		{
			return false;
		}
			
		$s = (time() - bab_mktime($last->createdOn));
		$d = ($s / 86400);
		
		$approb_alert = (int) absences_getVacationOption('approb_alert');
		
		if ($approb_alert > $d)
		{
			return false;
		}
		
		return true;
	}
}









/**
 * Get the list of request from the users, requests are :
 * 
 * - vacations requests
 * - workday entiling recovery
 * - time saving account deposits
 *
 * Sorted by the most recent in first
 */
class absences_RequestIterator implements SeekableIterator, Countable 
{

	/**
	 * 
	 * @var array
	 */
	public $users;
	
	/**
	 * Filter by organization
	 * @var int
	 */
	public $organization;
	
	/**
	 * 
	 * @var array
	 */
	private $data;
	
	/**
	 * 
	 * @var int
	 */
	private $pos;
	
	
	/**
	 * Return only one entry per folder
	 * @var bool
	 */
	public $one_per_folder = false;
	
	
	/**
	 * Filtrer les demandes necessitant ou pas un email aux approbateurs
	 * @var int
	 */
	public $appr_notified;
	
	/**
	 * Filtrer les demandes avec unes instance d'approbation
	 * @var bool
	 */
	public $idfai_set;
	
	/**
	 * Filtrer les demandes par status
	 * @var string
	 */
	public $status;
	
	
	/**
	 * Search all request created before this date
	 * @var string
	 */
	public $createdOn;
	
	
	/**
	 * Search all request modified before this date
	 * @var string
	 */
	public $modifiedOn;
	
	/**
	 * Filtrer les demandes par annee
	 * @var int
	 */
	public $year;
	
	/**
	 * Filtrer les demandes par date de debut superieur
	 * @var string
	 */
	public $startFrom;
	
	/**
	 * Filtrer les demandes par date de debut inferieur
	 * @var string
	 */
	public $startTo;
	
	/**
	 * Filtrer les demandes par statut d'archivage
	 * @var int
	 */
	public $archived = 0;
	
	
	public function __construct($users = null)
	{
		$this->users = $users;
	}
	
	/**
	 * Get all requests in one array (not sorted)
	 * @return array
	 */
	public function getArray()
	{
		require_once dirname(__FILE__).'/entry.class.php';
		require_once dirname(__FILE__).'/workperiod_recover_request.class.php';
		require_once dirname(__FILE__).'/cet_deposit_request.class.php';
		
		
		$return = array();
		
		$entries = new absences_EntryIterator;
		$entries->users = $this->users;
		$entries->organization = $this->organization;
		$entries->appr_notified = $this->appr_notified;
		$entries->idfai_set = $this->idfai_set;
		$entries->status = $this->status;
		$entries->createdOn = $this->createdOn;
		$entries->modifiedOn = $this->modifiedOn;
		$entries->year = $this->year;
		$entries->archived = $this->archived;
		$entries->startFrom = $this->startFrom;
		$entries->startTo = $this->startTo;
		
		$folders = array();
		
		foreach($entries as $entry)
		{
			if ($this->one_per_folder && isset($entry->folder) && $entry->folder > 0)
			{
				if (isset($folders[$entry->folder]))
				{
					continue;
				}
				
				$folders[$entry->folder] = 1;
			}
			
			$return[] = $entry;
		}
		
		
		$cetdeposits = new absences_CetDepositRequestIterator;
		$cetdeposits->users = $this->users;
		$cetdeposits->organization = $this->organization;
		$cetdeposits->appr_notified = $this->appr_notified;
		$cetdeposits->idfai_set = $this->idfai_set;
		$cetdeposits->status = $this->status;
		$cetdeposits->createdOn = $this->createdOn;
		$cetdeposits->modifiedOn = $this->modifiedOn;
		$cetdeposits->year = $this->year;
		$cetdeposits->archived = $this->archived;
		$cetdeposits->startFrom = $this->startFrom;
		$cetdeposits->startTo = $this->startTo;
		
		foreach($cetdeposits as $deposit)
		{
			$return[] = $deposit;
		}
		
		$workperiods = new absences_WorkperiodRecoverRequestIterator;
		$workperiods->users = $this->users;
		$workperiods->organization = $this->organization;
		$workperiods->appr_notified = $this->appr_notified;
		$workperiods->idfai_set = $this->idfai_set;
		$workperiods->status = $this->status;
		$workperiods->createdOn = $this->createdOn;
		$workperiods->modifiedOn = $this->modifiedOn;
		$workperiods->year = $this->year;
		$workperiods->archived = $this->archived;
		$workperiods->startFrom = $this->startFrom;
		$workperiods->startTo = $this->startTo;
		
		foreach($workperiods as $recovery)
		{
			$return[] = $recovery;
		}
		
		return $return;
	}
	
	/**
	 * Get all requests in one array
	 * @return array
	 */
	public function getSortedArray()
	{
		$arr = $this->getArray();
		bab_Sort::sortObjects($arr, 'createdOn', bab_Sort::CASE_INSENSITIVE);
		
		$arr = array_reverse($arr);
		
		return array_values($arr);
	}
	
	
	/**
	 * 
	 * @return multitype:absences_Request
	 */
	private function getData()
	{
		if (!isset($this->data))
		{
			$this->data = $this->getSortedArray();
		}
		
		return $this->data;
	}
	
	
	public function seek($position) {
		$this->pos = $position;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see SeekableIterator::current()
	 * 
	 * @return absences_Request
	 */
	public function current() {
		$data = $this->getData();
		return $data[$this->pos];
	}
	
	public function next() {
		$this->pos++;
	}
	
	public function key() {
		return $this->pos;
	}
	
	public function valid() {
		$data = $this->getData();
		return isset($data[$this->pos]);
	}
	
	public function rewind() {
		$this->pos = 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Countable::count()
	 * 
	 * @return int
	 */
	public function count() {
		return count($this->getData());
	}
}




