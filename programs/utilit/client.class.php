<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__).'/sync_items.class.php';


/**
 * HTTP client for synchronization
 *
 */
class absences_client
{


	/**
	 * 
	 * @var absences_sync_items
	 */
	private $sync_items;
	
	private $uuid_index = null;


	/**
	 * 
	 */
	public function __construct()
	{

		$this->sync_items = new absences_sync_items();
	}
	
	



	/**
	 * Get data from server with a query
	 * 
	 * @param string	$lastmodified			NULL = force update
	 * 
	 * @return array | null
	 */
	protected function getData($lastmodified = null)
	{
		
		
		$url = absences_getVacationOption('sync_url');
		$nickname = absences_getVacationOption('sync_nickname');
		$password = absences_getVacationOption('sync_password');

		if (empty($url) || empty($nickname) || empty($password))
		{
			throw new Exception('Missing server URL or nickname or password in configuration');
		}
		
		if ('/' !== substr($url, -1, 1))
		{
			$url .= '/';
		}
		
		$url .= 'index.php?addon=absences.server';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, sprintf('%s:%s', $nickname, $password));
		if ($lastmodified !== null)
		{
			bab_debug(sprintf('Local lastmodified date used in the If-modified-since header %s', $lastmodified));
			
			$headers = array(
					'If-modified-since: '.gmdate("D, d M Y H:i:s", bab_mktime($lastmodified)) . " GMT"
			);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		} else {
			$user_id = bab_getUserId();
			$user_name = $user_id ? bab_getUserName($user_id) : 'Anonymous';
			bab_debug(sprintf('Force update by "%s" : do not get the last modification date', $user_name));
		}

		$return = curl_exec($ch);

		if (false === $return)
		{
			throw new Exception(curl_error($ch));
		}
	


		if (304 === curl_getinfo($ch, CURLINFO_HTTP_CODE))
		{
			bab_debug('Received 304');
			$arr_return = null;
			
		} else {
			
			$memory_limit = (64*1024*1024) + (strlen($return) * 10);
			ini_set('memory_limit', $memory_limit);
			
			$arr_return = unserialize($return);
		
			if (false === $arr_return)
			{
				throw new Exception('Server : '.$return);
			}
			
			if ($content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE))
			{
				if (preg_match('/charset=([A-Z0-9\-]+)/', $content_type, $match))
				{
					if ($match[1] !== bab_Charset::getIso())
					{
						$arr_return = $this->getValueAccordingToDbCharset('', $arr_return, $match[1]);
					}
				}
			}
			
		}

		curl_close($ch);



		return $arr_return;
	}
	
	
	
	/**
	 * Query with a static cache file 
	 * 
	 * 60 secondes de cache par la duree 
	 * au dela la date de derniere mise a jour est verifiee sur le serveur
	 * 
	 * @throws Exception
	 * 
	 * @return array
	 */
	public function getCachedData()
	{
		$addon = bab_getAddonInfosInstance('absences');
		$cachefile = new bab_Path($addon->getUploadPath());
		$cachefile->createDir();
		
		$cachefile->push('server_data');
		
		if ($cachefile->fileExists())
		{
			$lastupdate = date('Y-m-d H:i:s', filemtime($cachefile->tostring()));
			bab_debug('Cache last update '.$lastupdate);
		} else {
			$lastupdate = null;
		}
		
		
		if (!isset($lastupdate) || (time() - bab_mktime($lastupdate)) > 60)
		{
			bab_debug('Get data from server');
			$data = $this->getData($lastupdate);
			if (null !== $data)
			{
				file_put_contents($cachefile->tostring(), serialize($data));
				return $data;
			}
			
			bab_debug('Server return null (304)');
		}
		
		
		// 304 not modified || not timed out
		
		bab_debug('Get data from cache');
		$data = file_get_contents($cachefile->tostring());
		$data = unserialize($data);
		
		if (false === $data)
		{
			$cachefile->delete();
		}
		
		return $data;
	}
	
	
	
	
	
	/**
	 * Get shared right with all depencies as objects
	 * @param string $uuid
	 * @return absences_Right
	 */
	public function getRight($uuid)
	{
		if (!isset($this->uuid_index))
		{
			$this->uuid_index = array();
			$data = $this->getCachedData();
			
			require_once dirname(__FILE__).'/right_rule.class.php';
			require_once dirname(__FILE__).'/right_cet.class.php';
			require_once dirname(__FILE__).'/right_inperiod.class.php';
			
			
			foreach($data as $row)
			{
				$right_row = $row['absences_rights'];
				
				$right_row['id'] = null;
				$right = new absences_Right(null);
				$right->setRow($right_row);
				$this->uuid_index[$right_row['uuid']] = $right;
				
				
				$type_row = $row['absences_types'];
				$type_row['id'] = null;
				$type = new absences_Type(null);
				$type->setRow($type_row);
				$right->setType($type);
				
				$rules_row = $row['absences_rights_rules'];
				$rules_row['id'] = null;
				$rules = new absences_RightRule();
				$rules->setRow($rules_row);
				$right->setRightRule($rules);
				
				if (empty($row['rules_type']))
				{
					$rules_type_row = $row['rules_type'];
					$rules_type_row['id'] = null;
					$rules_type = new absences_Type(null);
					$rules_type->setRow($rules_type_row);
					$rules->setType($rules_type);
				}
				
				
				if (!empty($row['absences_rights_cet']))
				{
					$cet_row = $row['absences_rights_cet'];
					$cet_row['id'] = null;
					$cet = new absences_RightCet();
					$cet->setRow($cet_row);
					$right->setRightCet($cet);
				}
				
				if (!empty($row['absences_rights_inperiod']))
				{
					foreach($row['absences_rights_inperiod'] as $inperiod_row)
					{
						$inperiod_row['id'] = null;
						$inperiod = new absences_RightInPeriod();
						$inperiod->setRow($inperiod_row);
						$right->addInPeriod($inperiod);
					}
				}
				
				
				if (!empty($row['absences_rgroup']))
				{
					$rgroup_row = $row['absences_rgroup'];
					$rgroup_row['id'] = null;
					$rgroup = new absences_Rgroup(null);
					$rgroup->setRow($rgroup_row);
					$right->setRgroup($rgroup);
				}
			}
			
		}
		
		
		if (!isset($this->uuid_index[$uuid]))
		{
			return null;
		}
		
		
		return $this->uuid_index[$uuid];
	}
	
	
	
	
	/**
	 * Convert charset for text values in array
	 * ignore keys begin with __ (binary data)
	 * 
	 * @param string $keyname
	 * @param string $input
	 * @param string $sStringIsoCharset
	 * 
	 * @return string
	 */
	protected function getValueAccordingToDbCharset($keyname, $input, $sStringIsoCharset)
	{
		if (bab_charset::getIso() === $sStringIsoCharset || 0 === strpos($keyname, '__')) {
			return $input;
		}
		
		if (is_array($input)) {
			foreach($input as $k => $data) {
				$input[$k] = $this->getValueAccordingToDbCharset($k, $data, $sStringIsoCharset);
			}
		
			return $input;
		}
		

		return mb_convert_encoding($input, bab_charset::getIso(), $sStringIsoCharset);
	}
	
	



	/**
	 * 
	 * @param string $uuid
	 * @return absences_Right
	 */
	public function addRight($uuid)
	{
		
		global $babDB;
		$right = $this->getRight($uuid);
		
		$type = $right->getType();
		$rgroup = $right->getRgroup();
		$right->id_type = 0;
		$right->id_rgroup = 0;
		
		// search a matching type
		$res = $babDB->db_query('SELECT id FROM absences_types WHERE name LIKE \''.$babDB->db_escape_like($type->name).'\'');
		if (0 !== $babDB->db_num_rows($res))
		{
			$arr = $babDB->db_fetch_assoc($res);
			$right->id_type = $arr['id'];
		}
		
		// search a matching rgroup
		$res = $babDB->db_query('SELECT id FROM absences_rgroup WHERE name LIKE \''.$babDB->db_escape_like($rgroup->name).'\'');
		if (0 !== $babDB->db_num_rows($res))
		{
			$arr = $babDB->db_fetch_assoc($res);
			$right->id_rgroup = $arr['id'];
		}
		
		
		$right->sync_status = 1;
		$right->sync_update = date('Y-m-d H:i:s');
		$right->insert();
		
		foreach($right->getInperiodRules() as $inperiod)
		{
			$inperiod->save();
		}
		
		$rule = $right->getRightRule();
		if ($rule->getRow())
		{
			$rule->trigger_type = 0;
			$type = $rule->getType();
			if (isset($type))
			{
				// search a matching type
				$res = $babDB->db_query('SELECT id FROM absences_types WHERE name LIKE \''.$babDB->db_escape_like($type->name).'\'');
				if (0 !== $babDB->db_num_rows($res))
				{
					$arr = $babDB->db_fetch_assoc($res);
					$rule->trigger_type = $arr['id'];
				}
			}
			
			$rule->id_right = $right->id;
			$rule->insert();
		}
		
		$cet = $right->getRightCet();
		if ($cet->getRow())
		{
			$cet->id_right = $right->id;
			$cet->insert();
		}
		
		return $right;
	}
	
	
	public function updateRight($uuid)
	{
		global $babDB;
		$right = $this->getRight($uuid);
		$right->sync_status = 1;
		$right->sync_update = date('Y-m-d H:i:s');
		$right->update();
		
		
		foreach($right->getInperiodRules() as $inperiod)
		{
			$inperiod->save();
		}
		
		$rule = $right->getRightRule();
		if ($rule->getRow())
		{
			$rule->id_right = $right->id;
			$rule->update();
		}
		
		$cet = $right->getRightCet();
		if ($cet->getRow())
		{
			$cet->id_right = $right->id;
			$cet->update();
		}
	}

}


