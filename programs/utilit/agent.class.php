<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/collection.class.php';
require_once dirname(__FILE__).'/vacincl.php';

/**
 * Agent, membre du personnel
 *
 * @property 	int 	$id_user
 * @property	int		$id_coll
 * @property	int		$id_sa
 * @property	int		$id_sa_cet
 * @property	int		$id_sa_recover
 * @property	string	$emails
 * @property    int     $id_organization
 */
class absences_Agent extends absences_Record
{
	/**
	 *
	 * @var int
	 */
	protected $id_user;

	/**
	 *
	 * @var absences_Organization
	 */
	protected $organization;


	/**
	 *
	 * @var absences_Collection
	 */
	protected $collection;

	/**
	 * Cache for user directory entry
	 * @var array
	 */
	private $direntry;

	/**
	 * Cache for user organization chart main entity
	 * @var array
	 */
	private $ocentity;


	/**
	 * Cache for approbation scheme
	 * @var array
	 */
	private $approbation;

	/**
	 *
	 * @var absences_AgentCet
	 */
	private $cet;


	/**
	 *
	 * @var absences_Agent
	 */
	private static $currentAgent;


	/**
	 *
	 * @var Array
	 */
	private $calendar_entities;


	/**
	 *
	 * @var Array
	 */
	private $managed_entities;


	/**
	 * Get the agent using the id_user
	 * @param	int	$id_user
	 * @return absences_Agent
	 */
	public static function getFromIdUser($id_user)
	{
		$agent = new absences_Agent;
		$agent->id_user = (int) $id_user;

		return $agent;
	}

	/**
	 * Get the logged in agent
	 * @return absences_Agent
	 */
	public static function getCurrentUser()
	{
		if (!isset(self::$currentAgent))
		{
			$id_user = bab_getUserId();
			if (0 === $id_user)
			{
				throw new Exception('The user is not logged in');
			}

			self::$currentAgent = self::getFromIdUser($id_user);
		}

		return self::$currentAgent;
	}



	/**
	 * Get Agent using the ID in "personnel" table
	 * @return absences_Agent
	 */
	public static function getFromIdPersonnel($id)
	{
		$agent = new absences_Agent;
		$agent->id = (int) $id;

		return $agent;
	}

	/**
	 * @return int
	 */
	public function getIdUser()
	{
		if (isset($this->id_user))
		{
			return $this->id_user;
		}

		$row = $this->getRow();
		return (int) $row['id_user'];
	}


	/**
	 * Regime
	 * @param	absences_Collection	$collection
	 */
	public function setCollection(absences_Collection $collection)
	{
		$this->collection = $collection;
		return $this;
	}

	/**
	 * Regime
	 * @return absences_Collection
	 */
	public function getCollection()
	{

		if (!isset($this->collection))
		{
			$row = $this->getRow();
			$this->collection = absences_Collection::getById($row['id_coll']);
		}

		return $this->collection;
	}






	/**
	 * Organisme
	 * @param	absences_Organization	$organization
	 */
	public function setOrganization(absences_Organization $organization)
	{
	    $this->organization = $organization;
	    return $this;
	}

	/**
	 * Organisme
	 * @return absences_Organization
	 */
	public function getOrganization()
	{

	    if (!isset($this->organization))
	    {
	        require_once dirname(__FILE__).'/organization.class.php';
	        $row = $this->getRow();
	        if($row['id_organization'] == 0){
	        	return null;
	        }
	        $this->organization = absences_Organization::getById($row['id_organization']);
	    }

	    try {
	        $this->organization->getRow();
	    } catch (Exception $e) {
	        return null;
	    }
	    
	    return $this->organization;
	}





	/**
	 * @return string
	 */
	public function getName()
	{
		return bab_getUserName($this->getIdUser());
	}



	/**
	 * Table row as an array
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;

			$query = 'SELECT * FROM absences_personnel WHERE ';
			if (isset($this->id))
			{
				$query .= 'id='.$babDB->quote($this->id);
			}

			if (isset($this->id_user))
			{
				$query .= 'id_user='.$babDB->quote($this->id_user);
			}

			$res = $babDB->db_query($query);
			$row = $babDB->db_fetch_assoc($res);

			if (!$row)
			{
				throw new Exception('This agent does not exists id='.$this->id.' id_user='.$this->id_user);
			}

			$this->setRow($row);

			return $this->row;
		}

		return $this->row;
	}


	/**
	 * Test if agent exists in personnel members
	 * @return bool
	 */
	public function exists()
	{
		try {
			$row = $this->getRow();
		} catch(Exception $e)
		{
			return false;
		}

		return (false !== $row);
	}



	/**
	 * All agent requests sorted by creation date
	 *
	 * @return absences_RequestIterator
	 */
	public function getRequestIterator()
	{
		require_once dirname(__FILE__).'/request.class.php';
		$I = new absences_RequestIterator(array($this->getIdUser()));

		return $I;
	}



	/**
 	 * iterateur des droits d'un agent, vue utilisateur au moment de la demande
 	 * droit ouverts, ordonnes alphabetiquement
	 *
	 * @return absences_AgentRightUserIterator
	 */
	public function getAgentRightUserIterator()
	{
		require_once dirname(__FILE__).'/agent_right.class.php';
		$I = new absences_AgentRightUserIterator;
		$I->setAgent($this);

		return $I;
	}



	/**
	 * iterateur des droits d'un agent, vue gestionnaire
 	 * tout les droits associes a l'agent, ordonnes par annees, puis alphabetiquement
	 *
	 * @return absences_AgentRightManagerIterator
	 */
	public function getAgentRightManagerIterator()
	{
		require_once dirname(__FILE__).'/agent_right.class.php';
		$I = new absences_AgentRightManagerIterator;
		$I->setAgent($this);

		return $I;
	}





	/**
	 * Add a vacation right to the agent
	 * return true if inserted or if already exists
	 *
	 * @param	absences_Right	$right
	 * @return bool
	 */
	public function addRight(absences_Right $right)
	{
		global $babDB;
		$babDB->db_queryWem('INSERT INTO absences_users_rights (id_user, id_right) 
				VALUES ('.$babDB->quote($this->getIdUser()).', '.$babDB->quote($right->id).')');

		$this->addMovement(sprintf(absences_translate('The right %s has been assigned to the user %s'), $right->description, $this->getName()));

		return true;
	}


	/**
	 * Add fixed entry if the right is FIXED
	 *
	 * @throws absences_EntryException
	 *
	 * @param	absences_Right	$right
	 * @param 	bool			$notify
	 *
	 * @return bool
	 */
	public function addFixedEntry(absences_Right $right, $notify = true)
	{
		if( absences_Right::FIXED !== $right->getKind() ) {
		    return false;
		}


		if (absences_addFixedVacation($this->getIdUser(), $right))
		{
			if ($notify)
			{
				absences_notifyOnVacationChange(array($this->getIdUser()), $right->quantity, $right->date_begin_fixed, $right->date_end_fixed,  ABSENCES_FIX_ADD);
			}

			return true;
		}

		return false;
	}


	/**
	 * Add fixed entry if the right is FIXED
	 *
	 * @throws absences_EntryException
	 *
	 * @param	absences_Right	$right
	 * @param 	bool			$notify
	 *
	 * @return bool
	 */
	public function addFixedEntryIfNotExists(absences_Right $right, $notify = true)
	{
	    if( absences_Right::FIXED !== $right->getKind()) {
	        return false;
	    }

	    if (absences_isFixedCreated($this->getIdUser(), $right->id)) {
	        return false;
	    }

	    return $this->addFixedEntry($right, $notify);
	}



	/**
	 * Remove vacation right from agent
	 * Ne pas autoriser si le regime de l'agent est liee au droit
	 * return true if link removed or does not exists, return false if remove failed (ex because of collection assignement)
	 *
	 * @param	absences_Right	$right
	 * @return bool
	 */
	public function removeRight(absences_Right $right)
	{

		$collection = $this->getCollection();
		if ($collection->isLinkedToRight($right))
		{
			// empecher la suppression du lien agent-droit tant que le regime force le lien
			return false;
		}

		require_once dirname(__FILE__).'/agent_right.class.php';

		$agentRight = new absences_AgentRight();
		$agentRight->setAgent($this);
		$agentRight->setRight($right);

		if (!$agentRight->getRow())
		{
			// le lien n'existe pas
			return false;
		}


		$agentRight->delete();

		$this->addMovement(sprintf(absences_translate('The right %s has been unassigned from the user %s'), $right->description, $this->getName()));

		return true;
	}

	/**
	 * remove fixed entry if the right is FIXED
	 * @param	absences_Right	$right
	 * @param 	bool			$notify
	 * @return absences_Agent
	 */
	public function removeFixedEntry(absences_Right $right, $notify = true)
	{
		if( absences_Right::FIXED === $right->getKind() )
		{
			global $babDB;


			$res = $babDB->db_query("
			    SELECT
			         vet.*,
			         veet.quantity

			    from absences_entries_elem veet
			         left join absences_entries vet on veet.id_entry=vet.id
			    where
			         veet.id_right=".$babDB->quote($right->id)."
			         and vet.id_user=".$babDB->quote($this->getIdUser())
			);

			while ($arr = $babDB->db_fetch_assoc($res)) {

			    if (absences_removeFixedVacation( $arr['id'])) {
			        if ($notify) {
			            absences_notifyOnVacationChange(array($arr['id_user']), $arr['quantity'], $arr['date_begin'], $arr['date_end'], ABSENCES_FIX_DELETE);
			        }
			    }
			}

		}

		return $this;
	}



	/**
	 * Test if right is linked to collection
	 * @param absences_Right $right
	 * @return boolean
	 */
	public function isLinkedToRight(absences_Right $right)
	{
		require_once dirname(__FILE__).'/agent_right.class.php';

		$link = new absences_AgentRight;
		$link->setAgent($this);
		$link->setRight($right);
		$row = $link->getRow();

		if ($row)
		{
			return true;
		}
		return false;
	}


	/**
	 * Get organizational chart main entity or null
	 * @return array
	 */
	public function getMainEntity()
	{
		if (!isset($this->ocentity))
		{
			$id_oc = absences_getVacationOption('id_chart');

			require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
			$this->ocentity = bab_OCGetUserMainEntity($this->getIdUser(), $id_oc);
		}

		return $this->ocentity;
	}

	/**
	 * Get approbation scheme informations
	 * @return array
	 */
	public function getApprobation()
	{
		if (empty($this->id_sa))
		{
			return null;
		}

		if (null === $this->approbation)
		{
			require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
			$this->approbation = bab_WFGetApprobationInfos($this->id_sa);
		}

		return $this->approbation;
	}


	/**
	 * Get superior if the agent is in organizational chart
	 * @return array
	 */
	public function getSuperior()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/afincl.php';
	    
	    $id_oc = absences_getVacationOption('id_chart');
	    
	    $superior = bab_getSupervisor((int) $this->id_user, $id_oc, 0);
	    
	    if (!isset($superior['name']) || 0 === count($superior['name'])) {
	        return null;
	    }
	    
	    return $superior;
	}



	/**
	 * Get directory entry
	 * @return array
	 */
	public function getDirEntry()
	{
		if (!isset($this->direntry))
		{
			$this->direntry = bab_admGetDirEntry($this->getIdUser(), BAB_DIR_ENTRY_ID_USER);
		}

		return $this->direntry;
	}


	/**
	 * Date contenue dans la fiche d'annuaire
	 * formats autorises :
	 * 		DD/MM/YYYY
	 * 		DD/MM/YY
	 * 		D/M/YYYY
	 * 		DD-MM-YYYY
	 * 		DD-MM-YY
	 * 		D-M-YYYY
	 *
	 * @param string $fieldname
	 *
	 * @return string		ISO date
	 */
	public function getDirEntryDate($fieldname)
	{
		$direntry = $this->getDirEntry();
		if (!isset($direntry[$fieldname]))
		{
			return null;
		}

		$value = $direntry[$fieldname]['value'];

		if ('' === $value)
		{
			return null;
		}

		if (!preg_match('#([0-9]{1,2})(?:/|-)([0-9]{1,2})(?:/|-)([0-9]{2,4})#', $value, $m))
		{
			return null;
		}

		return sprintf('%04d-%02d-%02d', (int) $m[3], (int) $m[2], (int) $m[1]);
	}



	/**
	 * @return bab_dirEntryPhoto
	 */
	private function getDirEntryPhoto()
	{
		$entry = $this->getDirEntry();
		if (isset($entry['jpegphoto']['photo']))
		{
			return $entry['jpegphoto']['photo'];
		}

		return null;
	}

	/**
	 * Get icon url only if thumbnailer is present (16x16)
	 * @return string
	 */
	public function getIcon()
	{
		$T = bab_functionality::get('Thumbnailer');

		if ($T && $photo = $this->getDirEntryPhoto())
		{
			$photo->setThumbSize(16, 16);
			return $photo->getUrl();
		}

		return null;
	}

	/**
	 * Get a small photo to display in agent page or requests page (64x64)
	 * if the thumbnailer is not present photo is not resized
	 * @return string
	 */
	public function getPhoto()
	{
		if ($photo = $this->getDirEntryPhoto())
		{
			$photo->setThumbSize(64, 64);
			return $photo->getUrl();
		}

		return null;
	}



	/**
	 * @return bool
	 */
	public function isInPersonnel()
	{
		global $babDB;

		try {

			$id_user = $this->getIdUser();

		} catch(Exception $e) {
			bab_debug($e->getMessage());
			return false;
		}

		$res = $babDB->db_query("select id from absences_personnel where id_user='".$babDB->db_escape_string($id_user)."'");
		if( $res && $babDB->db_num_rows($res) > 0)
		{
			return true;
		}

		return false;
	}

	/**
	 * Test if the user have access to at least one vacation right
	 * si les droits accessibles ont un solde a 0, la fonction renvoi true malgre tout, seul le nombre de droit accessible est teste
	 * (certains droits peuvent accepter les soldes negatifs)
	 * @return bool
	 */
	public function haveRights()
	{
		$I = $this->getAgentRightUserIterator();

		return ($I->count() > 0);
	}



	/**
	 * @return bool
	 */
	public function isManager()
	{
		return bab_isAccessValid('absences_managers_groups', 1, $this->getIdUser());

	}

	/**
	 * Test si l'agent est un approbateur
	 * @param	int	$id_user	test si l'agent est l'approbateur d'un demandeur en particulier
	 * @return bool
	 */
	public function isApprover($id_user = null)
	{
		global $babDB;

		$arrchi = bab_getWaitingIdSAInstance($this->getIdUser());
		$query = "SELECT idfai from ".ABSENCES_ENTRIES_TBL."  where status='' AND idfai IN(".$babDB->quote($arrchi).")";
		if (isset($id_user))
		{
			$query .= " AND id_user=".$babDB->quote($id_user);
		}

		$res = $babDB->db_query($query);
		if( $res && $babDB->db_num_rows($res) > 0)
		{
			return true;
		}

		return false;
	}




	/**
	 * Liste des entites gerees par la gestion deleguee
	 * @return array
	 */
	public function getManagedEntities()
	{
		if (!isset($this->managed_entities))
		{
			$id_oc = absences_getVacationOption('id_chart');

			require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';

			$userentities = bab_OCGetUserEntities($this->getIdUser(), $id_oc);
			absences_addCoManagerEntities($userentities, $this->getIdUser());

			$this->managed_entities = array();

			foreach ($userentities['superior'] as $entity)
			{
				$this->managed_entities[] = $entity;
				$children = bab_OCGetChildsEntities($entity['id'], $id_oc);
				$this->managed_entities = array_merge($this->managed_entities, $children);
			}

		}
		return $this->managed_entities;
	}

	/**
	 * Access a la gestion delegee
	 * @return bool
	 */
	public function isEntityManager()
	{
		$arr = $this->getManagedEntities();

		return (count($arr) > 0);
	}


	/**
	 * Gestionnaire delegue de l'entite
	 * @param int $id_entity
	 * @return bool
	 */
	public function isEntityManagerOf($id_entity)
	{
		$managed_entities = $this->getManagedEntities();

		foreach($managed_entities as $entity)
		{
			if ($id_entity == $entity['id'])
			{
				return true;
			}
		}

		return false;
	}



	/**
	 * Tester si l'agent est le supperieur d'un autre agent
	 * @param absences_Agent $agent
	 * @return bool
	 */
	public function isSuperiorOf(absences_Agent $agent)
	{
	    if ($agent->getIdUser() === $this->getIdUser())
	    {
	        return false;
	    }

		$user_entities_id = $agent->getMemberEntityIds();
		$managed_entities = $this->getManagedEntities();

		foreach($managed_entities as $entity)
		{
			if (isset($user_entities_id[$entity['id']]))
			{
				return true;
			}
		}
		return false;
	}




	/**
	 * Liste des entites autorises pour les visualisation de planning
	 * @return array
	 */
	public function getCalendarEntities()
	{
		if (!isset($this->calendar_entities))
		{
			global $babDB;

			$id_oc = absences_getVacationOption('id_chart');

			$this->calendar_entities = array();
			$res = $babDB->db_query("SELECT id_entity FROM absences_planning WHERE id_user=".$babDB->quote($this->getIdUser()));
			while ($arr = $babDB->db_fetch_assoc($res)) {
				$this->calendar_entities[$arr['id_entity']] = $arr['id_entity'];
				$tmp = bab_OCGetChildsEntities($arr['id_entity'], $id_oc);

				foreach($tmp as $entity) {
					$this->calendar_entities[$entity['id']] = $entity['id'];
				}
			}
		}

		return $this->calendar_entities;
	}

	/**
	 * Tester si l'agent a les droits pour voir le planning d'un autre agent
	 * @param absences_Agent $agent
	 * @return bool
	 */
	public function canViewCalendarOf(absences_Agent $agent)
	{
	    if ($this->isManager())
	    {
	        return true;
	    }
	    
	    if ($this->isSuperiorOf($agent))
	    {
	        return true;
	    }
	    
	    if ($this->isApprover($agent->getIdUser()))
	    {
	        return true;
	    }
	    
	    if (!$agent->isInPersonnel())
		{
			return false;
		}

		if ($this->getIdUser() === $agent->getIdUser())
		{
			return true;
		}
		
		if ($this->isSharedCalendarEntity($agent))
		{
			return true;
		}

		if ($this->isInSameEntityPlanning($agent))
		{
			return true;
		}

		return false;
	}




	/**
	 * listes des entities de l'agent
	 * @return array
	 */
	public function getMemberEntities()
	{
		$id_oc = absences_getVacationOption('id_chart');
		$arr = bab_OCGetUserEntities($this->getIdUser(), $id_oc);
		return array_merge($arr['superior'], $arr['temporary'], $arr['members']);
	}

	/**
	 * Liste des ID des entites de l'agent
	 * @return array
	 */
	public function getMemberEntityIds()
	{
		$user_entities_id = array();
		foreach($this->getMemberEntities() as $entity)
		{
			$user_entities_id[$entity['id']] = $entity['id'];
		}

		return $user_entities_id;
	}


	/**
	 * Tester si l'agent a acces au planning de l'agent passe en parametre en passant par le partage de planning des entites
	 * @param	absences_Agent $agent
	 * @return bool
	 */
	public function isSharedCalendarEntity(absences_Agent $agent)
	{
		$calendar_entities = $this->getCalendarEntities(); // entites agendas autorises en partage
		$user_entities = $agent->getMemberEntities(); // entites de l'utilisteur que l'on cherche a visualiser

		foreach($user_entities as $entity) {

			$id_entity = (int) $entity['id'];

			if (array_key_exists($id_entity, $calendar_entities)) {
				return true;
			}
		}

		return false;
	}



	/**
	 * Tester si l'agent passe en parametre est dans le  meme planning de service et que la visualisation du planning de service est activee
	 * @param absences_Agent $agent
	 * @return bool
	 */
	public function isInSameEntityPlanning(absences_Agent $agent)
	{
		if (0 === (int) absences_getVacationOption('entity_planning'))
		{
			return false;
		}


		if ($this->getIdUser() === $agent->getIdUser())
		{
			return true;
		}

		$myEntity = $this->getMainEntity();
		$agentEntity = $agent->getMainEntity();

		if ($myEntity['id'] === $agentEntity['id'])
		{
			return true;
		}

		return false;
	}
	
	
	public function canViewCustomPlanning($id_planning)
	{
	    return bab_isAccessValid('absences_custom_planning_groups', $id_planning,  $this->getIdUser());
	}


	/**
	 * Tester si l'agent peut voir le planning d'une autre entite
	 *
	 * @return bool
	 */
	public function canViewEntityPlanning($id_entity = null)
	{
	    if ($this->isManager()) {
	        return true;
	    }
	    
	    
		// cas possible avec le partage de planning

		$calendars = $this->getCalendarEntities();

		if (isset($calendars[$id_entity]))
		{
			return true;
		}

		// si supperieur de l'entite

		if ($this->isEntityManagerOf($id_entity))
		{
			return true;
		}

		// si planning du service

		$myEntity = $this->getMainEntity();
		if ((int) $id_entity === (int) $myEntity['id'] && 1 === (int) absences_getVacationOption('entity_planning')) {
		    return true;
		}


		return false;
	}

	/**
	 * Tester si l'agent peut voir le planning de son entite principale
	 */
	public function canViewMainEntityPlanning()
	{
		$myEntity = $this->getMainEntity();

		return $this->canViewEntityPlanning($myEntity['id']);
	}



	/**
	 * Declaration de jours travailles donnant droit a recuperation
	 * @return bool
	 */
	public function canCreateWorkperiodRecoverRequest()
	{
		$workperiod = (bool) absences_getVacationOption('workperiod_recover_request');

		if (!$workperiod)
		{
			return false;
		}

		if (!$this->isInPersonnel())
		{
			return false;
		}

		return true;
	}


	/**
	 * @return absences_AgentCet
	 */
	public function Cet()
	{
		if (!isset($this->cet))
		{
			require_once dirname(__FILE__).'/agent_cet.class.php';
			$this->cet = new absences_AgentCet($this);

		}

		return $this->cet;
	}


	/**
	 * Approbation sheme ID
	 * @return int
	 */
	public function getApprobationId()
	{
		return (int) $this->id_sa;
	}


	/**
	 * Approbation sheme ID from recover request
	 * @return int
	 */
	public function getRecoverApprobationId()
	{
		$id_sa_recover = (int) $this->id_sa_recover;
		if (0 === $id_sa_recover)
		{
			return $this->getApprobationId();
		}

		return $id_sa_recover;
	}


	/**
	 * Approbation sheme ID from cet request
	 * @return int
	 */
	public function getCetApprobationId()
	{
		$id_sa_cet = (int) $this->id_sa_cet;
		if (0 === $id_sa_cet)
		{
			return $this->getApprobationId();
		}

		return $id_sa_cet;
	}


	/**
	 * Create reports for expired rights of agent
	 *
	 */
	public function createReports()
	{
		$res = $this->getAgentRightManagerIterator();

		foreach($res as $agentRight)
		{
			/*@var $agentRight absences_AgentRight */

			$agentRight->createReport();
		}

	}
	
	

	private function getRecoveryRgroup($quantity_unit)
	{
	    global $babDB;
	
	    $res = $babDB->db_query("SELECT id FROM absences_rgroup WHERE recover='1' AND quantity_unit=".$babDB->quote($quantity_unit));
	
	    if ($babDB->db_num_rows($res) == 0)
	    {
	        return 0;
	    }
	
	    $row = $babDB->db_fetch_assoc($res);
	
	    return $row['id'];
	}
	
	
	
	/**
	 * Create a recovery right for the agent
	 * and return the id_right
	 * 
	 * @param string $begin            ISO date YYYY-MM-DD
	 * @param string $end              ISO date YYYY-MM-DD
	 * @parap string $description
	 * @param float $quantity
	 * @param string $quantity_unit D|H
	 * 
	 * @return int
	 */
	public function createRecoveryRight($begin, $end, $description, $quantity, $quantity_unit)
	{
        require_once dirname(__FILE__).'/right.class.php';
        global $babDB;


	    $babDB->db_query('INSERT INTO absences_rights
				(
					kind,
					description,
	                createdOn,
					date_entry,
					date_begin,
					date_end,
					cbalance,
					id_type,
					id_rgroup,
					quantity,
					quantity_unit,
					use_in_cet,
					no_distribution,
        	        date_begin_valid,
        	        date_end_valid,
	                hide_empty
				)
	    
			VALUES (
					'.$babDB->quote(absences_Right::RECOVERY).',
					'.$babDB->quote($description).',
	                CURDATE(),
					CURDATE(),
					'.$babDB->quote($begin).',
					'.$babDB->quote($end).',
					'.$babDB->quote('N').',
					'.$babDB->quote(absences_getRecoveryType()).',
					'.$babDB->quote($this->getRecoveryRgroup($quantity_unit)).',
					'.$babDB->quote($quantity).',
					'.$babDB->quote($quantity_unit).',
					'.$babDB->quote(0).',
					'.$babDB->quote(1).',
					'.$babDB->quote($begin).',
					'.$babDB->quote($end).',
	                '.$babDB->quote(1).'
				)');
	    
	    $id_right = $babDB->db_insert_id();
	    
	    
	    $babDB->db_query('INSERT INTO absences_rights_rules
				(
					id_right
				)
			VALUES
				(
					'.$babDB->quote($id_right).'
				)
	    
		');
	    
	    
	    $babDB->db_query('INSERT INTO absences_rights_inperiod
				(
					id_right,
					period_start,
					period_end,
					right_inperiod
				)
			VALUES
				(
					'.$babDB->quote($id_right).',
					'.$babDB->quote($begin).',
					'.$babDB->quote($end).',
					'.$babDB->quote(1).'
				)
		');
	    
	    
	    $babDB->db_query('INSERT INTO absences_users_rights
			(
				id_right,
				id_user
			)
			VALUES
			(
				'.$babDB->quote($id_right).',
				'.$babDB->quote($this->getIdUser()).'
			)
		');
	    
	    return $id_right;
	}



	/**
	 * Movements related to the agent
	 * @return absences_MovementIterator
	 */
	public function getMovementIterator()
	{
		require_once dirname(__FILE__).'/movement.class.php';

		$I = new absences_MovementIterator();
		$I->setAgent($this);

		return $I;
	}


	/**
	 *
	 * @param string $message	Generated message
	 * @param string $comment	Author comment
	 */
	public function addMovement($message, $comment = '')
	{
		require_once dirname(__FILE__).'/movement.class.php';

		$movement = new absences_Movement();
		$movement->message = $message;
		$movement->comment = $comment;
		$movement->setAgent($this);
		$movement->save();
	}


	public function delete()
	{
		global $babDB;

		$id_user = $this->getIdUser();
		absences_clearUserCalendar($id_user);

		$I = $this->getRequestIterator();
		foreach($I as $request)
		{
			$request->delete();
		}

		$babDB->db_query("delete from ".ABSENCES_USERS_RIGHTS_TBL." where id_user='".$babDB->db_escape_string($id_user)."'");
		$babDB->db_query("delete from ".ABSENCES_PERSONNEL_TBL." where id_user='".$babDB->db_escape_string($id_user)."'");

		$babDB->db_query("delete from absences_comanager where id_user='".$babDB->db_escape_string($id_user)."'");
		$babDB->db_query("delete from absences_planning where id_user='".$babDB->db_escape_string($id_user)."'");



	}



	public function setEmails($emails)
	{
		global $babDB;
		$id_user = $this->getIdUser();

		$babDB->db_query('UPDATE absences_personnel SET emails='.$babDB->quote($emails).' WHERE id_user='.$babDB->quote($id_user));
	}

	/**
	 * @return array
	 */
	public function getEmails()
	{
		return preg_split('/\s*,\s*/', $this->emails);
	}













	/**
	 * Set organization of agent from the "organizationname" field of the directory entry
	 * if possible or return false if the organization does not exists
	 *
	 * @return bool
	 */
	public function setOrganizationFromDirEntry()
	{
	    global $babDB;
	    $entry = $this->getDirEntry();
	    
	    if (!isset($entry)||false === $entry||0 === count($entry)) {
	        throw new Exception(absences_translate('Directory entry not found'));
	    }
	    
	    if (empty($entry['organisationname']) || empty($entry['organisationname']['value'])) {
	        throw new Exception(absences_translate('Organization name not found in directory entry, the organization associated  to the absence personnel member was not updated, nonetheless, the directory entry was sucessfully saved'));
	    }
	    
	    $organization = absences_Organization::getByName($entry['organisationname']['value']);

	    if (!isset($organization)) {
	        throw new Exception(
	            sprintf(
	                absences_translate('Organization not found in absences for "%s"'), 
	                $entry['organisationname']['value']
		        )
		    );
	    }

	    $this->setOrganization($organization);

	    $babDB->db_query('UPDATE absences_personnel SET
					id_organization='.$babDB->quote($organization->id).'
				WHERE
					id_user='.$babDB->quote($this->getIdUser()));

	    return true;
	}
}








class absences_AgentIterator extends absences_Iterator
{
    /**
     * @var absences_Collection
     */
	protected $collection;

	/**
	 * @var absences_Right
	 */
	protected $right;

	/**
	 * @var absences_Organization
	 */
	protected $organization;

	/**
	 * @var array
	 */
	public $exclude_users;

	/**
	 * Create an agent iterator with agent associated to a vacation right
	 * @param absences_Right $right
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
	}

	/**
	 * Create an agent iterator with agents associated to a collection
	 * @param absences_Collection $collection
	 */
	public function setCollection(absences_Collection $collection)
	{
		$this->collection = $collection;
	}

	/**
	 * Create the agent iterator with agents associated to an organization
	 * @param absences_Organization $organization
	 */
	public function setOrganization(absences_Organization $organization)
	{
	    $this->organization = $organization;
	}


	public function getObject($data)
	{
		$agent = new absences_Agent;
		$agent->setRow($data);


		if (isset($this->collection))
		{
			$agent->setCollection($this->collection);
		}

		if (isset($this->organization))
		{
		    $agent->setOrganization($this->organization);
		}

		return $agent;
	}

	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;

			$query = '
				SELECT
					a.*
				FROM
					absences_personnel a
			';

			$where = array();

			if (isset($this->organization)) {
			    $where[] = 'a.id_organization='.$babDB->quote($this->organization->id).'';
			}

			if (isset($this->collection)) {
				$where[] = 'a.id_coll='.$babDB->quote($this->collection->id).'';
			}

			if (isset($this->right)) {
				$query .= ', absences_users_rights ur';
				$where[] = 'ur.id_user=a.id_user';
				$where[] = 'ur.id_right='.$babDB->quote($this->right->id);
			}

			if (isset($this->exclude_users)) {
			    $where[] = 'a.id_user NOT IN('.$babDB->quote($this->exclude_users).')';
			}

			if ($where) {
			     $query.= ' WHERE '.implode(' AND ', $where);
			}

			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($query));
		}
	}

}