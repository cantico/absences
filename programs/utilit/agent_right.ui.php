<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/right.ui.php';


bab_Widgets()->includePhpClass('Widget_Form');



class absences_AgentRightEditor extends absences_RightBaseEditor 
{
	/**
	 * 
	 * @var absences_AgentRight
	 */
	protected $agentRight;
	

	/**
	 * @var Widget_RadioSet
	 */
	protected $quantity_type;
	
	
	public function __construct(absences_AgentRight $agentright)
	{
		$W = bab_Widgets();
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$this->agentRight = $agentright;
		
		bab_functionality::includefile('Icons');
		
		$this->setName('agent_right');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->addClass(Func_Icons::ICON_LEFT_24);
		$this->colon();
		
		$this->setCanvasOptions($this->Options()->width(70,'em'));
		
		$this->addFields();
		$this->loadFormValues();
		
		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}
	
	
	protected function addFields()
	{
		$this 
			->addItem($this->quantity_type())
			->addItem($this->quantity())
			->addItem($this->availability_rules())
			->addItem($this->renewal())
		;
		
		
		
		$right = $this->agentRight->getRight();
		$rightCet = $right->getRightCet();
		if ($rightCet->getRow())
		{
			$this->addItem($this->saving());
		}
		
		$this->addItem($this->movement_comment());
	}
	
	
	/**
	 * 
	 */
	protected function movement_comment()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			absences_translate('Comment on modification for history'), 
			$W->TextEdit()->setLines(2)->setMandatory(true, absences_translate('The comment is mandatory'))->addClass('widget-fullwidth'), 
			__FUNCTION__
		);
	}
	
	
	
	
	protected function loadFormValues()
	{
		
		$this->setHiddenValue('agent_right[id]', $this->agentRight->id);
		
		if (isset($_POST['agent_right']))
		{
			$values = $_POST['agent_right'];
		} else if (isset($this->agentRight))
		{
			$values = $this->agentRight->getRow();
			$right = $this->agentRight->getRight();
			$values['quantity'] = absences_editQuantity($this->agentRight->getQuantity(), $right->quantity_unit);

			$values['quantity_type'] = 'right';
			
			if ('' !== (string) $this->agentRight->quantity) {
			    $values['quantity_type'] = 'agent';
			}
			

		} else {
			return;
		}

		
		$this->setValues(array('agent_right' => $values));
	}
	
	
	protected function addButtons()
	{
		$W = bab_Widgets();
		
		$button = $W->FlowItems(
			$W->SubmitButton()->setName('cancel')->setLabel(absences_translate('Cancel')),
			$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');
		
		$this->addItem($button);
	}
	
	
	/**
	 * Criteres de disponibilite d'un droit
	 */
	protected function availability_rules()
	{
		$W = bab_Widgets();
	
	
	
		$input_date = $this->by_request_input_date();
		
		$accordions = $W->Accordions()
			->addPanel(absences_translate('Availability depends on the date of the vacation request input'), $input_date)
			->addPanel(absences_translate('Availability depends on the requested period dates'), $this->by_requested_period())
		;
		
		// $accordions->setOpenPanel($input_date->getId());
		
		return $accordions;
	}
	
	
	
	/**
	 * Availability depends on the requested period dates
	 */
	protected function by_requested_period()
	{
		$W = bab_Widgets();
		$period = $W->PeriodPicker()->setNames('inperiod_start', 'inperiod_end');
		
		$validoverlap = $W->LabelledWidget(
				absences_translate('Allow overlap between the request period and the test periods'),
				$W->CheckBox(),
				'validoverlap'
		);
	
		return $W->VBoxItems(
				$W->Label(absences_translate('The right is available if the vacation request is in the period')),
				$period,
				$validoverlap
		)->setVerticalSpacing(1,'em');
	}
	
	
	
	protected function getSign($value)
	{
	    if ($value >= 0) {
	        return '+';
	    }
	    
	    return '-';
	}
	
	
	protected function quantity_type()
	{
	    $W = bab_Widgets();
	    
	    $right = $this->agentRight->getRight();
	    $q = absences_quantity($right->quantity, $right->quantity_unit);
	    $nb = 1;
	    
	    if ($increment = $right->getIncrementQuantity()) {
	        if (0 !== (int) round(100 * $increment)) {
	            $q .= sprintf(absences_translate(' %s %s by monthly update'), $this->getSign($increment), absences_quantity(abs($increment), $right->quantity_unit));
	            $nb++;
	        }
	    }
	    
	    if ($dynamic = $this->agentRight->getDynamicQuantity()) {
	        if (0 !== (int) round(100 * $dynamic)) {
	            $q .= sprintf(absences_translate(' %s %s by consumption'), $this->getSign($increment), absences_quantity(abs($dynamic), $right->quantity_unit));
	            $nb++;
	        }
	    }
	    
	    if ($nb > 1) {
	        $q .= ' = '.absences_quantity($this->agentRight->getRightQuantity(), $right->quantity_unit);
	    }
	    
	    
	    $this->quantity_type = $W->RadioSet()
	       ->setName('quantity_type')
	       ->addOption('right', sprintf(absences_translate('Use the quantity provided by the right (%s)'), $q))
	       ->addOption('agent', absences_translate('Use a specific value'));
	    
	    return $this->quantity_type;
	}
	
	
	
	/**
	 * quantity
	 * quantite initiale + les ajustement dynamiques du droits (variables par mois + droits dynamiques)
	 * 
	 * @return Widget_FlowLayout
	 */
	protected function quantity()
	{
		$W = bab_Widgets();
	
		$lineedit = $W->LineEdit()->setSize(4)->setMaxSize(5)->setName('quantity');
		
		
		$right = $this->agentRight->getRight();
		$q = array();
		
		$increment = $this->agentRight->getAgentIncrementQuantity();
		if ($increment || $right->getIncrementQuantity()) {
		    $q[] = sprintf(absences_translate('%s by monthly update'), absences_quantity($increment, $right->quantity_unit));
		}
		 
		if ($dynamic = $this->agentRight->getDynamicQuantity()) {
		    if (0 !== (int) round(100 * $dynamic)) {
		        $q[] = sprintf(absences_translate('%s by consumption'), absences_quantity($dynamic, $right->quantity_unit));
		    }
		}
		
		$description = null;
		if ($q) {
		    $description = sprintf(absences_translate('The input quantity include %s'), implode(', ', $q));
		}
		
		
		$quantity = $W->LabelledWidget(
		    absences_translate('Quantity available for the user'),
		    $lineedit,
		    'quantity',
		    $description,
		    $this->agentRight->getRight()->getUnitLabel()
	    );
		
		
		$this->quantity_type->setAssociatedDisplayable($quantity, array('agent'));
		
		return $quantity;
	}
	
	
	
	protected function renewal()
	{
	    $W = bab_Widgets();
	    
	    return $W->LabelledWidget(absences_translate('Include this right in the next yearly renewal'), $W->CheckBox(), __FUNCTION__);
	}
	
}	





class absences_QuantityModificationCardFrame extends absences_CardFrame
{
    protected function createTable()
    {
        $W = bab_Widgets();
        bab_functionality::includeOriginal('Icons');
        
        $table = $W->BabTableView();
        $table->addClass('absences-quantity-modification-table');
        $table->addClass(Func_Icons::ICON_LEFT_16);
        
        $table->addColumnClass(0, 'date');
        
        return $table;
    }
    
    
    protected function quantityLabel($quantity, $unit)
    {
        $W = bab_Widgets();
        
        $sign = '+';
        if ($quantity < 0) {
            $sign = '-';
        }
        
        return $W->Label($sign.absences_quantity(abs($quantity), $unit));
    }
}








class absences_IncrementCardFrame extends absences_QuantityModificationCardFrame
{
    protected $agentright;

    public $contain_rows = false;

    public function __construct(absences_AgentRight $agentright, $layout = null)
    {
        parent::__construct(null, $layout);

        $this->agentright = $agentright;


        $this->addClass('absences-dynamicright-cardframe');
        $this->addClass(Func_Icons::ICON_LEFT_16);

        $this->loadPage();
    }



    protected function loadPage()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
        $W = bab_Widgets();


        $this->addItem($W->Title(absences_translate('Quantity modifications by monthly right modification')));

        
        $addon = bab_getAddonInfosInstance('absences');
        $right = $this->agentright->getRight();
        $table = $this->createTable();

        $row = 0;
        
        $table->addItem($W->Label(absences_translate('Initial quantity'))->addClass('right')->colon(), $row, 0);
        $table->addItem($W->Label(absences_quantity($this->agentright->getInitialQuantity(), $right->quantity_unit)), $row, 1);
        $row++;
        
        
        $table->addHeadRow($row);

        $table->addItem($W->Label(absences_translate('Modification date')), $row, 0);
        $table->addItem($W->Label(absences_translate('Quantity added to the right')), $row, 1);
        $row++;

        $I = $this->agentright->getIncrementIterator();

        if ($I->count() > 0)
        {
            $this->contain_rows = true;
        }
        
        
        if ($I instanceof absences_IncrementAgentRightIterator) {
            $this->addItem($W->Icon(
                absences_translate('The montly update are added each month from the specific user quantity because the user quantity has been modified. To get back to the default value, the quantity field must be set empty'),
                Func_Icons::STATUS_DIALOG_INFORMATION
            ));
        }
        
        if ($I instanceof absences_IncrementRightIterator) {
            $this->addItem($W->Icon(
                absences_translate('The montly update are added each month from the right quantity, if a specific quantity is set for the user, such changes will no longer be used'),
                Func_Icons::STATUS_DIALOG_INFORMATION
            ));
        }
        

        

        foreach($I as $increment)
        {
            $table->addItem($W->Label(bab_shortDate(bab_mktime($increment->createdOn))), $row, 0);
            $table->addItem($this->quantityLabel($increment->quantity, $right->quantity_unit), $row, 1);
            	
            $row++;
        }


        

        $this->addItem($table);
    }
}
















class absences_DynamicRightCardFrame extends absences_QuantityModificationCardFrame
{
	protected $agentright;
	
	public $contain_rows = false;
	
	public function __construct(absences_AgentRight $agentright, $layout = null)
	{
		parent::__construct(null, $layout);
	
		$this->agentright = $agentright;

		
		$this->addClass('absences-dynamicright-cardframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);
	
		$this->loadPage();
	}
	
	
	
	protected function loadPage()
	{
	    require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$W = bab_Widgets();
		bab_functionality::includeOriginal('Icons');
		
		
		$this->addItem($W->Title(absences_translate('Quantity modifications depending on the consumed quantity')));
		
		$right = $this->agentright->getRight();
		$t = $right->getDynamicTypes();
		
		$this->addItem($W->Label(sprintf(absences_translate('%s has consumed %s of %s beetween the %s and the %s'), 
				$this->agentright->getAgent()->getName(),
				absences_quantity(round($this->agentright->getDynamicTotalConfirmedDays(), 2), 'D'),
				implode(', ', $t),
				bab_shortDate(bab_mktime($right->dynconf_begin), false),
				bab_shortDate(bab_mktime($right->dynconf_end), false)
		)));
		
		
		$table = $this->createTable();
		

        $row = 0;
        
        $increment_quantity = $this->agentright->getIncrementQuantity();
        $initial_title = 0 === (int) round(100*$increment_quantity) ? absences_translate('Initial quantity') : absences_translate('Initial quantity + monthly updates');
        $table->addItem($W->Label($initial_title)->addClass('right')->colon(), $row, 0);
        $initial = $this->agentright->getInitialQuantity();
        $initial += $increment_quantity;
        $table->addItem($W->Label(absences_quantity($initial, $right->quantity_unit)), $row, 1, 1, 2);
        $row++;
        
        
        $table->addHeadRow($row);
		
		$table->addItem($W->Label(absences_translate('Modification date')), $row, 0);
		$table->addItem($W->Label(absences_translate('Quantity added to the right')), $row, 1);
		$table->addItem($W->Label(absences_translate('Vacation request that caused the change')), $row, 2);
		$row++;
		
		$right = $this->agentright->getRight();
		
		$I = $this->agentright->getDynamicRightIterator();
		
		if ($I->count() > 0)
		{
			$this->contain_rows = true;
		}
		
		$addon = bab_getAddonInfosInstance('absences');
		
		foreach($I as $dynamicRight)
		{
			/* @var $dynamicRight absences_DynamicRight */
		    
		    $request_url = new bab_url($addon->getUrl().'vacadmb');
		    $request_url->idx = 'morvw';
		    $request_url->id = $dynamicRight->id_entry;
		    $request_url->popup = 1;
			
			$table->addItem($W->Label(bab_shortDate(bab_mktime($dynamicRight->createdOn))), $row, 0);
			$table->addItem($this->quantityLabel($dynamicRight->quantity, $right->quantity_unit), $row, 1);
			$table->addItem($W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $request_url->toString())->setOpenMode(Widget_Link::OPEN_POPUP), $row, 2);
			
			$row++;
		}
		
		
		$this->addItem($table);
	}
}





class absences_AgentRightMovementFrame extends absences_CardFrame
{
    public $count;
    
    public function __construct(absences_AgentRight $agentRight)
    {
        $W = bab_Widgets();
        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(.5, 'em'));
        
        
        
        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass(Func_Icons::ICON_LEFT_24);
        
        $this->setCanvasOptions($this->Options()->width(70,'em'));
        
        $this->addItem($W->Title(absences_translate('History')));
        
        $res = $agentRight->getMovementIterator();
        $this->count = $res->count();
        
        foreach ($res as $movement) {
            
            $this->addMovement($movement);
        }
    }
    
    
    protected function addMovement(absences_Movement $movement)
    {
        $W = bab_Widgets();
        
        $date = $W->Label(bab_shortDate(bab_mktime($movement->createdOn)));
        $message = $W->RichText($movement->message)->setRenderingOptions(BAB_HTML_ALL ^ BAB_HTML_P);
        
        $date->setTitle(bab_getUserName($movement->id_author));
        
        if ($movement->comment) {
            $message = $W->VBoxItems(
                $message,
                $W->FlowItems(
                    $W->Label(absences_translate('Comment'))->colon(),
                    $W->RichText($movement->comment)->setRenderingOptions(BAB_HTML_ALL ^ BAB_HTML_P)
                )->setSpacing(.8, 'em')
            )->setSpacing(.5, 'em');
        }
        
        
        $this->addItem($W->HBoxItems($date, $message));
    }
}