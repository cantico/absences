<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/entry.class.php';
require_once dirname(__FILE__).'/entry_elem.class.php';

/**
 * Add a fixed vacation for a user
 * appelle lors de la creation/modification d'un droit de conge pre attribue
 * 
 * @param int $id_user
 * @param absences_Right $right
 * @param string $remarks
 * 
 * @throws absences_EntryException
 * 
 * @return bool
 */
function absences_addFixedVacation($id_user, absences_Right $right, $remarks = '')
{

	
	$entry = new absences_Entry;
	$entry->id_user = $id_user;
	$entry->date_begin = $right->date_begin_fixed;
	$entry->date_end = $right->date_end_fixed;
	$entry->comment = $remarks;
	$entry->status = 'Y';
	$entry->creation_type = absences_Entry::CREATION_FIXED;
	
	$elem = new absences_EntryElem();
	$elem->setEntry($entry);
	$elem->id_right = $right->id;
	$elem->quantity = $right->quantity;
	$elem->date_begin = $right->date_begin_fixed;
	$elem->date_end = $right->date_end_fixed;
	$entry->addElement($elem);
	
	
	
	try {
		$validity = $entry->checkValidity(false);
	} catch(absences_EntryException $e)
	{
		if (!$e->blocking)
		{
			// ignorer les erreur non bloquantes
			$validity = true;
		} else {
			
			throw $e;
		}

	}
	
	
	if ($validity)
	{
		$entry->save();
		$entry->saveElements();
		$entry->createPlannedPeriods();
		$entry->savePlannedPeriods();
		
		$entry->addElementsMovements(sprintf(absences_translate('Add the fixed vacation right %s to the user %s'), $right->description, bab_getUserName($id_user)));
		
		$entry->updateCalendar();
		absences_createPeriod($entry->id);
		
		$entry->applyDynamicRight();
		
		return true;
	}
	
	// on doit etre sur qu'il n'y a pas de demande existante
	// ile ne devrais pas y en avoir normalement
	if (false === absences_updateFixedVacation($id_user, $right)) {
	
	    return false; // tout est normal
	}
	
	// une mise a jour a ete faite
	return false;
}



/**
 * Update dates and quantity of a fixed vacation for a user
 * appelle lors de la creation/modification d'un droit de conge pre attribue
 * 
 * @param	int		             $id_user
 * @param 	absences_Right		 $right
 * 
 * @return bool
 */
function absences_updateFixedVacation($id_user, absences_Right $right)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	
	$res = $babDB->db_query("select 
		vet.id 	entry, 
		veet.id entryelem 
	from ".ABSENCES_ENTRIES_ELEM_TBL." veet 
		left join ".ABSENCES_ENTRIES_TBL." vet 
		on veet.id_entry=vet.id 
		where veet.id_right=".$babDB->quote($right->id)." 
			and vet.id_user=".$babDB->quote($id_user)."
	");
	
	
	if (0 === $babDB->db_num_rows($res)) {
		return false;
	}

	$entry = null;

	while( $arr = $babDB->db_fetch_assoc($res))
	{
		if (isset($entry))
		{
			// already updated, all other entries must not exists
			absences_delete_request($arr['entry']);
			continue;
		}
		
		if (absences_Right::FIXED !== (int) $right->kind) {
		    continue;
		}
		
		$entry = new absences_Entry;
		$entry->id = (int) $arr['entry'];
		$entry->date_begin = $right->date_begin_fixed;
		$entry->date_end = $right->date_end_fixed;
		$entry->id_user = $id_user;
		$entry->status = 'Y';
		$entry->creation_type = absences_Entry::CREATION_FIXED;
		
		$entry_elem = new absences_EntryElem;
		$entry_elem->id = (int) $arr['entryelem'];
		$entry_elem->id_right = $right->id;
		$entry_elem->id_entry = $entry->id;
		$entry_elem->quantity = $right->quantity;
		$entry_elem->date_begin = $right->date_begin_fixed;
		$entry_elem->date_end = $right->date_end_fixed;
		$entry_elem->setEntry($entry);
		
		$entry->addElement($entry_elem);
		
		
		try {
			$validity = $entry->checkValidity();
		} catch(absences_EntryException $e)
		{
			if (!$e->blocking)
			{
				// ignorer les erreur non bloquantes
				$validity = true;
			} else {
		
				throw $e;
			}
		
		}
		

		$begin = BAB_DateTime::fromIsoDateTime($entry->date_begin);
		$end = BAB_DateTime::fromIsoDateTime($entry->date_end);
		
		
		if ($validity)
		{
			$entry->save();
			$entry->saveElements();
			$entry->createPlannedPeriods();
			$entry->savePlannedPeriods();
			
			$entry->addElementsMovements(sprintf(absences_translate('Update fixed vacation %s for the user %s'), $right->description, bab_getUserName($id_user)));
			
			$entry->applyDynamicRight();

				
			// try to update event copy in other backend (caldav)
			absences_updatePeriod($arr['entry'], $begin, $end);
			
		} else {
			absences_removeFixedVacation($arr['entry']);
			
			// remove period in calendar backend
			if ($period = absences_getPeriod($arr['entry'], $id_user, $begin, $end)) {
			     $period->delete();
			}
		}
		
		
		
	}

	return true;
}


/**
 * Remove fixed vacation
 * return false if entry cannot be removed (entry not found), or true on success
 * 
 * @param int $id_entry
 * 
 * @return bool
 */
function absences_removeFixedVacation($id_entry)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	
	$res = $babDB->db_query("select id_user, date_begin, date_end FROM ".ABSENCES_ENTRIES_TBL." where id=".$babDB->quote($id_entry));
	$arr = $babDB->db_fetch_array($res);
	
	
	if (!$arr)
	{
		// not found
		return false;
	}

	$babDB->db_query("DELETE from ".ABSENCES_ENTRIES_TBL." where id='".$babDB->db_escape_string($id_entry)."'");
	$babDB->db_query("DELETE from ".ABSENCES_ENTRIES_ELEM_TBL." where id_entry='".$babDB->db_escape_string($id_entry)."'");
	$babDB->db_query("UPDATE absences_movement SET id_request='0' where request_class='absences_Entry' AND id_request='".$babDB->db_escape_string($id_entry)."'");
	
	absences_clearCalendars();
	
	// try to delete event copy in other backend (caldav)
	
	$begin = BAB_DateTime::fromIsoDateTime($arr['date_begin']);
	$end = BAB_DateTime::fromIsoDateTime($arr['date_end']);
	$period = absences_getPeriod($id_entry, $arr['id_user'],  $begin, $end);
	if ($period) {
		$period->delete();
	}
	
	return true;
}




/**
 * Update all fixed rights for one user
 * @param	int		$id_user
 * @param	array 	$messages
 */
function absences_updateFixedRightsOnUser($id_user, &$messages) {

	global $babDB, $babBody;
	
	/* @var $babBody babBody */

	// trouver les droits fixes de l'utilisateur
	
	$agent = absences_Agent::getFromIdUser($id_user);
	$I = $agent->getAgentRightUserIterator();
	$I->setKind(absences_Right::FIXED);

	foreach($I as $agentRight)
	{
		/*@var $agentRight absences_AgentRight */
		
		$right = $agentRight->getRight();
		
		try {
			if (false === absences_updateFixedVacation($id_user, $right)) {
				absences_addFixedVacation($id_user, $right);
			}
		}
		catch(absences_EntryException $e)
		{
			$messages[] = sprintf(
				absences_translate('Failed to update period for right "%s", %s (%s)'), 
				$right->description, 
				$e->getMessage(), 
				absences_DateTimePeriod($e->entry->date_begin, $e->entry->date_end)
			);
		}
	}

}


/**
 * Tester si la demande du droit a date fixe existe
 * @return bool
 */
function absences_isFixedCreated($id_user, $id_right)
{
    global $babDB;
    $res = $babDB->db_query("
            SELECT * 
        FROM 
            absences_entries e,
            absences_entries_elem ee
        WHERE 
            e.id=ee.id_entry 
            AND e.id_user=".$babDB->quote($id_user)." 
            AND ee.id_right=".$babDB->quote($id_right)
    );
    
    return (0 < $babDB->db_num_rows($res));
}
