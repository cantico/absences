<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/base.ui.php';
bab_Widgets()->includePhpClass('Widget_Form');


class absences_AgentCardFrame extends absences_CardFrame
{
	protected $agent;
	
	public function __construct(absences_Agent $agent, $layout = null)
	{
		parent::__construct(null, $layout);
		
		$this->agent = $agent;
		
		$this->loadPage();
	}
	
	protected function initClasses()
	{
		$this->addClass('absences-agent-cardframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);
	}
	
	protected function loadPage()
	{
		$this->initClasses();
		
		$W = bab_Widgets();
		$cols = $W->HBoxItems(
			$col1 = $W->VBoxLayout(), // photo
			$W->Items(
				$W->Title($this->agent->getName(),3),
				$W->HBoxItems(
					$col2 = $W->VBoxLayout()->setVerticalSpacing(.2,'em'), 	// nom, regime, fonction, service
					$col3 = $W->VBoxLayout()->setVerticalSpacing(.2,'em')	// positions dans l'organigramme / responsable hierarchique
				)->setHorizontalSpacing(3,'em')
			)
		)->setHorizontalSpacing(1,'em');
		
		$this->addItem($cols);
		
		if ($photo = $this->getPhoto())
		{
			$col1->addItem($photo);
		}
		
		
		if ($nickname = $this->getNickname())
		{
			$col2->addItem($nickname);
		}
		
		if ($disabled = $this->getDisabled())
		{
			$col2->addItem($disabled);
		}
		
		if ($email = $this->getEmail())
		{
			$col2->addItem($email);
		}
		
		if ($btel = $this->getDirField('btel'))
		{
			$col2->addItem($btel);
		}
		
		$this->addPersonnelInfos($col2);
		
		
		if ($direntry = $this->getDirEntry())
		{
			$col3->addItem($direntry);
		}
		
		if ($planning = $this->getPlanning())
		{
			$col3->addItem($planning);
		}
		
		/*
		if ($workinghours = $this->getWorkingHours())
		{
			$col3->addItem($workinghours);
		}
		*/
		
		$superior = absences_Agent::getCurrentUser()->isSuperiorOf($this->agent);
		
		
		if (absences_Agent::getCurrentUser()->isManager())
		{
            if ('rights' !== bab_rp('idx'))
            {
		        $col3->addItem($W->Link($W->Icon(absences_translate('Rights'), Func_Icons::APPS_VACATIONS), 
		            absences_addon()->getUrl().'vacadm&idx=rights&idu='.$this->agent->getIdUser())->setOpenMode(Widget_Link::OPEN_POPUP)
		        );
            }
		    $col3->addItem($W->Link($W->Icon(absences_translate('Export'), Func_Icons::MIMETYPES_OFFICE_SPREADSHEET), absences_addon()->getUrl().'vacadm&idx=export&id_user='.$this->agent->getIdUser()));
		
		} 
		else if ($superior && absences_getVacationOption('chart_superiors_set_rights'))
		{
		    $entity = $this->agent->getMainEntity();
		    $col3->addItem($W->Link($W->Icon(absences_translate('Rights'), Func_Icons::APPS_VACATIONS), 
		        absences_addon()->getUrl().'vacchart&idx=rights&idu='.$this->agent->getIdUser().'&ide='.$entity['id'].'&popup=1')->setOpenMode(Widget_Link::OPEN_POPUP)
		    );
		} 
		else if ($superior)
		{
		    $col3->addItem($W->Link($W->Icon(absences_translate('Balance'), Func_Icons::APPS_VACATIONS),
		            absences_addon()->getUrl().'vacuser&idx=viewrights&id_user='.$this->agent->getIdUser().'&popup=1')->setOpenMode(Widget_Link::OPEN_POPUP)
		    );
		}
		
		
		
		if ($superior = $this->agent->getSuperior())
		{
			//TRANSLATORS: Supervisor in organizational chart
			$col2->addItem($this->titledLabel(absences_translate('Supervisor'), $superior['name'][0]));
		}
		
		
		if ($organization = $this->agent->getOrganization())
		{
		    $col2->addItem($this->titledLabel(absences_translate('Organization'), $organization->name));
		}
		
		
		if ($entity = $this->agent->getMainEntity())
		{
			if (absences_Agent::getCurrentUser()->canViewEntityPlanning($entity['id']))
			{
			
				$col2->addItem(
					$W->FlowItems(
						$W->Label(absences_translate('Position'))->addClass('widget-strong')->colon(),
						$W->Items(
								$W->Label($entity['name']), 
								$W->Label(' ('),
								$W->Link(absences_translate('planning'), absences_addon()->getUrl().'planning&idx=entity_cal&popup=1&ide='.$entity['id'])->setOpenMode(Widget_Link::OPEN_POPUP),
								$W->Label(')')
						)
					)->addClass('widget-small')->setSpacing(.2,'em', .3,'em')
				);
			
			
			} else {
				
				$col2->addItem($this->titledLabel(absences_translate('Position'), $entity['name']));
			}
			
			
			//TRANSLATORS: Main role in organizational chart entity
			$col2->addItem($this->titledLabel(absences_translate('Role'), $entity['role_name']));
		}
		
		
		if (!$superior && !$entity)
		{
			// no organizational chart, replace with directory fields
			
			if ($departmentnumber = $this->getDirField('departmentnumber'))
			{
				$col2->addItem($departmentnumber);
			}
		}

	}
	
	
	/**
	 * 
	 */
	protected function addPersonnelInfos($col2)
	{
		if ($approbation = $this->agent->getApprobation())
		{
			$col2->addItem($this->titledLabel(absences_translate('Approbation'), $approbation['name'].sprintf(' (%s)', $approbation['type'])));
		}
		
		$collection = $this->agent->getCollection();
		if (isset($collection) && $collection->getRow())
		{
			$col2->addItem($this->titledLabel(absences_translate('Collection'), $collection->name));
		}
	}
	
	
	
	protected function getPhoto()
	{
		if ($photo = $this->agent->getPhoto())
		{
			$W = bab_Widgets();
			return $W->Image($photo, $this->agent->getName());
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param string $fieldname
	 */
	protected function getDirField($fieldname)
	{
		$direntry = $this->agent->getDirEntry();
		
		if (!isset($direntry[$fieldname]))
		{
			return null;
		}
		
		if (empty($direntry[$fieldname]['value']))
		{
			return null;
		}
		
		return $this->titledLabel(
			$direntry[$fieldname]['name'], 
			$direntry[$fieldname]['value']
		);
	}
	

	protected function getNickname()
	{
		return $this->titledLabel(
				absences_translate('Login ID'),
				bab_getUserNickname($this->agent->id_user)
		);
	}
	
	
	
	protected function getDisabled()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
		
		if (bab_userInfos::isValid($this->agent->getIdUser()))
		{
			return null;
		}
		
		bab_functionality::includeOriginal('Icons');
		$W = bab_Widgets();
		return $W->Icon(absences_translate('This account is disabled'), Func_Icons::STATUS_DIALOG_WARNING);
	}
	
	
	/**
	 *
	 *
	 */
	protected function getEmail()
	{
		$W = bab_Widgets();
		$direntry = $this->agent->getDirEntry();
	
		if (!isset($direntry['email']))
		{
			return null;
		}
	
		if (empty($direntry['email']['value']))
		{
			return null;
		}
		
		return $W->FlowItems(
				$W->Label($direntry['email']['name'])->addClass('widget-strong')->colon(),
				$W->Link($direntry['email']['value'], 'mailto:'.$direntry['email']['value'])
		)->addClass('widget-small')->setSpacing(.2,'em', .3,'em');
	}
	
	

	
	
	
	protected function getDirEntry()
	{
		$W = bab_Widgets();
		$link = bab_getUserDirEntryLink($this->agent->id_user, BAB_DIR_ENTRY_ID_USER);
	
		if (!$link)
		{
			return null;
		}
	
		return $W->Link($W->Icon(absences_translate('View directory entry'), Func_Icons::OBJECTS_CONTACT), $link)->setOpenMode(Widget_Link::OPEN_POPUP);
	}
	
	
	
	
	protected function getPlanning()
	{
		$W = bab_Widgets();
		
		$addon = bab_getAddonInfosInstance('absences');
		
		return $W->Link($W->Icon(absences_translate('View planning'), Func_Icons::APPS_CALENDAR), $addon->getUrl().'planning&idx=cal&popup=1&idu='.$this->agent->id_user)->setOpenMode(Widget_Link::OPEN_POPUP);
	}
	
	
	
	protected function getWorkingHours()
	{
		$W = bab_Widgets();
		
		$func = bab_functionality::get('WorkingHours');
		
		if (false === $func)
		{
			return null;
		}
		
		$url = $func->getUserSettingsPopupUrl($this->agent->getIdUser());
		
		if (null === $url)
		{
			return null;
		}
		
		return $W->Link($W->Icon(absences_translate('Working hours'), Func_Icons::APPS_CALENDAR), $url)->setOpenMode(Widget_Link::OPEN_POPUP);
	}
}



class absences_AgentFullFrame extends absences_AgentCardFrame
{
	/**
	 *
	 */
	protected function addPersonnelInfos($col2)
	{
		// do not display personnel info because exists in edit form
		// show link to directory entry instead 
	}
	
	
	protected function initClasses()
	{
		$this->addClass('absences-agent-fullframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);
	}

}







class absences_AgentMovementList extends absences_Paginate 
{
	const MAX = 20;
	
	public $altbg = true;
	
	public $t_date;
	public $date;
	
	public $t_author;
	public $author;
	
	public $t_message;
	public $message;
	
	public $t_comment;
	public $comment;
	
	private $res;
	
	public function __construct(absences_Agent $agent)
	{
		$this->t_date = absences_translate('Date');
		$this->t_author = absences_translate('Author');
		$this->t_request = absences_translate('Object');
		$this->t_message = absences_translate('Message');
		$this->t_comment = absences_translate('Regularization or approval comment by the author');
		
		
		$this->res = $agent->getMovementIterator();
		$this->res->rewind();
		
		$this->paginate($this->res->count(), self::MAX);
		$this->res->seek($this->pos);
		
		bab_functionality::includeOriginal('Icons');
	}
	
	public function getnext()
	{
		if (($this->res->key() - $this->pos) >= self::MAX)
		{
			return false;
		}
		
		if ($this->res->valid())
		{
			$W = bab_Widgets();
			$movement = $this->res->current();
			/*@var $movement absences_Movement */
			
			$this->altbg = !$this->altbg;
		
			$this->author = bab_toHtml(bab_getUserName($movement->id_author));
			$this->date = bab_toHtml(bab_shortDate(bab_mktime($movement->createdOn)));
			if ($request = $movement->getRequest())
			{
				$this->request = $request->getManagerFrame()->addClass(Func_Icons::ICON_LEFT_16)->display($W->HtmlCanvas());
			} else {
				$this->request = '';
			}
			$this->message = bab_toHtml($movement->message);
			$this->comment = bab_toHtml($movement->comment);
			
			$this->res->next();
			return true;
		}
		
		return false;
	}
	
	public function getHtml()
	{
		$babBody = bab_getInstance('babBody');
		
		if ($this->res->count() === 0)
		{
			$babBody->addError(absences_translate('There are no records yet for this user'));
			return '';
		}
		
		return absences_addon()->printTemplate($this, "agent.html", "movement");
	}
}












class absences_AgentRightsList
{
	public $nametxt;
	public $urlname;
	public $url;
	public $descriptiontxt;
	public $description;
	public $consumedtxt;
	public $consumed;
	public $fullname;
	public $titletxt;

	public $arr = array();
	public $babDB;
	private $res;

	public $iduser;
	public $idcoll;
	public $bview;

	public $updatetxt;
	public $invalidentry;
	public $invalidentry1;
	public $invalidentry2;

	public function __construct($id)
	{
		global $babDB;
		$this->iduser = $id;
		$this->idu = $id;
		$this->ide = bab_rp('ide');
		$this->updatetxt = absences_translate("Update");
		$this->desctxt = absences_translate("Description");
		$this->typetxt = absences_translate("Type");
		$this->consumedtxt = absences_translate("Consumed");
		$this->waitingtxt = absences_translate("Waiting");
		$this->datebtxt = absences_translate("Begin date");
		$this->dateetxt = absences_translate("End date");
		$this->quantitytxt = absences_translate("Rights");
		$this->balancetxt = absences_translate("Balance");
		$this->previsionaltxt = absences_translate("Previsional");
		$this->previsionalbalancetxt = absences_translate("Previ. Bal.");
		$this->datetxt = absences_translate("Entry date");
		$this->invalidentry = bab_toHtml(absences_translate("Invalid entry!  Only numbers are accepted or . !"), BAB_HTML_JS);
		$this->invalidentry1 = absences_translate("Invalid entry");
		$this->invalidentry2 = absences_translate("Days must be multiple of 0.5");
		$this->t_comment = absences_translate("Comment on quantities modification for user history:");
		$GLOBALS['babBody']->title = absences_translate("Vacation rights of:").' '.bab_getUserName($id);

		$infos = bab_getUserInfos($id);
		$this->currentUserLastname = $infos['sn'];
		$this->currentUserFirstname = $infos['givenname'];

		$this->tg = bab_rp('tg');

		require_once dirname(__FILE__).'/agent.class.php';
		require_once dirname(__FILE__).'/agent.ui.php';
		$agent = absences_Agent::getFromIdUser($id);
		$cardframe = new absences_AgentCardFrame($agent);
		$this->header = $cardframe->display(bab_Widgets()->HtmlCanvas());

		$this->res = $agent->getAgentRightManagerIterator();
		$this->res->rewind();
		
		$collectionName = absences_translate('Unknown');
		if ($collection = $agent->getCollection()) {
		    if ($collection->getRow()) {
		         $collectionName = $collection->name;
		    }
		}

		$this->not_in_collection = sprintf(absences_translate('This right is not in collection %s'), $collectionName);

		$this->agentRight = (absences_Agent::getCurrentUser()->isManager() && !bab_rp('ide'));
	}

	public function getnextright(&$ignore)
	{
		static $y = '';
		static $label = '';

		if( $this->res->valid())
		{

			$agentRight = $this->res->current();
			/*@var $agentRight absences_AgentRight */
			$right = $agentRight->getRight();

			$right->monthlyQuantityUpdate();
			
			$type = $right->getType();

			$this->res->next();


			$this->idright = $right->id;
			$this->description = bab_toHtml($right->description);
			$this->type = bab_toHtml($type->name);
			$this->color = bab_toHtml($type->color);
			$this->quantity = absences_editQuantity($agentRight->getQuantity(), $right->quantity_unit);
			
			$this->unit = bab_toHtml($right->getUnitLabel());
			
			$this->dynamic_quantity = null;
			$dyn = $agentRight->getDynamicQuantity();
			if (abs($dyn) > 0.01) {
			    $this->dynamic_quantity = bab_toHtml(
			        sprintf(
			            absences_translate(
			                'The quantity include %s because of the balance modification according to the consumed amount rule',
			                'The quantity include %s because of the balance modification according to the consumed amount rule',
			                $dyn
			            ),
			            absences_quantity($dyn, $right->quantity_unit)
	               )
	            );
			}
			
			
			$this->increment_quantity = null;
			$dyn = $agentRight->getIncrementQuantity();
			if (abs($dyn) > 0.01) {
			    $this->increment_quantity = bab_toHtml(
			            sprintf(
			                    absences_translate(
			                        'The quantity include %s because of the monthly modifications',
			                        'The quantity include %s because of the monthly modifications',
			                        $dyn
			                    ),
			                    absences_quantity($dyn, $right->quantity_unit)
			            )
			    );
			}
			
			$this->disp_quantity = absences_quantity($agentRight->getQuantity(), $right->quantity_unit);

			if ($message = $agentRight->getQuantityAlert())
			{
				$this->alert = bab_toHtml($message);
			} else {
				$this->alert = null;
			}


			if ($right->getYear() !== $y)
			{
				$this->year = null === $right->getYear() ? absences_translate('No theoretical period') : $right->getYear();
			} else {
				$this->year = '';
			}
			$y = $right->getYear();

			$rgrouplabel = $right->getRgroupLabel();
			$this->rgroup = $rgrouplabel !== $label && !empty($rgrouplabel) ? $rgrouplabel : '';
			$label = $rgrouplabel;

			$this->in_rgroup = null !== $right->getRgroup();

			$this->dateb = bab_shortDate(bab_mktime($right->date_begin), false);
			$this->datee = bab_shortDate(bab_mktime($right->date_end), false);

			$this->consumed = absences_quantity($agentRight->getConfirmedQuantity(), $right->quantity_unit);
			$this->waiting = absences_quantity($agentRight->getWaitingQuantity(), $right->quantity_unit);
			$this->previsional = absences_quantity($agentRight->getPrevisionalQuantity(), $right->quantity_unit);

			$balance = $agentRight->getQuantity()-$agentRight->getWaitingQuantity()-$agentRight->getConfirmedQuantity();
			$this->balance = absences_quantity($balance, $right->quantity_unit);
			$this->previsional_balance = absences_quantity($balance - $agentRight->getPrevisionalQuantity(), $right->quantity_unit);

			$this->readonly = ($right->getKind() === absences_Right::FIXED);
			$this->agent_right_url = bab_toHtml(absences_addon()->getUrl().'vacadm&idx=agentright&ar='.$agentRight->id);
			$this->in_collection = $agentRight->isRightInAgentCollection();

			return true;
		}
		else
			return false;

	}





	/**
	 * get previous or next user in manager list
	 * @param	string	$sign
	 * @param	string	$sqlOrderType
	 * @return 	int|false
	 */
	private function getUserInManagerList($sign, $sqlOrderType) {


		global $babDB;


		$ide = bab_rp('ide');

		// delegation administration list
		if ($ide && absences_isAccessibleEntityAsSuperior($ide)) {


			$users = bab_OCGetCollaborators($ide);
			$superior = bab_OCGetSuperior($ide);

			if (((int)$superior['id_user']) !== (int)$GLOBALS['BAB_SESS_USERID'] && false === absences_isAccessibleEntityAsCoManager($ide)) {
				$users[] = $superior;
			}


			$ordering = array();
			foreach($users as $key => $user) {
				$ordering[$key] = $user['lastname'].' '.$user['firstname'];
			}

			bab_sort::natcasesort($ordering);


			$previous = false;
			$next = false;

			$next_prev_index = array();

			foreach($ordering as $key => $dummy) {

				if (false !== $previous) {
					$next_prev_index[$previous]['next'] = $users[$key]['id_user'];
				}

				$next_prev_index[$users[$key]['id_user']] = array(
						'previous' => $previous,
						'next' => $next
				);

				$previous = $users[$key]['id_user'];
			}

			reset($ordering);
			$firstuser = $users[key($ordering)]['id_user'];
			next($ordering);
			$seconduser = $users[key($ordering)]['id_user'];

			$next_prev_index[$firstuser]['next'] = $seconduser;



			switch($sign) {
				case '<':
					return $next_prev_index[$this->iduser]['previous'];

				case '>':
					return $next_prev_index[$this->iduser]['next'];
			}


			return false;
		}






		// manager list

		$acclevel = absences_vacationsAccess();
		if( true === $acclevel['manager'])
		{

			$res = $babDB->db_query('
					SELECT
					p.id_user
					FROM
					'.ABSENCES_PERSONNEL_TBL.' p, '.BAB_USERS_TBL.' u

					WHERE
					u.id = p.id_user
					AND (u.lastname '.$sign.' '.$babDB->quote($this->currentUserLastname).'
					OR (u.lastname = '.$babDB->quote($this->currentUserLastname).'
					AND u.firstname '.$sign.' '.$babDB->quote($this->currentUserFirstname).')
			)

					ORDER BY u.lastname '.$sqlOrderType.', u.firstname '.$sqlOrderType.'

					LIMIT 0,2
					');



			if ($arr = $babDB->db_fetch_assoc($res)) {
				return (int) $arr['id_user'];
			}

		}





		return false;
	}




	public function previoususer() {

		static $i = 0;

		if (0 === $i) {

			$id_user = $this->getUserInManagerList('<','DESC');
			if (!$id_user) {
				return false;
			}

			$this->previous = bab_toHtml(bab_getUserName($id_user));

			require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
			$url = bab_url::request_gp();

			if (bab_rp('idu')) {
				$url = bab_url::mod($url, 'idu', $id_user);
			}

			if (bab_rp('id_user')) {
				$url = bab_url::mod($url, 'id_user', $id_user);
			}

			$this->previousurl = bab_toHtml($url);


			$i++;
			return true;
		}

		return false;
	}

	public function nextuser() {

		static $i = 0;

		if (0 === $i) {

			$id_user = $this->getUserInManagerList('>','ASC');
			if (!$id_user) {
				return false;
			}

			$this->next = bab_toHtml(bab_getUserName($id_user));

			require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
			$url = bab_url::request_gp();
			if (bab_rp('idu')) {
				$url = bab_url::mod($url, 'idu', $id_user);
			}

			if (bab_rp('id_user')) {
				$url = bab_url::mod($url, 'id_user', $id_user);
			}
			$this->nexturl = bab_toHtml($url);


			$i++;
			return true;
		}

		return false;
	}

}










class absences_PersonalOptionsEditor extends Widget_Form
{

	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));


		$this->setName('options');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(70,'em'));

		$this->addFields();
		$this->loadFormValues();

		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}



	protected function addFields()
	{
		$W = bab_Widgets();


		$this->addItem($W->LabelledWidget(
				absences_translate('Email to notify when a vacation request is accepted (other than me)'),
				$W->TextEdit()->setColumns(80)->setLines(2),
				'emails',
				absences_translate('Comma separated values')
		));

		
	}





	protected function loadFormValues()
	{
		global $babDB;

		
		$values = array();
		$agent = absences_Agent::getCurrentUser();
		
		$values['emails'] = $agent->emails;

		$this->setValues($values, array('options'));
	}


	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
				$W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}




















class absences_AgentEdit
{
    public $usertext;
    public $grouptext;
    public $userval;
    public $userid;
    public $groupval;
    public $groupid;
    public $collection;
    public $idcollection;
    public $collname;
    public $appschema;
    public $idsapp;
    public $saname;
    public $selected;
    public $add;
    public $bdel;
    public $delete;
    public $groupsbrowurl;
    public $usersbrowurl;

    public $orand;
    public $reset;
    
    
    /**
     * @var bool
     */
    public $changeorganization = true;

    /**
     * @var int
     */
    private $id_organization;
    
    /**
     * @var absences_OrganizationIterator
     */
    private $organizations;
    
    
    /**
     * @var array
     */
    private $wsprofiles;
    
    /**
     * @var ORM_Iterator
     */
    private $profiles;
    
    
    /**
     * @var int
     */
    private $loop_selectedProfile = 0;
    
    
    public $canuseprofiles = false;


    public function __construct($idp)
    {
        require_once dirname(__FILE__).'/organization.class.php';
        
        $this->usertext = absences_translate("User");
        $this->collection = absences_translate("Collection:");
        $this->appschema = absences_translate("Approbation schema for vacation requests:");
        $this->t_id_sa_cet = absences_translate("Approbation schema for deposit into the time savings account:");
        $this->t_id_sa_recover = absences_translate("Approbation schema for declaration of worked days entitling recovery:");
        $this->t_use_vr = absences_translate("Use schema for vacation requests");
        $this->t_list_request = absences_translate("List absence requests");
        $this->t_create_request = absences_translate("Create a vacation request");
        $this->t_emails = absences_translate('Email to notify when a vacation request is accepted, comma separated');
        $this->t_organization = absences_translate('Organization:');
        $this->t_workschedule = absences_translate('Work schedule profile');
        $this->t_from = absences_translate('From');
        $this->t_to = absences_translate('To');

        $this->listrequesturl = bab_toHtml(absences_addon()->getUrl()."vacadmb&idx=lreq&userid=".urlencode($idp));
        $this->createrequesturl = bab_toHtml(absences_addon()->getUrl()."vacuser&idx=period&rfrom=1&id_user=".urlencode($idp));

        $this->delete = absences_translate("Delete");
        $this->usersbrowurl = absences_addon()->getUrl()."vacadm&idx=browu&cb=";
        $this->tg = bab_rp('tg');
        $this->ide = isset($_REQUEST['ide']) ? $_REQUEST['ide'] : false;

        global $babDB;
        $W = bab_Widgets();
        $this->idp = $idp;
         
        $userpicker = $W->UserPicker()->setName('userid');
        bab_functionality::includeOriginal('Icons');
        $this->userpicker = $W->Frame()->addClass(Func_Icons::ICON_LEFT_16)->addItem($userpicker)->display($W->HtmlCanvas());
         

        list($n) = $babDB->db_fetch_array($babDB->db_query("SELECT COUNT(*) FROM ".ABSENCES_ENTRIES_TBL." WHERE id_user='".$babDB->db_escape_string($this->idp)."' AND status=''"));

        if ($n > 0) {
            $waiting = absences_translate(
                'Modification are disabled, the user have %d waiting request' ,
                'Modification are disabled, the user have %d waiting requests',
                $n);
            $this->waiting = sprintf($waiting, $n);
        }

        if (isset($_POST['action']) && $_POST['action'] == 'changeuser')
        {
            $this->userid = $_POST['userid'];
            $this->idsa = $_POST['idsa'];
            $this->id_sa_cet = $_POST['id_sa_cet'];
            $this->id_sa_recover = $_POST['id_sa_recover'];
            $this->idcol = $_POST['idcol'];
            $this->idp = $_POST['idp'];
            $this->id_organization = bab_pp('id_organization');
        }

        if( !empty($this->idp))
        {
            require_once dirname(__FILE__).'/agent.class.php';
            require_once dirname(__FILE__).'/agent.ui.php';
            $this->add = absences_translate("Modify");
            $agent = absences_Agent::getFromIdUser($this->idp);

            if (!$agent->exists())
            {
                // T7680 apres suppression d'une fiche d'annuaire
                $url = bab_url::get_request('tg');
                $url->idx = 'lper';
                $url->location();
            }

            $this->userid = $agent->getIdUser();


            $frame = new absences_AgentFullFrame($agent);

            $this->userval = $frame->display($W->HtmlCanvas());
            $this->idcol = $agent->id_coll;
            $this->idsa = $agent->id_sa;
            $this->id_sa_cet = $agent->id_sa_cet;
            $this->id_sa_recover = $agent->id_sa_recover;
            $this->emails = bab_toHtml($agent->emails);
            $this->id_organization = $agent->id_organization;
        }
        else
        {
            $this->add = absences_translate("Add");
            $this->idcol = '';
            $this->idsa = '';
            $this->id_sa_cet = '';
            $this->id_sa_recover = '';
            $this->userval = '';
            $this->userid = '';
            $this->emails = '';
            $this->id_organization = 0;
        }

        $this->groupval = "";
        $this->groupid = "";

        $this->sares = bab_WFGetApprobationsList();


        $this->colres = $babDB->db_query("select * from ".ABSENCES_COLLECTIONS_TBL." order by name asc");
        $this->countcol = $babDB->db_num_rows($this->colres);
         
        $this->organizations = new absences_OrganizationIterator();
        $this->organizations->rewind();
        
        $this->changeorganization = !(bool) absences_getVacationOption('organization_sync');
        
        $defaultWh = bab_functionality::get('WorkingHours');
        
        if ($workschedules = bab_functionality::get('WorkingHours/Workschedules')) {
            /*@var $workschedules Func_WorkingHours_Workschedules */
            $this->wsprofiles = $workschedules->getProfiles($this->idp);
            
            if (count($this->wsprofiles) > 0 && $workschedules instanceof $defaultWh) {
                $this->canuseprofiles = true;
            }
            
            
            $this->profiles = $workschedules->getUserProfiles($this->idp);
            $this->profiles->rewind();
        }
    }


    public function getnextorganization()
    {
        if ($this->organizations->valid()) {
            $organization = $this->organizations->current();
            $this->organizations->next();
            
            $this->value = bab_toHtml($organization->id);
            $this->name = bab_toHtml($organization->name);
            $this->selected = ($this->id_organization == $organization->id);
            
            return true;
        }
    }

    public function getnextsa()
    {

        if( list(,$arr) = each($this->sares) )
        {
            $this->saname = $arr['name'];
            $this->idsapp = $arr['id'];
            $this->selected = ( $this->idsa == $this->idsapp ) ? "selected" : '';
            $this->selected_cet = ( $this->id_sa_cet == $this->idsapp ) ? "selected" : '';
            $this->selected_recover = ( $this->id_sa_recover == $this->idsapp ) ? "selected" : '';

            return true;
        }
        reset($this->sares);
        return false;

    }

    public function getnextcol()
    {
        static $j= 0;
        if( $j < $this->countcol )
        {
            global $babDB;
            $arr = $babDB->db_fetch_array($this->colres);
            $this->collname = $arr['name'];
            $this->idcollection = $arr['id'];
            if( $this->idcol == $this->idcollection )
                $this->selected = "selected";
            else
                $this->selected = "";
            $j++;
            return true;
        }
        else
            return false;
    }

    
    public function getnextwsprofile()
    {
        if (list($id, $name) = each($this->wsprofiles)) {
            $this->id = bab_toHtml($id);
            $this->name = bab_toHtml($name);
            $this->selected = ($this->loop_selectedProfile === $id);
            
            return true;
        }
        
        reset($this->wsprofiles);
        return false;
    }
    
    
    public function getnextprofile()
    {
        static $firstRow = null;
        
        if (!isset($firstRow) && 0 === $this->profiles->count()) {
            $firstRow = true;
            $this->profileRow();
            return true;
        }
        
        
        if ($this->profiles->valid()) {
            $userProfile = $this->profiles->current();
            $this->profileRow($userProfile);
            
            
            $this->profiles->next();
            return true;
        }
        
        return false;
    }
    
    
    private function profileRow(workschedules_UserProfile $userProfile = null)
    {
        $W = bab_Widgets();
        $canvas = $W->HtmlCanvas();
        
        $this->userProfileId = '0';
        
        $from = $W->DatePicker()->setName(array('profiles', 'from', ''));
        $to = $W->DatePicker()->setName(array('profiles', 'to', ''));
        
        if (isset($userProfile)) {
            $this->userProfileId = $userProfile->id;
            $this->loop_selectedProfile = (int) $userProfile->profile->id;
            
            $from->setValue($userProfile->from);
            $to->setValue($userProfile->to);
        }
        
        $this->datepicker_from = $from->display($canvas);
        $this->datepicker_to = $to->display($canvas);
        
        $this->userProfileId = isset($userProfile) ? $userProfile->id : '0';
    }
    

    public function printhtml()
    {
        $babBody = bab_getBody();
        
        $babBody->addJavascriptFile(absences_addon()->getTemplatePath().'workschedules.js', true);
        
        $babBody->addStyleSheet(absences_addon()->getStylePath().'vacation.css');
        $babBody->babecho(absences_addon()->printTemplate($this, "vacadm.html", "personnelcreate"));
    }
}



