<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

bab_Widgets()->includePhpClass('Widget_Form');
bab_functionality::includefile('Icons');


class absences_AgentList extends absences_Paginate
{
    public $fullname;
    public $urlname;
    public $url;
    		
    public $fullnameval;
    
    public $arr = array();
    public $count;
    public $res;
    
    public $pos;
    public $selected;
    public $allselected;
    public $allurl;
    public $allname;
    public $checkall;
    public $uncheckall;
    
    public $filteron;
    public $idcollection;
    public $idsapp;
    public $collname;
    public $saname;
    
    public $deletealt;
    public $altlrbu;
    public $lrbuurl;
    public $calurl;
    public $altcal;
    
    public $altbg = true;
    
    /**
     * @var array
     */
    private $filter;
    
    public function __construct(Array $filter)
    {
    	$this->allname = absences_translate("All");
    	$this->uncheckall = absences_translate("Uncheck all");
    	$this->checkall = absences_translate("Check all");
    	$this->filteron = absences_translate("Filter on");
    	$this->collection = absences_translate("Collection");
    	$this->appschema = absences_translate("Approbation schema");
    	
    	$this->deletealt = absences_translate("Delete the checked users");
    	$this->altlrbu = absences_translate("Rights");
    	$this->altcal = absences_translate("Calendar");
    	$this->altvunew = absences_translate("Request");
    	$this->t_view_calendar = absences_translate("View calendars");
    	$this->t_movement = absences_translate("History");

    
    	$this->t_lastname = absences_translate("Lastname");
    	$this->t_firstname = absences_translate("Firstname");
    	$this->t_user_disabled = absences_translate("User disabled");
    
    	global $babDB;
    	
    	$this->filter = $filter;

        $req = $this->getQuery($filter);
    	$this->res = $babDB->db_query($req);
    	$this->count = $babDB->db_num_rows($this->res);
    	$this->paginate($this->count, $this->getNbItemPerPage());
    	$babDB->db_data_seek($this->res, $this->pos);
    	
    
    	$allurl = bab_url::get_request('tg', 'idx', 'idvr');
    	$allfilter = $filter;
    	$allfilter['lastname'] = '';
    	$allurl->filter = $allfilter;
    	
    	$this->allurl = bab_toHtml($allurl->toString());

    	
    	$form = new absences_AgentListFilterEditor();
    	$this->filtereditor = $form->display(bab_Widgets()->HtmlCanvas());
    }
    
    
    private function getNbItemPerPage()
    {
        if (isset($this->filter['max'])) {
            return (int) $this->filter['max'];
        }
        
        return ABSENCES_MAX_AGENTS_LIST;
    }
    
    
    
    private function getQuery(Array $filter)
    {
        global $babDB;
        
        $req = "SELECT
    			u.id,
    			u.lastname,
    			u.firstname,
    	        u.disabled,
    	        u.validity_start,
    	        u.validity_end,
                p.id agent_id,
                p.id_user,
    			p.id_sa,
    			p.id_coll,
    			c.name collname,
    			a.name saname
    		FROM
    			".BAB_USERS_TBL." u
    				join ".ABSENCES_PERSONNEL_TBL." p
    				LEFT JOIN ".ABSENCES_COLLECTIONS_TBL." c ON c.id=p.id_coll
    				LEFT JOIN ".BAB_FLOW_APPROVERS_TBL." a ON a.id=p.id_sa
    		WHERE
    			u.id=p.id_user 
    		";
        
        if (!empty($filter['lastname'])) {
            $req .= " and u.lastname LIKE '".$babDB->db_escape_like($filter['lastname'])."%'";
        }
        
        if (!empty($filter['firstname'])) {
            $req .= " and u.firstname LIKE '".$babDB->db_escape_like($filter['firstname'])."%'";
        }
        
        if( !empty($filter['organization'])) {
            $req .= " and p.id_organization=".$babDB->quote($filter['organization']);
        }
        
        if( !empty($filter['idcol'])) {
            $req .= " and p.id_coll=".$babDB->quote($filter['idcol']);
        }
        
        if(!empty($filter['idsa'])) {
            $req .= " and p.id_sa=".$babDB->quote($filter['idsa']);
        }
        
        $req .= " ORDER BY u.lastname, u.firstname asc";

        return $req;
    }
    
    
    public function getnext()
    {
    	static $i = 0;
    	
    	if ($i >= $this->getNbItemPerPage())
    	{
    	    return false;
    	}
    	
    	global $babDB;
    	
    	if($this->arr = $babDB->db_fetch_assoc($this->res))
    	{
    		$this->altbg = !$this->altbg;
    		$this->url = absences_addon()->getUrl()."vacadm&idx=modp&idp=".$this->arr['id'];
    		
    		$this->firstname = bab_toHtml($this->arr['firstname']);
    		$this->lastname = bab_toHtml($this->arr['lastname']);
    		$this->disabled = (bool) $this->arr['disabled'];
    		if ('0000-00-00' !== $this->arr['validity_start'] && $this->arr['validity_start'] > date('Y-m-d')) {
    		    $this->disabled = true;
    		}
    		
    		if ('0000-00-00' !== $this->arr['validity_end'] && $this->arr['validity_end'] < date('Y-m-d')) {
    		    $this->disabled = true;
    		}
    		
    		$this->userid = $this->arr['id'];
    		$this->lrbuurl = bab_toHtml(absences_addon()->getUrl()."vacadm&idx=rights&idu=".$this->userid);
    		$this->movementurl = bab_toHtml(absences_addon()->getUrl()."vacadm&idx=movement&idu=".$this->userid);
    		$this->calurl = bab_toHtml(absences_addon()->getUrl()."planning&idx=cal&idu=".$this->userid);

    		$W = bab_Widgets();
    		$menu = $W->Menu(null , $W->VBoxItems())
    		  ->addClass(Func_Icons::ICON_LEFT_16)
    		  ->addItem($W->Link(absences_translate('Absence'), absences_addon()->getUrl()."vacuser&idx=period&rfrom=1&id_user=".$this->userid)
    		      ->addClass('icon')
    		      ->addClass(Func_Icons::ACTIONS_LIST_ADD))
    		  ->addItem($W->Link(absences_translate('CET deposit'), absences_addon()->getUrl()."vacuser&idx=cet&id_user=".$this->userid)
    		      ->addClass('icon')
    		      ->addClass(Func_Icons::ACTIONS_LIST_ADD))
    		  ->addItem($W->Link(absences_translate('Workperiod recover'), absences_addon()->getUrl()."vacuser&idx=workperiod&id_user=".$this->userid)
    		      ->addClass('icon')
    		      ->addClass(Func_Icons::ACTIONS_LIST_ADD));
    		
    		$this->createmenu = $menu->display($W->HtmlCanvas());
    		
    		$this->collname = bab_toHtml($this->arr['collname']);
    		$this->saname = bab_toHtml($this->arr['saname']);
    		
    		if ($url = bab_getUserDirEntryLink($this->arr['id'], BAB_DIR_ENTRY_ID_USER))
    		{
    			$this->direntryurl = bab_toHtml($url);
    		} else {
    			$this->direntryurl = false;
    		}
    		
    		$i++;
    		return true;
    	}
    	else
    		return false;
    
    }
    
    public function getnextselect()
    {
    	static $k = 0;
    	static $t = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	if( $k < 26)
    	{
    		global $babDB;
    		
    		$this->selectname = mb_substr($t, $k, 1);
    		$letterfilter = $this->filter;
    		$letterfilter['lastname'] = $this->selectname;
    		
    		$url = bab_url::get_request('tg', 'idx', 'idvr');
    		$url->filter = $letterfilter;
    		
    		$this->selecturl = bab_toHtml($url->toString());
    
    		if( isset($this->filter['lastname']) && $this->filter['lastname'] === $this->selectname) {
    			$this->selected = 1;
    			
    		}
    		else {

    			$res = $babDB->db_query($this->getQuery($letterfilter));
    			if( $babDB->db_num_rows($res) > 0 )
    				$this->selected = 0;
    			else
    				$this->selected = 1;
    		}
    		$k++;
    		return true;
    	}
    	else
    		return false;
    
    }
    
}




/**
 * Display a menu to add items to the agent list
 * @return string
 */
function absences_agentListMenu()
{
    $toolbar = absences_getToolbar();

    $sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';

    $url = bab_url::get_request('tg', 'filter');
    $addurl = clone $url; $addurl->idx = 'addp';
    $grpurl = clone $url; $grpurl->idx = 'addg';

    $toolbar->addToolbarItem(
        new BAB_ToolbarItem(absences_translate('Add'), $addurl->toString(),
            $sImgPath . 'edit_add.png', '', '', '')
    );

    
    $toolbar->addToolbarItem(
        new BAB_ToolbarItem(absences_translate("Add/Modify by group"), $grpurl->toString(),
            $sImgPath . 'edit_add.png', '', '', '')
    );
    

    return $toolbar->printTemplate();
}














class absences_AgentListFilterEditor extends Widget_Form
{

    public function __construct()
    {
        $W = bab_Widgets();

        parent::__construct(null, $W->FlowLayout()->setSpacing(1,'em', 3,'em'));

        $this->addClass('BabLoginMenuBackground');
        $this->addClass('absences-filterform');
        $this->setName('filter');
        $this->colon();
        $this->setReadOnly();

        $this->addFields();
        $this->loadFormValues();

        $this->addButtons();
        $this->setSelfPageHiddenFields();
    }



    protected function addFields()
    {
        $this->addItem($this->lastname());
        $this->addItem($this->firstname());
        
        if ($org = $this->organization()) {
            $this->addItem($org);
        }
        
        $this->addItem($this->idcol());
        $this->addItem($this->idsa());
        $this->addItem($this->max());
    }


    
    protected function lastname()
    {
        $W = bab_Widgets();
        
        return $W->LabelledWidget(
            absences_translate('Lastname'),
            $W->LineEdit()->setSize(15),
            __FUNCTION__
        );
    }
    
    
    protected function firstname()
    {
        $W = bab_Widgets();
    
        return $W->LabelledWidget(
            absences_translate('Firstname'),
            $W->LineEdit()->setSize(15),
            __FUNCTION__
        );
    }
    
    
    protected function organization()
    {
        require_once dirname(__FILE__).'/organization.class.php';
        $W = bab_Widgets();
        
        $select = $W->Select();
        $select->addOption('', '');
        
        $collections = new absences_OrganizationIterator();
        
        if (0 === $collections->count()) {
            return null;
        }
        
        foreach($collections as $collection) {
            $select->addOption($collection->id, $collection->name);
        }
        
        return $W->LabelledWidget(
            absences_translate('Organization'),
            $select,
            __FUNCTION__
        );
    }
    
    protected function idcol()
    {
        $W = bab_Widgets();
        
        $select = $W->Select();
        $select->addOption('', '');
        
        $collections = new absences_CollectionIterator();
        foreach($collections as $collection) {
            $select->addOption($collection->id, $collection->name);
        }
    
        return $W->LabelledWidget(
            absences_translate('Collection'),
            $select,
            __FUNCTION__
        );
    }
    
    
    
    protected function idsa()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
        global $babDB;
        $W = bab_Widgets();
        
        $select = $W->Select();

        $options = array('' => '');
        $res = $babDB->db_query('SELECT id_sa FROM absences_personnel GROUP BY id_sa');
        while ($arr = $babDB->db_fetch_assoc($res)) {
            
            $scheme = bab_WFGetApprobationInfos($arr['id_sa']);
            
            if ($arr['id_sa'] && $scheme['name']) {
                $options[$arr['id_sa']] = $scheme['name'];
            }
        }
        

        $select->setOptions($options);
        
        return $W->LabelledWidget(
            absences_translate('Approval scheme'),
            $select,
            __FUNCTION__
        );
    }
    
    
    
    protected function max()
    {
        $W = bab_Widgets();
        
        $select = $W->Select()
            ->addOption(20, 20)
            ->addOption(100, 100)
            ->addOption(500, 500)
            ->addOption(9999999999, absences_translate('All'));
        
        $select->setValue(ABSENCES_MAX_AGENTS_LIST);
        
        return $W->LabelledWidget(
            absences_translate('Rows per page'),
            $select,
            __FUNCTION__
        );
    }



    protected function loadFormValues()
    {

        $this->setValues((array) bab_rp('filter'), array('filter'));
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $button = $W->FlowItems(
            $W->SubmitButton()->setName('search')->setLabel(absences_translate('Search'))
        )->setSpacing(1,'em');

        $this->addItem($button);
    }
}
