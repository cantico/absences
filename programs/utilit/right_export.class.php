<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/csv.class.php';

/**
 * Export all rights or year rights for each agents on a list of agents
 *
 */
class absences_RightExportCsv extends absences_Csv
{
    
    /**
     * 
     * @var int
     */
    protected $org;
    
    /**
     * 
     * @var int
     */
    protected $year;
    
    
    /**
     * @var array
     */
    protected $dirFields;

    
    /**
     * 
     * @var string
     */
    protected $outputCharset = null;
    
    /**
     * @param int    $org
     * @param int    $year
     * @param array  $dirFields         List of additional directory fields
     */
    public function __construct($org, $year, Array $dirFields, $separator = null, $sepdec = null, $outputCharset = null)
    {
        if (!empty($org)) {
            $this->org = $org;
        }
        
        if (!empty($year)) {
            $this->year = $year;
        }
        
        $this->dirFields = $dirFields;
        
        if (isset($separator)) {
            $this->separator = $separator;
        }
        
        if (isset($sepdec)) {
            $this->sepdec = $sepdec;
        }
        
        $this->outputCharset = bab_Charset::getIso();
        
        if (isset($outputCharset)) {
            $this->outputCharset = $outputCharset;
        }
    }
    
    
    /**
     * @return absences_AgentIterator
     */
    protected function selectAgents()
    {
        require_once dirname(__FILE__).'/organization.class.php';
        
        $res = new absences_AgentIterator();
        
        if (isset($this->org)) {
            $organization = absences_Organization::getById($this->org);
            $res->setOrganization($organization);
        }
        
        return $res;
    }
    
    
    
    
    /**
     * 
     * @param absences_Agent $agent
     * @return absences_AgentRightManagerIterator
     */
    protected function selectAgentRights(absences_Agent $agent)
    {
        require_once dirname(__FILE__).'/agent_right.class.php';
		$I = new absences_AgentRightManagerIterator;
		$I->setAgent($agent);
		
		if (isset($this->year)) {
		    $I->year = $this->year;
		}
		
		return $I;
    }
    
    
    /**
     * @return string[]
     */
    protected function getHeader()
    {
        $row = array(
            absences_translate('Lastname'),
            absences_translate('Firstname'),
            absences_translate('Right collection'),
            absences_translate('Organization'),
            absences_translate('Right description'),
            absences_translate('Type'),
            absences_translate('Initial quantity'),
    		absences_translate('Consumed'),
    		absences_translate('Waiting approval'),
    		absences_translate('Balance'),
    		absences_translate('Begin date'),
    		absences_translate('End date'),
    		absences_translate('Accessible')
        );
        
        $dir = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

        foreach ($this->dirFields as $fieldname) {
            
            if (!isset($dir[$fieldname])) {
                $row[] = '';
                continue;
            }
            
            
            $row[] = $dir[$fieldname]['name'];
        }
        
        return $row;
    }
    
    /**
     * @return array
     */
    protected function getRow(absences_Agent $agent, absences_AgentRight $agentRight)
    {
        $collectionName = '';
        $collection = $agent->getCollection();
        if ($collection->getRow()) {
            $collectionName = $collection->name;
        }
        
        $organizationName = '';
        if ($organization = $agent->getOrganization()) {
            $organizationName = $organization->name;
        }
        
        $right = $agentRight->getRight();
        
        $name = bab_getUserName($agent->getIdUser(), false);
        
        
        if (!$name['lastname'] && !$name['firstname']) {
            // this is a deleted user
            return null;
        }
        
        $accessible = $agentRight->isAccessibleByValidityPeriod()
            && $agentRight->isAccessibleOnPeriod(time(), time())
            && $agentRight->isAcessibleByDirectoryEntry() 
            && $right->isAccessibleIfFixed();
        
        $row = array(
            $name['lastname'],
            $name['firstname'],
            $collectionName,
            $organizationName,
            $right->description,
            $right->getType()->name,
            $agentRight->getQuantity(),
            $agentRight->getConfirmedQuantity(),
            $agentRight->getWaitingQuantity(),
            $agentRight->getBalance(),
            $this->date($right->date_begin),
            $this->date($right->date_end),
            $accessible ? '1' : '0'
        );
        
        foreach ($this->dirFields as $field) {
            $row[] = $this->getAgentDirValue($agent, $field);
        }
        
        return $row;
    }
    
    


    
    
    public function download()
    {
        $this->setHeaders(absences_translate('rights'));
        
        $this->outputArr($this->getHeader());
        
        foreach ($this->selectAgents() as $agent) {
            
            // allocated duration per agent
            bab_setTimeLimit(10);
            
            $res = $this->selectAgentRights($agent);
            
            foreach ($res as $agentRight) {
                $arr = $this->getRow($agent, $agentRight);
                if (!isset($arr)) {
                    continue;
                }
                $this->outputArr($arr);
            }
        }
        
        die();
    }
}







class absences_RightExportTemplate
{
    public $separatortxt;
    public $other;
    public $comma;
    public $tab;
    public $semicolon;
    public $export;
    public $sepdectxt;
    public $t_year;
    public $t_organization;
    public $additional_fields;
    
    protected $resYears;
    protected $resOrganization;
    protected $dirfields;
    
    public $year;
    public $fieldname;
    public $organization;
    public $id_organization;
    
    public function __construct()
    {
        global $babDB;
        
        $this->separatortxt = absences_translate("Separator");
        $this->other = absences_translate("Other");
        $this->comma = absences_translate("Comma");
        $this->tab = absences_translate("Tab");
        $this->semicolon = absences_translate("Semicolon");
        $this->export = absences_translate("Export");
        $this->sepdectxt = absences_translate("Decimal separator");
        $this->t_year = absences_translate('Year filter');
        $this->t_organization = absences_translate('Organization');
        $this->additional_fields = absences_translate("Additional fields to export:");

        $this->resYears = $babDB->db_query("SELECT YEAR(date_begin) year FROM absences_rights WHERE YEAR(date_begin)<>'0' GROUP BY year");
        
        $this->resOrganization = $babDB->db_query("SELECT * FROM `absences_organization` ORDER BY name ASC");

        $this->dirfields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
        
        unset($this->dirfields['sn']);
        unset($this->dirfields['givenname']);
        unset($this->dirfields['jpegphoto']);
    }
    
    

    /**
     * template method to list available years
     */
    public function getnextyear()
    {
        global $babDB;
    
        if ($arr = $babDB->db_fetch_assoc($this->resYears))
        {
            $this->year = bab_toHtml($arr['year']);
            return true;
        }
    
        return false;
    }
    
    
    public function getnextfield()
    {
        if (list($name,$arr) = each($this->dirfields))
        {
            $this->fieldname = bab_toHtml($name);
            $this->fieldlabel = bab_toHtml($arr['name']);
            return true;
        }
        return false;
    }
    
    
    public function getnextorganization()
    {
        global $babDB;
        
        if ($arr = $babDB->db_fetch_assoc($this->resOrganization))
        {
            $this->organization = bab_toHtml($arr['name']);
            $this->id_organization = bab_toHtml($arr['id']);
            return true;
        }
        
        return false;
    }
}












function absences_exportForm()
{
    if (!empty($_POST)) {
        
        $separator = ',';
        
        switch (bab_rp('wsepar')) {
            case '0':
                $separator = bab_rp('separ');
                break;
                
            case '1':
                $separator = ',';
                break;
                
            case '2':
                $separator = "\t";
                break;
            
            case '3':
                $separator = ';';
                break;
        }
        
        $dirfields = array_keys((array) bab_pp('dirfields'));

        $export = new absences_RightExportCsv(bab_rp('organization'), bab_rp('year'), $dirfields, $separator, bab_rp('sepdec'));
        $export->download();
    }
    
    
    $addon = bab_getAddonInfosInstance('absences');
    
    $template = new absences_RightExportTemplate();
    bab_getBody()->babecho($addon->printTemplate($template, 'rightexport.html'));
}