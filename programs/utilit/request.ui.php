<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/agent.class.php';
require_once dirname(__FILE__).'/agent.ui.php';


class absences_requestDetail
{
	public $daterequesttxt;
	public $daterequest;
	
	public $commenttxt;
	public $comment;
	public $remarktxt;
	public $remark;
	public $statustxt;
	public $status;
	
	public $t_history;
	public $t_author;
	public $t_message;
	
	public $agentcard = false;
	
	public $disp_movements;
	private $movements;
	
	public $altbg = true;
	
	protected $request;
	
	public function __construct(absences_Request $request, $withCardFrame = null)
	{
	    $this->request = $request;
	    
		$this->daterequesttxt = absences_translate("Request date");
		$this->t_createdOn = absences_translate('Date');
		$this->t_comment = absences_translate('Applicant comment');
		$this->statustxt = absences_translate("Status");
		$this->commenttxt = absences_translate("Appliquant comments");
		$this->remarktxt = absences_translate("Approvers comment");
		$this->t_history = absences_translate('History');
		$this->t_author = absences_translate('Author');
		$this->t_message = absences_translate('Message');
		$this->t_mvt_comment = absences_translate('Regularization or approval comment by the author');
		
		
		$W = bab_Widgets();
		$agent = absences_Agent::getFromIdUser($request->id_user);
		$this->owner = bab_toHtml($agent->getName());
		
		if (!isset($withCardFrame))
		{
			$withCardFrame = ($agent->getIdUser() !== bab_getUserId());
		}
		
		if ($withCardFrame)
		{
			$card = new absences_AgentCardFrame($agent);
			$this->agentcard = $card->display($W->HtmlCanvas());
		}
		
		
		$this->daterequest = bab_toHtml(bab_longDate(bab_mktime($request->createdOn())));
		
		$this->comment = bab_toHtml($request->comment, BAB_HTML_ALL);
		$this->remark = bab_toHtml($request->comment2, BAB_HTML_ALL);
		$this->status = bab_toHtml($request->getStatusStr());
		
		
		$this->movements = $request->getMovementIterator();
		
		$this->movements->rewind();
		$this->disp_movements = true;
		
		if ($this->movements->count() == 0)
		{
			$this->disp_movements = false;
		} 
	}
	
	
	/**
	 * 
	 * @var bool $manager_view
	 */
	protected function initDelete($manager_view)
	{
	    $this->url = isset($_REQUEST['from']) ? $_REQUEST['from'] : $_SERVER['HTTP_REFERER'];
	    $this->t_delete = absences_translate("Delete");
	    $this->delete_with_approval = false;
	    $this->rfrom = $manager_view ? '1' : '0';
	    
	    if ('Y' === $this->request->status && !$manager_view) {
	        $this->delete_with_approval = absences_translate('The request will be deleted after approval');
	    }
	}
	
	
	
	
	public function getnextmvt()
	{
		if ($this->movements->valid())
		{
			$movement = $this->movements->current();
			/*@var $movement absences_Movement */
			
			$this->altbg = !$this->altbg;
			
			$this->createdOn = bab_toHtml(bab_shortDate(bab_mktime($movement->createdOn), true));
			$this->author = bab_toHtml(bab_getUserName($movement->id_author));
			$this->comment = bab_toHtml($movement->comment);
			$this->message = bab_toHtml($movement->message);
	
			$this->movements->next();
			return true;
		}
	
		return false;
	}
}




/**
 * Visualisation popup
 */
class absences_vacationRequestDetail extends absences_requestDetail
	{
	public $datebegintxt;
	public $datebegin;
	public $halfnamebegin;
	public $dateendtxt;
	public $dateend;
	public $halfnameend;
	public $nbdaystxt;
	public $typename;
	public $nbdays;
	public $totaltxt;
	public $totalval;

			
	public $arr = array();
	public $count;
	public $res;
	public $veid;
	public $wusers = array();

	public $approb;

    public $altbg = true;

	public function __construct($id, $withCardFrame = null)
		{
		require_once dirname(__FILE__).'/agent.class.php';
		require_once dirname(__FILE__).'/agent.ui.php';
		require_once dirname(__FILE__).'/entry.class.php';
		require_once $GLOBALS['babInstallPath'].'/utilit/urlincl.php';
			
		
		$this->datebegintxt = absences_translate("Begin date");
		$this->dateendtxt = absences_translate("End date");
		$this->nbdaystxt = absences_translate("Quantities");
		$this->totaltxt = absences_translate("Total");
		$this->t_approb = absences_translate("Approver");
		$this->t_folder = absences_translate('Others dates in the same recurring request:');
		$this->t_createdby = absences_translate("Created by");
		

		$entry = absences_Entry::getById($id);
		
		if (!$entry->getRow()) {
		    throw new Exception(absences_translate('This entry does not exists'));
		}
		
		parent::__construct($entry, $withCardFrame);
		

		switch($entry->creation_type)
		{
			case absences_Entry::CREATION_FIXED:
				$this->info = absences_translate('This request has been automatically created with a fixed vacation right');
				break;
				
			default:
			case absences_Entry::CREATION_USER:
				$this->info = false;
				break;
		}
		
		if ($entry->createdby == $entry->id_user)
		{
			$this->createdby = false;
		} elseif($entry->createdby) {
			$this->createdby = bab_toHtml(bab_getUserName($entry->createdby));
		} else {
		    $this->createdby = false;
		}
		
		$this->datebegin = bab_toHtml(absences_longDate(bab_mktime($entry->date_begin)));
		$this->dateend = bab_toHtml(absences_longDate(bab_mktime($entry->date_end)));
		
		$this->approb = '';
		if ($entry->id_approver > 0) {
		    $this->approb = bab_toHtml(bab_getUserName($entry->id_approver));
		}
		
		$this->res = $entry->getElementsIterator();
		$this->res->rewind();
		
		$this->totalval = absences_vacEntryQuantity($id);
		$this->veid = $id;
		
		if ($this->folder = $entry->getFolderEntriesIterator())
		{
			$this->folder->rewind();
		}
	}
	
	
	

	public function getnexttype()
		{
		
		if( $this->res->valid())
			{
			$this->altbg = !$this->altbg;
			    
			$elem = $this->res->current();
			/*@var $elem absences_EntryElem */
			$right = $elem->getRight();
			$type = $right->getType();
			$rgroup = $right->getRgroup();
			
			if ($rgroup && $rgroup->getRow())
			{
				$this->rgroup_name = $rgroup->name;
			} else {
				$this->rgroup_name = '';
			}
			
			$this->typename = bab_toHtml($type->name);
			$this->typecolor = bab_toHtml($type->color);
			
			$this->period = bab_toHtml(absences_DateTimePeriod($elem->date_begin, $elem->date_end));
			$this->rightname = bab_toHtml($right->description);
			$this->nbdays = bab_toHtml(absences_quantity($elem->quantity, $right->quantity_unit));
			$this->res->next();
			return true;
			}
		
		return false;

		}
		
		
	public function getnextfe()
	{
		
		if ($this->folder->valid())
		{
			$entry = $this->folder->current();
			/*@var $entry absences_Entry */
			$this->date = bab_toHtml(bab_shortDate(bab_mktime($entry->date_begin), false));
			
			$url = bab_url::get_request_gp();
			$url->id = $entry->id;
			$this->url = bab_toHtml($url->toString());
			$this->folder->next();
			return true;
		}
		
		return false;
	}
		

}






class absences_CetDepositRequestDetail extends absences_requestDetail 
{
	public function __construct($id)
	{
		require_once dirname(__FILE__).'/cet_deposit_request.class.php';
		$request = absences_CetDepositRequest::getById($id);
		
		parent::__construct($request);
		
		
		$this->t_rightsource = absences_translate('Right source');
		$this->t_quantity = absences_translate('Quantity');
		
		
		$source = $request->getAgentRightSource()->getRight();
		
		$this->rightsource = bab_toHtml($source->description);
		$this->quantity = bab_toHtml(absences_quantity($request->quantity, $source->quantity_unit));
	}
}






class absences_WpRecoveryDetail extends absences_requestDetail
{
	public function __construct($id)
	{
		require_once dirname(__FILE__).'/workperiod_recover_request.class.php';
		$request = absences_WorkperiodRecoverRequest::getById($id);

		parent::__construct($request);
		
		
		$this->t_period = absences_translate('Worked period');
		$this->period = absences_DateTimePeriod($request->date_begin, $request->date_end);
		
		$this->t_type = absences_translate('Type');
		$this->type = $request->getType()->name;

		
		$this->t_quantity = absences_translate('Quantity');
		$this->quantity = bab_toHtml(absences_quantity($request->quantity, $request->quantity_unit));
	}
}




/**
 * Display a vacation request
 * @param int $id
 * 
 */
function absences_viewVacationRequestDetail($id, $withCardFrame = null) {
	global $babBody;
	/*@var $babBody babBody */
	
	try {
    	$temp = new absences_vacationRequestDetail($id, $withCardFrame);
    	$babBody->setTitle(absences_translate('Vacation request'));
    	$html = absences_addon()->printTemplate($temp, "request.html", "ventrydetail");
    	
    	if (bab_rp('popup')) {
    	    $babBody->menu = new babMenu(); // remove menu
    	    $babBody->babPopup($html);
    	} else {
    	    $babBody->babEcho($html);
    	}
    	
    	return $temp->count;
    	
	} catch(Exception $e) {
	    $babBody->addError($e->getMessage());
	}
	
	
	
	
}



function absences_viewCetDepositDetail($id) {
	global $babBody;
	$temp = new absences_CetDepositRequestDetail($id);
	$babBody->setTitle(absences_translate('Time saving account deposit request'));
	$babBody->babEcho(absences_addon()->printTemplate($temp, "request.html", "cetdeposit"));
}

function absences_viewWpRecoveryDetail($id) {
	global $babBody;
	$temp = new absences_WpRecoveryDetail($id);
	$babBody->setTitle(absences_translate('Working day recovery request'));
	$babBody->babEcho(absences_addon()->printTemplate($temp, "request.html", "wprecovery"));
}
