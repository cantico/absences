<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
************************************************************************
* Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
*                                                                      *
* This file is part of Ovidentia.                                      *
*                                                                      *
* Ovidentia is free software; you can redistribute it and/or modify    *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation; either version 2, or (at your option)  *
* any later version.													*
*																		*
* This program is distributed in the hope that it will be useful, but  *
* WITHOUT ANY WARRANTY; without even the implied warranty of			*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
* See the  GNU General Public License for more details.				*
*																		*
* You should have received a copy of the GNU General Public License	*
* along with this program; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
* USA.																	*
************************************************************************/

include_once $GLOBALS['babInstallPath']."utilit/ocapi.php";
include_once $GLOBALS['babInstallPath']."utilit/urlincl.php";

/**
 * Tester si l'utilisateur en cours est un superieur de l'utilisateur passe en parametre ou si c'est la meme personne
 * @param int $id_user
 *
 * @return bool
 */
function absences_IsUserUnderSuperior($id_user)
{
	if ($id_user == bab_getUserId())
	{
		return true;
	}

	require_once dirname(__FILE__).'/agent.class.php';

	$testedAgent = absences_Agent::getFromIdUser($id_user);
	$currentAgent = absences_Agent::getCurrentUser();

	return $currentAgent->isSuperiorOf($testedAgent);
}








class absences_notifyOnRequestChangeCls
{
	function __construct(absences_Entry $entry, $msg)
	{
		$this->message = $msg;
		$this->fromuser = absences_translate("User");
		$this->from = absences_translate("from");
		$this->until = absences_translate("until");
		$this->quantitytxt = absences_translate("Quantity");
		$this->commenttxt = absences_translate("Comment");
		$this->username = bab_toHtml($entry->getUserName());
		$this->begindate = bab_longDate(bab_mktime($entry->date_begin));
		$this->enddate = bab_longDate(bab_mktime($entry->date_end));
		$this->comment = bab_toHtml($entry->comment);
		$this->quantity = bab_toHtml(absences_vacEntryQuantity($entry->id));
	}
}




/**
 * Notifier le demandeur si la demande de conges est modifiee ou supprime
 * @param array | Iterator 	$entries
 * @param int				$id_user		Recipient
 * @param bool 	$delete
 */
function absences_notifyOnRequestChange($entries, $id_user, $delete = false)
{
	global $BAB_SESS_USER, $BAB_SESS_EMAIL;


	$mail = bab_mail();
	if( $mail == false )
		return;

	$mail->mailTo(bab_getUserEmail($id_user), bab_getUserName($id_user));

	$mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);

	$msg = $delete ? absences_translate("Vacation request has been deleted") : absences_translate("Vacation request has been modified");
	$mail->mailSubject($msg);

	$message = '';
	$messagetxt = '';

	foreach($entries as $entry)
	{
		$tempb = new absences_notifyOnRequestChangeCls($entry, $msg);
		$message .= $mail->mailTemplate(absences_addon()->printTemplate($tempb, "mailinfo.html", "newvacation"));
		$messagetxt .= absences_addon()->printTemplate($tempb, "mailinfo.html", "newvacationtxt");
	}

	$mail->mailBody($message, "html");
	$mail->mailAltBody($messagetxt);

	$mail->send();
}


/**
 * Notification for the manager when a request is deleted/modified
 *
 *
 */
class absences_notifyManagers
{
	private $res;

	public function __construct(Array $row, $msg)
	{
		global $babDB;


		$this->message = $msg;
		$this->fromuser = absences_translate("User");
		$this->from = absences_translate("from");
		$this->until = absences_translate("until");
		$this->username = bab_toHtml(bab_getUserName($row['id_user']));
		$this->begindate = bab_longDate(bab_mktime($row['date_begin']));
		$this->enddate = bab_longDate(bab_mktime($row['date_end']));
		$this->res = $babDB->db_query("select
				r.description,
				e.quantity,
				r.quantity_unit,
				rg.name rgroup_name
				from
				absences_entries_elem e,
				absences_rights r
				LEFT JOIN absences_rgroup rg ON rg.id=r.id_rgroup
				where
				e.id_right=r.id
				AND e.id_entry =".$babDB->quote($row['id'])
		);
		$this->comment = bab_toHtml($row['comment']);
	}


	public function getnextelem()
	{
		global $babDB;

		if ($arr = $babDB->db_fetch_assoc($this->res))
		{
			$this->description = bab_toHtml($arr['description']);
			$this->descriptiontxt = $arr['description'];
			$this->quantity = bab_toHtml(absences_quantity($arr['quantity'], $arr['quantity_unit']));
			$this->rgroup_name = bab_toHtml($arr['rgroup_name']);
			return true;
		}

		return false;
	}


	public static function send($id, $delete = false)
	{

		$mail = bab_mail();
		if( $mail == false )
		{
			return;
		}

		$notify = (bool) absences_getVacationOption('email_manager_ondelete');
		if (!$notify)
		{
			return;
		}


		global $babDB, $BAB_SESS_EMAIL, $BAB_SESS_USER;


		$row = $babDB->db_fetch_array($babDB->db_query("select * from ".ABSENCES_ENTRIES_TBL." where id='".$babDB->db_escape_string($id)."'"));

		if (!$row)
		{
			return;
		}

		$mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);

		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		$managers = aclGetAccessUsers('absences_managers_groups', 1);
		foreach ($managers as $m)
		{
			$mail->mailTo($m['email'], $m['name']);
		}

		$msg = $delete ? absences_translate("Vacation request has been deleted") : absences_translate("Vacation request has been modified");
		$mail->mailSubject($msg);

		$tempb = new absences_notifyManagers($row, $msg);
		$message = $mail->mailTemplate(absences_addon()->printTemplate($tempb, "mailinfo.html", "vacmanager"));
		$mail->mailBody($message, "html");

		$message = absences_addon()->printTemplate($tempb, "mailinfo.html", "vacmanagertxt");
		$mail->mailAltBody($message);

		$mail->send();
	}
}










/**
 * Get list of right for a user
 *
 * @param	string|false	$begin		ISO datetime
 * @param	string|false	$end		ISO datetime
 * @param	int|false		$id_user
 * @param	1|0				$rfrom		test active flag on right if user is not manager
 *
 * @return array
 */
function absences_getRightsOnPeriod($begin = false, $end = false, $id_user = false, $rfrom = 0)
{
	require_once dirname(__FILE__).'/agent_right.class.php';
	require_once dirname(__FILE__).'/agent.class.php';

	$return = array();
	$begin = $begin ? bab_mktime( $begin ) : $begin;
	$end = $end ? bab_mktime( $end ) : $end;


	if (!$id_user)  {
		$id_user = $GLOBALS['BAB_SESS_USERID'];

	}

	$agent = absences_Agent::getFromIdUser($id_user);

	$res = new absences_AgentRightUserIterator();
	$res->setAgent($agent);

	if( $rfrom == 1 )
	{
		$acclevel = absences_vacationsAccess();
		if( isset($acclevel['manager']) && $acclevel['manager'])
		{
			$res->showInactive();
		}
	}

	if ($begin && $end)
	{
		$res->setRequestPeriod($begin, $end);
	}



	foreach ($res as $agentRight )
	{

		/*@var $agentRight absences_AgentRight */


		$right = $agentRight->getRight();


		$return[$right->id] = array(
				'id'					=> $right->id,
				'date_begin'			=> $right->date_begin,
				'date_end'				=> $right->date_end,
		        'date_begin_valid'      => $right->date_begin_valid, // disponibilite en fonction de la date de saisie
		        'date_end_valid'        => $right->date_end_valid,
				'quantity'				=> $right->quantity,
				'quantity_unit'			=> $right->quantity_unit,
				'description'			=> $right->description,
				'cbalance'				=> $right->cbalance,
				'quantity_available'	=> $agentRight->getAvailableQuantity(),
				'used'					=> $agentRight->getConfirmedQuantity(),
				'waiting'				=> $agentRight->getWaitingQuantity(),
				'no_distribution'		=> $right->no_distribution,
				'id_rgroup'				=> $right->id_rgroup,
				'rgroup'				=> $right->getRgroupLabel(),
				'agentRight'			=> $agentRight,
				'sortkey'				=> $right->id_rgroup > 0 ? $right->getRgroupSortkey() : $right->getSortKey()
		);
	}


	return $return;
}

/**
 * Rights list for on user, grouped by right groups
 * @param int $id_user
 * @param int $rfrom
 */
function absences_getRightsByGroupOnPeriod($id_user, $rfrom = 0) {

	require_once dirname(__FILE__).'/agent_right.class.php';
	require_once dirname(__FILE__).'/agent.class.php';

	if (!$id_user)  {
		$id_user = $GLOBALS['BAB_SESS_USERID'];
	}

	$agent = absences_Agent::getFromIdUser($id_user);

	// creer les droits de report eventuels avant d'afficher la liste des droits
	$agent->createReports();

	$res = $agent->getAgentRightUserIterator();

	if( $rfrom == 1 )
	{
		$current_Agent = absences_Agent::getCurrentUser();
		if($current_Agent->isManager())
		{
			$res->showInactive();
		}
	}

	$rights = array();
	foreach($res as $agentRight) {

		/*@var $agentRight absences_AgentRight */

		$right = $agentRight->getRight();


		if (empty($right->id_rgroup) || null === $right->getRgroupLabel()) {
			$id				= 'r'.$right->id;
			$description	= $right->description;
			$type 			= $right->getType();
			$sortkey		= (int) $right->sortkey;
		} else {
			$id 			= 'g'.$right->id_rgroup;
			$description	= $right->getRgroupLabel();
			$type 			= null;
			$sortkey		= $right->getRgroupSortkey();
		}

		if (isset($rights[$id])) {
			$quantity			= $rights[$id]['quantity'] + $right->quantity;
			$quantity_available	= $rights[$id]['quantity_available'] + $agentRight->getAvailableQuantity();
			$used				= $rights[$id]['used'] + $agentRight->getConfirmedQuantity();
			$waiting			= $rights[$id]['waiting'] + $agentRight->getWaitingQuantity();
			$previsional        = $rights[$id]['previsional'] + $agentRight->getPrevisionalQuantity();
		} else {
			$quantity			= $right->quantity;
			$quantity_available	= $agentRight->getAvailableQuantity();
			$used				= $agentRight->getConfirmedQuantity();
			$waiting			= $agentRight->getWaitingQuantity();
			$previsional        = $agentRight->getPrevisionalQuantity();
		}

		$rights[$id] = array(
				'description'			=> $description,
				'quantity'				=> $quantity,
				'quantity_unit'			=> $right->quantity_unit,
				'quantity_available'	=> $quantity_available,
				'used'					=> $used,
				'waiting'				=> $waiting,
		        'previsional'           => $previsional,
				'type'					=> $type,
				'sortkey'				=> $sortkey
		);
	}

	bab_Sort::asort($rights, 'sortkey');

	return $rights;
}




class absences_Paginate
{
	/**
	 *
	 * @var int
	 */
	public $pos;

	public $topurl 		= "";
	public $bottomurl 	= "";
	public $nexturl 	= "";
	public $prevurl 	= "";

	public $t_position 	= '';
	
	public $display_pagination = false;


	protected function paginate($total, $max)
	{
		$this->t_first_page = absences_translate('First page');
		$this->t_previous_page = absences_translate('Previous page');
		$this->t_next_page = absences_translate('Next page');
		$this->t_last_page = absences_translate('Last page');


		$this->pos = (int) bab_rp('pos', 0);

		if( $total > $max )
		{
		    $this->display_pagination = true;
			require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
				
			$tmpurl = bab_url::get_request_gp();

			$page_number = 1 + ($this->pos / $max);
			$page_total = 1 + ($total / $max);
			$this->t_position = sprintf(absences_translate("Page %d/%d"), $page_number,$page_total);

			if( $this->pos > 0)
			{
				$tmpurl->pos = 0;
				$this->topurl = bab_toHtml($tmpurl->toString());
			}

			$next = $this->pos - $max;
			if( $next >= 0)
			{
				$tmpurl->pos = $next;
				$this->prevurl = bab_toHtml($tmpurl->toString());
			}

			$next = $this->pos + $max;
			if( $next < $total)
			{
				$tmpurl->pos = $next;
				$this->nexturl =  bab_toHtml($tmpurl->toString());

				if( $next + $max < $total)
				{
					$bottom = $total - $max;
				}
				else
				{
					$bottom = $next;
				}

				$tmpurl->pos = $bottom;
				$this->bottomurl =  bab_toHtml($tmpurl->toString());
			}
		}

	}
}





abstract class absences_MovementList extends absences_Paginate
{
    const MAX = 20;

    public $altbg = true;

    public $t_date;
    public $date;

    public $t_author;
    public $author;

    public $t_message;
    public $message;

    public $t_comment;
    public $comment;

    protected $res;

    public function __construct()
    {
        $this->t_date = absences_translate('Date');
        $this->t_author = absences_translate('Author');
        $this->t_request = absences_translate('Object');
        $this->t_message = absences_translate('Message');
        $this->t_comment = absences_translate('Regularization or approval comment by the author');

        bab_functionality::includeOriginal('Icons');
    }

    public function getnext()
    {
        if (($this->res->key() - $this->pos) >= self::MAX)
        {
            return false;
        }

        if ($this->res->valid())
        {
            $W = bab_Widgets();
            $movement = $this->res->current();
            /*@var $movement absences_Movement */
             
            $this->altbg = !$this->altbg;

            $this->author = bab_toHtml(bab_getUserName($movement->id_author));
            $this->date = bab_toHtml(bab_shortDate(bab_mktime($movement->createdOn)));
            if ($request = $movement->getRequest())
            {
                $this->request = $request->getManagerFrame()->addClass(Func_Icons::ICON_LEFT_16)->display($W->HtmlCanvas());
            } else {
                $this->request = '';
            }
            $this->message = bab_toHtml($movement->message);
            $this->comment = bab_toHtml($movement->comment);
             
            $this->res->next();
            return true;
        }

        return false;
    }

    public function getHtml()
    {
        $babBody = bab_getInstance('babBody');

        if ($this->res->count() === 0)
        {
            $babBody->addError(absences_translate('There are no records yet'));
            return '';
        }

        return absences_addon()->printTemplate($this, "agent.html", "movement");
    }
}





class absences_listVacationRequestsCls extends absences_Paginate
{
	public $altbg = true;
	public $cardframe;

	/**
	 *
	 * @var int
	 */
	public $folder;

	/**
	 *
	 * @var absences_EntryIterator
	 */
	private $folderIterator;

	/**
	 * @var array
	 */
	private $users;

	public $res;
	public $total;

	/**
	 * 1 si les demandes sont modifiee par une autre personne que l'auteur
	 * @var int
	 */
	public $rfrom = 0;

	/**
	 * Entitee de l'organigramme
	 * @var int
	 */
	public $ide = null;


	public function __construct($id_user, $display_username)
	{
		require_once dirname(__FILE__).'/request.class.php';


		$this->uncheckall = absences_translate("Uncheck all");
		$this->checkall = absences_translate("Check all");
		$this->nametxt = absences_translate("Fullname");
		$this->begindatetxt = absences_translate("Begin date");
		$this->enddatetxt = absences_translate("End date");
		$this->quantitytxt = absences_translate("Quantity");
		$this->statustxt = absences_translate("Status");
		$this->calendar = absences_translate("My planning");
		$this->t_entity_planning = absences_translate("Department planning");
		$this->myrights = absences_translate("My rights");
		$this->t_edit = absences_translate("Modification");
		$this->t_submit_previsional = absences_translate("Submit for approval");

		$this->t_first_page = absences_translate("First page");
		$this->t_previous_page = absences_translate("Previous page");
		$this->t_next_page = absences_translate("Next page");
		$this->t_last_page = absences_translate("Last page");

		$this->details = absences_translate("View details");
		$this->t_delete = absences_translate("Delete");

		$this->display_username = $display_username;



		if (!is_array($id_user))
			$this->users = array($id_user);
		else
			$this->users = $id_user;

		$this->calurl = bab_toHtml(absences_addon()->getUrl()."planning&idx=cal&idu=".implode(',', $this->users)."&popup=1");
		$this->personal = ($id_user == bab_getUserId());

		if ($this->personal)
		{
			$agent = absences_Agent::getCurrentUser();
			$entity = $agent->getMainEntity();
			$this->entity_planning = $agent->canViewMainEntityPlanning();
			$this->entity_calurl = bab_toHtml(absences_addon()->getUrl()."planning&idx=entity_cal&ide=".$entity['id']."&popup=1&emptylines=1");
		}

		$this->myrightsurl = bab_toHtml(absences_addon()->getUrl()."vacuser&idx=myrights");


		$this->res = new absences_RequestIterator($this->users);
		$this->res->one_per_folder = true;
		$this->total = $this->res->count();

		$this->paginate($this->total, ABSENCES_MAX_REQUESTS_LIST);

		$this->res->rewind();
		$this->res->seek($this->pos);
	}


	public function getnext()
	{

		if (($this->res->key() - $this->pos) >= ABSENCES_MAX_REQUESTS_LIST)
		{
			return false;
		}


		if ($this->res->valid())
		{
			$this->altbg = !$this->altbg;
			$request = $this->res->current();
			if (isset($request->folder) && $request->folder > 0)
			{
				$this->folder = $request->folder;
				$this->folderIterator = new absences_EntryIterator;
				$this->folderIterator->orderby = 'date_begin ASC';
				$this->folderIterator->folder = $this->folder;
				$this->folderIterator->id_entry_folder = $request->id;
				$this->folderIterator->users = $this->users;
			} else {
				$this->folder = null;
				$this->folderIterator = null;
			}
				
			$this->cardframe = $request->getCardFrame($this->display_username, $this->rfrom, $this->ide)->display(bab_Widgets()->HtmlCanvas());

			$this->res->next();
			return true;
		}
		return false;
	}



	public function getnextfolded()
	{
		if (isset($this->folderIterator) && $this->folderIterator->valid())
		{
			$request = $this->folderIterator->current();
			$this->cardframe = $request->getCardFrame($this->display_username, $this->rfrom, $this->ide)->display(bab_Widgets()->HtmlCanvas());
			$this->folderIterator->next();
			return true;
		}

		return false;
	}
}


/**
 * Liste des demandes
 *
 * @param array | int 	$id_user
 * @param bool 			$display_username
 * @param int			$rfrom				si les demandes de la liste sont modifiee par un gestionnaire ou gestionnaire delegue
 * @param int			$ide				id entite de l'organigramme >0 si gestionnaire delegue seulement
 */
function absences_listVacationRequests($id_user, $display_username = false, $rfrom = 0, $ide = null)
{
	$babBody = bab_getInstance('babBody');
	/*@var $babBody babBody */

	if (empty($id_user)) {
		$babBody->msgerror = absences_translate("ERROR: No members");
		return;
	}

	$temp = new absences_listVacationRequestsCls($id_user, $display_username);
	$temp->rfrom = $rfrom;
	$temp->ide = $ide;

	if (0 === $temp->total)
	{
		$babBody->addMessage(absences_translate('You have no ongoing requests'));
	}


	$babBody->addStyleSheet(absences_addon()->getStylePath().'vacation.css');
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacuser.html", "vrequestslist"));
}






/**
 *
 * @param int $id		ID user
 */
function absences_listRightsByUser($id)
{
	global $babBody;
	require_once dirname(__FILE__).'/agent.ui.php';

	$temp = new absences_AgentRightsList($id);
	$html = absences_addon()->printTemplate($temp, "vacadm.html", "rlistbyuser");
	
	if (1 === (int) bab_rp('popup', 0))
	{
	    $babBody->babPopup($html);
	} else {
	    $babBody->babEcho($html);
	}
}











/**
 * Mise a jour de la liste des droits d'un utilisateur
 *
 * @param	int		$userid
 * @param	array	$quantity
 *
 * @return 	boolean
 */
function absences_updateVacationRightByUser($userid, $quantity, $comment)
{
	require_once dirname(__FILE__).'/changes.class.php';

	$agent = absences_Agent::getFromIdUser($userid);
	foreach($agent->getAgentRightManagerIterator() as $agentRight)
	{
		$right = $agentRight->getRight();
		$changes = new absences_Changes();

		if (!isset($quantity[$right->id]))
		{
			continue;
		}
		
		$agentRightQuantity  = $agentRight->getQuantityFromInput($quantity[$right->id]);
		if (!isset($agentRightQuantity))
		{
		    continue;
		}

		
		$old_quantity = absences_quantity($agentRight->getQuantity(), $right->quantity_unit);
		$new_quantity = absences_quantity($agentRightQuantity, $right->quantity_unit);

		$changes->setQuantity($old_quantity, $new_quantity);
		$agentRight->quantity = $agentRightQuantity;
		$agentRight->save();
		
		$agentRight->addAgentMovement($agent, $changes, $comment);
	}

	return true;
}










class absences_rlistbyuserUnloadCls
{
    public $message;
    public $close;

    public function __construct($msg)
    {
        $this->message = $msg;
        $this->close = absences_translate("Close");
    }
}





function absences_rlistbyuserUnload($msg)
{
	$temp = new absences_rlistbyuserUnloadCls($msg);
	global $babBody;
	$babBody->babPopup(absences_addon()->printTemplate($temp, "vacadm.html", "rlistbyuserunload"));
}


















function absences_addVacationPersonnel($idp = false)
{
	require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
    require_once dirname(__FILE__).'/agent.ui.php';
	

	$temp = new absences_AgentEdit($idp);
	$temp->printhtml();
}







/**
 *
 * @param	int		$ide
 * @return	boolean
 */
function absences_isAccessibleEntityAsSuperior($ide) {

	$ide = (int) $ide;

	$id_oc = absences_getVacationOption('id_chart');


	$userentities = & bab_OCGetUserEntities($GLOBALS['BAB_SESS_USERID'], $id_oc);
	absences_addCoManagerEntities($userentities, $GLOBALS['BAB_SESS_USERID']);
	$userentities['superior'];

	foreach($userentities['superior'] as $arr) {
		if ($ide === (int) $arr['id']) {
			return true;
		}
	}

	return false;
}


/**
 *
 * @param	int		$ide
 * @return	boolean
 */
function absences_isAccessibleEntityAsCoManager($ide) {

	global $babDB;

	list($n) = $babDB->db_fetch_array($babDB->db_query('
			SELECT COUNT(*) FROM '.ABSENCES_COMANAGER_TBL.'
			WHERE
			id_user='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).'
			AND id_entity='.$babDB->quote($ide).'
			'));

	if ($n > 0) {
		return true;
	}

	return false;
}


/**
 * Relancer les demande en attente sur le nouveau shema d'aprobation
 * n'est plus utilise pour le moment car la modification de l'utilisateur est bloque si il reste des demandes en attente en cours
 *
 * @param int $userid
 * @param int $idsa
 */
function absences_updateVacationUser($userid, $idsa)
{
	global $babDB;

	$res = $babDB->db_query("select * from ".ABSENCES_ENTRIES_TBL." where id_user=".$babDB->quote($userid)." and status=''");
	while( $row = $babDB->db_fetch_array($res)) {
		if( $row['idfai'] != 0 ) {
			deleteFlowInstance($row['idfai']);
		}
		$idfai = makeFlowInstance($idsa, "vac-".$row['id']);
		setFlowInstanceOwner($idfai, $row['id_user']);
		$babDB->db_query("UPDATE ".ABSENCES_ENTRIES_TBL." SET idfai=".$babDB->quote($idfai)." where id=".$babDB->quote($row['id'])."");
		$nfusers = getWaitingApproversFlowInstance($idfai, true);
		absences_notifyVacationApprovers($row['id'], $nfusers);
	}
}



/**
 * Mise a jour du changement de regime
 */
function absences_updateUserColl()
{
	global $babDB;

	if (empty($_POST['idp']))
	{
		return false;
	}

	$agent = absences_Agent::getFromIdUser($_POST['idp']);

	$users_rights = array();
	$worked_ids = array();

	$old_collection = $agent->getCollection();
	$new_collection = absences_Collection::getById($_POST['idcol']);


	$res = $babDB->db_query("SELECT 
	       ur.id,
	       ur.id_right, 
	       cr.id in_collection 
       FROM ".ABSENCES_USERS_RIGHTS_TBL." ur
	       LEFT JOIN 
	           absences_coll_rights cr ON ur.id_right=cr.id_right AND cr.id_coll=".$babDB->quote($old_collection->id)."
       WHERE 
	       id_user=".$babDB->quote($_POST['idp'])."
	");
	
	
	$in_old_collection =array();
	while($arr = $babDB->db_fetch_array($res))
	{
		$users_rights[$arr['id_right']] = $arr['id'];
		if (isset($arr['in_collection'])) {
		    $in_old_collection[$arr['id_right']] = $arr['id_right'];
		}
	}

	$old_rights = $agent->getAgentRightManagerIterator();
	$used = array();
	foreach($old_rights as $r)
	{
		/*@var $r absences_AgentRight */
		$used[$r->id_right] = $r->getConfirmedQuantity();
	}

	$prefix = 'right_';
	$post_rights = array();

	/* control */

	foreach($_POST as $field => $value)
	{
		if (mb_substr($field,0,mb_strlen($prefix)) == $prefix)
		{
		    $value = (float) str_replace(',', '.', $value);
		    
			list(,$id_right) = explode('_',$field);
			if (isset($used[$id_right]))
			{
				$value += $used[$id_right];
			}

			$right = new absences_Right($id_right);
				
			if (((int) round(100*$right->quantity)) === ((int) round(100*$value)))
			{
			    // the value in the input is the default right value
				$value = '';

			} else if ($value < 0)
			{
				if ($right->cbalance == 'N')
				{
					$GLOBALS['babBody']->addError(sprintf(absences_translate("Negative balance are not allowed on right %s"), $right->description));
					return false;

				}
			}


			$post_rights[$id_right] = $value;
		}
	}

	/* RECORD */

	foreach($post_rights as $id_right => $user_quantity)
	{
	    
	    $renewal = isset($_POST['renewal'][$id_right]) ? '1' : '0';
	    
		if (isset($users_rights[$id_right]))
		{
			$babDB->db_query("UPDATE 
			        ".ABSENCES_USERS_RIGHTS_TBL." 
			        SET 
			            quantity='".$babDB->db_escape_string($user_quantity)."', 
			            renewal=".$babDB->quote($renewal)." 
			    WHERE id='".$babDB->db_escape_string($users_rights[$id_right])."'
			");
			$worked_ids[] = $users_rights[$id_right];
		}
		else
		{
			$babDB->db_query("INSERT INTO ".ABSENCES_USERS_RIGHTS_TBL." 
			        (id_user,id_right,quantity, renewal) 
			VALUES 
			        (
			            '".$babDB->db_escape_string($_POST['idp'])."', 
			            '".$babDB->db_escape_string($id_right)."', 
			            '".$babDB->db_escape_string($user_quantity)."',
			            ".$babDB->quote($renewal)."
			       )");
			$worked_ids[] = $babDB->db_insert_id();
		}
	}

	
	if (count($worked_ids) > 0)
	{
	    // on supprime les liens avec les droits de l'ancien regime
	    // il ne faut pas supprimer les droits qui etait en rouge dans la liste
	    
		$babDB->db_query("
		    DELETE FROM ".ABSENCES_USERS_RIGHTS_TBL." 
		    WHERE id NOT IN(".$babDB->quote($worked_ids).") 
		      AND id_user= '".$babDB->db_escape_string($_POST['idp'])."'
		      AND id_right IN(".$babDB->quote($in_old_collection).")
		 ");
	}
	


	

	if ($old_collection->id != $new_collection->id)
	{
		$babDB->db_query("UPDATE ".ABSENCES_PERSONNEL_TBL." SET id_coll='".$babDB->db_escape_string($_POST['idcol'])."' WHERE id_user='".$babDB->db_escape_string($_POST['idp'])."'");

		$agent->addMovement(sprintf(absences_translate('The collection has been modified from %s to %s'), $old_collection->name, $new_collection->name));
	}
	return true;
}


function absences_changeucol($id_user,$newcol)
{
	global $babBody;

	class tempa
	{
		var $altbg = true;

		function tempa($id_user,$newcol)
		{
			$this->t_oldcol = absences_translate("Old collection");
			$this->t_newcol = absences_translate("New collection");
			$this->t_record = absences_translate("Record");
			$this->t_quantity = absences_translate("Quantity");
			$this->t_nbdays = absences_translate("Day(s)");
			$this->t_right = absences_translate("Rights");
			$this->t_balance = absences_translate("Balance");
			$this->t_balance_title = absences_translate("User remaining quantity (waiting request ignored)");
			$this->t_new_balance_title = absences_translate("User remaining quantity on new right, the new user rights will equal to balance+consumed (waiting request ignored)");
			$this->t_confirmed = absences_translate("Consumed");
			$this->t_waiting = absences_translate("Waiting");
			$this->t_right_user_title = absences_translate("Default quantity in the vacation right and specific user quantity (tooltip on bold)");
			$this->t_right_title = absences_translate("Default quantity in the vacation right");
			$this->t_user_modified = absences_translate("A specific user quantity exists");
			$this->t_renewal = absences_translate('Renewal');
			$this->t_renewal_title = absences_translate('Include this right in the next yearly renewal');

			global $babDB;
			$this->tg = bab_rp('tg');
			$this->ide = bab_rp('ide', false);

			$agent = absences_Agent::getFromIdUser($id_user);
			$old_rights = $agent->getAgentRightManagerIterator();

			$this->id_user = $id_user;
			$this->id_coll = $newcol;
			
			$newCollection = absences_Collection::getById($newcol);

			$oldCollection = $agent->getCollection();
			$this->oldcol = '';
			
			if (isset($oldCollection) && $oldCollection->getRow()) {
			    $this->oldcol = bab_toHtml($oldCollection->name);
			}
			
			$this->newcol = bab_toHtml($newCollection->name);

			
			$req = "SELECT cr.id_right _id_right, ur.* FROM
				
			absences_coll_rights cr
			    LEFT JOIN absences_users_rights ur ON ur.id_right = cr.id_right
			    AND ur.id_user = ".$babDB->quote($id_user)."
			WHERE
			    cr.id_coll='".$babDB->db_escape_string($newcol)."'
			";
				
			$res = $babDB->db_query($req);
				
			$new_rights = array();
			while ($arr = $babDB->db_fetch_assoc($res))
			{
					
				if (isset($arr['id']))
				{
					$row = $arr;
					unset($row['_id_right']);
					$collRight = new absences_AgentRight;
					$collRight->setRow($arr);
				} else {
					$collRight = new absences_Right($arr['_id_right']);
						
					if (!$collRight->getRow())
					{
						bab_debug(sprintf('Failed to load right for id_right=%d', $arr['_id_right']), DBG_ERROR);
						continue;
					}
				}
					
				$new_rights[] = $collRight;
			}
				
			$this->totaldays = 0;

			$this->rights = array();

			foreach ($old_rights as $agentRight)
			{
				$right = $agentRight->getRight();

				if (!$right->getRow()) {
					bab_debug(sprintf('Failed to load right for agentRight %d, id_right=%d', $agentRight->id, $agentRight->id_right), DBG_ERROR);
					continue;
				}
				
				// ignorer les droits avec un solde a zero et qui sont 
				
				if (!$right->isAccessibleByValidityPeriod() && 0 === (int) round(100 * $agentRight->getAvailableQuantity())) {
				    continue;
				}
				

				$this->rights[$right->id] = array(
						'quantity_unit'			=> $right->quantity_unit,
						'description' 			=> $right->description,
						'quantity_old' 			=> $right->quantity,
						'user_quantity_old' 	=> $agentRight->quantity,
						'quantity_available' 	=> $agentRight->getAvailableQuantity(),
						'confirmed'				=> (float) $agentRight->getConfirmedQuantity(),
						'waiting'				=> (float) $agentRight->getWaitingQuantity()
				);
			}

			foreach ($new_rights as $collRight)
			{
					
					
				if ($collRight instanceof absences_AgentRight)
				{
					$right = $collRight->getRight();
					$agentRight = $collRight;
					$user_quantity_new = $agentRight->quantity;
					$quantity_available = $agentRight->getAvailableQuantity();
					$confirmed = (float) $agentRight->getConfirmedQuantity();
					$waiting = (float) $agentRight->getWaitingQuantity();
						
				} else {
					$right = $collRight;
						
					$user_quantity_new = $right->quantity;
					$quantity_available = null;
					$confirmed = null;
					$waiting = null;
				}

				if (!$right->getRow())
				{
					bab_debug(sprintf('Failed to load right for agentRight %d, id_right=%d', $agentRight->id, $agentRight->id_right), DBG_ERROR);
					continue;
				}
					
				if (!isset($this->rights[$right->id]))
				{

						
						
					$this->rights[$right->id] = array(
							'quantity_unit'			=> $right->quantity_unit,
							'description' 			=> $right->description,
							'quantity_new' 			=> $right->quantity,
							'user_quantity_new' 	=> $user_quantity_new,
							'quantity_available'	=> $quantity_available,
							'confirmed'				=> $confirmed,
							'waiting'				=> $waiting
					);

				}
				else
				{
					$this->rights[$right->id]['quantity_new'] = $right->quantity;
					$this->rights[$right->id]['confirmed'] = $confirmed;
					$this->rights[$right->id]['waiting'] = $waiting;
				}
			}
		}
			
			
		private function createQuantityProperty($right, $name)
		{
				
				
			if (!isset($right[$name]) || '' === $right[$name]) {
				$this->right[$name] = '';
			} else {
				$this->right[$name] = bab_toHtml(absences_quantity($right[$name], $right['quantity_unit']));
			}
		}

		function getnext()
		{
			if (list($this->id,$right) = each($this->rights))
			{
				$this->altbg = !$this->altbg;
				$default = (isset($right['quantity_new']) && $right['quantity_available'] > $right['quantity_new']) || !is_numeric($right['quantity_available']) ? $right['quantity_new'] : $right['quantity_available'];

				$newrightvalue = isset($_POST['right_'.$this->id]) ? $_POST['right_'.$this->id] : $default;
				$this->newrightvalue = bab_toHtml(absences_editQuantity($newrightvalue, $right['quantity_unit']));

				$this->createQuantityProperty($right, 'quantity_new');
				$this->createQuantityProperty($right, 'quantity_old');

				$this->createQuantityProperty($right, 'user_quantity_new');
				$this->createQuantityProperty($right, 'user_quantity_old');

				$this->createQuantityProperty($right, 'confirmed');
				$this->createQuantityProperty($right, 'waiting');

				$this->createQuantityProperty($right, 'quantity_available');


				$this->right['description'] = bab_toHtml($right['description']);
				//$this->right['quantity_available'] = bab_toHtml(absences_quantity($right['quantity_available'], $right['quantity_unit']));

                $this->renewal = isset($right['quantity_new']);
                
				return true;
			}
			else
			{
				return false;
			}
		}
			

			
	}


	$tempa = new tempa($id_user,$newcol);
	$babBody->babecho(absences_addon()->printTemplate($tempa, "vacadm.html", "changeucol"));

}


function absences_getWfName($id_approb)
{
	require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
	if (!$id_approb)
	{
		return absences_translate('None');
	}


	$arr = bab_WFGetApprobationInfos($id_approb);

	return $arr['name'];
}








/**
 * Set parameter for agent (create or modify)
 * if the agent does not exists, il will be created
 *
 * @param	int		$iduser
 * @param	int		$id_collection
 * @param	int		$idsa				Approval scheme
 * @param	int		$id_sa_cet			Optionnal approval scheme for time saving account
 * @param	int		$id_sa_recover		Optionnal approval scheme for work period recovery
 *
 * @throws Exception
 *
 * @return bool
 */
function absences_setAgentInfos($iduser, $id_collection, $idsa, $id_sa_cet = 0, $id_sa_recover = 0, $emails = null, $id_organization = null)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';

	if( empty($iduser) )
	{
		throw new Exception(absences_translate("You must specify a user"));
	}

	if( empty($idsa) )
	{
		throw new Exception(absences_translate("You must specify approbation schema"));
	}

	if( !empty($iduser))
	{
		$agent = absences_Agent::getFromIdUser($iduser);
		if( $agent->exists() )
		{

			$query = "UPDATE ".ABSENCES_PERSONNEL_TBL." SET
			
			id_sa='".$babDB->db_escape_string($idsa)."',
			id_sa_cet=".$babDB->quote($id_sa_cet).",
			id_sa_recover=".$babDB->quote($id_sa_recover)."";
			
			if (null !== $id_collection) {
			    $query .= ", id_coll=".$babDB->quote($id_collection);
			}
				
			if (null !== $emails) {
				$query .= ", emails=".$babDB->quote($emails);
			}
			
			if (null !== $id_organization) {
			    $query .= ", id_organization=".$babDB->quote($id_organization);
			}
				
				
			$babDB->db_query($query." WHERE id_user='".$babDB->db_escape_string($iduser)."'");

			if( $agent->id_sa != $idsa )
			{
				$old = bab_WFGetApprobationInfos($agent->id_sa);
				$new = bab_WFGetApprobationInfos($idsa);
				$agent->addMovement(sprintf(absences_translate('The approbation shema has been modified from %s to %s'), $old['name'], $new['name']));
			}

			if( $agent->id_sa_cet != $id_sa_cet )
			{
				$agent->addMovement(sprintf(absences_translate('The approbation shema for time saving account has been modified from %s to %s'),
						absences_getWfName($agent->id_sa_cet), absences_getWfName($id_sa_cet)));
			}

			if( $agent->id_sa_recover != $id_sa_recover )
			{
				$agent->addMovement(sprintf(absences_translate('The approbation shema for work period recovery has been modified from %s to %s'),
						absences_getWfName($agent->id_sa_recover), absences_getWfName($id_sa_recover)));
			}


		}
		else
		{
		    if( empty($id_collection) )
		    {
		        throw new Exception(absences_translate("You must specify a vacation collection"));
		    }
		    
		    
			$babDB->db_query("INSERT INTO ".ABSENCES_PERSONNEL_TBL."
					(
					id_user,
					id_coll,
					id_sa,
					id_sa_cet,
					id_sa_recover,
					emails,
			        id_organization
			) VALUES (
					'".$babDB->db_escape_string($iduser)."',
					".$babDB->quote($id_collection).",
					'".$babDB->db_escape_string((string) $idsa)."',
					'".$babDB->db_escape_string((string) $id_sa_cet)."',
					'".$babDB->db_escape_string((string) $id_sa_recover)."',
					".$babDB->quote((string) $emails).",
			        ".$babDB->quote((string) $id_organization)."
			)");

			$agent->addMovement(absences_translate('New personnel member created'));
		}
	}


	return true;
}



/**
 * Set rights associations according to collection
 *
 * @param	int		$id_user
 * @param	int		$id_collection
 *
 *
 */
function absences_setCollectionRights($id_user, $id_collection)
{
	require_once dirname(__FILE__).'/vacfixedincl.php';
	global $babDB;

	// create default user rights

	$babDB->db_query("DELETE FROM ".ABSENCES_USERS_RIGHTS_TBL." WHERE id_user='".$babDB->db_escape_string($id_user)."'");

	$res = $babDB->db_query("SELECT
			cr.id_right
			FROM
			absences_coll_rights cr
			WHERE
			cr.id_coll='".$babDB->db_escape_string($id_collection)."'
			");

	while($r = $babDB->db_fetch_array($res))
	{
		$babDB->db_query("INSERT INTO ".ABSENCES_USERS_RIGHTS_TBL." ( id_user,  id_right ) VALUES ('".$babDB->db_escape_string($id_user)."','".$babDB->db_escape_string($r['id_right'])."')");
	}


	// update fixed vacation right
	$messages = '';
	absences_updateFixedRightsOnUser($id_user, $messages);
}




/**
 * mise a jour d'un utilisateur, utiliser par le gestionnaire et le gestionnaire delegue
 * return false : retour au formulaire de modification de l'agent
 * return true : continuer vers l'ecran de modification du regime
 * return null : retour a la liste des agents
 * 
 * 
 * @return bool | null
 */
function absences_updateVacationPersonnel($id_user)
{
	global $babBody;
    require_once dirname(__FILE__).'/agent.class.php';
    
    $agent = absences_Agent::getFromIdUser($id_user);

	try {
	    // T8246: on ne modifie pas le regime, l'utilisateur est redirige vers la page du changement de regime
		absences_setAgentInfos($id_user, null, bab_rp('idsa'), bab_rp('id_sa_cet'), bab_rp('id_sa_recover'), bab_rp('emails'), bab_rp('id_organization'));

	} catch (Exception $e)
	{
		$babBody->addError($e->getMessage());
		return false;
	}
	
	
	// mise a jour des profiles de rythmes de travail
	
	if (isset($_POST['profiles'])) {
	    require_once dirname(__FILE__).'/workschedules.php';
	    
	    try {
	       absences_setProfiles($id_user, bab_pp('profiles'));
	    } catch (bab_SaveErrorException $e) {
	        $babBody->addError($e->getMessage());
	        return false;
	    }
	}
	
	
	
	if ((int) $agent->id_coll === (int) bab_rp('idcol')) {
	    // le regime n'a pas ete modifie, rediriger vers la liste des agents
	    
	    return null;
	}

	return true; // vers la modification du regime;
}





function absences_saveVacationPersonnel($userid,  $idcol, $idsa, $id_sa_cet, $id_sa_recover, $emails, &$messages)
{
	global $babBody;
	absences_setAgentInfos($userid,  $idcol, $idsa, $id_sa_cet, $id_sa_recover, $emails);
	absences_setCollectionRights($userid, $idcol);


	// update user menu
	bab_siteMap::clear($userid);

	return true;
}


class absences_notifyOnVacationChangeCls
{

	public function __construct($quantity, $date_begin,  $date_end, $msg)
	{
		$this->message = $msg;
		$this->from = absences_translate("from");
		$this->until = absences_translate("until");
		$this->quantitytxt = absences_translate("Quantity");
		$this->begindate = bab_strftime(bab_mktime($date_begin));
		$this->enddate = bab_strftime(bab_mktime($date_end));
		$this->quantity = $quantity;
	}
}


/**
 * Notify on vacation change
 * @param int		$idusers
 * @param int		$quantity
 * @param string	$date_begin		0000-00-00 00:00:00
 * @param string	$date_end		0000-00-00 00:00:00
 * @param string	$what
 * 
 * @return bool
 */
function absences_notifyOnVacationChange($idusers, $quantity, $date_begin, $date_end, $what)
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL;
	
	if (empty($quantity) || empty($date_begin)) {
	    return false;
	}


	$cntusers = count($idusers);

	if( $cntusers > 0 )
	{
		$mail = bab_mail();
		if( $mail == false )
			return false;

		$mail->mailFrom($BAB_SESS_EMAIL, $BAB_SESS_USER);
		switch($what)
		{
			case ABSENCES_FIX_UPDATE: $msg = absences_translate("Vacation has been updated");	break;
			case ABSENCES_FIX_DELETE: $msg = absences_translate("Vacation has been deleted");	break;
			default: $msg = absences_translate("Vacation has been scheduled");	break;
		}

		$mail->mailSubject($msg);

		$tempb = new absences_notifyOnVacationChangeCls($quantity, $date_begin, $date_end, $msg);
		$message = $mail->mailTemplate(absences_addon()->printTemplate($tempb, "mailinfo.html", "newfixvacation"));
		$mail->mailBody($message, "html");

		$message = absences_addon()->printTemplate($tempb, "mailinfo.html", "newfixvacationtxt");
		$mail->mailAltBody($message);

		for( $i=0; $i < $cntusers; $i++)
		{
			$mail->clearTo();
			$mail->mailTo(bab_getUserEmail($idusers[$i]), bab_getUserName($idusers[$i]));
			$mail->send();
		}
	}
	
	return true;
}


function absences_isPlanningAccessValid()
{
    if (bab_isAccessValid('absences_public_planning_groups', 1)) {
        return true;
    }
    
    if (bab_getUserIdObjects('absences_custom_planning_groups')) {
        return true;
    }
    
	global $babDB;
	$id_oc = absences_getVacationOption('id_chart');
	
	$res = $babDB->db_query("SELECT p.id_user 
	     FROM ".ABSENCES_PLANNING_TBL." p, bab_oc_entities e 
	        WHERE p.id_user=".$babDB->quote($GLOBALS['BAB_SESS_USERID'])." 
	        AND p.id_entity=e.id AND e.id_oc=".$babDB->quote($id_oc));
	return  ($babDB->db_num_rows($res) > 0);
}


/**
 *
 * @param string $field
 */
function absences_getVacationOption($field) {
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';

	static $arr = null;

	if (null == $arr) {
		$res = $babDB->db_query("SELECT * FROM ".ABSENCES_OPTIONS_TBL);
		if (0 < $babDB->db_num_rows($res)) {
			$arr = $babDB->db_fetch_assoc($res);
		} else {
			$arr = array(

					'chart_superiors_create_request' 	=> 0,
					'chart_superiors_set_rights'		=> 0,
					'allow_mismatch' 					=> 0,
					'workperiod_recover_request'		=> 0,
					'display_personal_history'			=> 0,
			        'modify_confirmed'       			=> 0,
			        'modify_waiting'       			    => 1,
					'email_manager_ondelete'			=> 1,
					'approb_email_defer'				=> 0,
					'entity_planning'					=> 0,
			        'entity_planning_display_types'		=> 0,
					'approb_alert'						=> 7,
			        'auto_approval'                     => 1,
					'auto_confirm'						=> 0,
					'sync_server'						=> 0,
					'sync_url'							=> '',
					'sync_nickname'						=> '',
					'sync_password'						=> '',
					'id_chart'							=> bab_OCgetPrimaryOcId(),
					'user_add_email'					=> 0,
					'end_recup'							=> 365,
					'delay_recovery'					=> 0,
					'maintenance'						=> 0,
					'archivage_day'						=> 1,
					'archivage_month'					=> 1,
				//	'public_calendar'					=> 0,
					'appliquant_email'					=> 1,
					'organization_sync'					=> 0
			);
		}
	}

	return $arr[$field];
}






/**
 * Set vacation events into object
 *
 * @todo process queries with null dates
 *
 * @param bab_UserPeriods				$user_periods			query result set
 * @param array							$id_users
 */
function absences_setVacationPeriods(bab_UserPeriods $user_periods, $id_users) {
	global $babDB;

	require_once $GLOBALS['babInstallPath'].'utilit/nwdaysincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	require_once dirname(__FILE__).'/entry.class.php';

	$begin = $user_periods->begin;
	$end = $user_periods->end;



	$query = "
    	SELECT 
    	   e.*, 
    	   c.calendar_backend 
    	from
    	   ".ABSENCES_ENTRIES_TBL." e 
    	       LEFT JOIN ".BAB_CAL_USER_OPTIONS_TBL." c ON e.id_user=c.id_user
    	WHERE
    	   e.id_user IN(".$babDB->quote($id_users).") 
    	   AND e.status!='N'
	";

	if (null !== $begin)
	{
		$query .= " AND date_end > ".$babDB->quote($begin->getIsoDateTime())." ";
	}

	if (null !== $end)
	{
		$query .= " AND date_begin < ".$babDB->quote($end->getIsoDateTime())." ";
	}

	$res = $babDB->db_query($query);

	// find begin and end

	$date_end = null;
	$date_begin = null;

	while ($row = $babDB->db_fetch_assoc($res))
	{
		if (null === $date_end || $row['date_end'] > $date_end)
		{
			$date_end = $row['date_end'];
		}

		if (null === $date_begin || $row['date_begin'] < $date_begin)
		{
			$date_begin = $row['date_begin'];
		}
	}

	if ($babDB->db_num_rows($res))
	{
		$babDB->db_data_seek($res, 0);
	}


	$begin 	= BAB_DateTime::fromIsoDateTime($date_begin);
	$end	= BAB_DateTime::fromIsoDateTime($date_end);

	$collections = array();

	while( $row = $babDB->db_fetch_assoc($res))
	{
		$backend = bab_functionality::get('CalendarBackend/'.$row['calendar_backend']);

		if (!$backend)
		{
			continue;
		}


		if (!isset($collections[$row['id_user']]))
		{
			$id_user = (int) $row['id_user'];
			$calendar = $backend->Personalcalendar($id_user);

			if ($calendar)
			{
				$collections[$row['id_user']] = $backend->VacationPeriodCollection($calendar);
			} else {
				$collections[$row['id_user']] = null;
			}

		}


		if (isset($collections[$row['id_user']]))
		{
			$entry = new absences_Entry();
			$entry->setRow($row);
				
			$p = new bab_CalendarPeriod;
			absences_setPeriodProperties($p, $entry);
			$collections[$row['id_user']]->addPeriod($p);
			$p->setProperty('UID', 'VAC'.$row['id']);
			$user_periods->addPeriod($p);
		}

	}
}





/**
 * Search for a vacation request in ovidentia database and update the corresponding calendar period if the period is found using the user calendar backend
 * @param int			$id_request
 * @param BAB_DateTime 	$begin			old begin date
 * @param BAB_DateTime	$end			old end date
 * @return unknown_type
 */
function absences_updatePeriod($id_request, BAB_DateTime $begin, BAB_DateTime $end)
{
	require_once dirname(__FILE__).'/entry.class.php';

	$entry = absences_Entry::getById($id_request);
	
	if (!$entry->getRow()) {
	    throw new Exception(sprintf('Absence request with id %s does not exists', $id_request));
	}


	$period = absences_getPeriod($id_request, $entry->id_user, $begin, $end);
	if (null === $period)
	{
		bab_debug('no period found in backend');
		return null;
	}

	absences_setPeriodProperties($period, $entry);

	$period->save();
}


/**
 * Create a new vacation request into calendar backend
 * @param $id_request
 * @return unknown_type
 */
function absences_createPeriod($id_request)
{
	require_once $GLOBALS['babInstallPath'].'utilit/calincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	require_once dirname(__FILE__).'/entry.class.php';

	$entry = absences_Entry::getById($id_request);


	$icalendars = bab_getICalendars($entry->id_user);

	$calendar = $icalendars->getPersonalCalendar();

	if (!$calendar)
	{
		// do not create the vacation period if no personal calendar
		return;
	}

	$backend = $calendar->getBackend();

	if ($backend instanceof Func_CalendarBackend_Ovi)
	{
		// do not create the vacation period if the calendar backend is ovidentia because the calendar api will get the original vacation period
		return;
	}

	$date_begin = BAB_DateTime::fromIsoDateTime($entry->date_begin);
	$date_end	= BAB_DateTime::fromIsoDateTime($entry->date_end);

	$period = $backend->CalendarPeriod($date_begin->getTimeStamp(), $date_end->getTimeStamp());
	$collection = $backend->CalendarEventCollection($calendar);
	$collection->addPeriod($period);

	absences_setPeriodProperties($period, $entry);

	$period->save();
}


/**
 * Gestion des types/couleurs sur le planning
 *
 */
class absences_EntryColors
{

	private $stack = array();


	private $freeHalfDays = array();

	/**
	 *
	 * @var int
	 */
	private $maxHalfDays = 0;


	/**
	 * Push and shift into a stack
	 * @param int		$id_entry
	 * @param string 	$searchkey
	 * @param mixed $push
	 *
	 * $push = array(
	 *		type, color
	 *	)
	 */
	private function typeColorStack($id_entry, $searchkey, $push = false) {

		if (!isset($this->stack[$id_entry][$searchkey])) {
			$this->stack[$id_entry][$searchkey] = array();
		}

		if (false === $push) {
			return array_shift($this->stack[$id_entry][$searchkey]);
		}

		array_push($this->stack[$id_entry][$searchkey], $push);
	}



	/**
	 *
	 * @param int $id_entry
	 * @param int $id_user
	 * @param BAB_DateTime $date_begin		Event start date
	 * @param BAB_DateTime $date_begin		Event end date
	 * @param BAB_DateTime $search_begin	colors start date		/ premier jour du mois
	 *
	 * @return array
	 */
	public function get($id_entry, $id_user, BAB_DateTime $date_begin, BAB_DateTime $date_end, BAB_DateTime $search_begin)
	{
		$searchkey = $search_begin->getIsoDate();

		if (!isset($this->stack[$id_entry][$searchkey])) {
			$this->setMaxHalfDays($date_begin, $date_end);
			$this->getFreeHalfDays($id_user, $date_begin, $date_end);
			$this->set($id_entry, $id_user, $date_begin, $search_begin);
		}

		return $this->typeColorStack($id_entry, $searchkey);
	}



	private function setMaxHalfDays(BAB_DateTime $date_begin, BAB_DateTime $date_end)
	{
		$nbdays = BAB_DateTime::dateDiffIso($date_begin->getIsoDate(), $date_end->getIsoDate());

		$this->maxHalfDays = (2* $nbdays);
	}



	/**
	 * Set colors of entry into stack
	 * @param 	int 			$id_entry
	 * @param 	int 			$id_user
	 * @param 	BAB_DateTime 	$date_begin		Debut de l'evenement
	 * @param	BAB_DateTime	$search_begin	Debut de la zone de recherche, premier jour du mois
	 */
	private function set($id_entry, $id_user, BAB_DateTime $date_begin, BAB_DateTime $search_begin)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/workinghoursincl.php';
		global $babDB;

		$req = "SELECT
        		e.quantity,
        		t.name type,
        		t.color
    		FROM 
		          ".ABSENCES_ENTRIES_ELEM_TBL." e,
    		      ".ABSENCES_RIGHTS_TBL." r,
    		      ".ABSENCES_TYPES_TBL." t
    		WHERE
    		  e.id_entry=".$babDB->quote($id_entry)."
    		  AND r.id=e.id_right
    		  AND t.id=r.id_type

	      ORDER BY e.date_begin";

		$res2 = $babDB->db_query($req);

		$type_day       = $date_begin->cloneDate();
		$type_day_end   = $date_begin->cloneDate();
		$ignore 		= array();


		while ($arr = $babDB->db_fetch_assoc($res2))
		{
			
				
			for($d = 0; $d < $arr['quantity']; $d += 0.5) {

				// si le demi-jour est ferie ou non travaille , repousser la zone de couleur d'un demi-jour
				while (!$this->isFree($type_day_end) && count($ignore) < $this->maxHalfDays) {
					
					$key = date('Ymda', $type_day_end->getTimeStamp());
					
					$ignore[$key] = 1;
					$type_day_end->add(12, BAB_DATETIME_HOUR);
				}

				$type_day_end->add(12, BAB_DATETIME_HOUR);

			}
			

			while ($type_day->getTimeStamp() < $type_day_end->getTimeStamp() ) {
				
				$key = date('Ymda', $type_day->getTimeStamp());

				if ($type_day->getTimeStamp() >= $date_begin->getTimeStamp() 
						&& $search_begin->getTimeStamp() <= $type_day->getTimeStamp() 
						&& !isset($ignore[$key])) {

					$this->typeColorStack(
							$id_entry,
							$search_begin->getIsoDate(),
							array(
									'id_type'       => $arr['type'],
									'color'         => $arr['color']
							)
					);
				}

				$type_day->add(12, BAB_DATETIME_HOUR);
			}
		}

	}

	/**
	 * Indexer les jours travailles par demie-journee sur la periode de la demande
	 *
	 * @param int			$id_user
	 * @param BAB_DateTime 	$date_begin		debut demande
	 * @param BAB_DateTime	$date_begin		fin demande
	 */
	private function getFreeHalfDays($id_user, BAB_DateTime $date_begin, BAB_DateTime $date_end)
	{

		$arr = absences_getHalfDaysIndex($id_user, $date_begin, $date_end, true);

		$this->freeHalfDays = $arr[2];
	}

	
	/**
	 * Test if half day is free
	 * @param BAB_DateTime $date
	 * @return bool
	 */
	private function isFree(BAB_DateTime $date)
	{
		$key = date('Ymda', $date->getTimeStamp());

		return isset($this->freeHalfDays[$key]);
	}
}



/**
 * Update the period properties with vacation entry informations
 *
 * @param 	bab_CalendarPeriod	$p
 * @param 	absences_Entry		$entry
 *
 *
 * @return unknown_type
 */
function absences_setPeriodProperties(bab_CalendarPeriod $p, absences_Entry $entry)
{
	global $babDB;

	$date_begin = BAB_DateTime::fromIsoDateTime($entry->date_begin);
	$date_end	= BAB_DateTime::fromIsoDateTime($entry->date_end);
	$p->setDates($date_begin, $date_end);


	list(, $category, $color) = $babDB->db_fetch_row($babDB->db_query("

			SELECT
			cat.id,
			cat.name,
			cat.bgcolor
			FROM
			".ABSENCES_COLLECTIONS_TBL." vct,
			".ABSENCES_PERSONNEL_TBL." vpt,
			".ABSENCES_ENTRIES_TBL." vet,
			".BAB_CAL_CATEGORIES_TBL." cat
			WHERE
			vpt.id_coll=vct.id
			AND vet.id_user=vpt.id_user
			AND vet.id=".$babDB->quote($entry->id)."
			AND cat.id = vct.id_cat
			"));


	if (!$entry->isPrevisonal())
	{
	    if ('' === $entry->status) {
	        $p->setProperty('SUMMARY'		, absences_translate("Waiting vacation"));
	    } else {
		  $p->setProperty('SUMMARY'			, absences_translate("Vacation"));
	    }
	} else {
		$p->setProperty('SUMMARY'			, absences_translate("Previsonal vacation"));
	}
	$p->setProperty('CATEGORIES'		, $category);
	$p->setProperty('X-CTO-COLOR'		, $color);
	$p->setProperty('X-CTO-VACATION'	, $entry->id);

	if ($entry->comment)
	{
		$p->setProperty('COMMENT'		, $entry->comment);
	}

	$description = '';
	$descriptiontxt = '';

	if ('' === $entry->status) {
		$description .= '<p>'.absences_translate("Waiting to be validated").'</p>';
		$descriptiontxt .= absences_translate("Waiting to be validated")."\n";
	}



	$ventilation = $entry->getElementsIterator();

	$label = (1 === $ventilation->count()) ? absences_translate('Vacations type') : absences_translate('Vacations types');

	$description .= '<table class="bab_cal_vacation_types" cellspacing="0">';
	$description .= '<thead><tr><td colspan="3">'.bab_toHtml($label).'</td></tr></thead>';
	$description .= '<tbody>';




	foreach($ventilation as $element) {

		/*@var $element absences_EntryElem */

		$days = rtrim($element->quantity,'0.');
		$right = $element->getRight();
		$type = $right->getType();

		$description .= sprintf(
				'<tr><td style="background:#%s">&nbsp; &nbsp;</td><td>%s</td><td>%s</td></tr>',
				$type->color,
				$days,
				$type->name
		);
			
		$descriptiontxt .= $days.' '.$type->name."\n";
	}
	$description .= '</tbody></table>';

	$data = array(
			'id' => $entry->id,
			'description' => $description,
			'description_format' => 'html',
			'id_user' => $entry->id_user,
			'date_begin' => $entry->date_begin, // dans le planning les periodes sont decoupees par tranches de 12H, on utilise ces dates pour retrouver la periode initiale de la demande
			'date_end' => $entry->date_end
	);

	$p->setData($data);

	$p->setProperty('DESCRIPTION', $descriptiontxt);

	$p->setProperty('TRANSP','OPAQUE');
}








/**
 * Clear calendar data
 * On non-working days changes by admin
 * On working hours changes by admin
 */
function absences_clearCalendars() {
	global $babDB;
	$babDB->db_query("DELETE FROM ".ABSENCES_CALENDAR_TBL."");
}


/**
 * Clear calendar data for user
 */
function absences_clearUserCalendar($id_user = NULL) {
	if (NULL === $id_user) {
		$id_user = $GLOBALS['BAB_SESS_USERID'];
	}
	global $babDB;
	$babDB->db_query("DELETE FROM ".ABSENCES_CALENDAR_TBL." WHERE id_user=".$babDB->quote($id_user));
}






/**
 * si type2 est prioritaire, return true
 */
function absences_compare($type1, $type2, $vacation_is_free) {

	if ($vacation_is_free) {

		$order = array(
				'bab_VacationPeriodCollection'			=> 1,
				'bab_NonWorkingPeriodCollection'		=> 2,
				'bab_WorkingPeriodCollection' 			=> 3,
				'bab_NonWorkingDaysCollection'			=> 6
		);

	} else {

		$order = array(

				'bab_NonWorkingPeriodCollection'		=> 1,
				'bab_WorkingPeriodCollection'			=> 2,
				'bab_VacationPeriodCollection'			=> 5,
				'bab_NonWorkingDaysCollection'			=> 6
		);

	}


	if (!isset($order[$type1]))
	{
		throw new Exception(sprintf('The vacation calendar request has received the collection %s from a calendar backend, the backends must not return events of non requested collections', $type1));
	}

	if (!isset($order[$type2]))
	{
		throw new Exception(sprintf('The vacation calendar request has received the collection %s from a calendar backend, the backends must not return events of non requested collections', $type2));
	}


	if ($order[$type2] > $order[$type1]) {
		return true;
	}

	return false;
}

function absences_is_free($collection) {


	switch(true) {
		case $collection instanceof bab_WorkingPeriodCollection:
			return true;

		case $collection instanceof bab_NonWorkingPeriodCollection:
		case $collection instanceof bab_VacationPeriodCollection:
		case $collection instanceof bab_NonWorkingDaysCollection:
			return false;
	}
}







/**
 * Return arrays with periods for each half-day beetween two dates
 *
 * @param	int				$id_user
 * @param	BAB_dateTime	$dateb
 * @param	BAB_dateTime	$datee
 * @param	boolean			$vacation_is_free
 * @return array
 * 				0 : working periods for each half-day
 * 				1 : only the main period for each half day
 * 				2 : free-busy status for each half day
 * 				3 : periods indexed by type
 *
 */
function absences_getHalfDaysIndex($id_user, BAB_DateTime $dateb, BAB_DateTime $datee, $vacation_is_free = false) {

	include_once $GLOBALS['babInstallPath']."utilit/utilit.php";
	include_once $GLOBALS['babInstallPath']."utilit/workinghoursincl.php";
	include_once $GLOBALS['babInstallPath']."utilit/calincl.php";

	$obj = new bab_UserPeriods(
			$dateb,
			$datee
	);

	$factory = bab_getInstance('bab_PeriodCriteriaFactory');
	/* @var $factory bab_PeriodCriteriaFactory */

	$criteria = $factory->Collection(
			array(
					'bab_NonWorkingDaysCollection',
					'bab_NonWorkingPeriodCollection',
					'bab_WorkingPeriodCollection',
					'bab_VacationPeriodCollection'
			)
	);

	$icalendars = bab_getICalendars($id_user);

	$calendar = $icalendars->getPersonalCalendar();


	if (!isset($calendar))
	{
		// the user personal calendar is not accessible
		// create an instance only for vacations

		$calendar = bab_functionality::get('CalendarBackend')->PersonalCalendar($id_user);
	}


	$criteria = $criteria->_AND_($factory->Calendar($calendar));

	$obj->createPeriods($criteria);
	$obj->orderBoundaries();

	// working periods for each half-day
	$index_working = array();

	// only the main period for each half day
	$index_reduced = array();

	// free-busy status for each half day
	$is_free = array();

	// periods indexed by type
	$stack = array();

	foreach($obj as $pe) {

		/*@var $pe bab_CalendarPeriod */

		// bab_debug($pe->toHtml(), DBG_TRACE, $dateb->getIsoDateTime().' '.$datee->getIsoDateTime());

		$group = $pe->split(12 * 3600);
		foreach($group as $p) {
				
			/*@var $p bab_CalendarPeriod */
			if ($p->ts_begin < $datee->getTimeStamp() && $p->ts_end > $dateb->getTimeStamp()) {
				$key = date('Ymda',$p->ts_begin);
				$collection = $p->getCollection();
				$type = get_class($collection);

				$stack[$key][$type] = $p;

				if (!isset($index_reduced[$key]) || absences_compare(get_class($index_reduced[$key]->getCollection()), $type, $vacation_is_free)) {
						
					// overwrite reduced index if absences_compare return true
						
					$index_reduced[$key] = $p;
						
					// and reset the free-busy status with the new period

					if (absences_is_free($collection)) {
						$is_free[$key] = 1;
					} elseif (isset($is_free[$key])) {
						unset($is_free[$key]);
					}
				}


				if ($p->getCollection() instanceof bab_WorkingPeriodCollection)
				{
					if (!isset($index_working[$key]))
					{
						$index_working[$key] = array();
					}
						
					$index_working[$key][] = $p;
				}

				// ajust period according to selection

				if ($p->ts_begin < $dateb->getTimeStamp())
				{
					$p->setBeginDate($dateb);
				}

				if ($p->ts_end > $datee->getTimeStamp())
				{
					$p->setEndDate($datee);
				}
			}
		}
	}


	return array($index_working, $index_reduced, $is_free, $stack);
}




function absences_group_insert($query, $exec = false) {
	static $values = array();
	if ($query) {
		$values[] = $query;
	}

	if (300 <= count($values) || (0 < count($values) && $exec)) {

		$GLOBALS['babDB']->db_query("
				INSERT INTO ".ABSENCES_CALENDAR_TBL."
				(id_user, monthkey, cal_date, ampm, period_type, id_entry, color, title)
				VALUES
				".implode(',',$values)."
				");
		$values = array();
	}
}







/**
 * Update planning for the given user
 * and the given period
 * @param int		$id_user
 * @param int		$year
 * @param int		$month
 */
function absences_updateCalendar($id_user, $year, $month) {

	global $babDB;
	include_once $GLOBALS['babInstallPath']."utilit/workinghoursincl.php";
	include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";


	$babDB->db_query("DELETE FROM ".ABSENCES_CALENDAR_TBL." WHERE monthkey=".$babDB->quote($month.$year).' AND id_user='.$babDB->quote($id_user));

	$dateb = new BAB_DateTime($year, $month, 1);
	$datee = $dateb->cloneDate();
	$datee->add(1, BAB_DATETIME_MONTH);

	list(, $index_reduced, , $stack) = absences_getHalfDaysIndex($id_user, $dateb, $datee);
	$previous = NULL;

	foreach($index_reduced as $key => $p) {

		$title		= $p->getProperty('SUMMARY');
		$ampm		= 'pm' === date('a',$p->ts_begin) ? 1 : 0;
		$data		= $p->getData();
		$id_entry	= 0;
		$color		= '';

		$collection = $p->getCollection();

		switch(true) {
			case $collection instanceof bab_WorkingPeriodCollection:
				$type = BAB_PERIOD_WORKING;
				break;

			case $collection instanceof bab_NonWorkingPeriodCollection:
				$type = BAB_PERIOD_NONWORKING;
				break;

			case $collection instanceof bab_VacationPeriodCollection:
				$type = BAB_PERIOD_VACATION;
				break;

			case $collection instanceof bab_NonWorkingDaysCollection:
				$type = BAB_PERIOD_NWDAY;
				if (version_compare(bab_getDbVersion(), '8.1.0', '<'))
				{
					$title = $p->getProperty('DESCRIPTION');
				}
				break;
					
		}




		if ($p->getCollection() instanceof bab_VacationPeriodCollection) {
			if (isset($stack[$key]['bab_WorkingPeriodCollection'])) {
				$id_entry = $data['id'];

				$C = bab_getInstance('absences_EntryColors');
				/*@var $C absences_EntryColors */

				$arr = $C->get($id_entry, $id_user, BAB_DateTime::fromIsoDateTime($data['date_begin']), BAB_DateTime::fromIsoDateTime($data['date_end']), $dateb);
				$color = $arr['color'];
			} else {
				$type = BAB_PERIOD_NONWORKING;
			}
		}


		$key = $id_user.$month.$year.$id_entry.$color.$type;

		if ($key !== $previous) {

			$previous = $key;
			absences_group_insert("(
					".$babDB->quote($id_user).",
					".$babDB->quote($month.$year).",
					".$babDB->quote(date('Y-m-d',$p->ts_begin)).",
					".$babDB->quote($ampm).",
					".$babDB->quote($type).",
					".$babDB->quote($id_entry).",
					".$babDB->quote($color).",
					".$babDB->quote($title)."
			)");

		}
	}

	absences_group_insert('',true);
}



/**
 * Date printout for periods
 * @param int $timestamp
 * @return string
 */
function absences_longDate($timestamp) {
	if (empty($timestamp)) {
		return '';
	}

	return bab_longDate($timestamp, true);
}

function absences_shortDate($timestamp) {
	if (empty($timestamp)) {
		return '';
	}

	return bab_shortDate($timestamp, true);
}


class absences_RequestDelete
{
    public static function direct(absences_Request $request, $folder)
    {
        if ($folder)
        {
            $allDeleted = true;
            $I = $request->getFolderEntriesIterator();
            absences_notifyOnRequestChange($I, $request->id_user, true);
        
            foreach($I as $folder_entry)
            {
                /*@var $folder_entry absences_Entry */
                absences_notifyManagers::send($folder_entry->id, true);
                if (!$folder_entry->delete()) {
                    $allDeleted = false;
                }
            }
        
        
        
        
        } else {
        
            absences_notifyManagers::send($request->id, true);
            absences_notifyOnRequestChange(array($request), $request->id_user, true);
            $allDeleted = $request->delete();
        }
        
        return $allDeleted;
    }
    
    public static function withApproval(absences_Request $request, $folder)
    {
        if ($folder)
        {
            
            $I = $request->getFolderEntriesIterator();
        
            foreach($I as $folder_entry)
            {
                /*@var $folder_entry absences_Entry */
                $folder_entry->todelete = 1;
                $folder_entry->createApprobationInstance();
            }

        
        } else {
            
            $request->todelete = 1;
            $request->createApprobationInstance();
        }
        
        return true;
    }
}


/**
 * Delete vacation request
 * notify user if vacation not elapsed
 * delete approbation instance
 * Update calendar
 * @param int $id_request
 * @param int $folder
 * @param int $rfrom            1 if the delete request is created by a manager from back office (need to be secured)
 * @return bool
 */
function absences_delete_request($id_request, $folder = 0, $rfrom = 0)
{
	require_once dirname(__FILE__).'/entry.class.php';
	
	$agent = absences_Agent::getCurrentUser();
	
	if (!$agent->isManager()) {
	    $rfrom = 0;
	}

	$request = absences_Entry::getById($id_request);

	if (!$request->getRow())
	{
		throw new Exception(absences_translate('This request does not exists'));
	}

	if ('Y' === $request->status && 0 === $rfrom) {
	    return absences_RequestDelete::withApproval($request, $folder);
	}

	return absences_RequestDelete::direct($request, $folder);
}



/**
 * Try to get a period from the calendar API from the request
 * The calendar backend can contain a period duplicated into the calendarEventCollection with need to be updated or deleted
 * This function can work without access to the personal calendar of the user
 *
 * @param	int				$id_request
 * @param	int				$id_user		search the period in this user personal calendar
 * @param	BAB_DateTime	$begin			request search begin date	(should be the request begin date)
 * @param	BAB_DateTime	$end			request search end date		(should be the request end date)
 *
 * @return bab_CalendarPeriod | null
 */
function absences_getPeriod($id_request, $id_user, BAB_DateTime $begin, BAB_DateTime $end)
{
	require_once $GLOBALS['babInstallPath'].'utilit/calincl.php';

	$icalendars = bab_getICalendars($id_user);

	$calendar = $icalendars->getPersonalCalendar();

	if (!$calendar)
	{
		return null;
	}

	$backend = $calendar->getBackend();

	$factory = $backend->Criteria();
	$criteria = $factory->Calendar($calendar);
	$criteria = $criteria->_AND_($factory->Collection('bab_CalendarEventCollection'));
	$criteria = $criteria->_AND_($factory->Begin($begin));
	$criteria = $criteria->_AND_($factory->End($end));
	$criteria = $criteria->_AND_($factory->Property('X-CTO-VACATION', $id_request));

	$periods = $backend->selectPeriods($criteria);

	foreach($periods as $period)
	{
		return $period;
	}

	return null;
}







function absences_deleteVacationRequest($id, $manager_view = false)
{
	global $babBody;
	require_once dirname(__FILE__).'/deleterequest.ui.php';

	$babBody->setTitle(absences_translate("Delete vacation request"));

	$temp = new absences_deleteVacationRequestCls($id, $manager_view);
	$babBody->babecho(absences_addon()->printTemplate($temp, "request.html", "delete_entry"));
}



function absences_deleteCetDepositRequest($id, $manager_view = false)
{
	global $babBody;
	require_once dirname(__FILE__).'/deleterequest.ui.php';

	$babBody->setTitle(absences_translate("Delete a time saving account deposit"));

	$temp = new absences_deleteCetDepositRequestCls($id, $manager_view);
	$babBody->babecho(absences_addon()->printTemplate($temp, "request.html", "delete_cetdeposit"));
}



function absences_deleteWpRecoveryRequest($id, $manager_view = false)
{
	global $babBody;
	require_once dirname(__FILE__).'/deleterequest.ui.php';

	$babBody->setTitle(absences_translate("Delete a work period recover request"));

	$temp = new absences_deleteWpRecoveryRequestCls($id, $manager_view);
	$babBody->babecho(absences_addon()->printTemplate($temp, "request.html", "delete_wprecovery"));
}






function absences_addCoManagerEntities(&$entities, $id_user) {
	global $babDB;
	$res = $babDB->db_query("SELECT id_entity FROM ".ABSENCES_COMANAGER_TBL." WHERE id_user=".$babDB->quote($id_user));

	if (0 == $babDB->db_num_rows($res)) {
		return;
	}

	if (!isset($entities['superior'])) {
		$entities['superior'] = array();
	}

	if (!function_exists('absences_is_superior')) {
		function absences_is_superior($entities, $ide) {
			foreach($entities['superior'] as $e) {
				if ($ide == $e['id']) {
					return true;
				}
			}
			return false;
		}
	}

	while ($arr = $babDB->db_fetch_assoc($res)) {
		$e = bab_OCGetEntity($arr['id_entity']);
		$e['id'] = $arr['id_entity'];
		$e['comanager'] = 1;
		if (!absences_is_superior($entities, $arr['id_entity'])) {
			$entities['superior'][] = $e;
		}
	}
}





/**
 * Get the list of available hours for a user
 * if the id_user parameter is null, return hours list based on site parameters
 *
 * @param	int		$id_user
 *
 * @return array
 */
function absences_hoursList($id_user = null)
{
	global $babDB, $babBody;

	$elapstime = null;

	if (null === $id_user)
	{
		$elapstime = (int) $babBody->babsite['elapstime'];
	} else {

		$res = $babDB->db_query("SELECT elapstime FROM bab_cal_user_options WHERE id_user=".$babDB->quote($id_user));
		if ($arr = $babDB->db_fetch_assoc($res))
		{
			$elapstime = (int) $arr['elapstime'];
		}
	}

	if (!isset($elapstime) || $elapstime === 0)
	{
		$elapstime = 5;
	}

	$list = array();
	$min = 0;
	$hour = 0;
	for ($i = 0; $i < 1440; $i += $elapstime)
	{
		$min += $elapstime;

		if ($min >= 60)
		{
			$hour++;
			$min = 0;
		}

		if (24 === $hour)
		{
			break;
		}

		$list[sprintf('%02d:%02d:00', $hour, $min)] = sprintf('%02d:%02d', $hour, $min);
	}

	// add fixed hours

	$list['00:00:00'] = '00:00';
	$list['11:59:59'] = '11:59';
	$list['12:00:00'] = '12:00';
	$list['23:59:59'] = '23:59';

	ksort($list);

	return $list;

}

/**
 * Display a vacation right quantity in a text input
 * @param float	$quantity	numeric
 * @return string
 */
function absences_editQuantity($quantity, $unit = 'H')
{
	switch($unit)
	{
		case 'D':
			$quantity = (string) round($quantity, 2);
			break;

		case 'H':
			$quantity = (string) round($quantity, 2);
			break;
	}

	if (false !== mb_strpos($quantity, '.'))
	{
		$quantity = rtrim($quantity, '0');
	}

	$quantity = rtrim($quantity, '.');

	if ('' === $quantity) {
		$quantity = '0';
	}

	$quantity = str_replace('.', ',', $quantity);

	return $quantity;
}








/**
 * Display a vacation right quantity
 * @param	string|float	$quantity		numeric
 * @param	string			$unit			D | H
 */
function absences_quantity($quantity, $unit)
{


	switch($unit) {
		case 'D':
			$unit = absences_translate('day', 'days', floor($quantity));
			break;
		case 'H':
			$unit = absences_translate('hour', 'hours', floor($quantity));

			// convertire en heures, minutes plustot que d'afficher des heures a virgules

			$minutes = round(($quantity - floor($quantity)) * 60);
			if (0 != $minutes) {
				$quantity = floor($quantity);

				if (0 == $quantity)
				{
					return $minutes.' '.absences_translate('minutes');
				}

				return $quantity.' '.$unit.' '.$minutes.' '.absences_translate('min');
			}

			break;
	}


	if (false !== mb_strpos($quantity, '.'))
	{
		$quantity = rtrim($quantity, '0');
	}

	$quantity = rtrim($quantity, '.');

	if ('' === $quantity) {
		$quantity = '0';
	}

	$quantity = str_replace('.', ',', $quantity);

	return $quantity.' '.$unit;
}




/**
 * Display a period as a string
 * @param string $begin		ISO datetime
 * @param string $end		ISO datetime
 */
function absences_DateTimePeriod($begin, $end, $funcname = 'bab_shortDate')
{
	$begin_day = mb_substr($begin, 0, 10);
	$end_day = mb_substr($end, 0, 10);

	if ($begin_day === $end_day)
	{
		$begin_hour = mb_substr($begin, 11, 5);
		$end_hour = mb_substr($end, 11, 5);

		return sprintf(absences_translate('The %s from %s to %s'), $funcname(bab_mktime($begin), false), $begin_hour, $end_hour);
	}

	return sprintf(absences_translate('From %s to %s'), $funcname(bab_mktime($begin)), $funcname(bab_mktime($end)));
}



/**
 * Get displayable quantity for one vacation entry
 * @param	int		$id_entry
 * @return string
 */
function absences_vacEntryQuantity($id_entry)
{
	global $babDB;

	$res = $babDB->db_query("
			SELECT
			SUM(e.quantity) quantity,
			r.quantity_unit
			FROM
			absences_entries_elem e,
			absences_rights r
			WHERE
			e.id_entry =".$babDB->quote($id_entry)."
			AND r.id = e.id_right
			GROUP BY r.quantity_unit
			ORDER BY r.quantity_unit DESC
			"
	);

	$list = array();
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$list[] = absences_quantity($arr['quantity'], $arr['quantity_unit']);
	}

	return implode(', ', $list);
}




/**
 * get a list of approvers with same requests to approve
 * @param int $notified Filter by notified status, default value output the unnotified requests
 * @return array
 */
function absences_getRequestsApprovers($notified = 0)
{
	require_once $GLOBALS['babInstallPath'].'utilit/wfincl.php';
	require_once dirname(__FILE__).'/request.class.php';


	$waitingRequests = new absences_RequestIterator;
	if (isset($notified)) {
	   $waitingRequests->appr_notified = $notified;
	}
	$waitingRequests->idfai_set = true;

	$by_approver = array();

	foreach($waitingRequests as $request)
	{
		/*@var $request absences_Request  */

		$arr = bab_WFGetWaitingApproversInstance($request->idfai);

		foreach($arr as $id_approver)
		{
			if (!isset($by_approver[$id_approver]))
			{
				$by_approver[$id_approver] = array();
			}
				
			$by_approver[$id_approver][get_class($request).'.'.$request->id] = $request;
		}
	}

	// sort by similar request lists
	$by_email = array();
	foreach($by_approver as $id_approver => $arr)
	{
		ksort($arr);
		$keys = array_keys($arr);
		$mailkey = implode(',', $keys);

		if (!isset($by_email[$mailkey]))
		{
			$by_email[$mailkey] = array(
					'approvers' => array(),
					'requests' => array()
			);
		}
		
		$by_email[$mailkey]['approvers'][$id_approver] = $id_approver;
		$by_email[$mailkey]['requests'] += $arr;
	}

	return $by_email;
}



/**
 * formulaire de recherche pour les liste des demandes gestionnaire
 */
class absences_getRequestSearchForm
{
	public function getForm(Array $statarr = null)
	{
		bab_functionality::includeOriginal('Icons');
		$W = bab_Widgets();
		$form = $W->Form(null, $W->FlowLayout()->setSpacing(1, 'em', 3,'em'));
		$form->setSelfPageHiddenFields()->setReadOnly();

		$form->addClass('absences-filterform');
		$form->addClass('widget-bordered');
		$form->addClass('BabLoginMenuBackground');
		$form->addClass('widget-centered');
		$form->addClass(Func_Icons::ICON_LEFT_16);
		$form->colon();

		$form->setCanvasOptions($form->Options()->width(97,'%'));

		if (isset($statarr)) {
		    
		    $options = array('' => '') + $statarr;
		    
    		$form->addItem(
    			$W->LabelledWidget(
    				absences_translate('Status'),
    				$W->Select()
    				->setOptions($options),
    				'idstatus'
    			)
    		);
		}

		$form->addItem(
			$W->LabelledWidget(
				absences_translate('User'),
				$W->UserPicker(),
				'userid'
			)
		);
		
		
		if ($org = $this->organization()) {
		    $form->addItem($org);
		}

		$form->addItem(
			$W->LabelledWidget(
				absences_translate('Date'),
				$W->PeriodPicker()->setNames('dateb', 'datee')
			)
		);

		$values = array(
			'idstatus'       => $this->param('idstatus'),
			'userid'         => (int) $this->param('userid'),
		    'organization'   => (int) $this->param('organization'),
			'dateb'          => $this->param('dateb'),
			'datee'          => $this->param('datee'),
			'vpos'           => (int) $this->param('vpos', 0)
		);

		$form->setValues($values);
		$form->addItem($W->SubmitButton()->setLabel(absences_translate('Search')));

		return $form;
	}
	
	
	/**
	 * 
	 */
	protected function organization()
	{
	    require_once dirname(__FILE__).'/organization.class.php';
	    
	    $W = bab_Widgets();
	    $select = $W->Select();
	    $select->addOption('', '');
	    
	    $collections = new absences_OrganizationIterator();
	    
	    if (0 === $collections->count()) {
	        return null;
	    }
	    
	    
	    foreach($collections as $collection) {
	        $select->addOption($collection->id, $collection->name);
	    }
	    
	    return $W->LabelledWidget(
            absences_translate('Organization'),
            $select,
            'organization'
	    );
	    
	}
	
	
	
	public function getHtmlForm(Array $statarr = null)
	{
	    $W = bab_Widgets();
	    $form = $this->getForm($statarr);
	    
	    return $form->display($W->HtmlCanvas());
	}


	public function param($name, $default = '')
	{
		if (isset($_REQUEST[$name]))
		{
			$_SESSION['babVacation'][$name] = $_REQUEST[$name];
			return $_REQUEST[$name];
		}

		if (isset($_SESSION['babVacation'][$name]))
		{
			return $_SESSION['babVacation'][$name];
		}

		return $default;
	}
}



/**
 * Compare 2 vacation right quantities
 * Soit un float php soit une chaine de mysql float, pour les saisie utilisateur remplacer , par . avant d'appeller la fonction
 *
 * @param	float | string	$qt1
 * @param	float | string	$qt2
 *
 * @return bool
 */
function absences_cq($qt1, $qt2)
{
    $qt1 = (float) $qt1;
    $qt2 = (float) $qt2;

    $v1 = (int) round(100*$qt1);
    $v2 = (int) round(100*$qt2);

    return ($v1 === $v2);
}
