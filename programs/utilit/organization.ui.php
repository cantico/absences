<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
bab_Widgets()->includePhpClass('Widget_Form');

/**
 * Template class for organization list
 * 
 */
class absences_OrganizationList
{
    public $altbg = true;
    
    private $res;
    
    public function __construct()
    {
        global $babDB;
        
        $this->t_edit = absences_translate('Edit');
        $this->t_name = absences_translate('Name');
        $this->t_agents = absences_translate('Personnel members');
        $sync = (bool) absences_getVacationOption('organization_sync');
        $this->allowedit = !$sync;
        $this->res = $babDB->db_query('SELECT 
            
            o.id, o.name, COUNT(p.id) agents FROM absences_organization o 
                LEFT JOIN absences_personnel p ON p.id_organization=o.id 
            
            GROUP BY o.id 
            ORDER BY o.name');
    }
    
    public function getnext()
    {
        global $babDB;
        
        if ($arr = $babDB->db_fetch_assoc($this->res)) {
            $this->altbg = !$this->altbg;
            $this->name = bab_toHtml($arr['name']);
            
            $url = bab_url::get_request('tg');
            $url->idx = 'edit';
            $url->id_organization = $arr['id'];
            
            $this->editurl = bab_toHtml($url->toString());
            
            $this->agents = bab_toHtml($arr['agents']);
            
            $url = new bab_url(absences_Addon()->getUrl().'vacadm');
            $url->idx = 'lper';
            $url->filter = array('organization' => $arr['id']);
            $this->agentsurl = bab_toHtml($url->toString());
            return true;
        }
        
        return false;
    }

}




/**
 * Get a html menu to create an organization
 * @return string
 */
function absences_organizationMenu()
{
    $toolbar = absences_getToolbar();

    $addon = bab_getAddonInfosInstance('absences');
    $sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';

    $sync = (int) absences_getVacationOption('organization_sync');
    if (!$sync)
    {
    
        $toolbar->addToolbarItem(
            new BAB_ToolbarItem(
                absences_translate('Add organization'), 
                $addon->getUrl().'organizations&idx=edit',
                $sImgPath . 'edit_add.png', '', '', '')
        );
    
    }

    return $toolbar->printTemplate();
}





class absences_OrganizationEditor extends Widget_Form
{

    /**
     * @var absences_Organization
     */
    protected $organization;
    
    
    public function __construct(absences_Organization $organization)
    {
        $W = bab_Widgets();

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));

        $this->organization = $organization;

        $this->setName('organization');
        $this->addClass('widget-bordered');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-centered');
        $this->addClass(Func_Icons::ICON_LEFT_16);
        
        $this->colon();

        $this->setCanvasOptions($this->Options()->width(70,'em'));

        $this->addFields();
        $this->setValues($organization->getRow(), array('organization'));

        $this->addButtons();
        $this->setSelfPageHiddenFields();
    }
    
    
    
    protected function addFields()
    {
        $W = bab_Widgets();

        $this->addItem($W->LabelledWidget(
            absences_translate('Name'),
            $W->LineEdit()->setSize(70)->setMaxSize(255),
            'name'
        ));
    }
    
    

    protected function addButtons()
    {
        $W = bab_Widgets();
    
        $button = $W->FlowItems(
            $W->SubmitButton()->setName('save')->setLabel(absences_translate('Save'))
        )->setSpacing(2,'em')->setVerticalAlign('middle');
        
        $url = bab_url::get_request('tg');
        $url->idx = 'delete';
        $url->id_organization = $this->organization->id;
        
        $agents = $this->organization->getAgentInterator();
        
        if ($agents) {
        
            if ($agents->count()) {
                
                $button->addItem(
                    $W->Link(
                        $W->Icon(absences_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), 
                        $url->toString()
                    )->setConfirmationMessage(sprintf(absences_translate('This organization is linked to %d users, do you really whant to delete it?'), $agents->count()))
                );
            } else {
                
                $button->addItem(
                    $W->Link($W->Icon(absences_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $url->toString())
                        ->setConfirmationMessage(absences_translate('Do you really whant to delete this organization?'))
                );
            }
        }
        
        
        $this->addItem($button);
    }
}
