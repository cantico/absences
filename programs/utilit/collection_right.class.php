<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



require_once dirname(__FILE__).'/record.class.php';
require_once dirname(__FILE__).'/right.class.php';
require_once dirname(__FILE__).'/collection.class.php';

require_once $GLOBALS['babInstallPath'].'utilit/iterator/iterator.php';

/**
 * A vacation right linked to a collection
 *
 */
class absences_CollectionRight extends absences_Record 
{
	
	private $collection;
	
	private $right;
	
	
	/**
	 * Create absences_CollectionRight
	 * @param int $id
	 * @return absences_CollectionRight
	 */
	public static function getById($id)
	{
		$collectionRight = new absences_CollectionRight;
		$collectionRight->id = $id;
	
		return $collectionRight;
	}
	
	
	
	/**
	 *
	 * @return array
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			global $babDB;
	
			if (isset($this->id))
			{
				$res = $babDB->db_query('SELECT * FROM absences_coll_rights WHERE id='.$babDB->quote($this->id));
				$this->setRow($babDB->db_fetch_assoc($res));
	
			} else if (isset($this->collection) && isset($this->right))
			{
				$res = $babDB->db_query('SELECT * FROM absences_coll_rights WHERE id_coll='.$babDB->quote($this->collection->id).' AND id_right='.$babDB->quote($this->right->id));
				$this->setRow($babDB->db_fetch_assoc($res));
			}
		}
	
		return $this->row;
	}
	
	/**
	 *
	 * @param absences_Collection $collection
	 * @return absences_CollectionRight
	 */
	public function setCollection(absences_Collection $collection)
	{
		$this->collection = $collection;
		return $this;
	}
	
	/**
	 *
	 * @param absences_Right $right
	 * @return absences_CollectionRight
	 */
	public function setRight(absences_Right $right)
	{
		$this->right = $right;
		return $this;
	}
	
	
	/**
	 * @return absences_Right
	 */
	public function getRight()
	{
		if (!isset($this->right))
		{
			$row = $this->getRow();
			$this->right = new absences_Right($row['id_right']);
		}
	
		return $this->right;
	}
	
	/**
	 * @return absences_Collection
	 */
	public function getCollection()
	{
		if (!isset($this->collection))
		{
			$row = $this->getRow();
			$this->collection = absences_Collection::getById($row['id_coll']);
		}
	
		return $this->collection;
	}
}




