<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once dirname(__FILE__).'/vacincl.php';
require_once dirname(__FILE__).'/request.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';


/**
 * Vacation request
 *
 *
 * @property int	$id_user
 * @property string $createdOn
 * @property int	$createdBy
 * @property string	$date_begin
 * @property string	$date_end
 * @property int	$idfai
 * @property string	$comment
 * @property string	$date			Last modification date or approbation date (last action)
 * @property string	$status			'' : unconfirmed ; 'Y' : confirmed ; 'P' : previsional
 * @property string	$comment2		Commentaire approbateur
 * @property int	$id_approver
 * @property int	$folder
 * @property int	$creation_type
 * @property int	$archived
 * 
 *
 */
class absences_Entry extends absences_Request
{

	const CREATION_USER = 0;
	const CREATION_FIXED = 1;




	/**
	 * Memory for saved or unsaved elements of a vacation request
	 * @var array
	 */
	private $elements = array();


	/**
	 * Memory for working periods of a vacation request
	 * they are the working periods saved with the entry at the creation of the request
	 * the periods will remain event after a workschedule modification for the user
	 * @var array
	 */
	private $plannedPeriods = array();

	/**
	 * @var float
	 */
	private $duration_days = null;

	/**
	 * @var float
	 */
	private $duration_hours = null;


	/**
	 * Cache for actual working periods from calendar
	 * @var bab_CalendarPeriod[]
	 */
	private $working_periods;


	/**
	 * cache for total quantity
	 * @var float
	 */
	private $total_days = null;

	/**
	 * cache for total quantity
	 * @var float
	 */
	private $total_hours = null;


	/**
	 * cache for total quantity per type
	 * @var array
	 */
	private $total_type_days = null;

	/**
	 * cache for total quantity per type
	 * @var float
	 */
	private $total_type_hours = null;



	/**
	 * @var array
	 */
	private $workingPeriodIndex = null;
	
	
	/**
	 * @see absences_EntryPeriod::getDurationDays
	 * @var array
	 */
	public $_getDurationDays_halfDays = array();



	/**
	 * @return absences_Entry
	 */
	public static function getById($id)
	{
		$request = new absences_Entry();
		$request->id = $id;

		return $request;
	}


	/**
	 * (non-PHPdoc)
	 * @see absences_Record::getRow()
	 */
	public function getRow()
	{
		if (null === $this->row)
		{
			if (!isset($this->id))
			{
				throw new Exception('Failed to load entry, missing entry id');
			}

			global $babDB;
			$res = $babDB->db_query('SELECT * FROM absences_entries WHERE id='.$babDB->quote($this->id));
			$this->setRow($babDB->db_fetch_assoc($res));
		}

		return $this->row;
	}


	/**
	 * (non-PHPdoc)
	 * @see absences_Request::getRequestType()
	 *
	 * @return string
	 */
	public function getRequestType()
	{
		switch($this->creation_type)
		{
			case '1':
				return absences_translate('Fixed vacation');

			default:
			case '0':
				return absences_translate('Requested vacation');
		}



	}




	public function getApprobationId()
	{
		if ($agent = $this->getAgent())
		{
			return $agent->getApprobationId();
		}

		return null;
	}





	/**
	 * Get elements stored in database
	 * @return absences_EntryElemIterator
	 */
	public function getElementsIterator()
	{
		$I = new absences_EntryElemIterator;
		$I->entry = $this;

		return $I;
	}


	public function getPlannedPeriodsIterator()
	{
	    $I = new absences_EntryPeriodIterator;
	    $I->entry = $this;

	    return $I;
	}



	/**
	 * Get entries within the same folder
	 * @return absences_EntryIterator
	 */
	public function getFolderEntriesIterator()
	{
		if (!$this->folder)
		{
			return null;
		}

		$I = new absences_EntryIterator;
		$I->folder = $this->folder;

		return $I;
	}







	/**
	 *
	 * @param string $message	Generated message
	 * @param string $comment	Author comment
	 */
	public function addElementsMovements($message, $comment = '')
	{
		foreach($this->elements as $elem)
		{
			/*@var $elem absences_EntryElem */
			$elem->addMovement($message, $comment);
		}
	}




	/**
	 * Load elements from database
	 * @throws Exception
	 */
	public function loadElements()
	{
		if (!isset($this->id))
		{
			throw new Exception('Failed to load entry elements, id missing');
		}

		$this->elements = array();

		foreach($this->getElementsIterator() as $entryElem)
		{
			/*@var $entryElem absences_EntryElem */
			$this->addElement($entryElem);
		}
	}

	/**
	 * Get the loaded elements
	 * @return array
	 */
	public function getElements()
	{
		return $this->elements;
	}

	/**
	 * Get element by right from the loaded elements
	 * @param int|absences_Right $right
	 * @return absences_EntryElem
	 */
	public function getElement($right)
	{
	    $id_right = $right;
	    if ($right instanceof absences_Right) {
	        $id_right = $right->id; 
	    }
	    
	    
	    if (empty($this->elements)) {
	        // not loaded
	        
	        require_once dirname(__FILE__).'/entry_elem.class.php';
	        
	        global $babDB;
	        
	        $res = $babDB->db_query('SELECT * 
	            FROM absences_entries_elem 
	            WHERE 
	               id_entry='.$babDB->quote($this->id).' 
	               AND id_right='.$babDB->quote($id_right));
	        
	        $row = $babDB->db_fetch_assoc($res);
	        
	        if (!$row) {
	            return null;
	        }
	        
	        $elem = new absences_EntryElem();
	        $elem->setRow($row);
	        
	        if ($right instanceof absences_Right) {
	           $elem->setRight($right);
	        }
	        
	        return $elem;
	    }

	    
	    // already loaded
	    
	    foreach($this->elements as $element) {
	        if ($element->id_right == $id_right) {
	            return $element;
	        }
	    }

	    return null;
	}
	
	
	
	/**
	 * Add a planned period
	 * @param absences_EntryPeriod $plannedPeriod
	 */
	public function addPeriod(absences_EntryPeriod $plannedPeriod)
	{
	    $this->plannedPeriods[] = $plannedPeriod;
	}




	/**
	 * Load the planned working periods from database
	 * @throws Exception
	 */
	public function loadPlannedPeriods()
	{
	    if (!isset($this->id))
	    {
	        throw new Exception('Failed to load entry periods, id missing');
	    }

	    $this->plannedPeriods = array();

	    $res = $this->getPlannedPeriodsIterator();

	    if (0 === $res->count()) {
	        return $this->createPlannedPeriods();
	    }

	    foreach($res as $plannedPeriod)
	    {
	        /*@var $plannedPeriod absences_EntryPeriod */
	        $this->addPeriod($plannedPeriod);
	    }
	}

	/**
	 * Get the loaded planned working periods
	 * @return absences_EntryPeriod[]
	 */
	public function getPlannedPeriods()
	{
	    return $this->plannedPeriods;
	}


	/**
	 * Get planned duration
	 *
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 *
	 * @return float
	 */
	public function getPlannedDurationDays($begin = null, $end = null)
	{
	    if (empty($this->plannedPeriods)) {
	        $this->loadPlannedPeriods();
	    }

	    $total = 0.0;

	    foreach ($this->getPlannedPeriods() as $entryPeriod) {
	        $total += $entryPeriod->getDurationDays($begin, $end);
	    }
	    

	    
	     

	    return $total;
	}


	/**
	 * Get planned duration
	 *
	 * @param string $begin    Optional limit to use for duration
	 * @param string $end      Optional limit to use for duration
	 *
	 * @return float
	 */
	public function getPlannedDurationHours($begin = null, $end = null)
	{
	    if (empty($this->plannedPeriods)) {
	        $this->loadPlannedPeriods();
	    }

	    $total = 0.0;

	    foreach ($this->getPlannedPeriods() as $entryPeriod) {
	        $total += $entryPeriod->getDurationHours($begin, $end);
	    }

	    return $total;
	}

	
	public function loadDefaultValues()
	{
	    
	    if (!isset($this->createdOn)) {
	        $this->createdOn = date('Y-m-d H:i:s');
	    }
	    
	    if (!isset($this->date)) {
	        $this->date = date('Y-m-d H:i:s');
	    }
	    
	    if (!isset($this->comment)) {
	        $this->comment = '';
	    }
	    
	    if (!isset($this->comment2)) {
	        $this->comment2 = '';
	    }
	    
	    if (!isset($this->idfai)) {
	        $this->idfai = '0';
	    }
	    
	    if (!isset($this->status)) {
	        $this->status = 'N';
	    }
	    
	    if (!isset($this->id_approver)) {
	        $this->id_approver = '0';
	    }
	    
	    if (!isset($this->folder)) {
	        $this->folder = '0';
	    }
	    
	    if (!isset($this->creation_type)) {
	        $this->creation_type = '0';
	    }
	    
	}


	/**
	 * Save entry to database
	 * without validity checking
	 *
	 * @return bool
	 */
	public function save()
	{
		// save entry

		global $babDB;

        $this->loadDefaultValues();

		if (isset($this->id))
		{
			$req = "
				UPDATE absences_entries
				SET
					`date`			=".$babDB->quote($this->date).",
					date_begin		=".$babDB->quote($this->date_begin).",
					date_end		=".$babDB->quote($this->date_end).",
					comment			=".$babDB->quote($this->comment).",
					idfai			=".$babDB->quote($this->idfai).",
					status			=".$babDB->quote($this->status).",
					comment2		=".$babDB->quote($this->comment2).",
					id_approver		=".$babDB->quote($this->id_approver).",
					folder			=".$babDB->quote($this->folder).",
					creation_type	=".$babDB->quote($this->creation_type)."
			";
			
			if (isset($this->todelete))
			{
			    $req .= ", todelete=".$babDB->quote($this->todelete);
			}

			if (isset($this->appr_notified))
			{
				$req .= ", appr_notified=".$babDB->quote($this->appr_notified);
			}


			$req .= " WHERE
					id=".$babDB->quote($this->id)." ";

			$babDB->db_query($req);


		} else {

			$babDB->db_query("
				INSERT INTO absences_entries
				(
					id_user,
					date_begin,
					date_end,
					comment,
					`createdOn`,
					`date`,
					idfai,
					status,
					comment2,
					id_approver,
					folder,
					creation_type
				)
				VALUES
				(
					".$babDB->quote($this->id_user).",
					".$babDB->quote($this->date_begin).",
					".$babDB->quote($this->date_end).",
					".$babDB->quote($this->comment).",
					".$babDB->quote($this->createdOn).",
					".$babDB->quote($this->date).",
					".$babDB->quote($this->idfai).",
					".$babDB->quote($this->status).",
					".$babDB->quote($this->comment2).",
					".$babDB->quote($this->id_approver).",
					".$babDB->quote($this->folder).",
					".$babDB->quote($this->creation_type)."
				)
			");

			$this->id = $babDB->db_insert_id();
		}


	}


	/**
	 * Sort the loaded element by right order, type name
	 */
	public function sortElements()
	{
	    usort($this->elements, array('absences_Entry', 'sortElem'));
	}

	private static function sortElem(absences_EntryElem $elem1, absences_EntryElem $elem2)
	{
	    $right1 = $elem1->getRight();
	    $right2 = $elem2->getRight();

	    if ($right1->sortkey > $right2->sortkey) {
	        return 1;
	    }

	    if ($right1->sortkey < $right2->sortkey) {
	        return -1;
	    }

	    $type1 = $right1->getType();
	    $type2 = $right2->getType();

	    return bab_compare(mb_strtolower($type1->name), mb_strtolower($type2->name));
	}
	
	
	/**
	 * Verifier si les dates des elements sont correctement parametres
	 * en cas de modificiation de la quantite, toujours mettre les dates a zero
	 * @return bool
	 */
	public function checkElementsDates()
	{
	    foreach($this->elements as $elem) {
	        if (!isset($elem->date_begin) || '0000-00-00 00:00:00' === $elem->date_begin) {
	            return false;
	        }
	        
	        if (!isset($elem->date_end) || '0000-00-00 00:00:00' === $elem->date_end) {
	            return false;
	        }
	    }
	    
	    return true;
	}
	
	


	/**
	 * Add dates to loaded elements using the user calendar
	 */
	public function setElementsDates()
	{
	    include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

	    if (0 === count($this->elements)) {
	        throw new absences_EntryException('No elements to set dates on, id_entry='.$this->id.', owner='.$this->getUserName());
	    }
	    
	    if ($this->checkElementsDates()) {
	        // dates allready set
	        // calling getFutureDate twice does not work
	        return;
	    }

	    if (1 === count($this->elements)) {
	        $element = reset($this->elements);
	        /*@var $element absences_EntryElem */
	        $element->date_begin = $this->date_begin;
	        $element->date_end = $this->date_end;
	        return;
	    }
	    
	    $this->sortElements();
        
	    $loop = BAB_DateTime::fromIsoDateTime($this->date_begin);
	    
	    

	    foreach($this->elements as $element) {
	        /*@var $element absences_EntryElem */
	        $element->date_begin = $loop->getIsoDateTime();
	        
	        $loop = $this->getFutureDate($loop, (float) $element->quantity, $element->getRight()->quantity_unit);
	        $element->date_end = $loop->getIsoDateTime();
	        
	        if ($element->date_end > $this->date_end) {
	            $element->date_end = $this->date_end;
	        }
	        
	        $loop = $this->getNextValidDate($loop);
	    }
	    
	    // round the last half day to the request period end
	    if ($this->getElementEndGap() <= 3600) {
	        $this->elements[count($this->elements)-1]->date_end = $this->date_end;
	    }
	}
	
	/**
	 * Get the gap beetween the last element end date and the period end date
	 * @return int
	 */
	protected function getElementEndGap()
	{
	    $computedLastDate = bab_mktime($this->elements[count($this->elements)-1]->date_end);
	    $periodEnd = bab_mktime($this->date_end);
	     
	    if ($periodEnd > $computedLastDate) {
	         return ($periodEnd - $computedLastDate);
	    }
	    
	    if ($computedLastDate > $periodEnd) {
	        return ($computedLastDate - $periodEnd);
	    }
	    
	    return 0;
	}


	/**
	 * set the date to the next valid date
	 * @param BAB_DateTime $date
	 * @return BAB_DateTime
	 */
	protected function getNextValidDate(BAB_DateTime $date)
	{
	    $moment = $date->getTimeStamp();

	    foreach ($this->getWorkingPeriods() as $period) {

	        if ($moment >= $period->ts_end) {
	            continue;
	        }

	        // if the future date (end date of the element) is in a worked period
	        // the future date is valid as a next date
	        if ($moment < $period->ts_end && $moment > $period->ts_begin) {
	            return $date;
	        }


	        return BAB_DateTime::fromTimeStamp($period->ts_begin);
	    }


	    return $date;
	}



	/**
	 * Add quantity to startdate only on working periods
	 *
	 * @param BAB_DateTime $startdate
	 * @param float $quantity
	 * @param string $quantity_unit D|H
	 *
	 * @return BAB_DateTime
	 */
	protected function getFutureDate(BAB_DateTime $startdate, $quantity, $quantity_unit)
	{
	    if ('H' === $quantity_unit) {
	        return $this->getFutureDate_Hours($startdate, $quantity);
	    }

	    return $this->getFutureDate_Days($startdate, $quantity);
	}


	/**
	 * Split working periods
	 * @return array
	 */
	protected function getWorkingPeriodsFromDate(BAB_DateTime $startdate)
	{
	    $periods = array();
	    $start = $startdate->getTimeStamp();

	    foreach ($this->getWorkingPeriods() as $period) {
	        
	        $period = clone $period; // do not overwrite the working periods
	                                 // create array of new objects

	        if ($start >= $period->ts_end) {
	            // continue to the next worked period
	            continue;
	        }

	        if ($start > $period->ts_begin && $start < $period->ts_end) {
	            $period->setBeginDate($startdate);
	            $periods[] = $period;
	            continue;
	        }

	        $periods[] = $period;
	    }

	    return $periods;
	}



	/**
	 * Add quantity to startdate only on working periods
	 *
	 * @param BAB_DateTime $startdate
	 * @param float $seconds_to_add
	 *
	 * @return BAB_DateTime
	 */
	protected function getFutureDate_Seconds(BAB_DateTime $startdate, $seconds_to_add)
	{
	    $worked_total = 0; //seconds


	    foreach ($this->getWorkingPeriodsFromDate($startdate) as $period) {

	        $add_in_period = ($seconds_to_add - $worked_total);
	        $worked_total += $period->getDuration();

	        if ((int) $worked_total === (int) $seconds_to_add) {
	            // la duree de la periode de travail est egale a duree demandee
	            // en tenant compte des periodes de travail precedentes

	            return BAB_DateTime::fromTimeStamp($period->ts_end);
	        }


            if ($worked_total > $seconds_to_add) {
                // la date future se trouve a l'interieur d'une periode travaillee
                $futureDate = $period->ts_begin + $add_in_period;
                return BAB_DateTime::fromTimeStamp($futureDate);
            }

            // continue to the next worked period

	    }

	    return BAB_DateTime::fromIsoDateTime($this->date_end);
	}






	/**
	 * Add quantity to startdate only on working periods
	 *
	 * @param BAB_DateTime $startdate
	 * @param float $quantity  hours
	 *
	 * @return BAB_DateTime
	 */
	protected function getFutureDate_Hours(BAB_DateTime $startdate, $quantity)
	{
	    $seconds_to_add = $quantity * 3600;

	    return $this->getFutureDate_Seconds($startdate, $seconds_to_add);
	}
	
	
	
	
	
	
	
	
	
    
	
	
	
	/**
	 * Add quantity to startdate only on working periods
	 *
	 * @param BAB_DateTime $startdate
	 * @param float $quantity  days
	 *
	 * @return BAB_DateTime
	 */
	protected function getFutureDate_Days(BAB_DateTime $startDate, $quantity)
	{
	    $total_seconds = $this->getTotalHours() * 3600;
	    $total_days = $this->getTotalDays();
	    $seconds_to_add = $quantity * $total_seconds / $total_days;
	    
	    return $this->getFutureDate_Seconds($startDate, $seconds_to_add);
	}
	
	
	
	



	/**
	 * Create the planned periods in the entry boundaries
	 * from the calendar working periods
	 */
	public function createPlannedPeriods()
	{
	    require_once dirname(__FILE__).'/entry_period.class.php';
	    $this->plannedPeriods = array();

	    foreach ($this->getWorkingPeriods() as $workingPeriod) {

	        /*@var $workingPeriod bab_CalendarPeriod */

	        $plannedPeriod = new absences_EntryPeriod();
	        $plannedPeriod->setEntry($this);
	        $plannedPeriod->date_begin = date('Y-m-d H:i:s', $workingPeriod->ts_begin);
	        $plannedPeriod->date_end = date('Y-m-d H:i:s', $workingPeriod->ts_end);

	        $this->plannedPeriods[] = $plannedPeriod;
	    }
	}


	



	/**
	 * Save elements of entry to database
	 *
	 */
	public function saveElements()
	{
		$processed_ids = array();

		foreach ($this->elements as $elem)
		{
			/*@var $elem absences_EntryElem */
		    
		    try {
		    
    			$elem->save();
    			$processed_ids[] = $elem->id;
    			
		    } catch(Exception $e) {
		        // fail to save one element, it will be deleted or not created
		        bab_debug($e->getMessage());
		    }
		}

		// delete removed elements

		global $babDB;

		$babDB->db_query('DELETE FROM absences_entries_elem WHERE id_entry='.$babDB->quote($this->id).' AND id NOT IN('.$babDB->quote($processed_ids).')');

	}




	public function savePlannedPeriods()
	{
	    if (empty($this->plannedPeriods)) {
	        throw new Exception('Planned periods where not loaded or set');
	    }

	    $processed_ids = array();

	    foreach ($this->plannedPeriods as $period)
	    {
	        /*@var $elem absences_EntryPeriod */
	        $period->save();

	        $processed_ids[] = $period->id;
	    }

	    global $babDB;

	    $babDB->db_query('DELETE FROM absences_entries_periods WHERE id_entry='.$babDB->quote($this->id).' AND id NOT IN('.$babDB->quote($processed_ids).')');

	}



	/**
	 * Apply dynamic rights for all right involved in the entry
	 */
	public function applyDynamicRight()
	{
		require_once dirname(__FILE__).'/agent_right.class.php';

		$I = new absences_AgentRightManagerIterator();
		$I->setAgent($this->getAgent());

		foreach ($I as $agentRight)
		{
			/*@var $agentRight absences_AgentRight */
			$agentRight->applyDynamicRight();
		}
	}




	/**
	 * Add element to list
	 * @param absences_EntryElem $elem
	 */
	public function addElement(absences_EntryElem $elem)
	{
		$this->elements[] = $elem;
	}

	/**
	 * Remove an element in the list
	 * @param int $id_right
	 *
	 * @return bool
	 */
	public function removeElement($id_right)
	{
		foreach ($this->elements as $key => $elem)
		{
			if ($elem->id_right == $id_right)
			{
				unset($this->elements[$key]);
				return true;
			}
		}

		return false;
	}


	/**
	 * Set the working period index for the entry
	 * Used in unit tests to set differents working periods
	 * if not used, the workingPeriodIndex will be generated from the calendar API
	 *
	 */
	public function setWorkingPeriodIndex(Array $index_working)
	{
	    $this->workingPeriodIndex = $index_working;
	    return $this;
	}

	/**
	 * Get working period index for the user and for the period
	 * @return array
	 */
	private function getWorkingPeriodIndex()
	{
	    if (!isset($this->workingPeriodIndex)) {

	        include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

	        list($index_working,, $is_free) = absences_getHalfDaysIndex(
	            $this->id_user,
	            BAB_DateTime::fromIsoDateTime($this->date_begin),
	            BAB_DateTime::fromIsoDateTime($this->date_end),
	            true
	        );


	        foreach ($index_working as $key => $period_list) {
	            if (!isset($is_free[$key])) {
	                unset($index_working[$key]);
	            }
	        }

	        $this->workingPeriodIndex = $index_working;
	    }

	    return $this->workingPeriodIndex;
	}



	/**
	 * Number of free days and hours between two dates
	 *
	 */
	private function loadDurations() {

		$this->duration_days 	= 0.0;
		$this->duration_hours 	= 0.0;
		$this->working_periods = array();



		$index_working = $this->getWorkingPeriodIndex();


		foreach ($index_working as $period_list) {

			$this->duration_days += 0.5;

			foreach($period_list as $p)
			{
				/*@var $p bab_CalendarPeriod */

				if ($p->getCollection() instanceof bab_WorkingPeriodCollection)
				{
				    $this->working_periods[] = $p;
					$this->duration_hours 	+= ($p->getDuration() / 3600);
				}
			}
		}
	}


	/**
	 * Get list of working periods of the entry
	 * @return bab_CalendarPeriod[]
	 */
	protected function getWorkingPeriods()
	{
	    if (!isset($this->working_periods)) {
	        $this->loadDurations();
	    }
	    
	    return $this->working_periods;
	}


	/**
	 * List of working periods for one day
	 * @param string $date 10 chars
	 *
	 * @return bab_CalendarPeriod[]
	 */
	public function getDayWorkingPeriods($date)
	{
	    $return = array();
	    foreach ($this->getWorkingPeriods() as $period) {
	        if ($date === date('Y-m-d', $period->ts_begin)) {
	            $return[] = $period;
	        }
	    }

	    return $return;
	}


	/**
	 * List of working periods for one day
	 * @param string $date 10 chars
	 *
	 * @return absences_EntryPeriod[]
	 */
	public function getDayPlannedPeriods($date)
	{
	    $return = array();
	    foreach ($this->getPlannedPeriodsIterator() as $period) {
	        if ($date === substr($period->date_begin, 0, 10)) {
	            $return[] = $period;
	        }
	    }

	    return $return;
	}




	/**
	 * Get period duration in days
	 * @return float
	 */
	public function getDurationDays()
	{
		if (!isset($this->duration_days))
		{
			$this->loadDurations();
		}

		return $this->duration_days;
	}

	/**
	 * Get period duration in hours
	 * @return float
	 */
	public function getDurationHours()
	{
		if (!isset($this->duration_hours))
		{
			$this->loadDurations();
		}

		return $this->duration_hours;
	}


	/**
	 * Convert a number of days to hours
	 * @param float $days
	 * @return float
	 */
	private function daysToHours($days)
	{
	    $plannedDays = $this->getPlannedDurationDays();
	    
	    if (!$plannedDays) {
	        return 0;
	    }
	    
		$ratio = $this->getPlannedDurationHours() / $plannedDays;
		return round(($ratio * $days), 2);
	}

	/**
	 * Convert a number of hours to days
	 * @param float $hours
	 * @return float
	 */
	private function hoursToDays($hours)
	{
	    $plannedHours = $this->getPlannedDurationHours();
	    
	    if (!$plannedHours) {
	        return 0;
	    }
	    
		$ratio = $this->getPlannedDurationDays() / $plannedHours;
		return round(($ratio * $hours), 2);
	}



	/**
	 * Compute totals for loaded elements
	 */
	private function loadedElementsTotal()
	{
		if (empty($this->elements))
		{
			$this->loadElements();
		}

		$this->total_days = 0.0;
		$this->total_hours = 0.0;
		$this->total_type_days = array();
		$this->total_type_hours = array();

		foreach($this->elements as $elem)
		{
			/*@var $elem absences_EntryElem */
			$right = $elem->getRight();

			$quantity = (float) $elem->quantity;

			switch($right->quantity_unit)
			{
				case 'D':
					$hours = $this->daysToHours($quantity);
					$this->addQUantityInCache($right, $quantity, $hours);
					break;
				case 'H':
				    $days = $this->hoursToDays($quantity);
				    $this->addQUantityInCache($right, $days, $quantity);
					break;
			}
		}
	}


	/**
	 * Populate the cache variables for one element
	 *
	 * @param absences_Right $right
	 * @param float $days
	 * @param float $hours
	 */
	private function addQUantityInCache(absences_Right $right, $days, $hours)
	{
	    if (!isset($this->total_type_days[$right->id_type])) {
	        $this->total_type_days[$right->id_type] = 0.0;
	    }

	    if (!isset($this->total_type_hours[$right->id_type])) {
	        $this->total_type_hours[$right->id_type] = 0.0;
	    }

	    $this->total_days += $days;
	    $this->total_hours += $hours;
	    $this->total_type_days[$right->id_type] += $days;
	    $this->total_type_hours[$right->id_type] += $hours;
	}





	/**
	 * Get sum of elements quantity, converted in days
	 *
	 * @param int $id_type Optional filter by type
	 *
	 * @return float
	 */
	public function getTotalDays($id_type = null)
	{
		if (!isset($this->total_days)) {
			$this->loadedElementsTotal();
		}

		if (isset($id_type)) {

		    if (!isset($this->total_type_days[$id_type])) {
		        return 0.0;
		    }

		    return $this->total_type_days[$id_type];
		}

		return $this->total_days;
	}

	/**
	 * Get sum of elements quantity, converted in hours
	 *
	 * @param int $id_type Optional filter by type
	 *
	 * @return float
	 */
	public function getTotalHours($id_type = null)
	{
		if (!isset($this->total_hours)) {
			$this->loadedElementsTotal();
		}

		if (isset($id_type)) {

		    if (!isset($this->total_type_hours[$id_type])) {
		        return 0.0;
		    }

		    return $this->total_type_hours[$id_type];
		}

		return $this->total_hours;
	}




	/**
	 * Number of days on entry between two dates
	 * si les dates de l'element sont a cheval sur la periode demandee
	 * on utlise les heures travailles qui etait en vigeur au moment de la creation
	 * de la demande
	 *
	 * @param string   $begin	Datetime
	 * @param string   $end		Datetime
	 * @param int      $id_type Optional filter by type
	 *
	 * @return float (days)
	 */
	public function getPlannedDaysBetween($begin, $end, $id_type = null)
	{
	    if (empty($this->elements))
	    {
	        $this->loadElements();
	    }

	    $total = 0.0;
	    foreach($this->elements as $elem)
	    {
	        /*@var $elem absences_EntryElem */
	        $right = $elem->getRight();

	        if (isset($id_type) && (int) $id_type !== (int) $right->id_type) {
	            continue;
	        }

	        $quantity = $elem->getPlannedQuantityBetween($begin, $end);

	        if ('H' === $right->quantity_unit) {
	            $days = $this->hoursToDays($quantity);
	        } else {
	            $days = $quantity;
	        }

	        $total += $days;
	    }

	    return $total;
	}




	/**
	 * Number of hours on entry between two dates
	 * si les dates de l'element sont a cheval sur la periode demandee
	 * on utlise les heures travailles qui etait en vigeur au moment de la creation
	 * de la demande
	 *
	 * @param string   $begin	Datetime
	 * @param string   $end		Datetime
	 * @param int      $id_type Optional filter by type
	 *
	 * @return float (hours)
	 */
	public function getPlannedHoursBetween($begin, $end, $id_type = null)
	{
	    if (empty($this->elements))
	    {
	        $this->loadElements();
	    }

	    $total = 0.0;
	    foreach($this->elements as $elem)
	    {
	        /*@var $elem absences_EntryElem */
	        $right = $elem->getRight();

	        if (isset($id_type) && (int) $id_type !== (int) $right->id_type) {
	            continue;
	        }

	        $quantity = $elem->getPlannedQuantityBetween($begin, $end);

	        if ('D' === $right->quantity_unit) {
	            $hours = $this->daysToHours($quantity);
	        } else {
	            $hours = $quantity;
	        }

	        $total += $hours;
	    }

	    return $total;
	}








	/**
	 * Test if the loaded elements in entry contains rights in hours
	 * @return bool
	 */
	public function containsHours()
	{
		foreach($this->elements as $elem)
		{
			if ('H' === $elem->getRight()->quantity_unit)
			{
				return true;
			}
		}

		return false;
	}


	/**
	 * Test if loaded elements in entry require approval
	 *
	 * @return bool
	 */
	public function requireApproval()
	{
		if (empty($this->elements))
		{
			$this->loadElements();
		}

		foreach($this->elements as $elem)
		{
			if (1 === (int) $elem->getRight()->require_approval)
			{
				return true;
			}
		}

		return false;
	}
	
	
	public function checkAvailable()
	{
	    $res = $this->getElementsIterator();
	    foreach ($res as $element) {
	        /*@var $element absences_EntryElem */
	        if (!$element->isQuantityAvailable()) {
	            throw new Exception(sprintf(absences_translate('Failed to submit this request, the quantity for right %s s not available'), $element->getRight()->description));
	        }
	    }
	}

	
	/**
	 * Check elements validity
	 * @throws UnexpectedValueException
	 * 
	 * @return int number of elements with quantity > 0
	 */
	protected function checkElementsValidity()
	{
	    $count = 0;
	    foreach($this->elements as $elem)
	    {
	        $quantity = (int) round(100 * $elem->quantity);
	        if ($quantity > 0)
	        {
	            $count++;
	        }
	    
	        $elem->checkValidity();
	    }
	    
	    return $count;
	}
	
	
	/**
	 * throw an exception if there is another entry in the same period
	 * @throws absences_EntryException
	 */
	protected function checkOtherEntriesValidity()
	{
	    $otherEntries = new absences_EntryIterator();
	    $otherEntries->from = $this->date_begin;
	    $otherEntries->to = $this->date_end;
	    $otherEntries->users = array($this->id_user);
	    
	    
	    foreach ($otherEntries as $otherEntry) {
	    
	        /* @var $otherEntry absences_Entry */
	    
	        if ($this->id != $otherEntry->id) {
	            $e = new absences_EntryException(absences_translate('There is allready an absence request in the period'));
	            $e->entry = $this;
	            $e->blocking = true;
	            throw $e;
	        }
	    }
	    
	}


	/**
	 * Check request validity
	 *
	 * @throws absences_EntryException
	 * @throws UnexpectedValueException
	 * 
	 * @param bool $checkDuration Checking duration require a saved entry
	 *                            Use false if entry is checked before save
	 * 
	 * @return bool
	 */
	public function checkValidity($checkDuration = true)
	{
		// verify mandatory data

		if (!isset($this->id_user) || $this->id_user <= 0)
		{
			throw new UnexpectedValueException('Unexpected id_user');
		}


		$count = $this->checkElementsValidity();


		if (0 === $count)
		{
			$e = new absences_EntryException(absences_translate('At least one vacation right must be used'));
			$e->entry = $this;
			$e->blocking = true;
			throw $e;
		}



		// test user validity

		require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';

		$creationdate = bab_userInfos::getCreationDate($this->id_user);

		if ($creationdate >= $this->date_end) {
			$e = new absences_EntryException(sprintf(absences_translate('The vacation request end before user creation date : %s'), bab_shortDate(bab_mktime($creationdate))));
			$e->entry = $this;
			$e->blocking = true;
			throw $e;
		}

		// T8359 : si l'utilisateur est reactive la demande ne sera pas presente, il faudra reenregistrer le droit
		// avant la correction de ce ticket, on creeais les demandes pour eviter ce probleme

		if (!bab_userInfos::isValid($this->id_user)) {

		    /*
		    $e = new absences_EntryException(absences_translate('This user account is disabled'));
		    $e->entry = $this;
		    $e->blocking = true;
		    throw $e;
		    */

		    return false;
		}


		// test quantity / period duration in days
		
		if ($checkDuration) {

    		$total = (int) round(100 * $this->getTotalDays());
    		$duration = (int) round(100 * $this->getDurationDays());
    
    
    		if (0 === $duration)
    		{
    			$e = new absences_EntryException(absences_translate('The selected period is not available'));
    			$e->entry = $this;
    			$e->blocking = true;
    			throw $e;
    		}
    
    
    
    		if ($total !== $duration)
    		{
    			$e = new absences_EntryException(sprintf(absences_translate('The total quantity (%s) does not match the period duration (%s)'), absences_quantity($this->getTotalDays(), 'D'), absences_quantity($this->getDurationDays(), 'D')));
    			$e->entry = $this;
    			$e->blocking = !((bool) absences_getVacationOption('allow_mismatch'));
    			throw $e;
    		}
		}

		// test quantity / period duration in hours only if there is one right in hours
		/*
		 *
		 * Pas de verification du nombre d'heure pour le moment, la precision de la verification du nombre de jours devrais suffire a verifier aussi les heures
		 * c'est ce qui a ete fait sur la partie en HTML lorsque les les tests etait fait uniquement cote client (vacuser.html template newvacation)
		 *
		if ($this->containsHours())
		{
			$total = (int) round(100 * $this->getTotalHours());
			$duration = (int) round(100 * $this->getDurationHours());

			if ($total !== $duration)
			{
				$e = new absences_EntryException(absences_translate('The total quantity does not match the period duration in hours'));
				$e->blocking = !((bool) absences_getVacationOption('allow_mismatch'));
				throw $e;
			}
		}

		*/



		// verifier si la periode demandee n'est pas deja occuppe par une autre demande
        $this->checkOtherEntriesValidity();
		

		return true;
	}


	/**
	 * (non-PHPdoc)
	 * @see absences_Request::modifiedOn()
	 *
	 * @return string
	 */
	public function modifiedOn()
	{
		return $this->date;
	}

	/**
	 * Test if the entry is a fixed vacation
	 * @return bool
	 */
	public function isFixed()
	{
		return (1 === (int) $this->creation_type);

	}





	/**
	 * Test if entry is previsonal
	 * @return bool
	 */
	public function isPrevisonal()
	{
		return ($this->status === 'P');
	}

	/**
	 * Test if at least one entry in folder is previsonal
	 * @return boolean
	 */
	public function isFolderPrevisonal()
	{
		if (!$this->folder)
		{
			return false;
		}

		$I = new absences_EntryIterator;
		$I->folder = $this->folder;
		$I->status = 'P';

		return ($I->count() > 0);
	}

	/**
	 * Get the first entry in folder
	 * @return absences_Entry
	 */
	public function getFolderFirst()
	{
		if (!$this->folder)
		{
			return null;
		}

		$I = new absences_EntryIterator;
		$I->folder = $this->folder;

		foreach($I as $first)
		{
			return $first;
		}

		return null;
	}


	/**
	 * Process specific code when the request is rejected
	 *
	 */
	protected function onReject()
	{
		$this->updateCalendar();
	}


	/**
	 * Process specific code when the request is confirmed
	 *
	 */
	public function onConfirm()
	{
		$this->updateCalendar();
	}



	/**
	 * Call the user calendar backend to update the event
	 * call the event to notify about calendar event modification
	 */
	public function updateCalendar()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';

		// try to update event copy in other backend (caldav)

		$begin = BAB_DateTime::fromIsoDateTime($this->date_begin);
		$end = BAB_DateTime::fromIsoDateTime($this->date_end);
		$period = absences_getPeriod($this->id, $this->id_user,  $begin, $end);
		if ($period) {

			// probably set a new description if the event has been approved or rejected
			absences_setPeriodProperties($period, $this);

			// save copy of event to calendar backend (if caldav)
			$period->save();
		}


		// Update calendar data overlapped with event

		$date_begin = bab_mktime($this->date_begin);
		$date_end	= bab_mktime($this->date_end);

		include_once $GLOBALS['babInstallPath']."utilit/eventperiod.php";
		$event = new bab_eventPeriodModified($date_begin, $date_end, $this->id_user);
		$event->types = BAB_PERIOD_VACATION;
		bab_fireEvent($event);
	}


	public function getTitle()
	{
	    if (isset($this->todelete) && $this->todelete) {
	        return absences_translate('vacation request to delete');
	    }
	    
		return absences_translate('vacation request');
	}


	public function getNotifyFields()
	{
		return array(
			absences_translate('From') 		=> bab_shortDate(bab_mktime($this->date_begin)),
			absences_translate('Until')		=> bab_shortDate(bab_mktime($this->date_end)),
			absences_translate('Quantity') 	=> absences_quantity($this->getTotalDays(), 'D')
		);
	}


	public function getYear()
	{
		$year = (int) substr($this->date_begin, 0, 4);

		if (0 === $year)
		{
			return null;
		}

		return $year;
	}


	public function getArchiveYear()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		$year = (int) substr($this->date_begin, 0, 4);

		if (0 === $year)
		{
			return null;
		}

		$day = absences_getVacationOption('archivage_day');
		$month = absences_getVacationOption('archivage_month');

		$currentYear = new BAB_DateTime($year, $month, $day);
		if($this->date_begin < $currentYear->getIsoDate()){
			$year = $year-1;
		}

		return $year;
	}

	public function archive()
	{
		global $babDB;

		$babDB->db_query("
			UPDATE absences_entries
			SET archived=".$babDB->quote(1)."
			WHERE
				id=".$babDB->quote($this->id)."
		");
	}


	public function setNotified()
	{
		global $babDB;

		if (!$this->id)
		{
			throw new Exception('Missing ID');
		}

		$babDB->db_query("
			UPDATE absences_entries
				SET appr_notified=".$babDB->quote(1)."
			WHERE
				id=".$babDB->quote($this->id)."
		");
	}


	public function getManagerEditUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		$ts = bab_mktime($this->date_begin);
		return $addon->getUrl().'vacuser&idx=period&id='.$this->id.'&year='.date('Y', $ts).'&month='.date('n', $ts).'&rfrom=1';
	}


	public function getManagerDeleteUrl()
	{
		$addon = bab_getAddonInfosInstance('absences');
		$url = new bab_url($addon->getUrl().'vacadmb');
		$url->idx = 'delete';
		$url->id_entry = $this->id;
		$url->from = $_SERVER['REQUEST_URI'];
		return $url->toString();
	}


	public function getManagerFrame()
	{
		$W = bab_Widgets();
		$vbox = $W->VBoxLayout();

		if ($this->isFixed())
		{
			$vbox->addItem($W->Icon(absences_translate('Fixed vacation period'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY));
		} else {
			$vbox->addItem($W->Link($W->Icon(absences_translate('Vacation request'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), absences_addon()->getUrl()."vacadmb&idx=morvw&id=".$this->id));
		}
		$vbox->addItem($W->Label(absences_DateTimePeriod($this->date_begin, $this->date_end)));

		return $vbox;
	}


	/**
	 *
	 * @param bool 			$display_username	Affiche le nom du demandeur dans la card frame ou non, utile pour les listes contenant plusieurs demandeurs possibles
	 * @param int			$rfrom				si les demandes de la liste sont modifiee par un gestionnaire ou gestionnaire delegue
 	 * @param int			$ide				id entite de l'organigramme >0 si gestionnaire delegue seulement
	 */
	public function getCardFrame($display_username, $rfrom, $ide)
	{
		bab_functionality::includeOriginal('Icons');

		$W = bab_Widgets();
		$layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em')->addClass('widget-full-width');
		$frame = $W->Frame(null, $layout);

		$frame->addClass(Func_Icons::ICON_LEFT_16);
		$frame->addClass('absences-entry-cardframe');
		$layout->addItem($col1 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
		$layout->addItem($col2 = $W->VBoxLayout()->setVerticalSpacing(.3,'em'));

		if ($this->isPrevisonal())
		{
			$btn = $W->Link($W->Button()->addItem($W->Label(absences_translate('Submit for approval'))), absences_addon()->getUrl()."vacuser&idx=subprev&id_entry=".$this->id);
			$btn->setSizePolicy('display-opened');
			$layout->addItem($btn);
		}

		if ($this->isFolderPrevisonal())
		{
			$first = $this->getFolderFirst();
			if ($first && $first->id === $this->id)
			{
				$btn = $W->Link($W->Button()->addItem($W->Label(absences_translate('Submit requests for approval'))), absences_addon()->getUrl()."vacuser&idx=subprev&id_entry=".$this->id);
				$btn->setSizePolicy('display-closed');
				$layout->addItem($btn);
			}
		}


		$layout->addItem($col3 = $W->VBoxLayout()->setVerticalSpacing(.5,'em'));

		if ($this->isFixed())
		{
			$col1->addItem($W->Icon(absences_translate('Fixed vacation period'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY));
			$col1->setCanvasOptions($col1->Options()->width(25,'em'));
		} else {
			$col1->addItem($W->Link($W->Icon(absences_translate('Vacation request'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), absences_addon()->getUrl()."vacuser&idx=morve&id=".$this->id));
			$col1->setCanvasOptions($col1->Options()->width(25,'em'));

			$col1->addItem($this->getStatusIcon());
		}

		$col2->setSizePolicy(Widget_SizePolicy::MAXIMUM);

		$col2->addItem($W->Title(absences_DateTimePeriod($this->date_begin, $this->date_end), 5));

		if ($display_username)
		{
			$col2->addItem($this->labelledValue(absences_translate('Owner'), $this->getUserName()));
		}

		$col2->addItem($this->labelledValue(absences_translate('Quantity'), absences_vacEntryQuantity($this->id)));




		if ($this->canModify())
		{
			$col3->addItem($W->Link($W->Icon(absences_translate('Modify'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->getEditUrl($rfrom, $ide)));
		}

		if ($this->canDelete())
		{
			$urldelete = absences_addon()->getUrl()."vacuser&idx=delete&id_entry=".$this->id;
			$col3->addItem($W->Link($W->Icon(absences_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $urldelete));
		}

		$frame->setTitle(sprintf(absences_translate('Created the %s'), bab_shortDate(bab_mktime($this->createdOn()))));

		return $frame;
	}





	/**
	 * @return string
	 */
	public function getEditUrl($rfrom, $ide = null)
	{
		$begin_ts = bab_mktime($this->date_begin);
		$url = absences_addon()->getUrl()."vacuser&idx=period&id=".$this->id."&year=".date('Y',$begin_ts)."&month=".date('n',$begin_ts);

		if (isset($rfrom))
		{
			$url .= '&rfrom='.$rfrom;
		}

		if (isset($ide))
		{
			$url .= '&ide='.$ide;
		}

		return $url;
	}



	/**
	 * Delete the vacation request
	 * @return bool
	 */
	public function delete()
	{



		global $babDB;

		if (!$this->getRow())
		{
			return false;
		}

		parent::delete();
		
		if ('0000-00-00 00:00:00' !== $this->date_begin && '0000-00-00 00:00:00' !== $this->date_end) {
    		include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
    		$date_begin = BAB_DateTime::fromIsoDateTime($this->date_begin);
    		$date_end	= BAB_DateTime::fromIsoDateTime($this->date_end);
    
    		$period = absences_getPeriod($this->id, $this->id_user, $date_begin, $date_end);
    		if (null !== $period)
    		{
    			$period->delete();
    		}
		}
		$babDB->db_query("DELETE FROM absences_dynamic_rights WHERE id_entry=".$babDB->quote($this->id));
		$babDB->db_query("DELETE FROM ".ABSENCES_ENTRIES_ELEM_TBL." WHERE id_entry=".$babDB->quote($this->id)."");
		$babDB->db_query("DELETE FROM ".ABSENCES_ENTRIES_TBL." WHERE id=".$babDB->quote($this->id));

		$this->applyDynamicRight();
		
		if (isset($date_begin) && isset($date_end)) {
    		$cal_begin = clone $date_begin;
    		$cal_end = clone $date_end;
    
    		$cal_end->add(1, BAB_DATETIME_MONTH);
    
    		while ($cal_begin->getTimeStamp() <= $cal_end->getTimeStamp()) {
    			$month	= $cal_begin->getMonth();
    			$year	= $cal_begin->getYear();
    			absences_updateCalendar($this->id_user, $year, $month);
    			$cal_begin->add(1, BAB_DATETIME_MONTH);
    		}
    		
    		$this->addMovement(
    		        sprintf(
    		                absences_translate('The vacation entry from %s to %s has been deleted'),
    		                $date_begin->shortFormat(true),
    		                $date_end->shortFormat(true)
    		        )
    		);
		}

		

		return true;
	}







	/**
	 * (non-PHPdoc)
	 * @see absences_Request::notifyOwner()
	 */
	public function notifyOwner()
	{
		parent::notifyOwner();

		if ('Y' != $this->status)
		{
			return;
		}

		$agent = $this->getAgent();
		$emails = $agent->getEmails();

		if (empty($emails))
		{
			return;
		}

		require_once dirname(__FILE__).'/request.notify.php';
		absences_notifyEntryOwnerEmails(array($this), $emails);
	}

}




class absences_EntryException extends Exception
{
	/**
	 * Optional entry associated to error (unsaved entry object)
	 * @var absences_Entry
	 */
	public $entry;


	/**
	 * Define if the exception if blocking a save
	 * @var bool
	 */
	public $blocking = true;
}



/**
 * Vacation requests
 * Sorted by creation date
 */
class absences_EntryIterator extends absences_Iterator
{

	/**
	 *
	 * @var string
	 */
	public $orderby = 'e.createdOn ASC';


	/**
	 *
	 * @var string
	 */
	public $folder;

	/**
	 *
	 * @var array
	 */
	public $users;


	/**
	 *
	 * @var mixed (array|string)
	 */
	public $status;


	/**
	 * Entry to ignore in the folder
	 * @var int
	 */
	public $id_entry_folder;


	/**
	 * Filtrer les demandes necessitant ou pas un email aux approbateurs
	 * @var int
	 */
	public $appr_notified;

	/**
	 * Filtrer les demandes avec unes instance d'approbation
	 * @var bool
	 */
	public $idfai_set;


	/**
	 * Filtrer les demandes avec une liste d'instances d'approbation
	 * @var array
	 */
	public $appr_idfai;


	/**
	 * Search all request created before this date time
	 * @var string
	 */
	public $createdOn;

	/**
	 * Search all request modified before this date
	 * @var string
	 */
	public $modifiedOn;


	/**
	 * Datetime
	 * @var string
	 */
	public $from;


	/**
	 * Datetime
	 * @var string
	 */
	public $to;


	/**
	 * Filtrer les demandes par annee
	 * @var int
	 */
	public $year;


	/**
	 * Filtrer les demandes par organisation
	 * @var int
	 */
	public $organization;


	/**
	 * Filtrer les demandes par statut d'archivage
	 * @var int
	 */
	public $archived = 0;
	
	/**
	 * with at least one element with this right
	 */
	public $id_right;



	public function getObject($data)
	{

		$entry = new absences_Entry;
		$entry->setRow($data);
		return $entry;

	}
	
	/**
	 * @return string
	 */
	protected function getBaseQuery()
	{
	    return 'SELECT e.* 
				FROM
					absences_entries e 
				';
	
	}
	
	
	/**
	 * @return array
	 */
	protected function getWhere()
	{
	    global $babDB;
	    
	    $where = array();
	    if (isset($this->folder))
	    {
	        $where[] = 'e.folder='.$babDB->quote($this->folder);
	    
	        if (isset($this->id_entry_folder))
	        {
	            $where[] = 'e.id<>'.$babDB->quote($this->id_entry_folder);
	        }
	    }
	    
	    if (!empty($this->users))
	    {
	        $where[] = 'e.id_user IN('.$babDB->quote($this->users).')';
	    }
	    
	    if (isset($this->status))
	    {
	        $where[] = 'e.status IN('.$babDB->quote($this->status).')';
	    }
	    
	    if (isset($this->createdOn))
	    {
	        $where[] = 'e.createdOn<='.$babDB->quote($this->createdOn);
	    }
	    
	    if (isset($this->modifiedOn))
	    {
	        $where[] = 'e.`date`<='.$babDB->quote($this->modifiedOn);
	    }
	    
	    if (isset($this->appr_notified))
	    {
	        $where[] = 'e.appr_notified='.$babDB->quote($this->appr_notified);
	    }
	    
	    if (isset($this->idfai_set) && $this->idfai_set)
	    {
	        $where[] = 'e.idfai>'.$babDB->quote(0);
	    }
	    
	    if (isset($this->appr_idfai))
	    {
	        $where[] = 'e.idfai IN('.$babDB->quote($this->appr_idfai).')';
	    }
	    
	    if (isset($this->from))
	    {
	        $where[] = 'e.date_end >'.$babDB->quote($this->from);
	    }
	    
	    if (isset($this->to))
	    {
	        $where[] = 'e.date_begin <'.$babDB->quote($this->to);
	    }
	    
	    if (isset($this->startTo))
	    {
	        $where[] = 'e.date_begin <='.$babDB->quote($this->startTo);
	    }
	    
	    if (isset($this->startFrom))
	    {
	        $where[] = 'e.date_begin >='.$babDB->quote($this->startFrom);
	    }
	    
	    if (isset($this->year))
	    {
	        $where[] = 'YEAR(e.date_begin)='.$babDB->quote($this->year);
	    }
	    
	    if (isset($this->organization) && $this->organization)
	    {
	        $where[] = 'e.id_user=p.id_user';
	        $where[] = 'p.id_organization='.$babDB->quote($this->organization);
	    }
	    
	    if (isset($this->archived))
	    {
	        $where[] = 'e.archived='.$babDB->quote($this->archived);
	    }
	    	
	    if (isset($this->id_right))
	    {
	        $where[] = 'e.id IN(SELECT id_entry FROM absences_entries_elem WHERE id_right='.$babDB->quote($this->id_right).')';
	    }
	    
	    return $where;
	}



	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			$req = $this->getBaseQuery();

			$where = $this->getWhere();
			
			if (isset($this->organization) && $this->organization)
			{
			    $req .= ', absences_personnel p';
			}

			if ($where)
			{
				$req .= ' WHERE '.implode(' AND ', $where);
			}

			$req .= ' ORDER BY '.$this->orderby;
			
			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}
}


/**
 * Entry elements iterator joined on entries
 */
class absences_ElementEntryIterator extends absences_EntryIterator
{
    
    public function getObject($data)
    {
        require_once dirname(__FILE__).'/type.class.php';
        require_once dirname(__FILE__).'/right.class.php';
        require_once dirname(__FILE__).'/entry_elem.class.php';
        
        $entry_row = $this->getRowByPrefix($data, 'entry');
        $entry = new absences_Entry($entry_row['id']);
        $entry->setRow($entry_row);
    
        $type_row = $this->getRowByPrefix($data, 'type');
        $type = new absences_Type($type_row['id']);
        $type->setRow($type_row);
    
        $right_row = $this->getRowByPrefix($data, 'right');
        $right = new absences_Right($right_row['id']);
        $right->setRow($right_row);
        $right->setType($type);
    
        $elem_row = $this->getRowByPrefix($data, 'elem');
        $elem = new absences_EntryElem();
        $elem->setRow($elem_row);
        $elem->setRight($right);
        $elem->setEntry($entry);
    
        return $elem;
    }
    
    
    /**
     * @return string
     */
    protected function getBaseQuery()
    {
        return 'SELECT 
                
                    e.id                    entry__id,
                    e.id_user               entry__id_user,
                    e.createdby             entry__createdby,
                    e.date_begin            entry__date_begin,
                    e.date_end              entry__date_end,
                    e.idfai                 entry__idfai,
                    e.comment               entry__comment,
                    e.createdOn             entry__createdOn,
                    e.date                  entry__date,
                    e.status                entry__status,
                    e.comment2              entry__comment2,
                    e.id_approver           entry__id_approver,
                    e.folder                entry__folder,
                    e.creation_type         entry__creation_type,
                    e.appr_notified         entry__appr_notified,
                    e.archived              entry__archived,
                    e.todelete              entry__todelete,
                    e.firstconfirm          entry__firstconfirm,
                
                    ee.id 					elem__id,
    				ee.id_entry 			elem__id_entry,
    				ee.id_right				elem__id_right,
    				ee.quantity				elem__quantity,
    			    ee.date_begin           elem__date_begin,
    			    ee.date_end             elem__date_end,
    
    				r.id					right__id,
    				r.id_creditor			right__id_creditor,
    				r.kind					right__kind,
    			    r.createdOn			    right__createdOn,
    				r.date_entry			right__date_entry,
    				r.date_begin			right__date_begin,
    				r.date_end				right__date_end,
    				r.quantity				right__quantity,
    				r.quantity_unit			right__quantity_unit,
    				r.quantity_inc_month 	right__quantity_inc_month,
    				r.quantity_inc_max 		right__quantity_inc_max,
    				r.quantity_inc_last		right__quantity_inc_last,
    				r.id_type				right__id_type,
    				r.description			right__description,
    				r.active				right__active,
    				r.cbalance	 			right__cbalance,
    				r.date_begin_valid		right__date_begin_valid,
    				r.date_end_valid		right__date_end_valid,
    				r.date_end_fixed		right__date_end_fixed,
    				r.date_begin_fixed		right__date_begin_fixed,
                    r.hide_empty 		    right__hide_empty,
    				r.no_distribution 		right__no_distribution,
    				r.use_in_cet			right__use_in_cet,
    				r.id_rgroup				right__id_rgroup,
    				r.earlier				right__earlier,
    				r.earlier_begin_valid 	right__earlier_begin_valid,
    				r.earlier_end_valid		right__earlier_end_valid,
    				r.later					right__later,
    				r.later_begin_valid		right__later_begin_valid,
    				r.later_end_valid		right__later_end_valid,
    				r.delay_before			right__delay_before,
    				r.id_report_type		right__id_report_type,
    				r.date_end_report		right__date_end_report,
    				r.description_report	right__description_report,
    				r.id_reported_from		right__id_reported_from,
    				r.quantity_alert_days	right__quantity_alert_days,
    				r.quantity_alert_types	right__quantity_alert_types,
    				r.quantity_alert_begin	right__quantity_alert_begin,
    				r.quantity_alert_end	right__quantity_alert_end,
    				r.dynconf_types			right__dynconf_types,
    				r.dynconf_begin			right__dynconf_begin,
    				r.dynconf_end			right__dynconf_end,
    				r.sync_status			right__sync_status,
    				r.sync_update			right__sync_update,
    				r.uuid					right__uuid,
    				r.archived				right__archived,
    				r.sortkey				right__sortkey,
    				r.require_approval		right__require_approval,
    
    
    				t.id					type__id,
    				t.name					type__name,
    				t.description			type__description,
    				t.color					type__color
				FROM
					absences_entries e,
                    absences_entries_elem ee,
                    absences_rights r,
                    absences_types t 
				';
    
    }
    
    /**
     * @return array
     */
    protected function getWhere()
    {
        $where = parent::getWhere();
        
        $where[] = 'ee.id_entry=e.id';
        $where[] = 'ee.id_right=r.id';
        $where[] = 'r.id_type=t.id';
        
        return $where;
    }
}



/**
 * Entry elements iterator
 */
class absences_EntryElemIterator extends absences_Iterator
{
	/**
	 *
	 * @var absences_Entry
	 */
	public $entry;

	/**
	 * Filter by a list of id vaction types
	 * @var int[]
	 */
	public $types;


	public function getObject($data)
	{
		require_once dirname(__FILE__).'/type.class.php';
		require_once dirname(__FILE__).'/right.class.php';
		require_once dirname(__FILE__).'/entry_elem.class.php';

		$type_row = $this->getRowByPrefix($data, 'type');
		$type = new absences_Type($type_row['id']);
		$type->setRow($type_row);

		$right_row = $this->getRowByPrefix($data, 'right');
		$right = new absences_Right($right_row['id']);
		$right->setRow($right_row);
		$right->setType($type);

		$elem_row = $this->getRowByPrefix($data, 'elem');
		$elem = new absences_EntryElem();
		$elem->setRow($elem_row);
		$elem->setRight($right);
		$elem->setEntry($this->entry);

		return $elem;
	}



	public function executeQuery()
	{
		if(is_null($this->_oResult))
		{
			global $babDB;
			$req = 'SELECT

				ee.id 					elem__id,
				ee.id_entry 			elem__id_entry,
				ee.id_right				elem__id_right,
				ee.quantity				elem__quantity,
			    ee.date_begin           elem__date_begin,
			    ee.date_end             elem__date_end,

				r.id					right__id,
				r.id_creditor			right__id_creditor,
				r.kind					right__kind,
			    r.createdOn			    right__createdOn,
				r.date_entry			right__date_entry,
				r.date_begin			right__date_begin,
				r.date_end				right__date_end,
				r.quantity				right__quantity,
				r.quantity_unit			right__quantity_unit,
				r.quantity_inc_month 	right__quantity_inc_month,
				r.quantity_inc_max 		right__quantity_inc_max,
				r.quantity_inc_last		right__quantity_inc_last,
				r.id_type				right__id_type,
				r.description			right__description,
				r.active				right__active,
				r.cbalance	 			right__cbalance,
				r.date_begin_valid		right__date_begin_valid,
				r.date_end_valid		right__date_end_valid,
				r.date_end_fixed		right__date_end_fixed,
				r.date_begin_fixed		right__date_begin_fixed,
			    r.hide_empty 		    right__hide_empty,
				r.no_distribution 		right__no_distribution,
				r.use_in_cet			right__use_in_cet,
				r.id_rgroup				right__id_rgroup,
				r.earlier				right__earlier,
				r.earlier_begin_valid 	right__earlier_begin_valid,
				r.earlier_end_valid		right__earlier_end_valid,
				r.later					right__later,
				r.later_begin_valid		right__later_begin_valid,
				r.later_end_valid		right__later_end_valid,
				r.delay_before			right__delay_before,
				r.id_report_type		right__id_report_type,
				r.date_end_report		right__date_end_report,
				r.description_report	right__description_report,
				r.id_reported_from		right__id_reported_from,
				r.quantity_alert_days	right__quantity_alert_days,
				r.quantity_alert_types	right__quantity_alert_types,
				r.quantity_alert_begin	right__quantity_alert_begin,
				r.quantity_alert_end	right__quantity_alert_end,
				r.dynconf_types			right__dynconf_types,
				r.dynconf_begin			right__dynconf_begin,
				r.dynconf_end			right__dynconf_end,
				r.sync_status			right__sync_status,
				r.sync_update			right__sync_update,
				r.uuid					right__uuid,
				r.archived				right__archived,
				r.sortkey				right__sortkey,
				r.require_approval		right__require_approval,


				t.id					type__id,
				t.name					type__name,
				t.description			type__description,
				t.color					type__color
			FROM
				absences_entries_elem ee,
				absences_rights r,
				absences_types t

			WHERE
				ee.id_entry='.$babDB->quote($this->entry->id).'
				AND ee.id_right = r.id
				AND r.id_type = t.id';

			if (isset($this->types)) {
			    $req .= ' AND t.id IN('.$babDB->quote($this->types).')';
			}

		    $req .= ' ORDER BY ee.date_begin';

			$this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
		}
	}


}



/**
 * Entry periods iterator
 */
class absences_EntryPeriodIterator extends absences_Iterator
{
    /**
     *
     * @var absences_Entry
     */
    public $entry;



    public function getObject($data)
    {
        require_once dirname(__FILE__).'/entry_period.class.php';



        $elem_row = $this->getRowByPrefix($data, 'period');
        $elem = new absences_EntryPeriod();
        $elem->setRow($elem_row);
        $elem->setEntry($this->entry);

        return $elem;
    }



    public function executeQuery()
    {
        if(is_null($this->_oResult))
        {
            global $babDB;
            $req = 'SELECT

				ep.id 					period__id,
				ep.id_entry 			period__id_entry,
			    ep.date_begin           period__date_begin,
			    ep.date_end             period__date_end

			FROM
				absences_entries_periods ep
			WHERE
				ep.id_entry='.$babDB->quote($this->entry->id).'';


            $req .= ' ORDER BY ep.date_begin';

            $this->setMySqlResult($this->getDataBaseAdapter()->db_query($req));
        }
    }


}
