<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/statistics.class.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/statistics.ui.php';


/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

function absence_FilterStatistics()
{
    require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
    require_once dirname(__FILE__).'/utilit/organization.class.php';
    $resOrganization = new absences_OrganizationIterator();
    
    if (0 === $resOrganization->count()) {
        
        // nothing to filter on, redirect to statistics
        
        $url = bab_url::get_request('tg');
        $url->idx = 'types';
        $url->location();
    }
    
	$W = bab_Widgets();
	$page = $W->BabPage();
	$page->addStyleSheet(absences_Addon()->getStylePath().'vacation.css');

	$page->setTitle(absences_translate('Statistics'));

	$editor = new absences_StatisticsEditor();

	$page->addItem($editor);
	$page->displayHtml();
}



$idx = bab_rp('idx', "types");

switch($idx)
{
	case 'filter':
		absence_FilterStatistics();
		break;

	case 'types':
		new absences_StatisticsXls(bab_rp('filter', array()));
		die();
		break;
}