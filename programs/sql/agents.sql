TRUNCATE absences_personnel;
TRUNCATE absences_users_rights;

INSERT INTO absences_personnel (id, id_user, id_coll) 
VALUES 
('1', '1', '1'),
('2', '2', '1');


INSERT INTO absences_users_rights (id, id_user, id_right, quantity) 
VALUES 
('1', '1', '1', ''),
('2', '1', '2', ''),
('3', '2', '1', '2.33'),
('4', '2', '2', '2.33');