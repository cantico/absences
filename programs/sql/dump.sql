CREATE TABLE `absences_organization` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
);



CREATE TABLE `absences_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `monthkey` mediumint(6) unsigned NOT NULL,
  `cal_date` date NOT NULL,
  `ampm` tinyint(1) unsigned NOT NULL,
  `period_type` tinyint(3) unsigned NOT NULL,
  `id_entry` int(10) unsigned NOT NULL,
  `color` varchar(6) NOT NULL default '',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`,`monthkey`,`cal_date`)
);


CREATE TABLE `absences_collections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `id_cat` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_cat` (`id_cat`)
);




CREATE TABLE `absences_coll_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_coll` int(11) unsigned NOT NULL DEFAULT '0',
  `id_type` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `colltype` (`id_coll`, `id_type`)
);


CREATE TABLE `absences_coll_rights` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_coll` int(11) unsigned NOT NULL DEFAULT '0',
  `id_right` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `collright` (`id_coll`, `id_right`)
);




CREATE TABLE `absences_comanager` (
  `id_entity` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_entity`,`id_user`)
);



CREATE TABLE `absences_entries` (
  `id` 				int(11) 	unsigned 	NOT NULL AUTO_INCREMENT,
  `id_user` 		int(11) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT 'absence owner',
  `createdby` 		int(11) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT 'absence author',
  `date_begin` 		datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` 		datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00',
  `idfai` 			int(11) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT 'Approval instance',
  `comment` 		tinytext 				NOT NULL									COMMENT 'Appliquant comment',
  `createdOn` 		datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request creation date',
  `date` 			datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request last modification date',
  `status` 			char(1) 				NOT NULL DEFAULT ''							COMMENT 'empty : waiting approval, Y : confirmed, N : rejected, P : previsional',
  `comment2` 		tinytext 				NOT NULL									COMMENT 'last approbation action approver comment',
  `id_approver` 	int(11) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT 'last approbation action approver',
  `folder`	 		int(11) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT 'Folder unique ID for user requests list',
  `creation_type`	tinyint(1) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT '0:user requested, 1:fixed vacation',
  `appr_notified`	tinyint(1) 	unsigned 	NOT NULL DEFAULT '0'						COMMENT '0:approbateur pas notifie ou relance de email a effectuer, 1:approbateur notifie',
  `archived` 		tinyint(1)	 			NOT NULL DEFAULT '0',
  `todelete`        tinyint(1)              NOT NULL DEFAULT '0'                        COMMENT 'Delete after approval',
  `firstconfirm`    tinyint(1)              NOT NULL DEFAULT '0'                        COMMENT 'If the request has been confirmed once',
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `id_user` (`id_user`),
  KEY `idfai` (`idfai`),
  KEY `date_begin` (`date_begin`),
  KEY `date_end` (`date_end`),
  KEY `folder` (`folder`),
  KEY `creation_type` (`creation_type`)
);




CREATE TABLE `absences_entries_elem` (
  `id` 			int(11) 		unsigned NOT NULL AUTO_INCREMENT,
  `id_entry` 	int(11) 		unsigned NOT NULL DEFAULT '0',
  `id_right` 	int(11) 		unsigned NOT NULL DEFAULT '0',
  `quantity` 	decimal(8,2) 	unsigned NOT NULL DEFAULT '0.00',
  `date_begin`  datetime                 NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end`    datetime                 NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_entry` (`id_entry`),
  KEY `id_right` (`id_right`)
);



CREATE TABLE `absences_entries_periods` (
  `id`          int(11)         unsigned NOT NULL AUTO_INCREMENT,
  `id_entry`    int(11)         unsigned NOT NULL DEFAULT '0',
  `date_begin`  datetime                 NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end`    datetime                 NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_entry` (`id_entry`)
);




CREATE TABLE `absences_managers` (
  `id` 			int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` 	int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
);


CREATE TABLE `absences_managers_groups` (
   `id` 		int(11) unsigned NOT NULL auto_increment,
   `id_object` 	int(11) unsigned NOT NULL 	DEFAULT '0',
   `id_group` 	int(11) unsigned NOT NULL	DEFAULT '0',
   PRIMARY KEY (`id`),
   KEY `id_object` (`id_object`),
   KEY `id_group` (`id_group`)
);



CREATE TABLE `absences_options` (
  `chart_superiors_create_request` 	tinyint(1) unsigned NOT NULL DEFAULT '0',
  `chart_superiors_set_rights` 		tinyint(1) unsigned NOT NULL DEFAULT '1',
  `chart_superiors_user_edit` 		tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allow_mismatch` 					tinyint(1) unsigned NOT NULL DEFAULT '0',
  `workperiod_recover_request`		tinyint(1) unsigned NOT NULL DEFAULT '0',
  `display_personal_history`		tinyint(1) unsigned NOT NULL DEFAULT '0',
  `modify_confirmed`                tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Users can edit or delete approved requests',
  `modify_waiting`                  tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Users can edit or delete waiting requests',
  `email_manager_ondelete`			tinyint(1) unsigned NOT NULL DEFAULT '1',
  `approb_email_defer`				tinyint(1) unsigned NOT NULL DEFAULT '0',
  `entity_planning`					tinyint(1) unsigned NOT NULL DEFAULT '0',
  `entity_planning_display_types`   tinyint(1) unsigned NOT NULL DEFAULT '0',
  `approb_alert`					tinyint(3) unsigned NOT NULL DEFAULT '7' COMMENT 'in days',
  `auto_approval`                   tinyint(1) unsigned NOT NULL DEFAULT '1',
  `auto_confirm`					tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'in days',
  `sync_server`						tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sync_url`						varchar(255) 		NOT NULL DEFAULT '',
  `sync_nickname`					varchar(255) 		NOT NULL DEFAULT '',
  `sync_password`					varchar(255) 		NOT NULL DEFAULT '',
  `id_chart` 						int(11)    unsigned NOT NULL DEFAULT '0',
  `user_add_email`					tinyint(1) unsigned NOT NULL DEFAULT '0',
  `end_recup`						int(11)	   unsigned NOT NULL DEFAULT '365' COMMENT 'in days',
  `delay_recovery`					tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'in days',
  `maintenance`						tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archivage_day`					tinyint(2) unsigned NOT NULL DEFAULT '1',
  `archivage_month`					tinyint(2) unsigned NOT NULL DEFAULT '1',
  `public_calendar`					tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Activate the public calendar',
  `appliquant_email`				tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Send email to appliquant on confirmed request',
  `organization_sync`				tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Sync user organization with the one in directory entry'
);




CREATE TABLE `absences_personnel` (
  `id` 				int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` 		int(11) unsigned NOT NULL DEFAULT '0',
  `id_coll` 		int(11) unsigned NOT NULL DEFAULT '0',
  `id_sa` 			int(11) unsigned NOT NULL DEFAULT '0',
  `id_sa_cet` 		int(11) unsigned NOT NULL DEFAULT '0',
  `id_sa_recover` 	int(11) unsigned NOT NULL DEFAULT '0',
  `emails`			tinytext 		 NOT NULL	COMMENT 'comma separated values',
  `id_organization` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_coll` (`id_coll`),
  KEY `id_sa` (`id_sa`),
  KEY `id_organization` (`id_organization`)
);



CREATE TABLE `absences_planning` (
  `id_entity` 	int(10) unsigned NOT NULL DEFAULT '0',
  `id_user` 	int(10) unsigned NOT NULL DEFAULT '0',
  KEY `id_user` (`id_user`)
);





CREATE TABLE `absences_custom_planning` (
  `id`              int(11) unsigned    NOT NULL auto_increment,
  `name`            varchar(255)        NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
);

CREATE TABLE `absences_custom_planning_groups` (
   `id`         int(11) unsigned NOT NULL auto_increment,
   `id_object`  int(11) unsigned NOT NULL   DEFAULT '0',
   `id_group`   int(11) unsigned NOT NULL   DEFAULT '0',
   PRIMARY KEY (`id`),
   KEY `id_object` (`id_object`),
   KEY `id_group` (`id_group`)
);


CREATE TABLE `absences_custom_planning_users` (
    `id_planning`   int(10) unsigned NOT NULL DEFAULT '0',
    `id_user`       int(10) unsigned NOT NULL DEFAULT '0',
    UNIQUE KEY `pair` (`id_planning`, `id_user`)
);



CREATE TABLE `absences_public_planning_groups` (
   `id`         int(11) unsigned NOT NULL auto_increment,
   `id_object`  int(11) unsigned NOT NULL   DEFAULT '0',
   `id_group`   int(11) unsigned NOT NULL   DEFAULT '0',
   PRIMARY KEY (`id`),
   KEY `id_object` (`id_object`),
   KEY `id_group` (`id_group`)
);





CREATE TABLE `absences_rgroup` (
  `id` 				int(10) 	unsigned 		NOT NULL AUTO_INCREMENT,
  `name` 			varchar(255) 				NOT NULL default '',
  `quantity_unit`	enum('H','D') 				NOT NULL DEFAULT 'D',
  `recover`			tinyint(1) 	unsigned 		NOT NULL default '0' 	COMMENT 'Utiliser pour les droits a recuperation cree par le logiciel',
  `sortkey` 		int(11) 	unsigned 		NOT NULL default '0',
  PRIMARY KEY (`id`),
  KEY `sortkey` (`sortkey`)
);




CREATE TABLE `absences_rights` (
  `id` 					int(11) 	unsigned 	NOT NULL AUTO_INCREMENT,
  `id_creditor` 		int(11) 	unsigned 	NOT NULL default '0'					COMMENT 'Auteur du droit',
  `kind` 				int(11) 	unsigned 	NOT NULL DEFAULT '0',
  `createdOn`           datetime                NOT NULL DEFAULT '0000-00-00 00:00:00'  COMMENT 'date de creation',
  `date_entry` 			datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00'	COMMENT 'date de derniere mise a jour',
  `date_begin` 			date 					NOT NULL DEFAULT '0000-00-00' 			COMMENT 'Periode theorique du droit',
  `date_end` 			date 					NOT NULL DEFAULT '0000-00-00',
  `quantity` 			decimal(8,2) unsigned 	NOT NULL DEFAULT '0.00',
  `quantity_unit` 		enum('H','D') 			NOT NULL DEFAULT 'D',
  `quantity_inc_month` 	decimal(8,2) unsigned 	NOT NULL DEFAULT '0.00' 				COMMENT 'Quantite a ajouter a chaque debut de mois',
  `quantity_inc_max` 	decimal(8,2) unsigned 	NOT NULL DEFAULT '0.00' 				COMMENT 'Quantite a ne pas depasser lors de l ajout mensuel',
  `quantity_inc_last` 	datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00' 	COMMENT 'Date du dernier ajout automatique',
  `id_type` 			int(11) 	unsigned 	NOT NULL default '0',
  `description` 		varchar(255) 			NOT NULL default '',
  `active` 				enum('Y','N') 			NOT NULL DEFAULT 'Y'					COMMENT 'le gestionnaire seulement peut utiliser le droit s il est inactif',
  `cbalance` 			enum('Y','N') 			NOT NULL DEFAULT 'Y'					COMMENT 'Accepter les soldes negatifs',
  `date_begin_valid` 	date 					NOT NULL DEFAULT '0000-00-00' 			COMMENT 'Disponibilite en fonction de la date de saisie de la demande de conges',
  `date_end_valid` 		date 					NOT NULL DEFAULT '0000-00-00',
  `date_end_fixed` 		datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_begin_fixed` 	datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hide_empty`          tinyint(1)  unsigned    NOT NULL default '0'                    COMMENT 'Hide right if quantity <= 0',
  `no_distribution` 	tinyint(1) 	unsigned 	NOT NULL default '0' 					COMMENT 'Repartition automatique des jours lors de la demande',
  `use_in_cet` 			tinyint(1) 	unsigned 	NOT NULL default '1' 					COMMENT 'Autoriser utilisation dans les CET',
  `cet_quantity` 		decimal(8,2) unsigned 	NOT NULL DEFAULT '0.00'					COMMENT 'quantite epargnable dans les CET',
  `id_rgroup` 			int(10) 	unsigned 	NOT NULL default '0',
  
  `earlier` 			varchar(128) 			NOT NULL DEFAULT '' 					COMMENT 'directory field with a date, Disponibilite en fonction d une date anterieure',
  `earlier_begin_valid` tinyint(3) 	unsigned 	NOT NULL default '0' 					COMMENT 'Nombre d annees apres la date : debut',
  `earlier_end_valid` 	tinyint(3) 	unsigned 	NOT NULL default '0' 					COMMENT 'Nombre d annees apres la date : fin',
  
  `later` 				varchar(128) 			NOT NULL DEFAULT '' 					COMMENT 'directory field with a date, Disponibilite en fonction d une date posterieure',
  `later_begin_valid` 	tinyint(3) 	unsigned 	NOT NULL default '0'					COMMENT 'Nombre d annees avant la date : debut',
  `later_end_valid` 	tinyint(3) 	unsigned 	NOT NULL default '0'					COMMENT 'Nombre d annees avant la date : fin',
  
  `delay_before` 		tinyint(3) 	unsigned 	NOT NULL default '0'					COMMENT 'Nombre de jour entre la date courrante et la date de la debut de la demande',
  
  `id_report_type` 		int(11) 	unsigned 	NOT NULL default '0'					COMMENT 'Right type for auto created report rights',
  `date_end_report`		date 					NOT NULL DEFAULT '0000-00-00'			COMMENT 'Availability end for the auto created report right',
  `description_report`  varchar(255) 			NOT NULL default ''						COMMENT 'Right description for auto created report',
  `id_reported_from`	int(11) 	unsigned 	NOT NULL default '0'					COMMENT 'Origine du droit de report',
  
  `quantity_alert_days` int(11) 	unsigned 	NOT NULL default '0'					COMMENT 'Alerte en fonction du nombre de jours pris sur des types',
  `quantity_alert_types` varchar(255)			NOT NULL default ''						COMMENT 'Liste de types separes par des virgules',
  `quantity_alert_begin` date 					NOT NULL DEFAULT '0000-00-00'			COMMENT 'Alerte en fonction du nombre de jours pris sur des types',
  `quantity_alert_end` 	date 					NOT NULL DEFAULT '0000-00-00'			COMMENT 'Alerte en fonction du nombre de jours pris sur des types',
  
  `dynconf_types` 		varchar(255)			NOT NULL default ''						COMMENT 'Liste de types separes par des virgules',
  `dynconf_begin` 		date 					NOT NULL DEFAULT '0000-00-00'			COMMENT 'periode de test des jours dynamiques',
  `dynconf_end` 		date 					NOT NULL DEFAULT '0000-00-00'			COMMENT 'periode de test des jours dynamiques',
  
  `sync_status`			tinyint(1) 				NOT NULL DEFAULT '0'					COMMENT '0=pas de synchro | 1=syncho | 2=plus sur le serveur | 3=erreur lors de la mise a jour | 8=partage sur le serveur',
  `sync_update`		 	datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00'	COMMENT 'derniere mise a jour par synchro',
  `uuid`				varchar(255) 			NOT NULL default ''						COMMENT 'Right unique ID',
  `archived` 			tinyint(1) 				NOT NULL DEFAULT '0',
  `sortkey` 			int(11) 	unsigned 	NOT NULL default '0',
  `require_approval` 	tinyint(1) 				NOT NULL DEFAULT '1',
  
  `renewal_uid`         varchar(255)            NOT NULL default ''                     COMMENT 'Right renewal',
  `renewal_parent`      int(11)     unsigned    NOT NULL default '0'                    COMMENT 'Right ID of the right in the previous period',
  
  PRIMARY KEY (`id`),
  KEY `id_type` (`id_type`),
  KEY `date_entry` (`date_entry`),
  KEY `id_rgroup` (`id_rgroup`),
  KEY `archived` (`archived`),
  KEY `sortkey` (`sortkey`)
);



CREATE TABLE `absences_rights_inperiod` (
  `id` 				int(10) unsigned 	NOT NULL AUTO_INCREMENT,
  `id_right` 		int(10) unsigned 	NOT NULL DEFAULT '0',
  `period_start` 	date 				NOT NULL DEFAULT '0000-00-00' 	COMMENT 'Disponibilite en fonction de la periode de conges demandee',
  `period_end` 		date 				NOT NULL DEFAULT '0000-00-00',
  `right_inperiod` 	tinyint(4) 			NOT NULL DEFAULT '0'			COMMENT '1:dans la periode | 2:en dehors de la periode',
  `uuid`			varchar(255) 		NOT NULL default ''				COMMENT 'period unique ID',
  PRIMARY KEY (`id`),
  KEY `id_right` (`id_right`)
);




CREATE TABLE `absences_rights_rules` (
  `id` 					int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_right` 			int(10) unsigned NOT NULL DEFAULT '0',
  `validoverlap` 		tinyint(1) unsigned NOT NULL DEFAULT '0',
  `trigger_nbdays_min` 	float NOT NULL DEFAULT '0' COMMENT 'Attribution du droit en fonction des jours demandes et valides',
  `trigger_nbdays_max` 	float NOT NULL DEFAULT '0',
  `trigger_type` 		int(10) unsigned NOT NULL DEFAULT '0',
  `trigger_p1_begin` 	date NOT NULL DEFAULT '0000-00-00',
  `trigger_p1_end` 		date NOT NULL DEFAULT '0000-00-00',
  `trigger_p2_begin` 	date NOT NULL DEFAULT '0000-00-00',
  `trigger_p2_end` 		date NOT NULL DEFAULT '0000-00-00',
  `trigger_overlap` 	tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_right` (`id_right`),
  KEY `trigger_type` (`trigger_type`)
);




CREATE TABLE `absences_rights_cet` (
  `id` 					int(10) 	unsigned 	NOT NULL AUTO_INCREMENT,
  `id_right` 			int(10) 	unsigned 	NOT NULL DEFAULT '0',
  `saving_begin` 		date 					NOT NULL DEFAULT '0000-00-00',
  `saving_end` 			date 					NOT NULL DEFAULT '0000-00-00',
  `per_year` 			decimal(4,1) unsigned 	NOT NULL DEFAULT '0.0' COMMENT 'max days saving per years',
  `per_cet` 			decimal(4,1) unsigned 	NOT NULL DEFAULT '0.0' COMMENT 'max days saving on CET period',
  `ceiling` 			decimal(4,1) unsigned 	NOT NULL DEFAULT '0.0' COMMENT 'max days in CET',
  `min_use` 			decimal(4,1) 			NOT NULL DEFAULT '0.0' COMMENT 'min day in CET usage -1 to force using all days in CET',
  PRIMARY KEY (`id`),
  KEY `id_right` (`id_right`)
);




CREATE TABLE `absences_types` (
  `id` 			int(11) 		unsigned 	NOT NULL AUTO_INCREMENT,
  `name` 		varchar(100) 				NOT NULL DEFAULT '',
  `description` tinytext 					NOT NULL,
  `quantity` 	decimal(3,1) 				NOT NULL DEFAULT '0.0',
  `maxdays` 	decimal(3,1) 				NOT NULL DEFAULT '0.0',
  `mindays` 	decimal(3,1) 				NOT NULL DEFAULT '0.0',
  `defaultdays` decimal(3,1) 				NOT NULL DEFAULT '0.0',
  `color` 		varchar(6) 					NOT NULL DEFAULT '',
  `cbalance` 	enum('Y','N') 				NOT NULL DEFAULT 'Y',
  `recover`		tinyint(1) 		unsigned 	NOT NULL default '0' 	COMMENT 'Utiliser pour les droits a recuperation cree par le logiciel',
  PRIMARY KEY (`id`)
);





CREATE TABLE `absences_users_rights` (
  `id` 					int(11) 	unsigned	NOT NULL AUTO_INCREMENT,
  `id_user` 			int(11) 	unsigned 	NOT NULL DEFAULT '0',
  `id_right` 			int(11) 	unsigned 	NOT NULL DEFAULT '0',
  `quantity` 			char(5) 				NOT NULL DEFAULT ''				COMMENT 'surcharge des parametres du droit',
  `date_begin_valid` 	date 					NOT NULL DEFAULT '0000-00-00' 	COMMENT 'surcharge des parametres du droit',
  `date_end_valid` 		date 					NOT NULL DEFAULT '0000-00-00'	COMMENT 'surcharge des parametres du droit',
  `inperiod_start` 		date 					NOT NULL DEFAULT '0000-00-00' 	COMMENT 'surcharge des parametres du droit',
  `inperiod_end` 		date 					NOT NULL DEFAULT '0000-00-00'	COMMENT 'surcharge des parametres du droit',
  `validoverlap` 		tinyint(1) 	unsigned 	NOT NULL DEFAULT '0'			COMMENT 'surcharge des parametres du droit',
  `saving_begin` 		date 					NOT NULL DEFAULT '0000-00-00'	COMMENT 'surcharge des parametres du CET',
  `saving_end` 			date 					NOT NULL DEFAULT '0000-00-00'	COMMENT 'surcharge des parametres du CET',
  `renewal`             tinyint(1)  unsigned    NOT NULL default '1'            COMMENT 'Use this right in the next renewal by year',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_right` (`id_user`, `id_right`) 
);



CREATE TABLE `absences_users_rights_history` (
  `id`                  int(11)     unsigned    NOT NULL AUTO_INCREMENT,
  `id_user_right`       int(11)     unsigned    NOT NULL DEFAULT '0',
  `linkexists`          tinyint(1)  unsigned    NOT NULL default '0'            COMMENT 'link exists before the date',
  `quantity`            char(5)                 NOT NULL DEFAULT ''             COMMENT 'quantity before the date, if empty, use the right quantity',
  `date_end`            datetime                NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_user_right` (`id_user_right`) 
);



CREATE TABLE `absences_increment_right` (
  `id`                  int(11)     unsigned    NOT NULL AUTO_INCREMENT,
  `id_right`            int(11)     unsigned    NOT NULL DEFAULT '0'            COMMENT 'lien vers absences_rights.id',
  `quantity`            decimal(8,2)            NOT NULL DEFAULT '0.00'         COMMENT 'quantite dynamiquement ajoutee a la quantite, dans la meme unite que le droit associe',
  `monthkey`            int(6)      unsigned    NOT NULL DEFAULT '0',
  `createdOn`           datetime                NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `right_entry` (`id_right`, `monthkey`) 
);



CREATE TABLE `absences_increment_user_right` (
  `id`                  int(11)     unsigned    NOT NULL AUTO_INCREMENT,
  `id_user_right`       int(11)     unsigned    NOT NULL DEFAULT '0'            COMMENT 'lien vers absences_users_rights.id',
  `quantity`            decimal(8,2)            NOT NULL DEFAULT '0.00'         COMMENT 'quantite dynamiquement ajoutee a la quantite, dans la meme unite que le droit associe',
  `monthkey`            int(6)      unsigned    NOT NULL DEFAULT '0',
  `createdOn`           datetime                NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `right_entry` (`id_user_right`, `monthkey`) 
);


CREATE TABLE `absences_dynamic_rights` (
  `id` 					int(11) 	unsigned	NOT NULL AUTO_INCREMENT,
  `id_user_right` 		int(11) 	unsigned 	NOT NULL DEFAULT '0'			COMMENT 'lien vers absences_users_rights.id droit correspondant au solde modifie (parametrage de l''alerte)',
  `quantity` 			decimal(8,2)  			NOT NULL DEFAULT '0.00' 		COMMENT 'Quantite dynamiquement ajoutee au solde, dans la meme unite que le droit associe',
  `id_entry` 			int(11) 	unsigned 	NOT NULL DEFAULT '0'			COMMENT 'lien vers la demande qui a provoque l''ajout',
  `createdOn` 			datetime 				NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `right_entry` (`id_user_right`, `id_entry`) 
);



CREATE TABLE `absences_dynamic_configuration` (
  `id` 					int(11) 	unsigned	NOT NULL AUTO_INCREMENT,
  `id_right` 			int(11) 	unsigned 	NOT NULL DEFAULT '0'			COMMENT 'lien vers absences_rights.id droit correspondant au solde modifie (parametrage de l''alerte)',
  `test_quantity` 		decimal(8,2) unsigned  	NOT NULL DEFAULT '0.00' 		COMMENT 'quantite qui declenche le versement calcule sur le total des types parametres dans l''alerte, toujours en jours',
  `quantity` 			decimal(8,2)  			NOT NULL DEFAULT '0.00' 		COMMENT 'Quantite a ajouter/retirer sur le solde, dans la meme unite que le droit associe',
  PRIMARY KEY (`id`)
);




CREATE TABLE `absences_workperiod_recover_request` (
  `id` 					int(11) 		unsigned 	NOT NULL AUTO_INCREMENT,
  `id_user` 			int(11) 		unsigned 	NOT NULL DEFAULT '0',
  `date_begin` 			datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` 			datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_type`				int(11) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT 'Link to absences_workperiod_type',
  `idfai` 				int(11) 		unsigned 	NOT NULL DEFAULT '0',
  `comment` 			tinytext 					NOT NULL									COMMENT 'reason for the request',
  `createdOn` 			datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request creation date',
  `modifiedOn` 			datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request last modification date',
  `status` 				char(1) 					NOT NULL DEFAULT ''							COMMENT 'empty : waiting approval, Y : confirmed, N : rejected',
  `comment2` 			tinytext 					NOT NULL									COMMENT 'approver comment',
  `id_approver` 		int(11) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT 'last approbation action approver',
  `appr_notified`		tinyint(1) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT '0:approbateur pas notifie ou relance de email a effectuer, 1:approbateur notifie',
  `id_right`			int(11) unsigned 			NOT NULL DEFAULT '0'						COMMENT 'Right created on approval',
  `quantity` 			decimal(8,2) 	unsigned 	NOT NULL DEFAULT '0.00',
  `quantity_unit` 		enum('H','D') 				NOT NULL DEFAULT 'D',
  `validity_end`		date	 					NOT NULL DEFAULT '0000-00-00'				COMMENT 'Validity end of the created right',
  `archived` 			tinyint(1)	 				NOT NULL DEFAULT '0',
  `todelete`            tinyint(1)                  NOT NULL DEFAULT '0'                        COMMENT 'Delete after approval',
  `firstconfirm`        tinyint(1)                  NOT NULL DEFAULT '0'                        COMMENT 'If the request has been confirmed once',
  PRIMARY KEY (`id`),
  KEY `modifiedOn` (`modifiedOn`),
  KEY `id_user` (`id_user`),
  KEY `idfai` (`idfai`)
);


CREATE TABLE `absences_workperiod_type` (
  `id` 				int(11) 		unsigned 	NOT NULL AUTO_INCREMENT,
  `name` 			varchar(100) 				NOT NULL DEFAULT '',
  `quantity` 		decimal(8,2) 	unsigned 	NOT NULL DEFAULT '0.00',
  `quantity_unit` 	enum('H','D') 				NOT NULL DEFAULT 'D',
  PRIMARY KEY (`id`)
);



CREATE TABLE `absences_cet_deposit_request` (
  `id` 						int(11) 		unsigned 	NOT NULL AUTO_INCREMENT,
  `id_user` 				int(11) 		unsigned 	NOT NULL DEFAULT '0',
  `id_agent_right_cet` 		int(11) 		unsigned	NOT NULL DEFAULT '0'						COMMENT 'deposit CET agent right',
  `id_agent_right_source`	int(11) 		unsigned	NOT NULL DEFAULT '0'						COMMENT 'source agent right',
  `quantity` 				decimal(8,2) 	unsigned 	NOT NULL DEFAULT '0.00'						COMMENT 'Same unit as id_agent_right_cet AND id_agent_right_source',
  `idfai` 					int(11) 		unsigned 	NOT NULL DEFAULT '0',
  `comment` 				tinytext 					NOT NULL									COMMENT 'reason for the request',
  `createdOn` 				datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request creation date',
  `modifiedOn` 				datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request last modification date',
  `status` 					char(1) 					NOT NULL DEFAULT ''							COMMENT 'empty : waiting approval, Y : confirmed, N : rejected',
  `comment2` 				tinytext 					NOT NULL									COMMENT 'approver comment',
  `id_approver` 			int(11) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT 'last approbation action approver',
  `appr_notified`			tinyint(1) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT '0:approbateur pas notifie ou relance de email a effectuer, 1:approbateur notifie',
  `archived` 				tinyint(1)	 				NOT NULL DEFAULT '0',
  `todelete`                tinyint(1)                  NOT NULL DEFAULT '0'                        COMMENT 'Delete after approval',
  `firstconfirm`            tinyint(1)                  NOT NULL DEFAULT '0'                        COMMENT 'If the request has been confirmed once',
  PRIMARY KEY (`id`),
  KEY `modifiedOn` (`modifiedOn`),
  KEY `id_user` (`id_user`),
  KEY `idfai` (`idfai`)
);


CREATE TABLE `absences_movement` (
  `id` 					int(11) 		unsigned 	NOT NULL AUTO_INCREMENT,
  `id_user` 			int(11) 		unsigned 	NOT NULL DEFAULT '0'						COMMENT	'Agent may not exists',
  `id_right`	 		int(11) 		unsigned	NOT NULL DEFAULT '0'						COMMENT 'Right may not exists',
  `id_request`	 		int(11) 		unsigned	NOT NULL DEFAULT '0'						COMMENT 'Request may not exists',
  `request_class` 		varchar(100) 				NOT NULL DEFAULT ''							COMMENT 'absences_Entry | absences_WorkperiodRecoverRequest | absences_CetDepositRequest',
  `id_author`			int(11) 		unsigned	NOT NULL DEFAULT '0'						COMMENT 'source right',
  `comment` 			tinytext 					NOT NULL									COMMENT 'regularization comment from author',
  `createdOn` 			datetime 					NOT NULL DEFAULT '0000-00-00 00:00:00' 		COMMENT 'request creation date',
  `message` 			tinytext 					NOT NULL							 		COMMENT 'auto generated message',
  `status`              char(1)                              DEFAULT null                       COMMENT 'empty : waiting approval, Y : confirmed, N : rejected, P : previsional',
  PRIMARY KEY (`id`),
  KEY `createdOn` (`createdOn`),
  KEY `id_user` (`id_user`),
  KEY `id_right` (`id_right`)
);
