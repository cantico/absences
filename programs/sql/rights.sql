TRUNCATE absences_rights;

INSERT INTO absences_rights (id, description, quantity, quantity_unit, id_type, sortkey) 
VALUES 
(
    1,
    'Test right',
    '25.00',
    'D',
    1,
    1
);



INSERT INTO absences_rights (id, createdOn, description, quantity, quantity_unit, id_type, kind, quantity_inc_month, sortkey) 
VALUES 
(
    '2',
    '2015-01-01 00:00:00',
    'Test right, monthly update',
    '0.00',
    'D',
    '1',
    '8',
    '0.25',
    2
);



INSERT INTO absences_rights (id, description, quantity, quantity_unit, id_type, sortkey) 
VALUES 
(
    '3',
    'Test right 3',
    '10.00',
    'D',
    '1',
    '1'
);