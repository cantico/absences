<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/organization.class.php';
include_once dirname(__FILE__).'/utilit/exportentry.class.php';
bab_Widgets()->includePhpClass('Widget_Form');




class absences_exportVacationRequestsCls
{
    public $datebegintxt;
    public $dateendtxt;
    public $dateformattxt;
    public $statustxt;
    public $separatortxt;
    public $other;
    public $comma;
    public $tab;
    public $semicolon;
    public $export;
    public $sepdectxt;

    public $t_yes;
    public $t_no;
    public $additional_fields;
    public $t_users_without_requests;
    public $t_organization;
    public $t_overlap;
    public $t_splittype;

    public $organization;
    public $id_organization;

    private $resOrganization;
    private $dirfields;

    public function __construct()
    {
        $this->datebegintxt = absences_translate("Begin date");
        $this->dateendtxt = absences_translate("End date");
        $this->dateformattxt = "( ".absences_translate("dd-mm-yyyy")." )";
        $this->statustxt = absences_translate("Status");
        $this->separatortxt = absences_translate("Separator");
        $this->other = absences_translate("Other");
        $this->comma = absences_translate("Comma");
        $this->tab = absences_translate("Tab");
        $this->semicolon = absences_translate("Semicolon");
        $this->export = absences_translate("Export");
        $this->sepdectxt = absences_translate("Decimal separator");
        $this->t_yes = absences_translate("Yes");
        $this->t_no = absences_translate("No");
        $this->additional_fields = absences_translate("Additional fields to export:");
        $this->t_users_without_requests = absences_translate("Include uers without requests on the period");
        $this->t_splittype = absences_translate("Split requests by types");
        $this->t_organization = absences_translate('Organization');

        $this->t_overlap = absences_translate("Quantities and dates of requests are modified to fit in the export period");

        $W = bab_Widgets();
        $Canvas = $W->HtmlCanvas();

        $this->widgetdateb = $W->DatePicker()->setName('dateb')->display($Canvas);
        $this->widgetdatee = $W->DatePicker()->setName('datee')->display($Canvas);

        $this->statarr = array(absences_translate("Waiting"), absences_translate("Accepted"), absences_translate("Refused"));

        $this->dirfields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

        unset($this->dirfields['sn']);
        unset($this->dirfields['givenname']);
        unset($this->dirfields['jpegphoto']);

        $this->resOrganization = new absences_OrganizationIterator();
        $this->useorg = $this->resOrganization->count();
        $this->resOrganization->rewind();
    }

    public function getnextstatus()
    {
        static $i = 0;
        if( $i < count($this->statarr))
        {
            $this->statusid = $i;
            $this->statusname = $this->statarr[$i];
            $i++;
            return true;
        }
        else
            return false;
    }

    /**
     * template method to list available organization
     */
    public function getnextorganization()
    {
        	
        if ($this->resOrganization->valid())
        {
            $organization = $this->resOrganization->current();
            $this->organization = bab_toHtml($organization->name);
            $this->id_organization = bab_toHtml($organization->id);
            $this->resOrganization->next();
            return true;
        }

        return false;
    }


    public function getnextfield()
    {
        if (list($name,$arr) = each($this->dirfields))
        {
            $this->fieldname = bab_toHtml($name);
            $this->fieldlabel = bab_toHtml($arr['name']);
            return true;
        }
        return false;
    }
}



function exportVacationRequests()
{
	global $babBody;
	

	$temp = new absences_exportVacationRequestsCls();
	$babBody->babecho(absences_addon()->printTemplate($temp, "exportvac.html", "reqexport"));
}










class absences_SageExportEditor extends Widget_Form
{
	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));

		$this->setName('sage');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(70,'em'));


		$this->addFields();
		$this->addButtons();
		$this->setSelfPageHiddenFields();

		if (isset($_POST['sage']))
		{
			$this->setValues(array('sage' => $_POST['sage']));
		} else {

		    $default = array('sage' => array('types' => array()));

		    // set default values
		    global $babDB;

		    //TRANSLATORS: filter by name for vacation types availables in sage export
		    $res = $babDB->db_query("SELECT id FROM absences_types WHERE name LIKE '%".$babDB->db_escape_like(absences_translate('paid'))."%'");
		    while($arr = $babDB->db_fetch_assoc($res)) {
		        $default['sage']['types'][] = $arr['id'];
		    }
		    $this->setValues($default);
		}
	}


	protected function addFields()
	{
		$W = bab_Widgets();

		$this->addItem(
				$W->LabelledWidget(
						absences_translate('Export vacations on period'),
						$W->PeriodPicker()->setMandatory(true, absences_translate('The period is mandatory')),
						'period'
				)
		);


		$select = $W->Select();
		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);
		foreach($fields as $name => $arr)
		{
			$select->addOption($name, $arr['name']);

			if (false !== mb_strpos(mb_strtolower($arr['name']), 'matricule'))
			{
				$select->setValue($name);
			}
		}

		$this->addItem(
				$W->LabelledWidget(
						//TRANSLATORS: Matricule Sage directory field
						absences_translate('sage user number in'),
						$select,
						'user_number'
				)
		);

		/*
		$separator = $W->Frame()
			->addItem(
					$W->LabelledWidget(
							absences_translate('Separator'),
							$select_format = $W->Select()
							->addOption(';', absences_translate('Semicolon'))
							->addOption(',', absences_translate('Comma'))
							->addOption('{tab}', absences_translate('Tab')),
							'separator'
					)
			);


		$this->addItem(
				$W->LabelledWidget(
						absences_translate('Format'),
						$W->Select()
							->addOption('sage', absences_translate('Sage text'))
							->addOption('csv', absences_translate('CSV'))
							->setAssociatedDisplayable($separator, array('csv')),
						'format'
				)
		);


		$this->addItem($separator);
		*/

        $this->addItem($this->types());
        
        if ($org = $this->organization()) {
            $this->addItem($org);
        }

        $this->addItem($W->Label(absences_translate("Quantities are modified to fit in the export period according to the selected types (dates are not modified)")));

	}

	
	protected function organization()
	{
	    $W = bab_Widgets();
	
	    $select = $W->Select()->addClass('absences-select');
	    $select->addOption('', '');
	    
	    $res = new absences_OrganizationIterator();
	    
	    if (0 === $res->count()) {
	        return null;
	    }
	    
	    foreach ($res as $organization) {
	        $select->addOption($organization->id, $organization->name);
	    }
	
	    return $W->LabelledWidget(
	        absences_translate('Organization'),
	        $select,
	        __FUNCTION__
	    );
	}


	protected function types()
	{
	    $W = bab_Widgets();
	    global $babDB;

	    $select = $W->Multiselect();
	    $select->setHeadersDisplay();

	    $res = $babDB->db_query('SELECT * FROM absences_types ORDER BY name');
	    while($arr = $babDB->db_fetch_assoc($res)) {
	        $select->addOption($arr['id'], $arr['name']);
	    }

	    return $W->LabelledWidget(
    	    absences_translate('Vacation type'),
    	    $select,
	        'types'
    	);
	}





	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
				$W->SubmitButton()->setName('export')->setLabel(absences_translate('Export'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}






class absences_sageExportFile
{
	const OUTPUT_CHARSET = 'CP1252';


	/**
	 * Datetime
	 * @var string
	 */
	public $from;

	/**
	 * Datetime
	 * @var string
	 */
	public $to;


	/**
	 * Array of id vacation types
	 * @var array
	 */
	public $types;
	
	
	/**
	 * Filter by organization
	 * @var int
	 */
	public $organization;


	/**
	 * Directory entry field name
	 * @var string
	 */
	public $user_number_fieldname;

	/**
	 *
	 * @var string
	 */
	public $separator;

	/**
	 *
	 * @var unknown_type
	 */
	private $res;

	
	public function init() {

	    global $babDB;
	    
	    $query = '
				SELECT
					p.id_user,
					u.lastname,
					u.firstname
				FROM
					absences_personnel p,
					bab_users u
				WHERE
					u.id=p.id_user
		';
	    
	    if ($this->organization > 0) {
	        $query .= ' AND p.id_organization='.$babDB->quote($this->organization);
	    }
	    
	    $query .= ' ORDER BY u.lastname, u.firstname';
	    
	    $this->res = $babDB->db_query($query);
	}



	/**
	 * @return array
	 */
	private function getEntries($id_user)
	{
		require_once dirname(__FILE__).'/utilit/entry.class.php';

		$I = new absences_EntryIterator();
		$I->users = array($id_user);
		$I->status = 'Y';
		$I->from = $this->from;
		$I->to = $this->to;

		$quantity = 0.0;
		$periods = array();
		foreach($I as $entry)
		{
			/*@var $entry absences_Entry */

		    $eeI = $entry->getElementsIterator();
		    $eeI->types = $this->types;

		    $entry_quantity = 0.0;

		    foreach($eeI as $element) {
			    /* @var $element absences_EntryElem */

		        $right = $element->getRight();
		        if (!isset($right)) {
		            continue;
		        }

		        $type = $right->getType();
		        if (!isset($type)) {
		            continue;
		        }

		        if (!in_array($type->id, $this->types)) {
		            continue;
		        }

		        // ici on recupere la quantite de l'element tronque avec les dates
		        // de la periode demandee
		        $elem_quantity = $element->getPlannedQuantityBetween($this->from, $this->to);

		        if ('H' === $right->quantity_unit) {
		            // l'export sage contient des jours uniqument

		            $elem_quantity = ($elem_quantity/8);
		        }

    			$entry_quantity += $elem_quantity;
    			
    			
    			// dans la liste des periodes on affiche les periodes des types selectionnes
    			// les dates ne peuvent depasser par rapport au mois demande
    			
    			
    			$elem_from = $element->date_begin > $this->from ? $element->date_begin : $this->from;
    			$elem_to = $element->date_end < $this->to ? $element->date_end : $this->to;
    			
    			$periods[] = array(
    			    'from' 	=> date('d/m/y', bab_mktime($elem_from)),
    			    'to' 	=> date('d/m/y', bab_mktime($elem_to))
    			);
		    }

		    if ($entry_quantity > 0.001) {

    		    // la quantite totale des demandes du mois doit contenir que les conges
    		    // payes, uniquement pour le mois demande

    		    $quantity += $entry_quantity;
		    }
		}


		return array($quantity, $periods);
	}


	public function getnext()
	{
		global $babDB;

		if ($arr = $babDB->db_fetch_assoc($this->res))
		{
			$direntry = bab_admGetDirEntry($arr['id_user'], BAB_DIR_ENTRY_ID_USER);
			$arr['user_number'] = $direntry[$this->user_number_fieldname]['value'];

			list($quantity, $periods) = $this->getEntries($arr['id_user']);

			$arr['quantity'] = round($quantity, 1);
			$arr['periods'] = $periods;

			return $arr;
		}
	}


	private function setHeaders($extension, $mime)
	{
		$filename = mb_substr($this->from, 0, 10).'_'.mb_substr($this->to, 0, 10);

		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: public");
		header('Content-Disposition: attachment; filename="'.$filename.$extension.'"'."\n");
		header('Content-Type: '.$mime."\n");
		header('Content-transfert-encoding: binary'."\n");
	}


	public function downloadSage()
	{
		$this->setHeaders('.txt', 'text/plain');

		while($arr = $this->getnext())
		{
			$p = '';
			foreach($arr['periods'] as $dates)
			{
				$p .= $dates['from'].$dates['to'];
			}


			echo str_pad('VB'.$arr['user_number'], 446);
			echo str_pad(number_format($arr['quantity'], 1, ',', ''), 50);
			echo str_pad('0', 11);
			echo str_pad($p, 432);

			echo "\r\n";
		}
	}


	private function csvEncode($value)
	{
		return '"'.str_replace('"', '""', $value).'"';
	}



	public function downloadCsv()
	{
		$this->setHeaders('.csv', 'text/csv');

		while($arr = $this->getnext())
		{
			$p = '';
			foreach($arr['periods'] as $dates)
			{
				$p .= implode(' ', $dates).'-';
			}

			$row = array(
				$this->csvEncode($arr['user_number']),
				$this->csvEncode(number_format($arr['quantity'], 1, ',', '')),
				$this->csvEncode(bab_convertStringFromDatabase($arr['lastname'].' '.$arr['firstname'], self::OUTPUT_CHARSET)),
				$this->csvEncode($p)
			);

			echo implode($this->separator, $row)."\n";
		}
	}
}







function absences_sageExport()
{
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	$page->addStyleSheet(absences_addon()->getStylePath().'vacation.css');


	if (isset($_POST['sage']))
	{
		$values = $_POST['sage'];
		$datePicker = $W->DatePicker();

		$file = new absences_sageExportFile;
		$file->from = $datePicker->getISODate($values['period']['from']).' 00:00:00';
		$file->to = $datePicker->getISODate($values['period']['to']).' 23:59:59';
		$file->user_number_fieldname = $values['user_number'];
		$file->separator = isset($values['separator']) ? $values['separator'] : ',';
		$file->types = $values['types'];
		$file->organization = (int) $values['organization'];
		
		$file->init();

		$format = isset($values['format']) ? $values['format'] : 'sage';

		switch($format)
		{
			case 'csv':
				$file->downloadCsv();
				die();

			case 'sage':
			    $file->downloadSage();
			    die();
		}
	}


	$editor = new absences_SageExportEditor();

	$page->setTitle(absences_translate('Export vacations requests for sage'));
	$page->addItem($editor);
	$page->displayHtml();
}






/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

$idx = bab_rp('idx', "reqx");



if( isset($_POST['bexport']))
{
    $W = bab_Widgets();
    $datePicker = $W->DatePicker();
    $dateb = BAB_DateTime::fromIsoDateTime($datePicker->getISODate($_POST['dateb']).' 00:00:00');
    $datee = BAB_DateTime::fromIsoDateTime($datePicker->getISODate($_POST['datee']).' 00:00:00');
    $datee->add(1, BAB_DATETIME_DAY);

	$export = new absences_ExportEntry(
	    $dateb, $datee, 
	    $_POST['idstatus'], $_POST['wsepar'], $_POST['separ'], $_POST['sepdec'], 
	    $_POST['users_without_requests'], bab_pp('dirfields', array()), bab_pp('organization', ''), bab_pp('splittype'));
	$export->output();
}


switch($idx)
{

	case 'sage':
		absences_sageExport();
		$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
		$babBody->addItemMenu("sage", absences_translate("Sage"), absences_addon()->getUrl()."exportvac&idx=sage");
		break;


	case "reqx":

		$babBody->setTitle(absences_translate("Export vacations requests"));
		exportVacationRequests();
		$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
		$babBody->addItemMenu("reqx", absences_translate("Export"), absences_addon()->getUrl()."exportvac&idx=reqx");
		break;
}

$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');