<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

include_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath']."utilit/wfincl.php";
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
include_once dirname(__FILE__)."/utilit/vacincl.php";
include_once dirname(__FILE__)."/utilit/entry.class.php";
include_once dirname(__FILE__)."/utilit/request.ui.php";





class absences_approbEntryDetail extends absences_requestDetail
{
	public $datebegintxt;
	public $datebegin;
	public $halfnamebegin;
	public $dateendtxt;
	public $dateend;
	public $halfnameend;
	public $nbdaystxt;
	public $typename;
	public $nbdays;
	public $totaltxt;
	public $totalval;
	public $confirm;
	public $refuse;
	public $fullname;
	public $commenttxt;
	public $remarktxt;
	public $remark;

	public $arr = array();
	public $db;
	public $count;
	public $res;
	public $veid;

	public $folder;
	
	
	public $t_alert;
	public $t_nomatch;
	public $t_folder;
	public $t_confirm_folder;
	public $t_createdby;
	
	/**
	 * @var bool|string
	 */
	public $createdby;

	/**
	 * @var bool|string
	 */
	public $todelete;
	
	
	public $begin;
	public $end;
	public $id_user;
	public $totaldates_days;
	public $totaldates_hours;
	public $availability;
	public $negative;
	public $nomatch;
	public $alert;
	public $typecolor;
	public $rightname;
	public $date;
	

	public function __construct(absences_Entry $entry)
	{
		parent::__construct($entry, true);
		
		require_once dirname(__FILE__).'/utilit/agent.ui.php';

		$this->datebegintxt = absences_translate("Begin date");
		$this->dateendtxt = absences_translate("End date");
		$this->nbdaystxt = absences_translate("Quantities");
		$this->totaltxt = absences_translate("Total");
		$this->commenttxt = absences_translate("Approver comment");
		
		$this->refuse = absences_translate("Refuse");
		$this->remarktxt = absences_translate("Applicant comment");
		$this->t_alert = absences_translate("Negative balance");
		$this->t_nomatch = absences_translate("The length of the period is different from the requested vacation");
		$this->t_folder = absences_translate("Other dates of the recurring request");
		$this->t_confirm_folder = absences_translate("Apply the same choice on all recurring request periods");
		$this->t_createdby = absences_translate("Created by");
		
		if ($entry->createdby == $entry->id_user)
		{
			$this->createdby = false;
		} else if ($entry->createdby) {
			$this->createdby = bab_toHtml(bab_getUserName($entry->createdby));
		} else {
		    $this->createdby = false;
		}
		
		$this->confirm = absences_translate("Approve");

		$this->todelete = false;
		if ($entry->todelete) {
		    $this->confirm = absences_translate("Confirm deletion");
		    $this->todelete = absences_translate('Deletion request');
		}
		
		$this->begin 		= bab_mktime($entry->date_begin);
		$this->end 			= bab_mktime($entry->date_end);
		$this->datebegin	= bab_toHtml(absences_longDate($this->begin));
		$this->dateend		= bab_toHtml(absences_longDate($this->end));
		$this->id_user		= $entry->id_user;
		$this->fullname		= bab_toHtml(bab_getUserName($entry->id_user));
		$this->remark 		= bab_toHtml($entry->comment, BAB_HTML_ALL);

        // here we do not use the planned duration to verify 
        // if a workschedule modification has been made beetween the 
        // request creation and the validation step
		$this->totaldates_days = $entry->getDurationDays();
		$this->totaldates_hours = $entry->getDurationHours();

		if (0 === (int) round(100 * $this->totaldates_hours)) {
			// pas d'heures travaillees
			$this->availability = sprintf(absences_translate('%s in period'), absences_quantity($this->totaldates_days, 'D'));
		} else {
			$this->availability = sprintf(absences_translate('%s or %s in period'), absences_quantity($this->totaldates_days, 'D'), absences_quantity($this->totaldates_hours, 'H'));
		}

		$rights = absences_getRightsOnPeriod($entry->date_begin, $entry->date_end, $entry->id_user);
		$this->negative = array();
		foreach ($rights as $r)
		{
			$after = $r['quantity_available'] - $r['waiting'];
			if ($after < 0)
				$this->negative[$r['id']] = $after;
		}

		$this->res = $entry->getElementsIterator();
		$this->res->rewind();


		$this->totalval = array('D' => 0, 'H' => 0); // quantity in days
		$this->veid = bab_toHtml($entry->id);
		$this->nomatch = false;


		if ($this->folder = $entry->getFolderEntriesIterator())
		{
			$this->folder->appr_idfai = bab_getWaitingIdSAInstance(bab_getUserId());

			$this->folder->rewind();
		}
	}

	function getnexttype()
	{
		if( $this->res->valid())
		{
			$elem = $this->res->current();
			/*@var $elem absences_EntryElem */
			$right = $elem->getRight();
			$type = $right->getType();

			$this->totalval[$right->quantity_unit] += $elem->quantity;
			$this->nbdays = absences_quantity($elem->quantity, $right->quantity_unit);
			$this->alert = isset($this->negative[$right->id]) ? $this->negative[$right->id] : false;

			$this->typecolor = bab_toHtml($type->color);
			$this->typename = bab_toHtml($type->name);
			$this->rightname = bab_toHtml($right->description);

			$this->res->next();

			return true;
		}
		return false;

	}

	function getmatch()
	{
		// si aucun droit en heure, verifier que le nombre de jours pris corespond au nombre de jour de la periode
		if (0 === round(100 * $this->totalval['H']))
		{
		    
		    
			// pour les demi-jours, une precision d'un chiffre apres la virgule suffi
			$this->nomatch = !(round(10 * $this->totalval['D']) === round(10 * $this->totaldates_days));
			return false;
		}

		// jours non pris (doit etre occupe par les heures)
		$days1 = $this->totaldates_days - $this->totalval['D'];

		// nombre de jours calcules corespondant aux heures prises sur la periode
		if ($this->totaldates_hours > 0)
		{
			$days2 = ($this->totaldates_days * $this->totalval['H']) / $this->totaldates_hours;
		} else {
			$days2 = 0;
		}

		
		
		$this->nomatch = $days1 !== $days2;
		return false;
	}


	public function getnextfe()
	{

		if ($this->folder->valid())
		{
			$entry = $this->folder->current();
			/*@var $entry absences_Entry */
			$this->date = bab_toHtml(bab_shortDate(bab_mktime($entry->date_begin), false));

			$this->folder->next();
			return true;
		}

		return false;
	}
}






function absences_confirmWaitingVacation($id)
{
	global $babBody, $babDB;

	$entry = absences_Entry::getById($id);

	if (!$entry->getRow()) {
		$babBody->addError(absences_translate("This vacation request does not exists"));
		$babBody->babpopup('');
		return 0;
	}

	$temp = new absences_approbEntryDetail($entry);
	
	$Icons = bab_functionality::get('Icons');
	/*@var $Icons Func_Icons */
	$Icons->includeCss();
	
	$babBody->babPopup(absences_addon()->printTemplate($temp, "approb.html", "confirmvacation"));
}



















/**
 * Next approval step
 * @param absences_WorkperiodRecoverRequest $workperiod
 */
function absences_confirmWaitingRecoverRequestSave(absences_WorkperiodRecoverRequest $workperiod)
{
	$W = bab_Widgets();
	$values = bab_pp('workperiod');
	
	$id_type = absences_getRecoveryType();
	
	if (empty($id_type))
	{
		throw new Exception(absences_translate('The right type for recovery is not configured'));
	}
	
	
	// mise a jour de la quantite et de quantity_unit
	
	$workperiod->quantity = str_replace(',', '.', $values['quantity']);
	$workperiod->quantity_unit = $values['quantity_unit'];
	$workperiod->validity_end = $W->DatePicker()->getISODate($values['validity_end']);
	
	if (isset($values['confirm']))
	{
		$workperiod->approbationNext(true, $values['comment2']);
	} else if (isset($values['refuse']))
	{
		$workperiod->approbationNext(false, $values['comment2']);
	}
	
	$workperiod->notifyOwner();
	$workperiod->notifyApprovers();
	
	$url = bab_url::get_request('tg');
	$url->idx = 'unload';
	$url->location();
}






function absences_confirmWaitingRecoverRequest($id_workperiod)
{
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.ui.php';
	$W = bab_Widgets();
	$page = $W->babPage();
	
	$Icons = bab_functionality::get('Icons');
	/*@var $Icons Func_Icons */
	$Icons->includeCss();
	
	$workperiod = absences_WorkperiodRecoverRequest::getById($id_workperiod);
	
	if (!$workperiod->getRow())
	{
		$page->addError(absences_translate('This entry does not exist'));
		$page->displayHtml();
		return;
	}
	
	if ('' != $workperiod->status)
	{
		$page->addError(absences_translate('This entry is not waiting for confirmation'));
		$page->displayHtml();
		return;
	}
	
	
	if (!empty($_POST))
	{
		try {
			absences_confirmWaitingRecoverRequestSave($workperiod);
	
		} catch (Exception $e)
		{
			$page->addError($e->getMessage());
		}
	}
	
	
	$page->setTitle(absences_translate('Confirm the workperiod recovery request'));
	
	
	$editor = new absences_WorkperiodRecoverApprobEditor($workperiod);
	
	$page->addItem($editor);
	$page->setEmbedded(false);
	$page->displayHtml();
}



/**
 * Next approval step
 * @param absences_CetDepositRequest $deposit
 * @throws Exception
 * @return boolean
 */
function absences_confirmWaitingCetDepositSave(absences_CetDepositRequest $deposit)
{
	$cet = bab_pp('cet');
	
	
	// mise a jour de la quantite
	
	$agentRight = $deposit->getAgentRightSource();

	if (!isset($agentRight) || !$agentRight->getRow())
	{
		throw new Exception(absences_translate('The source right of this request is not valid'));
	}
	
	$right = $agentRight->getRight();
	
	if (!isset($right) || !$right->getRow())
	{
		throw new Exception(absences_translate('The source right of this request is not valid'));
	}
	
	$cet['quantity'] = (float) str_replace(',', '.', $cet['quantity']);

	
	$agentCet = $agentRight->getAgent()->Cet();
	if (!$agentCet->testDepositQuantity($agentRight, $cet['quantity'], $deposit))
	{
		return false;
	}
	

	$agentRightCet = $deposit->getAgentRightCet();
	
	$deposit->id_agent_right_cet = $agentRightCet->id;
	

	
	
	
	
	$deposit->quantity = $cet['quantity'];
	
	if (isset($cet['confirm']))
	{
		$deposit->approbationNext(true, $cet['comment2']);
		
	} else if (isset($cet['refuse']))
	{
		$deposit->approbationNext(false, $cet['comment2']);
	}
	
	$deposit->notifyOwner();
	$deposit->notifyApprovers();
	
	$url = bab_url::get_request('tg');
	$url->idx = 'unload';
	$url->location();
}



function absences_confirmWaitingCetDeposit($id_deposit)
{
	require_once dirname(__FILE__).'/utilit/cet_deposit_request.class.php';
	require_once dirname(__FILE__).'/utilit/cet_deposit_request.ui.php';
	$W = bab_Widgets();
	$page = $W->babPage();
	
	$Icons = bab_functionality::get('Icons');
	/*@var $Icons Func_Icons */
	$Icons->includeCss();
	
	$deposit = absences_CetDepositRequest::getById($id_deposit);
	
	if (!$deposit->getRow())
	{
		$page->addError(absences_translate('This deposit does not exist'));
		$page->displayHtml();
		return;
	}
	
	if ('' != $deposit->status)
	{
		$page->addError(absences_translate('This deposit is not waiting for confirmation'));
		$page->displayHtml();
		return;
	}
	
	
	if (!empty($_POST))
	{
		try {
			absences_confirmWaitingCetDepositSave($deposit);
			
		} catch (Exception $e)
		{
			$page->addError($e->getMessage());
		}
	}
	
	
	$page->setTitle(absences_translate('Confirm the time saving account deposit'));
	
	
	$editor = new absences_CetDepositRequestApprobEditor($deposit);	
	
	$page->addItem($editor);
	$page->setEmbedded(false);
	$page->displayHtml();
}




/**
 * @param int		$veid		Vacation entry id
 * @param string	$remarks
 * @param boolean	$action
 * @return boolean
 */
function absences_confirmVacationRequest($veid, $remarks, $action)
{
	require_once dirname(__FILE__).'/utilit/entry.class.php';
	require_once dirname(__FILE__).'/utilit/request.notify.php';

	$entry = absences_Entry::getById($veid);

	if (1 === (int) bab_pp('folder', 0))
	{
		$I = $entry->getFolderEntriesIterator();
		$I->appr_idfai = bab_getWaitingIdSAInstance(bab_getUserId());

		foreach($I as $folder_entry)
		{
			/*@var $folder_entry absences_Entry */
			$folder_entry->approbationNext($action, $remarks);
			$folder_entry->applyDynamicRight();


			// on fait une notification groupee a la place
			// $folder_entry->notifyOwner();
		}

		
		if (isset($folder_entry))
		{
			// last entry

			if ($action && 'Y' == $folder_entry->status)
			{
				absences_notifyRequestAuthor($I, absences_translate("An absences request set has been accepted"), '', $folder_entry->id_user);

				$agent = $folder_entry->getAgent();
				$emails = $agent->getEmails();
				if (!empty($emails))
				{
					absences_notifyEntryOwnerEmails($I, $emails);
				}
			} else if ('N' == $folder_entry->status) {

				absences_notifyRequestAuthor($I, absences_translate("An absences request set has been rejected"), '', $folder_entry->id_user);
			}
			
			
			$folder_entry->notifyApprovers();
		}

	} else {

		$entry->approbationNext($action, $remarks);
		$entry->applyDynamicRight();
		$entry->notifyOwner();
		$entry->notifyApprovers();
	}

	return true;
}



// main


if( '' != ($conf = bab_rp('conf')))
{
	if( $conf == 'vac' )
	{
		if (isset($_POST['confirm'])) {

			if (!absences_confirmVacationRequest(bab_pp('veid'), bab_pp('remarks'), true)) {
				$babBody->addError(absences_translate('Access denied'));
				return;
			}

		} else if (isset($_POST['refuse'])) {

			if (!absences_confirmVacationRequest(bab_pp('veid'), bab_pp('remarks'), false)) {
				$babBody->addError(absences_translate('Access denied'));
				return;
			}

		}
		$idx = 'unload';
	}
}



$idx = bab_rp('idx');

switch($idx)
{
	case "unload":
		include_once $babInstallPath."utilit/uiutil.php";
		popupUnload(absences_translate("Update done"), $GLOBALS['babUrlScript']."?tg=approb&idx=all");
		exit;
	
	
	case 'confvac':
		absences_confirmWaitingVacation(bab_gp('idvac'));
		exit;
		break;
		
	case 'recover':
		absences_confirmWaitingRecoverRequest(bab_rp('id_workperiod'));
		break;
		
	case 'cet':
		absences_confirmWaitingCetDeposit(bab_rp('id_deposit'));
		break;
}
