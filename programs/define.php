<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



define("ABSENCES_MANAGERS_TBL", "absences_managers");
define("ABSENCES_TYPES_TBL", "absences_types");
define("ABSENCES_COLLECTIONS_TBL", "absences_collections");
define("ABSENCES_PERSONNEL_TBL", "absences_personnel");
define("ABSENCES_RIGHTS_TBL", "absences_rights");
define("ABSENCES_RIGHTS_RULES_TBL", "absences_rights_rules");
define("ABSENCES_RIGHTS_INPERIOD_TBL", "absences_rights_inperiod");
define("ABSENCES_USERS_RIGHTS_TBL", "absences_users_rights");
define("ABSENCES_ENTRIES_TBL", "absences_entries");
define("ABSENCES_ENTRIES_ELEM_TBL", "absences_entries_elem");
define("ABSENCES_PLANNING_TBL", "absences_planning");
define("ABSENCES_OPTIONS_TBL", "absences_options");
define("ABSENCES_CALENDAR_TBL", "absences_calendar");
define("ABSENCES_RGROUPS_TBL", "absences_rgroup");
define("ABSENCES_COMANAGER_TBL", "absences_comanager");

/**
 * @deprecated : liaison regime/type : on laisse les anciennes donnes dans la table mais elle ne doit plus etre utilise
 */
define("ABSENCES_COLL_TYPES_TBL", "absences_coll_types");


define("ABSENCES_MAX_RIGHTS_LIST", 20);



define("ABSENCES_MAX_REQUESTS_LIST", 20);

define("ABSENCES_MAX_AGENTS_LIST", 20);

define("ABSENCES_FIX_DELETE", 2);
define("ABSENCES_FIX_UPDATE", 1);
define("ABSENCES_FIX_ADD",    0);


define("ABSENCES_RECUR_DAILY",	1);
define("ABSENCES_RECUR_WEEKLY",	2);
define("ABSENCES_RECUR_MONTHLY",	3);
define("ABSENCES_RECUR_YEARLY",	4);

