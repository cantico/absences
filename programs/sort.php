<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/right.class.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';




class absences_sortRights
{
	
	/**
	 * @return absences_RightSort[]
	 */
	public static function getSortableItems()
	{
		global $babDB;
		
		$return = array();
		
		$res = $babDB->db_query("SELECT * FROM absences_rights WHERE id_rgroup='0'");
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$right = new absences_Right($arr['id']);
			$right->setRow($arr);
			$return[] = $right;
		}
		
		$res = $babDB->db_query("SELECT * FROM absences_rgroup");
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$rgroup = new absences_Rgroup($arr['id']);
			$rgroup->setRow($arr);
			$return[] = $rgroup;
		}
		
		
		bab_Sort::sortObjects($return, 'getSortKey');
		
		
		return $return;
	}
	
	
	
	public static function getForm()
	{
		bab_functionality::includeOriginal('Icons');
		
		$W = bab_Widgets();
		$form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		$form->setName('sort');
		$form->addClass('widget-bordered');
		$form->addClass('widget-centered');
		$form->addClass('BabLoginMenuBackground');
		$form->addClass(Func_Icons::ICON_LEFT_16);
		$form->setCanvasOptions($form->Options()->width(70,'em'));
		
		$form->addItem($W->Title(absences_translate('Sort rights and rights groups'), 3));
		
		$list = $W->VBoxLayout()->setVerticalSpacing(.2,'em');
		$list->sortable(true);
		$form->addItem($list);
		
		foreach(self::getSortableItems() as $item)
		{
			/*@var $item absences_RightSort */
			$list->addItem(
				$W->FlowItems(
					$W->Hidden()->setName(array('item', ''))->setValue(get_class($item).':'.$item->getId()),
					$W->Icon($item->getSortLabel(), $item->getIconClassName())
				)
			);
		}
		
		$form->setSelfPageHiddenFields();
		
		$form->addItem(
			$W->SubmitButton()
				->setLabel(absences_translate('Save'))
				->setName('save')
		);
		
		return $form;
	}
	
	/**
	 * Update sortkeys
	 * @param array $values
	 */
	private static function save($values)
	{
		$items = $values['item'];
		$i = 0;
		foreach($items as $item)
		{
			list($class, $id) = explode(':', $item);
			$object = new $class($id);
			$object->setSortKey($i);
			$i++;
		}
		
		// go to manager menu
		
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = new bab_url;
		$url->tg = 'addon/absences/vacadm';
		$url->location();
	}
	
	
	/**
	 * display form page
	 */
	public static function display()
	{
		if (isset($_POST['sort']['save']))
		{
			self::save(bab_pp('sort'));
		}
		
		
		$W = bab_Widgets();
		$page = $W->BabPage();
		$page->addItem(self::getForm());
		
		$page->displayHtml();
	}
}




/* main */
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->addError(absences_translate("Access denied"));
	return;
}

$idx = bab_rp('idx', 'right');


switch($idx)
{

	case 'right':
		absences_sortRights::display();
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');