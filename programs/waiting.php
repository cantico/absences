<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once $babInstallPath."utilit/afincl.php";
include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/request.class.php';




function absences_waitingEmails()
{
    $W = bab_Widgets();
    $page = $W->babPage();
    
    $page->setTitle(absences_translate('Waiting requests by approvers'));
    
    $arr = absences_getRequestsApprovers(null);
    
    $tree = $W->SimpleTreeView();
    $root = $tree->createRootNode('Emails', 'Root');
    
    $mailId = 0;
    foreach($arr as $email) {
        $mailId++;
        $element = $tree->createElement('email'.$mailId, 'email', 'Mail '.$mailId);
        $tree->appendElement($element, 'Root');
        
        $approvers = $tree->createElement('approvers'.$mailId, 'approvers', absences_translate('Approvers'));
        $approvers->setIcon($GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/folder.gif');
        $tree->appendElement($approvers, 'email'.$mailId);
        
        $requests = $tree->createElement('requests'.$mailId, 'requests', absences_translate('Requests'));
        $requests->setIcon($GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/folder.gif');
        $tree->appendElement($requests, 'email'.$mailId);
        
        foreach($email['approvers'] as $id_user) {
            $user = $tree->createElement('user'.$id_user, 'user', bab_getUserName($id_user));
            $tree->appendElement($user, 'approvers'.$mailId);
        }
       
        foreach($email['requests'] as $request) {
            
            /*@var $request absences_Request */
            
            $reqnode = $tree->createElement('request'.$request->id, 'request', $request->getUserName().' : '.$request->getTitle());
            
            try {
                $tree->appendElement($reqnode, 'requests'.$mailId);
            } catch (ErrorException $e) {
                bab_debug($e->getMessage());
            }
        }
    }
    
    $page->addItem($tree);
	$page->displayHtml();
}




function absences_waitingRequestList()
{
	$W = bab_Widgets();
	$page = $W->babPage();
	
	$page->addStyleSheet(absences_Addon()->getStylePath().'vacation.css');
	$page->setTitle(absences_translate('Waiting requests'));
	
	$f = new absences_getRequestSearchForm();
	
	$I = new absences_RequestIterator();
	$I->status = '';
	
	
	if ($userid = $f->param('userid'))
	{
	    $I->users = array($userid);
	}
	
	if ($organization = $f->param('organization'))
	{
	    $I->organization = array($organization);
	}
	
	$datePicker = $W->DatePicker();
	
	if ('0000-00-00' !== $begin = $datePicker->getISODate($f->param('dateb', null)))
	{
	    $I->startFrom = $begin;
	}
	
	if ('0000-00-00' !== $end = $datePicker->getISODate($f->param('datee', null)))
	{
	    $I->startTo = $end;
	}
	
	bab_functionality::includeOriginal('Icons');
	
	$table = $W->BabTableView();
	$table->addClass(Func_Icons::ICON_LEFT_16);
	
	$table->addItem($W->Label(absences_translate('Request type'))		, 0, 0);
	$table->addItem($W->Label(absences_translate('Appliquant'))			, 0, 1);
	$table->addItem($W->Label(absences_translate('Approver(s)'))		, 0, 2);
	$table->addItem($W->Label(absences_translate('Approbation sheme'))	, 0, 3);
	$table->addItem($W->Label(absences_translate('Last action date'))	, 0, 4);
	$table->addItem($W->Label(absences_translate('Last action'))		, 0, 5);
	$table->addItem($W->Label(absences_translate('Edit'))				, 0, 6);
	$table->addItem($W->Label(absences_translate('Delete'))				, 0, 7);
	
	$table->addHeadRow(0);
	
	$row = 1;
	foreach($I as $request)
	{
		/* @var $request absences_Request */
		
		$appliquant = absences_Agent::getFromIdUser($request->id_user);
		$approbation = $appliquant->getApprobation();
		
		if ($movement = $request->getLastMovement())
		{
			$last_createdOn = $movement->createdOn;
			$last_message = $movement->message;
		} else {
			$last_createdOn = '';
			$last_message = '';
		}
		
		
		$table->addItem($W->Label($request->getRequestType())															, $row, 0 );
		$table->addItem($W->Label($appliquant->getName())																, $row, 1 );
		$table->addItem($W->Label($request->getNextApprovers())															, $row, 2 );
		$table->addItem($W->Label($approbation['name'].sprintf(' (%s)', $approbation['type']))							, $row, 3 );
		$table->addItem($W->Label(bab_shortDate(bab_mktime($last_createdOn)))											, $row, 4 );
		$table->addItem($W->Label($last_message)																		, $row, 5 );
		$table->addItem($W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $request->getManagerEditUrl())		, $row, 6 );
		
		
		$deleteLink = $W->Link($W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE), $request->getManagerDeleteUrl());
		
		if (!($request instanceof absences_Entry))
		{
			$deleteLink->setConfirmationMessage(absences_translate('Do you really want to delete?'));	
		}
		
		
		$table->addItem($deleteLink	, $row, 7 );
		
		if ($request->approbAlert())
		{
			$table->addRowClass($row, 'widget-strong');
		}
		
		$row++;
	}
	
	
	
	
	$page->addItem($f->getForm());
	$page->addItem($table);
	
	$page->displayHtml();
}






// main
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}


if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}

$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");


$idx = bab_rp('idx', "list");


switch($idx)
{
    case 'emails':
        $babBody->addItemMenu("emails", absences_translate("By approvers"), absences_addon()->getUrl()."waiting&idx=emails");
        absences_waitingEmails();
        break;
    
    
	default:
	case 'list':
		$babBody->addItemMenu("list", absences_translate("Waiting requests"), absences_addon()->getUrl()."waiting&idx=list");
		absences_waitingRequestList();
		break;
}

$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');