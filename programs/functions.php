<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



include_once dirname(__FILE__).'/define.php';

if ($icons = bab_functionality::get('Icons')) {
    /*@var $icons Func_Icons */
    
    try {
        $icons->includeCss();
    } catch(Exception $e) {
        bab_debug($e->getMessage());
    }
}

/**
 * Translate
 * @param string $str
 * @return string
 */
function absences_translate($str, $str_plurals = null, $number = null)
{


	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('absences');
		
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}


/**
* @return bab_addonInfos
*/
function absences_addon()
{
	return bab_getAddonInfosInstance('absences');
}



/**
 * Vacation access
 * @return multitype:boolean
 */
function absences_vacationsAccess()
{
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	
	$agent = absences_Agent::getCurrentUser();

	$array = array();

	if($agent->isInPersonnel())
	{
		$array['user'] = true;
	}

	if ($agent->isManager())
	{
		$array['manager'] = true;
	}
	
	if ($agent->isApprover())
	{
		$array['approver'] = true;
	}
	
	return $array;
}



function absences_kinds()
{
	return array(
		absences_Right::REGULAR 	=> absences_translate('Regular'),
		absences_Right::FIXED 		=> absences_translate('Fixed dates'),
		absences_Right::CET 		=> absences_translate('Time savings account'),
		absences_Right::INCREMENT 	=> absences_translate('Monthly variable right'),
		absences_Right::RECOVERY 	=> absences_translate('Recovery right'),
		absences_Right::REPORT 		=> absences_translate('Report right')
	);
}


function absences_syncStatus()
{
	return array(
		absences_Right::SYNC_CLIENT 		=> absences_translate('Synchronization activated'),
		absences_Right::SYNC_CLIENT_END 	=> absences_translate('No synchronization, the right is not available on server'),
		absences_Right::SYNC_CLIENT_ERROR 	=> absences_translate('Synchronization failed because of an error'),
		absences_Right::SYNC_SERVER 		=> absences_translate('Shared right')
	);
}


/**
 * 
 * @param string $date		Datepicker date
 * @param string $hour
 */
function absences_dateTimeForm($date, $hour)
{
	$W = bab_Widgets();
	$datePicker = $W->DatePicker();
	
	$dateiso = $datePicker->getISODate($date);
	
	return $dateiso.' '.$hour;
}





/**
 * @return bool
 */
function absences_lockedForMainteance()
{
	$agent = absences_Agent::getCurrentUser();
	
	if ($agent->isManager())
	{
		return false;
	}
	
	return (bool) absences_getVacationOption('maintenance');
}


function absences_getMaintenanceMessage()
{
	return absences_translate('This feature is not available at this time because a maintenance action is in progress');
}



/**
 * Display a menu to create vacation request, CET request, workingdays recovery request
 *
 */
function absences_getToolbar()
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/toolbar.class.php';
    $babBody = bab_getBody();
    $babBody->addStyleSheet('toolbar.css');

    $toolbar = new BAB_Toolbar();
    return $toolbar;
}


/**
 * @return int
 */
function absences_getRecoveryType()
{
    global $babDB;

    $res = $babDB->db_query("SELECT id FROM absences_types WHERE recover='1'");

    if ($babDB->db_num_rows($res) == 0) {
        return 0;
    }

    $row = $babDB->db_fetch_assoc($res);

    return (int) $row['id'];
}


function absences_addSpoofButton($type, $label)
{


    require_once $GLOBALS['babInstallPath'] . 'utilit/toolbar.class.php';
    $babBody = bab_getBody();
    $toolbar = new BAB_Toolbar();
    $sImgPath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/';
    $toolbar->addToolbarItem(
        new BAB_ToolbarItem($label, absences_addon()->getUrl().'spoofing&request_type='.$type,
            $sImgPath . 'edit_add.png', '', '', '')
    );
    
    $babBody->addStyleSheet('toolbar.css');
    $babBody->babEcho($toolbar->printTemplate());
}




function absences_deleteAgentsWithNoUser()
{
    global $babDB;
    require_once dirname(__FILE__).'/utilit/agent.class.php';
    
    $res = $babDB->db_query('SELECT a.id_user FROM absences_personnel a LEFT JOIN bab_users u ON u.id=a.id_user WHERE u.id IS NULL');
    while ($arr = $babDB->db_fetch_assoc($res)) {
        if ($arr['id_user']) {
            $agent = absences_Agent::getFromIdUser($arr['id_user']);
            $agent->delete();
        }
    }
}

/**
 * function from the orgchart addon to force one primary role
 */
function absence_rePrimary()
{
    if(!bab_isUserAdministrator()){
        return false;
    }

    global $babDB;

    $sql = "SELECT id
            FROM bab_oc_roles_users

            WHERE id_user NOT IN (
                SELECT id_user FROM bab_oc_roles_users WHERE isprimary = 'Y'
            )

            GROUP BY id_user
            ";

    $res = $babDB->db_query($sql);

    while($arr = $babDB->db_fetch_array($res)){
        $babDB->db_query("update bab_oc_roles_users set isprimary='Y' where id=".$babDB->quote($arr['id']));
    }
}



function absences_fixRequestLastMovement($table, $className)
{
    global $babDB;
    
    $res = $babDB->db_query("SELECT e.id, l.createdOn, l.id movement , e.status
	       FROM
	           ".$table." e
	            LEFT JOIN  absences_movement l ON l.id_request= e.id AND request_class=".$babDB->quote($className)."
	    INNER JOIN
    (SELECT max(createdOn) createdOn FROM absences_movement WHERE request_class=".$babDB->quote($className)." AND status IS NOT NULL GROUP BY id_request) m
	    ON m.createdOn=l.createdOn
	  
	    WHERE
	       e.status<>l.status
	    ");
    while ($arr = $babDB->db_fetch_assoc($res)) {
        bab_installWindow::message("Wrong status in movements history for $className ".$arr['id'].', fixing last movement');
        $babDB->db_query('UPDATE absences_movement SET status='.$babDB->quote($arr['status']).' WHERE id='.$babDB->quote($arr['movement']));
    }
}

