<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/options.ui.php';




function vacationOptions()
{
	require_once $GLOBALS['babInstallPath'].'admin/acl.php';
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	
	if (isset($_POST['options']))
	{
	
		if( isset($_POST['options']['save'] ))
		{
			$values = $_POST['options'];
			record_options($values);
			
			aclSetRightsString('absences_managers_groups', 1, $values['managers']);
			aclSetRightsString('absences_public_planning_groups', 1, $values['public_planning']);
		}
	}
	
	
	
	$editor = new absences_OptionsEditor();
	
	$page->setTitle(absences_translate('Vacations options'));
	$page->addItem($editor);
	$page->displayHtml();
}






function record_options($values) {
	global $babDB;

	if (!isset($values['id_chart']))
	{
		$values['id_chart'] = 0;
	}
	
	
	list($n) = $babDB->db_fetch_array($babDB->db_query("SELECT COUNT(*) FROM absences_options"));
	if ($n > 0) {

		$babDB->db_query('
			UPDATE absences_options

			SET chart_superiors_create_request = ' . $babDB->quote($values['chart_superiors_create_request']) . ',
				chart_superiors_set_rights = ' . $babDB->quote($values['chart_superiors_set_rights']) . ',
				chart_superiors_user_edit = ' . $babDB->quote($values['chart_superiors_user_edit']) . ',
				allow_mismatch = ' . $babDB->quote($values['allow_mismatch']).' ,
				workperiod_recover_request = '.$babDB->quote($values['workperiod_recover_request']).' ,
				display_personal_history = '.$babDB->quote($values['display_personal_history']).' ,
		        modify_confirmed = '.$babDB->quote($values['modify_confirmed']).' ,
		        modify_waiting = '.$babDB->quote($values['modify_waiting']).' ,
				email_manager_ondelete = '.$babDB->quote($values['email_manager_ondelete']).' ,
				approb_email_defer = '.$babDB->quote($values['approb_email_defer']).' ,
				entity_planning = '.$babDB->quote($values['entity_planning']).', 
		        entity_planning_display_types = '.$babDB->quote($values['entity_planning_display_types']).', 
				approb_alert = '.$babDB->quote($values['approb_alert']).',
		        auto_approval = '.$babDB->quote($values['auto_approval']).',
				auto_confirm = '.$babDB->quote($values['auto_confirm']).',
				sync_server = '.$babDB->quote($values['sync_server']).',
				sync_url = '.$babDB->quote($values['sync_url']).',
				sync_nickname = '.$babDB->quote($values['sync_nickname']).',
				sync_password = '.$babDB->quote($values['sync_password']).',
				id_chart = '.$babDB->quote($values['id_chart']).',
				user_add_email = '.$babDB->quote($values['user_add_email']).',
				end_recup = '.$babDB->quote($values['end_recup']).',
				delay_recovery = '.$babDB->quote($values['delay_recovery']).',
				maintenance = '.$babDB->quote($values['maintenance']).',
				archivage_day = '.$babDB->quote($values['archivage_day']).',
				archivage_month = '.$babDB->quote($values['archivage_month']).', 
				appliquant_email = '.$babDB->quote($values['appliquant_email']) .', 
				organization_sync = '.$babDB->quote($values['organization_sync'])

		);
	} else {
		$babDB->db_query('

			INSERT INTO absences_options (
				chart_superiors_create_request, 
				chart_superiors_set_rights,
				chart_superiors_user_edit,
				allow_mismatch,
				workperiod_recover_request,
				display_personal_history,
		        modify_confirmed,
		        modify_waiting,
				email_manager_ondelete,
				approb_email_defer,
				entity_planning,
		        entity_planning_display_types,
				approb_alert,
		        auto_approval,
				auto_confirm,
				sync_server,
				sync_url,
				sync_nickname,
				sync_password,
				id_chart,
				user_add_email,
				end_recup,
				delay_recovery,
				maintenance, 
				archivage_day, 
				archivage_month,
				appliquant_email,
				organization_sync
			)
			VALUES (
				' . $babDB->quote($values['chart_superiors_create_request']) . ',
				' . $babDB->quote($values['chart_superiors_set_rights']) . ',
				' . $babDB->quote($values['chart_superiors_user_edit']) . ',
				' . $babDB->quote($values['allow_mismatch']) . ',
				' . $babDB->quote($values['workperiod_recover_request']) . ',
				' . $babDB->quote($values['display_personal_history']). ',
		        ' . $babDB->quote($values['modify_confirmed']). ',
		        ' . $babDB->quote($values['modify_waiting']). ',
				' . $babDB->quote($values['email_manager_ondelete']). ',
				' . $babDB->quote($values['approb_email_defer']). ',
				' . $babDB->quote($values['entity_planning']). ',
		        ' . $babDB->quote($values['entity_planning_display_types']). ',
				' . $babDB->quote($values['approb_alert']). ',
		        ' . $babDB->quote($values['auto_approval']). ',
				' . $babDB->quote($values['auto_confirm']). ',
				' . $babDB->quote($values['sync_server']). ',
				' . $babDB->quote($values['sync_url']). ',
				' . $babDB->quote($values['sync_nickname']). ',
				' . $babDB->quote($values['sync_password']). ',
				' . $babDB->quote($values['id_chart']). ',
				' . $babDB->quote($values['user_add_email']). ',
				' . $babDB->quote($values['end_recup']). ',
				' . $babDB->quote($values['delay_recovery']). ',
				' . $babDB->quote($values['maintenance']). ',
				' . $babDB->quote($values['archivage_day']). ',
				' . $babDB->quote($values['archivage_month']). ',
				' . $babDB->quote($values['appliquant_email']). ',
				' . $babDB->quote($values['organization_sync']). '
			)'
		);
	}
}





function absences_initOrganizations()
{
    $W = bab_Widgets();
    $page = $W->BabPage(null, $W->VBoxLayout());
    $page->setEmbedded(false);
    
    $page->setTitle(absences_translate('Set the personnel members organization field using their directory entries'));
    
    require_once dirname(__FILE__).'/utilit/agent.class.php';
    require_once dirname(__FILE__).'/utilit/organization.class.php';
    
    // update the organization list
    absences_Organization::createFromDirectory();
    
    $agents = new absences_AgentIterator();
    
    
    foreach($agents as $agent) {
        // set the associated organization
        
        try {
            $agent->setOrganizationFromDirEntry();
            $page->addItem($W->Label(sprintf(absences_translate('%s has been modified'), $agent->getName())));
        } catch (Exception $e) {
            
            $label = $agent->getName().' : '.$e->getMessage();
            
            if ($link = bab_getUserDirEntryLink($agent->getIdUser())) {
                $page->addItem($W->Link($label, $link)->setOpenMode(Widget_Link::OPEN_POPUP));
            } else {
                $page->addItem($W->Label($label));
            }
        }
    }
    
    $page->displayHtml();
}




/* main */
bab_requireCredential();
if( !bab_isUserAdministrator() )
	{
	$babBody->title = absences_translate("Access denied");
	return;
	}


$idx = bab_rp('idx', 'options');





switch ($idx) {
    case 'init':
        absences_initOrganizations();
        break;
	    
	default:
	case 'options':
	    $babBody->addItemMenu('options', absences_translate("Options"), absences_addon()->getUrl()."admvacs&idx=options");
		vacationOptions();
		break;
}

$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','Admin');

