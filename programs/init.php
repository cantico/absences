<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

include_once dirname(__FILE__).'/functions.php';


function absences_onDeleteAddon()
{
	include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

	bab_removeAddonEventListeners('absences');
	
	$functionalities = new bab_functionalities();
	$functionalities->unregister('PortletBackend/Absences');
	$functionalities->unregister('AbsencesAgent');
	
	return true;
}



function absences_upgrade($version_base, $version_ini)
{
	global $babDB;
	
	include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	
	$addon = bab_getAddonInfosInstance('absences');
	

    bab_removeAddonEventListeners('absences');
    
    $addon->addEventListener('bab_eventUserDeleted'                   , 'absences_onUserDeleted'                 , 'events.php');
    $addon->addEventListener('bab_eventBeforeSiteMapCreated'          , 'absences_onBeforeSiteMapCreated'        , 'events.php');
    $addon->addEventListener('bab_eventUserModified'                  , 'absences_onUserModified'                , 'events.php');
    $addon->addEventListener('bab_eventBeforeWaitingItemsDisplayed'   , 'absences_onBeforeWaitingItemsDisplayed' , 'events.php');
    $addon->addEventListener('bab_eventConfirmMultipleWaitingItems'   , 'absences_onConfirmMultipleWaitingItems' , 'events.php');
    $addon->addEventListener('bab_eventPeriodModified'                , 'absences_onModifyPeriod'                , 'events.php');
    $addon->addEventListener('bab_eventBeforePeriodsCreated'          , 'absences_onBeforePeriodsCreated'        , 'events.php');
    $addon->addEventListener('LibTimer_eventHourly'                   , 'absences_onHourly'                      , 'events.php');
    $addon->addEventListener('bab_eventAfterEventCategoryDeleted'     , 'absences_onAfterEventCategoryDeleted'   , 'events.php');
    $addon->addEventListener('bab_eventOrgChartEntityAfterDeleted'    , 'absences_onOrgChartEntityAfterDeleted'  , 'events.php');
    $addon->addEventListener('bab_eventDirectory'                     , 'absences_onDirectory'                   , 'events.php');
    

	
	$tables = new bab_synchronizeSql(dirname(__FILE__).'/sql/dump.sql');
	//var_dump($tables->getDifferences());
	
	if ($tables->isCreatedTable('absences_rights') && bab_isTable('bab_vac_managers'))
	{
		include_once dirname(__FILE__).'/olddata.php';
		
		absence_copy_table('bab_vac_managers'		, ABSENCES_MANAGERS_TBL);
		absence_copy_table('bab_vac_types'			, ABSENCES_TYPES_TBL);
		absence_copy_table('bab_vac_collections'	, ABSENCES_COLLECTIONS_TBL);
		absence_copy_table('bab_vac_coll_types'		, ABSENCES_COLL_TYPES_TBL);
		absence_copy_table('bab_vac_personnel'		, ABSENCES_PERSONNEL_TBL);
		absence_copy_table('bab_vac_rights'			, ABSENCES_RIGHTS_TBL);
		absence_copy_table('bab_vac_rights_rules'	, ABSENCES_RIGHTS_RULES_TBL);
		absence_copy_table('bab_vac_rights_inperiod', ABSENCES_RIGHTS_INPERIOD_TBL);
		absence_copy_table('bab_vac_users_rights'	, ABSENCES_USERS_RIGHTS_TBL);
		absence_copy_table('bab_vac_entries'		, ABSENCES_ENTRIES_TBL);
		absence_copy_table('bab_vac_entries_elem'	, ABSENCES_ENTRIES_ELEM_TBL);
		absence_copy_table('bab_vac_planning'		, ABSENCES_PLANNING_TBL);
		absence_copy_table('bab_vac_options'		, ABSENCES_OPTIONS_TBL);
		absence_copy_table('bab_vac_calendar'		, ABSENCES_CALENDAR_TBL);
		absence_copy_table('bab_vac_rgroup'			, ABSENCES_RGROUPS_TBL);
		absence_copy_table('bab_vac_comanager'		, ABSENCES_COMANAGER_TBL);
		
		bab_installWindow::message(absences_translate('Import vacations data to addon... done.'));
	}
	
	
	// verifier les cles de la table absences_users_rights
	
	if (bab_isKeyExists('absences_users_rights', 'id_user'))
	{
		$babDB->db_query('ALTER TABLE absences_users_rights DROP INDEX id_user');
	}
	
	if (bab_isKeyExists('absences_users_rights', 'id_right'))
	{
		$babDB->db_query('ALTER TABLE absences_users_rights DROP INDEX id_right');
	}
	
	if (!bab_isKeyExists('absences_users_rights', 'user_right'))
	{
		bab_installWindow::message(absences_translate('Error, the program was not able to create the unique key "user_right", duplicated entries in table absences_users_rights must be removed manually before continue'));
		return false;
	}

	
	// fix the kind field if not set
	
	$res = $babDB->db_query("SELECT id, kind, date_begin_fixed FROM absences_rights WHERE kind='0'");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		if ($arr['date_begin_fixed'] !== '0000-00-00 00:00:00')
		{
			$babDB->db_query("UPDATE absences_rights SET kind='2' WHERE id=".$babDB->quote($arr['id']));
		} else {
			$babDB->db_query("UPDATE absences_rights SET kind='1' WHERE id=".$babDB->quote($arr['id']));
		}
	}
	
	
	// mises a jour des demandes pour les droits a date fixe
	
	$res = $babDB->db_query("SELECT e.id FROM 
			absences_entries e, 
			absences_entries_elem ee, 
			absences_rights r
			WHERE 
				ee.id_entry=e.id 
				AND r.id=ee.id_right 
				AND r.kind='2'
			GROUP BY e.id 
	");
	
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$babDB->db_query("UPDATE absences_entries SET creation_type='1' WHERE id=".$babDB->quote($arr['id'])); 
	}
	
	
	
	// correction des demandes n'ayant pas de date de creation
	
	$res = $babDB->db_query("SELECT `id`, `date` FROM absences_entries WHERE createdOn='0000-00-00 00:00:00'");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$babDB->db_query("UPDATE absences_entries SET createdOn=".$babDB->quote($arr['date'])." WHERE id=".$babDB->quote($arr['id']));
	}
	
	$typescount = 0;
	$res = $babDB->db_query('SELECT COUNT(*) FROM '.$babDB->backTick('absences_types'));
	if ($arr = $babDB->db_fetch_array($res))
	{
	    $typescount = (int) $arr[0];
	}
	
	
	// default types
	
	if (0 === $typescount)
	{
		bab_execSqlFile(dirname(__FILE__).'/sql/types.sql', 'UTF-8');
		bab_installWindow::message(absences_translate('Import vacations types... done.'));
	}
	
	
	
	$collectionscount = 0;
	$res = $babDB->db_query('SELECT COUNT(*) FROM '.$babDB->backTick('absences_collections'));
	if ($arr = $babDB->db_fetch_array($res))
	{
	    $collectionscount = (int) $arr[0];
	}
	
	// default collections
	
	if (0 === $collectionscount)
	{
	    bab_execSqlFile(dirname(__FILE__).'/sql/collections.sql', 'UTF-8');
	    bab_installWindow::message(absences_translate('Import collections... done.'));
	}
	
	
	
	// considerer toutes demandes anciennes deja notifiees, ne pas toucher aux demandes du jour pour si on notifie une fois par jour
	
	$babDB->db_query("UPDATE absences_entries SET appr_notified='1' WHERE appr_notified='0' AND status='' AND createdOn<DATE_SUB(NOW(),INTERVAL 1 DAY)");
	
	
	
	// essayer de creer les liaisons droit-regimes a partir des liaisons types-regimes qui n'exisents plus dans l'application
	// pour cela on a les liaisons utilisateur-droit, si tout les utilisateur d'un regime sont relie a un droit alors on peut crer la liaison regime-droit
	// on effectue le test que si l'ancienne liaison regime-type-droit est presente
	
	if ($tables->isCreatedTable('absences_coll_rights'))
	{
	
		$res = $babDB->db_query('
			SELECT 
				ct.id_coll, 
				r.id 
			FROM 
				absences_coll_types ct,
				absences_rights r
			WHERE 
				r.id_type=ct.id_type 
				
		');
	
		while ($link = $babDB->db_fetch_assoc($res))
		{
			// chercher les membres du regime qui ne sont pas associes au droit
			$res2 = $babDB->db_query('SELECT * FROM 
					absences_personnel p 
						LEFT JOIN absences_users_rights ur ON p.id_user=ur.id_user AND ur.id_right='.$babDB->quote($link['id']).' 
				WHERE 
					p.id_coll='.$babDB->quote($link['id_coll']).'
					AND ur.id IS NULL 
			');
			
			if ($babDB->db_num_rows($res2) == 0)
			{
				$babDB->db_query('INSERT INTO absences_coll_rights (id_coll, id_right) VALUES ('.$babDB->quote($link['id_coll']).','.$babDB->quote($link['id']).')');
			}
		}
	}
	
	
	
	
	if ($tables->isCreatedTable('absences_public_planning_groups')) {
	    // dans les version precedente, le planing public etait active par checkbox
	    $res = $babDB->db_query('SELECT public_calendar FROM absences_options');
	    if ($arr = $babDB->db_fetch_assoc($res)) {
	        if ($arr['public_calendar']) {
	            require_once $GLOBALS['babInstallPath'].'admin/acl.php';
	            aclSetGroups_all('absences_public_planning_groups', 1);
	        }
	    }
	}
	
	
	// add missing UUID
	
	require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
	$res = $babDB->db_query("SELECT id FROM absences_rights WHERE uuid=''");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$babDB->db_query('UPDATE absences_rights SET uuid='.$babDB->quote(bab_uuid()).' WHERE id='.$babDB->quote($arr['id']));
	}
	
	$res = $babDB->db_query("SELECT id FROM absences_rights_inperiod WHERE uuid=''");
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$babDB->db_query('UPDATE absences_rights_inperiod SET uuid='.$babDB->quote(bab_uuid()).' WHERE id='.$babDB->quote($arr['id']));
	}
	
	
	// reprise des gestionnaires en tant que groupe avec ACL, faire la reprise si la table absences_options contient des lignes
	
	$res = $babDB->db_query("SELECT * FROM absences_managers");
	if ($babDB->db_num_rows($res) > 0)
	{
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		
		$users = aclGetAccessUsers('absences_managers_groups', 1);
		if (0 === count($users))
		{
			$groupname = absences_translate('Vacations managers');
			$i = 1;
			
			$rootgroups = bab_getGroups(BAB_REGISTERED_GROUP, false);
			$names = array_flip($rootgroups['name']);
			
			while(isset($names[$groupname])) {
				$groupname .= ' '.$i;
				$i++;
			}
	
			$id_group = bab_createGroup($groupname, absences_translate('Created automatically while upgrading to the new absences addon'), 0);
			
			while ($arr = $babDB->db_fetch_assoc($res))
			{
				bab_addUserToGroup($arr['id_user'], $id_group);
			}
			
			
			aclAdd('absences_managers_groups', $id_group, 1);
			
			$babDB->db_query('TRUNCATE absences_managers');
		}
	}
	
	
	// selection de l'organigramme par defaut
	require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
	if ($idprimaryoc = bab_OCgetPrimaryOcId())
	{
		$babDB->db_query('UPDATE absences_options SET id_chart='.$babDB->quote($idprimaryoc)." WHERE id_chart='0'");
	}
	
	
	
	// reprise des dates lors de l'ajout des dates dans la table absences_entries_elem
	require_once dirname(__FILE__).'/upgrade/entry_elem.php';
	absences_upgradeEntryElemDates::onUpgrade();
	
	
	$addon = bab_getAddonInfosInstance('absences');
	bab_functionality::includefile('PortletBackend');

    if (class_exists('Func_PortletBackend')) {
       $addon->registerFunctionality('PortletBackend/Absences', 'portletbackend.class.php');
    }
    $addon->registerFunctionality('AbsencesAgent', 'utilit/agent.api.php');
    $addon->registerFunctionality('AbsencesRequest', 'utilit/request.api.php');


	
	
	
	// T8341 Reprise des droits de recuperation creer avec une date de fin= a la date de debut
	
	$babDB->db_query("UPDATE absences_rights_inperiod SET period_end=DATE_ADD(period_end,INTERVAL 1 YEAR) 
	    WHERE period_start = period_end AND id_right IN(
	       SELECT id FROM absences_rights WHERE kind='16')");
	$babDB->db_query("UPDATE absences_rights SET date_end=DATE_ADD(date_end,INTERVAL 1 YEAR) 
	    WHERE kind='16' AND date_begin=date_end");
	
	
	// reprise des declaration de jours travaille approuvees par auto-approbation sans droit cree
	// avant la version 2.67 id_right n'existait pas
	// Utiliser ce code seulement si il y a des droit a recuperation manquant, il peut etre dangereux si le format du nom du droit change
	// la method restoreMissingRight cree le droit manquant ou cree la liaison manquante
	
	/*
	$res = $babDB->db_query("SELECT * FROM absences_workperiod_recover_request WHERE id_right='0' AND status='Y' AND modifiedOn>'2015-05-30 00:00:00'");
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
	while ($arr = $babDB->db_fetch_assoc($res)) {
	    $request = new absences_WorkperiodRecoverRequest();
	    $request->setRow($arr);
	    $request->restoreMissingRight();
	}
	*/
	
	
	
	
	$babDB->db_query("UPDATE absences_cet_deposit_request SET firstconfirm='1' WHERE status='Y'");
	$babDB->db_query("UPDATE absences_workperiod_recover_request SET firstconfirm='1' WHERE status='Y'");
	$babDB->db_query("UPDATE absences_entries SET firstconfirm='1' WHERE status='Y'");
	
	
	// T9250 Export des soldes a une date donnee
	
	$babDB->db_query("UPDATE absences_rights SET createdOn=date_entry WHERE createdOn='0000-00-00 00:00:00'");
	
	
	
	// verification de l'historique des status
	
	absences_fixRequestLastMovement('absences_entries', 'absences_Entry');
	absences_fixRequestLastMovement('absences_workperiod_recover_request', 'absences_WorkperiodRecoverRequest');
	absences_fixRequestLastMovement('absences_cet_deposit_request', 'absences_CetDepositRequest');
	
	absences_deleteAgentsWithNoUser();
	absence_rePrimary();
	
	return true;
}
