<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';












class absences_EntitiesCls
{
    var $altbg = true;

    public function __construct($entities)
    {
        $id_oc = absences_getVacationOption('id_chart');
         
        $this->all_manager = false;
        $this->entities = $this->inheritCoManager($entities, $this->all_manager);
        bab_Sort::asort($this->entities, 'name', bab_Sort::CASE_INSENSITIVE);
        
        $this->t_name = absences_translate('Name');
        $this->t_description = absences_translate('Description');
        $this->t_members = absences_translate('Members');
        $this->t_calendar = absences_translate('Planning');
        $this->t_requests = absences_translate('Requests');
        $this->t_planning = absences_translate('Planning acces');
        $this->t_comanager = absences_translate('Co-managers');
    }
    
    
    
    /**
     * Inherit co-manager on subentities
     * @param array $entities
     * @param bool &$all_manager
     * @return array
     */
    protected function inheritCoManager($entities, &$all_manager)
    {
        $id_oc = absences_getVacationOption('id_chart');
         
        $all_manager = false;
        $new_entities = array();
        while (list(,$arr) = each($entities))
        {
            if (!isset($arr['comanager'])) {
                $all_manager = true;
            }
    
            if (!isset($this->entities[$arr['id']])) {
                $new_entities[$arr['id']] = $arr;
            }
            $arr2 = bab_OCGetChildsEntities($arr['id'], $id_oc);
            for ($i = 0 ; $i < count($arr2) ; $i++) {
                if (isset($arr['comanager'])) {
                    $arr2[$i]['comanager'] = 1;
                }
    
                if (!isset($this->entities[$arr2[$i]['id']])) {
                    $new_entities[$arr2[$i]['id']] = $arr2[$i];
                }
            }
        }
    
        return $new_entities;
    }



    public function getnext()
    {
        if (list(,$this->arr) = each($this->entities))
        {
            $this->manager 				= !isset($this->arr['comanager']);
            $this->altbg 				= !$this->altbg;
            $this->arr['name'] 			= bab_toHtml($this->arr['name']);
            $this->arr['description'] 	= bab_toHtml($this->arr['description']);
            return true;
        }
        else
            return false;
    }
}






/**
 * List of entities
 * @param array $entities   Managed entities
 */
function absences_entities($entities)
{
    $babBody = bab_getBody();


    $temp = new absences_EntitiesCls($entities);
    $babBody->babecho(absences_addon()->printTemplate($temp, "vacchart.html", 'entities'));

}








function entity_members($ide, $template)
{
	global $babBody;

	class temp
		{
		var $altbg = true;

		function temp($ide)
			{
			$this->ide = $ide;
			$users = bab_OCGetCollaborators($ide);
			$superior = bab_OCGetSuperior($ide);
			$this->superior_id = 0;
			if ($superior !== 0 )
				{
				$this->superior_id = $superior['id_user'];
				$this->superior_name = bab_toHtml($superior['lastname'].' '.$superior['firstname']);
				}
			$this->b_rights = ($this->superior_id != $GLOBALS['BAB_SESS_USERID']);
			$this->set_rights = (bool) absences_getVacationOption('chart_superiors_set_rights');
			
			// si co-gestionnaire de cette entite, pas de droit sur le supperieur

			if (absences_isAccessibleEntityAsCoManager($this->ide)) {
				$this->b_rights = false;
			}
			
			$this->t_name = absences_translate('Name');
			$this->t_calendar = absences_translate('Planning');
			$this->t_rights = absences_translate('Rights');
			$this->t_asks = absences_translate('Requests');
			$this->t_view_calendar = absences_translate('View calendars');
			$this->t_collection = absences_translate('Collection');
			$this->t_schema = absences_translate('Approbation schema');
			$this->t_request = absences_translate('Request');
			$this->t_viewrights = absences_translate('Balance');
			$this->checkall = absences_translate('Check all');
			$this->uncheckall = absences_translate('Uncheck all');
			$this->t_not_in_personel = absences_translate('This user is not in personel members');

			$this->requests = absences_getVacationOption('chart_superiors_create_request');
			
			$this->users = array();
			
			while (list(,$arr) = each($users))
				{
				if ($arr['id_user'] != $this->superior_id)
					{
					$this->users[$arr['id_user']] = $arr['lastname'].' '.$arr['firstname'];
					}
				}
			bab_sort::natcasesort($this->users);

			if (count($this->users) > 0)
				{
				$tmp = array_keys($this->users);
				$tmp[] = $this->superior_id;
				}
			elseif (!empty($this->superior_id))
				{
				$tmp = array($this->superior_id);
				}
			else
				$tmp = array();


			if (count($tmp) > 0)
				{
				$this->more = array();

				global $babDB;
				$req = "SELECT p.id_user,c.name coll,f.name sa FROM ".ABSENCES_PERSONNEL_TBL." p LEFT JOIN ".ABSENCES_COLLECTIONS_TBL." c ON c.id=p.id_coll LEFT JOIN ".BAB_FLOW_APPROVERS_TBL." f ON f.id=p.id_sa WHERE p.id_user IN(".$babDB->quote($tmp).")";
				$res = $babDB->db_query($req);
				while ($arr = $babDB->db_fetch_array($res))
					{
					$this->more[$arr['id_user']] = array( $arr['coll'], $arr['sa'] );
					}
				}

			$this->s_collection = '';
			$this->s_schema = '';
			if ($superior !== 0 && isset($this->more[$this->superior_id]))
				{
				list($this->s_collection, $this->s_schema ) = $this->more[$this->superior_id] ;
				$this->s_collection = bab_toHtml($this->s_collection);
				$this->s_schema = bab_toHtml($this->s_schema);
				}
			}

		function getnext()
			{
			if (list($this->id_user,$this->name) = each($this->users))
				{
				$this->altbg = !$this->altbg;
				$this->b_rights = absences_canChartEditRights($this->id_user);
				$this->collection = isset($this->more[$this->id_user][0]) ? bab_toHtml($this->more[$this->id_user][0]) : '';
				$this->schema = isset($this->more[$this->id_user][1]) ? bab_toHtml($this->more[$this->id_user][1]) : '';
				$this->name = bab_toHtml($this->name);
				
				return true;
				}
			else
				return false;
			}
		}

	$temp = new temp($ide);
	
	$entity = bab_OCGetEntity($ide);
	
	$babBody->setTitle(sprintf(absences_translate('Members of entity "%s"'), $entity['name']));
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacchart.html", $template));
	
}




function entity_users($ide)
{
	$users = bab_OCGetCollaborators($ide);
	$superior = bab_OCGetSuperior($ide);

	$tmp = array();
	foreach ($users as $user)
		{
		$tmp[$user['id_user']] = $user['id_user'];
		}

	if (!isset($tmp[$superior['id_user']]) && !empty($superior['id_user']))
		$tmp[$superior['id_user']] = $superior['id_user'];

	return array_keys($tmp);
}

function entity_requests($ide )
{
	global $babBody;
	$entity = bab_OCGetEntity($ide);
	
	$babBody->setTitle(sprintf(absences_translate('Vacation requests by members of entity "%s"'), $entity['name']));
	
	
	absences_listVacationRequests(entity_users($ide), true, 1, $ide);
}



function entity_comanager($ide) {
	$e =  bab_OCGetEntity($ide);
	$GLOBALS['babBody']->setTitle(absences_translate("Co-managers").' : '.$e['name']);

	include_once $GLOBALS['babInstallPath'].'utilit/selectusers.php';
	global $babBody, $babDB;
	$obj = new bab_selectusers();
	$obj->addVar('ide', $ide);
	$res = $babDB->db_query("SELECT id_user FROM ".ABSENCES_COMANAGER_TBL." WHERE id_entity=".$babDB->quote($ide));
	while (list($id) = $babDB->db_fetch_array($res))
		{
		$obj->addUser($id);
		}
	$obj->setRecordCallback('saveCoManager');
	$babBody->babecho($obj->getHtml());

}


function viewVacUserDetails($ide, $id_user) {
	
	require_once dirname(__FILE__).'/utilit/agent.ui.php';
	
	
	$agent = absences_Agent::getFromIdUser($id_user);
	
	$W = bab_Widgets();
	$page = $W->BabPage();
	$frame = $W->Frame(null , $W->VBoxLayout()->setVerticalSpacing(2,'em'))
		->addClass('widget-bordered')
		->addClass('BabLoginMenuBackground')
		->addClass('widget-centered')
		->addClass(Func_Icons::ICON_LEFT_24);
		
	$frame->setCanvasOptions($frame->Options()->width(70,'em'));
	
	$page->addItem($frame);
	
	
	$url = bab_url::get_request('tg', 'ide', 'iduser');
	$url->idx = 'modp';
	
	if ($id_user != $GLOBALS['BAB_SESS_USERID'] && absences_getVacationOption('chart_superiors_user_edit'))
	{
		$frame->addItem($W->Link($W->Icon(absences_translate("Modify"), Func_Icons::ACTIONS_DOCUMENT_EDIT), $url->toString()));
	}
	
	$frame->addItem(new absences_AgentCardFrame($agent));
	
	
	$page->displayHtml();
	
	


}





function saveCoManager($userids, $params) {

	$ide = $params['ide'];
	global $babDB;
	$babDB->db_query("DELETE FROM ".ABSENCES_COMANAGER_TBL." WHERE id_entity = ".$babDB->quote($ide));

	foreach ($userids as $uid)
	{
		$babDB->db_query("INSERT INTO ".ABSENCES_COMANAGER_TBL." (id_user, id_entity) VALUES ('".$babDB->db_escape_string($uid)."','".$babDB->db_escape_string($ide)."')");
	}
	
	header('location:'.absences_addon()->getUrl()."vacchart&idx=entities");
	exit;
}



/**
 * 
 * @param int $id_user
 * @return boolean
 */
function absences_canChartEditRights($id_user)
{
	return ($id_user != bab_getUserId() && absences_getVacationOption('chart_superiors_set_rights') && absences_IsUserUnderSuperior($id_user));
}



function absences_updateVacationChartPersonnel($id_user)
{
    $update = absences_updateVacationPersonnel($id_user);
    if(true === $update) {
        return 'changeucol';
    }
    
    if (false === $update) {
        return 'modp';
    }

    $url = bab_url::get_request('tg', 'ide');
    $url->idx = 'entity_members';
    $url->location();
}



// main
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
$userentities = $agent->getManagedEntities();
$entities_access = count($userentities);



$idx = bab_rp('idx', 'entities');


if( isset($_POST['add']) && $entities_access > 0 )
	{
	switch($_POST['add'])
		{
		case 'modrbu':
			if ( absences_canChartEditRights($_POST['idu']) )
			{
				if (absences_updateVacationRightByUser($_POST['idu'], $_POST['quantity'], bab_pp('comment')))
				{
					bab_url::get_request('tg', 'idx', 'ide', 'idu')->location();
				}
			}
			break;

		case 'changeuser':
			if (!absences_getVacationOption('chart_superiors_user_edit'))
			{
				break;
			}
			if (!empty($_POST['idp'])) {
				$idx = absences_updateVacationChartPersonnel($_POST['idp']);
			}
			else
			{
				try {
					
					$messages = array();
					if( !absences_saveVacationPersonnel(bab_pp('userid'), bab_pp('idcol'), bab_pp('idsa'), bab_pp('id_sa_cet'), bab_pp('id_sa_recover'), bab_pp('emails'), $messages))
					{
					$idx ='addp';
					}
				} catch (Exception $e)
				{
					$babBody->addError($e->getMessage());
					$idx ='addp';
				}
				
				if (!empty($messages))
				{
					/*@var $babBody babBody */
					foreach($messages as $message)
					{
						$babBody->addMessage($message);
					}
				}
			}
			break;
		

		case 'changeucol':
			if (!absences_getVacationOption('chart_superiors_user_edit'))
			{
				break;
			}
			if (!absences_updateUserColl())
				$idx = $add;
			break;
		}
	}
	
if (!bab_rp('popup'))
{	
    if ($agent->isInPersonnel())
    {
    	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
    }
    
    if( $agent->isManager())
    {
    	$babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
    }
    
    if ($agent->isEntityManager())
    {
    	$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
    }
}



switch($idx)
	{
	case 'lper':
		$idx = 'entity_members';
	case 'entity_members':
		
		$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".bab_rp('ide'));
		if ($entities_access > 0)
			entity_members(bab_rp('ide'), 'entity_members');
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case 'planning_members':
		if (absences_isPlanningAccessValid())
			{
			$babBody->title = absences_translate("Entity members");
			$babBody->addItemMenu("planning", absences_translate("Plannings"), absences_addon()->getUrl()."planning&idx=userlist");
			$babBody->addItemMenu("planning_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=planning_members");
			entity_members($_REQUEST['ide'], 'planning_members');
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;



	case 'rights':

		if (absences_canChartEditRights(bab_rp('idu')))
			{
    			if (!bab_rp('popup'))
    			{
    		    	$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".bab_rp('ide'));
    		    	$babBody->addItemMenu("rights", absences_translate("Rights"), absences_addon()->getUrl()."vacchart&idx=rights&idu=".bab_rp('idu').'&ide='.bab_rp('ide'));
    			}
    			absences_listRightsByUser(bab_rp('idu'));
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case "rlbuul":
		absences_rlistbyuserUnload(absences_translate("Your request has been updated"));
		exit;

	case 'asks':
		$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_GET['ide']);
		if (absences_IsUserUnderSuperior($_GET['id_user']))
			{

			$babBody->title = absences_translate("Vacation requests list");
			$babBody->addItemMenu("asks", absences_translate("Requests"), absences_addon()->getUrl()."vacchart&idx=asks");
			absences_listVacationRequests($_GET['id_user'], false, 1, bab_rp('ide'));
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case 'entity_requests':

		if ($entities_access > 0)
			{
			$babBody->addItemMenu("entity_requests", absences_translate("Requests"), absences_addon()->getUrl()."vacchart&idx=entity_requests");
			entity_requests($_GET['ide']);
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;
		



	case 'comanager':
		$babBody->addItemMenu("comanager", absences_translate("Co-managers"), absences_addon()->getUrl()."vacchart&idx=comanager");

		$ide = bab_rp('ide');
		
		if ($entities_access > 0 && !empty($ide))
			entity_comanager($ide);
		break;

	case 'view':
		$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_GET['ide']);
		
		if (absences_IsUserUnderSuperior($_GET['iduser']) && $_GET['iduser'] != $GLOBALS['BAB_SESS_USERID'])
			{
			$babBody->addItemMenu("view", absences_translate("User"), absences_addon()->getUrl()."vacchart&idx=view&ide=".$_GET['ide']);
			$babBody->title = bab_getUserName($_GET['iduser']);
			viewVacUserDetails($_GET['ide'], $_GET['iduser']);
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case "modp":
		$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_GET['ide']);
		
		if (absences_IsUserUnderSuperior($_REQUEST['iduser']) && $_GET['iduser'] != $GLOBALS['BAB_SESS_USERID'])
			{
			$babBody->addItemMenu("modp", absences_translate("Modify"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_GET['ide']);
			$babBody->title = absences_translate("Modify user");
			absences_addVacationPersonnel($_REQUEST['iduser']);
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case 'changeucol':
		$babBody->addItemMenu("entity_members", absences_translate("Entity members"), absences_addon()->getUrl()."vacchart&idx=entity_members&ide=".$_REQUEST['ide']);
		if (absences_IsUserUnderSuperior($_POST['idp']) && $_POST['idp'] != $GLOBALS['BAB_SESS_USERID'])
			{
			$babBody->addItemMenu("changeucol", absences_translate("Modify"), absences_addon()->getUrl()."vacchart&idx=changeucol&ide=".$_REQUEST['ide']);
			$babBody->title = absences_translate("Change user collection");
			absences_changeucol( $_POST['idp'], $_POST['idcol'] );
			}
		else
			{
			$babBody->addError(absences_translate("Access denied"));
			}
		break;

	case 'planning':
		if (absences_isPlanningAccessValid())
			{
			$babBody->addItemMenu("planning", absences_translate("Plannings"), absences_addon()->getUrl()."planning&idx=userlist");
			$babBody->title = absences_translate("Planning list");
			absences_accessible_plannings($userentities);
			}
		break;

	default:
	case 'entities':
		if ($entities_access > 0)
		{
			$babBody->title = absences_translate("Entities list");
			absences_entities($userentities);
		}
		break;
	}

$babBody->setCurrentItemMenu($idx);

?>