<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/right.class.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';
require_once dirname(__FILE__).'/utilit/sync.ui.php';




function absences_syncServerSave($values)
{
	global $babDB;
	
	// uncheck non-visible rights
	
	$I = new absences_RightIterator();
	foreach($I as $right)
	{
		/*@var $right absences_Right */
		
		if (!isset($values[$right->id]) && absences_Right::SYNC_SERVER === $right->getSyncStatus())
		{
			$babDB->db_query('UPDATE absences_rights SET sync_status='.$babDB->quote(0).', date_entry=NOW() WHERE id='.$babDB->quote($right->id));
		}
	}
	
	
	foreach($values['right'] as $id_right => $checked)
	{
		$status = $checked ? absences_Right::SYNC_SERVER : 0;
		$babDB->db_query('UPDATE absences_rights SET sync_status='.$babDB->quote($status).', date_entry=NOW() WHERE id='.$babDB->quote($id_right));
	}
	
	// redirect to form page
	
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
	$url = bab_url::get_request('tg');
	$url->location();
}



function absences_syncServerEdit()
{
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	if (isset($_POST['sync_server']))
	{

		if( isset($_POST['sync_server']['save'] ))
		{
	
			// modification uniquement
	
			$values = $_POST['sync_server'];
			absences_syncServerSave($values);
		}
	}
	

	
	$editor = new absences_SyncServerEditor();
	
	$page->setTitle(absences_translate('Edit the shared vacation rights'));
	$page->addItem($editor);
	$page->displayHtml();
}





/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");

$idx = bab_rp('idx', "edit");


switch($idx)
{
	case 'edit':
		absences_syncServerEdit();
		$babBody->addItemMenu("edit", absences_translate("Edit"), absences_addon()->getUrl()."sync_server&idx=edit");
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');