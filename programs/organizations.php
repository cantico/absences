<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/organization.ui.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/organization.class.php';

/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
	{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

$idx = bab_rp('idx', 'list');




function absences_Organizations()
{
    $babBody = bab_getBody();
    
    $babBody->setTitle(absences_translate('Organizations list'));
    $babBody->babecho(absences_organizationMenu());
    
    $template = new absences_OrganizationList();
    $addon = bab_getAddonInfosInstance('absences');
    $babBody->babecho($addon->printTemplate($template, 'organizationslist.html'));
}


function absences_saveOrganization()
{
    require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
    
    $id = bab_pp('id_organization');
    $posted = bab_pp('organization');
    
    if ($id) {
        $organization = absences_Organization::getById($id);
    } else {
        $organization = new absences_Organization();
    }
    
    $organization->name = $posted['name'];
    $organization->save();
    
    bab_url::get_request('tg')->location();
}


function absences_editOrganization()
{
    if (isset($_POST['organization'])) {
        return absences_saveOrganization();
    }
    
    $W = bab_Widgets();
    $id_organization = (int) bab_rp('id_organization');
    
    if ($id_organization) {
        $organization = absences_Organization::getById($id_organization);
    } else {
        $organization = new absences_Organization();
        $organization->setRow(array());
    }
    
    $page = $W->BabPage();
    $page->setTitle(absences_translate('Edit organization'));
    
    $form = new absences_OrganizationEditor($organization);
    $page->addItem($form);
    
    $page->displayHtml();
}



function absences_deleteOrganization()
{
    $id = (int) bab_rp('id_organization');
    if (!$id) {
        return false;
    }
    
    $organization = absences_Organization::getById($id);
    if (!$organization->delete()) {
        return false;
    }
    
    bab_url::get_request('tg')->location();
}


// MAIN

if ($agent->isInPersonnel())
{
    $babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}

if( $agent->isEntityManager())
{
    $babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart");
}

$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
$babBody->addItemMenu('list', absences_translate("Organizations"), absences_addon()->getUrl()."organizations&idx=list");


switch($idx)
{
    case 'delete':
        absences_deleteOrganization();
        break;
    
    case 'edit':
        absences_editOrganization();
        break;
    
    default:
    case 'list':
        absences_Organizations();
    break;
}
$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','Admin');