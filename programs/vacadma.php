<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/utilit/vacfixedincl.php';
require_once dirname(__FILE__).'/utilit/right.class.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';



function listVacationRigths($idtype, $idcoll, $rgroup, $dateb, $datee, $active, $pos, $archived, $recovery)
{
	global $babBody;
	require_once dirname(__FILE__).'/utilit/rightlist.ui.php';

	if ($recovery) {
		$babBody->setTitle(absences_translate("Recovery rights"));
	}	
	else if ($archived) {
		$babBody->setTitle(absences_translate("Archived vacations rights"));
	} else {
		$babBody->setTitle(absences_translate("Vacations rights"));
	}
		
	$babBody->babecho(absences_rightListMenu());
	
	$name = bab_rp('name');

	$temp = new absences_RightList($idtype, $idcoll, $rgroup, $dateb, $datee, $active, $pos, $archived, $recovery, $name);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "vrightslist"));
	return $temp->count;

}



function absences_choseBeneficiariesMethod($id)
{
	bab_functionality::includeOriginal('Icons');
	$W = bab_Widgets();
	
	$page = $W->BabPage();
	$page->setTitle(absences_translate('Vacation right beneficiaries'));
	$right = new absences_Right($id);
	$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
	$page->addItem($frame);
	$frame->addClass('widget-bordered');
	$frame->addClass('BabLoginMenuBackground');
	$frame->addClass('widget-centered');
	$frame->setCanvasOptions($frame->Options()->width(60,'em'));
	
	$frame->addItem($W->Title($right->description, 4));
	
	$I = $right->getAgentRightIterator();
	
	if (0 === $I->count())
	{
		$frame->addItem($W->Label(absences_translate('This right has no beneficiaries, please use collections or personnel members to add beneficiaries to the vacation right')));
	}
	
	$buttons = $W->FlowLayout()->setSpacing(2,'em');
	$frame->addItem($buttons);
	
	$buttons->addItem(
		$W->Link(
			$W->Button()->addItem($W->Icon(absences_translate('By collections'), Func_Icons::ACTIONS_USER_GROUP_PROPERTIES)),
			absences_addon()->getUrl().'vacadma&idx=lvrc&idvr='.$id
		)
	);
	
	$buttons->addItem(
			$W->Link(
					$W->Button()->addItem($W->Icon(absences_translate('By users'), Func_Icons::ACTIONS_USER_PROPERTIES)),
					absences_addon()->getUrl().'vacadma&idx=lvrp&idvr='.$id
			)
	);
	
	
	$page->displayHtml();
}




function absences_addModifyVacationRight($id = false, $from = false)
{
	
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	require_once dirname(__FILE__).'/utilit/right.act.php';
	
	
	if (isset($_POST['right']))
	{
		if ( isset($_POST['right']['cancel']) )
		{
			absences_RightAct::redirect();
		}
		
		
		if( isset($_POST['right']['save'] ))
		{
			absences_RightAct::save();
		}
		
		if( isset($_POST['right']['delete']))
		{
			deleteVacationRight($_POST['right']['id']);
		}
		
		
	}
	
	$W = bab_Widgets();
	
	$page = $W->BabPage();
	$right = null;
	
	if ($id)
	{
		$right = new absences_Right($id);
		$page->setTitle(absences_translate('Edit vacation right'));
		$editor = new absences_RightEditor($right);
	} else {
	    
	    if ($from) {
	        $right = new absences_Right($from);
	    }
	    
		$page->setTitle(absences_translate('Create vacation right'));
		$editor = new absences_CreateRightEditor($right);
	}
	
	
	$page->addItem($editor);
	
	
	$page->displayHtml();
}






function absences_fixedRightUpdate($id)
{
	
	$W = bab_Widgets();
	
	$page = $W->BabPage();
	
	$right = new absences_Right($id);
	
	
	if (!$right->getRow())
	{
		throw new Exception(absences_translate('The vacation right does not exists'));
	}
	
	if (absences_Right::FIXED !== $right->getKind())
	{
		throw new Exception(absences_translate('The vacation right is not fixed'));
	}
	
	
	$page->setTitle(sprintf(absences_translate('Update requests for the fixed vacation right "%s"'), $right->description));
	
	$nextbutton = $W->Button()->addItem($W->Label(absences_translate('Next')));
	
	$page->addItem(
		$W->Frame()
		->setCanvasOptions($page->Options()->width(70,'em'))
		->addClass('widget-bordered')
		->addClass('BabLoginMenuBackground')
		->addClass('widget-centered')
		->addItem(
			$W->ProgressBar()
				->setTitle(absences_translate('Process beneficiaries list'))
				->setProgressAction($W->Action()->fromRequest()->setParameter('idx', 'fixedud_progress'))
				->setCompletedAction($W->Action()->fromRequest()->setParameter('idx', 'lrig'), $nextbutton)
		)
		->addItem($nextbutton)
	);
	
	
	$page->displayHtml();
}




function absences_fixedRightUpdateProgress($id)
{
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	require_once dirname(__FILE__).'/utilit/right.act.php';
	
	$W = bab_Widgets();
	
	$right = new absences_Right($id);
	
	if (absences_Right::FIXED !== $right->getKind())
	{
		throw new Exception(absences_translate('The vacation right is not fixed'));
	}
	
	$progress = $W->ProgressBar();
	
	absences_RightAct::updateFixed($right, $progress);
	die();
}



function absences_beneficiariesUpdateProgress($id)
{
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	require_once dirname(__FILE__).'/utilit/right.act.php';
	
	$W = bab_Widgets();
	
	$right = new absences_Right($id);
	
	$progress = $W->ProgressBar();
	
	absences_RightAct::updateCollectionsBeneficiaries($right, $progress, (array) bab_gp('collection', array()));
	die();
}





/**
 * 
 * @param int $id_right
 * @param string $next_idx
 */
function absences_userRightReport($id_right, $next_idx)
{
	require_once dirname(__FILE__).'/utilit/collection.class.php';
	$W = bab_Widgets();

	$page = $W->BabPage();

	$right = new absences_Right($id_right);
	
	if (!$right->getRow())
	{
		throw new Exception(absences_translate('The vacation right does not exists'));
	}



	$page->setTitle(sprintf(absences_translate('Error report for right "%s"'), $right->description));
	
	
	$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1.5,'em'))
		->setCanvasOptions($page->Options()->width(70,'em'))
		->addClass('widget-bordered')
		->addClass('BabLoginMenuBackground')
		->addClass('widget-centered');
	
	$page->addItem($frame);
	

	// si aucun probleme a signaler, diriger le gestionnaire vers la page droit/regime
	
	
	$I = $right->getAgentRightIterator();
	$display = false;
	
	foreach($I as $agentRight)
	{
		/*@var $agentRight absences_AgentRight */
		$errors = $agentRight->getErrors();
		
		if (empty($errors))
		{
			continue;
		}
		
		$display = true;
		
		$messageLayout = $W->VBoxLayout();
		$user = $W->Section($agentRight->getAgent()->getName(), $messageLayout);
		$frame->addItem($user);
		
		foreach($errors as $error)
		{
			$messageLayout->addItem($W->Label($error));
		}
	}
	
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	$url = bab_url::get_request('tg', 'pos');
	$url->idx = $next_idx;
	$url->idvr = $id_right;
	
	
	if  (!$display)
	{
		global $babBody;
		/* @var $babBody babBody */
		
		// reporter les messages a la page suivante
		foreach($babBody->messages as $m)
		{
			$babBody->addNextPageMessage($m);
		}
		
		$url->location();
	}
	
	
	$frame->addItem($W->Link($W->Button()->addItem($W->Label(absences_translate('Continue'))), $url->toString()));
	

	$page->displayHtml();
}










/**
 * 
 * @param int $id_group
 * @param bool $tree
 * 
 * @return array
 */
function absences_getUsersFromGroup($id_group, $tree)
{
	$groups = array($id_group);
	
	if ($tree)
	{
		$arr = bab_getGroups($id_group, true);
		$groups = array_merge($groups, $arr['id']);
	}
	
	$members = bab_getGroupsMembers($groups);
	$users = array();
	foreach($members as $arr)
	{
		$users[] = (int) $arr['id'];
	}
	
	return $users;
}



function absences_addVacationRightPersonnel($id)
{
	$W = bab_Widgets();
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	global $babDB;
	
	$page = $W->BabPage();
	
	if (isset($_POST['right']))
	{	
	
		if( isset($_POST['right']['save'] ))
		{
			$id_right = (int) $_POST['right']['id'];
			$id_group = (int) $_POST['right']['group'];
			$tree = (bool) $_POST['right']['tree'];
			$users = absences_getUsersFromGroup($id_group, $tree);
			$right = new absences_Right($id_right);
			
			// croiser avec les membres du personnel
			
			$res = $babDB->db_query('SELECT * FROM absences_personnel WHERE id_user IN('.$babDB->quote($users).')');
			while ($arr = $babDB->db_fetch_assoc($res))
			{
				$agent = new absences_Agent;
				$agent->setRow($arr);
				if ($agent->addRight($right))
				{
					try {
						$agent->addFixedEntry($right);
					} 
					catch(absences_EntryException $e)
					{
						$page->addError($agent->getName().' : '.$e->getMessage());
						$agent->removeRight($right);
					}
				}
			}
		}
		
		if (!$page->containErrors())
		{
			$url = bab_url::get_request('tg', 'idvr');
			$url->idx = 'lvrp';
			$url->location();
		}
	}
	
	
	
	
	
	
	$right = new absences_Right($id);
	
	
	if (!$right->getRow())
	{
		throw new Exception(absences_translate('The vacation right does not exists'));
	}
	
	$page->setTitle(sprintf(absences_translate('Add personnel members to the right %s'), $right->description));
	

	$editor = new absences_RightAddPersonnelEditor($right);
	$page->addItem($editor);
	
	
	$page->displayHtml();
}











function listVacationRightPersonnel($idvr)
{
    require_once dirname(__FILE__).'/utilit/beneficiarieslist.ui.php';
    
	$list = new absences_BeneficiariesList($idvr, (array) bab_rp('filter', array()));
	
	$addon = bab_getAddonInfosInstance('absences');

	$babBody = bab_getInstance('babBody');
	$babBody->addStyleSheet($addon->getStylePath().'vacation.css');
	$babBody->babecho(absences_addon()->printTemplate($list, "vacadma.html", "vrpersonnellist"));
}
	
	
	
	
	
	
	
	
function listVacationRightCollection($idvr)
{
	
	class temp
	{

		public $altbg = true;
		public $name;
		public $description;
		public $idvr;

		public function __construct($idvr)
		{
			require_once dirname(__FILE__).'/utilit/collection.class.php';
			require_once dirname(__FILE__).'/utilit/collection_right.class.php';
			require_once dirname(__FILE__).'/utilit/right.class.php';

			$babBody = bab_getInstance('babBody');

			$this->allname = absences_translate("All");
			$this->uncheckall = absences_translate("Uncheck all");
			$this->checkall = absences_translate("Check all");
			$this->modify = absences_translate("Modify");
			$this->t_name = absences_translate("Name");
			$this->t_description = absences_translate("Description");
			$this->t_linked = absences_translate("Linked");
			$this->t_unlinked = absences_translate("Unlinked");
			$this->t_edit = absences_translate("Edit");

			global $babDB;
			$this->idvr = (int) $idvr;

			
			$this->right = new absences_Right($idvr);

			$babBody->setTitle(sprintf(absences_translate("Collections associated to the right %s"), $this->right->description));
			$type = $this->right->getType();
			
			if ($type->id)
			{
				$this->right_type = bab_toHtml($type->name);
			} else {
				$this->right_type = '';
			}

			$req = "select *
				from
					absences_collections 
				order by name";
			
			
			$this->res = $babDB->db_query($req);
		}

		public function getnext()
		{
			global $babDB;
			if($arr = $babDB->db_fetch_array($this->res))
			{

				
				$this->altbg = !$this->altbg;

				// create the agent and the agent_right objects

				$collection = new absences_Collection();
				$collection->setRow($arr);


				$this->selected = "";

				$res2 = $babDB->db_query("select * from absences_coll_rights where id_coll='".$babDB->db_escape_string($collection->id)."' AND id_right ='".$babDB->db_escape_string($this->idvr)."'");
				if( $res2 && $babDB->db_num_rows($res2) > 0 )
				{
					$this->selected = "checked";
				}
				

				$this->id_collection = bab_toHtml($collection->id);
				$this->name = bab_toHtml($collection->name);
				$this->description = bab_toHtml($collection->description);

				return true;
			}
			else
				return false;

		}

		
		
		
		public function displaySaveProgress()
		{
			$W = bab_Widgets();
			
			$page = $W->BabPage();

			
			$page->setTitle(sprintf(absences_translate('Update users associated to vacation right "%s"'), $this->right->description));
			
			$nextbutton = $W->Button()->addItem($W->Label(absences_translate('Next')));
			
			$checked_collections = bab_pp('collection', array());
			
			$page->addItem(
					$W->Frame()
					->setCanvasOptions($page->Options()->width(70,'em'))
					->addClass('widget-bordered')
					->addClass('BabLoginMenuBackground')
					->addClass('widget-centered')
					->addItem(
							$W->ProgressBar()
							->setProgress(0)
							->setTitle(absences_translate('Process beneficiaries list'))
							->setProgressAction($W->Action()->fromRequest()->setParameter('idx', 'lvrc_progress')->setParameter('collection', $checked_collections))
							->setCompletedAction($W->Action()->fromRequest()->setParameter('idx', 'lvrc'), $nextbutton)
					)
					->addItem($nextbutton)
			);
			
			
			$page->displayHtml();
		}
	}
	

	$temp = new temp($idvr);
	
	
	if (!empty($_POST))
	{
		// $temp->save();
		$temp->displaySaveProgress();
		return;
	}
	

	$babBody = bab_getInstance('babBody');
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "vrcollectionlist"));
}
	
	
	
	

function viewVacationRightPersonnel($idvr)
{
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	
	
	
	$W = bab_Widgets();
	
	$page = $W->BabPage();
	$right = null;
	
	
	$right = new absences_Right($idvr);
	$page->setTitle(absences_translate('View vacation right'));

	$fullframe = new absences_RightFullFrame($right);
	$page->addItem($fullframe);
	
	
	$page->displayHtml();
}



function rgrouplist() {

	global $babBody;
	class temp
		{
		var $altbg = true;

		function temp()
			{
			$this->t_name = absences_translate('Name');
			$this->t_edit = absences_translate('Edit');
			$this->t_rights = absences_translate('Rights');
			global $babDB;
			$this->res = $babDB->db_query("SELECT * FROM ".ABSENCES_RGROUPS_TBL."");
			}

		function getnext()
			{
			global $babDB;
			if ($arr = $babDB->db_fetch_assoc($this->res)) {
				$this->altbg		= !$this->altbg;
				$this->name			= bab_toHtml($arr['name']);
				$this->id_rgroup	= bab_toHtml($arr['id']);


				$this->rgroup = $babDB->db_query("SELECT description FROM ".ABSENCES_RIGHTS_TBL." WHERE id_rgroup=".$babDB->quote($arr['id']));
				return true;
			}
			return false;
		}

		function getnextright() {
			global $babDB;
			
			static $i = 0;
			
			if ($i === 10) {
			    $this->description = bab_toHtml(
		            sprintf(
		                absences_translate('And %d more vacation rights...'),
		                $babDB->db_num_rows($this->rgroup)
		            )
		        );
			    
			    $i++;
			    return true;
			}
			
			if ($i > 10) {
			    return false;
			}
			
			
			
			if ($arr = $babDB->db_fetch_assoc($this->rgroup)) {
				$this->description = bab_toHtml($arr['description']);
				$i++;
				
				
				return true;
			}
			
			$i = 0;
			return false;
		}

	}

	$temp = new temp();
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "rgrouplist"));
}



function rgroupmod() {

	global $babBody;
	class temp
	{
		function temp()
		{
			$this->t_name = absences_translate('Name');
			$this->t_recover = absences_translate('Use this right group to store the created recovery rights');
			$this->t_quantity_unit = absences_translate('Allowed quantity unit');
			$this->t_days = absences_translate('Days');
			$this->t_hours = absences_translate('Hours');
			$this->t_record = absences_translate('Record');
			$this->t_delete = absences_translate('Delete');
			global $babDB;
			$this->id_rgroup = bab_rp('id_rgroup');
			if ($this->id_rgroup) {
				$res = $babDB->db_query("SELECT * FROM ".ABSENCES_RGROUPS_TBL." WHERE id=".$babDB->quote($this->id_rgroup));
				$arr = $babDB->db_fetch_assoc($res);
				$this->name = bab_toHtml($arr['name']);
				$this->recover = $arr['recover'];
				$this->quantity_unit = bab_toHtml($arr['quantity_unit']);
			} else {
				$this->name = '';
				$this->recover = 0;
				$this->quantity_unit = 'D';
			}
		}
	}

	$temp = new temp();
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "rgroupmod"));
}




function modifyVacationRightPersonnel($idvr, $userids, $nuserids)
{
	
	global $babDB;
	require_once dirname(__FILE__).'/utilit/right.class.php';
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	
	$babBody = bab_getInstance('babBody');
	/*@var $babBody babBody */
	$count = sizeof($userids);
	
	$right = new absences_Right($idvr);

	if (!$right->getRow())
	{
		throw new Exception('This vacation right does not exists');
	}

	for( $i = 0; $i < sizeof($nuserids); $i++)
	{
	    bab_setTimeLimit(10);
	    
		if( $nuserids[$i] != "" && ( $count == 0 || !in_array($nuserids[$i], $userids)))
		{
			$agent = absences_Agent::getFromIdUser($nuserids[$i]);
			if ($agent->removeRight($right))
			{
				$agent->removeFixedEntry($right);
			}
		}
	}
	
	bab_setTimeLimit(60);
	

	for( $i = 0; $i < $count; $i++)
	{
		if( !in_array($userids[$i], $nuserids) )
		{
		    bab_setTimeLimit(10);
			$agent = absences_Agent::getFromIdUser($userids[$i]);
			if ($agent->addRight($right))
			{
				try {
					$agent->addFixedEntry($right);
				} catch(absences_EntryException $e)
				{
					$babBody->addError($agent->getName().' : '.$e->getMessage());
					$agent->removeRight($right);
				}
			}
		}
	}
	
	bab_setTimeLimit(60);
	
	if (0 === count($babBody->errors))
	{
		$babBody->addNextPageMessage(sprintf(absences_translate('The checked personal members were associated with "%s"'), $right->description));
		return true;
	}
	
	return false;
}


function deleteVacationRightConf($idvr) {

	global $babBody;
	class temp
		{
		var $yes;
		var $no;
		var $invalidentry1;
		var $tpsel;
		var $colsel;

		function temp($idvr)
			{
			$this->idvr = $idvr;
			$this->t_alert = absences_translate("Some vacation requests are linked to this right, if you delete the right, the vacation requests will be deleted with it");

			$this->t_request = absences_translate("Last request with this right");
			$this->t_confirm = absences_translate("Confirm");

			global $babDB;
			$this->res = $babDB->db_query(
				"SELECT
					UNIX_TIMESTAMP(e.date_begin) date_begin
				FROM
					".ABSENCES_ENTRIES_ELEM_TBL." ee,
					".ABSENCES_ENTRIES_TBL." e
				WHERE
					ee.id_right=".$babDB->quote($idvr)."
					AND e.id = ee.id_entry

				ORDER BY e.date_begin DESC"
				);

			$arr = $babDB->db_fetch_assoc($this->res);
			$nb_requests = $babDB->db_num_rows($this->res);
			$this->request = bab_toHtml(absences_longDate($arr['date_begin']));
			if (1 == $nb_requests) {
				$this->t_nb_requests = bab_toHtml(absences_translate("one request will be deleted"));
			} else {
				$this->t_nb_requests = bab_toHtml(sprintf(absences_translate("%d requests will be deleted"),$nb_requests));
			}
		}
	}

	$temp = new temp($idvr);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "rightsdelete"));
	$babBody->title = absences_translate("Delete vacation right");
	$babBody->addItemMenu("delvr", absences_translate("Delete"), absences_addon()->getUrl()."vacadma&idx=delvr");
}





/**
 * 
 */
function rightcopy() {

	global $babBody;
	require_once dirname(__FILE__).'/utilit/rightincl.php';

	if (isset($_POST['copy_rights'])) {
		$temp = new absences_RightCopyStep2();
		$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "rightcopy2"));

	} else {
		$temp = new absences_RightCopyStep1();
		$babBody->babecho(absences_addon()->printTemplate($temp, "vacadma.html", "rightcopy"));
	}
}




/**
 * Delete vacation right and request associated to the vacation right
 * @param int $idvr
 * @return bool
 */
function deleteVacationRight($idvr)
{
	global $babBody, $babDB;
	list($total) = $babDB->db_fetch_row($babDB->db_query("select count(id) as total from ".ABSENCES_ENTRIES_ELEM_TBL." where id_right=".$babDB->quote($idvr)));
	if( $total == 0 )
	{
		$babDB->db_query("DELETE FROM ".ABSENCES_RIGHTS_TBL." WHERE id=".$babDB->quote($idvr));
		$babDB->db_query("DELETE FROM ".ABSENCES_USERS_RIGHTS_TBL." WHERE id_right=".$babDB->quote($idvr));
		$babDB->db_query("DELETE FROM ".ABSENCES_RIGHTS_RULES_TBL." WHERE id_right=".$babDB->quote($idvr));
		$babDB->db_query("UPDATE absences_movement SET id_right='0' WHERE id_right=".$babDB->quote($idvr));

		$res = $babDB->db_query("SELECT id_entry FROM ".ABSENCES_ENTRIES_ELEM_TBL." WHERE id_right=".$babDB->quote($idvr));
		while ($arr = $babDB->db_fetch_assoc($res)) {
			absences_delete_request($arr['id_entry']);
		}
		
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		$url = bab_url::get_request('tg');
		$url->idx = 'lrig';
		$url->location();
	}

	$babBody->addError(absences_translate('The vacation right cannot be deleted because of existing associated requests'));
	return false;
}


function modRgroup() {

	global $babDB, $babBody;

	$name = bab_pp('name');
	if (empty($name)) {
		$babBody->addError(absences_translate('The name is mandatory'));
		return false;
	}	
		
	global $babDB;
	$id = bab_pp('id_rgroup');
	$recover = bab_pp('recover', 0);
	$quantity_unit = bab_pp('quantity_unit', 'D');
	
	if (empty($id)) {
		$babDB->db_query("INSERT INTO absences_rgroup (name, recover, quantity_unit) VALUES (".$babDB->quote($name).", ".$babDB->quote($recover).", ".$babDB->quote($quantity_unit).")");
		$id = $babDB->db_insert_id();
	} else {
		
		if ($recover)
		{
			$res = $babDB->db_query('SELECT g.id FROM absences_rgroup g, absences_rights r 
					WHERE r.id_rgroup=g.id AND g.id='.$babDB->quote($id).' AND r.kind<>'.$babDB->quote(absences_Right::RECOVERY));
			
			if ($babDB->db_num_rows($res) > 0)
			{
				$babBody->addError(absences_translate('This right group already contains rights of a different kind than recovery'));
				return false;
			}
		}
		
		
		
		$babDB->db_query("UPDATE absences_rgroup SET 
			name=".$babDB->quote($name).", 
			recover=".$babDB->quote($recover).",  
			quantity_unit=".$babDB->quote($quantity_unit)." 
		WHERE id=".$babDB->quote($id));
	}
	
	if ($recover)
	{
		$babDB->db_query("UPDATE absences_rgroup SET recover='0' WHERE id<>".$babDB->quote($id));
	}
	
	return true;
}


function deleteRgroup() {
	global $babDB;
	$id = bab_rp('id_rgroup');
	if (!empty($id)) {
		$babDB->db_query("DELETE FROM ".ABSENCES_RGROUPS_TBL." WHERE id=".$babDB->quote($id));
	}
}




function absences_rightMovements($id_right)
{
    require_once dirname(__FILE__).'/utilit/right.class.php';
    require_once dirname(__FILE__).'/utilit/right.ui.php';

    $babBody = bab_getInstance('babBody');
    $right = new absences_Right($id_right);
    $W = bab_Widgets();

    $card = new absences_RightCardFrame($right);

    $card->addClass('widget-bordered');
    $card->addClass('BabLoginMenuBackground');

    $list = new absences_RightMovementList($right);

    $babBody->setTitle(absences_translate('Right history'));

    $babBody->babEcho($card->display($W->HtmlCanvas()));
    $babBody->babEcho($list->getHtml());
}




/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
	{
	$babBody->msgerror = absences_translate("Access denied");
	return;
	}
	
$idx = bab_rp('idx', "lrig");


if( isset($_POST['action']) )
	{
	switch ($_POST['action'])
		{

		case 'rgroupmod':
			if (isset($_POST['rgroup_delete'])) {
				deleteRgroup();

			} else if (!modRgroup()) {
				$idx = 'rgroupmod';
			}
			break;

		}

	}

	
	
if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");



switch($idx)
	{



	case "delvru":
		$userids = bab_rp('userids', array());
		if (modifyVacationRightPersonnel(bab_rp('idvr'), $userids, bab_rp('nuserids', array())))
		{
			
			require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
			$url = bab_url::get_request('tg', 'pos');
			$url->idx = 'user_right_report';
			$url->id_right = bab_rp('idvr');
			$url->nextidx = 'lvrp';
			$url->location();
		}
		
		$idx = 'lvrp';
		// no break
		
	case "lvrp":
		listVacationRightPersonnel(bab_rp('idvr'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("lvrc", absences_translate("Collections beneficiaries"), absences_addon()->getUrl()."vacadma&idx=lvrc&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("lvrp", absences_translate("Users beneficiaries"), absences_addon()->getUrl()."vacadma&idx=lvrp&idvr=".bab_rp('idvr'));
		break;
		
	case 'lvrp_add':
		absences_addVacationRightPersonnel(bab_rp('idvr'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("lvrp", absences_translate("Users beneficiaries"), absences_addon()->getUrl()."vacadma&idx=lvrp&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("lvrp_add", absences_translate("Grant by group"), absences_addon()->getUrl()."vacadma&idx=lvrp_add&idvr=".bab_rp('idvr'));
		break;
		
	case "lvrc":
	
		listVacationRightCollection(bab_rp('idvr'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("lvrc", absences_translate("Collections beneficiaries"), absences_addon()->getUrl()."vacadma&idx=lvrc&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("lvrp", absences_translate("Users beneficiaries"), absences_addon()->getUrl()."vacadma&idx=lvrp&idvr=".bab_rp('idvr'));
		break;
		
	case 'lvrc_progress':
		absences_beneficiariesUpdateProgress(bab_rp('idvr'));
		break;
		
	case "viewvr":
	    viewVacationRightPersonnel(bab_rp('idvr'));
	    $babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
	    $babBody->addItemMenu("viewvr", absences_translate("Vacation right"), absences_addon()->getUrl()."vacadma&idx=viewvr&idvr=".bab_rp('idvr'));
	    $babBody->addItemMenu("modvr", absences_translate("Edit"), absences_addon()->getUrl()."vacadma&idx=modvr&idvr=".bab_rp('idvr'));
	    $babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacadma&idx=movement&idvr=".bab_rp('idvr'));
	    break;
		
	case 'movement':
	    $babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
	    $babBody->addItemMenu("viewvr", absences_translate("Vacation right"), absences_addon()->getUrl()."vacadma&idx=viewvr&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("modvr", absences_translate("Modify"), absences_addon()->getUrl()."vacadma&idx=modvr&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacadma&idx=movement&idvr=".bab_rp('idvr'));
	    absences_rightMovements(bab_rp('idvr'));
	    break;

	case "modvr":
		absences_addModifyVacationRight(bab_rp('idvr'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("viewvr", absences_translate("Vacation right"), absences_addon()->getUrl()."vacadma&idx=viewvr&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("modvr", absences_translate("Modify"), absences_addon()->getUrl()."vacadma&idx=modvr&idvr=".bab_rp('idvr'));
		$babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacadma&idx=movement&idvr=".bab_rp('idvr'));
		break;

	case "addvr":
		absences_addModifyVacationRight(false, bab_rp('from'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("addvr", absences_translate("Add"), absences_addon()->getUrl()."vacadma&idx=addvr");
		break;
		
	case 'nobenef';
		absences_choseBeneficiariesMethod(bab_rp('idvr'));
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("nobenef", absences_translate("Beneficiaries"), absences_addon()->getUrl()."vacadma&idx=nobenef");
		break;
		
	case 'fixedud':
		// fixed vacation right requests progress bar
		absences_fixedRightUpdate(bab_rp('idvr'));
		$babBody->addItemMenu("fixedud", absences_translate("Update"), absences_addon()->getUrl()."vacadma&idx=fixedud");
		break;
		
	case 'fixedud_progress':
		absences_fixedRightUpdateProgress(bab_rp('idvr'));
		break;
		
	case 'user_right_report':
		// user-right links report
		absences_userRightReport(bab_rp('id_right'), bab_rp('nextidx'));
		$babBody->addItemMenu("user_right_report", absences_translate("Update"), absences_addon()->getUrl()."vacadma&idx=user_right_update");
		break;
	

	case 'rgroup':
		$babBody->title = absences_translate("Rights groups");
		$babBody->addItemMenu("rgroup", absences_translate("Rights groups"), absences_addon()->getUrl()."vacadma&idx=rgroup");
		$babBody->addItemMenu("rgroupmod", absences_translate("Add"), absences_addon()->getUrl()."vacadma&idx=rgroupmod");
		rgrouplist();
		break;

	case 'rgroupmod':
		$babBody->title = absences_translate("Right group");
		$babBody->addItemMenu("rgroup", absences_translate("Rights groups"), absences_addon()->getUrl()."vacadma&idx=rgroup");
		$babBody->addItemMenu("rgroupmod", absences_translate("Edit"), absences_addon()->getUrl()."vacadma&idx=rgroupmod");
		rgroupmod();
		break;


	case 'copy':
		$babBody->title = absences_translate("Rights renewal by years");
		$babBody->addItemMenu('copy', absences_translate("Rights renewal"), absences_addon()->getUrl()."vacadma&idx=rgroupmod");
		rightcopy();
		break;

	case "lrig":
	case 'recovery':
	case 'archives':
	default:
		$datee			= bab_rp('datee');
		$dateb			= bab_rp('dateb');
		$idtype			= bab_rp('idtype');
		$idcoll			= bab_rp('idcoll');
		$rgroup			= bab_rp('rgroup');
		$pos			= bab_rp('pos',0);
		$active			= bab_rp('active','');
		$archived		= bab_rp('archived', 0);
		$recovery		= bab_rp('recovery', 0);

		listVacationRigths($idtype, $idcoll, $rgroup, $dateb, $datee, $active, $pos, $archived, $recovery);
		$babBody->addItemMenu("lrig", absences_translate("Rights"), absences_addon()->getUrl()."vacadma&idx=lrig");
		$babBody->addItemMenu("recovery", absences_translate("Recovery rights"), absences_addon()->getUrl()."vacadma&idx=recovery&recovery=1");
		$babBody->addItemMenu("archives", absences_translate("Archives"), absences_addon()->getUrl()."vacadma&idx=archives&archived=1");
		break;
	}
$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');

