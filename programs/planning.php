<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
include_once dirname(__FILE__)."/functions.php";
include_once dirname(__FILE__)."/utilit/vacincl.php";
include_once dirname(__FILE__)."/utilit/planningincl.php";
include_once dirname(__FILE__)."/utilit/agent.class.php";







class absences_PlanningTreeView
{
    public $altbg = true;
    

    private $res;

    public function __construct()
    {
        global $babDB;

        $this->t_name = absences_translate('Search by name');
        $this->t_quantity = absences_translate('Quantity');

    }

    
    private function addEntitesToTree(Widget_SimpleTreeView $tree, $root)
    {
        global $babDB;
        $W = bab_Widgets();
        $addon = absences_addon();
        
        $id_chart = absences_getVacationOption('id_chart');
        $res = $babDB->db_query('SELECT e.id entity, e.name, t.id, t.id_parent 
            FROM 
                
                bab_oc_trees t 
                    LEFT JOIN bab_oc_entities e ON e.id_node=t.id 
            
            WHERE t.id_user='.$babDB->quote($id_chart).' ORDER BY name
        ');
        
        while ($arr = $babDB->db_fetch_assoc($res)) {
            
            $id_parent = $arr['id_parent'] ? 'entity'.$arr['id_parent'] : $root;
            $entity = $W->Link($arr['name'], $addon->getUrl().'planning&idx=entity_cal&ide='.$arr['entity'], 'entity'.$arr['id']);
            $entity->addClass('icon')->addClass(Func_Icons::OBJECTS_ORGANIZATION);
            
            $element = $tree->createElement($entity->getId());
            $element->setItem($entity);
            
            $tree->appendElement($element, $id_parent);
            
            $element->addAction(
                'edit_entity',
                absences_translate('Edit entity planning'),
                $GLOBALS['babSkinPath'] . 'images/Puces/edit.png',
                $addon->getUrl().'planning&idx=edit_entity&ide='.$arr['entity'],
                ''
            );
        }
    }
    
    
    private function addCustomToTree(Widget_SimpleTreeView $tree, $root)
    {
        global $babDB;
        $W = bab_Widgets();
        $addon = absences_addon();
        
        $res = $babDB->db_query('SELECT id, name FROM absences_custom_planning ORDER BY name');
        while($arr = $babDB->db_fetch_assoc($res)) {
            
            $custom = $W->Link($arr['name'], $addon->getUrl().'planning&idx=custom&id='.$arr['id'], 'custom'.$arr['id']);
            $custom->addClass('icon')->addClass(Func_Icons::OBJECTS_ORGANIZATION);
            
            $element = $tree->createElement($custom->getId());
            $element->setItem($custom);
            
            $tree->appendElement($element, $root);
            
            $element->addAction(
                    'set_users_custom',
                    absences_translate('Set custom planning displayed users'),
                    $GLOBALS['babSkinPath'] . 'images/Puces/user-group-new.png',
                    $addon->getUrl().'planning&idx=setusers&id='.$arr['id'],
                    ''
            );
            
            
            $element->addAction(
                    'edit_custom',
                    absences_translate('Edit custom planning'),
                    $GLOBALS['babSkinPath'] . 'images/Puces/edit.png',
                    $addon->getUrl().'planning&idx=edit&id='.$arr['id'],
                    ''
            );
            

            
        }
    }


    public function display()
    {
        bab_functionality::includefile('Icons');
        
        $addon = absences_addon();
        $W = bab_Widgets();
        $tree = $W->SimpleTreeView('plannings');
        $tree->setPersistent();
        $tree->addClass(Func_Icons::ICON_LEFT_16);
        
        
        $rootNode = $tree->createRootNode(absences_translate('Plannings'));
        
        
        // create nodes for each types
        
        $entities = $W->Icon(absences_translate('Entities'), Func_Icons::APPS_ORGCHARTS)->setId('entities');
        $tree->addItem($entities, 'Root');
        $this->addEntitesToTree($tree, $entities->getId());
        
        
        // custom plannings
        
        $item = $W->Icon(absences_translate('Custom plannings'), Func_Icons::APPS_USERS)->setId('custom');
        $element = $tree->createElement($item->getId());
        $element->setItem($item);
        
        $tree->appendElement($element, 'Root');
        $this->addCustomToTree($tree, $item->getId());
        
        $element->addAction(
            'add',
            absences_translate('Add custom planning'),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            $addon->getUrl().'planning&idx=edit',
            ''
        );
        
        
        // complete planning
        
        if (bab_isAccessValid('absences_public_planning_groups', 1)) {
            $complete = $W->Link(absences_translate('Complete planning'), $addon->getUrl().'planning&idx=public')
                ->setId('public')
                ->addClass('icon')
                ->addClass(Func_Icons::APPS_DIRECTORIES);
            
            $element = $tree->createElement($complete->getId());
            $element->setItem($complete);
            
            $tree->appendElement($element, 'Root');
        }
        
        
        
        /*
        $element->addAction(
            'configure_public_planning',
            absences_translate('configure complete planning'),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit.png',
            $addon->getUrl().'planning&idx=configure_public_planning',
            ''
        );
        */

        bab_getBody()->babEcho($tree->display($W->HtmlCanvas()));
    }
}



function absences_PlanningSave()
{
    global $babDB;
    require_once $GLOBALS['babInstallPath'].'admin/acl.php';
    $id_planning = 0;
    $arr = bab_pp('planning');
    
    if (isset($arr['id'])) {
        $id_planning = $arr['id'];
    }
    
    if ($id_planning) {
        $babDB->db_query('UPDATE absences_custom_planning SET name='.$babDB->quote($arr['name']).' 
                WHERE id='.$babDB->quote($id_planning));
        
    } else {
        
        $babDB->db_query('INSERT INTO absences_custom_planning (name) VALUES ('.$babDB->quote($arr['name']).')');
        $id_planning = $babDB->db_insert_id();
    }
    
    aclSetRightsString('absences_custom_planning_groups', $id_planning, $arr['groups']);
    
    $url = bab_url::get_request('tg');
    $url->idx = 'list';
    
    $url->location();
}



function absences_getPlanningValues($id)
{     
    require_once $GLOBALS['babInstallPath'].'admin/acl.php';
    global $babDB;
    
    $res = $babDB->db_query('SELECT * FROM absences_custom_planning WHERE id='.$babDB->quote($id));
    $arr = $babDB->db_fetch_assoc($res);
    
    $arr['groups'] = aclGetRightsString('absences_custom_planning_groups', $id);
    
    return $arr;
}




function absences_PlanningEdit()
{
    
    if (isset($_POST['planning'])) {
        absences_PlanningSave();
    }
    
    
    $id_planning = (int) bab_rp('id');
    
    
    $W = bab_Widgets();
    
    $page = $W->BabPage();
    
    if (empty($id_planning)) {
        $page->setTitle(absences_translate('Create new planning'));
    } else {
        $page->setTitle(absences_translate('Edit planning'));
    }
    
    
    $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));
    
    $form->setName('planning');
    $form->addClass(Func_Icons::ICON_LEFT_16);
    
    $form->addClass('BabLoginMenuBackground');
    $form->addClass('widget-bordered');
    
    $form->addItem($W->LabelledWidget(absences_translate('Name'), $W->LineEdit()->setSize(80), 'name'));
    $form->addItem($W->Acl()->setName('groups')->setTitle(absences_translate('Who can view this planning?')));
    
    $buttons = $W->FlowLayout()->setHorizontalSpacing(3, 'em');
    $buttons->addItem($W->SubmitButton()->setLabel(absences_translate('Save')));
    
    $form->addItem($buttons);
    
    if ($id_planning) {
        
        $form->setHiddenValue('planning[id]', $id_planning);
        $form->setValues(array('planning' => absences_getPlanningValues($id_planning)));

        
        $url = bab_url::get_request('tg', 'id');
        $url->idx = 'delete';
        $buttons->addItem($W->Link(absences_translate('Delete'), $url->toString())
                ->setConfirmationMessage(absences_translate('Do you really want to delete this planning?'))
                ->addClass('icon')
                ->addClass(Func_Icons::ACTIONS_EDIT_DELETE));
    }
    
    $form->display($W->HtmlCanvas());
    
    $page->addItem($form);
    $page->displayHtml();
}



function absences_savePlanningUsers($arr)
{
    global $babDB;
    $id_planning = (int) bab_rp('id');
    
    $babDB->db_query('DELETE FROM absences_custom_planning_users WHERE id_planning='.$babDB->quote($id_planning));
    
    foreach($arr as $id_user) {
        $babDB->db_query('INSERT INTO absences_custom_planning_users (id_planning, id_user) 
                VALUES ('.$babDB->quote($id_planning).', '.$babDB->quote($id_user).')');
    }
    
    $url = bab_url::get_request('tg', 'id');
    $url->idx = 'list';
    $url->location();
}



function absences_PlanningSetUsers()
{
    global $babDB;
    
    $babBody = bab_getBody();
    $id_planning = (int) bab_rp('id');
    
    $res = $babDB->db_query('SELECT name FROM absences_custom_planning  WHERE id='.$babDB->quote($id_planning));
    $arr = $babDB->db_fetch_assoc($res);
    
    if (!$arr) {
        throw new Exception('This planning does not exists');
    }
    
    $babBody->setTitle(absences_translate("Planning members").' : '.$arr['name']);
    
    include_once $GLOBALS['babInstallPath'].'utilit/selectusers.php';
    global $babBody, $babDB;
    $obj = new bab_selectusers();
    $obj->addVar('id', $id_planning);
    $res = $babDB->db_query("SELECT id_user FROM absences_custom_planning_users WHERE id_planning=".$babDB->quote($id_planning));
    while (list($id) = $babDB->db_fetch_array($res)) {
        $obj->addUser($id);
    }
    $obj->setRecordCallback('absences_savePlanningUsers');
    $babBody->babecho($obj->getHtml());
}




function absences_planningDelete()
{
    global $babDB;
    $id_planning = (int) bab_rp('id');
    
    $babDB->db_query('DELETE FROM absences_custom_planning WHERE id='.$babDB->quote($id_planning));
    
    $url = bab_url::get_request('tg');
    $url->idx = 'list';
    
    $url->location();
}

function absences_getPlanningName($id)
{
    global $babDB;
    $res = $babDB->db_query("SELECT name FROM absences_custom_planning WHERE id=".$babDB->quote($id));
    $arr = $babDB->db_fetch_assoc($res);
    return $arr['name'];
}

function absences_getPlanningUsers($id)
{
    global $babDB;
    $users = array();
    $res = $babDB->db_query("SELECT id_user FROM absences_custom_planning_users WHERE id_planning=".$babDB->quote($id));
    while (list($id) = $babDB->db_fetch_array($res)) {
        $users[$id] = $id;
    }
    
    return $users;
}

/**
 * Display a vacation calendar, do not test access right per user
 * 
 * @param	array		$users		array of id_user to display
 * @param	boolean		$period		allow period selection, first step of vacation request
 */
function absences_displayCalendar($users, $period = false) {
    
    $display_users = false;
    $defaultNbMonth = 12;
    if (count($users) > 1) {
        $display_users = true;
        $defaultNbMonth = 1;
    }
    
    $nbmonth = (int) bab_rp('nbmonth', $defaultNbMonth);
    absences_viewVacationCalendar($users, $period, true, $nbmonth, $display_users);
}



/**
 * Display a vacation calendar
 * test access rights
 * @param	array		$users		array of id_user to display
 * @param	boolean		$period		allow period selection, first step of vacation request
 */
function absences_userViewVacationCalendar($users, $period = false) {

	global $babBody, $babDB;
	$current_agent = absences_Agent::getCurrentUser();

	foreach($users as $uid) {
		$target_agent = absences_Agent::getFromIdUser($uid);
		if (!$current_agent->canViewCalendarOf($target_agent)) {
			$babBody->addError(absences_translate('Access denied'));
			$babBody->babPopup('');
			return;
		}
	}

	absences_displayCalendar($users, $period);
}



/**
 * Get users to display in a entity calendar
 * @param int $ide
 */
function absences_getEntityUsers($ide)
{
    global $babDB;
    $users = array();
    $res = bab_OCSelectEntityCollaborators($ide);
    while ($arr = $babDB->db_fetch_assoc($res)) {
        $users[$arr['id_user']] = $arr['id_user'];
    }

    return $users;
}


/**
 * Affiche le planning d'une entite d'organigramme
 * @param int $ide
 */
function entity_cal($ide)
{

	global $babDB, $babBody;
	
	$agent = absences_Agent::getCurrentUser();
	
    $entity_planning = (bool) absences_getVacationOption('entity_planning');
    
    if (!$entity_planning && !$agent->isEntityManager() && !$agent->isManager()) {
        $babBody->msgerror = absences_translate("Access denied to entity planning, access has been disabled by administrator");
		return;
    }
    
    $entity = bab_OCGetEntity($ide);
    bab_getBody()->setTitle($entity['name']);
    
    $display_types = (bool) absences_getVacationOption('entity_planning_display_types');
    
    
	$calendars = $agent->getCalendarEntities();
	
	if (!$display_types) {
	    // les personnes autorisees peuvent quand meme voir les types
	    $display_types = (isset($calendars[$ide]) || $agent->isEntityManagerOf($ide));
	}
	
	$users = absences_getEntityUsers($ide);
	$all = (bool) bab_rp('all');
	
	if ($all) {
	    foreach (absences_getChildsEntities($ide) as $entity) {
	        $users += absences_getEntityUsers($entity['id']);
	    }
	}
	

	$defaultNbMonth = 12;
	if (count($users) > 1) {
	    $defaultNbMonth = 1;
	}
	
	$nbmonth = (int) bab_rp('nbmonth', $defaultNbMonth);
	
	absences_viewVacationCalendar($users, false, $display_types, $nbmonth, true);

}






function absences_saveEntityPlanning($userids, $params)
{
    $ide = $params['ide'];
    global $babDB;
    $babDB->db_query("DELETE FROM ".ABSENCES_PLANNING_TBL." WHERE id_entity = ".$babDB->quote($ide));

    foreach ($userids as $uid)
    {
        $babDB->db_query("INSERT INTO ".ABSENCES_PLANNING_TBL." (id_user, id_entity) VALUES ('".$babDB->db_escape_string($uid)."','".$babDB->db_escape_string($ide)."')");
    }
    
    $agent = absences_Agent::getCurrentUser();
    if ($agent->isManager()) {
        header('location:'.absences_addon()->getUrl()."planning&idx=list");
        exit;
    }

    header('location:'.absences_addon()->getUrl()."vacchart&idx=entities");
    exit;
}



function absences_edit_entity($ide)
{
    
    include_once $GLOBALS['babInstallPath'].'utilit/selectusers.php';
    global $babBody, $babDB;
    
    $e =  bab_OCGetEntity($ide);
    $babBody->setTitle(sprintf(absences_translate('Planning access "%s" (other than delegated managers above this entity)'),$e['name']));

    $obj = new bab_selectusers();
    $obj->addVar('ide', $ide);
    $res = $babDB->db_query("SELECT id_user FROM ".ABSENCES_PLANNING_TBL." WHERE id_entity=".$babDB->quote($ide));
    while (list($id) = $babDB->db_fetch_array($res)) {
        $obj->addUser($id);
    }
    $obj->setRecordCallback('absences_saveEntityPlanning');
    $babBody->babecho($obj->getHtml());
}







function absences_publicCalendar()
{
	global $babBody, $babDB;
	
	if (!bab_isAccessValid('absences_public_planning_groups', 1))
	{
		$babBody->addError(absences_translate('Access denied to public calendar'));
		return;
	}
	
	
	$nbmonth = (int) bab_rp('nbmonth', 1);
	$initusers = absences_getSearchLimit($nbmonth);
	
	$users = array();
	$res = absences_publicCalendarUsers(bab_rp('keyword', null), bab_rp('departments', null), bab_rp('searchtype'), bab_rp('dateb'), bab_rp('datee'), bab_rp('date'));
	
	$i = 0;
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$users[] = $arr['id'];
		if ($i > $initusers) {
			break;
		}
		
		$i++;
	}
	
	absences_viewVacationCalendar($users, false, true, $nbmonth, true, $babDB->db_num_rows($res));
}






function absence_canViewCalendarOf(absences_Agent $agent)
{
    if (bab_isAccessValid('absences_public_planning_groups', 1)) {
        return true;
    }
    
    require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
    
    if (!bab_isUserLogged()) {
        return false;
    }
    
    $currentAgent = absences_Agent::getCurrentUser();
    if ($currentAgent->canViewCalendarOf($agent)) {
        return true;
    }

    return false;
}


/**
 * Ouput JSON for a list of users in one month 
 * 
 * @param array     $users
 * @param int       $month
 * @param int       $year
 * @param string    $dateb      Search by entry dates, datepicker input
 * @param string    $datee      Search by entry dates, datepicker input
 * @throws Exception
 */
function absences_ouputUserMonthJson($users, $month, $year, $dateb, $datee)
{
	require_once $GLOBALS['babInstallPath'].'utilit/json.php';
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	
	$users = (array) $users;
	$month = (int) $month;
	$year = (int) $year;
	
	// convert dates to ISO
	
	$datePicker = bab_Widgets()->DatePicker();
	
	$dateb = $datePicker->getISODate($dateb);
	$datee = $datePicker->getISODate($datee);
	
	
	$output = array();
	
	if (empty($users) || 0 === $month || 0 === $year)
	{
		throw new Exception('Wrong parameters');
	}
	
	

	foreach($users as $id_user)
	{
		$id_user = (int) $id_user;
		$target_agent = absences_Agent::getFromIdUser($id_user);
		
		if (!absence_canViewCalendarOf($target_agent)) {
			continue;
		}
		
		$arr = absences_getPeriodIndex($id_user, $month, $year, $dateb, $datee);
		foreach($arr as &$v)
		{
			$v['title'] = bab_convertStringFromDatabase($v['title'], 'UTF-8');
		}
		$output[] = $arr;
	}


	echo bab_json_encode($output);
	die();
}





/**
 * Output in json format
 * Get the list of user to display in planning, if keyword is empty, return all users
 * Get total number of result
 * 
 * @param string $keyword
 * @param array $departments
 * 
 */
function absences_searchUsers($keyword, $departments, $searchtype, $dateb, $datee, $date, $pos, $limit)
{
	
	require_once $GLOBALS['babInstallPath'].'utilit/json.php';
	global $babDB;
	
	if (!bab_isAccessValid('absences_public_planning_groups', 1))
	{
		die('Access denied to public calendar');
	}
	
	$pos = (int) $pos;
	$limit = (int) $limit;
	

	$res = absences_publicCalendarUsers($keyword, $departments, $searchtype, $dateb, $datee, $date);
	$count = $babDB->db_num_rows($res);
	
	if ($pos > $count)
	{
		die('pos must be lower than total count');
	}
	
	$babDB->db_data_seek($res, $pos);
	
	$return = array(
		'count' => $count,
		'users' => array()	
	);
	
	$i = 0;
	while($arr = $babDB->db_fetch_assoc($res))
	{
		$return['users'][] = array(
			'id' =>	$arr['id'],
			'name' => bab_convertStringFromDatabase($arr['lastname'].' '.$arr['firstname'], 'UTF-8')
		);
		$i++;
		
		if ($i > $limit)
		{
			break;
		}
	}
	
	echo bab_json_encode($return);
	die();
}

















class absences_AccessiblePlanningsCls
{
    var $altbg = true;
    
    private $plannings = array();

    function __construct()
    {
        
        $this->t_name = absences_translate('Name');
        $this->t_calendar = absences_translate('Planning');
        
    }
    
    
    public function addEntities(array $entities)
    {
        $addon = absences_addon();
        $baseurl = new bab_url($addon->getUrl().'planning');
        $baseurl->idx = 'entity_cal';
        $baseurl->popup = 1;
        
        foreach($entities as $e) {
            
            $url = clone $baseurl;
            $url->ide = $e['id'];
            
            $this->plannings[] = array(
                'name' => $e['name'],
                'url' => $url
            );
        }
    }
    
    
    public function addCustomPlannings()
    {
        global $babDB;
        
        $addon = absences_addon();
        $baseurl = new bab_url($addon->getUrl().'planning');
        $baseurl->idx = 'custom';
        $baseurl->popup = 1;
        
        $accessibles = bab_getUserIdObjects('absences_custom_planning_groups');
        
        $res = $babDB->db_query('SELECT * FROM absences_custom_planning WHERE id IN('.$babDB->quote($accessibles).')');
        while ($arr = $babDB->db_fetch_assoc($res)) {
            
            $url = clone $baseurl;
            $url->id = $arr['id'];
            
            $this->plannings[] = array(
                'name' => $arr['name'],
                'url' => $url
            );
        }
    }
    
    
    public function addPublicPlanning()
    {
        if (!bab_isAccessValid('absences_public_planning_groups', 1)) {
            return;
        }
        
        $addon = absences_addon();
        $url = new bab_url($addon->getUrl().'planning');
        $url->idx = 'public';
        $url->popup = 1;
        
        
        $this->plannings[] = array(
            'name' => absences_translate('Complete planning'),
            'url' => $url
        );
    }
    
    
    public function sort()
    {
        bab_Sort::asort($this->plannings, 'name', bab_Sort::CASE_INSENSITIVE);
    }



    function getnext()
    {
        if (list(,$arr) = each($this->plannings)) {
            $this->altbg 		= !$this->altbg;
            $this->name 		= bab_toHtml($arr['name']);
            $this->url 			= bab_toHtml($arr['url']->toString());
            return true;
        }
        else
            return false;
    }
}








/**
 *
 */
function absences_accessible_plannings()
{
    $babBody = bab_getBody();
    
    $babBody->setTitle(absences_translate('Plannings list'));
    
    $agent = absences_Agent::getCurrentUser();
    
    if ($agent->isInPersonnel()) {
        $babBody->addItemMenu("lvreq", absences_translate("Requests"), absences_addon()->getUrl()."vacuser&idx=lvreq");
        if (absences_getVacationOption('display_personal_history'))
        {
            $babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacuser&idx=movement");
        }
        	
        if (absences_getVacationOption('user_add_email'))
        {
            $babBody->addItemMenu("options", absences_translate("Options"), absences_addon()->getUrl()."vacuser&idx=options");
        }
    }
    
    if( $agent->isManager())
    {
        $babBody->addItemMenu("list", absences_translate("Management"), absences_addon()->getUrl()."vacadm");
    }
    
    if ($agent->isEntityManager())
    {
        $babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart&idx=entities");
    }
    
    $babBody->addItemMenu("userlist", absences_translate("Plannings"), absences_addon()->getUrl()."planning&idx=userlist");
    
    
    $entities = $agent->getManagedEntities();


    global $babDB;
    $id_oc = absences_getVacationOption('id_chart');

    $res =$babDB->db_query("SELECT e.id, e.name, e.description
		FROM ".ABSENCES_PLANNING_TBL." p,
			bab_oc_entities e
		WHERE p.id_user='".$babDB->db_escape_string($GLOBALS['BAB_SESS_USERID'])."'
			AND p.id_entity=e.id
			AND e.id_oc=".$babDB->quote($id_oc));
    $entities = array();
    while ($arr = $babDB->db_fetch_assoc($res)) {
        $entities[] = $arr;
    }
     

    $temp = new absences_AccessiblePlanningsCls();
    $temp->addEntities($entities);
    $temp->addCustomPlannings();
    $temp->addPublicPlanning();
    $temp->sort();
    
    $babBody->babecho(absences_addon()->printTemplate($temp, "planning.html", 'userlist'));

}















// main


$idx = bab_rp('idx', "cal");




switch($idx)
{

    case 'userlist':
        
        
        // liste des planning partages accessible a l'utilisateur
        // les entites accessibles (gestion delegue et co-gestion), les entites partages, les plannings personnalises accessibles
        // et le planning complet
        absences_accessible_plannings();
        break;
    
    
    case 'list':
        bab_requireCredential();
        $agent = absences_Agent::getCurrentUser();
        if(!$agent->isManager()) {
            $babBody->msgerror = absences_translate("Access denied to planning list");
            return;
        }
        
        $treeview = new absences_PlanningTreeView();
        $treeview->display();
        
        break;
        
   
        
    case 'edit': // create / edit custom planning
        bab_requireCredential();
        $agent = absences_Agent::getCurrentUser();
        if(!$agent->isManager()) {
            $babBody->addError(absences_translate("Access denied to planning"));
            return;
        }
        
        absences_PlanningEdit();
        
        break;
        
    
    case 'setusers':
        bab_requireCredential();
        $agent = absences_Agent::getCurrentUser();
        if(!$agent->isManager()) {
            $babBody->addError(absences_translate("Access denied to planning"));
            return;
        }
        
        absences_PlanningSetUsers();
        break;
        
        
    case 'delete':
        bab_requireCredential();
        $agent = absences_Agent::getCurrentUser();
        if(!$agent->isManager()) {
            $babBody->addError(absences_translate("Access denied to planning"));
            return;
        }
        
        absences_planningDelete();
        break;
        
        
    case 'custom':
        $agent = absences_Agent::getCurrentUser();
        if (!$agent->canViewCustomPlanning(bab_rp('id'))) {
            $babBody->addError(absences_translate("Access denied to planning"));
            return;
        }
        $babBody->setTitle(absences_getPlanningName(bab_rp('id')));
        absences_displayCalendar(absences_getPlanningUsers(bab_rp('id')));
        break;
        
	case "cal":
		bab_requireCredential();
		$agent = absences_Agent::getCurrentUser();
		
		if(!$agent->isInPersonnel() && !$agent->isEntityManager() && !$agent->isManager() && !$agent->isApprover())
		{
			$babBody->msgerror = absences_translate("Access denied to planning");
			return;
		}

		$users = explode(',',bab_rp('idu'));
		absences_userViewVacationCalendar($users);
		break;
		
	case 'entity_cal':
		bab_requireCredential();
		$agent = absences_Agent::getCurrentUser();
		
		$myplanning = false;
		$myEntity = $agent->getMainEntity();
		if (isset($myEntity))
		{
			$myplanning = ($myEntity['id'] == bab_rp('ide'));
		}
		
		if(!$agent->canViewEntityPlanning(bab_rp('ide')))
		{
			$babBody->msgerror = absences_translate("Access denied to planning");
			return;
		}
		
		
		entity_cal(bab_rp('ide'));
		break;
		

		
	case 'edit_entity': // configure partage
	    bab_requireCredential();
	    $agent = absences_Agent::getCurrentUser();
	    $babBody->addItemMenu("edit_entity", absences_translate("Planning access"), absences_addon()->getUrl()."planning&idx=entity_requests");
	
	    $ide = bab_rp('ide');
	
	    if($agent->canViewEntityPlanning($ide)) {
	        absences_edit_entity($ide);
	    }
	    break;
		
		
		
	case 'public':
		absences_publicCalendar();
		break;
		
		
	case 'load': // ajax
		
		$GLOBALS['babLanguage'] = bab_getLanguage();
		
		// for ovidentia < 8.4.91
		$babBody = bab_getBody();
		
		absences_ouputUserMonthJson(bab_rp('users'), bab_rp('month'), bab_rp('year'), bab_rp('dateb'), bab_rp('datee'));
		break;
		
		
	case 'users': // ajax, public calendar only
	    $departments = bab_rp('departments');
	    if (empty($departments)) {
	        $departments = null;
	    }
	    
		absences_searchUsers(bab_rp('keyword'), $departments, bab_rp('searchtype'), bab_rp('dateb'), bab_rp('datee'), bab_rp('date'), bab_rp('pos'), bab_rp('limit'));
		break;
}



$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');