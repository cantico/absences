<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/




include_once dirname(__FILE__).'/functions.php';
include_once $babInstallPath."utilit/afincl.php";
include_once $babInstallPath."utilit/mailincl.php";
include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/right.class.php';

function addVacationType($vtid, $what, $tname, $description, $tcolor, $cbalance)
	{
	global $babBody, $babDB;
	class temp
		{
		var $name;
		var $description;
		var $quantity;
		var $tnameval;
		var $descriptionval;
		var $quantityval;
		var $vtid;
		var $what;

		var $invalidentry1;

		var $add;

		function temp($vtid, $what, $tname, $description, $tcolor, $cbalance)
			{
			global $babDB;
			$this->what = $what;
			$this->vtid = $vtid;
			$this->name = absences_translate("Name");
			$this->description = absences_translate("Description");
			$this->colortxt = absences_translate("Color");
			$this->yestxt = absences_translate("Yes");
			$this->notxt = absences_translate("No");
			$this->t_recover = absences_translate("Use this right type to store the created recovery rights");

			$this->selctorurl = bab_toHtml($GLOBALS['babUrlScript']."?tg=selectcolor&idx=popup&callback=setColor");
			$this->tcolor = $tcolor;

			if( $what == "modvt" && empty($tname))
				{
				$arr = $babDB->db_fetch_array($babDB->db_query("select * from ".ABSENCES_TYPES_TBL." where id=".$babDB->quote($vtid)));
				$this->tnameval = bab_toHtml($arr['name']);
				$this->descriptionval = bab_toHtml($arr['description']);
				$this->tcolorval = bab_toHtml($arr['color']);
				$this->recover = (bool) $arr['recover'];

				if( $arr['cbalance'] == 'Y')
					{
					$this->yselected = 'selected';
					$this->nselected = '';
					}
				else
					{
					$this->yselected = '';
					$this->nselected = 'selected';
					}
				}
			else
				{
				$this->tnameval = bab_toHtml($tname);
				$this->descriptionval = bab_toHtml($description);
				$this->tcolorval = bab_toHtml($tcolor);
				$this->recover = false;

				if( $cbalance == 'N')
					{
					$this->nselected = 'selected';
					$this->yselected = '';
					}
				else
					{
					$this->nselected = '';
					$this->yselected = 'selected';
					}
				}

			if( $what == "modvt" )
				{
				$this->bdel = true;
				$this->del = absences_translate("Delete");
				$this->add = absences_translate("Modify");
				}
			else
				{
				$this->bdel = false;
				$this->add = absences_translate("Add");
				}
			}
		}

	list($count) = $babDB->db_fetch_row($babDB->db_query("select count(*) as total from ".ABSENCES_TYPES_TBL));
	$temp = new temp($vtid, $what, $tname, $description, $tcolor, $cbalance);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadm.html", "vtypecreate"));
	return $count;
	}

function listVacationCollections()
	{
	global $babBody;

	class temp
		{
		var $nametxt;
		var $urlname;
		var $url;
		var $descriptiontxt;
		var $description;

		var $arr = array();
		var $count;
		var $res;

		public $altbg = true;

		function temp()
			{
			$this->nametxt = absences_translate("Name");
			$this->descriptiontxt = absences_translate("Description");
			$this->t_rights = absences_translate('Rights');
			$babDB = $GLOBALS['babDB'];
			$req = "select * from ".ABSENCES_COLLECTIONS_TBL." order by name asc";
			$this->res = $babDB->db_query($req);
			$this->count = $babDB->db_num_rows($this->res);
			}

		function getnext()
			{
			static $i = 0;
			if( $i < $this->count)
				{
				global $babDB;
				$this->altbg = !$this->altbg;
				$arr = $babDB->db_fetch_array($this->res);
				$this->url = absences_addon()->getUrl()."vacadm&idx=modvc&id=".$arr['id'];
				$this->urlname = bab_toHtml($arr['name']);
				$this->description = bab_toHtml($arr['description']);
				$this->righturl = bab_toHtml(absences_addon()->getUrl()."vacadma&idx=lrig&idcoll=".$arr['id']);
				$i++;
				return true;
				}
			else
				return false;

			}
		}

	$temp = new temp();
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadm.html", "vcollist"));
	return $temp->count;

	}

function listVacationTypes()
	{
	global $babBody;

	class temp
		{
		var $nametxt;
		var $urlname;
		var $url;
		var $descriptiontxt;
		var $quantitytxt;
		var $description;
		var $quantity;
		var $altaddvr;
		public $altbg = true;
		var $arr = array();
		var $count;
		var $res;

		function temp()
			{
			$this->nametxt = absences_translate("Name");
			$this->descriptiontxt = absences_translate("Description");
			$this->colortxt = absences_translate("Color");
			$this->altaddvr = absences_translate("Allocate vacation rights");
			$babDB = $GLOBALS['babDB'];
			$req = "select * from ".ABSENCES_TYPES_TBL." order by name asc";
			$this->res = $babDB->db_query($req);
			$this->count = $babDB->db_num_rows($this->res);
			}

		function getnext()
			{
			static $i = 0;
			if( $i < $this->count)
				{
				global $babDB;
				$arr = $babDB->db_fetch_array($this->res);
				$this->altbg = !$this->altbg;
				$this->url = bab_toHtml(absences_addon()->getUrl()."vacadm&idx=modvt&id=".$arr['id']);
				$this->urlname = bab_toHtml($arr['name']);
				$this->description = bab_toHtml($arr['description']);
				$this->tcolor = bab_toHtml($arr['color']);
				$this->addurl = bab_toHtml(absences_addon()->getUrl()."vacadma&idx=addvr&idtype=".$arr['id']);
				$i++;
				return true;
				}
			else
				return false;

			}
		}

	$temp = new temp();
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadm.html", "vtypelist"));
	return $temp->count;

	}

function addVacationCollection($vcid, $what, $tname, $description, $vtypeids, $category)
	{
	global $babBody;
	class temp
		{
		var $name;
		var $description;
		var $vactypes;
		var $tnameval;
		var $descriptionval;
		var $add;
		var $vtypename;
		var $vtcheck;
		var $vcid;
		var $vtids = array();

		var $arr = array();
		var $count;
		var $res;
		function temp($vcid, $what, $tname, $description, $vtypeids, $category)
			{
			global $babDB;
			$this->name = absences_translate("Name");
			$this->description = absences_translate("Description");
			$this->vactypes = absences_translate("Vacations types");
			$this->category = absences_translate("Category to use in calendar");
			$this->t_checkall = absences_translate("Check/Uncheck all types");
			$this->vcid = $vcid;
			$this->what = $what;

			if( $what == "modvc")
				{
				$this->bdel = true;
				$this->del = absences_translate("Delete");
				$this->add = absences_translate("Modify");
				}
			else
				{
				$this->bdel = false;
				$this->add = absences_translate("Add");
				}

			if( $what == "modvc" && empty($tname))
				{
				$arr = $babDB->db_fetch_array($babDB->db_query("select * from ".ABSENCES_COLLECTIONS_TBL." where id=".$babDB->quote($vcid)));
				$this->tnameval = bab_toHtml($arr['name']);
				$this->descriptionval = bab_toHtml($arr['description']);
				$this->categoryval = bab_toHtml($arr['id_cat']);

				}
			else
				{
				$this->tnameval = bab_toHtml($tname);
				$this->descriptionval = bab_toHtml($description);
				$this->categoryval = bab_toHtml($category);
				}


			include_once $GLOBALS['babInstallPath']."utilit/calapi.php";
			$this->categs = bab_calGetCategories();

			$this->catcount = count($this->categs);
			}



		function getnextcat()
			{
			static $i = 0;
			if( $i < $this->catcount)
				{
				$this->categid = $this->categs[$i]['id'];
				$this->categname = bab_toHtml($this->categs[$i]['name']);
				if( $this->categid == $this->categoryval )
					{
					$this->selected = 'selected';
					}
				else
					{
					$this->selected = '';
					}
				$i++;
				return true;
				}
			else
				return false;

			}
		}

	$temp = new temp($vcid, $what, $tname, $description, $vtypeids, $category);
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadm.html", "vcolcreate"));
	}

	
/**
 * Tester l'export pour 1 utilisateur, valeurs avec date et sans date
 */
function testExportBalances()
{
    require_once dirname(__FILE__).'/utilit/exportbalance.php';
    
    $W = bab_Widgets();
    $export = new exportAvailableBalancesCls();
    
    $page = $W->BabPage();
    $page->addItem($export->compareDateForOneUser(bab_rp('id_user')));
    $page->displayHtml();
}
	

// 	available balances export
function exportAvailableBalances()
{
	global $babBody;
    require_once dirname(__FILE__).'/utilit/exportbalance.php';


	$export = new exportAvailableBalancesCls();

	if (!empty($_POST)) {
		$export->csv();
	}

	$babBody->babecho($export->getHtml());
}


function listVacationPersonnel()
{
	require_once dirname(__FILE__).'/utilit/agentlist.ui.php';
	$babBody = bab_getBody();

	$babBody->babecho(absences_agentListMenu());

	$addon = bab_getAddonInfosInstance('absences');
	$babBody->addStyleSheet($addon->getStylePath().'vacation.css');

	$temp = new absences_AgentList((array) bab_rp('filter', array()));
	$babBody->babecho(absences_addon()->printTemplate($temp, "vacadm.html", "vpersonnellist"));
	return $temp->count;
}




function addGroupVacationPersonnel()
	{
	global $babBody;
	class temp
		{

		function temp()
			{
			$this->grouptext = absences_translate("Group");
			$this->collection = absences_translate("Collection");
			$this->appschema = absences_translate("Approbation schema for vacation requests:");
			$this->cetschema = absences_translate("Approbation schema for deposit into the time savings account:");
			$this->recoverschema = absences_translate("Approbation schema for declaration of worked days entitling recovery:");
			$this->t_use_vr = absences_translate("Use schema for vacation requests");
			$this->groupsbrowurl = absences_addon()->getUrl()."vacadm&idx=browg&cb=";
			$this->t_add = absences_translate("Add");
			$this->t_modify = absences_translate("Modify");
			$this->t_record = absences_translate("Record");
			$this->t_add_modify = absences_translate("Add or modify users by group");
			$this->t_modify_alert = absences_translate("Users with waiting requests will not be modified");

			global $babDB;

			$this->idsa = bab_rp('idsa', 0);
			$this->idcol = isset($_REQUEST['idcol']) ? $_REQUEST['idcol'] : '';


			$this->groupval = "";
			$this->groupid = "";

			$W = bab_Widgets();

			$this->grouppicker = $W->GroupPicker()->setName('groupid')->display($W->HtmlCanvas());

			$this->sares = $babDB->db_query("select * from ".BAB_FLOW_APPROVERS_TBL." order by name asc");
			if( !$this->sares )
				$this->countsa = 0;
			else
				$this->countsa = $babDB->db_num_rows($this->sares);

			$this->colres = $babDB->db_query("select * from ".ABSENCES_COLLECTIONS_TBL." order by name asc");
			$this->countcol = $babDB->db_num_rows($this->colres);
			}

		function getnextsa()
			{
			global $babDB;
			static $j= 0;
			if( $j < $this->countsa )
				{

				$arr = $babDB->db_fetch_array($this->sares);
				$this->saname = bab_toHtml($arr['name']);
				$this->idsapp = bab_toHtml($arr['id']);
				if( $this->idsa == $this->idsapp )
					$this->selected = "selected";
				else
					$this->selected = "";
				$j++;
				return true;
				}
			else
				{
				$j = 0;
				if ($this->countsa > 0)
					$babDB->db_data_seek($this->sares,0);
				return false;
				}
			}

		function getnextcol()
			{
			static $j= 0;
			if( $j < $this->countcol )
				{
				global $babDB;
				$arr = $babDB->db_fetch_array($this->colres);
				$this->collname = bab_toHtml($arr['name']);
				$this->idcollection = bab_toHtml($arr['id']);
				if( $this->idcol == $this->idcollection )
					$this->selected = "selected";
				else
					$this->selected = "";
				$j++;
				return true;
				}
			else
				return false;
			}

		function printhtml()
			{
			    $GLOBALS['babBody']->babecho(absences_addon()->printTemplate($this, "vacadm.html", "grouppersonnelcreate"));
			}


		public function displaySaveProgress()
			{
				$W = bab_Widgets();
				$page = $W->BabPage();


				$groupid = (int) bab_pp('groupid');


				$page->setTitle(sprintf(absences_translate('Add users of group %s into personel members'), bab_getGroupName($groupid)));

				$nextbutton = $W->Button()->addItem($W->Label(absences_translate('Next')));


				$page->addItem(
						$W->Frame()
						->setCanvasOptions($page->Options()->width(70,'em'))
						->addClass('widget-bordered')
						->addClass('BabLoginMenuBackground')
						->addClass('widget-centered')
						->addItem(
								$W->ProgressBar()
								->setProgress(0)
								->setTitle(absences_translate('Process users list'))
								->setProgressAction($W->Action()->fromRequest()->setParameter('idx', 'addg_progress'))
								->setCompletedAction($W->Action()->fromRequest()->setParameter('idx', 'lper'), $nextbutton)
						)
						->addItem($nextbutton)
				);


				$page->displayHtml();
			}

		}

	$temp = new temp();

	if (!empty($_POST))
	{
		$temp->displaySaveProgress();
		return;
	}

	$temp->printhtml();
	}










function deleteVacationPersonnel($pos, $idcol, $idsa, $userids)
	{
	global $babBody, $idx;

	class tempa
		{
		var $warning;
		var $message;
		var $title;
		var $urlyes;
		var $urlno;
		var $yes;
		var $no;

		function tempa($pos, $idcol, $idsa, $userids)
			{
			global $BAB_SESS_USERID, $babDB;
			$this->message = absences_translate("Are you sure you want to remove those personnel members");
			$this->title = "";
			$items = "";

			for($i = 0; $i < count($userids); $i++)
				{
				$req = "select * from ".BAB_USERS_TBL." where id='".$babDB->db_escape_string($userids[$i])."'";
				$res = $babDB->db_query($req);
				if( $babDB->db_num_rows($res) > 0)
					{
					$arr = $babDB->db_fetch_array($res);
					$this->title .= "<br>". bab_composeUserName($arr['firstname'], $arr['lastname']);
					$items .= $arr['id'];
					}
				if( $i < count($userids) -1)
					$items .= ",";
				}
			$this->warning = absences_translate("WARNING: This operation will remove personnel members, all the vacations parameters and the users vacation requests, the ovidentia users will remain"). "!";
			$this->urlyes = absences_addon()->getUrl()."vacadm&idx=lper&pos=".$pos."&idcol=".$idcol."&idsa=".$idsa."&action=Yes&items=".$items;
			$this->yes = absences_translate("Yes");
			$this->urlno = absences_addon()->getUrl()."vacadm&idx=lper&pos=".$pos."&idcol=".$idcol."&idsa=".$idsa;
			$this->no = absences_translate("No");
			}
		}

	if( count($userids) <= 0)
		{
		$babBody->msgerror = absences_translate("Please select at least one item");
		listVacationPersonnel();
		$idx = "lper";
		return;
		}
	$tempa = new tempa($pos, $idcol, $idsa, $userids);
	$babBody->babecho(absences_addon()->printTemplate($tempa, "warning.html", "warningyesno"));
	}


function admmenu()
{
	require_once dirname(__FILE__).'/utilit/menu.ui.php';

	$W = bab_Widgets();
	$page = $W->BabPage();

	$menu = new absences_ManagerMenu();

	$page->setTitle(absences_translate('Vacation management'));
	$page->addItem($menu->getFrame());
	$page->displayHtml();
}


function saveVacationType($tname, $description, $tcolor, $maxdays=0, $mindays=0, $default=0)
	{
	global $babBody;
	if( empty($tname)) {
		$babBody->msgerror = absences_translate("ERROR: You must provide a name")." !";
		return false;
		}

	$babDB = $GLOBALS['babDB'];

	$req = "select id from ".ABSENCES_TYPES_TBL." where name='".$babDB->db_escape_string($tname)."'";
	$res = $babDB->db_query($req);
	if( $res && $babDB->db_num_rows($res) > 0 )
		{
		$babBody->msgerror = absences_translate("This vacation type already exists") ." !";
		return false;
		}

	$req = "insert into ".ABSENCES_TYPES_TBL." ( name, description, quantity, maxdays, mindays, defaultdays, color, recover)";
	$req .= " values (
	'".$babDB->db_escape_string($tname)."',
	'" .$babDB->db_escape_string($description). "',
		'" .$babDB->db_escape_string(0). "',
		'" .$babDB->db_escape_string($maxdays). "',
		'" .$babDB->db_escape_string($mindays). "',
		'" .$babDB->db_escape_string($default). "',
		'" .$babDB->db_escape_string($tcolor). "',
		".$babDB->quote(bab_pp('recover', 0))."
	)";
	$res = $babDB->db_query($req);
	return true;
	}

function updateVacationType($vtid, $tname, $description, $tcolor, $maxdays=0, $mindays=0, $default=0)
	{
	global $babBody;
	if( empty($tname))
		{
		$babBody->msgerror = absences_translate("ERROR: You must provide a name")." !";
		return false;
		}

	$babDB = $GLOBALS['babDB'];

	$req = "SELECT id from ".ABSENCES_TYPES_TBL." WHERE name='".$babDB->db_escape_string($tname)."' AND id!='".$babDB->db_escape_string($vtid)."'";
	$res = $babDB->db_query($req);
	if( $res && $babDB->db_num_rows($res) > 0 )
		{
		$babBody->msgerror = absences_translate("This vacation type already exists") ." !";
		return false;
		}

	$req = "UPDATE ".ABSENCES_TYPES_TBL."
		SET
			name='".$babDB->db_escape_string($tname)."',
			description='".$babDB->db_escape_string($description)."',
			quantity='0',
			maxdays='".$babDB->db_escape_string($maxdays)."',
			mindays='".$babDB->db_escape_string($mindays)."',
			defaultdays='".$babDB->db_escape_string($default)."',
			color='".$babDB->db_escape_string($tcolor)."',
			recover=".$babDB->quote(bab_pp('recover', 0))."
		WHERE
			id='".$babDB->db_escape_string($vtid)."'";
	$res = $babDB->db_query($req);


	// clear planning cache
	$babDB->db_query("DELETE FROM ".ABSENCES_CALENDAR_TBL."");

	return true;
	}

/**
 *
 * @param int $vtid
 */
function deleteVacationType($vtid)
	{
	global $babBody, $babDB;
	$bdel = true;

	list($total) = $babDB->db_fetch_array($babDB->db_query("select count(id) as total from ".ABSENCES_RIGHTS_TBL." where id_type='".$babDB->db_escape_string($vtid)."'"));
	if( $total > 0 )
	{
		$bdel = false;
	}

	if( $bdel )
		{
		$babDB->db_query("delete from ".ABSENCES_TYPES_TBL." where id='".$babDB->db_escape_string($vtid)."'");
		$babDB->db_query("delete from ".ABSENCES_COLL_TYPES_TBL." where id_type='".$babDB->db_escape_string($vtid)."'");
		}
	else
		$babBody->msgerror = absences_translate("This vacation type is used and can't be deleted") ." !";
	}


/**
 *
 * @param unknown_type $tname
 * @param unknown_type $description
 * @param unknown_type $category
 * @return boolean
 */
function absences_saveVacationCollection($tname, $description, $category)
	{
	global $babBody;
	if( empty($tname))
		{
		$babBody->msgerror = absences_translate("ERROR: You must provide a name")." !";
		return false;
		}

	$babDB = $GLOBALS['babDB'];

	$req = "select id from ".ABSENCES_COLLECTIONS_TBL." where name='".$babDB->db_escape_string($tname)."'";
	$res = $babDB->db_query($req);
	if( $res && $babDB->db_num_rows($res) > 0 )
		{
		$babBody->msgerror = absences_translate("This collection already exists") ." !";
		return false;
		}

	$req = "insert into ".ABSENCES_COLLECTIONS_TBL." ( name, description, id_cat )";
	$req .= " values ('".$babDB->db_escape_string($tname)."', '" .$babDB->db_escape_string($description)."', '" .$babDB->db_escape_string($category). "')";
	$res = $babDB->db_query($req);
	$id = $babDB->db_insert_id();

	return true;
	}


/**
 *
 * @param int $vcid
 * @param string $tname
 * @param string $description
 * @param int $category
 */
function absences_updateVacationCollection($vcid, $tname, $description, $category)
	{
	global $babBody, $babDB;
	if( empty($tname))
		{
		$babBody->msgerror = absences_translate("ERROR: You must provide a name")." !";
		return false;
		}


	$req = "select id from ".ABSENCES_COLLECTIONS_TBL." where name='".$babDB->db_escape_string($tname)."' and id!='".$babDB->db_escape_string($vcid)."'";
	$res = $babDB->db_query($req);
	if( $res && $babDB->db_num_rows($res) > 0 )
		{
		$babBody->msgerror = absences_translate("This collection already exists") ." !";
		return false;
		}

	$res = $babDB->db_query("update ".ABSENCES_COLLECTIONS_TBL." set name='".$babDB->db_escape_string($tname)."', description='".$babDB->db_escape_string($description)."', id_cat='".$babDB->db_escape_string($category)."' where id='".$babDB->db_escape_string($vcid)."'");

	return true;
	}


function absences_beneficiariesUpdateProgress($id)
	{
		require_once dirname(__FILE__).'/utilit/right.ui.php';
		require_once dirname(__FILE__).'/utilit/right.act.php';

		$W = bab_Widgets();

		$right = new absences_Right($id);

		$progress = $W->ProgressBar();

		absences_RightAct::updateCollectionsBeneficiaries($right, $progress, (array) bab_gp('collection', array()));
		die();
	}


function absences_updateVacationPersonnelGroup()
{
	global $babDB, $babBody;

	$W = bab_Widgets();
	$progress = $W->ProgressBar();


	$groupid 		= bab_rp('groupid');
	$addmodify 		= bab_rp('addmodify');
	$idcol 			= bab_rp('idcol');
	$idsa 			= bab_rp('idsa');
	$id_sa_cet 		= bab_rp('id_sa_cet');
	$id_sa_recover 	= bab_rp('id_sa_recover');


	if( empty($groupid) )
		{
		echo bab_toHtml(absences_translate("You must specify a group"), BAB_HTML_ALL);
		return false;
		}

	if( !in_array($addmodify, array('add','modify')) )
		{
		echo bab_toHtml(absences_translate("error"), BAB_HTML_ALL);
		return false;
		}

	if( empty($idcol) && $addmodify == 'add' )
		{
		echo bab_toHtml(absences_translate("You must specify a vacation collection"), BAB_HTML_ALL);
		return false;
		}

	if( empty($idsa) )
		{
		echo bab_toHtml(absences_translate("You must specify an approbation schema") , BAB_HTML_ALL);
		return false;
		}


	$users = (array) bab_getGroupsMembers($groupid);
	$total = count($users);
	$pos = 1;

	foreach($users as $user)
	{
		$agent = absences_Agent::getFromIdUser($user['id']);

		if( $agent->exists() && $addmodify == 'modify')
		{
			if ($agent->id_sa != $idsa || $agent->id_sa_cet != $id_sa_cet || $agent->id_sa_recover != $id_sa_recover)
			{
				$wr = $agent->getRequestIterator()->count();

				if ($wr > 0)
				{
					// modification d'un membre du personnel avec des demandes en attente
					// absences_updateVacationUser($arr['id_user'], $idsa);

					echo bab_toHtml(sprintf(absences_translate(
						'%s has not been modified because of %d waiting request',
						'%s has not been modified because of %d waiting requests', $wr ), $agent->getName(), $wr) , BAB_HTML_ALL);

				} else {

					// modification d'un membre du personnel qui n'a pas de demande en attente
					$babDB->db_query("update ".ABSENCES_PERSONNEL_TBL." set
						id_sa='".$babDB->db_escape_string($idsa)."',
						id_sa_cet=".$babDB->quote($id_sa_cet).",
						id_sa_recover=".$babDB->quote($id_sa_recover)."
					WHERE id_user=".$babDB->quote($user['id']));
				}
			}
		}
		else if (!$agent->exists() && $addmodify == 'add')
		{
			// creation du nouveau membre du personnel
			try {
				$messages = array();
				absences_saveVacationPersonnel($user['id'], $idcol, $idsa, $id_sa_cet, $id_sa_recover, null, $messages);

				foreach($messages as $m)
				{
					echo bab_toHtml($user['name'].' : '.$m, BAB_HTML_ALL);
				}


			} catch (Exception $e)
			{
				echo bab_toHtml($e->getMessage() , BAB_HTML_ALL);
			}
		}


		$progress->updateProgress(($pos * 100) / $total, $agent->getName());
		$pos++;
	}

	$progress->updateProgress(100, absences_translate('Finished'));
	die();
}


/**
 *
 * @param int $vcid
 */
function absences_deleteVacationCollection($vcid)
	{
	global $babDB;
	$bdel = true;

	list($total) = $babDB->db_fetch_array($babDB->db_query("select count(id) as total from ".ABSENCES_PERSONNEL_TBL." where id_coll='".$babDB->db_escape_string($vcid)."'"));
	if( $total > 0 )
		{
		$bdel = false;
		}

	if( $bdel )
		{
		$babDB->db_query("delete from ".ABSENCES_COLLECTIONS_TBL." where id='".$babDB->db_escape_string($vcid)."'");
		$babDB->db_query("delete from ".ABSENCES_COLL_TYPES_TBL." where id_coll='".$babDB->db_escape_string($vcid)."'");
		}
	else
		$babBody->msgerror = absences_translate("This vacation collection is used and can't be deleted") ." !";
	}


/**
 * Remove vacation personnel
 * @param unknown_type $items
 */
function confirmDeletePersonnel($items)
{

	global $babDB;

	$arr = explode(",", $items);
	$cnt = count($arr);
	for($i = 0; $i < $cnt; $i++)
	{
		$agent = absences_Agent::getFromIdUser($arr[$i]);
		$agent->delete();
		bab_siteMap::clear($arr[$i]);
	}

	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	$url = bab_url::get_request('tg', 'idx');
	$url->location();
}




/**
 * Edit the agent-right link
 * @param int $agent_right
 */
function absences_editAgentRight($agent_right)
{
	$W = bab_Widgets();
	require_once dirname(__FILE__).'/utilit/agent_right.class.php';
	require_once dirname(__FILE__).'/utilit/agent.ui.php';
	require_once dirname(__FILE__).'/utilit/right.ui.php';
	require_once dirname(__FILE__).'/utilit/agent_right.ui.php';
	require_once dirname(__FILE__).'/utilit/changes.class.php';
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';


	$agentRight = absences_AgentRight::getById($agent_right);
	$agent = $agentRight->getAgent();
	$right = $agentRight->getRight();
	


	if (isset($_POST['agent_right']))
	{
		$changes = new absences_Changes();
		$ar = $_POST['agent_right'];

		if ( isset($ar['cancel']) )
		{
			$url = bab_url::get_request('tg');
			$url->idx = 'rights';
			$url->idu = $agentRight->id_user;
			$url->location();
		}


		if( isset($ar['save'] ))
		{
			$date = $W->DatePicker();

			if ('right' === $ar['quantity_type']) {
			    $ar['quantity'] = '';
			}
			
			$agentRightQuantity = $agentRight->getQuantityFromInput($ar['quantity']);
			
			if (isset($agentRightQuantity)) {
			    
			    if ('' === $agentRight->quantity && '' !== $agentRightQuantity) {
			        $changes->fromRightQuantityToAgentQuantity();
			    }
			    
			    if ('' !== $agentRight->quantity && '' === $agentRightQuantity) {
			        $changes->fromAgentQuantityToRightQuantity();
			    }
			    
    			$old_quantity = absences_quantity($agentRight->getQuantity(), $right->quantity_unit);
    			$agentRight->quantity = (string) $agentRightQuantity;
    			$new_quantity = absences_quantity($agentRight->getQuantity(), $right->quantity_unit);

    			$changes->setQuantity($old_quantity, $new_quantity);
    			
			}

			if (isset($ar['date_begin_valid']) && isset($ar['date_end_valid']))
			{
				$date_begin_valid = $date->getISODate($ar['date_begin_valid']);
				$date_end_valid = $date->getISODate($ar['date_end_valid']);

				$changes->setValidPeriod($agentRight->date_begin_valid, $agentRight->date_end_valid, $date_begin_valid, $date_end_valid);
				$agentRight->date_begin_valid = $date_begin_valid;
				$agentRight->date_end_valid = $date_end_valid;
			}

			if (isset($ar['inperiod_start']) && isset($ar['inperiod_end']))
			{
				$inperiod_start = $date->getISODate($ar['inperiod_start']);
				$inperiod_end = $date->getISODate($ar['inperiod_end']);

				$changes->setInPeriod($agentRight->inperiod_start, $agentRight->inperiod_end, $inperiod_start, $inperiod_end);
				$changes->setValidOverlap($agentRight->validoverlap, $ar['validoverlap']);

				$agentRight->inperiod_start = $inperiod_start;
				$agentRight->inperiod_end = $inperiod_end;
				$agentRight->validoverlap = $ar['validoverlap'];
			}

			if (isset($ar['saving_begin']) && isset($ar['saving_end']))
			{
				// CET
				$saving_begin = $date->getISODate($ar['saving_begin']);
				$saving_end = $date->getISODate($ar['saving_end']);

				$changes->setSavingPeriod($agentRight->saving_begin, $agentRight->saving_end, $saving_begin, $saving_end);
				$agentRight->saving_begin = $saving_begin;
				$agentRight->saving_end = $saving_end;
			}


			$agentRight->save();

			$agentRight->addAgentMovement($agent, $changes, $ar['movement_comment']);
			


			$url = bab_url::get_request('tg');
			$url->idx = 'rights';
			$url->idu = $agentRight->id_user;
			$url->location();
		}
	}








	$page = $W->BabPage();
	$page->addStyleSheet(absences_Addon()->getStylePath().'vacation.css');



	$page->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
	$page->addItemMenu("rights", absences_translate("User rights"), absences_addon()->getUrl()."vacadm&idx=rights&idu=".$agent->id_user);
	$page->addItemMenu("agentright", absences_translate("User right"), absences_addon()->getUrl()."vacadm&idx=agentright&ar=".bab_rp('ar'));




	$page->setTitle(sprintf(absences_translate('Right parameter only for %s'), $agent->getName()));

	$agentCard = new absences_AgentCardFrame($agent);
	$agentCard->addClass('widget-bordered');
	$agentCard->addClass('widget-centered');
	$agentCard->setCanvasOptions($agentCard->Options()->width(70,'em'));

	$page->addItem($agentCard);


	$rightCard = new absences_RightCardFrame($right);
	$rightCard->addClass('widget-bordered');
	$rightCard->addClass('widget-centered');
	$rightCard->setCanvasOptions($rightCard->Options()->width(70,'em'));

	$page->addItem($rightCard);


	if ($message = $agentRight->getQuantityAlert())
	{
		$rightCard->addItem($W->Icon($message, Func_Icons::STATUS_DIALOG_WARNING));
	}
	
	// liste des modification du solde par mois (droits variables par mois)

	$dynrightCard = new absences_IncrementCardFrame($agentRight);
	$dynrightCard->addClass('widget-bordered');
	$dynrightCard->addClass('widget-centered');
	$dynrightCard->setCanvasOptions($rightCard->Options()->width(70,'em'));
	if ($dynrightCard->contain_rows)
	{
	    $page->addItem($dynrightCard);
	}
	

	// liste des modification dynamiques du solde

	$dynrightCard = new absences_DynamicRightCardFrame($agentRight);
	$dynrightCard->addClass('widget-bordered');
	$dynrightCard->addClass('widget-centered');
	$dynrightCard->setCanvasOptions($rightCard->Options()->width(70,'em'));
	if ($dynrightCard->contain_rows)
	{
		$page->addItem($dynrightCard);
	}

	$editor = new absences_AgentRightEditor($agentRight);
	$page->addItem($editor);
	
	$history = new absences_AgentRightMovementFrame($agentRight);
	if ($history->count) {
	   $page->addItem($history);
	}

	$page->displayHtml();
}





function absences_agentMovements($id_user)
{
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	require_once dirname(__FILE__).'/utilit/agent.ui.php';

	$babBody = bab_getInstance('babBody');
	$agent = absences_Agent::getFromIdUser($id_user);
	$W = bab_Widgets();

	$card = new absences_AgentCardFrame($agent);

	$card->addClass('widget-bordered');
	$card->addClass('BabLoginMenuBackground');

	$list = new absences_AgentMovementList($agent);

	$babBody->setTitle(absences_translate('User history'));

	$babBody->babEcho($card->display($W->HtmlCanvas()));
	$babBody->babEcho($list->getHtml());
}



function absences_updateManagerVacationPersonnel($id_user)
{
    $update = absences_updateVacationPersonnel($id_user);
    
    if(true === $update) {
        return 'changeucol'; // vers la modification du regime
    }

    if (false === $update) {
        return 'modp';
    }
    
    $url = bab_url::get_request('tg');
    $url->idx = 'lper';
    $url->location();
}



/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();

if( !$agent->isManager())
	{
	$babBody->msgerror = absences_translate("Access denied");
	return;
	}

$idx = bab_rp('idx', "menu");

if( isset($_POST['add']) )
	{
	switch($_POST['add'])
		{
		case 'addvt':

			if(!saveVacationType(bab_rp('tname'), bab_rp('description'), bab_rp('tcolor')))
				$idx ='addvt';

			break;

		case 'modvt':
			if(bab_rp('bdel', null))
			{
				deleteVacationType(bab_rp('vtid'));
			}
			else if(!updateVacationType(bab_rp('vtid'), bab_rp('tname'), bab_rp('description'), bab_rp('tcolor')))
			{
				$idx ='addvt';
			}
			break;

		case 'addvc':
			if(!absences_saveVacationCollection(bab_rp('tname'), bab_rp('description'), bab_rp('category')))
			{
				$idx ='addvc';
			}
			break;

		case 'modvc':

			if(bab_rp('bdel', null))
			{
				absences_deleteVacationCollection(bab_rp('vcid'));
			}
			else if(!absences_updateVacationCollection(bab_pp('vcid'), bab_pp('tname'), bab_pp('description'), bab_pp('category')))
			{
				$idx ='addvc';
			}
			break;


		case 'changeuser':

			if (!empty($_POST['idp'])) {
			    $idx = absences_updateManagerVacationPersonnel($_POST['idp']);
			}
			else {
				$idsa = bab_pp('idsa', 0);
				$id_sa_cet = bab_pp('id_sa_cet', 0);
				$id_sa_recover = bab_pp('id_sa_recover', 0);
				$emails = bab_pp('emails');

				try {

					$messages = array();
					if(!absences_saveVacationPersonnel(bab_pp('userid'), bab_pp('idcol'), $idsa, $id_sa_cet, $id_sa_recover, $emails, $messages))
					{
					$idx ='addp';
					}
				} catch (Exception $e)
				{
					$babBody->addError($e->getMessage());
					$idx ='addp';
					break;
				}


				if (!empty($messages))
				{
					foreach($messages as $message)
					{
						$babBody->addNextPageMessage($message);
					}
				}

				require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

				$url = bab_url::get_request('tg', 'pos', 'idcol', 'idsa');
				$url->idx = 'lper';
				$url->location();
			}
			break;


		case 'changeucol':
			if (!absences_updateUserColl())
				$idx = $add;
			break;

		case 'modrbu':
			if (true === absences_updateVacationRightByUser(
				bab_pp('idu'),
				bab_pp('quantity'),
				bab_pp('comment')
			)) {

				require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';

				$url = bab_url::get_request('tg', 'idx', 'idu');
				$url->location();
			}
			break;
		}
	}
else if( isset($_GET['action']) && $_GET['action'] == "Yes")
	{
	confirmDeletePersonnel($_GET['items']);
	$idx = "lper";
	}



if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}

if( $agent->isEntityManager())
{
	$babBody->addItemMenu("entities", absences_translate("Delegate management"), absences_addon()->getUrl()."vacchart");
}

$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");



switch($idx)
	{
	case "browu":
		include_once $babInstallPath."utilit/lusersincl.php";
		bab_browseUsers(bab_rp('pos'), bab_rp('cb'));
		exit;
		break;
	case "browg":
		include_once $babInstallPath."utilit/grpincl.php";
		browseGroups(bab_rp('cb'));
		exit;
		break;

	case 'rights':
		absences_listRightsByUser(bab_rp('idu'));

		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("rights", absences_translate("User rights"), absences_addon()->getUrl()."vacadm&idx=rights&idu=".bab_rp('idu'));
		break;

	case 'agentright':
		absences_editAgentRight(bab_rp('ar'));

		break;


	case 'movement':
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("movement", absences_translate("History"), absences_addon()->getUrl()."vacadm&idx=movement&idu=".bab_rp('idu'));
		absences_agentMovements(bab_rp('idu'));
		break;


	case "delu":
		$babBody->title = absences_translate("Delete users");
		deleteVacationPersonnel(
			bab_rp('pos'),
			bab_rp('idcol'),
			bab_rp('idsa'),
			bab_rp('userids',array())
		);
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("delu", absences_translate("Delete"), absences_addon()->getUrl()."vacadm&idx=delu");
		break;

	case "modp":
		$babBody->title = absences_translate("Modify user");

		absences_addVacationPersonnel($_REQUEST['idp']);

		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("modp", absences_translate("Modify"), absences_addon()->getUrl()."vacadm&idx=modp");
		break;

	case "addp":
		$babBody->title = absences_translate("Add users");
		absences_addVacationPersonnel();

		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("addp", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addp");
		break;

	case 'changeucol':
		$babBody->title = sprintf(absences_translate("Change user collection for %s"), bab_getuserName($_POST['idp']));
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("changeucol", absences_translate("User collection"), absences_addon()->getUrl()."vacadm&idx=changeucol");
		absences_changeucol($_POST['idp'],$_POST['idcol']);
		break;

	case "addg":
		$babBody->title = absences_translate("Add/Modify users by group");
		addGroupVacationPersonnel();
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("addg", absences_translate("Add/Modify"), absences_addon()->getUrl()."vacadm&idx=addg");
		break;

	case 'addg_progress':
		absences_updateVacationPersonnelGroup();
		break;

	case "lper":
		$babBody->title = absences_translate("Personnel");

		$pos = bab_rp('pos');
		$chg = bab_rp('chg', null);

		if( isset($chg))
		{
			if( mb_strlen($pos) > 0 && $pos[0] == "-" )
				$pos = mb_strlen($pos)>1? $pos[1]: '';
			else
				$pos = "-" .$pos;
		}
		listVacationPersonnel();
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		
		break;

	// 	available balances export
	case 'abexport':
		$babBody->setTitle(absences_translate("Available balances export"));
		exportAvailableBalances();
		$babBody->addItemMenu("lper", absences_translate("Personnel"), absences_addon()->getUrl()."vacadm&idx=lper");
		$babBody->addItemMenu("abexport", absences_translate("Available balances export"), absences_addon()->getUrl()."vacadm&idx=abexport");
		break;
		
	case 'testexp':
	    testExportBalances();
	    break;


	case "lcol":

		$babBody->title = absences_translate("Vacations type's collections");
		listVacationCollections();
		$babBody->addItemMenu("lcol", absences_translate("Collections"), absences_addon()->getUrl()."vacadm&idx=lcol");
		$babBody->addItemMenu("addvc", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvc");
		break;

	case "modvc":
		$babBody->title = absences_translate("Modify vacation type's collection");

		$vcid = bab_rp('vcid', bab_rp('id'));
		$what = bab_rp('what', 'modvc');

		addVacationCollection($vcid, $what, bab_rp('tname'), bab_rp('description'), bab_rp('vtypeids', array()), bab_rp('category', 0));

		$babBody->addItemMenu("lcol", absences_translate("Collections"), absences_addon()->getUrl()."vacadm&idx=lcol");
		$babBody->addItemMenu("modvc", absences_translate("Modify"), absences_addon()->getUrl()."vacadm&idx=modvc");
		$babBody->addItemMenu("addvc", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvc");

		break;

	case "addvc":
		$babBody->title = absences_translate("Add vacation type's collection");

		$vcid = bab_rp('vcid', bab_rp('id'));
		$what = bab_rp('what', 'addvc');

		addVacationCollection($vcid, $what, bab_rp('tname'), bab_rp('description'), bab_rp('vtypeids', array()), bab_rp('category', 0));
		$babBody->addItemMenu("lcol", absences_translate("Collections"), absences_addon()->getUrl()."vacadm&idx=lcol");
		$babBody->addItemMenu("addvc", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvc");

		break;

	case "modvt":
		$babBody->title = absences_translate("Modify vacation type");

		$vtid = bab_rp('vtid', bab_rp('id'));
		$what = bab_rp('what', 'modvt');

		addVacationType($vtid, $what, bab_rp('tname'), bab_rp('description'), bab_rp('tcolor'), bab_rp('cbalance'));
		$babBody->addItemMenu("lvt", absences_translate("Types"), absences_addon()->getUrl()."vacadm&idx=lvt");
		$babBody->addItemMenu("modvt", absences_translate("Modify"), absences_addon()->getUrl()."vacadm&idx=modvt");
		$babBody->addItemMenu("addvt", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvt");
		break;

	case "addvt":
		$babBody->title = absences_translate("Add vacation type");

		$vtid = bab_rp('vtid', '');
		$what = bab_rp('what', 'addvt');

		$babBody->addItemMenu("lvt", absences_translate("Types"), absences_addon()->getUrl()."vacadm&idx=lvt");
		$babBody->addItemMenu("addvt", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvt");
		addVacationType($vtid, $what, bab_rp('tname'), bab_rp('description'), bab_rp('tcolor'), bab_rp('cbalance'));

		break;

	case "lvt":

		$babBody->title = absences_translate("Vacations types");
		$babBody->addItemMenu("lvt", absences_translate("Types"), absences_addon()->getUrl()."vacadm&idx=lvt");
		$babBody->addItemMenu("addvt", absences_translate("Add"), absences_addon()->getUrl()."vacadm&idx=addvt");
		listVacationTypes();

		break;

	case 'export':
		require_once dirname(__FILE__).'/utilit/agent_export.class.php';
		$excel = new absences_AgentXls(absences_Agent::getFromIdUser(bab_gp('id_user')));
		die();

	case 'rightsexport':
	    $babBody->setTitle(absences_translate('Export rights'));
	    require_once dirname(__FILE__).'/utilit/right_export.class.php';
	    absences_exportForm();
	    break;

	case "menu":
	default:
		$babBody->title = absences_translate("Vacations management");
		admmenu();
		break;
	}
$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');

