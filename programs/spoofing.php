<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';


function absences_gotoRequestCreation()
{
    
    $user = (int) bab_pp('user');
    $request_type = bab_pp('request_type');
    
    
    if (empty($user)) {
        throw new Exception('The user is required');
    }
    
    if (empty($request_type)) {
        throw new Exception('The request_type is required');
    }
    
    
    $addon = absences_addon();
    $url = new bab_url($addon->getUrl().'vacuser');
    $url->id_user = $user;
    
    
    switch($request_type) {
        case 'entry':
            $url->idx = 'period';
            $url->rfrom = 1;
            $url->location();
            break;
            
        case 'workperiod_recover_request':
            $url->idx = 'workperiod';
            $url->location();
            break;
            
        case 'cet_deposit_request':
            $url->idx = 'cet';
            $url->location();
            break;
    }
    
    throw new Exception('Unexpected request type '.$request_type);
}





function absences_setSpoofingUser()
{
    if (!empty($_POST)) {
        return absences_gotoRequestCreation();
    }
    
    
    $W = bab_Widgets();
    $page = $W->BabPage();
    
    $form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    $form->addClass('BabLoginMenuBackground');
    $form->addClass('widget-bordered');
    $form->addClass(Func_Icons::ICON_LEFT_16);
    
    $form->setHiddenValue('tg', bab_rp('tg'));
    
    
    $options = array(
        'entry' => absences_translate('A vacation request'),
        'workperiod_recover_request' => absences_translate('A workperiod recover request'),
        'cet_deposit_request' => absences_translate('A time saving account deposit request')
    );
    
    
    if (bab_rp('request_type')) {
        $request_type = bab_rp('request_type');
        
        if (!isset($options[$request_type])) {
            throw new Exception(absences_translate('This request type does not exists'));
        }
        
        $form->addItem($W->Hidden(null, 'request_type'));
        $form->addItem($W->Title(sprintf(absences_translate('Create %s instead of a personnel member'), mb_strtolower($options[$request_type]))));
        
    } else {
        
        
        $form->addItem(
            $W->LabelledWidget(
                absences_translate('Request type'),
                $W->Select()->setOptions($options),
                'request_type'
            )
        );
    }
    
    
    $form->addItem(
        $W->LabelledWidget(
            absences_translate('Select the user to substitute'), 
            $W->UserPicker(),
            'user'
        )
    );
    
    $form->addItem(
        $W->SubmitButton()
            ->setLabel(absences_translate('Continue'))
    );
    
    $form->setValues($_REQUEST);
    
    $page->addItem($form);
    $page->displayHtml();
}



$idx = bab_rp('idx', "setuser");

switch($idx)
{
    case 'setuser':
        absences_setSpoofingUser();
        break;
}