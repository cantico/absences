<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/vacincl.php';
require_once dirname(__FILE__).'/utilit/cet_deposit_request.class.php';
require_once dirname(__FILE__).'/utilit/cet_deposit_request.ui.php';
require_once dirname(__FILE__).'/utilit/agent.class.php';

class absences_CetListCls extends absences_Paginate 
{
	const MAX = 30;
	
	private $res;
	
	public $altbg = true;

	
	public function __construct()
	{
		$this->t_user = absences_translate('User');
		$this->t_right_source = absences_translate('Right source');
	 	$this->t_quantity = absences_translate('Quantity');
		$this->t_status = absences_translate('Status');
		$this-> t_delete = absences_translate('Delete');
		$this->t_confirmdelete = absences_translate('Do you really want to delete this CET deposit request? the associated quantity in the time saving account will not be removed');
		
		
		$f = new absences_getRequestSearchForm();
		
		$this->res = new absences_CetDepositRequestIterator;
		$this->res->archived = (int) bab_rp('archived', 0);
		
		if ($status = $f->param('idstatus'))
		{
			if ('W' === $status)
			{
				$status = '';
			}
			$this->res->status = $status;
		}
		
		if ($userid = $f->param('userid'))
		{
			$this->res->users = array($userid);
		}
		
		if ($organization = $f->param('organization'))
		{
		    $this->res->organization = $organization;
		}
		
		$W = bab_Widgets();
		$datePicker = $W->DatePicker();
		
		if ($begin = $datePicker->getISODate($f->param('dateb', null)))
		{
			$this->res->startFrom = $begin;
		}
		
		if ($end = $datePicker->getISODate($f->param('datee', null)))
		{
			$this->res->startTo = $end;
		}
		$this->res->rewind();
		
		$this->paginate($this->res->count(), self::MAX);
		$this->res->seek($this->pos);
		
		
		$this->searchform = $f->getHtmlForm(array('W' => absences_translate("Waiting"), 'Y' => absences_translate("Accepted"), 'N' => absences_translate("Refused")));
	}
	
	
	public function getnext()
	{
		if (($this->res->key() - $this->pos) >= self::MAX)
		{
			return false;
		}
		
		if ($this->res->valid())
		{
			$this->altbg = !$this->altbg;
			
			$deposit = $this->res->current();
			/*@var $deposit absences_CetDepositRequest */
			
			$this->editurl = bab_toHtml(absences_addon()->getUrl().'vacadmcet&idx=edit&id='.$deposit->id);
			$this->deleteurl = bab_toHtml(absences_addon()->getUrl().'vacadmcet&idx=delete&id='.$deposit->id);
			$this->username = bab_toHtml(bab_getUserName($deposit->id_user));
			
			$right = $deposit->getAgentRightSource()->getRight();
			
			$description = isset($right->description) ? $right->description : absences_translate('Unknown');
			$quantity_unit = isset($right->quantity_unit) ? $right->quantity_unit : 'D';
			
			$this->right_source = bab_toHtml($description);
			$this->quantity = bab_toHtml(absences_quantity($deposit->quantity, $quantity_unit));
			$this->status = bab_toHtml($deposit->getStatusStr());
			
			
			$this->res->next();
			
			return true;
		}
		
		return false;
	}
}



function absences_CetList()
{
	$babBody = bab_getInstance('babBody');
	$list = new absences_CetListCls;
	
	if (bab_rp('archived'))
	{
		$babBody->setTitle(absences_translate('Archived deposits in time saving accounts'));
	} else {
		$babBody->setTitle(absences_translate('Deposits in time saving accounts'));
	}
	absences_addSpoofButton('cet_deposit_request', absences_translate('Add a time saving account deposit'));
	$babBody->babEcho(absences_addon()->printTemplate($list, "vacadmcet.html", "list"));
}



function absences_CETgotoList()
{
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

	$url = bab_url::get_request('tg');
	$url->idx = 'list';
	$url->location();
}



/**
 * Save form
 * @param absences_CetDepositRequest $deposit
 * @param Array $cet
 * @throws Exception
 */
function absences_saveCetdepositRequestAdm(absences_CetDepositRequest $deposit, Array $cet)
{
	if ('' === $deposit->status)
	{
		// waiting validation, modification allowed
		$deposit->quantity = str_replace(',', '.', $cet['quantity']);
		$deposit->id_agent_right_source = (int) $cet['id_agent_right_source'];
	}
	
	
	$deposit->comment = $cet['comment'];
	$deposit->save();
	
	return true;
}



function absences_CetEdit()
{
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	if (isset($_POST['cet']))
	{
		if ( isset($_POST['cet']['cancel']) )
		{
			absences_CETgotoList();
		}
	
	
		if( isset($_POST['cet']['save'] ))
		{
	
			// modification uniquement
	
			$values = $_POST['cet'];
			$deposit = absences_CetDepositRequest::getById($values['id']);
	
			if (!$deposit->getRow())
			{
				throw new Exception('Deposit not found');
			}
	
			try {
				absences_saveCetdepositRequestAdm($deposit, $values);
				absences_CETgotoList();
			} catch(Exception $e)
			{
				$page->addError($e->getMessage());
			}
		}
	}
	
	$id = bab_gp('id', null);
	
	if ($id)
	{
		$deposit = absences_CetDepositRequest::getById($id);
	} else {
		$deposit = null;
	}
	
	$editor = new absences_CetDepositRequestEditor($deposit);
	
	$page->setTitle(absences_translate('Time saving account deposit request'));
	$page->addItem($editor);
	$page->displayHtml();
}





function absences_WorkingDayDelete()
{
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	$id = bab_gp('id', null);

	if ($id)
	{
		$wd = absences_CetDepositRequest::getById($id);
		$wd->delete();
	}

	bab_url::get_request('tg')->location();
}




/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

if ($agent->isInPersonnel())
{
	$babBody->addItemMenu("vacuser", absences_translate("Vacations"), absences_addon()->getUrl()."vacuser");
}
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");
$babBody->addItemMenu("list", absences_translate("Deposits"), absences_addon()->getUrl()."vacadmcet&idx=list");

$idx = bab_rp('idx', "list");


switch($idx)
{
	case 'edit':
		absences_CetEdit();
		$babBody->addItemMenu("edit", absences_translate("Edit"), absences_addon()->getUrl()."vacadmcet&idx=edit&id=".bab_rp('id'));
		break;
		
	
	case 'list':
	case 'archives':
		absences_CetList();
		$babBody->addItemMenu("archives", absences_translate("Archives"), absences_addon()->getUrl()."vacadmcet&idx=archives&archived=1");
		break;
		
	case 'delete':
		absences_WorkingDayDelete();
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');