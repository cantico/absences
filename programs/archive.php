<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

include_once dirname(__FILE__).'/functions.php';
include_once dirname(__FILE__).'/utilit/vacincl.php';
include_once dirname(__FILE__).'/utilit/agent.class.php';
include_once dirname(__FILE__).'/utilit/archive.ui.php';
include_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

/**
 * @return array
 */
function absences_getArchivableRights()
{
	require_once dirname(__FILE__).'/utilit/right.class.php';
	$I = new absences_RightIterator();
	$I->archived = 0;
	
	$rights = array();
	
	foreach($I as $right)
	{
		/*@var $right absences_Right */
		if (!$right->isResulted())
		{
			continue;
		}
	
		$y = $right->getYear();
	
		if (!isset($y))
		{
			continue;
		}

		$rights[] = $right;
	}
	
	return $rights;
}





function absences_ArchiveRightsSave($values)
{
    global $babBody;
	$year = $values['year'];
	
	if (empty($year))
	{
		return false;
	}
	
	$filtered = array();
	$rights = absences_getArchivableRights();
	foreach($rights as $right) {
	    if ($right->getYear() == $year) {
	       $filtered[] = $right;
	    }
	}
	
	if (0 === count($filtered)) {
	    $babBody->addNextPageMessage(absences_translate('No rights to archive'));
	    return true;
	}
	
	foreach($filtered as $right)
	{
		$right->archive();
	}
	
	$babBody->addNextPageMessage(sprintf(absences_translate('%d rights where archived'), count($filtered)));
	return true;
}




/**
 * Archivage des droits par annee
 * proposer les annes contenant que des droits soldes et inactifs (les droits supportant les soldes negatifs doivent etres desactives)
 */
function absences_ArchiveRights()
{
	
	$W = bab_Widgets();
	$page = $W->BabPage();
	
	
	
	if (isset($_POST['archive']))
	{
		
		if( isset($_POST['archive']['save'] ))
		{
			$values = $_POST['archive'];
			absences_ArchiveRightsSave($values);
		}
		
		// Go to main menu
		$url = new bab_url;
		$url->tg = 'addon/absences/vacadm';
		$url->location();
	}
	
	
	
	
	$editor = new absences_RightArchiveYearEditor();
	
	$page->setTitle(absences_translate('Archive vacation rights by year'));
	$page->addItem($editor);
	$page->displayHtml();
}





function absences_ArchiveRequestsSave($values)
{
    global $babBody;
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	
	$organization = (int) $values['organization'];
	$year = $values['year'];
	
	if (empty($year))
	{
		return false;
	}
	
	
	require_once dirname(__FILE__).'/utilit/request.class.php';
	$I = new absences_RequestIterator();
	$I->archived = 0;
	
	if ($organization > 0) {
	    $I->organization = $organization;
	}
	
	$day = absences_getVacationOption('archivage_day');
	$month = absences_getVacationOption('archivage_month');
	$startDate = new BAB_DateTime($year, $month, $day);
	$endDate = new BAB_DateTime($year+1, $month, $day-1);
	$I->startFrom = $startDate->getIsoDate(). ' 00:00:00';
	$I->startTo = $endDate->getIsoDate(). ' 23:59:59';
	
	
	if (0 === $I->count()) {
	    $babBody->addNextPageMessage(absences_translate('No requests to archive'));
	    return true;
	}
	
	
	foreach($I as $request)
	{
		$request->archive();
	}
	

	$babBody->addNextPageMessage(sprintf(absences_translate('%d requests where archived'), $I->count()));

	return true;
}



/**
 * Archivage des demandes par annees
 */
function absences_ArchiveRequests()
{
	
	$W = bab_Widgets();
	$page = $W->BabPage();

	
	if (isset($_POST['archive']))
	{
	
		if( isset($_POST['archive']['save'] ))
		{
			$values = $_POST['archive'];
			absences_ArchiveRequestsSave($values);
		}
	
		// Go to main menu
		$url = new bab_url;
		$url->tg = 'addon/absences/vacadm';
		$url->location();
	}
	
	
	
	
	$editor = new absences_RequestArchiveYearEditor();
	
	
	$page->addStyleSheet(absences_Addon()->getStylePath().'vacation.css');
	$page->setTitle(absences_translate('Archive requests by year'));
	$page->addItem($editor);
	$page->displayHtml();
}


/* main */
bab_requireCredential();
$agent = absences_Agent::getCurrentUser();
if( !$agent->isManager())
{
	$babBody->msgerror = absences_translate("Access denied");
	return;
}

$idx = bab_rp('idx', 'request');
$babBody->addItemMenu("menu", absences_translate("Management"), absences_addon()->getUrl()."vacadm&idx=menu");


switch($idx)
{
	case 'right':
		
		absences_ArchiveRights();
		$babBody->addItemMenu("right", absences_translate("Archive"), absences_addon()->getUrl()."archive&idx=right");
		
		break;
		
	case 'request':
		absences_ArchiveRequests();
		$babBody->addItemMenu("request", absences_translate("Archive"), absences_addon()->getUrl()."archive&idx=right");
		
		break;
}


$babBody->setCurrentItemMenu($idx);
bab_siteMap::setPosition('absences','User');