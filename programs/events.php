<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

include_once dirname(__FILE__).'/functions.php';


function absences_onUserDeleted(bab_eventUserDeleted $event) {

	require_once dirname(__FILE__).'/utilit/agent.class.php';
	
	$agent = absences_Agent::getFromIdUser($event->id_user);
	$agent->delete();
}


/**
 * before sitemap creation
 * registered on this event to add more nodes to core sitemap
 *
 * @param	bab_eventBeforeSiteMapCreated 	$event
 */
function absences_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event) {

	
	require_once dirname(__FILE__).'/utilit/agent.class.php';
	
	$addon = bab_getAddonInfosInstance('absences');
	bab_functionality::includeOriginal('Icons');
	
	if (bab_isUserLogged())
	{
		$agent = absences_Agent::getCurrentUser();
	}

	// add node to access sitemap editor

	$delegations = bab_getUserSitemapDelegations();


	foreach( $delegations as $key => $deleg ) {
		
		if (bab_isUserAdministrator())
		{

			$position = array('root', $key, 'babAdmin', 'babAdminSection');
	
			$item = $event->createItem('absencesAdmin');
	
			$item->setLabel(absences_translate('Vacations'));
			$item->setDescription(absences_translate('Vacations management'));
			$item->setLink($addon->getUrl().'admvacs');
			$item->addIconClassname(Func_Icons::APPS_VACATIONS);
			$item->setPosition($position);
	
			$event->addFunction($item);
		
		}


		if (isset($agent) && ($agent->isInPersonnel() || $agent->isManager() || $agent->isEntityManager()))
		{
		
			$position = array('root', $key, 'babUser', 'babUserSection');
	
			$item = $event->createItem('absencesUser');
	
			$item->setLabel(absences_translate('Vacations'));
			$item->setDescription(absences_translate('Vacations management'));
			$item->setLink($addon->getUrl().'vacuser');
			$item->addIconClassname(Func_Icons::APPS_VACATIONS);
			$item->setPosition($position);
	
			$event->addFunction($item);

		}
	}
}



function absences_onUserModified(bab_eventUserModified $event)
{
	
}




function absences_onBeforeWaitingItemsDisplayed(bab_eventBeforeWaitingItemsDisplayed $event)
{

	include_once $GLOBALS['babInstallPath']."utilit/wfincl.php";
	include_once dirname(__FILE__)."/utilit/vacincl.php";
	
	global $babDB;
	
	$inst = bab_getWaitingIdSAInstance(bab_getUserId());
	
	if (!$inst)
	{
		return;
	}
	
	
	absences_listWaitingEntries($event, $inst);
	absences_listWaitingWorkperiod($event, $inst);
	absences_listWaitingDeposit($event, $inst);
}





function absences_onConfirmMultipleWaitingItems(bab_eventConfirmMultipleWaitingItems $event)
{
	require_once dirname(__FILE__).'/utilit/entry.class.php';
	require_once dirname(__FILE__).'/utilit/workperiod_recover_request.class.php';
	require_once dirname(__FILE__).'/utilit/cet_deposit_request.class.php';
	require_once dirname(__FILE__).'/utilit/request.notify.php';
	require_once dirname(__FILE__).'/utilit/vacincl.php';
	
	$entries = $event->getItems('absences_listWaitingEntries');
	foreach($entries as $id_entry)
	{
		$entry = absences_Entry::getById($id_entry);
		$entry->approbationNext(true, '');
		$entry->applyDynamicRight();
		$entry->notifyOwner();
	}
	
	$workperiods = $event->getItems('absences_listWaitingWorkperiod');
	foreach($workperiods as $id_workperiod)
	{
		$workperiod = absences_WorkperiodRecoverRequest::getById($id_workperiod);
		$workperiod->approbationNext(true, '');	
		$workperiod->notifyOwner();
	}
	
	$deposits = $event->getItems('absences_listWaitingDeposit');
	foreach($deposits as $id_deposit)
	{
		$deposit = absences_CetDepositRequest::getById($id_deposit);
		$deposit->approbationNext(true, '');
		$deposit->notifyOwner();
	}
	
	
	$defer = (bool) absences_getVacationOption('approb_email_defer');
	if (!$defer)
	{
		
		absences_notifyRequestApprovers();
	}
}





function absences_listWaitingEntries(bab_eventBeforeWaitingItemsDisplayed $event, Array $inst)
{
	global $babDB;
	$res = $babDB->db_query("select e.*, GROUP_CONCAT(DISTINCT t.name ORDER BY t.name ASC SEPARATOR ', ') types from ".ABSENCES_ENTRIES_TBL." e 
				LEFT JOIN absences_entries_elem ee ON ee.id_entry=e.id 
				LEFT JOIN absences_rights r ON r.id=ee.id_right 
				LEFT JOIN absences_types t ON t.id=r.id_type 
	            
			where idfai IN (".$babDB->quote($inst).") GROUP BY e.id ORDER BY date_begin ASC");
	
	if (0 === $babDB->db_num_rows($res))
	{
		return;
	}
	
	if ($event instanceof bab_eventWaitingItemsCount)
	{
		$event->addItemCount(absences_translate("Vacations requests waiting to be validated"), $babDB->db_num_rows($res));
		return;
	} 
	
	$arr = array();

	while ($row = $babDB->db_fetch_assoc($res))
	{
		$quantity = absences_vacEntryQuantity($row['id']);
		$period = absences_DateTimePeriod($row['date_begin'], $row['date_end']);
		$description = bab_toHtml(sprintf("%s (%s)\n%s", $quantity, $row['types'], $period), BAB_HTML_ALL ^ BAB_HTML_P);
		
		if ($row['firstconfirm']) {
		    $description .= '<br /><img src="'.$GLOBALS['babInstallPath'].'skins/ovidentia/images/Puces/edit.png" align="absmiddle" /> 
		    <span style="color:red">'.bab_toHtml(absences_translate('Modification of an allready confirmed request')).'</span>';
		}
		
		if ($row['todelete']) {
		    $description .= '<br /><img src="'.$GLOBALS['babInstallPath'].'skins/ovidentia/images/Puces/del.gif" align="absmiddle" /> 
		    <span style="color:red">'.bab_toHtml(absences_translate('Deletion request')).'</span>';
		}
		
		if (!empty($row['comment']))
		{
			$description .='<br /><br />';
			$description .=sprintf('%s : <em>%s</em>', bab_toHtml(absences_translate('Applicant comment')), bab_toHtml($row['comment']));
		}

		$l = array(
				'text' 			=> bab_getUserName($row['id_user']),
				'description' 	=> $description,
				'url'			=> absences_addon()->getUrl()."approb&idx=confvac&idvac=".$row['id'],
				'popup'			=> 1,
				'idschi'		=> $row['idfai'],
				'id'			=> $row['id']
		);


		$arr[] = $l;
	}


	if ($arr) {
		$event->addObject(absences_translate("Vacations requests waiting to be validated"), $arr, __FUNCTION__);
	}
	
}




function absences_listWaitingWorkperiod(bab_eventBeforeWaitingItemsDisplayed $event, Array $inst)
{
	global $babDB;
	
	$res = $babDB->db_query("select r.*, t.name type from
			absences_workperiod_recover_request r
			LEFT JOIN absences_workperiod_type t ON t.id=r.id_type
			where r.idfai IN (".$babDB->quote($inst).") order by r.date_begin asc");
	
	if (0 === $babDB->db_num_rows($res))
	{
		return;
	}
	
	
	if ($event instanceof bab_eventWaitingItemsCount)
	{
		$event->addItemCount(absences_translate("Worked days entitling recovery waiting to be validated"), $babDB->db_num_rows($res));
		return;
	} 
	
	$arr = array();

	while ($row = $babDB->db_fetch_assoc($res))
	{


		$l = array(
				'text' 			=> bab_getUserName($row['id_user']),
				'description' 	=> sprintf(absences_translate('%s from %s to %s'), $row['type'], absences_shortDate(bab_mktime($row['date_begin'])), absences_shortDate(bab_mktime($row['date_end']))),
				'url'			=> absences_addon()->getUrl()."approb&idx=recover&id_workperiod=".$row['id'],
				'popup'			=> 1,
				'idschi'		=> $row['idfai'],
				'id'			=> $row['id']
		);


		$arr[] = $l;
	}


	if ($arr) {
		$event->addObject(absences_translate("Worked days entitling recovery waiting to be validated"),$arr, __FUNCTION__);
	}
	
	
}




function absences_listWaitingDeposit(bab_eventBeforeWaitingItemsDisplayed $event, Array $inst)
{
	global $babDB;
	
	
	$res = $babDB->db_query("
		SELECT cet.*, r.quantity_unit, r.description
		FROM
			absences_cet_deposit_request cet
			LEFT JOIN absences_users_rights ur ON ur.id=cet.id_agent_right_source
			LEFT JOIN absences_rights r ON r.id=ur.id_right
		WHERE
			cet.idfai IN (".$babDB->quote($inst).") order by cet.createdOn asc");
	
	if (0 === $babDB->db_num_rows($res))
	{
		return;
	}
	
	if ($event instanceof bab_eventWaitingItemsCount)
	{
		$event->addItemCount(absences_translate("Deposits in the time saving account waiting to be validated"), $babDB->db_num_rows($res));
		return;
	} 
	
	$arr = array();

	while ($row = $babDB->db_fetch_assoc($res))
	{
		$description = isset($row['description']) ? $row['description'] : absences_translate('unknown');
		$quantity_unit = isset($row['quantity_unit']) ? $row['quantity_unit'] : absences_translate('unknown');

		$l = array(
				'text' 			=> bab_getUserName($row['id_user']),
				'description' 	=> sprintf(absences_translate('Move %s from %s'), absences_quantity($row['quantity'], $quantity_unit), $description),
				'url'			=> absences_addon()->getUrl()."approb&idx=cet&id_deposit=".$row['id'],
				'popup'			=> 1,
				'idschi'		=> $row['idfai'],
				'id'			=> $row['id']
		);


		$arr[] = $l;
	}


	if ($arr) {
		$event->addObject(absences_translate("Deposits in the time saving account waiting to be validated"),$arr, __FUNCTION__);
	}
	
}



/**
 * Refresh calendar if modified
 * @param	bab_eventPeriodModified	$event
 */
function absences_onModifyPeriod($event) {
	global $babDB;

	$vacation 	= (BAB_PERIOD_VACATION 	=== ($event->types & BAB_PERIOD_VACATION));
	$nwday		= (BAB_PERIOD_NWDAY 	=== ($event->types & BAB_PERIOD_NWDAY));
	$working	= (BAB_PERIOD_WORKING	=== ($event->types & BAB_PERIOD_WORKING));

	if (!$vacation && !$nwday && !$working) {
		return;
	}

	if (false === $event->id_user) {
		$babDB->db_query("TRUNCATE absences_calendar");
		return;
	}

	require_once dirname(__FILE__).'/utilit/vacincl.php';
	absences_clearUserCalendar($event->id_user);
	return;

	// le code si dessous existait pour optimiser le cache mais dans de rares cas cela ne fonctionne pas
	// la modification d'une periode peut entrainer des changement de couleurs sur une autre periode qui n'a pas
	// ete modifiee

	/*
	 if (false === $event->begin || false === $event->end) {
	absences_clearUserCalendar($event->id_user);
	return;
	}


	include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

	$date_begin = BAB_DateTime::fromTimeStamp($event->begin);
	$date_end	= BAB_DateTime::fromTimeStamp($event->end);
	$date_end->add(1, BAB_DATETIME_MONTH);

	while ($date_begin->getTimeStamp() <= $date_end->getTimeStamp()) {
	$month	= $date_begin->getMonth();
	$year	= $date_begin->getYear();
	absences_updateCalendar($event->id_user, $year, $month);
	$date_begin->add(1, BAB_DATETIME_MONTH);
	}

	*/
}





function absences_onBeforePeriodsCreated(bab_eventBeforePeriodsCreated $event)
{
	$userperiods = $event->periods;
	/*@var $userperiods bab_UserPeriods */
	
	$users = $userperiods->getUsers();
	
	if ($userperiods->isPeriodCollection('bab_VacationPeriodCollection') && $users) {
		require_once dirname(__FILE__)."/utilit/vacincl.php";
		absences_setVacationPeriods($userperiods, $users);
	}
}




function absences_onHourly(LibTimer_eventHourly $event)
{
	require_once dirname(__FILE__).'/utilit/vacincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
	require_once dirname(__FILE__).'/utilit/right.class.php';
	require_once dirname(__FILE__).'/utilit/request.class.php';
	global $babDB;
	
	$defer = (bool) absences_getVacationOption('approb_email_defer');
	
	// daily notification after 12:00
	
	if ($defer)
	{
		if (12 <= (int) date('G'))
		{
			$registry = bab_getRegistryInstance();
			$registry->changeDirectory('/absences/');

			// get last saved call
			$lastcall = $registry->getValue('approb_email_defer');
			
			if (null === $lastcall || ((time() - bab_mktime($lastcall)) / 3600) > 12  )
			{
				// pas de dernier appel ou dernier appel il y a plus de 12 heures
				
				$registry->setKeyValue('approb_email_defer', date('Y-m-d H:i:s'));
				
				require_once dirname(__FILE__).'/utilit/request.notify.php';
				absences_notifyRequestApprovers();
			}
		}
	}
	
	
	// droit avec ajout de solde en debut de mois tant que la date de fin n'est pas connue

	$I = new absences_RightIterator();
	$I->kind = absences_Right::INCREMENT;
	$I->increment = true;
	
	foreach($I as $right)
	{
		/*@var $right absences_Right */
		
		if ($right->getKind() !== absences_Right::INCREMENT)
		{
			throw new Exception('Error in iterator, wrong right kind');
		}

		$right->monthlyQuantityUpdate($event);
	}
	
	
	// auto approbation
	
	$auto_confirm = (int) absences_getVacationOption('auto_confirm');
	if (0 !== $auto_confirm)
	{
		$date = BAB_DateTime::now();
		$date->less($auto_confirm, BAB_DATETIME_DAY);
		
		$I = new absences_RequestIterator;
		$I->status = '';
		$I->modifiedOn = $date->getIsoDateTime();
		
		foreach($I as $request)
		{
		    /*@var $request absences_Request */
			$request->autoConfirm();
		}
	}	
	
	
	// synchro
	
	$I = new absences_RightIterator();
	$I->sync_status = absences_Right::SYNC_CLIENT;
	
	if (0 < $I->count())
	{
		require_once dirname(__FILE__).'/utilit/client.class.php';
		$client = new absences_client;
		foreach($I as $right)
		{
			$client->updateRight($right->uuid);
		}
	}
}



function absences_onAfterEventCategoryDeleted(bab_eventAfterEventCategoryDeleted $event)
{
	global $babDB;
	$babDB->db_query("update absences_collections SET id_cat='0' WHERE id_cat=".$babDB->quote($event->id_category));
}



function absences_onOrgChartEntityAfterDeleted(bab_eventOrgChartEntityAfterDeleted $event)
{
	global $babDB;
	$babDB->db_query("DELETE from absences_planning where id_entity=".$babDB->quote($event->id_entity));
}




/**
 * Directory entry modification/creation/delete
 * 
 * @param bab_eventDirectory $event
 */
function absences_onDirectory(bab_eventDirectory $event)
{
    if (!($event instanceof bab_eventUserCreated || $event instanceof bab_eventUserModified)) {
        return;
    }
    
    require_once dirname(__FILE__).'/utilit/vacincl.php';
    
    if (0 === (int) absences_getVacationOption('organization_sync')) {
        return;
    }
    
    
    $id_user = (int) $event->id_user;
    
    
    require_once dirname(__FILE__).'/utilit/agent.class.php';
    
    // set the associated organization
    $agent = absences_Agent::getFromIdUser($id_user);
    
    if (!$agent->exists()) {
        return;
    }
    
    require_once dirname(__FILE__).'/utilit/organization.class.php';
    
    // update the organization list
    absences_Organization::createFromDirectory();
    
    try {
        $agent->setOrganizationFromDirEntry();
    } catch (Exception $e) {
        // ignore error, organization is not set in dir entry
    }
    
}
